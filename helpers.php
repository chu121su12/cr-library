<?php

use CR\Library\Helpers\Helpers;
use Symfony\Component\VarDumper\VarDumper;

if (! \function_exists('dd')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed
     * @return void
     */
    function dd(...$args)
    {
        foreach ($args as $x) {
            VarDumper::dump($x);
        }

        die(1);
    }
}

if (! \function_exists('ddd')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed
     * @return void
     */
    function ddd(...$args)
    {
        echo "\"/>--><!-- --><pre>\n";

        foreach ($args as $x) {
            \var_dump($x);
        }

        die(1);
    }
}

if (! \function_exists('micro_time')) {
    /**
     * Create a new CarbonImmutable instance for the current time.
     *
     * @param  \DateTimeZone|string|null  $tz
     * @return \Carbon\CarbonImmutable
     */
    function micro_time($tz = null)
    {
        return Helpers::microTime($tz);
    }
}
