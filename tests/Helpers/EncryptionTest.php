<?php

namespace Tests\Helpers;

use CR\Library\Helpers\Encryption as E;

class EncryptionTest extends \Tests\LaravelOrchestraTestCase
{
    /**
     * @dataProvider encryptDataProvider
     */
    public function testEncryption($value)
    {
        $this->assertSame($value, E::aesDecrypt(E::aesEncrypt($value)));

        $this->assertSame($value, E::rsaDecrypt(E::rsaEncrypt($value)));

        $value = \serialize($value);

        $this->assertSame($value, E::aesDecryptString(E::aesEncryptString($value)));

        $this->assertSame($value, E::rsaDecryptString(E::rsaEncryptString($value)));

        $this->assertTrue(E::rsaVerifyString(E::rsaSignString($value), $value));
    }

    public static function encryptDataProvider()
    {
        return [
            [''],
            [null],
            ['a'],
            [\str_repeat('c', 100)],
        ];
    }
}
