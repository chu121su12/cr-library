<?php

namespace Tests\Helpers;

use CR\Library\Helpers\Helpers as H;

class HelpersTest extends \Tests\LaravelOrchestraTestCase
{
    /**
     * @dataProvider romanNumeralDataProvider
     */
    public function testRomanNumeral($expected, $value)
    {
        $this->assertSame($expected, H::langNumberToRomanNumeral($value));
    }

    public static function romanNumeralDataProvider()
    {
        return [
            ['I', 1],
            ['II', 2],
            ['IV', 4],
            ['V', 5],
            ['VI', 6],
            ['CXXIII', 123],
        ];
    }
}
