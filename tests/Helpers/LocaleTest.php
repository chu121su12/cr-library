<?php

namespace Tests\Helpers;

use Brick\Math\Exception\NumberFormatException;
use CR\Library\Helpers\LocaleIndonesia as L;

class LocaleTest extends \Tests\LaravelOrchestraTestCase
{
    /**
     * @dataProvider terbilangDataProvider
     */
    public function testTerbilang($expected, $value, $keepZeroDecimals)
    {
        $this->assertSame($expected, L::terbilang($value, $keepZeroDecimals));
    }

    public static function terbilangDataProvider()
    {
        return [
            ['nol', 0, false],
            ['nol', '0', false],
            ['seratus dua puluh tiga juta empat ratus lima puluh enam ribu tujuh ratus delapan puluh sembilan', '123456789', false],
            ['tiga ratus sebelas koma empat enam sembilan', '311.469', false],
            ['nol koma nol lima', '0.05', false],
            ['nol koma nol enam', '0.060', false],
            ['nol koma nol enam nol', '0.060', true],
        ];
    }

    public function testFormatNumber()
    {
        $fn = function ($a, $b) {
            try {
                return L::formatNumber($a, $b);
            } catch (NumberFormatException $e) {
                return $e->getMessage();
            }
        };

        $this->assertSame('1,234.5', $fn('1234.5', null));
        $this->assertSame('The number 1234.5 overflow decimals.', $fn('1234.5', 0));
        $this->assertSame('1,234.5', $fn('1234.5', 1));
        $this->assertSame('1,234.50', $fn('1234.5', 2));

        $this->assertSame('1,234.50', $fn('1234.50', null));
        $this->assertSame('The number 1234.50 overflow decimals.', $fn('1234.50', 0));
        $this->assertSame('1,234.5', $fn('1234.50', 1));
        $this->assertSame('1,234.50', $fn('1234.50', 2));
        $this->assertSame('1,234.500', $fn('1234.50', 3));

        $this->assertSame('3', $fn('3', null));
        $this->assertSame('34', $fn('34', null));
        $this->assertSame('345', $fn('345', null));
        $this->assertSame('3,456', $fn('3456', null));
        $this->assertSame('345,678', $fn('345678', null));
        $this->assertSame('3,456,789', $fn('3456789', null));
    }
}
