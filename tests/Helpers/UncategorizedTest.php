<?php

namespace Tests\Helpers;

use Carbon\CarbonImmutable;
use CR\Library\Helpers\Uncategorized as H;
use CR\Library\Helpers\Uncategorized;

class UncategorizedTest extends \Tests\LaravelOrchestraTestCase
{
    /**
     * @dataProvider providerParseLltllvFields
     */
    public function testParseLltllvFields($value, $expected)
    {
        $this->assertSame($expected, H::parseLltllvFields($value));
    }

    public static function providerParseLltllvFields()
    {
        return [
            ['2020119asdqwezxc11r16rtyfgh', [
                '__REMAINING__' => '',
                '01' => 'asdqwezxc',
                'r' => 'rtyfgh',
            ]],
            ['201119asdqwezxc11r16rtyfgh', [
                '__REMAINING__' => '',
                1 => 'asdqwezxc',
                'r' => 'rtyfgh',
            ]],
            ['201119asdqwezxca1r16rtyfgh', [
                '__REMAINING__' => 'a1r16rtyfgh',
                1 => 'asdqwezxc',
            ]],
            ['120119asdqwezxc12rs16rtyfgh', [
                '__REMAINING__' => '',
                '01' => 'asdqwezxc',
                'rs' => 'rtyfgh',
            ]],
        ];
    }

    /**
     * @dataProvider providerParseLlvArray
     */
    public function testParseLlvArray($value, $expected)
    {
        $this->assertSame($expected, H::parseLlvArray($value));
    }

    public static function providerParseLlvArray()
    {
        return [
            ['2020119asdqwezxc11r16rtyfgh', [
                0 => '01',
                1 => 'asdqwezxc',
                2 => 'r',
                3 => 'rtyfgh',
                '__REMAINING__' => '',
            ]],
            ['201119asdqwezxc11r16rtyfgh', [
                0 => '1',
                1 => 'asdqwezxc',
                2 => 'r',
                3 => 'rtyfgh',
                '__REMAINING__' => '',
            ]],
            ['201119asdqwezxca1r16rtyfgh', [
                0 => '1',
                1 => 'asdqwezxc',
                '__REMAINING__' => 'a1r16rtyfgh',
            ]],
        ];
    }

    /**
     * @dataProvider providerParseTlvFields
     */
    public function testParseTlvFields($value, $expected)
    {
        $this->assertSame($expected, H::parseTlvFields($value));
    }

    public static function providerParseTlvFields()
    {
        return [
            ['0109asdqwezxcrs06rtyfgh', [
                '__REMAINING__' => '',
                '01' => 'asdqwezxc',
                'rs' => 'rtyfgh',
            ]],
            ['0109asdqwezxcrsffrtyfgh', [
                '__REMAINING__' => 'rsffrtyfgh',
                '01' => 'asdqwezxc',
            ]],
        ];
    }

    /**
     * @dataProvider providerParseTlvField
     */
    public function testParseTlvField($value, $expected)
    {
        $this->assertSame($expected, H::parseTlvField('01', $value));
    }

    public static function providerParseTlvField()
    {
        return [
            ['0109asdqwezxc', ['asdqwezxc', true]],
            ['0109asdqwezx', ['asdqwezx', false]],
            ['0109asdqwezxde', ['asdqwezxd', 'e']],
            ['0209asdqwezx', ['', false]],
            ['01abasdqwezxc', ['', false]],
            ['0100', ['', true]],
            ['01001', ['', '1']],
        ];
    }

    public function testGenerateBasedTimeId()
    {
        $time = CarbonImmutable::createFromFormat('Ymd His u', '20230101 000000 000000');
        $this->assertSame('1k72t0000000', H::generateBasedTimeId($time));

        $time = CarbonImmutable::createFromFormat('Ymd His u', '20240102 030405 678912');
        $this->assertSame('1k82u3qic0k0', H::generateBasedTimeId($time));

        $time = CarbonImmutable::createFromFormat('Ymd His u', '90001231 235960 999900');
        $this->assertSame('6y12t000ach0', H::generateBasedTimeId($time));

        $time = CarbonImmutable::createFromFormat('Ymd His u', '90001231 240000 999949');
        $this->assertSame('6y12t000ach0', H::generateBasedTimeId($time));

        $time = CarbonImmutable::createFromFormat('Ymd His u', '90010101 000000 999999');
        $this->assertSame('6y12t000ach0', H::generateBasedTimeId($time));

        $time = CarbonImmutable::createFromFormat('Ymd His u', '99991231 235959 999999');
        $this->assertSame('7pry7u5gm240', H::generateBasedTimeId($time));

    }
}
