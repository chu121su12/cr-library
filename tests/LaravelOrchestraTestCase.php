<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase;
use Illuminate\Support\Facades\Config;
use Orchestra\Testbench\Concerns\CreatesApplication;

class LaravelOrchestraTestCase extends TestCase
{
    use CreatesApplication;

    public function setUp()
    {
        parent::setUp();

        Config::set('app.key', 'base64://////////////////////////////////////////8=');
    }
}
