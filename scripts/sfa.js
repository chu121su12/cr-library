window._sfa2 = ((window, document) => {
    function consoleLog() {
        console.log(arguments);
        return arguments[0];
    }

    const {
        computed,
        getCurrentInstance,
        h,
        nextTick,
        onBeforeMount,
        onMounted,
        onUnmounted,
        reactive,
        ref,
        resolveComponent,
        toRef,
        toRefs,
        watch,
        watchEffect,
    } = Vue;

    const _ohop = Object.prototype.hasOwnProperty;
    const _ots = Object.prototype.toString;
    const _aslice = Array.prototype.slice;
    const raf = requestAnimationFrame;
    const EN_DASH = '—';
    const EM_DASH = '—';

    const DEFAULT_FILLED_CASES = [undefined, null, '', [], {}];
    const STR_PROPER_CASE_RE = /(?:_|(?<=[^A-Z0-9]))(?=[A-Z])/g;
    const STR_TRIM_SPACE_RE = /^\s+|\s+$/g;
    const XHR_SPACE_ENCODED_RE = /%20/g;

    const getType = object => _ots.call(object);
    const isArray = object => getType(object) === '[object Array]';
    const isFilled = (value, comparedTo) => (comparedTo ? onlyArray(comparedTo) : DEFAULT_FILLED_CASES).indexOf(value) === -1;
    const isFunction = object => typeof object === 'function';
    const isNumeric = object => !isNaN(parseFloat(object)) && isFinite(object);
    const isObject = object => getType(object) === '[object Object]';
    const isString = object => getType(object) === '[object String]';
    const onlyArray = arrayLike => isArray(arrayLike) ? arrayLike : [];
    const onlyObject = objectLike => isObject(objectLike) ? objectLike : {};
    const strCapitalize = str => str.charAt(0).toUpperCase() + str.slice(1);
    const toArray = arrayLike => _aslice.call(arrayLike);

    const domAppendChild = (parent, child) => parent.appendChild(child);
    const domBodyOrHtml = () => domBody() || domHtml();
    const domBody = () => document.body || domFirstTag('body');
    const domFirstTag = tag => toArray(document.getElementsByTagName(tag))[0];
    const domHead = () => document.head || domFirstTag('head');
    const domHtml = () => document.documentElement || domFirstTag('html');

    const domCreate = (element, options, children) => {
        const el = document.createElement(element);
        if (isObject(options)) {
            objectEach(options, (value, prop) => {
                if (isObject(el[prop]) && isObject(value)) {
                    objectMerge(el[prop], value);
                } else {
                    el[prop] = value;
                }
            });
        }
        onlyArray(children).forEach(child => domAppendChild(el, child));
        return el;
    };

    const domDetach = element => {
        const parent = element & element.parentNode;
        if (parent && parent.removeChild) {
            parent.removeChild(element);
        }
    };

    const instantLink = href => {
        const link = domCreate('a', {
            href,
            target: '_blank',
        });
        domAppendChild(domBodyOrHtml(), link);
        setTimeout(() => {
            link.click();
            setTimeout(() => domDetach(link), 1);
        }, 1);
    }

    const simpleHash = object => {
        const string = JSON.stringify(object);
        if (!string || string.length == 0) {
            return 0;
        }

        let hash = 0;
        for (let char = '', i = 0, l = string.length; i < l; ++i) {
            char = string.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash = hash & hash;
        }

        return hash;
    };

    const randomString = (length, pool) => {
        length = length || 16;
        pool = pool || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        const charactersLength = pool.length;
        let result = '';

        for (let i = 0; i < length; ++i) {
            result += pool.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
    };

    const iCookie = name => {
        const match = document.cookie.match(new RegExp(name + '=([^;]+)', 'i'));
        return match ? match[1] : undefined;
    };

    const promiseSerial = promises => onlyArray(promises).reduce((promise, fn) => promise.then(
        result => fn().then(Array.prototype.concat.bind(result))
    ), Promise.resolve([]));

    const defaultActionHandler = (action, context) => {
        if (action && action.message) {
            alert(action.message);
        } else if (context.xhr.status >= 400) {
            alert(context.xhr.statusText);
        }
    };
    let activeActionHandler = defaultActionHandler;
    const actionHandlerWrapper = (responseOrError, options) => {
        const response = responseOrError.response || responseOrError || {};
        const data = response.data || {};

        (data._actions || [{}]).forEach(action => activeActionHandler(action, {
            options,
            data,
            xhr: response.xhr,
        }));

        if (responseOrError.response) {
            throw responseOrError;
        }

        return responseOrError;
    };

    const arrayWrap = object => {
        switch (getType(object)) {
            case '[object Array]':
                return toArray(object);

            case '[object Null]':
            case '[object Undefined]':
                return [];

            case '[object String]':
            case '[object Number]':
            case '[object Boolean]':
                return [object];

            case '[object Object]':
            default:
                return objectValues(object);
        }
    };

    const arrayFlattenObject = array => {
        const object = {};
        array.forEach(item => objectMerge(object, item));
        return object;
    };

    const objectEvery = (object, callback) => {
        if (object) {
            let i = 0;
            for (let prop in object) {
                if (_ohop.call(object, prop)) {
                    if (callback(object[prop], prop, i++, object) === false) {
                        return false;
                    }
                }
            }
        }
        return true;
    };

    const objectEach = (object, callback) => objectEvery(object, (v, k, i, o) => callback(v, k, i, o) || true);
    const objectMap = (object, callback) => (objectEach(object, (v, k, i, o) => object[k] = callback(v, k, i, o)), object);

    const objectMapToArray = (object, callback) => {
        const array = [];
        objectEach(object, (v, k, i, o) => callback(v, k, i, o));
        return array;
    };

    const objectValues = object => {
        const values = [];
        objectEach(obj, item => values.push(item));
        return values;
    };

    const objectLength = object => {
        let count = 0;
        objectEach(object, () => ++count);
        return count;
    };

    const dataGet = (obj, key, defaultValue, separator) => {
        const keys = key.split(separator || '.');
        for (let i = 0, l = keys.length; i < l; ++i) {
            let loop = keys[i];
            if (isObject(obj)) {
                if (!_ohop.call(obj, loop)) {
                    return defaultValue;
                }

            } else if (obj.length > 0) {
                loop = parseInt(loop, 10);

                if (loop >= obj.length) {
                    return defaultValue;
                }

            }

            obj = obj[loop];
        }

        return obj === undefined ? defaultValue : obj;
    };

    const dataHas = (obj, key, separator) => {
        const keys = key.split(separator || '.');

        for (let i = 0, l = keys.length; i < l; ++i) {
            let loop = keys[i];
            if (isObject(obj)) {
                if (!_ohop.call(obj, loop)) {
                    return false;
                }

            } else if (obj.length > 0) {
                loop = parseInt(loop, 10);

                if (loop >= obj.length) {
                    return false;
                }

            }

            obj = obj[loop];
        }

        return obj !== undefined;
    };

    const dataSet = (obj, key, setValue, separator) => {
        const keys = key.split(separator || '.');

        for (let i = 0, l = keys.length; i < l; ++i) {
            if (isObject(obj)) {
                if (!_ohop.call(obj, loop)) {
                    obj[loop] = {};
                    continue;
                }

            } else if (obj.length > 0) {
                let loopInt = loop = parseInt(loop, 10);

                while (loopInt-- >= obj.length) {
                    obj.push(undefined);
                }
            }

            obj = obj[loop];
        }

        obj[loop] = setValue;
    };

    const addEventListener = (element, event, callback, options) => isObject(options) ? element.addEventListener(event, callback, options) : element.addEventListener(event, callback);

    const off = (element, event, callback, options) => options ? element.removeEventListener(event, callback, options) : element.removeEventListener(event, callback);

    const on = (element, event, callback, options) => {
        addEventListener(element, event, callback, options);
        let isUnloaded = false;
        return {
            start: new Date(),
            callback,
            element,
            event,
            options,

            off() {
                if (!isUnloaded) {
                    isUnloaded = true;
                    off(element, event, callback, options);
                }
            },
        };
    };

    const autoEvent = (element, event, callback, options) => {
        const auto = on(element, event, callback, options);
        once(window, 'unload', auto.off);
        return auto;
    };

    const once = (element, event, callback, options) => on(element, event, callback, objectMerge({}, options, { once: true }));

    const preventLeaving = () => on(window, 'beforeunload', event => (event.preventDefault(), false));

    const ready = (() => {
        let isReady = false;
        let readyStack = [];
        const invoke = callback => callback && callback();

        (callback => {
            if (document.readyState !== 'loading') {
                callback();
            } else if (document.addEventListener) {
                once(document, 'DOMContentLoaded', callback);
            } else {
                document.attachEvent('onreadystatechange', () => document.readyState !== 'loading' && callback());
            }
        })(() => {
            if (isReady === false) {
                isReady = true;
                readyStack.forEach(invoke);
                readyStack = null;
            }
        });

        const toReady = (type, callback) => {
            if (isReady) {
                invoke(callback);
            } else {
                readyStack[type](callback);
            }

            return callback;
        };

        const ready = callback => toReady('push', callback);
        ready.unshift = callback => toReady('unshift', callback);
        return ready;
    })();

    const cssStringFromObject = (() => {
        const camelizeRE = /-(\w)/g;
        const camelize = str => str.replace(camelizeRE, (_, c) => (c ? c.toUpperCase() : ''));
        const hyphenateRE = /\B([A-Z])/g;
        const hyphenate = str => str.replace(hyphenateRE, '-$1').toLowerCase();
        const prefixes = ['Webkit', 'Moz', 'ms'];
        const prefixesLc = ['webkit', 'moz', 'ms'];
        const style = domCreate('div').style;

        const autoPrefixCache = {};
        const autoPrefix = rawName => {
            if (rawName in autoPrefixCache) {
                return autoPrefixCache[rawName];
            }
            let name = strCapitalize(camelize(rawName));
            for (let i = 0; i < prefixes.length; ++i) {
                const prefixed = prefixes[i] + name;
                if (prefixed in style) {
                    return (autoPrefixCache[rawName] = `-${prefixesLc[i]}-${rawName}`);
                }
            }
            return (autoPrefixCache[rawName] = rawName);
        };

        const generateCssString = (css, important) => {
            let rule = '';
            const ruleBreak = (important ? '!important;' : ';');

            if (isArray(css)) {
                css.forEach(arrayRule => {
                    if (isString(arrayRule)) {
                        rule += arrayRule + ruleBreak;
                    } else {
                        rule += generateCssString(arrayRule, important);
                    }
                });

            } else {
                objectEach(css, (values, key) => arrayWrap(values).forEach(value => {
                    if (value !== undefined && value !== null && value !== false) {
                        rule += hyphenate(autoPrefix(key)) + ':' + value + ruleBreak;
                    }
                }));
            }

            return rule;
        };

        return generateCssString;
    })();

    const cssStringFromObjectObject = (() => {
        const CSS_ESCAPE_SELECTOR_RE = /([[\]()/,.:!*#+$])/g;

        return css => {
            if (!css) {
                return '';
            }

            if (isString(css)) {
                return css;
            }

            let cssConcat = '';

            objectEach(css, (definitions, selector) => {
                let rule = '';
                if (isString(definitions)) {
                    rule = definitions;
                } else if (isObject(definitions)) {
                    rule = cssStringFromObject(definitions);
                }

                if (rule) {
                    cssConcat += selector.replace(CSS_ESCAPE_SELECTOR_RE, '\\$1') + '{' + rule + '}';
                }
            });

            return cssConcat;
        };
    })();

    const cssStringToObject = css => {
        if (!css) {
            return {};
        }

        if (css.cssText) {
            return cssStringToObject(css.cssText);
        }

        if (!isString(css)) {
            return css;
        }

        const parts = css.split('{');
        const definitions = {};
        parts[1].split('}')[0].split(';').forEach(rule => {
            const definition = rule.split(':');
            if (definition[0].trim() && definition[1].trim()) {
                definitions[definition[0]] = definition[1];
            }
        });

        const rule = {};
        rule[parts[0]] = definitions;
        return rule;
    };

    const cssAddScope = (() => {
        const CSS_MATCH_KEYFRAME = /^\s*\d+%\s*$/; // keyframe rules && root
        const CSS_MATCH_ROOT = /^\s*(:root|html)\b/;
        const CSS_MATCH_SCOPE = /^\s*:scope\b/;

        return (root, css) => cssStringFromObjectObject(css).split('}').map(rule => {
            const parts = rule.split('{');
            const partsLength = parts.length;
            if (partsLength > 1) {
                parts[partsLength - 2] = parts[partsLength - 2].split(',').map(selector => {
                    let selectors = selector.trim().split(' ');
                    if (selector.match(CSS_MATCH_ROOT)) {
                        selectors = [selector.trim().replace(':scope', root)];
                    } else if (selector.match(CSS_MATCH_SCOPE)) {
                        selectors[0] = selectors[0].replace(':scope', root);
                    } else if (!selector.match(CSS_MATCH_KEYFRAME)) {
                        selectors.unshift(root);
                    }

                    return selectors.join(' ');
                }).join(',');
            }

            return parts.join('{');
        }).join('}');
    })();

    // // 0: true, 1: false, 2: undefined
    // const tristate = (value, falseValues, trueValues) => {
    //     // return if value is in falseValues
    //     if (onlyArray(falseValues).indexOf(value) !== -1) {
    //         return 1;
    //     }

    //     // consider undefined trueValues as reverse of falseValues
    //     if (trueValues === undefined) {
    //         return 0;
    //     }

    //     // check if value in trueValues
    //     if (onlyArray(trueValues).indexOf(value) !== -1) {
    //         return 0;
    //     }

    //     return 2;
    // };

    function resolveValues() {
        const arg0 = arguments[0];
        if (! isFunction(arg0)) {
            return arg0;
        }

        return arg0.apply(null, toArray(arguments).slice(1));
    }

    function mergeClassBinding() {
        let merged = {};
        toArray(arguments).forEach(arg => {
            if (isObject(arg)) {
                merged = { ...merged, ...arg };
            } else if (isArray(arg)) {
                arg.forEach(cls => merged[cls] = true);
            } else if (isString(arg)) {
                merged[arg] = true;
            }
        });
        return merged;
    }

    function objectMerge() {
        let merged = {};
        toArray(arguments).forEach((arg, index) => {
            if (index === 0) {
                merged = arg;
            } else {
                objectEach(arg, (v, k) => {
                    if (v !== undefined) {
                        merged[k] = v;
                    }
                });
            }
        });
        return merged;
    }

    const jsonSnapshot = data => {
        try {
            return jsonParse(JSON.stringify(data));
        } catch (error) {
            console.warn({data, error});
            return undefined;
        }
    };

    const jsonParse = data => {
        try {
            return JSON.parse(data);
        } catch (error) {
            console.warn({data, error});
            return undefined;
        }
    };

    const overlay = (() => {
        let overlay = null;

        return () => {
            if (overlay && !overlay.parentNode) {
                overlay = null;
            }

            if (!overlay) {
                overlay = domCreate('div', {
                    style: {
                        left: 0,
                        position: 'absolute',
                        top: 0,
                        zIndex: 1000,
                    },
                });

                overlay.detach = () => {
                    domDetach(overlay);
                    overlay = null;
                };
                domAppendChild(domBodyOrHtml(), overlay);
            }

            return overlay;
        };
    })();

    const watchCleanText = model => {
        const status = ref(isFilled(model.value) ? 0 : 1);

        watch(model, value => {
            if (!isFilled(value)) {
                status.value = 0;
            } else if (status.value !== 2) {
                status.value = 2;
            }
        });

        return {
            status,

            onInput: () => {
                if (status.value === 1) {
                    status.value = 2;
                }
            },
            onBlur: () => {
                if (status.value === 2) {
                    status.value = 0;
                }
            },
        };
    };

    const xhr = options => {
        options = options || {};

        let body = options.data;
        const hasBody = body !== undefined && body !== null;
        options.headers = options.headers || {};
        options.method = (options.method || (hasBody ? 'POST' : 'GET')).toUpperCase();

        let url = options.url;
        if (options.params) {
            const paramsArray = [];

            objectEach(options.params, (value, key) => paramsArray.push(
                encodeURIComponent(key) + '=' + encodeURIComponent(value)
            ));

            if (paramsArray.length) {
                url += '?' + paramsArray.join('&').replace(XHR_SPACE_ENCODED_RE, '+');
            }
        }

        if ('PUT_PATCH_DELETE_POST'.split('_').indexOf(options.method) > -1) {
            options.headers['X-Requested-With'] = 'XMLHttpRequest';
        }

        const xhr = new XMLHttpRequest();

        xhr.open(options.method, url, true);
        objectEach(options.headers, (value, key) => xhr.setRequestHeader(key, value));

        if (hasBody) {
            body = [...onlyArray(options.transformRequest), data => {
                if (typeof data === 'string') {
                    //
                } else if (data instanceof FormData) {
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                } else {
                    xhr.setRequestHeader('Content-Type', 'application/json');
                    return JSON.stringify(data);
                }

                return data;
            }].reduce((data, transformer) => transformer(data, options.headers), body);
        }

        return new Promise((resolve, reject) => {
            xhr.onreadystatechange = () => {
                if (xhr.readyState !== 4) {
                    return;
                }

                const data = xhr.responseText;

                const payload = {
                    data: jsonParse(data),
                    raw: data,
                    xhr,
                };

                if (xhr.status >= 200 && xhr.status < 400) {
                    resolve(payload);
                } else {
                    reject({ response: payload });
                }
            };

            if (hasBody) {
                xhr.send(body);
            } else {
                xhr.send();
            }
        });
    };

    xhr.withAction = options => {
        const handler = data => {
            const after = options && options.after;
            after && after();
            return actionHandlerWrapper(data, options);
        };

        return xhr(options).then(handler, handler);
    };

    const VirtualScroller = {
        components: {
            DynamicScroller: Vue3VirtualScroller.DynamicScroller,
            DynamicScrollerItem: Vue3VirtualScroller.DynamicScrollerItem,
            ResizeObserver: Vue3Resize.ResizeObserver,
        },
        props: {
            // direction = vertical/horizontal
            // keyField = id
            list: { default: [] },
            minItemSize: { default: 5 },
            sizeDependencyKeys: { default: [] },
        },
        methods: {
            sizeDependencies(item) {
                return (this.sizeDependencyKeys || []).map(key => item[key]);
            },
            scroller() {
                return this.$refs ? this.$refs.base : null;
            },
        },
        template: `<DynamicScroller ref="base" :items="(list || [])" :min-item-size="minItemSize" v-bind="$attrs" #="p"
            ><DynamicScrollerItem :active="p.active" :data-index="p.index" :item="p.item" :size-dependencies="sizeDependencies(p.item)"
                ><slot :data="p.item" :index="p.index" /></DynamicScrollerItem></DynamicScroller>`,
    };

    const Spinner = {
        props: {
            background: { default: 'transparent' },
            blade: { default: 12 },
            bladeHeightRatio: { default: 0.555 },
            bladeWidth: { default: 0.074 },
            delay: { default: 1.2 },
            deviate: { default: 0.2 },
            duration: { default: 1 },
            precision: { default: 6 },
            scale: { default: 1 },
        },
        computed: {
            wrapperStyle() {
                const size = parseFloat(this.scale).toFixed(this.precision);
                return {
                    display: 'inline-block',
                    height: `${size}em`,
                    position: 'relative',
                    width: `${size}em`,
                };
            },
        },
        methods: {
            bladeStyle(bladeIndex) {
                return {
                    animation: 'loader-spinner-fade ' + this.duration.toFixed(this.precision) + 's infinite linear',
                    animationDelay: (bladeIndex * (this.duration / this.blade)).toFixed(this.precision) + 's',
                    backgroundColor: this.background,
                    borderRadius: '1em',
                    bottom: 0,
                    height: (this.scale * (0.5 * this.bladeHeightRatio)).toFixed(this.precision) + 'em',
                    left: (this.scale * (0.5 - this.bladeWidth / 2)).toFixed(this.precision) + 'em',
                    position: 'absolute',
                    transform: 'rotate(' + ((180 + (bladeIndex * (360 / this.blade))) % 360).toFixed(0) + 'deg)',
                    transformOrigin: 'center -' + (this.scale * (0.5 - 0.5 * this.bladeHeightRatio)).toFixed(this.precision) + 'em',
                    width: (this.scale * this.bladeWidth).toFixed(this.precision) + 'em',
                };
            },
        },
        template: `<div :style="wrapperStyle"><div v-for="i in blade" :style="bladeStyle(i - 1)" /></div>`,
    };

    const Loader = {
        components: { S: Spinner },
        emits: [ 'update:modelValue' ],
        props: {
            blockerStyle: Object,
            inert: { default: true },
            loaderDeviate: { default: 0.2 },
            loaderWait: { default: 1.3 },
            modelValue: Boolean,
            spinner: { default: true },
            spinnerScale: { default: 4 },
        },
        setup(props, ctx) {
            const { modelValue } = toRefs(props);
            const root = ref(null);
            const sizes = VueUse.useElementSize(root);

            const height = sizes.height;
            const spinnerVisible = ref(false);
            const width = sizes.width;
            let spinnerTimer = null;

            watch(modelValue, value => {
                root.value.inert = props.inert && value;

                if (spinnerTimer !== null) {
                    clearTimeout(spinnerTimer);
                    spinnerTimer = null;
                }

                if (value) {
                    const timeout = (props.loaderWait > 0 ? props.loaderWait : 0)
                        + (props.loaderDeviate > 0 ? Math.random() * props.loaderDeviate : 0);

                    spinnerTimer = setTimeout(() => {
                        spinnerVisible.value = props.spinner;
                    }, timeout * 1000);
                } else {
                    spinnerVisible.value = false;
                }

                ctx.emit('update:modelValue', value);
            });

            const absoluteCover = computed(() => ({
                height: height.value + 'px',
                left: 0,
                position: 'absolute',
                top: 0,
                width: width.value + 'px',
            }));

            return {
                modelValue,
                root,
                spinnerScale: props.spinnerScale,
                spinnerVisible,

                wrapperStyle: { position: 'relative', zIndex: 1 },

                blockerStyle: computed(() => ({
                    opacity: .3,
                    ...props.blockerStyle,
                    ...absoluteCover.value,
                })),

                spinnerStyle: computed(() => ({
                    alignItems: 'center',
                    display: 'flex',
                    justifyContent: 'center',
                    ...absoluteCover.value,
                })),
            };
        },
        template: `
            <div ref="root">
                <div v-show="modelValue" :style="wrapperStyle">
                    <div class="background-blur" :style="blockerStyle"></div>
                    <transition name="fade">
                        <div v-if="spinnerVisible" :style="spinnerStyle">
                            <S :scale="spinnerScale" />
                        </div>
                    </transition>
                </div>
                <slot />
            </div>
        `,
    };

    const SimpleObjectDisplay = {
        props: {
            as: null,
            emptyValue: { default: EM_DASH },
            emptyValues: { default: DEFAULT_FILLED_CASES },
            trimSpaces: { default: true },
            value: null,
        },
        setup(props) {
            const { value } = toRefs(props);
            const isFilled = computed(() => props.emptyValues.indexOf(isString(value.value) && props.trimSpaces ? value.value.replace(STR_TRIM_SPACE_RE, '') : value.value) === -1);

            return {
                as: props.as,
                emptyValue: props.emptyValue,
                isFilled,
                isList: computed(() => isFilled.value && isArray(value.value) || isObject(value.value)),
                value,
                list: computed(() => {
                    if (isArray(value.value)) {
                        return value.value;
                    }

                    if (isObject(value.value)) {
                        return objectMapToArray(value.value, (v, k) => `${k}: ${v}`);
                    }

                    return [];
                }),
            };
        },
        template: `<span v-if="!isFilled" :style="{userSelect:'none'}">{{ emptyValue }}</span
            ><ul v-bind="$attrs" v-else-if="isList"><li v-for="value in list">{{ value }}</li></ul
            ><component v-bind="$attrs" v-else-if="as" :is="as">{{ value }}</component
            ><template v-else>{{ value }}</template>`,
    };

    const TextEllipsisDisplay = {
        components: { C: SimpleObjectDisplay },
        props: ['value'],
        setup(props) {
            const instance = getCurrentInstance();
            const parentNode = ref(null);
            const textNode = ref(null);
            const { value } = toRefs(props);
            nextTick(() => parentNode.value = instance.ctx.$el.parentNode);

            const { width: parentWidth } = VueUse.useElementSize(parentNode);
            const { height } = VueUse.useElementSize(textNode);

            const spacePosition = computed(() => {
                const length = value.value.length;
                const half = parseInt(length / 2, 10);

                let space = value.value.indexOf(' ', half);
                if (space === -1) {
                    space = value.value.lastIndexOf(' ', half);
                }

                return space === -1 ? half + 1 : space;
            });

            return {
                startText: computed(() => value.value.substring(0, spacePosition.value)),
                endText: computed(() => value.value.substring(spacePosition.value)),

                textNode,
                isFilled: computed(() => isFilled(value.value)),
                style: computed(() => ({ position: 'absolute', width: parentWidth.value + 'px' })),
                value,
                wrapper: computed(() => ({ position: 'relative', width: '100%', height: height.value + 'px' })),
            };
        },
        template: `<C v-if="!isFilled"
            /><div v-else :style="wrapper"><span ref="textNode"
                :data-content-end="endText"
                :data-content-start="startText"
                :title="value"
                :style="style"
                class="text-mid-ellipsis"
            ></span></div>`,
    };

    const ResourceTable = {
        components: { S: SimpleObjectDisplay },
        emits: ['toggleSelection', 'selection', 'selectAll'],
        props: {
            actionLabel: String,
            actionText: { default: EM_DASH },
            baseBorderColor: { default: 'gray' },
            borderHorizontal: Boolean,
            borderVertical: Boolean,
            compact: Boolean,
            data: Array,
            fields: undefined,
            replaceFields: undefined,
            resourceAttributeKey: { default: 'attributes' },
            resourceKey: { default: 'id' },
            withAction: Boolean,
            withNumbering: Boolean,
            withSelection: Boolean,

            tableClass: {},
            tableWrapperClass: {},
            listWrapperClass: {},
            headerRowClass: {},
            baseHeaderActionCellClass: {},
            headerCellClass: {},
            rowWrapperClass: {},
            rowClass: {},
            baseActionCellClass: {},
            cellClass: {},
        },
        setup(props, ctx) {
            const borderWeight = '200';
            // const supportsGrid = ref(typeof domCreate('div').style.grid === 'string');
            const supportsGrid = ref(false);
            // setInterval(() => supportsGrid.value = !supportsGrid.value, 5000);

            const {
                baseBorderColor,
                borderHorizontal,
                borderVertical,
                compact,
                withAction,
                withNumbering,
                withSelection,
            } = toRefs(props);

            const fieldsData = computed(() => {
                const replaceFields = props.replaceFields;
                let fields = props.fields;
                let shouldReplaceFields = false;

                if (isArray(fields)) {
                    shouldReplaceFields = true;

                } else {
                    const newFields = [];
                    objectEach(fields, (field, name) => {
                        if (isString(field)) {
                            newFields.push({ name, label: field });
                        } else if (isObject(field)) {
                            newFields.push({ name, ...field });
                        }
                    });
                    fields = newFields;
                }

                if (replaceFields === false) {
                    shouldReplaceFields = false;
                } else if (replaceFields === null || replaceFields === undefined) {
                    // as is
                } else { // truthy
                    shouldReplaceFields = true;
                }

                const fieldsProp = normalizeFields(fields);
                let fieldsToShow = [];

                if (shouldReplaceFields) {
                    fieldsToShow = fieldsProp;

                } else {
                    const mergedFieldsData = resourceFields().map(field => {
                        const mergeIndex = fieldsProp.findIndex(f => f.name === field.name);
                        return mergeIndex === -1 ? field : objectMerge(field, fieldsProp.splice(mergeIndex, 1)[0]);
                    });

                    fieldsToShow = [...mergedFieldsData, ...fieldsProp];
                }

                return fieldsToShow.filter(field => field.visible);
            });

            const normalizeResource = source => onlyArray(source).map((row, rowIndex) => {
                let rowObject = {};
                let rowData = {};

                if (props.resourceAttributeKey in row) {
                    rowObject = { ...row };
                    rowData = row[props.resourceAttributeKey];
                } else {
                    rowData = row;
                }

                if (!(props.resourceKey in rowObject) && (props.resourceKey in rowData)) {
                    rowObject.id = rowData[props.resourceKey];
                }

                const selected = !!rowObject.selected;

                rowObject.count = 'count' in rowObject ? rowObject.count : rowIndex + 1;
                rowObject.original = { ...rowData };
                rowObject.selected = selected;

                rowObject.attributes = objectMap(rowData, (cell, key, index) => {
                    const cellObject = onlyObject(cell);
                    return {
                        ...cellObject,
                        value: 'value' in cellObject ? cell.value : cell,
                    };
                });

                return rowObject;
            });

            const resourceComputed = computed(() => normalizeResource(props.data));
            const resourceData = ref([]);
            resourceData.value = normalizeResource(props.data);

            watch(resourceComputed, data => resourceData.value = data);
            watch(resourceData, () => ctx.emit('selection', gatherSelections()));

            const gatherSelections = () => {
                const selection = [];
                resourceData.value.forEach(row => {
                    if (row.selected) {
                        selection.push(row);
                    }
                });

                if (resourceData.value.length === selection.size) {
                    ctx.emit('selectAll', selection);
                }

                return selection;
            };

            const defaultFormatter = (value, context) => value;

            const normalizeFields = (fields) => onlyArray(fields).map(field => {
                const fieldObject = onlyObject(field);
                const name = isString(field) ? field : fieldObject.name;
                const label = isString(field)
                        ? strCapitalize(field).replace(STR_PROPER_CASE_RE, ' ')
                        : fieldObject.label;

                return {
                    align: 'left',
                    component: null,
                    formatter: defaultFormatter,
                    visible: true,
                    ...fieldObject,
                    name,
                    label: label || strCapitalize(name).replace(STR_PROPER_CASE_RE, ' '),
                };
            });

            const resourceFields = () => {
                if (resourceData.value && resourceData.value.length) {
                    const rowData = resourceData.value[0].attributes;
                    if (objectLength(rowData) !== 0) {
                        const fields = [];
                        objectEach(rowData, (v, k) => fields.push(k));
                        return normalizeFields(fields);
                    }
                }

                return [];
            };

            return {
                actionLabel: props.actionLabel,
                actionText: props.actionText,
                compact,
                fieldsData,
                resourceData,
                supportsGrid,
                withAction,
                withNumbering,
                withSelection,

                displayComponent(cell) {
                    const componentName = cell.component;
                    if (componentName) {
                        const component = resolveComponent(componentName);
                        if (component !== componentName) {
                            return component;
                        }
                    }

                    return 'template';
                },

                fieldSlotName: name => {
                    name = name + '';

                    if (name !== '') {
                        return name.split(STR_PROPER_CASE_RE).map(strCapitalize).join('');
                    }
                },

                resolveCell: (rowData, fieldCell) => rowData.attributes[fieldCell.name] || {},

                selectRow: (row, index) => {
                    row.selected = !row.selected;
                    ctx.emit('toggleSelection', {row, index});
                    ctx.emit('selection', gatherSelections());
                },

                tableClass: computed(() => mergeClassBinding(
                    'min-w-full',
                    { 'grid': supportsGrid.value },
                    props.tableClass
                )),

                tableStyle: computed(() => (
                    {
                        gridTemplateColumns: [
                            withNumbering.value || withSelection.value ? 'min-content' : '',
                            `repeat(${fieldsData.value.length}, minmax(min-content, auto))`,
                            withAction.value ? 'min-content' : '',
                        ].join(' '),
                    }
                )),

                tableWrapperClass: computed(() => mergeClassBinding(
                    'align-middle',
                    'inline-block',
                    'min-w-full',
                    'overflow-hidden',
                    `border-${baseBorderColor.value}-${borderWeight}`,
                    props.tableWrapperClass
                )),

                listWrapperClass: props.listWrapperClass,

                headerRowClass: computed(() => mergeClassBinding(
                    'text-sm',
                    props.headerRowClass,
                    { 'contents': supportsGrid.value }
                )),

                headerControlCellClass: mergeClassBinding(
                    'w-px',
                    `border-${baseBorderColor.value}-${borderWeight}`,
                    props.headerCellClass,
                    props.baseHeaderActionCellClass
                ),

                headerActionCellClass: mergeClassBinding(
                    'w-px',
                    `border-${baseBorderColor.value}-${borderWeight}`,
                    props.headerCellClass,
                    props.baseHeaderActionCellClass
                ),

                headerCellClass: (field, index) => mergeClassBinding(
                    `text-${field.align}`,
                    `border-${baseBorderColor.value}-${borderWeight}`,
                    {
                        'whitespace-nowrap': !compact.value,
                        'w-px': compact.value ? false : '' + field.label === ''
                    },
                    props.headerCellClass,
                    field.class
                ),

                rowWrapperClass: props.rowWrapperClass,

                rowClass: (row, index) => compact.value ? mergeClassBinding(
                    `border-${baseBorderColor.value}-${borderWeight}`,
                    row.listItemClass
                ) : (
                    { 'contents': supportsGrid.value }
                ),

                controlCellClass: (row, index) => mergeClassBinding(
                    'align-top',
                    `border-${baseBorderColor.value}-${borderWeight}`,
                    props.cellClass,
                    props.baseActionCellClass
                ),

                actionCellClass: (row, index) => mergeClassBinding(
                    'align-top',
                    `border-${baseBorderColor.value}-${borderWeight}`,
                    props.cellClass,
                    props.baseActionCellClass
                ),

                cellClass: (cell, field, index, row, rowIndex) => mergeClassBinding(
                    `text-${field.align}`,
                    'align-top',
                    `border-${baseBorderColor.value}-${borderWeight}`,
                    {
                        'whitespace-nowrap': !compact.value,
                    },
                    props.cellClass,
                    cell.class
                ),
            };
        },
        template: `
            <div class="overflow-x-auto">
                <div v-if="compact" class="listWrapperClass">
                    <ul :class="headerRowClass">
                        <li v-for="(fieldCell, fieldIndex) in fieldsData"
                            :set="(scope = {
                                compact,
                                field: fieldCell,
                                fields: fieldsData,
                                index: fieldIndex,
                                label: fieldCell.label,
                                resource: resourceData,
                            })"
                            :class="headerCellClass(fieldCell, fieldIndex)"
                        >
                            <slot :name="'header-' + fieldIndex" v-bind="scope">
                                <slot :name="'header' + fieldSlotName(fieldCell.name)" v-bind="scope">
                                    <slot name="header" v-bind="scope">
                                        {{ fieldCell.label }}
                                    </slot>
                                </slot>
                            </slot>
                        </li>
                    </ul>

                    <ul :class="rowWrapperClass">
                        <li v-for="(rowData, rowIndex) in resourceData"
                            :set="(scopeAction = {
                                compact,
                                fields: fieldsData,
                                count: rowData.count,
                                resource: resourceData,
                                row: rowData,
                                rowIndex,
                                selected: rowData.selected,
                            }, scopeControl = {
                                selectRow: () => selectRow(rowData, rowIndex),
                                withNumbering,
                                withSelection,
                            })"
                            :key="rowData.id"
                            :class="rowClass(rowData, rowIndex)"
                        >
                            <div class="flex justify-between">
                                <div v-if="withNumbering || withSelection" :class="controlCellClass(rowData, rowIndex)">
                                    <slot :name="'control-' + rowIndex" v-bind="{ ...scopeAction, ...scopeControl }">
                                        <slot name="control" v-bind="{ ...scopeAction, ...scopeControl }">
                                            <label class="flex items-center space-x-1">
                                                <div v-show="withNumbering">{{ rowData.count }}</div>
                                                <input v-show="withSelection" type="checkbox" :checked="rowData.selected" @change="scopeControl.selectRow()">
                                            </label>
                                        </slot>
                                    </slot>
                                </div>
                                <div v-if="withAction" :class="actionCellClass(rowData, rowIndex)">
                                    <slot :name="'action-' + rowIndex" v-bind="scopeAction">
                                        <slot name="action" v-bind="scopeAction">
                                            {{ actionText }}
                                        </slot>
                                    </slot>
                                </div>
                            </div>
                            <div v-for="(fieldCell, fieldIndex) in fieldsData"
                                :set="(cell = resolveCell(rowData, fieldCell), cellScope = {
                                    cell,
                                    field: fieldCell,
                                    fieldIndex,
                                    label: fieldCell.label,
                                    value: cell.value,
                                    formattedValue: () => fieldCell.formatter(cell.value, cellScope),
                                    ...scopeAction,
                                })"
                                :class="cellClass(cell, fieldCell, fieldIndex, rowData, rowIndex)"
                            >
                                <slot :name="'cell-' + rowIndex + '-' + fieldIndex" v-bind="cellScope">
                                    <slot :name="'field-' + fieldIndex" v-bind="cellScope">
                                        <slot :name="'field' + fieldSlotName(fieldCell.name)" v-bind="cellScope">
                                            <slot name="cell" v-bind="cellScope">
                                                {{ fieldCell.label }}:
                                                <component v-if="fieldCell.component" :is="displayComponent(fieldCell)" v-bind="{
                                                    props: cellScope,
                                                    value: cell.value,
                                                }" />
                                                <S v-else :value="cellScope.formattedValue()" />
                                            </slot>
                                        </slot>
                                    </slot>
                                </slot>
                            </div>
                        </li>
                    </ul>
                </div>

                <div v-else :class="tableWrapperClass">
                    <table :class="tableClass" :style="tableStyle">
                        <thead :class="{ contents: supportsGrid }">
                            <tr :class="headerRowClass">
                                <th v-if="withNumbering || withSelection" :class="headerControlCellClass"></th>
                                <th v-for="(fieldCell, fieldIndex) in fieldsData"
                                    :set="(scope = {
                                        compact,
                                        field: fieldCell,
                                        fields: fieldsData,
                                        index: fieldIndex,
                                        label: fieldCell.label,
                                        resource: resourceData,
                                    })"
                                    :class="headerCellClass(fieldCell, fieldIndex)"
                                >
                                    <slot :name="'header-' + fieldIndex" v-bind="scope">
                                        <slot :name="'header' + fieldSlotName(fieldCell.name)" v-bind="scope">
                                            <slot name="header" v-bind="scope">
                                                {{ fieldCell.label }}
                                            </slot>
                                        </slot>
                                    </slot>
                                </th>
                                <th v-if="withAction" :class="headerActionCellClass">{{ actionLabel }}</th>
                            </tr>
                        </thead>
                        <tbody :class="{ ...rowWrapperClass, contents: supportsGrid }">
                            <tr v-for="(rowData, rowIndex) in resourceData"
                                :set="(scopeAction = {
                                    compact,
                                    fields: fieldsData,
                                    count: rowData.count,
                                    resource: resourceData,
                                    row: rowData,
                                    rowIndex,
                                    selected: rowData.selected,
                                }, scopeControl = {
                                    selectRow: () => selectRow(rowData, rowIndex),
                                    withNumbering,
                                    withSelection,
                                })"
                                :key="rowData.id"
                                :class="rowClass(rowData, rowIndex)"
                            >
                                <th v-if="withNumbering || withSelection" :class="controlCellClass(rowData, rowIndex)">
                                    <slot :name="'control-' + rowIndex" v-bind="{ ...scopeAction, ...scopeControl }">
                                        <slot name="control" v-bind="{ ...scopeAction, ...scopeControl }">
                                            <label class="flex items-center space-x-1">
                                                <div v-show="withNumbering">{{ rowData.count }}</div>
                                                <input v-show="withSelection" type="checkbox" :checked="rowData.selected" @change="scopeControl.selectRow()">
                                            </label>
                                        </slot>
                                    </slot>
                                </th>
                                <td v-for="(fieldCell, fieldIndex) in fieldsData"
                                    :set="(cell = resolveCell(rowData, fieldCell), cellScope = {
                                        cell,
                                        field: fieldCell,
                                        fieldIndex,
                                        label: fieldCell.label,
                                        value: cell.value,
                                        formattedValue: () => fieldCell.formatter(cell.value, cellScope),
                                        ...scopeAction,
                                    })"
                                    :class="cellClass(cell, fieldCell, fieldIndex, rowData, rowIndex)"
                                >
                                    <slot :name="'cell-' + rowIndex + '-' + fieldIndex" v-bind="cellScope">
                                        <slot :name="'field-' + fieldIndex" v-bind="cellScope">
                                            <slot :name="'field' + fieldSlotName(fieldCell.name)" v-bind="cellScope">
                                                <slot name="cell" v-bind="cellScope">
                                                    <component v-if="fieldCell.component" :is="displayComponent(fieldCell)" v-bind="{
                                                        props: cellScope,
                                                        value: cell.value,
                                                    }" />
                                                    <S v-else :value="cellScope.formattedValue()" />
                                                </slot>
                                            </slot>
                                        </slot>
                                    </slot>
                                </td>
                                <th v-if="withAction" :class="actionCellClass(rowData, rowIndex)">
                                    <slot :name="'action-' + rowIndex" v-bind="scopeAction">
                                        <slot name="action" v-bind="scopeAction">
                                            {{ actionText }}
                                        </slot>
                                    </slot>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        `,
    };

    const PopperTeleport = {
        components: { OnClickOutside: VueUse.OnClickOutside },
        emits: [ 'update:toggle' ],
        props: {
            overlay: { default: undefined },
            placement: String,
            strategy: String,
            target: { default: undefined },
            toggle: Boolean,
        },
        setup(props, ctx) {
            const overlayTarget = computed(() => overlay.value || overlay());
            const popperDom = ref(null);
            const { toggle } = toRefs(props);
            let focusTrapInstance = null;
            let popperInstance = null;

            const createPopper = (target, popperDom) => Popper.createPopper(
                target.$ && target._ && target.$el ? target.$el : target,
                popperDom.value,
                {
                    strategy: props.strategy || 'fixed',
                    placement: props.placement || 'bottom-start',
                    modifiers: [{
                        name: 'offset',
                        options: { offset: [0, 8] },
                    }, {
                        name: 'flip',
                        options: {
                            fallbackPlacements: [
                                'top-start',
                                'bottom-end',
                                'top-end',
                            ],
                        },
                    }],
                }
            );

            watch(() => props.target, target => {
                if (popperInstance) {
                    popperInstance.destroy();
                }

                if (!target) {
                    return;
                }

                if (popperDom.value) {
                    popperInstance = createPopper(target, popperDom);
                } else {
                    nextTick(() => popperInstance = createPopper(target, popperDom));
                }
            });

            const closeToggle = () => ctx.emit('update:toggle', false);

            watch(toggle, value => {
                ctx.emit('update:toggle', value);

                if (value) {
                    nextTick(() => {
                        if (!focusTrapInstance) {
                            focusTrapInstance = focusTrap.createFocusTrap(popperDom.value);
                        }

                        try {
                            focusTrapInstance.activate();
                        } catch (error) {
                            console.warn({error});
                        }

                        if (popperInstance) {
                            popperInstance.forceUpdate();
                        }
                    });
                } else {
                    if (focusTrapInstance) {
                        focusTrapInstance.deactivate();
                        focusTrapInstance = null;
                    }
                }
            });

            return {
                closeToggle,
                overlayTarget,
                popperDom,
                slotBinding: { toggle, closeToggle },
                toggle,
            };
        },
        template: `
            <teleport :to="overlayTarget">
                <div v-show="toggle">
                    <slot name="background" v-bind="slotBinding" />
                    <OnClickOutside @trigger="closeToggle()">
                        <div
                            @keydown.esc="closeToggle()"
                            ref="popperDom"
                            v-show="toggle"
                        >
                            <slot v-bind="slotBinding" />
                        </div>
                    </OnClickOutside>
                </div>
            </teleport>
        `,
    };

    const SelectDropdown = {
        components: {
            VirtualScroller,
            OnClickOutside: VueUse.OnClickOutside,
        },
        emits: [ 'model', 'update:search', 'update:toggle', 'update:value' ],
        props: {
            fuseKeys: { default: ['model.value', 'model.label'] },
            labelKey: { default: 'label' },
            missingModel: { default: { value: undefined, label: '' } },
            options: Array,
            optionsCallback: Function,
            popperTarget: { default: undefined },
            search: { default: '' },
            searchDebounce: { default: 100 },
            selectedKey: { default: 'selected' },
            toggle: Boolean,
            value: { default: undefined },
            valueNormalizer: Function,
            valueKey: { default: 'value' },
        },
        setup(props, ctx) {
            const fuseInstance = new Fuse([], { keys: props.fuseKeys });
            const fuseSearchDom = ref(null);
            const internalOptions = ref([]);
            const lastSelected = ref(null);
            const optionsIsLoading = ref(true);
            const popperDom = ref(null);
            const searchInput = ref(props.search);
            const shouldScrollToSelected = ref(true);
            const valueNormalizer = toRef(props, 'valueNormalizer');
            const virtualScrollerDomInstance = ref(null);
            const { toggle, valueKey } = toRefs(props);
            let focusTrapInstance = null;
            let popperInstance = null;

            watch(() => props.popperTarget, target => {
                if (popperInstance) {
                    popperInstance.destroy();
                }

                popperInstance = Popper.createPopper(target, popperDom.value, {
                    strategy: 'fixed',
                    placement: 'bottom-start',
                    modifiers: [{
                        name: 'offset',
                        options: { offset: [0, 8] },
                    }, {
                        name: 'flip',
                        options: {
                            fallbackPlacements: ['top-start'],
                        },
                    }],
                });
            });

            const selectOptions = computed(() => {
                if (searchInput.value.length > 0) {
                    return fuseInstance.search(searchInput.value).map(item => {
                        item.item.refIndex = item.refIndex;
                        return item.item;
                    });
                }

                return internalOptions.value.map((item, index) => {
                    item.refIndex = index;
                    return item;
                });
            });

            const closeToggle = () => ctx.emit('update:toggle', false);

            watch(toggle, value => {
                if (value) {
                    nextTick(() => {
                        fuseSearchDom.value.focus();
                        if (!focusTrapInstance) {
                            focusTrapInstance = focusTrap.createFocusTrap(popperDom.value);
                        }

                        try {
                            focusTrapInstance.activate();
                        } catch (error) {
                            console.warn({error});
                        }

                        if (popperInstance) {
                            popperInstance.forceUpdate();
                        }
                    });
                } else {
                    if (focusTrapInstance) {
                        focusTrapInstance.deactivate();
                        focusTrapInstance = null;
                    }
                }
            });

            const applyOptionValue = value => valueNormalizer.value ? valueNormalizer.value(value) : value;

            const applyOptions = data => {
                let options = [];

                if (data && data.length && data.length > 0) {
                    let hasSelected = false;
                    options = data.map((item, index) => {
                        const selected = !hasSelected && item[props.selectedKey];

                        if (selected) {
                            hasSelected = true;
                        }

                        item[valueKey.value] = applyOptionValue(item[valueKey.value]);

                        return {
                            model: item,
                            selected,
                            hovered: false,
                        };
                    });
                }

                optionsIsLoading.value = false;
                fuseInstance.setCollection(internalOptions.value = options);
            };

            const refreshOptions = (force) => {
                optionsIsLoading.value = true;
                const callback = props.optionsCallback;
                if (callback) {
                    callback(force).then(applyOptions);
                } else {
                    applyOptions(props.options || []);
                }
            };

            const virtualScrollerVisible = () => {
                if (shouldScrollToSelected.value) {
                    shouldScrollToSelected.value = false;
                    nextTick(() => raf(scrollToSelectedItem));
                }
            };

            const scrollToSelectedItem = () => {
                const scroller = virtualScrollerDomInstance.value.scroller();
                if (scroller && lastSelected.value && lastSelected.value.virtualIndex) {
                    scroller.scrollToItem(lastSelected.value.virtualIndex);
                }
            };

            const clearSelection = () => {
                lastSelected.value = null;
                selectOptionFromSelection(null);
            };

            const searchEscape = event => {
                event.target.value !== '' && event.stopPropagation();
            };

            const listDoSearch = _.debounce(
                event => ctx.emit('update:search', searchInput.value = event.target.value),
                props.searchDebounce
            );

            const optionLabel = optionProps => optionProps.data.model[props.labelKey];

            const optionHover = (props, hover) => {
                if (hover) {
                    props.data.hovered = true;
                    unhoverOption(props.index);

                } else {
                    props.data.hovered = false;
                }
            };

            const unhoverOption = _.debounce(
                hovered => internalOptions.value.forEach((item, index) => {
                    if (index !== hovered && item.hovered) {
                        item.hovered = false;
                    }
                }),
                100
            );

            const optionSelected = optionProps => {
                selectOptionFromValue(optionProps.data.model[valueKey.value]);
                shouldScrollToSelected.value = false;
                closeToggle();
            };

            const selectOptionFromValue = value => selectOptionFromValueDelegate(applyOptionValue(value));

            let selectValue = null;
            const selectOptionFromValueDelegate = value => {
                let selection = null;
                selectValue = value;

                internalOptions.value.forEach((item, index) => {
                    if (item.selected) {
                        item.selected = false;
                    }

                    if (selection === null && value === item.model[valueKey.value]) {
                        item.selected = true;
                        lastSelected.value = item;
                        selection = item.model;
                    }
                });

                if (lastSelected.value) {
                    lastSelected.value.virtualIndex = selectOptions.value
                        .findIndex(item => item.refIndex === lastSelected.value.refIndex);
                    shouldScrollToSelected.value = true;
                }

                selectOptionFromSelection(selection);
            };

            const selectOptionFromSelection = selection => {
                if (! selection) {
                    selection = props.missingModel;
                }

                ctx.emit('model', selection);
                ctx.emit('update:value', !selection ? selection : selection[valueKey.value]);
            };

            watch(() => props.value, selectOptionFromValue);

            watch(internalOptions, () => selectOptionFromValue(selectValue));

            onBeforeMount(() => refreshOptions(false));

            return {
                clearSelection,
                closeToggle,
                fuseSearchDom,
                internalOptions,
                lastSelected,
                listDoSearch,
                optionHover,
                optionLabel,
                optionSelected,
                optionsIsLoading,
                overlay: overlay(),
                popperDom,
                refreshOptions,
                scrollToSelectedItem,
                searchEscape,
                searchInput,
                selectOptions,
                toggle,
                virtualScrollerDomInstance,
                virtualScrollerVisible,

                styles: computed(() => ({
                    wrapper: () => ({
                        background: VueUse.usePreferredDark().value ? 'black' : 'white',
                        borderRadius: '1rem',
                        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, .12), 0 2px 4px 0 rgba(0, 0, 0, .08)',
                        minWidth: '32rem',
                    }),
                    actionContainer: {
                        display: 'flex',
                        padding: '.5rem',
                    },
                    actionButton: {
                        alignItems: 'center',
                        display: 'flex',
                        justifyContent: 'center',
                        marginLeft: '.5rem',
                    },
                    listEmptyContainer: {
                        alignItems: 'center',
                        display: 'flex',
                        justifyContent: 'center',
                        padding: '2rem',
                    },
                    virtualScroller: {
                        maxHeight: '40vh',
                        minHeight: '5rem',
                    },
                    selectOption: props => ({
                        background: props.data.hovered ? (VueUse.usePreferredDark().value ? 'blue' : 'lightblue') : undefined,
                        cursor: 'pointer',
                        display: 'flex',
                        fontWeight: props.data.selected ? 'bold' : 'normal',
                        padding: '.5rem 1rem',
                    }),
                    optionCheckAddon: {
                        flex: 'none',
                        width: '2rem',
                        userSelect: 'none',
                    },
                    defaultOption: {
                        userSelect: 'none',
                    },
                })),
            };
        },
        template: `
            <teleport :to="overlay">
                <OnClickOutside @trigger="closeToggle()">
                    <div
                        @keydown.esc="closeToggle()"
                        ref="popperDom"
                        v-show="toggle"
                    >
                        <div :style="styles.wrapper()">
                            <div :style="styles.actionContainer">
                                <input
                                    :readonly="internalOptions.length === 0"
                                    @input="listDoSearch($event)"
                                    @keydown.esc="searchEscape($event)"
                                    class="form-control"
                                    ref="fuseSearchDom"
                                >
                                <button v-if="!optionsIsLoading"
                                    :style="styles.actionButton"
                                    @click="refreshOptions(true)"
                                    class="btn btn-info"
                                    type="button"
                                ><svg :style="{ width: '1.5rem' }" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M4 2a1 1 0 011 1v2.101a7.002 7.002 0 0111.601 2.566 1 1 0 11-1.885.666A5.002 5.002 0 005.999 7H9a1 1 0 010 2H4a1 1 0 01-1-1V3a1 1 0 011-1zm.008 9.057a1 1 0 011.276.61A5.002 5.002 0 0014.001 13H11a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0v-2.101a7.002 7.002 0 01-11.601-2.566 1 1 0 01.61-1.276z" clip-rule="evenodd" />
                                </svg></button>
                                <button v-if="lastSelected && lastSelected.refIndex"
                                    :style="styles.actionButton"
                                    @click="scrollToSelectedItem()"
                                    class="btn btn-info"
                                    type="button"
                                ><svg :style="{ width: '1.5rem' }" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                    <path d="M10 12a2 2 0 100-4 2 2 0 000 4z" />
                                    <path fill-rule="evenodd" d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z" clip-rule="evenodd" />
                                </svg></button>
                                <button v-if="lastSelected"
                                    :style="styles.actionButton"
                                    @click="clearSelection()"
                                    class="btn btn-danger"
                                    type="button"
                                ><svg :style="{ width: '1.5rem' }" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg></button>
                            </div>
                            <div v-if="optionsIsLoading" :style="styles.listEmptyContainer">loading...</div>
                            <div v-else-if="internalOptions.length === 0" :style="styles.listEmptyContainer">empty</div>
                            <div v-else-if="selectOptions.length === 0" :style="styles.listEmptyContainer">no result</div>
                            <VirtualScroller v-else
                                #="props"
                                :list="selectOptions"
                                :style="styles.virtualScroller"
                                @visible="virtualScrollerVisible()"
                                keyField="refIndex"
                                ref="virtualScrollerDomInstance"
                            >
                                <div
                                    :style="styles.selectOption(props)"
                                    @click="optionSelected(props)"
                                    @keydown.enter.prevent="optionSelected(props)"
                                    @mouseout="optionHover(props, false)"
                                    @mouseover="optionHover(props, true)"
                                    tabindex=0
                                >
                                    <div :style="styles.optionCheckAddon">{{
                                        props.data.selected ? '✓' : ''
                                    }}</div>
                                    <div><slot v-bind="props"><div :style="styles.defaultOption">{{ optionLabel(props) }}</div></slot></div>
                                </div>
                            </VirtualScroller>
                        </div>
                    </div>
                </OnClickOutside>
            </teleport>
        `,
    };

    const AutoNumericInput = {
        emits: [ 'update:modelValue' ],
        props: {
            config: { default: Object },
            modelValue: { default: '' },
        },
        setup(props, ctx) {
            const hidden = ref(null);
            const input = ref(null);
            let anHidden = null;
            let autonumeric = null;

            const setValueInput = value => {
                try {
                    if (anHidden && autonumeric) {
                        anHidden.set(value);
                        ctx.emit('update:modelValue', autonumeric.getNumericString());
                    }
                } catch (e) {
                    nextTick(() => {
                        setValueWatch(anHidden.getFormatted());
                    });
                }
            };

            const setValueWatch = value => {
                try {
                    if (autonumeric) {
                        autonumeric.set(value);
                        ctx.emit('update:modelValue', autonumeric.getNumericString());
                    }
                } catch (e) {
                }
            };

            onMounted(() => {
                anHidden = new AutoNumeric(
                    hidden.value,
                    { ...props.config, overrideMinMaxLimits: null }
                );

                autonumeric = new AutoNumeric(input.value, props.config);

                input.value.addEventListener('input', event => {
                    setValueInput(event.target.value);
                });
            });

            watch(() => props.modelValue, setValueWatch);

            return {
                hidden,
                input,

                hiddenStyle: {
                    position: 'absolute',
                    visibility: 'hidden',
                    left: '-1px',
                    top: '-1px',
                },
            };
        },
        template: `<input v-bind="$attrs" ref="input"><input tabindex="-1" readonly ref="hidden" :style="hiddenStyle">`,
    };

    const AutosizeTextareaInput = {
        emits: [ 'update:modelValue' ],
        props: {
            autosize: { default: true },
            modelValue: { default: '' },
        },
        setup(props, ctx) {
            const hasAutosize = 'autosize' in window;
            const model = ref(props.modelValue);
            const textarea = ref(null);
            const { autosize: azStatus } = toRefs(props);

            watch(() => props.modelValue, value => model.value = value);

            watch(model, value => {
                ctx.emit('update:modelValue', value);
                if (hasAutosize && azStatus.value) {
                    nextTick(() => autosize.update(textarea.value));
                }
            });

            if (hasAutosize && azStatus.value) {
                onMounted(() => autosize(textarea.value));
            }

            watch(azStatus, set => {
                if (hasAutosize) {
                    if (set) {
                        autosize(textarea.value);
                        nextTick(() => autosize.update(textarea.value));
                    } else {
                        autosize.destroy(textarea.value);
                    }
                }
            });

            return { model, textarea };
        },
        template: `<textarea ref="textarea" v-model="model" />`,
    };

    const IMaskInput = {
        emits: [ 'value' ],
        props: {
            imask: { default: {} },
            mask: { default: '' },
            value: { default: '' },
        },
        setup(props, ctx) {
            const input = ref(null);
            let imaskInstance = null;

            const update = () => {
                ctx.emit('value', imaskInstance ? {
                    masked: imaskInstance.value,
                    unmasked: imaskInstance.unmaskedValue,
                } : {});
            };

            onMounted(() => {
                if (!('IMask' in window)) {
                    return;
                }

                imaskInstance = IMask(input.value, {
                    mask: props.mask,
                    ...props.imask,
                });
                imaskInstance.value = props.value;
                imaskInstance.on('accept', update);
                imaskInstance.on('complete', update);
            });

            watch(() => props.value, imaskInstance.value = value);

            return { input };
        },
        template: `<input ref="input" type="text">`,
    };

    const FlatPickrInput = (() => {
        const DATE_PREFORMAT_RE = /-/g;
        const DATE_TIME_MATCH_RE = /, H:i:S$/;
        const datePreFormat = value => value ? (value.replace ? new Date(value.replace(DATE_PREFORMAT_RE, '/')) : value) : undefined;

        return {
            emits: [ 'update:modelValue' ],
            props: {
                enableTime: { default: true, },
                firstDayOfWeek: { default: 1 },
                flatpickr: { default: {}, },
                format: { default: 'l, j F Y, H:i:S' },
                locale: { default: window.flatpickr.l10ns.id },
                modelValue: { default: '', },
            },
            setup(props, ctx) {
                const input = ref(null);
                const model = ref(props.modelValue);
                const { enableTime } = toRefs(props);
                let flatpickrInstance = null;

                onMounted(() => flatpickrInstance = flatpickr(input.value, {
                    ...config.value,
                    defaultDate: datePreFormat(model.value),
                }));

                const updateValue = value => ctx.emit('update:modelValue', value);

                const config = computed(() => ({
                    dateFormat: props.format.replace(enableTime.value ? '' : DATE_TIME_MATCH_RE, ''),
                    enableSeconds: true,
                    enableTime: enableTime.value,
                    locale: objectMerge({}, props.locale, { firstDayOfWeek: props.firstDayOfWeek }),

                    onClose(selectedDates, dateStr, instance) {
                        const date = selectedDates[0];

                        if (!date) {
                            updateValue(null);
                            return;
                        }

                        const year = date.getFullYear();

                        const part = objectMap({
                            month: date.getMonth() + 1,
                            day: date.getDate(),
                            hour: date.getHours(),
                            minute: date.getMinutes(),
                            second: date.getSeconds(),
                        }, part => (part < 10 ? '0' : '') + part);

                        updateValue([
                            [year, part.month, part.day].join('-'),
                            enableTime.value ? [part.hour, part.minute, part.second].join(':') : '',
                        ].join(' ').trim());
                    },

                    ...props.flatpickr,
                }));

                return { input, model };
            },
            template: `<input ref="input" type="text">`,
        };
    })();

    const DateTimeDisplay = {
        components: { SimpleObjectDisplay },
        props: {
            as: { default: 'time' },
            displayMode: { default: 'formatted' }, // datetime, duration
            displayTime: { default: true, },
            duration: Boolean,
            emptyValue: { default: EN_DASH },
            format: { default: 'dddd, D MMMM YYYY, HH:mm:ss' },
            locale: { default: 'id' },
            parseFormat: String,
            value: null,
        },
        setup(props) {
            const {
                as,
                displayMode,
                displayTime,
                duration,
                emptyValue,
                format,
                locale,
                parseFormat,
                value,
            } = toRefs(props);

            const hasDuration = !!dayjs.duration;
            const mode = ref(hasDuration && duration.value ? 'duration' : displayMode.value);

            const normalizedFormat = computed(() => format.value === 'dddd, D MMMM YYYY, HH:mm:ss'
                ? (displayTime.value ? format.value : 'dddd, D MMMM YYYY')
                : format.value
            );

            const dayJsInstance = computed(() => {
                try {
                    if (value.value === undefined || value.value === null) {
                        return null;
                    }

                    return parseFormat.value ? dayjs(value.value, parseFormat.value) : dayjs(value.value);
                } catch (error) {
                    console.warn({value: value.value, parseFormat: parseFormat.value, error});
                    return null;
                }
            });

            const dateTimeUnformatted = computed(() => dayJsInstance.value
                ? dayJsInstance.value.format(displayTime.value ? 'YYYY-MM-DD HH:mm:ss' : 'YYYY-MM-DD')
                : null
            );

            const dateTimeFormatted = computed(() => dayJsInstance.value
                ? dayJsInstance.value.locale(locale.value).format(normalizedFormat.value)
                : null
            );

            const dateTimeDurationFormat = computed(() => dayJsInstance.value && hasDuration
                ? dayjs.duration(dayJsInstance.value.diff(dayjs())).locale(locale.value).humanize(true)
                : null
            );

            return {
                as,
                dateTimeUnformatted,
                dayJsInstance,

                title: computed(() => dayJsInstance.value
                    ? (hasDuration
                        ? [dateTimeUnformatted.value || emptyValue.value, dateTimeFormatted.value || emptyValue.value, dateTimeDurationFormat.value || emptyValue.value].join('; ')
                        : [dateTimeUnformatted.value || emptyValue.value, dateTimeFormatted.value || emptyValue.value].join('; '))
                    : (value.value === undefined || value.value === null
                        ? emptyValue.value
                        : `Invalid value: ${value.value}`
                    )),

                display: computed(() => {
                    if (!dayJsInstance.value) {
                        return null;
                    }

                    switch (mode.value) {
                        case 'formatted': return dateTimeFormatted.value;
                        case 'duration': return dateTimeDurationFormat.value;
                        case 'datetime':
                        default: return dateTimeUnformatted.value;
                    }
                }),

                toggleModes: () => {
                    switch (mode.value) {
                        case 'formatted': return mode.value = hasDuration ? 'duration' : 'datetime';
                        case 'duration': return mode.value = 'datetime';
                        case 'datetime':
                        default: return mode.value = 'formatted';
                    }
                },
            };
        },
        template: `<component :is="as" :datetime="dateTimeUnformatted" :title="title"
            ><SimpleObjectDisplay :value="display"
            /><span v-if="dayJsInstance" @click="toggleModes" class="text-gray-600 cursor-default select-none"
                ><slot name="toggle"> ⇄</slot></span></component>`,
    };

    const DateDisplay = {
        components: { C: DateTimeDisplay },
        template: '<C v-bind="$attrs" :displayTime="false" />',
    };

    const CurrencyDisplay = {
        components: { SimpleObjectDisplay },
        props: {
            as: { default: 'span' },
            decimal: { default: 2 },
            decimalCharacter: { default: '.', },
            digitGroupSeparator: { default: ',', },
            formatted: Boolean,
            prefix: String,
            suffix: String,
            toggle: { default: true },
            value: null,
        },
        setup(props) {
            const { value } = toRefs(props);

            const defaultConfig = () => {
                const config = {
                    currencySymbol: props.prefix || '',
                    suffixText: props.suffix || '',
                    decimalPlaces: props.decimal,
                    // maximumValue: '9999999999999.99',
                    // minimumValue: '-9999999999999.99',
                };

                if (props.digitGroupSeparator !== props.decimalCharacter) {
                    config.digitGroupSeparator = props.digitGroupSeparator;
                    config.decimalCharacter = props.decimalCharacter;
                }

                return config;
            };

            const showFormatted = ref(true);

            const numberFormatted = computed(() => isNaN(value.value)
                ? value.value
                : AutoNumeric.format(value.value, defaultConfig())
            );

            return {
                as: props.as,
                showToggle: computed(() => props.toggle),
                title: computed(() => `${numberFormatted.value} (${props.value})`),
                toggle: () => showFormatted.value = !showFormatted.value,
                value,

                display: computed(() => props.formatted
                    ? numberFormatted.value
                    : (showFormatted.value ? numberFormatted.value : props.value)
                ),
            };
        },
        template: `<component :is="as" :title="title"
            ><SimpleObjectDisplay :value="display"
            /><span v-if="showToggle" @click="toggle" class="text-gray-600 cursor-default select-none"
                ><slot name="toggle"> ⇄</slot></span></component>`,
    };

    const ScopedStyle = (() => {
        const scopedStylePrefix = randomString();
        const scopedStyleCache = {};
        return {
            props: {
                as: { default: 'div' },
                css: { default: '' },
                styleIf: { default: true },
            },

            setup(props, ctx) {
                const css = computed(() => {
                    if (props.css) {
                        return props.css;
                    }

                    const el = domCreate('div');
                    Vue.createApp({ setup: () => () => ctx.slots.style() }).mount(el);
                    return el.innerText;
                });

                const scopedClass = computed(() => scopedStylePrefix + simpleHash(css.value));

                const styleTracker = computed(() => {
                    if (!(scopedClass.value in scopedStyleCache)) {
                        scopedStyleCache[scopedClass.value] = {
                            count: 0,
                            element: domCreate('style', {
                                innerHTML: cssAddScope('.' + scopedClass.value, css.value),
                            }),
                        };
                    }

                    return scopedStyleCache[scopedClass.value];
                });

                const toggleStyle = shouldStyle => {
                    if (shouldStyle) {
                        if (styleTracker.value.count++ === 0) {
                            domAppendChild(domHead(), styleTracker.value.element);
                        }
                    } else {
                        if (styleTracker.value.count !== 0 && --styleTracker.value.count === 0) {
                            domDetach(styleTracker.value.element);
                        }
                    }
                };

                watch(() => props.styleIf, toggleStyle);
                toggleStyle(props.styleIf);
                onUnmounted(() => toggleStyle(false));

                return { as: props.as, scopedClass };
            },

            template: `<component :is="as" :class="scopedClass"><div v-if="false"><slot name="style" /></div><slot /></component>`,
        };
    })();

    const StyledResourceTable = {
        components: { C: ResourceTable },
        props: ['cellClass', 'headerCellClass'],
        methods: { mergeClassBinding },
        template: `<C v-bind="$attrs"
            :cellClass="mergeClassBinding('py-2', 'px-4', 'align-middle', 'border-t', 'leading-normal', cellClass)"
            :headerCellClass="mergeClassBinding('py-1', 'px-4', headerCellClass)"
            class="bg-white"
            ><template v-for="(_, name) in $slots" v-slot:[name]="data"
                ><slot :name="name" v-bind="data" /></template></C>`,
    };

    const baseComponents = {
        VirtualScroller,
        Spinner,
        Loader,
        SimpleObjectDisplay,
        TextEllipsisDisplay,
        ResourceTable,
        PopperTeleport,
        SelectDropdown,
        AutoNumericInput,
        AutosizeTextareaInput,
        IMaskInput,
        FlatPickrInput,
        DateTimeDisplay,
        DateDisplay,
        CurrencyDisplay,
        ScopedStyle,
    };

    const styledComponents = {
        StyledResourceTable,
    };

    ready(overlay);

    return {
        getType,
        isArray,
        isFilled,
        isFunction,
        isNumeric,
        isObject,
        isString,

        onlyArray,
        onlyObject,
        strCapitalize,
        toArray,

        domBody,
        domFirstTag,
        domHead,
        domHtml,
        domCreate,
        domDetach,

        instantLink,
        simpleHash,
        randomString,
        iCookie,
        promiseSerial,

        arrayWrap,
        arrayFlattenObject,
        objectEvery,
        objectEach,
        objectMap,
        objectMapToArray,
        objectValues,
        objectLength,
        dataGet,
        dataHas,
        dataSet,

        off,
        on,
        autoEvent,
        once,
        preventLeaving,
        ready,

        cssStringFromObject,
        cssStringToObject,
        cssAddScope,
        resolveValues,
        mergeClassBinding,
        objectMerge,
        jsonSnapshot,
        jsonParse,
        overlay,
        xhr,

        setActionHandler(handler) { activeActionHandler = (handler || defaultActionHandler); },
    };
})(window, document);
