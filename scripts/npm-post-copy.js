
import uglifyJs from 'uglify-js';
import { fs, beautifyFile, copyResource, getCleanCss, uglifyMinify, minifyHtmlSafeish } from './npm-copy-base.js';
import { createGenerator } from '@unocss/core';
import { presetUno } from '@unocss/preset-uno';
import { presetAttributify } from '@unocss/preset-attributify';

const assetFolder = 'src/Avon/res/';

(async () => {
    const makeUnocss = () => createGenerator({}, {
        presets: [
            presetAttributify(),
            presetUno(),
        ],
    });

    const files = await Promise.all([
        fs.promises.readFile(assetFolder + 'view/login.blade.php', 'utf8'),
        fs.promises.readFile(assetFolder + 'view/resources.blade.php', 'utf8'),
        fs.promises.readFile(assetFolder + 'asset/avon/avon.js', 'utf8'),
        fs.promises.readFile(assetFolder + 'asset/avon/avon.css', 'utf8'),
    ]);
    
    const unocss = makeUnocss();
    const unocssResult = await unocss.generate(`${files[0]} ${files[1]} ${files[2]}`, {
        preflights: false,
    });

    const baseCss =  `${files[3]}\n\n${unocssResult.css}`;
    fs.writeFileSync(`${assetFolder}asset/avon/base.css`, baseCss);
    fs.writeFileSync(`${assetFolder}asset/avon/base.min.css`, getCleanCss().minify(baseCss).styles);

    const baseJs = `(()=>__avon._cacheStyle=${JSON.stringify([...unocssResult.matched])})();\n\n${files[2]}`;
    fs.writeFileSync(`${assetFolder}asset/avon/base.js`, baseJs);
    fs.writeFileSync(`${assetFolder}asset/avon/base.min.js`, uglifyMinify(baseJs).code);
})();

const downloadTasks = [
    ['asset/primevue', 'https://unpkg.com/primevue/umd/primevue.min.js'],
    ['asset/primevue/themes', 'https://unpkg.com/@primevue/themes/umd/aura.min.js'],
].map(async ([path, url]) => {
    let i = 5;
    while (i-- > 0) {
        try {
            const fileName = url.replace(/^.*(\/[^/]+)$/, '$1');

            await fetch(url)
                .then(x => x.text())
                .then(x => fs.writeFileSync(`${assetFolder}${path}${fileName}`, x))
                .then(() => console.log(`+ ${url} downloaded`));

            break;
        }
        catch (e) {
            if (i === 0) {
                console.log(`- ${url} error`)
            }
        }
    }
});

[
    'asset/avon/block.css',
    'asset/avon/block.js',
    'asset/avon/main.css',
    'asset/avon/main.js',
    'asset/avon/init.js',
    'asset/primevue/resources/themes/theme.css',
    'asset/primevue/resources/themes/theme-layer.css',
].forEach(path => copyResource(`${assetFolder}${path}`, `${assetFolder}${path}`));

[
    'asset/chartist/plugins/chartist-plugin-tooltip-mod.js',
].forEach(path => copyResource(`${assetFolder}${path}`, `${assetFolder}${path}`));

await Promise.all(downloadTasks);

[
    'asset/dayjs/locale/id.js',
    'asset/jsencrypt/jsencrypt.min.js',
    'asset/unocss/runtime/core.global.js',
    'asset/unocss/runtime/preset-attributify.global.js',
    'asset/unocss/runtime/preset-icons.global.js',
    'asset/unocss/runtime/preset-uno.global.js',
    'asset/primevue/primevue.min.js',
    'asset/primevue/themes/aura.min.js',
].forEach(path => beautifyFile(`${assetFolder}${path}`, `${assetFolder}${path.replace(/(\.min)?\.js$/, '.expanded.js')}`));

fs.readFile(`${assetFolder}asset/avon/avon.js`, {encoding: 'utf-8'}, (readErr, file) => {
    const modifyAstTemplateNodeAsString = (callback, node) => new uglifyJs.AST_Template({
        strings: callback(node.value.strings.join('___AST_TEMPLATE_STRING_JOIN___'))
            .split('___AST_TEMPLATE_STRING_JOIN___'),
        expressions: node.value.expressions,
        tag: node.value.tag,
    });

    const ast = uglifyJs.parse(file);
    ast.walk(new uglifyJs.TreeWalker(node => {
        if (node instanceof uglifyJs.AST_ObjectProperty && node.value instanceof uglifyJs.AST_Template) {
            if (node.key === 'template') {
                if (node.value.strings.length === 1) {
                    node.value = modifyAstTemplateNodeAsString(x => minifyHtmlSafeish(x, {
                        customAttrCollapse: /^(ss-\w+|uno-layer-\w+|:?button-class|:class|:style|v-bind|v-if)$/,
                    }), node);
                }
            }
            else if (node.key === 'cssTemplate') {
                node.value = modifyAstTemplateNodeAsString(x => getCleanCss().minify(x).styles, node);
            }
        }
    }));

    // fs.writeFile(`${assetFolder}asset/avon/avon.min.js`, uglifyMinify(ast.print_to_string()).code);
    // fs.writeFile(`${assetFolder}asset/avon/avon.min.js`, uglifyMinify(file).code);
});
