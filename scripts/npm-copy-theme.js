const { moduleFolder, copyResource } = require('./npm-copy-base.js');

[
    ['theme.css', '../src/Avon/res/asset/primevue/resources/themes/', 'primevue/resources/themes/'],
].forEach(row => (
    (files, from, to, fn) => fn !== false && (files.constructor === Array ? files : [files])
        .forEach(file => (fn||copyResource)(`${moduleFolder}${from}${file}`, `src/Avon/res/asset/${to||from}${file}`))
)(...row));
