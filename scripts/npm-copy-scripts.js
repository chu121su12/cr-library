import { moduleFolder, copyOnly, copyResource } from './npm-copy-base.js';

const publicFolder = 'public/asset/';

// avon
[
    [[
        'inter.css',
        'inter-variable.css',
        'web',
        'variable',
    ], 'inter-ui/', 'inter-ui/'],

    ['autoNumeric.min.js', 'autonumeric/dist/', 'autonumeric/'],
    ['autosize.js', 'autosize/dist/', 'autosize/'],
    ['axios.min.js', 'axios/dist/', 'axios/'],
    ['big.js', 'big.js/'],

    [[
        'billboard.pkgd.min.js',
        'theme/datalab.css',
        'theme/graph.css',
        'theme/insight.css',
    ], 'billboard.js/dist/', 'billboard.js/'],

    [[
        'index.css',
        'index.umd.js',
    ], 'chartist/dist/', 'chartist/'],
    [[
        'chartist-plugin-tooltip.css',
        'chartist-plugin-tooltip.js',
    ], 'chartist-plugin-tooltips/dist/', 'chartist/plugins/'],

    [[
        'codemirror.js',
        'codemirror.css',
    ], 'codemirror/lib/', 'codemirror/'],

    ['crypto-js.js', 'crypto-js/'],
    ['jsencrypt.min.js', 'jsencrypt/bin/', 'jsencrypt/'],

    [[
        'dayjs.min.js',
        'locale/id.js', // minified
        'plugin/customParseFormat.js',
        'plugin/duration.js',
        'plugin/relativeTime.js',
        'plugin/timezone.js',
        'plugin/utc.js',
    ], 'dayjs/'],

    [[
        'flatpickr.css',
        'flatpickr.js',
        'l10n/id.js',
    ], 'flatpickr/dist/', 'flatpickr/'],

    ['index.umd.min.js', 'tabbable/dist/', 'tabbable/'],
    ['focus-trap.umd.js', 'focus-trap/dist/', 'focus-trap/'],

    ['fuse.js', 'fuse.js/dist/', 'fuse.js/'],
    ['inert.js', 'wicg-inert/dist/', 'inert/'],
    ['jszip.js', 'jszip/dist/', 'jszip/'],

    [[
        'leaflet.js',
        'leaflet.css',
        'images',
    ], 'leaflet/dist/', 'leaflet/'],
    [[
        'L.Control.Locate.min.js',
        'L.Control.Locate.min.css',
    ], 'leaflet.locatecontrol/dist/', 'leaflet.locatecontrol/'],

    [[
        'localforage.js',
        'localforage.nopromises.js',
    ], 'localforage/dist/', 'localforage/'],

    ['marked.min.js', 'marked/'],
    ['mitt.umd.js', 'mitt/dist/', 'mitt/'],

    ['popper.js', '@popperjs/core/dist/umd/', 'popperjs/'],

    [[
        'runtime/core.global.js',
        'runtime/preset-attributify.global.js',
        'runtime/preset-icons.global.js',
        'runtime/preset-uno.global.js',
        ], '@unocss/', 'unocss/'],

    [[
        'tagify.js',
        'tagify.polyfills.min.js',
        'tagify.css',
    ], '@yaireo/tagify/dist/', 'tagify/'],

    ['thumbmark.umd.js', '@thumbmarkjs/thumbmarkjs/dist/', 'thumbmarkjs/'],
    ['fingerprint2.js', 'moe-fingerprint/', 'fingerprint/'],

    [[
        'vue.global.js',
        'vue.global.prod.js',
    ], 'vue/dist/', 'vue3/', copyOnly],

    [[
        'core/index.iife.js',
        'shared/index.iife.js',
    ], '@vueuse/', 'vueuse-'],

    ['vue3-observe-visibility2.min.js', 'vue3-observe-visibility2/dist/', 'vue-virtual-scroller/'],
    [[
        'vue3-resize.css',
        'vue3-resize.min.js',
    ], 'vue3-resize/dist/', 'vue-virtual-scroller/'],
    [[
        'vue3-virtual-scroller.css',
        'vue3-virtual-scroller.min.js',
    ], 'vue3-virtual-scroller/dist/', 'vue-virtual-scroller/'],

    // etc

    ['xlsx.full.min.js', 'xlsx/dist/', 'xlsx/'],

    // [[
    //     'resources/themes/bootstrap4-dark-blue/theme.css',
    //     'resources/themes/bootstrap4-light-blue/theme.css',

    //     'core/core.js',
    //     // 'api/api.js',
    //     // 'badge/badge.js',
    //     // 'base/base.js',
    //     // 'basecomponent/basecomponent.js',
    //     // 'basedirective/basedirective.js',
    //     // 'baseicon/baseicon.js',
    //     // 'button/button.js',
    //     // 'config/config.js',
    //     // 'confirmationeventbus/confirmationeventbus.js',
    //     // 'dialog/dialog.js',
    //     // 'dropdown/dropdown.js',
    //     // 'dynamicdialogeventbus/dynamicdialogeventbus.js',
    //     // 'focustrap/focustrap.js',
    //     // 'icons/icons.js',
    //     // 'inputnumber/inputnumber.js',
    //     // 'inputtext/inputtext.js',
    //     // 'menu/menu.js',
    //     // 'message/message.js',
    //     // 'overlayeventbus/overlayeventbus.js',
    //     // 'paginator/paginator.js',
    //     // 'passthrough/passthrough.js',
    //     // 'portal/portal.js',
    //     // 'progressbar/progressbar.js',
    //     // 'ripple/ripple.js',
    //     // 'terminalservice/terminalservice.js',
    //     // 'tieredmenu/tieredmenu.js',
    //     // 'toasteventbus/toasteventbus.js',
    //     // 'tooltip/tooltip.js',
    //     // 'tree/tree.js',
    //     // 'useconfirm/useconfirm.js',
    //     // 'usedialog/usedialog.js',
    //     // 'usestyle/usestyle.js',
    //     // 'usetoast/usetoast.js',
    //     // 'utils/utils.js',
    //     // 'virtualscroller/virtualscroller.js',

    //     'confirmationservice/confirmationservice.js',
    //     'dialogservice/dialogservice.js',
    //     'toastservice/toastservice.js',

    //     'autocomplete/autocomplete.js',
    //     'avatar/avatar.js',
    //     'badgedirective/badgedirective.js',
    //     'blockui/blockui.js',
    //     'calendar/calendar.js',
    //     'card/card.js',
    //     'cascadeselect/cascadeselect.js',
    //     'chart/chart.js',
    //     'checkbox/checkbox.js',
    //     'chip/chip.js',
    //     'chips/chips.js',
    //     'colorpicker/colorpicker.js',
    //     'column/column.js',
    //     'columngroup/columngroup.js',
    //     'confirmdialog/confirmdialog.js',
    //     'confirmpopup/confirmpopup.js',
    //     'contextmenu/contextmenu.js',
    //     'datatable/datatable.js',
    //     'dataview/dataview.js',
    //     'dataviewlayoutoptions/dataviewlayoutoptions.js',
    //     'deferredcontent/deferredcontent.js',
    //     'divider/divider.js',
    //     'dynamicdialog/dynamicdialog.js',
    //     'fileupload/fileupload.js',
    //     'image/image.js',
    //     'inlinemessage/inlinemessage.js',
    //     'inputmask/inputmask.js',
    //     'inputswitch/inputswitch.js',
    //     'listbox/listbox.js',
    //     'multiselect/multiselect.js',
    //     'overlaypanel/overlaypanel.js',
    //     'password/password.js',
    //     'progressspinner/progressspinner.js',
    //     'radiobutton/radiobutton.js',
    //     'row/row.js',
    //     'scrollpanel/scrollpanel.js',
    //     'selectbutton/selectbutton.js',
    //     'slider/slider.js',
    //     'splitbutton/splitbutton.js',
    //     'splitter/splitter.js',
    //     'splitterpanel/splitterpanel.js',
    //     'steps/steps.js',
    //     'styleclass/styleclass.js',
    //     'tag/tag.js',
    //     'textarea/textarea.js',
    //     'timeline/timeline.js',
    //     'toast/toast.js',
    //     'togglebutton/togglebutton.js',
    //     'toolbar/toolbar.js',
    //     'tristatecheckbox/tristatecheckbox.js',

    //     'accordion/accordion.js',
    //     'accordiontab/accordiontab.js',
    //     'avatargroup/avatargroup.js',
    //     'breadcrumb/breadcrumb.js',
    //     'carousel/carousel.js',
    //     'dock/dock.js',
    //     'editor/editor.js',
    //     'fieldset/fieldset.js',
    //     'galleria/galleria.js',
    //     'inplace/inplace.js',
    //     'knob/knob.js',
    //     'megamenu/megamenu.js',
    //     'menubar/menubar.js',
    //     'orderlist/orderlist.js',
    //     'organizationchart/organizationchart.js',
    //     'panel/panel.js',
    //     'panelmenu/panelmenu.js',
    //     'picklist/picklist.js',
    //     'rating/rating.js',
    //     'scrolltop/scrolltop.js',
    //     'sidebar/sidebar.js',
    //     'skeleton/skeleton.js',
    //     'speeddial/speeddial.js',
    //     'tabmenu/tabmenu.js',
    //     'tabpanel/tabpanel.js',
    //     'tabview/tabview.js',
    //     'terminal/terminal.js',
    //     'treeselect/treeselect.js',
    //     'treetable/treetable.js',
    // ], 'primevue/'],

    // [[
    //     'fonts/primeicons.eot',
    //     'fonts/primeicons.svg',
    //     'fonts/primeicons.ttf',
    //     'fonts/primeicons.woff',
    //     'primeicons.css',
    // ], 'primeicons/'],

].forEach(row => (
    (files, from, to, fn) => fn !== false && (files.constructor === Array ? files : [files])
        .forEach(file => (fn||copyResource)(`${moduleFolder}${from}${file}`, `src/Avon/res/asset/${to||from}${file}`))
)(...row));
