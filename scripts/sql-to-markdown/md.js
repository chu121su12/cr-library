(() => {
    const documentReady = (() => {
        const den = 'DOMContentLoaded';

        let DOCUMENT = document;

        let documentReadyQueue = [];

        let domIsReady = false;

        function dcl() {
            if (!domIsReady) {
                domIsReady = true;

                [...documentReadyQueue].forEach((callback) => {
                    try {
                        callback();
                    }
                    catch (error) {
                        console.error(error);
                    }
                });

                DOCUMENT.removeEventListener(den, dcl);

                DOCUMENT = null;

                documentReadyQueue = [];
            }
        }

        DOCUMENT.addEventListener(den, dcl);

        return (callback) => {
            if (domIsReady) {
                callback();
            }
            else {
                documentReadyQueue.push(callback);
            }
        };
    })();

    const makeDebounce = (() => {
        function runSource(source, that, args) {
            try {
                source && source.apply && source.apply(that, args);
            }
            catch (_error) {
            }
        }

        function runInspect(source, inspect, mode, that, args) {
            try {
                return inspect && inspect.apply && inspect.apply(that, [
                    mode,
                    () => runSource(source, that, args),
                ]) === false;
            }
            catch (_error) {
                return false;
            }
        }

        return (options, source) => {
            options = parseFloat(options) == options ? { delay: options, trailing: true } : (options || {});
            const inspect = options.inspect;
            const leading = !!options.leading;
            const trailing = !!options.trailing;
            const delay = options.delay;
            source = source || options.source || (() => {});

            let timeoutID = null;
            let didLeading = false;

            function reset() {
                clearTimeout(timeoutID);
                timeoutID = null;
            }

            function bounced() {
                const args = arguments;
                const that = this;
                reset();

                if (runInspect(source, inspect, 1, that, args)) {
                    return;
                }

                if (!didLeading) {
                    didLeading = true;
                    if (runInspect(source, inspect, 2, that, args)) {
                        return;
                    }
                    if (leading) {
                        runSource(source, that, args);
                    }
                }

                timeoutID = setTimeout(() => {
                    if (!runInspect(source, inspect, 3, that, args)) {
                        if (trailing) {
                            runSource(source, that, args);
                        }
                    }
                    didLeading = false;
                }, delay);
            }

            bounced.reset = reset;
            bounced.original = source;
            bounced.bounced = bounced;

            return bounced;
        };
    })();

    const randomStringDefaultPool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    const randomString = (length, pool) => {
        length = length || 16;
        pool = pool || randomStringDefaultPool;
        const charactersLength = pool.length;
        let result = '';

        for (let i = 0; i < length; ++i) {
            result += pool.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
    };

    function parseTableLine(tableName, ln) {
        const lnlc = ln.trim().toLowerCase();

        if (lnlc.startsWith('primary key')) {
            return null;
        }

        const match = /^(`\w+`(?:[.]`[.\w]+`)*)[ ]+(\w+(?:[(]\d+(?:,\d+)*[)])?) *(.*)/.exec(ln.trim());
        if (! match) {
            return null;
        }

        const [_, colName, _fullType, rest] = match;
        let defaultValue = '';
        let nullable = true;
        let _rests = (rest ?? '');

        if (_rests !== '') {
            const rest1 = /^(.*)\bdefault\b[ ]*(\w+(?:\(\d*\))?|'[^']*?')[ ]?(.*)$/i.exec(_rests);
            if (rest1) {
                defaultValue = (rest1[2] ?? '').trim();
                if (defaultValue !== '') {
                    _rests = rest1[1] + ' ' + rest1[3];
                }
            }
        }
        let _defaultValueDesc = '';
        if (defaultValue === "''") {
            _defaultValueDesc = '(string kosong)';
        }

        if (_rests !== '') {
            const rest2 = /^[ ]*((?:not[ ]+)?null)\b(.*)$/i.exec(_rests);
            if (rest2) {
                if (rest2[1].toLowerCase().startsWith('not')) {
                    nullable = false;
                }
                _rests = (rest2[2] ?? '').trim();
            }
        }

        let dataType = _fullType;
        let typeLength = 0;
        let precision = '';
        const typeMatch = /^(\w+)(?:\((\d+)((?:,\d+)*)\))?$/.exec(_fullType);
        if (typeMatch) {
            dataType = typeMatch[1];
            if (typeMatch[2] != null) {
                typeLength = parseInt(typeMatch[2], 10);
            }
            if (typeMatch[3] != null) {
                precision = typeMatch[3].replace(/^,/, '');
            }
        }

        let _guessedType = _fullType;
        if (['int', 'bigint', 'tinyint'].includes(dataType.trim().toLowerCase())) {
            _guessedType = dataType;
        }

        const returnValue = {
            _type: 'table',
            _line: tableName + ' -- -- ' + ln.trim().toLowerCase(),
            colName,
            dataType,
            typeLength,
            precision,
            defaultValue,
            nullable,
            _defaultValueDesc,
            _guessedType,
            _fullType,
            _rests,
        };

        return returnValue;
    }

    function captureTablesFromNewlineFormattedCreateTable(content) {
        let tableLines = [];
        let table = '';
        const tables = {};

        const storeTable = () => {
            if (table === '') {
                return;
            }

            tables[table] = tableLines
                .filter((ln) => ln.startsWith(' '))
                .map((ln) => ln.trim())
                .map((ln) => ln.replace(/,$/, ''))
                .map((ln) => parseTableLine(table, ln))
                .filter(Boolean)
                ;
        };

        for (let line of content.split('\n')) {
            line = line.replace(/-- .*/, '');
            if (line.trim().toLowerCase().startsWith('create table')) {
                storeTable();

                tableLines = [line];
                table = line.replace(/create table([^(]+).*/i, '$1');
            }
            else {
                tableLines.push(line);
            }
        }
        storeTable();

        return tables;
    }

    function generateSqlMdFromTables(tables) {
        const markdowns = [];
        for (const [table, defs] of Object.entries(tables)) {
            const lines = [];

            lines.push('## Tabel ``` ' + table + '```');

            const tables = [];
            for (const def of defs) {
                if (def._type === 'table') {
                    const {
                        colName,
                        defaultValue,
                        nullable,
                        _defaultValueDesc,
                        _guessedType,
                        _rests,
                    } = def;

                    const name = '``` ' + colName + ' ```';
                    const defaultValDsc = _defaultValueDesc === '' ? '' : (' _' + _defaultValueDesc + '_');
                    const defaultVal = defaultValue === '' ? '—' : ('`' + defaultValue + '`' + defaultValDsc);
                    tables.push(`${name}|${_guessedType}|${nullable ? '`null`' : '✗'}|${defaultVal}|${_rests}`);
                }
            }
            if (tables.length !== 0) {
                lines.push('Nama|Tipe|null?|Default|Lainnya');
                lines.push(':-|:-|:-:|:-|:-');
                lines.push(...tables);
                lines.push('---');
            }

            markdowns.push(lines.join('\n').replace(/^[\u200B\u200C\u200D\u200E\u200F\uFEFF]/,""));
        }

        return markdowns;
    }

    function generateSqlMarkdown(text) {
        const tables = captureTablesFromNewlineFormattedCreateTable(
            text
        );

        return generateSqlMdFromTables(tables).join('\n\n');
    }

    function getPreElementTextContent(id)
    {
        const pre = document.getElementById(id);
        if (pre && pre.textContent.trim() !== '') {
            const text = pre.textContent;

            if (pre) {
                pre.parentNode.removeChild(pre);
            }

            return text;
        }

        return '';
    }

    function createDiv(id, parent) {
        let dom = document.getElementById(id);
        if (!dom) {
            dom = document.createElement('div');
            dom.setAttribute('id', id);
            (parent || document.body).insertBefore(dom, null);
        }
        return dom;
    }

    function createRootFontResizerElement(parent)
    {
        parent = parent || document.body;

        let input = document.getElementById('fsize');
        if (!input) {
            input = document.createElement('input');
            input.setAttribute('id', 'fsize');
            input.setAttribute('type', 'number');
            input.setAttribute('value', '100');
            input.setAttribute('min', '10');
            input.setAttribute('max', '300');
            input.setAttribute('step', '0.5');
            input.style['font-size'] = '16pt';
            const div = document.createElement('div');
            div.classList.add('print-none');
            parent.insertBefore(div, null);
            div.insertBefore(input, null);
        }

        input.addEventListener('input', () => {
            document.documentElement.style['font-size'] = input.value + '%';

            [...document.querySelectorAll('svg.mermaid')].forEach((el) => {
                const wRef = parseInt(el.getAttribute('data-width'), 10);
                const newW = wRef * input.value / 100;
                el.style.width = newW + 'px';
            });
        });
        document.documentElement.style['font-size'] = '100%';
    }

    function createInputResizerElement(parent)
    {
        let input = document.getElementById('tawidth');
        if (!input) {
            input = document.createElement('input');
            input.setAttribute('id', 'tawidth');
            input.setAttribute('type', 'number');
            input.setAttribute('value', '1');
            input.setAttribute('min', '1');
            input.setAttribute('max', '10');
            input.style.float = 'right';
            const div = document.createElement('div');
            parent.insertBefore(div, null);
            div.insertBefore(input, null);
        }

        input.addEventListener('input', () => {
            parent.style.width = (input.value * 10) + 'rem';
        });
        parent.style.width = '10rem';
    }

    function createTextareaElement(parent, name)
    {
        let input = document.getElementById('input' + name);

        if (!input) {
            input = document.createElement('textarea');
            input.setAttribute('id', 'input' + name);
            input.setAttribute('name', 'input' + name);
            input.setAttribute('resize', 'both');

            const value = getPreElementTextContent(name);
            input.value = value || ('-- replace this content with ' + name);

            parent.insertBefore(input, null);
        }

        return input;
    }

    const availableMermaidTypes = [
        'flowchart',
        'graph',
        'sequenceDiagram',
        'classDiagram',
        'stateDiagram',
        'stateDiagram-v2',
        'erDiagram',
        'journey',
        'gantt',
        'pie',
        'quadrantChart',
        'requirementDiagram',
        'gitGraph',
        'mindmap',
        'timeline',
        'zenuml',
        'C4Context',
        'sankey-beta',
        'xychart-beta',
        'block-beta',
        'packet-beta',
        'kanban',
        'architecture-beta',
    ];

    function captureMultipleMermaidByDiagramTypes(content) {
        // starts with ---, %%
        let previousHeaderLines = [];
        let headerLines = [];
        let diagramLines = [];
        let type = '';
        const diagrams = [];

        let hasAnyDiagram = false;

        const storeDiagram = () => {
            diagrams.push({
                type,
                headers: previousHeaderLines,
                title: previousHeaderLines
                    .filter(line => line.match(/^ *title *: *[^ ]/i))
                    .map(line => line.replace(/^ *title *: *(.+)/i, '$1'))
                    .join(' '),
                diagram: diagramLines.join('\n'),
            });

            previousHeaderLines = headerLines;
            type = '';
            headerLines = [];
            diagramLines = [];
        };

        let inHeader = false;
        for (const line of content.split('\n')) {
            const lineTrim = line.trim();

            if (lineTrim.startsWith('---')) {
                inHeader = !inHeader;
                continue;
            }

            if (inHeader) {
                headerLines.push(line);
            }
            else {
                for (const item of availableMermaidTypes) {
                    if (lineTrim.startsWith(item)) {
                        storeDiagram();
                        hasAnyDiagram = true;
                        type = item;
                    }
                }
                diagramLines.push(line);
            }
        }
        storeDiagram();

        return diagrams;
    }

    function getUnsetScalingFactor(cssProperty, element)
    {
        const bcr1 = element.getBoundingClientRect();
        const h1 = bcr1.height;
        const w1 = bcr1.width;

        const originalStyle = element.style[cssProperty];

        element.style[cssProperty] = 'unset';

        const bcr2 = element.getBoundingClientRect();
        const h2 = bcr2.height;
        const w2 = bcr2.width;

        element.style[cssProperty] = originalStyle;

        return {
            h: h2 / h1,
            w: w2 / w1,
        };
    }

    async function mermaidRenderSingle(input) {
        const { type, title, diagram } = input;

        const parsed = await mermaid.parse(diagram, {
            suppressErrors: true,
        });

        if (!parsed) {
            return '';
        }

        const temporaryContainer = document.createElement('div');
        const id = randomString(16);
        temporaryContainer.setAttribute('id', id);
        document.body.insertBefore(temporaryContainer, null);

        const rendered = await mermaid.render(id, diagram);
        temporaryContainer.parentNode?.removeChild(temporaryContainer);

        html = rendered?.svg || '';

        if (!html) {
            return '';
        }

        const svgContainer = document.createElement('div');
        svgContainer.innerHTML = html;
        const svg = svgContainer.firstChild;

        document.body.insertBefore(svgContainer, null);

        const sf = getUnsetScalingFactor('font-size', svg.querySelector('text[style*="font-size: 12px"]'));
        const width = svg.getBoundingClientRect().width * Math.max(sf.h, sf.w);
        svg.setAttribute('data-width', width);
        svg.style['max-width'] = 'unset';
        svg.style['width'] = width + 'px';
        svg.classList.add('mermaid');

        svgContainer.parentNode?.removeChild(svgContainer);

        const titleH2 = marked.parse('## ' + (title || type));
        return titleH2 + svg.outerHTML;
    }

    async function mermaidRender(text) {
        const hr = marked.parse('---');

        const blocks = await Promise.allSettled(
            captureMultipleMermaidByDiagramTypes(text)
                .map(mermaidRenderSingle)
        );

        return blocks
            .map(x => x.value)
            .filter(Boolean)
            .join('\n' + hr + '\n');
    }

    documentReady(() => {
        mermaid.initialize({
            startOnLoad: false,
            useMaxWidth: false,
            theme: 'neutral',
            fontFamily: 'var(--font-family-sans-serif)',
        });

        createRootFontResizerElement();

        const mdView = createDiv('markdown');

        const inputs = createDiv('inputs');
        inputs.classList.add('print-none');

        createInputResizerElement(inputs);
        const mermaidInput = createTextareaElement(inputs, 'mermaid');
        const sqlInput = createTextareaElement(inputs, 'sql');

        const renderer = makeDebounce({
            delay: 1000,
            trailing: true,
            leading: true,
            async source() {
                const mmd = mermaidInput.value;
                const sql = sqlInput.value;

                const sqlMdHtml = marked.parse(generateSqlMarkdown(sql));
                const mermaidMdHtml = await mermaidRender(mmd);

                const diagramHr = mermaidMdHtml ? marked.parse('---') : '';

                mdView.innerHTML = mermaidMdHtml + diagramHr + sqlMdHtml;
            },
        });

        [
            mermaidInput,
            sqlInput,
        ].forEach(input => input.addEventListener('input', renderer));

        renderer();
    });
})();
