<?php function html($data) { ?><!doctype html>
<html><head><meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Test Page</title>

<div><?= \implode('<br>', $data['ips']); ?></div><hr>

<button id="run">run</button><br>
<button id="abort" disabled>stop</button><hr>

<div id="status">0</div>

<table style="text-align:right">
<thead><tr>
<th>time</th>
<th>[w]</th>
<th>size</th>
<th>~bandwidth</th>
<th>~duration</th>
</tr></thead>

<tbody id="timings"></tbody>
</table>

<script>(function() {
    function getTime() { return (new Date()).getTime(); }

    function download(size, resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.onabort = function() {
            reject && reject({
                timing: { endTime: getTime(), startTime: startTime, },
                xhr: xhr,
            });
        };
        xhr.onreadystatechange = function() {
            if (xhr.readyState !== 4) return;

            var payload = {
                timing: { endTime: getTime(), startTime: startTime, },
                xhr: xhr,
            };
            if (xhr.status >= 200 && xhr.status < 400) {
                resolve && resolve(payload);
            } else {
                reject && reject(payload);
            }
        };
        xhr.open('POST', location + '?payload=' + size, true);
        var startTime = getTime();
        xhr.send();
        return xhr;
    }

    function beginTest(size, done) {
        return download(size, function(payload) {
            var payloadSize = payload.xhr.responseText.length;
            var timing = payload.timing;
            var time = timing.endTime - timing.startTime;
            done && done({
                endTime: timing.endTime,
                payloadSize: payloadSize,
                speedBps: Math.round(payloadSize / (time / 1000)),
                startTime: timing.startTime,
                status: 1,
                time: time,
            });
        }, function(payload) {
            var endTime = getTime();
            done && done({
                endTime: endTime,
                payloadSize: -1,
                speedBps: -1,
                startTime: payload.timing.startTime,
                status: 0,
                time: endTime - payload.timing.startTime,
            });
        });
    }

    var hitCounter = 0;
    var pings = [];
    var benchmarks = [];

    var status = document.getElementById('status');
    var timings = document.getElementById('timings');
    var runner = document.getElementById('run');
    var aborter = document.getElementById('abort');
    var running = false;
    var runningXhr;

    function reduceSum(key, data) {
        return data
            .filter(function(x) { return x.status; })
            .map(function(x) { return x[key]; })
            .sort(function(a, b) { return a - b; })
            .slice(1, -1)
            .reduce(function(p, c) { return p + c; }, 0);
    }

    var NBSP = '&nbsp;';
    var STR = '';
    function updateTimingData(data, result) {
        status.innerHTML = ++hitCounter;
        if (!result.status) return;

        data.push(result);
        if (data.length < 3) return;
        if (data.length > 10) data.splice(0, 1);

        var latest = data.slice(-1)[0];
        var time = Math.round(reduceSum('time', data) / (data.length - 2));
        var rowData = {
            start: (STR + new Date(latest.startTime)).replace(/.*\b(\d+:\d+:\d+).*/, '$1'),
            data: data.length,
            size: STR + latest.payloadSize + NBSP + 'B',
            speed: STR + Math.round(reduceSum('speedBps', data) / (data.length - 2) / 1024) + NBSP + 'kBps',
            time: STR + time + NBSP + 'ms',
        };

        var row = document.createElement('tr');
        for (var label in rowData) {
            var cell = document.createElement('td');
            cell.innerHTML = rowData[label];
            row.appendChild(cell);
        }
        timings.insertBefore(row, timings.firstChild);
        return latest.payloadSize / time;
    }

    var runningRate = 30;
    function runTest() {
        if (hitCounter >= 30) stopTest();

        requestAnimationFrame(function(){
            if (!running) return;

            if (Math.random() > 0.5) {
                runningXhr = beginTest(1, function(result) {
                    updateTimingData(pings, result);
                    setTimeout(runTest, Math.random() * 999 + 444);
                });
            } else {
                var size = Math.max(1024, Math.min(
                    2 * 1024 * 1024,
                    runningRate * 2000 * (0.8 + 2.4 * Math.random())
                ));
                runningXhr = beginTest(Math.round(size), function(result) {
                    var rate = updateTimingData(benchmarks, result);
                    if (rate > 0) runningRate = (runningRate + rate) / 2;
                    setTimeout(runTest, Math.random() * 555 + 111);
                });
            }
        });
    }

    function stopTest() {
        running = false;
        runningXhr && runningXhr.abort && runningXhr.abort();
        runner.disabled = false;
        aborter.disabled = true;
    }

    runner.onclick = function() {
        hitCounter = 0;
        pings = [];
        benchmarks = [];

        var row = document.createElement('tr');
        var cell = document.createElement('td');
        cell.setAttribute('colspan', 5);
        cell.innerHTML = new Date;
        row.appendChild(cell);
        timings.insertBefore(row, timings.firstChild);

        runner.disabled = true;
        aborter.disabled = false;
        running = true;
        runTest();
    };

    aborter.onclick = stopTest;
})();</script>

<?php }

if (! (isset($_POST) && \array_key_exists('payload', $_GET))) {
    $server = \array_intersect_key($_SERVER ?: [], \array_flip([
        'HTTP_CLIENT_IP',
        'HTTP_X_FORWARDED_FOR',
        'HTTP_X_FORWARDED',
        'HTTP_X_CLUSTER_CLIENT_IP',
        'HTTP_FORWARDED_FOR',
        'HTTP_FORWARDED',
        'REMOTE_ADDR',
    ]));

    html([
        'ips' => \array_map(static function ($v, $k) {
            return "{$k}: {$v}";
        }, $server, \array_keys($server)),
    ]);
} else {
    $size = \max(1, \min(
        4 * 1024 * 1024,
        (int) $_GET['payload']
    )); // clamp from 1b - 4mb

    // should prevent compression
    \header('Content-Length: ' . $size);
    \header('Content-Type: application/octet-stream');

    function randomString($length)
    {
        return \str_shuffle(\substr(\str_repeat(
            \md5(\mt_rand()),
            2 + $length / 32
        ), 0, $length));
    }

    // echo randomString($size);
    echo \str_repeat(' ', $size);
}
