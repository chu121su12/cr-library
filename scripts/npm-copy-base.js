import nodeFs from 'fs';
import fs from 'graceful-fs';
import path from 'path';

import CleanCss from 'clean-css';
import uglifyJs from 'uglify-js';
import htmlMinifier from 'html-minifier';
// const { optimize: svgoOptimize } = require('svgo');

fs.gracefulify(nodeFs);

const expandLimit = 1.0;

const moduleFolder = 'node_modules/';

const cleanCssMinify = new CleanCss({
    sourceMap: false,
    level: {
        1: {
            specialComments: false,
        },
    },
});

const cleanCssBeautify = new CleanCss({
    format: 'beautify',
    sourceMap: false,
});

function uglifyMinify (input) {
    return uglifyJs.minify(input, {
        ie8: true,
        webkit: false,
        compress: {
            passes: 1,
        },
        output: {
            beautify: false,
        },
    });
}

const svgoPlugins = [
    'cleanupAttrs',
    'inlineStyles',
    'removeDoctype',
    'removeXMLProcInst',
    'removeComments',
    'removeMetadata',
    'removeTitle',
    'removeDesc',
    'removeUselessDefs',
    'removeXMLNS',
    'removeEditorsNSData',
    'removeEmptyAttrs',
    'removeHiddenElems',
    'removeEmptyText',
    'removeEmptyContainers',
    'cleanupEnableBackground',
    'minifyStyles',
    'convertStyleToAttrs',
    'convertColors',
    'removeUnknownsAndDefaults',
    'removeNonInheritableGroupAttrs',
    'removeUnusedNS',
    'cleanupIDs',
    'cleanupNumericValues',
    'moveElemsAttrsToGroup',
    'moveGroupAttrsToElems',
    'collapseGroups',
    'sortAttrs',
    'sortDefsChildren',
    'removeDimensions',
];

function copyCss (from, to) {
    fs.copyFile(from, to, writeErr => {
        if (writeErr) throw writeErr;
    });

    const minified = to.match(/\.min\.css$/);

    to = to.replace(/(\.min)?\.css$/, '.css');

    fs.readFile(from, {encoding: 'utf-8'}, (readErr, input) => {
        if (readErr) throw readErr;

        cleanCssMinify.minify(input, (minifyError, output) => {
            if (minifyError) throw {
                file: from,
                error: minifyError,
            };

            fs.writeFile(to.replace(/\.css$/, '.min.css'), minified ? input : output.styles, writeErr => {
                if (writeErr) throw writeErr;
            });

            if (output.styles.length / input.length > expandLimit) {
                cleanCssBeautify.minify(input, (beautifyError, beautifyOutput) => {
                    if (beautifyError) throw {
                        file: from,
                        error: beautifyError,
                    };

                    fs.writeFile(to.replace(/\.css$/, '.expanded.css'), beautifyOutput.styles, writeErr => {
                        if (writeErr) throw writeErr;
                    });
                });
            }
        });

    });
}

function copyJs (from, to) {
    fs.copyFile(from, to, writeErr => {
        if (writeErr) throw writeErr;
    });

    const minified = to.match(/\.min\.js$/);

    to = to.replace(/(\.min)?\.js$/, '.js');

    fs.readFile(from, {encoding: 'utf-8'}, (readErr, input) => {
        if (readErr) throw readErr;

        const result = uglifyMinify(input);

        if (result.error) {
            fs.writeFile(to, input, writeErr => {
                if (writeErr) throw writeErr;
            });

        } else {
            fs.writeFile(to.replace(/\.js$/, '.min.js'), minified ? input : result.code, writeErr => {
                if (writeErr) throw writeErr;
            });

            if (result.code.length / input.length > expandLimit) {
                fs.writeFile(to.replace(/\.js$/, '.expanded.js'), beautify(input).code, writeErr => {
                    if (writeErr) throw writeErr;
                });
            }
        }
    });
}

function copySvg (from, to) {
    copyOnly(from, to);

    // fs.readFile(from, {encoding: 'utf-8'}, (readErr, input) => {
    //     if (readErr) throw readErr;

    //     svgoOptimize(input, {
    //         multipass: true,
    //         path: to,
    //         plugins: svgoPlugins,
    //     });
    // });
}

function gatherSubdirectory (dir) {
    const results = [];

    fs.readdirSync(dir).forEach(file => {
        const fullPath = dir + path.sep + file;

        const stat = fs.statSync(fullPath);

        if (stat && stat.isDirectory()) {
            // results = results.concat(gatherSubdirectory(fullPath));
        } else {
            results.push(fullPath);
        }
    });

    return results;
}

function copyOnly (from, to) {
    fs.mkdirSync(path.dirname(to), { recursive: true });

    fs.copyFile(from, to, writeErr => {
        if (writeErr) throw writeErr;
    });
}

function copyResource (from, to) {
    fs.mkdirSync(path.dirname(to), { recursive: true });

    if (fs.lstatSync(from).isDirectory()) {
        fs.mkdirSync(to, { recursive: true });

        return gatherSubdirectory(from).forEach(file => copyResource(file, to + path.sep + path.basename(file)));
    }

    if (from.match(/package\.json$/)) {
        return;

    } else if (from.match(/\.css$/)) {
        copyCss(from, to);
    } else if (from.match(/\.js$/)) {
        copyJs(from, to);
    } else if (from.match(/\.svg$/)) {
        copySvg(from, to);
    } else {
        copyOnly(from, to);
    }
}

function beautify (input) {
    return uglifyJs.minify(input, {
        ie8: true,
        webkit: false,
        compress: false,
        output: {
            ascii_only: true,
            beautify: true,
        },
    });
}

function beautifyFile (from, to) {
    if (! to) {
        to = from.match(/\.min/)
            ? from.replace(/\.min(\.\w+)$/, '$1')
            : from.replace(/\.(\w+)$/, '.expanded.$1');
    }

    if (from.match(/\.js$/)) {
        fs.readFile(from, {encoding: 'utf-8'}, (readErr, input) => {
            if (readErr) throw readErr;

            const result = beautify(input);
    
            if (result.error) {
                fs.writeFile(to, input, writeErr => {
                    if (writeErr) throw writeErr;
                });
    
            } else {
                fs.writeFile(to, result.code, writeErr => {
                    if (writeErr) throw writeErr;
                });
            }
        });
    }
}

function minifyHtmlSafeish (input, options) {
    return htmlMinifier.minify(input, {
        caseSensitive: true,
        collapseWhitespace: true,
        conservativeCollapse: true,
        ...(options || {}),
    });
};

function getCleanCss() {
    return cleanCssMinify;
}

export {
    fs,
    moduleFolder,
    copyOnly,
    copyResource,
    beautifyFile,
    getCleanCss,
    uglifyMinify,
    minifyHtmlSafeish,
}
