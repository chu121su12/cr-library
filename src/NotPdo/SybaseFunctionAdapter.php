<?php

namespace CR\Library\NotPdo;

use PDO;

class SybaseFunctionAdapter
{
    private static $connectionCount = 0;

    private static $connections = [];

    private static $lastConnection;

    public static function fetchAll($queryResult)
    {
        if ($queryResult->fetch === false) {
            $queryResult->fetch = [];
            $queryResult->fetch = $queryResult->statement->fetchAll() ?: [];
            $queryResult->fetchCount = \count($queryResult->fetch);
        }
    }

    public static function provideSybaseFunctions()
    {
        if (! \function_exists('sybase_connect')) {
            require_once __DIR__ . '/../../patch/sybase_functions.php';
        }
    }

    public static function sybase_affected_rows($connection)
    {
        $connection = self::resolveActiveConnection($connection);

        if ($queryResult = $connection->lastResult) {
            self::fetchStatementNext($queryResult);

            return $queryResult->fetchCount;
        }

        return 0;
    }

    public static function sybase_close($connection)
    {
        $connection = self::resolveActiveConnection($connection);

        foreach (self::$connections as $index => $activeConnection) {
            if ($connection->connectionKey === $activeConnection->connectionKey) {
                unset(self::$connections[$index]);
                self::$connections = \array_values(self::$connections);
            }
        }
    }

    public static function sybase_connect($servername, $username, $password, $charset, $appname, $new)
    {
        $connectionKey = \http_build_query([
            'host' => $servername,
            'username' => $username,
            'password' => \sha1(isset($password) ? $password : ''),
        ]);

        if (! $new) {
            foreach (self::$connections as $activeConnection) {
                if ($activeConnection->connectionKey === $connectionKey) {
                    return $activeConnection;
                }
            }
        }

        self::$lastConnection = (object) [
            'sybase_connect' => true,
            'connectionKey' => $connectionKey,
            'pdo' => new PDO("dblib:host={$servername}", $username, $password),
            'connectionCount' => self::$connectionCount,
            'lastResult' => null,
        ];

        self::$connections[] = self::$lastConnection;

        ++self::$connectionCount;

        return self::$lastConnection;
    }

    public static function sybase_connect_env($host, $port, $database, $username, $password)
    {
        Sybase::env([
            'database' => $database,
            'host' => $host,
            'port' => $port,
        ]);

        return self::sybase_connect($database, $username, $password, null, null, true);
    }

    public static function sybase_fetch_assoc($queryResult)
    {
        return self::fetchStatementNext($queryResult);
    }

    public static function sybase_fetch_object($queryResult, $object)
    {
        $result = self::fetchStatementNext($queryResult);

        if ($result === false) {
            return false;
        }

        if ($object === null) {
            return (object) $result;
        }

        if (\is_string($object)) {
            $object = new $object;
        }

        foreach ($result as $key => $_value) {
            $object->{$key} = $result[$key];
        }

        return $object;
    }

    public static function sybase_free_result($queryResult)
    {
        $queryResult->statement = false;
        $queryResult->result = null;
        $queryResult->fetch = [];
        $queryResult->fetchCount = 0;
        $queryResult->fetchCursor = 1;
        $queryResult->connection->lastResult = null;

        return true;
    }

    public static function sybase_query($query, $connection)
    {
        $connection = self::resolveActiveConnection($connection);
        $pdo = $connection->pdo;

        $statement = $pdo->prepare($query);
        $connection->lastResult = (object) [
            'sybase_query' => true,
            'query' => $query,
            'connection' => $connection,
            'statement' => $statement,
            'result' => $statement->execute(),
            'fetch' => false,
            'fetchCount' => 0,
            'fetchCursor' => 0,
        ];

        return $connection->lastResult;
    }

    private static function fetchStatementNext($queryResult)
    {
        self::fetchAll($queryResult);

        if ($queryResult->fetchCursor < $queryResult->fetchCount) {
            return (array) $queryResult->fetch[$queryResult->fetchCursor++];
        }
    }

    private static function resolveActiveConnection($connection)
    {
        return $connection ?: self::$lastConnection;
    }
}
