<?php

namespace CR\Library\NotPdo;

use CR\Library\Helpers\Database;
use CR\Library\Helpers\Helpers;
use CR\Library\Helpers\LocaleIndonesia;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use PDO;

class Sybase
{
    protected static $watchRecursive;

    protected static $current;

    protected static $lastAttempted;

    protected $bindingIndexes = [];

    protected $bindings;

    protected $bindingsOriginal;

    protected $bindingTypes = [];

    protected $bindingValues = [];

    protected $config;

    protected $connection;

    protected $env;

    protected $fetchModes = [
        'arg1' => null,
        'arg2' => null,
    ];

    protected $options;

    protected $pdoResolver;

    protected $rowCount;

    //

    protected $sql;

    protected $sqlBound;

    protected $sqlOriginal;

    protected $statement;

    protected $statementClass;

    public static function doLog()
    {
        return false;
    }

    public function beginTransaction()
    {
        $this->runUnpreparedQuery('BEGIN TRANSACTION;', null, $this->connection);
    }

    public function bindValue($index, $value, $type)
    {
        $this->bindingValues[] = $value;
        $this->bindingIndexes[] = $index;
        $this->bindingTypes[] = $type;

        return $this;
    }

    public function closeCursor()
    {
        try {
            while (true) {
                while ($this->sqlFetchAssocBase()) { // finish current query
                }

                if (! $this->statement || ! \method_exists($this->statement, 'nextRowset')) {
                    break;
                }

                if (! $this->statement->nextRowset()) {
                    break;
                }
            }
        }
        catch (Exception $e) {
            Helpers::reportIgnoredException('Error closing cursor info.', $e);

            return false;
        }

        return true;
    }

    public function commit()
    {
        $this->runUnpreparedQuery('COMMIT;', null, $this->connection);
    }

    public function connect()
    {
        if (self::$watchRecursive) {
            $currentAttempt = \implode('|', [
                Arr::get($this->config, 'driver'),
                Arr::get($this->config, 'host'),
                Arr::get($this->config, 'port'),
                Arr::get($this->config, 'database'),
            ]);

            if (self::$lastAttempted === $currentAttempt) {
                $exception = new Exception('Unable to conect');
                Log::debug($exception->getTraceAsString());

                throw $exception;
            }

            self::$lastAttempted = $currentAttempt;
        }

        if (self::doLog()) {
            Log::debug(\sprintf('sql-notpdo-connecting: [%s]', $this->config['name']), Arr::only($this->config, [
                'driver',
                'host',
                'port',
                'database',
            ]));
        }

        self::env($this->config);

        $this->env = true;
        $this->statement = null;

        $this->connection = Helpers::withExceptionErrorHandler(function () {
            return self::callUserFunc($this->pdoResolver ?: self::functionResolver(), $this);
        });

        if ($this->connection === null) {
            throw new Exception(\sprintf('Cannot connect to database. (%s)', \json_encode($this->config)));
        }

        if (self::doLog()) {
            Log::debug(\sprintf('sql-notpdo-connected: [%s]', $this->config['name']));
        }

        return $this;
    }

    public function disconnect()
    {
        if (self::$watchRecursive) {
            if ($this->connection) {
                self::$lastAttempted = null;
            }
        }

        $current = self::$current;

        if (! $this->pdoResolver) {
            rescue(function () {
                if ($this->connection === null) {
                    self::callUserFunc('sybase_close');
                }
                else {
                    self::callUserFunc('sybase_close', $this->connection);
                }
            });
        }

        $this->connection = null;
        $this->statement = null;

        if (self::doLog()) {
            Log::debug(\sprintf('sql-notpdo-disconnected: [%s]', $current));
        }
    }

    public function execute($bindings = null)
    {
        $this->reconnect();

        $this->bindingsOriginal = ($bindings === null && \count($this->bindingValues))
            ? $this->bindingValues
            : $bindings;

        $this->bindings = $this->queryBind($this->bindingsOriginal);
        $this->sqlBound = $this->sqlCounterBinder($this->sql, $this->bindings);
        $this->statement = $this->runPreparedQuery($this->sql, $this->bindingsOriginal, $this->connection);

        $rowCount = $this->rowCount();

        $this->bindingValues = [];
        $this->bindingIndexes = [];
        $this->bindingTypes = [];

        return $rowCount;
    }

    public function fetch($type = null, $class = null)
    {
        if ($type === null) {
            $type = $this->fetchModes['arg1'];
        }

        return $this->sqlFetchAssocCall($type, $class);
    }

    public function fetchAll($type = null, $class = null)
    {
        // type array/assoc / both?
        // what : obj?

        if ($type === null) {
            $type = $this->fetchModes['arg1'];
        }

        $result = [];
        while ($v1 = $this->sqlFetchAssocCall($type, $class)) {
            $result[] = $v1;
        }

        return $result;
    }

    //

    public function init($statementClass, $config, $options)
    {
        $this->connection = null;
        $this->config = $config;
        $this->options = $options;
        $this->statementClass = $statementClass;
    }

    public function lastInsertId($seqname = null)
    {
        $this->statement = $this->runPreparedQuery('SELECT @@identity AS last_id', null, $this->connection);

        $result = $this->sqlFetchAssocBase();

        return $result->last_id;
    }

    public function prepare($sql, $options = null)
    {
        $this->sqlOriginal = $sql;
        $this->sql = $sql;

        $statementClass = $this->statementClass;

        return new $statementClass($this);
    }

    public function quote($value, $paramtype = null)
    {
        if (null === $value) {
            return 'null';
        }

        if ($value === '') {
            return "''";
        }

        if (\is_int($value)) {
            return $value;
        }

        if (\is_bool($value)) {
            return (int) $value;
        }

        return Helpers::withExceptionErrorHandler(static function () use ($value) {
            try {
                return "'" . \str_replace(
                    "'",
                    "''",
                    Helpers::removeInvisibleCharacters($value, false)
                ) . "'";
            }
            catch (Exception $e) {
                Helpers::reportIgnoredException('Weird error when removing invisible characters.', $e);

                return "'" . Database::quoteRemoveInvisibleCharacters($value) . "'";
            }
        });
    }

    public function rollBack()
    {
        $this->runUnpreparedQuery('ROLLBACK;', null, $this->connection);
    }

    public function rowCount()
    {
        if (null === $this->rowCount) {
            $this->rowCount = false;

            $this->rowCount = $this->getRowCount();
        }

        return $this->rowCount;
    }

    public function setFetchMode($a, $b)
    {
        $this->fetchModes['arg1'] = $a;
        $this->fetchModes['arg2'] = $b;
    }

    public function setPdoResolver($callback)
    {
        $this->pdoResolver = $callback;
    }

    protected function getRowCount()
    {
        return $this->pdoResolver
            ? $this->statement->rowCount()
            : self::callUserFunc('sybase_affected_rows', $this->connection);
    }

    protected function queryBind($bindings)
    {
        if ($bindings === null) {
            return [];
        }

        \array_walk($bindings, function (&$v1) {
            $v1 = $this->quote($v1);
        });

        return $bindings;
    }

    protected function reconnect()
    {
        $current = self::$current;

        $reconnected = false;

        if (self::doLog()) {
            Log::debug(\sprintf('sql-notpdo-reconnecting: [%s] -> [%s]', $current, $this->config['name']));
        }

        if ($current !== $this->config['name']) {
            $reconnected = true;

            if ($current !== null) {
                $this->disconnect();
                /** @var \Illuminate\Database\DatabaseManager $db */
                $db = app('db');
                $db->disconnect($current);
            }

            $this->connect();
            self::$current = $this->config['name'];
        }

        if (! $this->connection) {
            $reconnected = true;

            $this->connect();
        }

        if (self::doLog()) {
            if ($reconnected) {
                Log::debug(\sprintf('sql-notpdo-reconnect: true [%s] -> [%s]', $current, $this->config['name']));
            }
            else {
                Log::debug(\sprintf('sql-notpdo-reconnect: false [%s]', $current));
            }
        }

        return $this;
    }

    protected function runPreparedQuery($sql, $bindings, $connection)
    {
        if ($this->pdoResolver) {
            $statement = $connection->prepare($sql);

            $statement->execute($bindings);

            return $statement;
        }

        $bound = $this->sqlCounterBinder($sql, $this->queryBind($bindings));

        if (self::doLog() && ! app()->isProduction()) {
            Log::debug(\sprintf('sql-notpdo-prepared-bound-sql %s (%s): %s', $this->config['name'], $connection, $bound));
        }

        return self::callUserFunc('sybase_query', $bound, $connection);
    }

    protected function runUnpreparedQuery($sql, $bindings, $connection)
    {
        $bound = $this->sqlCounterBinder($sql, $this->queryBind($bindings));

        if (self::doLog() && ! app()->isProduction()) {
            Log::debug(\sprintf('sql-notpdo-unprepared-bound-sql %s (%s): %s', $this->config['name'], $connection, $bound));
        }

        return $this->pdoResolver
            ? $connection->exec($bound)
            : self::callUserFunc('sybase_query', $bound, $connection);
    }

    protected function sqlCounterBinder($sql, $bindings)
    {
        if (\count($bindings)) {
            $sqls = \explode('?', $sql);

            if (\count($bindings) !== \count($sqls) - 1) {
                throw new Exception('unable to bind query: ' . Database::naiveQueryBinder($sql, $bindings, $this));
            }

            $sql = '';
            $counter = 0;
            foreach ($bindings as $binding) {
                $sql .= ($sqls[$counter++] . $binding);
            }

            $sql .= $sqls[$counter];
        }

        return $sql;
    }

    protected function sqlFetchAssocBase()
    {
        return $this->pdoResolver
            ? $this->statement->fetch(PDO::FETCH_ASSOC)
            : self::callUserFunc('sybase_fetch_object', $this->statement);
    }

    protected function sqlFetchAssocCall($type = null, $class = null)
    {
        $result = $this->sqlFetchAssocBase();

        if (! $result) {
            return $result;
        }

        switch ($type) {
            case PDO::FETCH_ASSOC:
                return (array) $result;

            case PDO::FETCH_BOTH:
                return with((array) $result, static function ($result) {
                    $return = [];
                    $i = 0;
                    foreach ($result as $key => $value) {
                        $return[$key] = $value;
                        $return[$i++] = $value;
                    }

                    return $return;
                });

            case PDO::FETCH_NUM:
                return \array_values((array) $result);

            case PDO::FETCH_OBJ:
                return (object) $result;

            case PDO::FETCH_CLASS:
                throw new Exception('fetch with class cast not implemented');

            case PDO::FETCH_BOUND:
            case PDO::FETCH_INTO:
            case PDO::FETCH_LAZY:
            case PDO::FETCH_NAMED:
            default:
                throw new Exception('fetch mode ' . $type . ' not implemented');
        }
    }

    public static function env($config)
    {
        $temp = storage_path('tmp');

        $path = Helpers::temporaryFile($temp, 'ftds');
        \file_put_contents($path, self::sybaseFtdsConfig($config));
        \putenv("FREETDS={$path}");
        \putenv("FREETDSCONF={$path}");

        $path = Helpers::temporaryFile($temp, 'ftl');
        \file_put_contents($path, self::sybaseFtdsLocale($config));
        \putenv("LOCALESCONF={$path}");
    }

    public static function fixSybaseDate($date)
    {
        return LocaleIndonesia::fixSybaseDate($date);
    }

    public static function pdoResolver()
    {
        return static function ($instance) {
            try {
                if (self::doLog()) {
                    Log::debug('sql-notpdo-resolver: using pdo');
                }

                return new PDO(
                    "dblib:host={$instance->config['database']}",
                    $instance->config['username'],
                    $instance->config['password'],
                    isset($instance->options)
                        ? Arr::wrap($instance->options)
                        : [
                            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        ]
                );
            }
            catch (Exception $e) {
                throw new Exception(\sprintf('Connection exception on %s', $instance->config['name']), $e->getCode(), $e);
            }
        };
    }

    protected static function functionResolver()
    {
        return static function ($instance) {
            try {
                if (self::doLog()) {
                    Log::debug('sql-notpdo-resolver: using sybase_connect');
                }

                return self::callUserFunc(
                    'sybase_connect',
                    $instance->config['database'],
                    $instance->config['username'],
                    $instance->config['password']
                );
            }
            catch (Exception $e) {
                throw new Exception(\sprintf('Connection exception on %s', $instance->config['name']), $e->getCode(), $e);
            }
        };
    }

    protected static function sybaseFtdsConfig($config)
    {
        return \implode("\n", [
            '[global]',
            \sprintf("\ttext size = %s", isset($config['text size']) ? $config['text size'] : '64512'),
            isset($config['dump file']) ? ("\tdump file = {$config['dump file']}") : '',
            '',

            isset($config['database']) ? "[{$config['database']}]" : '',
            isset($config['host']) ? ("\thost = {$config['host']}") : '',
            isset($config['port']) ? ("\tport = {$config['port']}") : '',
            '',

            isset($config['version']) ? ("\ttds version = {$config['version']}") : 'tds version = 5.0',
            '',
        ]);
    }

    protected static function sybaseFtdsLocale($config)
    {
        return \implode("\n", [
            '[default]',
            \sprintf(
                "\tdate format = %s",
                isset($config['date format'])
                    ? $config['date format']
                    : '%b %e %Y %I:%M:%S:%z%p'
            ),
            '',

            '[en_US]',
            \sprintf(
                "\tdate format = %s",
                isset($config['date format-en_US'])
                    ? $config['date format-en_US']
                    : '%b %e %Y %I:%M:%S:%z%p'
            ),
            \sprintf(
                "\tlanguage = %s",
                isset($config['language-en_US'])
                    ? $config['language-en_US']
                    : 'us_english'
            ),
            \sprintf(
                "\tcharset = %s",
                isset($config['charset-en_US'])
                    ? $config['charset-en_US']
                    : 'iso_1'
            ),
            '',
        ]);
    }

    private static function callUserFunc($function, ...$parameters)
    {
        return $function(...$parameters);
    }
}
