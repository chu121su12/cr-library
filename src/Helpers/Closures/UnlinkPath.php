<?php

namespace CR\Library\Helpers\Closures;

class UnlinkPath
{
    private $path;

    private $unsafe;

    public function __construct($path, $unsafe)
    {
        $this->path = $path;

        $this->unsafe = $unsafe;
    }

    public function __invoke()
    {
        if ($this->unsafe || (\file_exists($this->path) && \is_file($this->path))) {
            \unlink($this->path);
        }
    }
}
