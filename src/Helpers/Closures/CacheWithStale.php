<?php

namespace CR\Library\Helpers\Closures;

use Closure;
use CR\Library\Helpers\Helpers;
use Illuminate\Support\Facades\Cache;

class CacheWithStale
{
    private $keyStale;

    private $keyFresh;

    private $ttlFresh;

    private $ttlStale;

    private $callback;

    private $lock;

    private $store;

    private $sync;

    public function __construct($key, $ttlFresh, $ttlStale, Closure $callback, $blocks = true, $store = null, $sync = false)
    {
        $this->ttlFresh = $ttlFresh;

        $this->ttlStale = $ttlStale;

        $this->callback = $callback;

        $this->keyStale = '_cache-stale:::' . $key;

        $this->keyFresh = '_cache-fresh:::' . $key;

        $this->store = $store;

        $lockStore = \is_string($blocks) ? $blocks : $store;

        $this->lock = $blocks ? Cache::store($lockStore)->lock('_cache-lock:::' . $key, $ttlFresh) : optional();

        $this->sync = $sync;
    }

    private function remember($get)
    {
        try {
            $this->lock->block($this->ttlFresh);

            $store = Cache::store($this->store);

            $value = $get ? $store->get($this->keyStale) : null;

            if ($value === null) {
                $value = value($this->callback);

                $store->put($this->keyStale, $value, $this->ttlStale);

                $store->put($this->keyFresh, 1, $this->ttlFresh);
            }

            return $value;
        }
        finally {
            $this->lock->release();
        }
    }

    public function run()
    {
        $cached = $this->remember(true);

        if (! Cache::store($this->store)->get($this->keyFresh)) {
            if ($this->sync) {
                $this->remember(false);
            }
            else {
                Helpers::safeDispatchAfterResponse(function () {
                    $this->remember(false);
                });
            }
        }

        return $cached;
    }
}
