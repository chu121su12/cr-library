<?php

namespace CR\Library\Helpers\Closures;

use CR\Library\Helpers\Helpers;
use Exception;
use Illuminate\Support\Facades\Log;
use Throwable;

class DispatchPayload
{
    private $callback;

    private $cancel;

    private $context;

    private $trace;

    public function __construct($callback, $trace, $context)
    {
        $this->cancel = new DispatchCancel;

        $this->callback = $callback;

        $this->context = $context;

        $this->trace = $trace;

        Log::debug('-dispatch: register', $context);
    }

    public function __invoke()
    {
        if ($this->cancel->check()) {
            Log::debug('-dispatch: cancelled', $this->context);

            return;
        }

        $this->cancel->cancel();

        Log::debug('-dispatch: starting', $this->context);

        try {
            $callback = $this->callback;

            $callback([
                '__cr_task_id' => $this->context['__cr_task_id'],
            ]);
        }
        catch (Exception $e) {
        }
        catch (Throwable $e) {
        }

        if (isset($e)) {
            Log::debug('-dispatch: exception thrown: ' . $e->getMessage(), $this->context);

            Helpers::reportIgnoredException("Exception on dispatched task ({$this->context['__cr_task_id']}).", $e);

            Helpers::reportExceptionStack($this->trace);
        }
        else {
            Log::debug('-dispatch: finished', $this->context);
        }
    }

    public function getCancellation()
    {
        return $this->cancel;
    }
}
