<?php

namespace CR\Library\Helpers\Closures;

class DispatchCancel
{
    private $cancelToken = false;

    public function __invoke()
    {
        if ($this->cancelToken === true) {
            return false;
        }

        return $this->cancelToken = true;
    }

    public function check()
    {
        return $this->cancelToken;
    }

    public function cancel()
    {
        $this->cancelToken = true;
    }
}
