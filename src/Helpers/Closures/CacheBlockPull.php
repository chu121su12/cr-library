<?php

namespace CR\Library\Helpers\Closures;

use Illuminate\Support\Facades\Cache;

class CacheBlockPull
{
    private $evicted = false;

    private $key;

    public function __construct($key)
    {
        $this->key = $key;
    }

    public function __invoke()
    {
        if (! $this->evicted) {
            $this->evicted = true;

            Cache::pull($this->key);
        }
    }
}
