<?php

namespace CR\Library\Helpers\Closures;

class Noop
{
    private $return;

    public function __construct($return)
    {
        $this->return = $return;
    }

    public function __invoke()
    {
        return $this->return;
    }

    public static function closure($return)
    {
        return static function () use ($return) {
            return $return;
        };
    }
}
