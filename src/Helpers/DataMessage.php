<?php

namespace CR\Library\Helpers;

use Brick\Math\BigDecimal;
use Brick\Math\BigInteger;
use Brick\Money\Money;
use Carbon\CarbonImmutable;
use Closure;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\Stringable;
use RuntimeException;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * @method ?array array(?string $key, mixed $fallback = null)
 * @method ?string string(?string $key, mixed $fallback = null)
 */
class DataMessage
{
    use Patch\DataMessageGetter;

    protected $source;

    protected $lastValidationDefinition = [];

    /**
     * @param  mixed $source
     */
    public function __construct($source)
    {
        $this->source = value($source);
    }

    /**
     * @param  string $key
     * @return LazyCollection
     */
    public function collectArray($key)
    {
        return new LazyCollection(function () use ($key) {
            return $this->getArray($key, []);
        });
    }

    /**
     * @param  string $key
     * @param  ?string $fromFormat
     * @return CarbonImmutable
     */
    public function dateTime($key, $fromFormat = null)
    {
        $value = $this->getString($key, null);

        if ($value === null) {
            return null;
        }

        if ($fromFormat === null) {
            return new CarbonImmutable($value);
        }

        return CarbonImmutable::createFromFormat($fromFormat, $value);
    }

    /**
     * @param  string $key
     * @return BigDecimal
     */
    public function decimal($key)
    {
        return BigDecimal::of($this->getString($key, null));
    }

    /**
     * @param  string $key
     * @return BigInteger
     */
    public function integer($key)
    {
        return BigInteger::of($this->getString($key, null));
    }

    /**
     * @param  string $key
     * @param  string $currency
     * @return Money
     */
    public function money($key, $currency)
    {
        return Money::of($this->getString($key, null), $currency);
    }

    /**
     * @param  string $key
     * @param  string $fallback
     * @return Stringable
     */
    public function str($key, $fallback = '')
    {
        return new Stringable($this->getString($key, $fallback));
    }

    /**
     * @param  string $key
     * @return int
     */
    public function castInt($key, $fallback = 0)
    {
        return (int) $this->getString($key, $fallback);
    }

    /**
     * @param  string $key
     * @return string
     */
    public function castStr($key, $fallback = '')
    {
        return (string) $this->getString($key, $fallback);
    }

    /**
     * @param  array<string> $keys
     * @return array<string, ?string>
     */
    public function strings(...$keys)
    {
        $values = [];

        foreach ($keys as $key) {
            $values[$key] = $this->getString($key, null);
        }

        return $values;
    }

    /**
     * @param  array<string> $keys
     * @return array<string, ?string>
     */
    public function stringsValidated(...$keys)
    {
        $keys = \func_num_args() === 0 ? \array_keys($this->lastValidationDefinition) : $keys;

        return $this->strings(...$keys);
    }

    /**
     * @param  array<string> $keys
     * @return string
     */
    public function encrypts(...$keys)
    {
        return Crypt::encrypt($this->strings(...$keys));
    }

    /**
     * @param  string $key
     * @return self
     */
    public function decrypt($key)
    {
        return new self(static::decryptArray($key));
    }

    /**
     * @param  string $key
     * @return array<string|int, mixed>
     */
    public function decryptArray($key)
    {
        return Helpers::arrayify(Crypt::decrypt($this->getString($key, null)), []);
    }

    /**
     * @return Collection
     */
    public function lastValidationDefinition()
    {
        return new Collection($this->lastValidationDefinition);
    }

    /**
     * @param  array<string> $skips
     * @return static
     */
    public function validate(array $definition, ...$skips)
    {
        $this->validateWithAllKeys($skips, $definition);

        return $this;
    }

    /**
     * @return static
     */
    public function validateAny(array $definition)
    {
        $this->validateWithAllKeys(false, $definition);

        return $this;
    }

    /**
     * @param  array<string> $skips
     * @return array
     */
    public function validated(array $definition, ...$skips)
    {
        return $this->validateWithAllKeys($skips, $definition)->validated();
    }

    /**
     * @return array
     */
    public function validatedAny(array $definition)
    {
        return $this->validateWithAllKeys(false, $definition)->validated();
    }

    /**
     * @param  ?string $key
     * @return mixed
     */
    public function get($key = null, $default = null)
    {
        return $key === null ? $this->source() : data_get($this->source(), $key, $default);
    }

    /**
     * @return mixed
     */
    public function source()
    {
        return $this->source;
    }

    /**
     * @param  ?string $key
     * @param  mixed $fallback
     * @return ?array
     */
    protected function getArray($key, $fallback)
    {
        return Helpers::arrayify($this->get($key), $fallback);
    }

    /**
     * @param  string $key
     * @param  mixed $fallback
     * @return ?string
     */
    protected function getString($key, $fallback)
    {
        return Helpers::stringify($this->get($key), $fallback);
    }

    /**
     * @param  false|array<string> $withKeys
     * @return CastingValidator
     */
    private function validateWithAllKeys($withKeys, array $definition)
    {
        $this->lastValidationDefinition = $definition;

        $validator = CastingValidator::make($this->get(null), $definition);

        $validator->validate();

        if (\is_array($withKeys)) {
            CastingValidator::once(
                (new Collection(Helpers::arrayify($this->get(null), [])))
                    ->diffKeys($definition)
                    ->diffKeys(\array_flip($withKeys)),
                static function ($_attribute, $values, Closure $fail) {
                    if ($values->isNotEmpty()) {
                        $types = $values
                            ->map(static function ($value, $key) {
                                return \sprintf('%s (%s)', $key, \gettype($value));
                            })
                            ->join('; ');

                        $fail("The following keys not validated: {$types}.");
                    }
                }
            );
        }

        return $validator;
    }

    /**
     * @param  mixed $data
     * @return ?string
     */
    private static function stringifyMixed($data)
    {
        return Helpers::stringify($data);
    }

    /**
     * @param  Response|Request $request
     * @param  false|array<string> $withKeys
     * @return static
     */
    public static function fromHttpMessage($request)
    {
        if (! ($request instanceof Request || $request instanceof Response)) {
            throw new RuntimeException;
        }

        $json = $request->json();

        if ($json instanceof ParameterBag) {
            $json = $json->all(null);
        }

        return new static((array) $json);
    }
}
