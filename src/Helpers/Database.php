<?php

namespace CR\Library\Helpers;

use DateTimeInterface;

class Database
{
    public static function bindString($times = 1)
    {
        return \implode(', ', \array_fill(0, $times, '?'));
    }

    public static function generateUpdateSetPreparedQuery($array)
    {
        $set = [];
        foreach (\array_keys($array) as $key) {
            $set[] = "[{$key}] = ?";
        }

        return \implode(', ', $set);
    }

    public static function naiveQueryBinder($sql, $bindings = [], $pdo = null)
    {
        if ($bindingCount = \count($bindings)) {
            $sqlParts = \explode('?', $sql);

            if (\count($sqlParts) !== $bindingCount + 1) {
                return $sql . ' -- cannot bind different ? count -- ' . \json_encode($bindings);
            }

            $counter = 0;
            $newSql = '';

            foreach ($bindings as $binding) {
                $value = '';
                $needQuote = false;

                if ($binding === null) {
                    $value = 'null';
                }
                elseif (\is_int($binding)) {
                    $value = (string) $binding;
                }
                elseif (\is_scalar($binding)) {
                    $value = $binding;
                    $needQuote = true;
                }
                elseif ($binding instanceof DateTimeInterface) {
                    $value = $binding->format('Y-m-d H:i:s.u');
                    $needQuote = true;
                }
                elseif (\is_object($binding) && \method_exists($binding, '__toString')) {
                    $value = (string) $binding;
                    $needQuote = true;
                }
                else {
                    return $sql . ' -- cannot bind non-stringable value -- ' . \json_encode($bindings);
                }

                if ($needQuote) {
                    if (null === $pdo) {
                        return $sql . ' -- cannot bind without pdo instance -- ' . \json_encode($bindings);
                    }

                    $value = $pdo->quote($value);
                }

                $newSql .= ($sqlParts[$counter++] . $value);
            }

            $sql = $newSql . $sqlParts[$counter];
        }

        return $sql;
    }

    public static function quoteRemoveInvisibleCharacters($value)
    {
        return \str_replace(
            ['\\', "\0", "\n", "\r", "'", '"', "\x1a"],
            ['\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'],
            $value
        );
    }
}
