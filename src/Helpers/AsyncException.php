<?php

namespace CR\Library\Helpers;

use Exception;

class AsyncException extends Exception
{
    public $others;

    public function __construct($previous, array $others = [])
    {
        parent::__construct($previous->getMessage(), $previous->getCode(), $previous);

        $this->others = $others;
    }
}
