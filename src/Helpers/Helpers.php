<?php

namespace CR\Library\Helpers;

use Brick\Math\BigDecimal;
use Brick\Math\BigNumber;
use Brick\Math\RoundingMode;
use Brick\Money\Money;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;
use Closure;
use CR\Library\Helpers\Closures\CacheBlockPull;
use CR\Library\Helpers\Closures\CacheWithStale;
use CR\Library\Helpers\Closures\DispatchPayload;
use CR\Library\Helpers\Closures\Noop;
use CR\Library\Helpers\Closures\UnlinkPath;
use DateTimeInterface;
use Error;
use Exception;
use Icewind\SMB\BasicAuth as SmbBasicAuth;
use Icewind\SMB\Options as SmbOptions;
use Icewind\SMB\ServerFactory as SmbServerFactory;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Http\Request;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Mail\Events\MessageSent;
use Illuminate\Mail\SentMessage;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\ConfigurationUrlParser;
use Illuminate\Support\Enumerable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;
use InvalidArgumentException;
use JsonSerializable;
use MatthiasMullie\Minify\CSS as CSSMinifier;
use MatthiasMullie\Minify\JS as JSMinifier;
use PhpOffice\PhpSpreadsheet\Cell\DataType as PopsDataType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date as PopsDate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat as PopsNumberFormat;
use RuntimeException;
use SplFileInfo;
use Stringable;
use Swift_Attachment as SwiftAttachment;
use Swift_Mailer as SwiftMailer;
use Swift_Message as SwiftMessage;
use Swift_SmtpTransport as SwiftSmtpTransport;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\SentMessage as SymfonySentMessage;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Throwable;
use Traversable;
use UAParser\Parser as UAParser;
use UnitEnum;
use Verdant\Array2XML;
use Verdant\XML2Array;

class Helpers
{
    const FOREVER_SECONDS = 60 * 60 * 24 * 366 * 5;

    public static $startTime;

    private static $appMixManifests = [];

    private static $eventDispatcher;

    public static function appMix($path, $manifestDirectory = '')
    {
        if (! \str_starts_with($path, '/')) {
            $path = "/{$path}";
        }

        if ($manifestDirectory && ! \str_starts_with($manifestDirectory, '/')) {
            $manifestDirectory = "/{$manifestDirectory}";
        }

        if (\file_exists(public_path($manifestDirectory . '/hot'))) {
            return new HtmlString("//localhost:8080{$path}");
        }

        $manifestPath = public_path($manifestDirectory . '/mix-manifest.json');

        if (! isset(self::$appMixManifests[$manifestPath])) {
            if (! \file_exists($manifestPath)) {
                throw new Exception('The Mix manifest does not exist.');
            }

            self::$appMixManifests[$manifestPath] = \json_decode(\file_get_contents($manifestPath), true);
        }

        $manifest = self::$appMixManifests[$manifestPath];

        if (! isset($manifest[$path])) {
            throw new Exception("Unable to locate Mix file: {$path}. Please check your webpack.mix.js output paths and try again.");
        }

        return new HtmlString($manifestDirectory . $manifest[$path]);
    }

    public static function arrayify($items, $fallback = null)
    {
        if (\is_array($items)) {
            return $items;
        }

        if ($items instanceof Enumerable) {
            return $items->all();
        }

        if ($items instanceof Arrayable) {
            return $items->toArray();
        }

        if ($items instanceof Traversable) {
            return iterator_to_array($items);
        }

        if ($items instanceof Jsonable) {
            return backport_json_decode($items->toJson(), true);
        }

        if ($items instanceof JsonSerializable) {
            return (array) $items->jsonSerialize();
        }

        if ($items instanceof UnitEnum) {
            return [$items];
        }

        return $fallback === null ? null : self::arrayify($fallback);
    }

    public static function arrayMergeRecursiveDistinct(array &$array1, array &$array2)
    {
        $merged = $array1;
        $value = null;
        foreach ($array2 as $key => &$value) {
            if (\is_array($value) && isset($merged[$key]) && \is_array($merged[$key])) {
                $merged[$key] = self::arrayMergeRecursiveDistinct($merged[$key], $value);
            }
            else {
                $merged[$key] = $value;
            }
        }

        return $merged;
    }

    public static function arraySign($parameters = [], $once = false, $expiration = null)
    {
        $parameters = Arr::wrap($parameters);

        if (isset($parameters['_signature']) || isset($parameters['_nonce']) || isset($parameters['_expires'])) {
            throw new Exception('Unable to sign data with reserved key.');
        }

        if ($expiration) {
            $expiration = \is_int($expiration)
                ? self::microTime()->addRealSeconds($expiration)
                : $expiration;

            $parameters['_expires'] = $expiration->getTimestamp();
        }

        $signature = with($parameters, static function ($toSign) {
            \ksort($toSign);

            return \hash_hmac(
                'sha256',
                \serialize($toSign),
                Config::get('app.key')
            );
        });

        $parameters['_signature'] = $signature;

        if ($once) {
            $nonce = Str::random(30);

            Cache::put("_cache-array-sign/{$signature}", $nonce, self::microTime()->addDays(30));

            $parameters['_nonce'] = $nonce;
        }

        return $parameters;
    }

    public static function arraySignVerify($parameters = [], $once = false)
    {
        $parameters = Arr::wrap($parameters);

        $signature = with($parameters, static function ($toVerify) {
            unset($toVerify['_signature'], $toVerify['_nonce']);

            \ksort($toVerify);

            return \hash_hmac(
                'sha256',
                \serialize($toVerify),
                Config::get('app.key')
            );
        });

        $hashEquals = \hash_equals($signature, (string) data_get($parameters, '_signature', ''));

        $expires = data_get($parameters, '_expires');

        $timestamp = self::microTime()->getTimestamp();

        $expired = $expires && $timestamp > $expires;

        $nonce = data_get($parameters, '_nonce');

        $storedNonce = (string) Cache::pull('_cache-array-sign/{$signature}');

        $revoked = $once && $storedNonce !== (string) $nonce;

        if ($hashEquals && ! $expired && ! $revoked) {
            unset($parameters['_signature'], $parameters['_nonce'], $parameters['_expires']);

            return $parameters;
        }

        throw new RuntimeException('Invalid signature');
    }

    public static function arrayThreeWaySplit($existing, $updates, $strict = false)
    {
        $unchanged = [];
        $deleted = [];

        $existing = Arr::wrap($existing);
        $updates = Arr::wrap($updates);

        foreach ($existing as $item) {
            if (\in_array($item, $updates, $strict)) {
                $unchanged[] = $item;
            }
            else {
                $deleted[] = $item;
            }
        }

        $added = [];
        foreach ($updates as $item) {
            if (! \in_array($item, $existing, $strict)) {
                $added[] = $item;
            }
        }

        return [
            'unchanged' => $unchanged,
            'added' => $added,
            'deleted' => $deleted,
        ];
    }

    public static function arrayThreeWaySplitKey($existing, $updates, $strict = false)
    {
        return self::arrayThreeWaySplit(
            \array_keys($existing),
            \array_keys($updates),
            $strict
        );
    }

    public static function bladeRender($template, $data = [], $random = true)
    {
        $path = storage_path('framework/views2');

        $filename = $random ? Str::random() : \sha1($template);

        $filepath = \sprintf('%s/%s.blade.php', $path, $filename);

        if (! \file_exists($filepath)) { // | mtime > now - 1 jam
            View::addLocation($path);

            \file_put_contents($filepath, \trim($template));

            self::unlinkLater($filepath);
        }

        return View::make($filename, $data)->render();
    }

    public static function bulkDbInsert($query, array $inserts, $limit = null)
    {
        $limit = (2 ** 16) - 1;
        $bulk = [];
        $approximateLength = 0;
        $results = [];

        foreach ($inserts as $insert) {
            $insertLength = \mb_strlen(\json_encode($insert, \JSON_UNESCAPED_UNICODE));

            if (\count($bulk) && $approximateLength + $insertLength > $limit) {
                $results[] = $query->insert($bulk);
                $bulk = [];
                $approximateLength = 0;
            }

            $bulk[] = $insert;
            $approximateLength += $insertLength;
        }

        if (\count($bulk)) {
            $results[] = $query->insert($bulk);
        }

        return $results;
    }

    public static function dateWrapOrCreateFromFormat($format, $date, $timestamp = null)
    {
        if ($date instanceof CarbonInterface) {
            return $date->toImmutable();
        }

        if ($date instanceof DateTimeInterface) {
            return new CarbonImmutable($date);
        }

        if ($date) {
            return CarbonImmutable::createFromFormat($format, $date);
        }
    }

    public static function dateWrapOrCreateFromDatabaseTimestamp($date)
    {
        if ($date instanceof CarbonInterface) {
            return $date->toImmutable();
        }

        if ($date instanceof DateTimeInterface) {
            return new CarbonImmutable($date);
        }

        if (! \is_string($date)) {
            return CarbonImmutable::parse($date);
        }

        try {
            return new CarbonImmutable($date);
        }
        catch (Exception $e) {
        }

        try {
            $micro = \mb_substr($date, 21, 3) . ' ';

            return CarbonImmutable::createFromFormat(
                'M j Y h:i:s:u A',
                \trim(\substr_replace($date, $micro, 21, 3))
            );
        }
        catch (Exception $e) {
        }
    }

    public static function dbDisconnect($connection, $callback = null)
    {
        $connection = \is_string($connection) ? DB::connection($connection) : $connection;

        $result = $callback ? value($callback, $connection) : $callback;

        $pdo = $connection->getPdo();

        if (\method_exists($pdo, 'disconnect')) {
            $pdo->disconnect();
        }

        $connection->disconnect();

        return $result;
    }

    public static function disk($driver = 'local')
    {
        return Storage::disk($driver);
    }

    public static function cacheBlockAdd($key, $callback, $ttl = null, $lockSeconds = 600)
    {
        $lock = Cache::lock('_cache-block:::' . $key, $lockSeconds + 1);

        try {
            $lock->block($lockSeconds);

            if (Cache::has($key)) {
                return false;
            }

            if ($ttl) {
                Cache::put($key, value($callback), $ttl);
            }
            else {
                Cache::put($key, value($callback));
            }

            return new CacheBlockPull($key);
        }
        finally {
            $lock->release();
        }
    }

    public static function cacheRemember($key, $ttl, Closure $callback)
    {
        $value = Cache::get($key);

        if (! is_null($value)) {
            return $value;
        }

        $value = $callback();

        try {
            Cache::put($key, $value, value($ttl, $value));
        }
        catch (Exception $e) {
            self::reportIgnoredException('Error populating cache.', $e);
        }
        finally {
            return $value;
        }
    }

    /** @deprecated */
    public static function cacheWithStale($key, $ttlFresh, $ttlStale, Closure $callback)
    {
        return (new CacheWithStale($key, $ttlFresh, $ttlStale, $callback))->run();
    }

    public static function checkPrivateIp($ipAddress)
    {
        if (\is_string($ipAddress)) {
            if (\preg_match('/^\d{1,3}(\.\d{1,3}){1,3}$/', $ipAddress)) {
                $ipParts = \explode('.', $ipAddress);

                $check = \count($ipParts) === 2 ? 0 : (int) $ipParts[1];

                switch ((int) $ipParts[0]) {
                    case 10: case 127: return true;
                    case 172: return $check >= 16 && $check < 32;
                    case 192: return $check === 168;
                    case 169: return $check === 254;
                }

                return false;
            }

            if (\str_contains($ipAddress, ':')) {
                $ipParts = \explode(':', $ipAddress);
                $firstBlock = $ipParts[0];
                $prefix = \substr($firstBlock, 0, 2);

                return (bool) (
                    \strcasecmp($firstBlock, 'fe80') == 0
                    || \strcasecmp($firstBlock, '100') == 0
                    || (\strcasecmp($prefix, 'fc') == 0 && \strlen($firstBlock) >= 4)
                    || (\strcasecmp($prefix, 'fd') == 0 && \strlen($firstBlock) >= 4)
                );
            }
        }

        throw new RuntimeException('Unable to parse IP address');
    }

    public static function checkSocket($host, $port, $timeout = 30)
    {
        return self::withExceptionErrorHandler(static function () use ($host, $port, $timeout) {
            try {
                $errorCode = null;
                $errorMessage = null;

                $socket = \fsockopen($host, $port, $errorCode, $errorMessage, $timeout);

                if ($socket && \is_resource($socket)) {
                    return true;
                }
            }
            finally {
                if (isset($socket) && $socket) {
                    try {
                        \fclose($socket);
                    }
                    catch (Exception $_e) {
                    }
                    catch (Throwable $_e) {
                    }
                }
            }

            return false;
        });
    }

    public static function coalesce($iterable)
    {
        if ($iterable !== null) {
            foreach ($iterable as $value) {
                if ($value !== null) {
                    return $value;
                }
            }
        }
    }

    public static function convertArrayToXml($array, array $config = [])
    {
        return Array2XML::createXML($array, $config)->saveXML();
    }

    public static function convertXmlToArray($xml, array $config = [])
    {
        return XML2Array::createArray($xml, $config);
    }

    public static function ensureNoDirectoryClimb($subPath)
    {
        if (\str_contains($subPath, '..')) {
            throw new RuntimeException('Subpath may be climbing directory tree');
        }

        return $subPath;
    }

    public static function excelAoa(array $aoa, $format = 'xlsx')
    {
        // 'Xlsx', 'Xls', 'Ods', 'Html', 'Pdf', 'Csv',

        $spreadsheet = tap(new Spreadsheet, static function ($spreadsheet) use ($aoa) {
            $columnLetter = static function ($oneBasedNumber) {
                $letter = '';
                while ($oneBasedNumber) {
                    $temp = --$oneBasedNumber % 26;
                    $letter .= \chr($temp + 65);
                    $oneBasedNumber = ($oneBasedNumber - $temp) / 26;
                }

                return \strrev($letter);
            };

            $sheet = $spreadsheet->setActiveSheetIndex(0);
            $sheet->setTitle('Sheet1');

            $maxColumn = 0;
            $rowCount = 0;
            foreach ($aoa as $rowData) {
                ++$rowCount;
                $columnIndex = 0;
                foreach ($rowData as $cellData) {
                    $columnCount = ++$columnIndex;
                    $cellReference = $columnLetter($columnCount) . $rowCount;
                    $callback = \is_callable($cellData) ? $cellData : static function () use ($cellData, $sheet, $cellReference) {
                        $cell = $sheet->getCell($cellReference);

                        if ($cellData instanceof DateTimeInterface) {
                            $date = new CarbonImmutable($cellData);
                            $cell->setValue(PopsDate::PHPToExcel($date->format('U')));
                            $sheet->getStyle($cellReference)
                                ->getNumberFormat()
                                ->setFormatCode(
                                    PopsNumberFormat::FORMAT_DATE_DATETIME
                                );
                        }
                        elseif ($cellData instanceof BigNumber) {
                            $cell->setValue((string) $cellData);
                            $sheet->getStyle($cellReference)
                                ->getNumberFormat()
                                ->setFormatCode(
                                    PopsNumberFormat::FORMAT_NUMBER
                                );
                        }
                        elseif (\is_string($cellData)) {
                            $cell->setValueExplicit($cellData, PopsDataType::TYPE_STRING);
                        }
                        elseif (\is_int($cellData)) {
                            $cell->setValueExplicit($cellData, PopsDataType::TYPE_NUMERIC);
                        }
                        else {
                            try {
                                $cell->setValue($cellData);
                            }
                            catch (Exception $e) {
                                $cell->setValue((string) $cellData);
                            }
                        }
                    };

                    value($callback, $sheet, $cellReference, $columnCount, $rowCount);
                }
                $maxColumn = \max($maxColumn, $columnIndex);
            }

            for ($column = $maxColumn; $column >= 0; --$column) {
                $sheet->getColumnDimensionByColumn($column)->setAutoSize(true);
            }
        });

        return tap(self::temporaryFile(), static function ($path) use ($format, $spreadsheet) {
            $writer = IOFactory::createWriter($spreadsheet, \ucfirst(\strtolower($format)));
            $writer->save($path);
        });
    }

    public static function forkEach(array $items, callable $handler, $concurrency = 1)
    {
        return (new ForkEach($handler))->concurrency($concurrency)->runWithOutput($items);
    }

    public static function hashing($value, $config = null)
    {
        if (\is_string($config)) {
            return Hash::check($value, $config);
        }

        return Hash::make($value, Arr::wrap($config));
    }

    public static function hashObject($object)
    {
        return \str_replace(['/', '+', '='], '', \base64_encode(\md5(\spl_object_hash($object), true)));
    }

    public static function haversineLatLong($latitude1, $longitude1, $latitude2, $longitude2, $radiusInKm = 6371) // earth by default
    {
        $deltaLatitude = \deg2rad($latitude2 - $latitude1);

        $deltaLongitude = \deg2rad($longitude2 - $longitude1);

        $a
            = \sin($deltaLatitude / 2) * \sin($deltaLatitude / 2)
            + \cos(\deg2rad($latitude1)) * \cos(\deg2rad($latitude2))
            * \sin($deltaLongitude / 2) * \sin($deltaLongitude / 2);

        return $radiusInKm * 2 * \atan2(\sqrt($a), \sqrt(1 - $a));
    }

    public static function httpServerRequest($jsonRaw, array $headers = [])
    {
        $request = Request::capture();

        $request->setJson(new InputBag((array) json_decode($jsonRaw, true)));

        $request->headers = new HeaderBag($headers);

        return $request;
    }

    public static function langNumberToRomanNumeral($number)
    {
        $number = (int) $number;
        $chars = 'IVXLCDM';
        $result = '';

        for ($a = 5, $b = 0; $number; $b++, $a ^= 7) {
            $order = $number % $a;
            $number = $number / $a ^ 0;

            while ($order--) {
                $index = $order > 2 ? $b + $number - ($number &= -2) + $order = 1 : $b;
                $result = $chars[$index] . $result;
            }
        }

        return $result;
    }

    public static function microTime($timezone = null)
    {
        list($mtSecond, $mtSub) = self::microTimeData();
        $time = $mtSecond . $mtSub;

        return $timezone
            ? CarbonImmutable::createFromTimestamp($time, $timezone)
            : CarbonImmutable::createFromTimestamp($time);
    }

    public static function microTimeData()
    {
        $microtime = \microtime(false);
        $space = \strpos($microtime, ' ');

        return [
            \substr($microtime, $space + 1),
            \substr($microtime, 1, $space - 1 /* +2 */),
            $microtime,
        ];
    }

    public static function minifyCss($content)
    {
        return \class_exists(CSSMinifier::class)
            ? \trim((new CSSMinifier)->add($content)->minify())
            : $content;
    }

    public static function minifyHtml($content)
    {
        return \trim((new TinyHtmlMinifier([]))->minify($content));
    }

    public static function minifyJs($content)
    {
        return \class_exists(JSMinifier::class)
            ? \trim((new JSMinifier)->add($content)->minify())
            : $content;
    }

    public static function minifyJsTemplateHtml($content)
    {
        if (\class_exists(JSMinifier::class) && \class_exists(CSSMinifier::class)) {
            // minify css preflight
            $content = \preg_replace_callback('/\B_css:\s*`([^`]++)`/', static function ($matches) {
                return 'template:`' . self::minifyCss($matches[1]) . '`';
            }, $content);

            // template - expand self-close element
            $content = \preg_replace_callback('/\btemplate:\s*`([^`]++)`/', static function ($matches) {
                return 'template:`' . \preg_replace_callback('/<([-\d\w]+)([^>]*)(?<=\/)>/', static function ($matches) {
                    return '<' . $matches[1] . \mb_substr($matches[2], 0, -1) . '></' . $matches[1] . '>';
                }, $matches[1]) . '`';
            }, $content);

            // template - minify html
            $content = \preg_replace_callback('/\btemplate:\s*`([^`]++)`/', static function ($matches) {
                return 'template:`' . self::minifyHtml($matches[1]) . '`';
            }, $content);

            // minify js
            $content = self::minifyJs($content);

            // template - remove self-close closing
            $content = \preg_replace_callback('/\btemplate:\s*`([^`]++)`/', static function ($matches) {
                return 'template:`' . \preg_replace('/<([-\d\w]+)([^>]*)(?<!\/)><\/\1>/', '<$1$2/>', $matches[1]) . '`';
            }, $content);

            return $content;
        }

        return \preg_replace_callback('/template:\s*`([^`]++)`/', static function ($matches) {
            return 'template:`' . self::minifyHtml($matches[1]) . '`';
        }, $content);
    }

    public static function noopFunction($return = null)
    {
        return Noop::closure($return);
    }

    public static function normalizeBcryptHashBeforeChecking($hash)
    {
        $hash = isset($hash) ? $hash : '';

        if (Config::get('hashing.bcrypt.verify') && str_starts_with($hash, '$2')) {
            return \preg_replace('/^[$]2[abx]?[$]/', '\$2y\$', $hash) ?: $hash;
        }

        return $hash;
    }

    public static function numericFormat($formattings, $number)
    {
        if ($number instanceof Money) {
            $number = $number->getAmount();
        }

        $number = BigDecimal::of($number);

        if ($number->isNegative()) {
            return '-' . self::numericFormat($formattings, $number->negated());
        }

        $fractionDigits = data_get($formattings, 'FRACTION_DIGITS');

        if ($fractionDigits !== null) {
            $roundingMode = data_get($formattings, 'ROUNDING_MODE', RoundingMode::UNNECESSARY);
            $number = $number->toScale($fractionDigits, $roundingMode);
        }

        $groupingSize = (int) data_get($formattings, 'GROUPING_SIZE', 3);
        $groupingSeparatorSymbol = \strrev((string) data_get($formattings, 'GROUPING_SEPARATOR_SYMBOL', ','));

        $groups = [];
        foreach (\str_split(\strrev($number->getIntegralPart()), $groupingSize) as $group) {
            if (\count($groups) !== 0) {
                $groups[] = $groupingSeparatorSymbol;
            }
            $groups[] = $group;
        }

        if ((int) $fractionDigits > 0) {
            $decimalSeparatorSymbol = (string) data_get($formattings, 'DECIMAL_SEPARATOR_SYMBOL', '.');
            $fractions = $number->getFractionalPart();

            return \sprintf('%s%s%s', \strrev(\implode('', $groups)), $decimalSeparatorSymbol, $fractions);
        }

        return \strrev(\implode('', $groups));
    }

    public static function parseQuerystring($string)
    {
        $return = null;
        \parse_str($string, $return);

        return $return;
    }

    public static function rateLimit($key, $maxAttempts, $decaySeconds = 60)
    {
        return RateLimiter::attempt($key, $maxAttempts, self::noopFunction(), $decaySeconds) === false
            ? RateLimiter::availableIn($key)
            : 0;
    }

    public static function rateLimits($key, $attemptsConfig)
    {
        // attempt=0:decay=60:label=*,attempt:decay:label,...

        $availables = [0];

        foreach (\explode(',', $attemptsConfig) as $index => $attemptConfig) {
            $limits = \explode(':', $attemptConfig);

            $keySuffix = isset($limits[2]) ? $limits[2] : (string) $index;

            $availables[] = self::rateLimit(
                "_rate-limit:::{$key}:::{$keySuffix}",
                isset($limits[0]) && \is_numeric($limits[0]) ? (int) $limits[0] : 0,
                isset($limits[1]) && \is_numeric($limits[1]) ? (int) $limits[1] : 60
            );
        }

        return (int) \max(...$availables);
    }

    // from ci
    public static function removeInvisibleCharacters($str, $url_encoded = true)
    {
        $non_displayables = [];

        // every control character except newline (dec 10),
        // carriage return (dec 13) and horizontal tab (dec 09)
        if ($url_encoded) {
            $non_displayables[] = '/%0[0-8bcef]/';  // url encoded 00-08, 11, 12, 14, 15
            $non_displayables[] = '/%1[0-9a-f]/'; // url encoded 16-31
        }

        $non_displayables[] = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S'; // 00-08, 11, 12, 14-31, 127

        $count = null;
        do {
            $str = \preg_replace($non_displayables, '', $str, -1, $count);
        }
        while ($count);

        return $str;
    }

    public static function reportExceptionStack($exception)
    {
        $exception = $exception ?: new RuntimeException('Missing original exception');

        do {
            report($exception);
        }
        while ($exception = $exception->getPrevious());
    }

    public static function reportIgnoredException($message, $exception)
    {
        $code = $exception && \method_exists($exception, 'getCode') ? $exception->getCode() : 0;

        self::reportExceptionStack(new MaybeIgnoredException($message, backport_is_numeric($code) ? $code : 0, $exception));
    }

    public static function safeDispatchAfterResponse($callback)
    {
        $context = [
            '__cr_internal' => true,
            '__cr_task_id' => Str::random(8),
        ];

        $trace = new MaybeIgnoredException("dispatch() trace ({$context['__cr_task_id']})");

        $dispatchCallable = new DispatchPayload($callback, $trace, $context);

        if (\function_exists('defer')) {
            defer($dispatchCallable)->always();
        }
        elseif (\version_compare(\PHP_VERSION, '7.4', '>=')) {
            dispatch($dispatchCallable)->afterResponse();
        }
        else {
            app()->terminating($dispatchCallable);
        }

        return $dispatchCallable->getCancellation();
    }

    public static function safeOb($callback, ...$arguments)
    {
        \ob_start();

        try {
            self::withExceptionErrorHandler(...\array_merge([$callback], $arguments));
        }
        finally {
            return \ob_get_clean();
        }
    }

    public static function sequence($url = null)
    {
        if ($url) {
            try {
                $id = \explode(' ', Http::get($url)->body());
                if (\count($id) === 2) {
                    $sum = 0;
                    for ($l = \strlen($id[0]) - 1; $l >= 0; --$l) {
                        $sum += (int) $id[0][$l];
                    }
                    if (\substr((string) $sum, -1) === $id[1]) {
                        return [
                            (string) $id[0],
                            Str::uuid()->toString(),
                            self::microTime(),
                        ];
                    }
                }
            }
            catch (Exception $e) {
                self::reportIgnoredException('Error generating sequence.', $e);
            }
        }

        return [
            (string) SequenceGenerator::make()->nextId(),
            Str::uuid()->toString(),
            self::microTime(),
        ];
    }

    public static function smbShare($config)
    {
        if (! \is_array($config)) {
            $config = Config::get($config);
        }

        if (isset($config['url'])) {
            $config = \array_merge($config, (new ConfigurationUrlParser)->parseConfiguration($config));

            $config['share'] = Arr::pull($config, 'database');
        }

        if (isset($config['host']) && isset($config['port'])) {
            $config['hostport'] = "{$config['host']}:{$config['port']}";
        }

        $auth = new SmbBasicAuth($config['username'], $config['domain'], $config['password']);

        $server = new SmbServerFactory(
            tap(new SmbOptions)->setTimeout(isset($config['timeout']) ? $config['timeout'] : 30)
        );

        return $server->createServer($config['hostport'], $auth)->getShare($config['share']);
    }

    public static function sqlDateRange($date1, $date2 = null)
    {
        if ($date2 === null) {
            $date1Carbon = with(self::parseDate($date1) ?: self::microTime())->startOfDay();

            return [
                $date1Carbon,
                $date1Carbon->copy()->addDay(),
            ];
        }

        $date2Carbon = self::parseDate($date2);
        if (! $date2Carbon) {
            return self::sqlDateRange($date1);
        }
        $date2Carbon = $date2Carbon->startOfDay();

        $date1Carbon = self::parseDate($date1);
        if (! $date1Carbon) {
            return [
                $date2Carbon,
                $date2Carbon->copy()->addDay(),
            ];
        }
        $date1Carbon = $date1Carbon->startOfDay();

        if ($date1Carbon->isAfter($date2Carbon)) {
            $tmp = $date1Carbon;
            $date1Carbon = $date2Carbon;
            $date2Carbon = $tmp;
        }

        return [
            $date1Carbon->startOfDay(),
            $date2Carbon->addDay(),
        ];
    }

    public static function sqlLikeValue($value, $type = null)
    {
        if ($type === null) {
            $type = '%';
        }

        switch ((string) $type) {
            case '%': return '%' . \addcslashes(isset($value) ? $value : '', '_%\\') . '%';
            case '_%': return \addcslashes(isset($value) ? $value : '', '_%\\') . '%';
            case '%_': return '%' . \addcslashes(isset($value) ? $value : '', '_%\\');
            case '=':
            default: $value;
        }
    }

    public static function start()
    {
        if (! isset(self::$startTime)) {
            self::$startTime = CarbonImmutable::createFromTimestamp(
                \defined('LARAVEL_START') ? LARAVEL_START : \microtime(false),
                Config::get('app.timezone')
            );
        }

        return self::$startTime->copy();
    }

    public static function stringify($item, $fallback = null)
    {
        if (\is_string($item)) {
            return $item;
        }

        if (\is_scalar($item)) {
            return (string) $item;
        }

        if ($item instanceof Stringable) {
            return $item->__toString();
        }

        if (\is_object($item) && \method_exists($item, 'toString')) {
            $toString = $item->toString();

            return \is_string($toString) ? $toString : null;
        }

        return $fallback === null ? null : self::stringify($fallback);
    }

    public static function strDefaultTrue($value, $trueValue)
    {
        if (\is_string($value) && ! blank($value)) {
            return $value;
        }

        if ($value === true) {
            return self::strDefaultTrue($trueValue, null);
        }

        return null;
    }

    public static function strFalseOrNull($string, $false = '', $null = '')
    {
        if ($string === false) {
            return $false;
        }

        if ($string === null) {
            return $null;
        }

        return $string;
    }

    public static function strRandomPool($length = 16, $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $result = [];

        $max = \max(\mb_strlen($pool, '8bit') - 1, 0);
        for ($i = 0; $i < $length; ++$i) {
            $result[] = $pool[\random_int(0, $max)];
        }

        return \substr(\implode('', $result), 0, $length);
    }

    public static function swiftmailerSendMail($mail, $smtp = null)
    {
        if (! isset($mail['to']) && ! isset($mail['cc']) && ! isset($mail['bcc'])) {
            throw new InvalidArgumentException('Missing destination (To, CC, BCC)');
        }

        if (! $smtp) {
            $smtp = Config::get('mail.mailers.smtp');
            $smtp['name'] = 'smtp';
        }
        elseif (\is_string($smtp)) {
            $name = $smtp;
            $smtp = Config::get("mail.mailers.{$smtp}");
            $smtp['name'] = $name;
        }
        else {
            $smtp = Collection::wrap($smtp)->all();
            $smtp['name'] = (isset($smtp['name']) ? $smtp['name'] : false) ?: '?';
        }

        if (isset($smtp['url'])) {
            $smtp = \array_merge($smtp, (new ConfigurationUrlParser)->parseConfiguration($smtp));

            $smtp['transport'] = Arr::pull($smtp, 'driver');
        }

        $notVerifyingPeer = isset($smtp['verify_peer']) && ! $smtp['verify_peer'];

        $transport = new SwiftSmtpTransport(
            $smtp['host'],
            $smtp['port'],
            isset($smtp['encryption']) ? $smtp['encryption'] : ($notVerifyingPeer ? '' : 'tls')
        );

        $transport
            ->setUsername($smtp['username'])
            ->setPassword($smtp['password']);

        $streamOptions = null;
        if (isset($smtp['stream_options'])) {
            $streamOptions = (array) $smtp['stream_options'];
        }

        if ($notVerifyingPeer) {
            $streamOptions = $streamOptions ?: [];
            $streamOptions['ssl'] = \array_merge((array) data_get($streamOptions, 'ssl'), [
                'allow_self_signed' => true,
                'verify_peer' => false,
                'verify_peer_name' => false,
            ]);
        }

        if ($streamOptions) {
            $transport->setStreamOptions($streamOptions);
        }

        $mailer = new SwiftMailer($transport);

        $message = new SwiftMessage(isset($mail['subject']) ? $mail['subject'] : 'No Subject');

        $recipients = [];

        foreach (['from', 'reply_to', 'to', 'return_path', 'cc', 'bcc'] as $type) {
            if (isset($mail[$type])) {
                $recipient = Arr::wrap($mail[$type]);

                $message->{'set' . Str::studly($type)}($recipient);

                if (\in_array($type, ['to', 'cc', 'bcc'])) {
                    $recipients[] = new Address(...$recipient);
                }
            }
        }

        foreach (['from', 'reply_to', 'to', 'return_path'] as $type) {
            if (! isset($mail[$type])) {
                $defaultName = Config::get("mail.{$type}.name");
                $defaultAddress = Config::get("mail.{$type}.address");

                if ($type === 'from') {
                    $defaultAddress = $defaultAddress ?: (
                        filter_var($smtp['username'], FILTER_VALIDATE_EMAIL) ? $smtp['username'] : null
                    );
                }

                if ($defaultAddress) {
                    $message->{'set' . Str::studly($type)}(
                        $defaultName && \is_string($defaultName)
                            ? [$defaultAddress => $defaultName]
                            : $defaultAddress
                    );
                }
            }
        }

        $body = isset($mail['body']) ? $mail['body'] : '';

        if (\is_string($body)) {
            $message->setBody($body);
        }
        elseif (\is_callable($body)) {
            $body($message, $smtp);
        }
        else {
            $html = [];

            foreach (Collection::wrap($body)->all() as $value) {
                if (\is_string($value)) {
                    $html[] = e($value);
                }
                elseif ($value instanceof Htmlable) {
                    $html[] = $value->toHtml();
                }
                elseif ($value instanceof SplFileInfo) {
                    $message->attach(SwiftAttachment::fromPath($value->getRealPath()));
                }
                else {
                    throw new Exception('Unknown body part.');
                }
            }

            $message->addPart(\implode("\n", $html), 'text/html');
        }

        Log::debug('mail: swift config', Arr::except($smtp, ['password']));

        $events = self::appEvents();

        $failedRecipients = [];

        $email = new Email;

        $mail['message'] = $message;

        if (! $events || $events->until(new MessageSending($email, $mail)) !== false) {
            $result = $mailer->send($message, $failedRecipients);

            if ($result && $events) {
                $envelope = new Envelope(new Address($mail['from']), $recipients);

                $sentMessage = new SentMessage(new SymfonySentMessage($email, $envelope));

                $events->dispatch(new MessageSent($sentMessage, $mail));
            }
        }

        return $failedRecipients;
    }

    public static function temporaryFile($tempDir = '', $suffix = 'file')
    {
        if (! \file_exists($base = ($tempDir ?: storage_path($tempDir)))) {
            \mkdir($base, 0777, true);
        }

        if (! \is_dir($base)) {
            throw new Exception(\sprintf('Cannot create temporary folder. (%s; %s)', $tempDir, $base));
        }

        $i = 100;
        while (true) {
            $path = \sprintf('%s/%s.%s', $tempDir, Str::uuid()->toString(), $suffix);
            if ($tempDir !== $base) {
                $path = storage_path($path);
            }

            if ($i-- === 0 || ! \file_exists($path)) {
                break;
            }
        }

        if (! $path) {
            throw new Exception(\sprintf('Cannot create temporary folder. (%s; %s)', $tempDir, $path));
        }

        self::unlinkLater($path);

        return $path;
    }

    public static function unlinkLater($path, $unsafe = false)
    {
        \register_shutdown_function(new UnlinkPath($path, $unsafe));
    }

    public static function urlSegments($url, $root = null)
    {
        $root = $root ?: request()->root();

        if (! \str_starts_with($url, $root)) {
            return [];
        }

        $refererAbsolute = \substr($url, \mb_strlen($root));

        $pattern = \trim($refererAbsolute, '/');

        $decodedPath = \rawurldecode($pattern === '' ? '/' : $pattern);

        return \array_values(\array_filter(\explode('/', $decodedPath), static function ($value) {
            return $value !== '';
        }));
    }

    public static function userAgentHumanize($uaString)
    {
        if (\class_exists(UAParser::class)) {
            $parsed = UAParser::create()->parse($uaString);

            $os = $parsed->os->major;
            $brand = $parsed->device->brand;
            $model = $parsed->device->model;
            $bm = $brand && $model ? "{$brand} ({$model})" : ($brand ?: $model);

            return $os && $bm ? "{$os}, {$bm}" : ($os ?: $bm);
        }
    }

    public static function willNotThrow($exception = null) {}

    public static function willThrow($exception = null) {}

    public static function withExceptionErrorHandler($callback, ...$arguments)
    {
        \set_error_handler(static function ($errno, $errstr, $errfile, $errline, $errcontext = null) {
            throw new Error(\sprintf('%s (%s) at %s:%s (%s)', $errstr, $errno, $errfile, $errline, \json_encode($errcontext)), $errno);
        });

        try {
            return $callback(...$arguments);
        }
        finally {
            \restore_error_handler();
        }
    }

    public static function writeToTemp($callback)
    {
        $path = \tempnam(\sys_get_temp_dir(), 'tmp');

        self::unlinkLater($path);

        $callback($path);

        return $path;
    }

    public static function parseTimeInteger($timeString, $delimiter = ':')
    {
        if ($timeString instanceof DateTimeInterface) {
            return (int) $timeString->format('His');
        }

        $padded = \array_pad(\explode($delimiter, $timeString, 4), 3, '0');

        $result = '';

        foreach (\array_slice($padded, 0, 3) as $part) {
            $result .= \str_pad(\mb_substr(\trim($part), -2), 2, '0', \STR_PAD_LEFT);
        }

        return (int) $result;
    }

    protected static function parseDate()
    {
        try {
            return CarbonImmutable::parse(...\func_get_args());
        }
        catch (Exception $e) {
            self::reportIgnoredException('Error parsing date.', $e);

            return false;
        }
    }

    protected static function appEvents()
    {
        if (! self::$eventDispatcher) {
            self::$eventDispatcher = app('events');
        }

        return self::$eventDispatcher;
    }
}
