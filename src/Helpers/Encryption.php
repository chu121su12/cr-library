<?php

namespace CR\Library\Helpers;

use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class Encryption
{
    const AES_ALGORITHM = 'AES-256-CBC';

    const AES_IV_SIZE = 16;

    const AES_PREFIX_BYTE = 1;

    const HASH_KEY_SALT = 'salt';

    const HMAC_HASH_ALGORITHM = 'sha256';

    const PAYLOAD_COUNT = 3;

    const RSA_KEY_CACHE_DURATION = 60 * 60 * 24 * 3;

    const RSA_KEY_SIZE = 4096;

    const RSA_SIGN_VERIFY_ALGORITHM = 'sha256';

    protected static function aesDecryptValidation($cipher, $appKey)
    {
        $payload = \explode('.', $cipher, self::PAYLOAD_COUNT + 1);

        if (! (\is_array($payload) && \count($payload) === self::PAYLOAD_COUNT)) {
            throw new Exception('The payload is invalid.', 400);
        }

        list($payloadIv, $payloadMac, $payloadData) = $payload;

        $checkIvBytes = self::aesIv();

        $decodedIv = self::base64UrlDecode($payloadIv);

        $decodedData = self::base64UrlDecode($payloadData);

        $hmacInput = self::aesHmac(
            self::base64UrlDecode($payloadMac),
            $checkIvBytes
        );

        $hmacPayload = self::aesHmac(
            self::aesHash($decodedIv, $decodedData, $appKey),
            $checkIvBytes
        );

        if (! \hash_equals($hmacInput, $hmacPayload)) {
            throw new Exception('The MAC is invalid.', 400);
        }

        return [$decodedIv, $decodedData];
    }

    public static function aesDecrypt($cipher, $key = null)
    {
        return \unserialize(\substr(self::aesDecryptString($cipher, $key), self::AES_PREFIX_BYTE));
    }

    public static function aesDecryptString($cipher, $key = null)
    {
        $appKey = $key ?: self::hashedAppKeyBinary();

        list($decodedIv, $decodedData) = self::aesDecryptValidation($cipher, $appKey);

        $decrypted = \openssl_decrypt(
            $decodedData,
            self::AES_ALGORITHM,
            $appKey,
            \OPENSSL_RAW_DATA,
            $decodedIv
        );

        if ($decrypted === false) {
            throw new Exception('Could not decrypt the data.', 400);
        }

        if (! \is_string($decrypted)) {
            throw new Exception('Could not decrypt non-string data.', 400);
        }

        return $decrypted;
    }

    public static function aesEncrypt($message, $key = null)
    {
        return self::aesEncryptString(\openssl_random_pseudo_bytes(self::AES_PREFIX_BYTE) . \serialize($message), $key);
    }

    public static function aesEncryptString($message, $key = null)
    {
        if (! \is_string($message)) {
            throw new Exception('Could not encrypt non-string data.', 400);
        }

        $appKey = $key ?: self::hashedAppKeyBinary();

        $iv = self::aesIv();

        $cipher = \openssl_encrypt(
            $message,
            self::AES_ALGORITHM,
            $appKey,
            \OPENSSL_RAW_DATA,
            $iv
        );

        if ($cipher === false) {
            throw new Exception('Could not encrypt the data.', 400);
        }

        return \implode('.', [
            self::base64UrlEncodeNoPad($iv),
            self::base64UrlEncodeNoPad(self::aesHash($iv, $cipher, $appKey)),
            self::base64UrlEncodeNoPad($cipher),
        ]);
    }

    public static function rsaDecrypt($cipher, $secretKey = null, $passphrase = null)
    {
        return \unserialize(self::rsaDecryptString($cipher, $secretKey, $passphrase))[1];
    }

    public static function rsaDecryptString($cipher, $secretKey = null, $passphrase = null)
    {
        $parts = [];

        $key = null;

        try {
            $key = \openssl_pkey_get_private(
                $secretKey ?: self::rsaLoadCached()['sk'],
                $passphrase === false ? null : ($passphrase ?: self::hashedAppKeyHex())
            );

            foreach (\explode('.', $cipher) as $part) {
                $decrypted = null;

                \openssl_private_decrypt(self::base64UrlDecode($part), $decrypted, $key);

                $parts[] = $decrypted;
            }
        }
        catch (Exception $e) {
            throw new Exception('Cannot decrypt data.', 400, $e);
        }
        finally {
            self::unloadKey($key);
        }

        $decrypted = self::base64UrlDecode(\implode('', $parts));

        if (! \is_string($decrypted)) {
            throw new Exception('Could not decrypt non-string data.');
        }

        return $decrypted;
    }

    public static function rsaEncrypt($message, $publicKey = null)
    {
        return self::rsaEncryptString(\serialize([self::aesIv(), $message]), $publicKey);
    }

    public static function rsaEncryptString($message, $publicKey = null)
    {
        if (! \is_string($message)) {
            throw new Exception('Could not encrypt non-string data.', 400);
        }

        $key = null;

        try {
            $publicKey = $publicKey ?: self::rsaLoadCached()['pk'];

            $key = \openssl_pkey_get_public($publicKey);

            $parts = [];

            foreach (\str_split(self::base64UrlEncodeNoPad($message), self::rsaSplitSize($publicKey)) as $part) {
                $encrypted = null;

                \openssl_public_encrypt($part, $encrypted, $key);

                $parts[] = self::base64UrlEncodeNoPad($encrypted);
            }

            return \implode('.', $parts);
        }
        catch (Exception $e) {
            throw new Exception('Cannot encrypt data.', 400, $e);
        }
        finally {
            self::unloadKey($key);
        }
    }

    public static function rsaGenerateNewKeyPairs($size = self::RSA_KEY_SIZE, $passphrase = null)
    {
        $configFile = Helpers::writeToTemp(static function ($path) {
            \file_put_contents($path, \implode(\PHP_EOL . \PHP_EOL, [
                'HOME=.',
                'RANDFILE=$ENV::HOME/.rnd',
                '[v3_ca]',
            ]));
        });

        $config = [
            'config' => $configFile,
            'encrypt_key_cipher' => \OPENSSL_CIPHER_AES_256_CBC,
            'private_key_bits' => $size ?: self::RSA_KEY_SIZE,
            'private_key_type' => \OPENSSL_KEYTYPE_RSA,
        ];

        $key = \openssl_pkey_new($config);

        $publicKey = \openssl_pkey_get_details($key)['key'];

        $secretKey = null;

        \openssl_pkey_export(
            $key,
            $secretKey,
            $passphrase === false ? null : ($passphrase ?: self::hashedAppKeyHex()),
            $config
        );

        return [
            $secretKey,
            $publicKey,
            $config,
            \file_get_contents($configFile),
        ];
    }

    public static function rsaLoadCached($cacheKey = null)
    {
        $cacheKey = $cacheKey ?: \sprintf(
            'avon:asymetric-keys-rev2:%s:%s',
            self::RSA_KEY_SIZE,
            Helpers::microTime()->format('Ymd')
        );

        return Cache::remember($cacheKey, self::RSA_KEY_CACHE_DURATION, static function () use ($cacheKey) {
            Log::debug('encryption: new key `' . $cacheKey . '\' created');

            list($sk, $pk) = self::rsaGenerateNewKeyPairs(self::RSA_KEY_SIZE, false);

            return [
                'sk' => $sk,
                'pk' => $pk,
                'epk' => self::base64UrlEncodeNoPad($pk),
                'spk' => self::rsaSignString($pk, $sk, false),
            ];
        });
    }

    public static function rsaSignString($message, $secretKey = null, $passphrase = null)
    {
        if (! \is_string($message)) {
            throw new Exception('Could not sign non-string data.', 400);
        }

        $key = null;

        try {
            $key = \openssl_pkey_get_private(
                $secretKey ?: self::rsaLoadCached()['sk'],
                $passphrase === false ? null : ($passphrase ?: self::hashedAppKeyHex())
            );

            $signature = null;

            \openssl_sign($message, $signature, $key, self::RSA_SIGN_VERIFY_ALGORITHM);

            return self::base64UrlEncodeNoPad($signature);
        }
        catch (Exception $e) {
            throw new Exception('Could not sign the data.', 400, $e);
        }
        finally {
            self::unloadKey($key);
        }
    }

    public static function rsaVerifyString($signature, $message, $publicKey = null)
    {
        if (! \is_string($message)) {
            throw new Exception('Could not verify non-string data.', 400);
        }

        $key = null;

        try {
            $publicKey = $publicKey ?: self::rsaLoadCached()['pk'];

            $key = \openssl_pkey_get_public($publicKey);

            $result = \openssl_verify($message, self::base64UrlDecode($signature), $key, self::RSA_SIGN_VERIFY_ALGORITHM);

            return $result === 1;
        }
        catch (Exception $e) {
            throw new Exception('Could not verify the data.', 400, $e);
        }
        finally {
            self::unloadKey($key);
        }
    }

    protected static function unloadKey($key)
    {
        if ($key && \function_exists('openssl_free_key')) {
            \openssl_free_key($key);
        }
    }

    protected static function aesHash($iv, $value, $key)
    {
        return self::aesHmac($iv . $value, $key);
    }

    protected static function aesHmac($value, $key)
    {
        return \hash_hmac(self::HMAC_HASH_ALGORITHM, $value, $key, true);
    }

    protected static function aesIv()
    {
        return \openssl_random_pseudo_bytes(self::AES_IV_SIZE);
    }

    protected static function hashedAppKeyBinary()
    {
        return \hash('sha256', config('app.key'), true);
    }

    protected static function hashedAppKeyHex()
    {
        return \hash('sha256', config('app.key'), false);
    }

    public static function hashPassword($password, $salt = null)
    {
        return \hash_pbkdf2('sha512', $password, $salt ?: self::HASH_KEY_SALT . $password, 150000, 32, true);
    }

    protected static function rsaSplitSize($key)
    {
        $keyLength = (int) \strlen(\preg_replace(['/-.*/', '/\s+/'], ['', ''], $key));

        $size = 384;

        switch (true) {
            case $keyLength <= 128: $size = 512;

                break;
            case $keyLength <= 220: $size = 1024;

                break;
            case $keyLength <= 392: $size = 2048;

                break;
            case $keyLength <= 564: $size = 3072;

                break;
            default: /* case $keyLength <= 736 */ $size = 4096;

                break;
        }

        return $size / 16 - 1;
    }

    public static function base64UrlEncodeNoPad($input)
    {
        return \strtr(\rtrim(\base64_encode($input), '='), '+/', '-_');
    }

    public static function base64UrlDecode($input)
    {
        return \base64_decode(\strtr($input, '-_', '+/'), true);
    }
}
