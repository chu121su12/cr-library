<?php

namespace CR\Library\Helpers;

use Brick\Math\BigDecimal;
use Brick\Math\BigInteger;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\RoundingMode;
use Brick\Money\Money;

class LocaleIndonesia
{
    const DEFAULT_DATE_FORMAT = 'l, j F Y';

    const DEFAULT_DATE_TIME_FORMAT = 'l, j F Y, H:i:s';

    const SHORT_MONTH_REGEX_REPLACE = [
        '/\bJan\b/' => 'Jan',
        '/\bPeb\b/' => 'Feb',
        '/\bMar\b/' => 'Mar',
        '/\bApr\b/' => 'Apr',
        '/\bMei\b/' => 'May',
        '/\bJun\b/' => 'Jun',
        '/\bJul\b/' => 'Jul',
        '/\bAgu\b/' => 'Aug',
        '/\bSep\b/' => 'Sep',
        '/\bOkt\b/' => 'Oct',
        '/\bNov\b/' => 'Nov',
        '/\bDes\b/' => 'Dec',
    ];

    const DATE_REGEX_REPLACE = [
        '/\bSunday\b/' => 'Minggu',
        '/\bMonday\b/' => 'Senin',
        '/\bTuesday\b/' => 'Selasa',
        '/\bWednesday\b/' => 'Rabu',
        '/\bThursday\b/' => 'Kamis',
        '/\bFriday\b/' => 'Jumat',
        '/\bSaturday\b/' => 'Sabtu',

        '/\bJanuary\b/' => 'Januari',
        '/\bFebruary\b/' => 'Februari',
        '/\bMarch\b/' => 'Maret',
        '/\bApril\b/' => 'April',
        '/\bMay\b/' => 'Mei',
        '/\bJune\b/' => 'Juni',
        '/\bJuly\b/' => 'Juli',
        '/\bAugust\b/' => 'Agustus',
        '/\bSeptember\b/' => 'September',
        '/\bOctober\b/' => 'Oktober',
        '/\bNovember\b/' => 'November',
        '/\bDecember\b/' => 'Desember',

        '/\bSun\b/' => 'Min',
        '/\bMon\b/' => 'Sen',
        '/\bTue\b/' => 'Sel',
        '/\bWed\b/' => 'Rab',
        '/\bThu\b/' => 'Kam',
        '/\bFri\b/' => 'Jum',
        '/\bSat\b/' => 'Sab',

        '/\bJan\b/' => 'Jan',
        '/\bFeb\b/' => 'Feb',
        '/\bMar\b/' => 'Mar',
        '/\bApr\b/' => 'Apr',
        // '/\bMay\b/' => 'Mei', // duplicate
        '/\bJun\b/' => 'Jun',
        '/\bJul\b/' => 'Jul',
        '/\bAug\b/' => 'Agu',
        '/\bSep\b/' => 'Sep',
        '/\bOct\b/' => 'Okt',
        '/\bNov\b/' => 'Nov',
        '/\bDec\b/' => 'Des',
    ];

    const NUMBER_ZERO_STRING = 'nol';

    const NUMBER_DIGIT_LOOKUP = [
        '', 'satu', 'dua', 'tiga',
        'empat', 'lima', 'enam',
        'tujuh', 'delapan', 'sembilan',
        'sepuluh', 'sebelas',
    ];

    const NUMBER_OF_THOUSANDS_LOOKUP = [
        'ribu',
        'juta',
        'miliar',
        'triliun',
        'kuardriliun',
        'kuintiliun',
        'sektiliun',
        'septiliun',
        'oktiliun',
        'noniliun',
        'desiliun',
        'undesiliun',
        'duodesiliun',
        'tredesiliun',
        'kuattodesiliun',
        'kuindesiliun',
        'sekdesiliun',
        'septemdesiliun',
        'oktodesiliun',
        'novemdesiliun',
        'vigintiliun',
        'unvigintiliun',
        'duovigintiliun',
        'trevigintiliun',
        'kuattuovigintriliun',
        'kuinvigintiliun',
        'sekvigintiliun',
        'septenvigintriliun',
        'oktovigintriliun',
        'novemvigintriliun',
        'trigintiliun',
    ];

    public static function translateDateString($date)
    {
        if (! \is_string($date)) {
            return $date;
        }

        return \preg_replace(
            \array_keys(self::DATE_REGEX_REPLACE),
            \array_values(self::DATE_REGEX_REPLACE),
            $date
        );
    }

    public static function fixSybaseDate($value)
    {
        $maybeEnglish = \preg_replace(
            \array_keys(self::SHORT_MONTH_REGEX_REPLACE),
            \array_values(self::SHORT_MONTH_REGEX_REPLACE),
            $value
        );

        return \preg_replace('/(\d\d:\d\d:\d\d):(\d{3,})$/', '$1.$2', $maybeEnglish);
    }

    public static function terbilang($nilai, $keepZeroDecimals = false)
    {
        if ($nilai instanceof Money) {
            $nilai = $nilai->getAmount();
        }

        return \preg_replace('/\s\s+/', ' ', \trim(self::spellNumber(BigDecimal::of($nilai), $keepZeroDecimals)));
    }

    public static function formatNumber($nilai, $decimals = null)
    {
        return self::formatNumberBase(',', '.', $nilai, $decimals);
    }

    public static function formatNumeric($nilai, $decimals = null)
    {
        return self::formatNumberBase('.', ',', $nilai, $decimals);
    }

    protected static function formatNumberBase($thousandsSeparator, $decimalSeparator, $nilai, $decimals)
    {
        if ($nilai instanceof Money) {
            $nilai = $nilai->getAmount();
        }

        $nilai = BigDecimal::of($nilai);

        $formattedIntegral = ($nilai->isNegative() ? '-' : '') . \ltrim(\strrev(
            preg_replace('/(.{3})/', '$1,', \strrev($nilai->abs()->getIntegralPart()))
        ), $thousandsSeparator);

        if ($decimals === 0) {
            if ($nilai->hasNonZeroFractionalPart()) {
                throw new NumberFormatException(\sprintf('The number %s overflow decimals.', (string) $nilai));
            }

            return $formattedIntegral;
        }

        $fraction = $nilai->getFractionalPart();
        if ($decimals === null) {
            return $nilai->hasNonZeroFractionalPart()
                ? "{$formattedIntegral}{$decimalSeparator}{$fraction}"
                : $formattedIntegral;
        }

        $normalizedFraction = \rtrim($fraction, '0');
        if ($decimals < 0) {
            $decimals = 0;
        }
        elseif ($decimals < \mb_strlen($normalizedFraction)) {
            throw new NumberFormatException(\sprintf('The number %s overflow decimals.', (string) $nilai));
        }

        $paddedFraction = \str_pad($normalizedFraction, $decimals, '0', \STR_PAD_RIGHT);

        return "{$formattedIntegral}{$decimalSeparator}{$paddedFraction}";
    }

    protected static function spellNumber(BigDecimal $number, $keepZeroDecimals)
    {
        if ($number->isZero()) {
            return self::NUMBER_ZERO_STRING;
        }

        if ($number->isNegative()) {
            return 'negatif ' . self::spellNumber($number->negated(), $keepZeroDecimals);
        }

        $integer = $number->getIntegralPart();

        return \sprintf(
            '%s%s',
            $integer === '0' ? self::NUMBER_ZERO_STRING : self::spellInteger(BigInteger::of($integer)),
            self::spellDecimal($number->getFractionalPart(), $keepZeroDecimals)
        );
    }

    protected static function spellInteger(BigInteger $number)
    {
        if ($number->isLessThan(12)) {
            return ' ' . self::NUMBER_DIGIT_LOOKUP[(int) ((string) $number)];
        }

        if ($number->isLessThan(20)) {
            return self::spellInteger($number->minus(10)) . ' belas';
        }

        if ($number->isLessThan(100)) {
            return self::spellInteger($number->dividedBy(10, RoundingMode::DOWN)) . ' puluh' . self::spellInteger($number->mod(10));
        }

        if ($number->isLessThan(200)) {
            return ' seratus' . self::spellInteger($number->minus(100));
        }

        if ($number->isLessThan(1000)) {
            return self::spellInteger($number->dividedBy(100, RoundingMode::DOWN)) . ' ratus' . self::spellInteger($number->mod(100));
        }

        if ($number->isLessThan(2000)) {
            return ' seribu' . self::spellInteger($number->minus(1000));
        }

        $ofThousands = BigInteger::of(1000);

        foreach (self::NUMBER_OF_THOUSANDS_LOOKUP as $thousandName) {
            $nextThousands = $ofThousands->multipliedBy(1000);

            if ($number->isLessThan($nextThousands)) {
                return \sprintf(
                    '%s %s%s',
                    self::spellInteger($number->dividedBy($ofThousands, RoundingMode::DOWN)),
                    $thousandName,
                    self::spellInteger($number->mod($ofThousands))
                );
            }

            $ofThousands = $nextThousands;
        }

        throw new NumberFormatException(\sprintf('The number %s cannot be formatted.', (string) $number));
    }

    protected static function spellDecimal($decimalString, $keepZeroDecimals)
    {
        if (! $keepZeroDecimals) {
            $decimalString = \preg_replace('/0+$/', '', $decimalString);
        }

        if ($decimalString === '') {
            return '';
        }

        $decimals = \array_map(static function ($digit) {
            $digit = (int) $digit;

            return $digit ? self::NUMBER_DIGIT_LOOKUP[$digit] : self::NUMBER_ZERO_STRING;
        }, \mb_str_split($decimalString, 1));

        return ' koma ' . \implode(' ', $decimals);
    }
}
