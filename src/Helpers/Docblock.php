<?php

namespace CR\Library\Helpers;

use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionClassConstant;
use ReflectionException;
use ReflectionFunction;
use ReflectionMethod;
use ReflectionProperty;
use Symfony\Component\Finder\Finder;

class Docblock
{
    protected $width;

    protected $overwrite;

    protected $publicOnly;

    public function __construct($width = 80, $overwrite = true, $publicOnly = true)
    {
        $this->width = $width;

        $this->overwrite = $overwrite;

        $this->publicOnly = $publicOnly;

        // (new Docblock)->generateForFiles($path, 'CR\\Library');
    }

    public function generateForFunction($functionName)
    {
        $function = $functionName instanceof ReflectionFunction ? $functionName : new ReflectionFunction($functionName);

        return [$function, $this->renewBlocks($function, collect([$function]))];
    }

    public function generateForClass($className)
    {
        $class = $className instanceof ReflectionClass ? $className : new ReflectionClass($className);

        return [$class, $this->renewBlocks($class, collect(\array_merge(
            [$class],
            $this->publicOnly
                ? $class->getProperties(ReflectionProperty::IS_PUBLIC)
                : $class->getProperties(),
            $this->publicOnly
                ? $class->getMethods(ReflectionMethod::IS_PUBLIC)
                : $class->getMethods(),
            collect(
                \version_compare(\PHP_VERSION, '8', '>=') && $this->publicOnly
                    ? $class->getConstants(ReflectionClassConstant::IS_PUBLIC)
                    : $class->getConstants()
            )->keys()->values()->all()
        )))];
    }

    public function generateForFiles($path, $namespace = null)
    {
        collect($this->getClasses($path, $namespace ?: ''))->map(function ($class) {
            list($reflect, $outputFile) = $this->generateForClass($class);

            $this->rewrite($reflect, $outputFile);
        });
    }

    protected function rewrite($reflect, $outputFile)
    {
        file_put_contents($reflect->getFileName(), $outputFile);
    }

    protected function getClasses($path, $namespace)
    {
        $classes = [];

        foreach ((new Finder)->files()->in($path = realpath($path)) as $file) {
            try {
                $classRelative = trim(Str::replaceFirst(str_replace('/', '\\', $path), '', $file->getRealPath()), '\\');

                $maybeClassName = trim(trim($namespace, '\\') . '\\' . ucfirst(Str::replaceLast('.php', '', $classRelative)), '\\');

                $classes[] = new ReflectionClass($maybeClassName);
            }
            catch (ReflectionException $_e) {
                continue;
            }
        }

        return array_filter($classes);
    }

    protected function renewBlocks($reflect, $target)
    {
        $filename = $reflect->getFileName();

        $file = file($filename);

        $target = $target
            ->map(function ($target) use ($filename, $file) {
                $output = $this->getMaybeStartLine($file, $target);

                if (! $output) {
                    return;
                }

                list($maybeStartLine, $isMethod) = $output;

                if ($isMethod && $target->getFileName() !== $filename) {
                    return;
                }

                $docComment = is_string($target) ? '' : ($target->getDocComment() ?: '');

                if ($docComment && ! $this->overwrite) {
                    return;
                }

                $lines = (preg_match_all('/(\r\n?|\n)/', $docComment) ?: 0) + 1;

                $indent = preg_replace('/^([ \t]*).*/s', '$1', $file[$maybeStartLine - 1]);

                return [
                    'docLines' => $docComment ? $lines : 0,
                    'maybeStartLine' => $maybeStartLine,
                    'comment' => $this->formatDocBlock(
                        $indent === false ? '' : $indent,
                        $this->generateDocBlockContent($target, $docComment)
                    ),
                ];
            })
            ->filter()
            ->values()
            ->all();

        foreach ($target as $x => $_) {
            $comment = $target[$x]['comment'];

            if (! $comment) {
                continue;
            }

            $anchor = $target[$x]['maybeStartLine'];

            array_splice(
                $file,
                $anchor - $target[$x]['docLines'] - 1,
                $target[$x]['docLines'],
                [$comment]
            );

            foreach ($target as $y => $_) {
                if ($target[$y]['maybeStartLine'] >= $anchor) {
                    $target[$y]['maybeStartLine'] += -$target[$x]['docLines'] + 1;
                }
            }
        }

        return implode('', $file);
    }

    protected function getMaybeStartLine($file, $target)
    {
        if (is_string($target)) {
            foreach ($file as $lineNumber => $line) {
                $name = $target;
                if (preg_match("/(?:(?:public|protected|private)\\b)?const\\b[^\\n\\r,]*\\b{$name}\\b/", $line)) {
                    return [$lineNumber + 1, false];
                }
            }

            return;
        }

        if ($target instanceof ReflectionProperty) {
            foreach ($file as $lineNumber => $line) {
                $name = $target->getName();
                if (preg_match("/(?:public|protected|private)\\b[^\\n\\r,]*\\\${$name}\\b/", $line)) {
                    return [$lineNumber + 1, false];
                }
            }

            return;
        }

        return [$target->getStartLine(), true];
    }

    protected function formatDocBlock($indent, $content)
    {
        $baseIndent = $this->width - mb_strwidth($indent) - 3;

        $body = [];

        if ($main = trim(data_get($content, 'main'))) {
            $body = [$main];
        }

        if ($description = trim(data_get($content, 'description'))) {
            $body[] = $this->formatAsBlock('', $description, $baseIndent);
        }

        foreach (data_get($content, 'parameters', []) as $parameter => $description) {
            $body[] = array_merge(
                [$parameter],
                $this->formatAsPaddedBlock($description, $baseIndent)
            );
        }

        if (! count($body)) {
            return '';
        }

        $body = collect($body)
            ->map(function ($lines) use ($indent) {
                return collect($lines)
                    ->map(function ($line) use ($indent) { return "{$indent} * {$line}"; })
                    ->join("\n");
            })
            ->join("\n{$indent} *\n");

        return "{$indent}/**\n{$body}\n{$indent} */\n";
    }

    protected function formatAsBlock($append, $fullLine, $width, $firstWidth = 0)
    {
        if (! $fullLine) {
            return [];
        }

        if ($firstWidth === 0) {
            $firstWidth = $width;
        }

        $lines = [];

        $chunk = [];

        $index = 0;

        foreach (preg_split('/\s+/', $fullLine) as $word) {
            $checkLength = $index === 0 ? $firstWidth : $width;

            $thisLine = implode(' ', $chunk);

            $nextLine = count($chunk) ? trim($thisLine . ' ' . $word) : $word;

            if (mb_strwidth($nextLine) > $checkLength) {
                $lines[] = "{$append}{$thisLine}";

                $chunk = [$word];

                ++$index;
            }
            else {
                $chunk[] = $word;
            }
        }

        $lines[] = $append . implode(' ', $chunk);

        return $lines;
    }

    protected function formatAsPaddedBlock($fullLine, $width)
    {
        return $this->formatAsBlock('    ', $fullLine, $width - 4);
    }

    protected function generateDocBlockContent($target, $originalRawComment)
    {
        return [
            'main' => $this->getMainComment($target, $originalRawComment),

            'description' => $this->getDescription($target, $originalRawComment),

            'parameters' => $this->getParameters($target, $originalRawComment),
        ];
    }

    protected function getMainComment($target, $originalRawComment)
    {
        // example: 'main comment'
        // example: 'constant comment'

        //
    }

    protected function getDescription($target, $originalRawComment)
    {
        //
    }

    protected function getParameters($target, $originalRawComment)
    {
        // example: ['@var  string  $something' => 'description']
        // example: ['@param  string  $something' => 'description']
        // example: ['@method  ?self  fn()' => 'description']
        // example: ['@throws  Class' => 'description']
        // example: ['@return  Type' => 'description']

        return [];
    }
}
