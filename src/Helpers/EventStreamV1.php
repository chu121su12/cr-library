<?php

namespace CR\Library\Helpers;

use Exception;
use Illuminate\Support\Facades\Date;
use Throwable;

class EventStreamV1 implements \Illuminate\Contracts\Support\Responsable
{
    protected $breakCallback;

    protected $callback;

    protected $counter = 0;

    protected $dateNow;

    protected $headers = [
        'Content-Type' => 'text/event-stream',
    ];

    protected $idleThreshold = 1000;

    protected $increment = 1;

    protected $incrementLimit = 5;

    protected $microtime;

    protected $now;

    protected $reconnectCommand;

    protected $scope = 2;

    protected $unixNow;

    protected $watchdog;

    protected $watchdogLimit = 10;

    public function comment($data)
    {
        echo ":{$data}\n\n";
        \flush();
    }

    public function data($data, $event = null)
    {
        if ($event) {
            echo "event:{$event}\n";
        }

        echo "data:{$data}\n\n";
        \flush();
    }

    public function event($event, $data = '')
    {
        echo "event:{$event}\n";
        echo "data:{$data}\n\n";
        \flush();
    }

    public function json($data, $event = 'json')
    {
        $this->data(\json_encode($data), $event);
    }

    public function jsonEvent($event, $data)
    {
        $this->data(\json_encode($data), $event);
    }

    public function once($callback = null)
    {
        if ($callback) {
            $this->withCallback($callback);
        }

        return $this->withBreakCallback(false);
    }

    public function toResponse($request)
    {
        return response()->stream(function () {
            $this->streamCallback();
        }, 200, $this->headers);
    }

    public function withBreakCallback($callback)
    {
        $this->breakCallback = $callback;

        return $this;
    }

    public function withCallback($callback)
    {
        $this->callback = $callback;

        return $this;
    }

    public function withHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }

    protected function callback()
    {
        return $this->callback;
    }

    protected function isCounterOverLimit()
    {
        return $this->counter >= 1 * 60 * 60;
    }

    protected function isRestartedByAppConfig()
    {
        if ($time = $this->serverEventUpdateTime()) {
            return Date::parse($time)->format('U') - $this->now > -2;
        }
    }

    protected function newLoop()
    {
        $this->dateNow = Helpers::microTime();

        $this->now = $this->dateNow->format('U.u');

        $this->microtime = (float) $this->now;

        $this->unixNow = (int) $this->now;
    }

    protected function pingCommand()
    {
        if ($this->counter % 15 === 0) {
            $this->comment('!');
        }
    }

    protected function reconnectCommand()
    {
        return $this->reconnectCommand;
    }

    protected function regulateIncrement()
    {
        if (\microtime(true) - $this->microtime < $this->idleThreshold) {
            if ($this->increment < $this->incrementLimit) {
                ++$this->increment;
            }
        }
        else {
            $this->increment = 1;
        }
    }

    protected function serverEventUpdateTime()
    {
        $class = 'App\Models\AppSetting';

        if (! \class_exists($class)) {
            return null;
        }

        $update = $class::where('key', 'notifications.update')->first();

        return $update && isset($update->data)
            ? $update->data
            : null;
    }

    protected function streamCallback()
    {
        try {
            \ob_end_flush();
        }
        catch (Exception $e) {
            if ($e->getMessage() !== 'ob_end_flush(): failed to delete and flush buffer. No buffer to delete or flush') {
                throw $e;
            }
        }

        $watchdogLimit = $this->watchdogLimit;
        $watchdog = 1;
        $willBreak = false;

        while (true) {
            if ($this->counter >= 60 * 61) {
                break;
            }

            $willThrow = null;

            try {
                $this->newLoop();
                $this->pingCommand();

                if ($this->counter > 1) {
                    if (
                        $this->isCounterOverLimit()
                        || $this->isRestartedByAppConfig()
                    ) {
                        $this->watchdog = $watchdog;
                        if (\is_callable($callback = $this->reconnectCommand())) {
                            $callback($this);
                        }
                    }
                }

                if (\is_callable($callback = $this->callback())) {
                    $callback($this);
                }

                if ($watchdog > 1) {
                    --$watchdog;
                }
            }
            catch (Exception $willThrow) {
            }
            catch (Throwable $willThrow) {
            }

            if (isset($willThrow)) {
                if ($watchdog < $watchdogLimit) {
                    ++$watchdog;
                }

                Helpers::reportIgnoredException('Error running eventStream callback.', $willThrow);
            }

            \flush();
            $this->regulateIncrement();
            $this->watchdog = $watchdog;
            $this->counter += $this->increment;

            $breakCallback = $this->breakCallback;
            if ($breakCallback === false) {
                $willBreak = true;
            }
            elseif (\is_callable($breakCallback) && $breakCallback()) {
                $willBreak = true;
            }

            if ($willBreak) {
                break;
            }

            \sleep($this->increment);
        }
    }

    protected function watchdogCounter()
    {
        return $this->watchdog;
    }
}
