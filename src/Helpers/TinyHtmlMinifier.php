<?php

namespace CR\Library\Helpers;

// https://github.com/jenstornell/tiny-html-minifier/blob/master/src/TinyHtmlMinifier.php
class TinyHtmlMinifier
{
    private $build;

    private $elements;

    private $head;

    private $options;

    private $output;

    private $skip;

    private $skipName;

    public function __construct(array $options)
    {
        $this->options = $options;
        $this->output = '';
        $this->build = [];
        $this->skip = 0;
        $this->skipName = '';
        $this->head = false;
        $this->elements = [
            'slash' => [
                'path',
                'option',
            ],
            'skip' => [
                'code',
                'pre',
                'script',
                'textarea',
            ],
            'inline' => [
                'a',
                'abbr',
                'acronym',
                'b',
                'bdo',
                'big',
                'br',
                'cite',
                'code',
                'dfn',
                'em',
                'i',
                'img',
                'kbd',
                'map',
                'object',
                'samp',
                'small',
                'span',
                'strong',
                'sub',
                'sup',
                'tt',
                'var',
                'q',
            ],
            'hard' => [
                '!doctype',
                'body',
                'html',
            ],
        ];
    }

    // Run minifier
    public function minify($html)
    {
        if (! isset($this->options['disable_comments'])
            || ! $this->options['disable_comments']) {
            $html = $this->removeComments($html);
        }

        $rest = $html;

        while (! empty($rest)) {
            $parts = \explode('<', $rest, 2);
            $this->walk($parts[0]);
            $rest = (isset($parts[1])) ? $parts[1] : '';
        }

        return $this->output;
    }

    // Add chevrons around element
    private function addChevrons($element, $noll)
    {
        if (empty($element)) {
            return $element;
        }
        $char = ($this->contains('>', $noll)) ? '>' : '';

        return '<' . $element . $char;
    }

    // Build html
    private function buildHtml()
    {
        foreach ($this->build as $build) {
            if (! empty($this->options['collapse_whitespace'])) {
                if (\trim($build['content']) === '') {
                    continue;
                }

                if ($build['type'] !== 'content' && ! \in_array($build['name'], $this->elements['inline'], true)) {
                    $build['content'] = \trim($build['content']);
                }
            }

            $this->output .= $build['content'];
        }

        $this->build = [];
    }

    // Compact content
    private function compact($content, $name, $element)
    {
        if ($this->skip !== 0) {
            $name = $this->skipName;
        }
        else {
            $content = \preg_replace('/\s+/', ' ', $content);
        }

        if (\in_array($name, $this->elements['skip'], true)) {
            return $content;
        }
        if (\in_array($name, $this->elements['hard'], true)
            || $this->head) {
            return $this->minifyHard($content);
        }

        return $this->minifyKeepSpaces($content);
    }

    // Check if string contains string
    private function contains($needle, $haystack)
    {
        return \str_contains($haystack, $needle);
    }

    // Find name by part
    private function findName($part)
    {
        $name_cut = \explode(' ', $part, 2)[0];
        $name_cut = \explode('>', $name_cut, 2)[0];
        $name_cut = \explode("\n", $name_cut, 2)[0];
        $name_cut = \preg_replace('/\s+/', '', $name_cut);

        return \strtolower(\str_replace('/', '', $name_cut));
    }

    // Minify all, even spaces between elements
    private function minifyHard($element)
    {
        $element = \preg_replace('!\s+!', ' ', $element);
        $element = \trim($element);

        return \trim($element);
    }

    // Strip but keep one space
    private function minifyKeepSpaces($element)
    {
        return \preg_replace('!\s+!', ' ', $element);
    }

    // Remove comments
    private function removeComments($content = '')
    {
        return \preg_replace('/(?=<!--)([\s\S]*?)-->/', '', $content);
    }

    // Remove unneeded element meta
    private function removeMeta($element, $name)
    {
        if ($name === 'style') {
            $element = \str_replace(
                [
                    ' type="text/css"',
                    "' type='text/css'",
                ],
                ['', ''],
                $element
            );
        }
        elseif ($name === 'script') {
            $element = \str_replace(
                [
                    ' type="text/javascript"',
                    " type='text/javascript'",
                ],
                ['', ''],
                $element
            );
        }

        return $element;
    }

    // Remove unneeded self slash
    private function removeSelfSlash($element)
    {
        if (\substr($element, -3) === ' />') {
            if (\in_array(\substr($element, 1, \strpos($element, ' ') - 1), $this->elements['slash'], true)) {
                $element = \substr($element, 0, -3) . '/>';
            }
            else {
                $element = \substr($element, 0, -3) . '>';
            }
        }

        return $element;
    }

    // Set skip if elements are blocked from minification
    private function setSkip($name, $type)
    {
        foreach ($this->elements['skip'] as $element) {
            if ($element === $name && $this->skip === 0) {
                $this->skipName = $name;
            }
        }
        if (\in_array($name, $this->elements['skip'], true)) {
            if ($type === 'open') {
                ++$this->skip;
            }
            if ($type === 'close') {
                --$this->skip;
            }
        }
    }

    // Strip whitespace from element
    private function stripWhitespace($element)
    {
        if ($this->skip === 0) {
            $element = \preg_replace('/\s+/', ' ', $element);
        }

        return \trim($element);
    }

    // Create element
    private function toElement($element, $noll, $name)
    {
        $element = $this->stripWhitespace($element);
        $element = $this->addChevrons($element, $noll);
        $element = $this->removeSelfSlash($element);

        return $this->removeMeta($element, $name);
    }

    // Return type of element
    private function toType($element)
    {
        return (\substr($element, 1, 1) === '/') ? 'close' : 'open';
    }

    // Walk trough html
    private function walk(&$part)
    {
        $all_parts = \explode('>', $part);
        $tag_parts = [];

        if (\count($all_parts) >= 2) {
            $tail = \array_pop($all_parts);
            $tag_parts[] = \implode('>', $all_parts);
            $tag_parts[] = $tail;
        }
        else {
            $tag_parts = $all_parts;
        }

        $tag_content = $tag_parts[0];

        if (! empty($tag_content)) {
            $name = $this->findName($tag_content);
            $element = $this->toElement($tag_content, $part, $name);
            $type = $this->toType($element);

            if ($name === 'head') {
                $this->head = $type === 'open';
            }

            $this->build[] = [
                'name' => $name,
                'content' => $element,
                'type' => $type,
            ];

            $this->setSkip($name, $type);

            // if (! empty($tag_content)) {
                $content = (isset($tag_parts[1])) ? $tag_parts[1] : '';
                if ($content !== '') {
                    $this->build[] = [
                        'content' => $this->compact($content, $name, $element),
                        'type' => 'content',
                    ];
                }
            // }

            $this->buildHtml();
        }
    }
}
