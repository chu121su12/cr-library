<?php

namespace CR\Library\Helpers;

use RuntimeException;

class MaybeIgnoredException extends RuntimeException {}
