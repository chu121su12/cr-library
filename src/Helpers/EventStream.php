<?php

namespace CR\Library\Helpers;

use Exception;
use Illuminate\Support\Facades\Log;
use Throwable;

class EventStream implements \Illuminate\Contracts\Support\Responsable
{
    const IDLE_THRESHOLD = 1000;

    const INCREMENT_LIMIT = 5;

    const PING_INTERVAL_LIMIT = 15;

    const WATCH_DOG_LIMIT = 30;

    private $counterIncrement = 1;

    private $handler;

    private $headers;

    private $loopCounter = 0;

    private $loopStartTime;

    private $once = false;

    private $pingInterval = 0;

    private $trace;

    private $watchDog = 0;

    public function __construct(callable $handler, $headers = null)
    {
        $this->handler = $handler;

        $this->headers = $headers === null ? [
            'Content-Type' => 'text/event-stream',
        ] : $headers;

        $this->trace = new Exception('Original trace');
    }

    public static function once(callable $handler, $headers = null)
    {
        return tap(new static($handler, $headers), static function ($instance) {
            $instance->once = true;
        });
    }

    public function end($reply = null)
    {
        if ($reply) {
            $this->json('_finish', optional($reply)->toArray());
        }
        else {
            $this->json('_finish', '');
        }
    }

    public function comment($data)
    {
        echo ":{$data}\n\n";
        \flush();
    }

    public function data($event, $data)
    {
        echo "event:{$event}\n";
        echo "data:{$data}\n\n";
        \flush();
    }

    public function reply(Reply $reply)
    {
        $this->json('_reply', $reply->toArray());
    }

    public function json($event, $data)
    {
        $this->data($event, \json_encode($data));
    }

    public function ping()
    {
        $this->comment('!');
    }

    public function toResponse($request)
    {
        return response()->stream(function () {
            try {
                \ob_end_flush();
            }
            catch (Exception $e) {
                $emptyObMessage = 'ob_end_flush(): failed to delete and flush buffer. No buffer to delete or flush';

                if ($e->getMessage() !== $emptyObMessage) {
                    throw $e;
                }
            }

            try {
                $this->loopStreamCallable();
            }
            catch (Exception $e) {
                Helpers::reportExceptionStack($e);
                Helpers::reportExceptionStack($this->trace);
            }

            $this->end();
        }, 200, $this->headers);
    }

    protected function loopStreamCallable()
    {
        $loopHardLimit = 8 * 60 * 60;
        $shouldPing = false;

        while (true) {
            if ($this->loopCounter >= $loopHardLimit) {
                throw new Exception('Watchdog counter limit reached');
            }

            $e = null;

            try {
                $this->loopStartTime = Helpers::microTime();

                if ($shouldPing) {
                    $shouldPing = false;
                    $this->pingInterval = 0;
                    $this->ping();
                }

                if (value($this->handler, $this) === false) {
                    return;
                }

                if ($this->once) {
                    return;
                }

                if ($this->watchDog > 0) {
                    --$this->watchDog;
                }
            }
            catch (Exception $e) {
            }
            catch (Throwable $e) {
            }

            if (isset($e)) {
                $this->watchDog += 3;

                Log::info(\sprintf('eventStream: exception [%s] %s', $this->watchDog, $e->getMessage()));

                if ($this->watchDog >= self::WATCH_DOG_LIMIT) {
                    throw new Exception('Watchdog limit reached', 0, $e);
                }

                Helpers::reportIgnoredException('Error running eventStream callback.', $e);
            }

            \flush();

            if (\microtime(true) - $this->loopStartTime->format('U.u') < self::IDLE_THRESHOLD) {
                if ($this->counterIncrement < self::INCREMENT_LIMIT) {
                    ++$this->counterIncrement;
                }
            }
            else {
                $this->counterIncrement = 1;
            }

            \sleep($this->counterIncrement);

            if (! $shouldPing) {
                $this->pingInterval += (int) $this->counterIncrement;

                if ($this->pingInterval >= self::PING_INTERVAL_LIMIT) {
                    $shouldPing = true;
                }
            }

            $this->loopCounter += $this->counterIncrement;
        }
    }
}
