<?php

namespace CR\Library\Helpers;

use Brick\Math\BigDecimal;
use Brick\Math\BigInteger;
use Brick\Math\Exception\MathException;
use Brick\Money\Money;
use Carbon\CarbonImmutable;
use Closure;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ClosureValidationRule;
use Illuminate\Validation\Validator as BaseValidator;
use ReflectionFunction;
use stdClass;

class CastingValidator extends BaseValidator
{
    private static $CASTING_VARNAME = 'isCasting';

    public static function make(array $data, array $rules, array $messages = [], array $attributes = [])
    {
        Validator::resolver(static function (...$args) {
            return new self(...$args);
        });

        $casting = Validator::make(...\func_get_args());

        Validator::resolver(static function (...$args) {
            return new BaseValidator(...$args);
        });

        return $casting;
    }

    public function validated()
    {
        if (! $this->messages) {
            $this->passes();
        }

        throw_if($this->messages->isNotEmpty(), $this->exception, $this);

        $results = [];

        $missingValue = new stdClass;

        foreach ($this->getRules() as $key => $rules) {
            $value = data_get($this->getData(), $key, $missingValue);

            if ($this->excludeUnvalidatedArrayKeys
                && in_array('array', $rules)
                && $value !== null
                && ! empty(preg_grep('/^' . preg_quote($key, '/') . '\.+/', array_keys($this->getRules())))) {
                continue;
            }

            if ($value !== $missingValue) {
                foreach ($this->castValue($rules, $value, $key) as $castedKey => $castedValue) {
                    Arr::set($results, $castedKey, $castedValue);
                }
            }
        }

        return $this->replacePlaceholders($results);
    }

    private function castValue(array $rules, $value, $key)
    {
        foreach ($rules as $rule) {
            if ($rule instanceof ClosureValidationRule) {
                $thirdCallableParameters = data_get((new ReflectionFunction($rule->callback))->getParameters(), '3');

                if (optional($thirdCallableParameters)->getName() === self::$CASTING_VARNAME) {
                    $value = value($rule->callback, $key, $value, static function () {}, $rules);

                    return \is_array($value) ? $value : [$key => $value];
                }
            }
        }

        return [$key => $value];
    }

    public static function asDateTime($fromFormat = null)
    {
        return static function ($attribute, $value, Closure $fail, $isCasting) use ($fromFormat) {
            if ($fromFormat === null && $isCasting) {
                foreach (\is_array($isCasting) ? $isCasting : Arr::get($isCasting->getRules(), $attribute) as $rule) {
                    if (\is_string($rule) && \str_starts_with($rule, 'date_format:')) {
                        $fromFormat = \substr($rule, 12);

                        break;
                    }
                }
            }

            $value = $fromFormat === null
                ? CarbonImmutable::parse($value)
                : CarbonImmutable::createFromFormat($fromFormat, $value);

            if ($value === false || $value === null) {
                if ($fromFormat === null) {
                    $fail("The {$attribute} field must be a date/datetime value.");
                }
                else {
                    $fail("The {$attribute} field must be a date/datetime value of format {$fromFormat}.");
                }
            }

            if ($isCasting) {
                return $value;
            }
        };
    }

    public static function asDecimal()
    {
        return static function ($attribute, $value, Closure $fail, $isCasting) {
            try {
                $value = BigDecimal::of($value);
            }
            catch (MathException $_e) {
                $fail("The {$attribute} field must be a decimal value.");
            }

            if ($isCasting) {
                return $value;
            }
        };
    }

    public static function asInteger()
    {
        return static function ($attribute, $value, Closure $fail, $isCasting) {
            try {
                $value = BigInteger::of($value);
            }
            catch (MathException $_e) {
                $fail("The {$attribute} field must be an integer value.");
            }

            if ($isCasting) {
                return $value;
            }
        };
    }

    public static function asMoney($currency)
    {
        return static function ($attribute, $value, Closure $fail, $isCasting) use ($currency) {
            try {
                $value = Money::of($value, $currency);
            }
            catch (MathException $_e) {
                $fail("The {$attribute} field must be a currency value of {$currency}.");
            }

            if ($isCasting) {
                return $value;
            }
        };
    }

    public static function failIf($condition, $message)
    {
        if (value($condition)) {
            self::once($message, static function ($_attribute, $message, Closure $fail) {
                $fail($message);
            });
        }
    }

    public static function once($value, ...$validations)
    {
        self::make([
            '__value' => $value = value($value),
        ], [
            '__value' => $validations,
        ])->validate();

        return $value;
    }
}
