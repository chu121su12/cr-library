<?php

namespace CR\Library\Helpers;

use Brick\Math\BigInteger;
use Exception;
use Illuminate\Support\Facades\Cache;

class SequenceGeneratorBigInt
{
    const CUSTOM_EPOCH = '1577836800000';

    const NODE_ID = 0;

    const NODE_ID_BITS = 10;

    const SEQUENCE_BITS = 10;

    private static $cacheKey;

    private static $customEpoch;

    private static $instance;

    private static $maxSequence;

    private $lastTimestamp;

    private $sequence;

    public function nextId()
    {
        $currentTimestamp = self::timestamp();

        if ($currentTimestamp->isLessThan($this->lastTimestamp)) {
            throw new Exception('Invalid System Clock!');
        }

        if ($currentTimestamp->isEqualTo($this->lastTimestamp)) {
            $this->saveSequence($this->sequence->plus(1)->and_(self::$maxSequence));
            if ($this->sequence->isZero()) {
                $currentTimestamp = $this->waitNextMillis($currentTimestamp);
            }
        }
        else {
            $this->saveSequence(BigInteger::zero());
        }

        $this->lastTimestamp = $currentTimestamp;

        return (string) $currentTimestamp
            ->shiftedLeft(self::NODE_ID_BITS + self::SEQUENCE_BITS)
            ->or_(BigInteger::of(self::NODE_ID)->shiftedLeft(self::SEQUENCE_BITS))
            ->or_($this->sequence);
    }

    private function saveSequence($sequence)
    {
        $this->sequence = $sequence;

        Cache::store('internal')->put(self::$cacheKey, (string) $sequence, Helpers::FOREVER_SECONDS);
    }

    private function waitNextMillis($currentTimestamp)
    {
        while ($currentTimestamp->isEqualTo($this->lastTimestamp)) {
            $currentTimestamp = self::timestamp();
        }

        return $currentTimestamp;
    }

    public static function make()
    {
        if (! isset(self::$instance)) {
            self::$cacheKey = 'global.sequence-generator.' . self::CUSTOM_EPOCH;

            self::$customEpoch = BigInteger::of(self::CUSTOM_EPOCH);

            self::$instance = tap(new self, static function ($instance) {
                $sequence = Cache::store('internal')
                    ->get(self::$cacheKey) ?: '0';

                $instance->sequence = BigInteger::of($sequence);

                $instance->lastTimestamp = BigInteger::one()->negated();

                $instance->sequence = BigInteger::zero();
            });

            self::$maxSequence = BigInteger::of(2)->power(10)->minus(1);
        }

        return self::$instance;
    }

    private static function timestamp()
    {
        $microtime = \microtime();
        $space = \strpos($microtime, ' ');
        $timestamp = \substr($microtime, $space + 1) . \substr($microtime, 2, 3);

        return BigInteger::of($timestamp)->minus(self::$customEpoch);
    }
}
