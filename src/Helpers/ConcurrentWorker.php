<?php

namespace CR\Library\Helpers;

use duncan3dc\Forker\Fork;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ConcurrentWorker
{
    const WATCH_DOG_LIMIT = 30;

    private $awaits = [];

    private $concurrency = 1;

    private $drainedCallback;

    private $delay = 0;

    private $fake = false;

    private $handler;

    private $inFlight = 0;

    private $loopTrace;

    private $notifyCallback;

    private $paused = false;

    private $queue = [];

    private $sourceTrace;

    private $stash = [];

    private $watchDog = 0;

    public function concurrency($concurrency)
    {
        $this->concurrency = (int) $concurrency;

        return $this;
    }

    public function fake($set = true)
    {
        $this->fake = $set;

        return $this;
    }

    public function handler($callable)
    {
        $this->handler = $callable;

        return $this;
    }

    public function drained($callable)
    {
        $this->drainedCallback = $callable;

        return $this;
    }

    public function delay($delay)
    {
        $this->delay = (int) $delay;

        return $this;
    }

    public function notify($callable)
    {
        $this->notifyCallback = $callable;

        return $this;
    }

    public function push($callables)
    {
        foreach (Arr::wrap($callables) as $callable) {
            if ($this->paused) {
                $this->stash[] = $callable;
            }
            else {
                $this->queue[] = $callable;
            }
        }

        $this->next();
    }

    public function unshift($callables)
    {
        foreach (Arr::wrap($callables) as $callable) {
            if ($this->paused) {
                \array_unshift($this->stash, $callable);
            }
            else {
                \array_unshift($this->queue, $callable);
            }
        }

        $this->next();
    }

    public function pause()
    {
        if (! $this->paused) {
            $this->paused = true;
            $this->stash = $this->queue;
            $this->queue = [];
        }
    }

    public function resume()
    {
        if ($this->paused) {
            $this->paused = false;
            $this->queue = $this->stash;
            $this->stash = [];
            $this->next();
        }
    }

    public function clear()
    {
        $this->paused = false;
        $this->queue = [];
        $this->stash = [];
        $this->sourceTrace = null;
    }

    protected function next()
    {
        if (! $this->sourceTrace) {
            $this->sourceTrace = new Exception('Origin trace');
        }

        $this->loopTrace = new Exception('Latest trace');

        try {
            while (true) {
                if (! $this->runLoop()) {
                    break;
                }
            }
        }
        catch (Exception $e) {
            throw new AsyncException($e, [$this->loopTrace, $this->sourceTrace]);
        }

        $this->sourceTrace = null;
    }

    private function runLoop()
    {
        if ($this->paused) {
            return false;
        }

        while ($this->inFlight < $this->concurrency && \count($this->queue)) {
            $this->createFork(\array_shift($this->queue));

            if (\count($this->queue) && $this->delay) {
                \sleep($this->delay);
            }

            return true;
        }

        while (\count($this->awaits)) {
            foreach ($this->awaits as $await) {
                if ($this->resolveFork(...$await)) {
                    return true;
                }
            }
        }

        return \count($this->queue) > 0;
    }

    protected function createFork($task)
    {
        $fork = new Fork;

        $key = Str::random();

        $callable = $this->watchDogCallable(function () use ($key, $task) {
            try {
                $result = ($handler = $this->handler)
                    ? $handler($task, $key)
                    : $task($key);

                Cache::store('internal')->put("fork::r-{$key}", $result, Helpers::FOREVER_SECONDS);
            }
            catch (Exception $e) {
                Cache::store('internal')->put("fork::e-{$key}", $e, Helpers::FOREVER_SECONDS);

                throw $e;
            }
        });

        if ($this->fake) {
            $callable();

            $pid = null;
        }
        else {
            $pid = $fork->call($callable);
        }

        ++$this->inFlight;

        $this->awaits[$key] = [$key, $fork, $pid];

        return $this->awaits[$key];
    }

    protected function resolveFork($key, $fork, $pid)
    {
        if (! isset($this->awaits[$key])) {
            return false;
        }

        if ($this->fake) {
            unset($this->awaits[$key]);
        }
        else {
            if ($fork->isRunning($pid)) {
                return false;
            }

            unset($this->awaits[$key]);

            value($this->watchDogCallable(static function () use ($fork) {
                $fork->wait();
            }));
        }

        --$this->inFlight;

        $result = Cache::pull("fork::r-{$key}");
        $exception = Cache::pull("fork::e-{$key}");
        if ($this->notifyCallback) {
            value($this->watchDogCallable(function () use ($result, $exception) {
                value($this->notifyCallback, $result, $exception);
            }));
        }

        if ($this->inFlight === 0 && $this->drainedCallback) {
            value($this->watchDogCallable(function () {
                value($this->drainedCallback);
            }));
        }

        return true;
    }

    protected function watchDogCallable($callable)
    {
        return function () use ($callable) {
            try {
                Helpers::withExceptionErrorHandler($callable);

                if ($this->watchDog > 0) {
                    --$this->watchDog;
                }
            }
            catch (Exception $e) {
                $this->watchDog += 3;

                Log::info(\sprintf('fork: exception [%s] %s', $this->watchDog, $e->getMessage()));

                if ($this->watchDog >= self::WATCH_DOG_LIMIT) {
                    throw new Exception('Watchdog limit reached', 0, $e);
                }

                Helpers::reportIgnoredException('Error running fork callback.', $e);
            }
        };
    }
}
