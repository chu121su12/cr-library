<?php

namespace CR\Library\Helpers;

use LogicException;
use RuntimeException;

class Uncategorized
{
    /**
     * Usage:
     * - V1 <empty>
     *   - empty: empty string
     *
     * - V2 <digit> <L|R> <P|T> <Z|S>
     *   - digit: how many digit to pad
     *   - L|R: pad left/right
     *   - P|T: pad only/trim if longer than digit
     *   - Z|S: pad with zero/space
     *
     * - V3 <digit> <L|R> <any> <P|T>
     *   - any: string, first char
     */
    public static function pad($count, $format, $input = null)
    {
        if (\func_num_args() === 2) {
            $input = $format;
            $format = $count;
        }
        elseif (\func_num_args() === 3) {
            $format = $count . $format;
        }
        else {
            throw new LogicException('Invalid pad format value');
        }

        if (! \is_string($format)) {
            throw new LogicException('Invalid pad format value');
        }

        $format = \trim($format);

        if ($format === '') {
            return $input;
        }

        $matches1 = [];
        $matches2 = [];
        $canTrim = false;

        if (\preg_match('/^(\d+)([LR])([PT])([SZ])$/i', $format, $matches1)) {
            $size = (int) $matches1[1];
            $direction = \strtoupper($matches1[2]) === 'L' ? \STR_PAD_LEFT : \STR_PAD_RIGHT;
            $canTrim = \strtoupper($matches1[3]) === 'T';
            $char = \strtoupper($matches1[4]) === 'Z' ? '0' : ' ';
        }
        elseif (\preg_match('/^(\d+)([LR])(.+)([PT])$/i', $format, $matches2)) {
            $size = (int) $matches1[1];
            $direction = \strtoupper($matches1[2]) === 'L' ? \STR_PAD_LEFT : \STR_PAD_RIGHT;
            $canTrim = \strtoupper($matches1[4]) === 'T';
            $char = $matches1[3][0];
        }
        else {
            throw new LogicException("Invalid pad format `{$format}`");
        }

        if (\strlen($input) <= $size) {
            return \str_pad($input, $size, $char, $direction);
        }

        if ($canTrim) {
            return \substr($input, 0, $size);
        }

        throw new LogicException('Data is longer than digit');
    }

    public static function parseLltllvFields($line, $remainingKey = '__REMAINING__')
    {
        $tags = [$remainingKey => ''];
        $kv = ['', ''];

        while (\mb_strlen($line) > 4) { // 4 minimum lltllv 1010
            $temp = $line;

            for ($i = 0; $i < 2; ++$i) {
                $lengthLength = \substr($temp, 0, 1);
                if (! \is_numeric($lengthLength)) {
                    break 2;
                }

                $lengthLength = (int) $lengthLength;
                $length = \substr($temp, 1, $lengthLength);
                if (! \is_numeric($length)) {
                    break 2;
                }

                $lengthLength = 1 + $lengthLength;
                $length = (int) $length;
                $kv[$i] = \substr($temp, $lengthLength, $length);
                $temp = \substr($temp, $lengthLength + $length);
            }

            $tags[$kv[0]] = $kv[1];
            $line = $temp;
        }

        $tags[$remainingKey] = $line === false ? '' : $line;

        return $tags;
    }

    public static function createLltllv($array)
    {
        $llvArray = [];

        foreach ($array as $key => $value) {
            $llvArray[] = $key;
            $llvArray[] = $value;
        }

        return self::createLlv($llvArray);
    }

    public static function parseLlvArray($line, $remainingKey = '__REMAINING__')
    {
        $values = [];

        while (\mb_strlen($line) > 2) { // 2 minimum llv 10
            $lengthLength = \substr($line, 0, 1);
            if (! \is_numeric($lengthLength)) {
                break;
            }

            $lengthLength = (int) $lengthLength;
            $length = \substr($line, 1, $lengthLength);
            if (! \is_numeric($length)) {
                break;
            }

            $lengthLength = 1 + $lengthLength;
            $length = (int) $length;
            $values[] = \substr($line, $lengthLength, $length);
            $line = \substr($line, $lengthLength + $length);
        }

        $values[$remainingKey] = $line === false ? '' : $line;

        return $values;
    }

    public static function createLlv($array)
    {
        $lines = [];

        foreach ($array as $value) {
            $valueLength = \mb_strlen($value);
            $lengthLength = \mb_strlen('' . $valueLength);

            if ($lengthLength > 9) {
                throw new RuntimeException('llv value length cannot exceed 10^9 characters');
            }

            $lines[] = \sprintf('%s%s%s', $lengthLength, $valueLength, $value);
        }

        return \implode('', $lines);
    }

    public static function parseTlvField($field, $line, $lengthLength = 2)
    {
        if (! \str_starts_with($line, $field)) {
            return ['', false];
        }

        $fieldLength = \strlen($field);
        $length = \substr($line, $fieldLength, $lengthLength);
        if (! \is_numeric($length)) {
            return ['', false];
        }

        $length = (int) $length;
        $result = \substr($line, $fieldLength + $lengthLength, $length);
        $resultLength = \strlen($result);
        if ($resultLength < $length) {
            return [$result, false];
        }

        $matchLength = $resultLength + $lengthLength + $fieldLength;
        if ($matchLength === \strlen($line)) {
            return [$result === false ? '' : $result, true];
        }

        return [$result, \substr($line, $matchLength)];
    }

    public static function parseTlvFields($line, $tagLength = 2, $valueLengthLength = 2, $remainingKey = '__REMAINING__')
    {
        $tags = [$remainingKey => ''];
        $tvLength = (int) ($tagLength + $valueLengthLength);

        while (\mb_strlen($line) > $tvLength) {
            $length = \substr($line, $tagLength, $valueLengthLength);

            if (! \is_numeric($length)) {
                break;
            }

            $length = (int) $length;
            $tags[\substr($line, 0, $tagLength)] = \substr($line, $tvLength, $length);
            $line = \substr($line, $tvLength + $length);
        }

        $tags[$remainingKey] = $line === false ? '' : $line;

        return $tags;
    }

    public static function createTlv($array, $tagLength = 2, $valueLengthLength = 2)
    {
        $lines = [];

        $maxValueLength = 10 ** $valueLengthLength;

        foreach ($array as $key => $value) {
            if (\mb_strlen($key) > $tagLength) {
                throw new RuntimeException("tlv tag length cannot exceed {$tagLength} characters");
            }

            $valueLength = \mb_strlen($value);
            if ($valueLength > $maxValueLength) {
                throw new RuntimeException("tlv value length cannot exceed 10^{$valueLengthLength} characters");
            }

            $lines[] = \sprintf(
                '%s%s%s',
                self::pad('2LPZ', $key),
                self::pad('2LPZ', $valueLength),
                $value
            );
        }

        return \implode('', $lines);
    }

    public static function generateRateLimitIpKey($request = null)
    {
        $request = $request ?: request();

        $possibleIpList = collect($request->server)->only([
            'HTTP_CLIENT_IP',
            'HTTP_X_FORWARDED_FOR',
            'HTTP_X_FORWARDED',
            'HTTP_X_CLUSTER_CLIENT_IP',
            'HTTP_FORWARDED_FOR',
            'HTTP_FORWARDED',
            'REMOTE_ADDR',
        ]);

        return \sha1($possibleIpList->filter()->join('|'));
    }

    public static function generateRateLimitUserLikeKey($username)
    {
        return \sha1(\strtoupper(\preg_replace('/[^@.\w-]+/', '', $username)));
    }

    private static function clamp($current, $min, $max)
    {
        return \max($min, \min($max, $current));
    }

    private static function clampColor($current, $min = 0, $max = 255)
    {
        return self::clamp($current, $min, $max);
    }

    private static function rand($min, $max)
    {
        return (int) \mt_rand((int) $min, (int) $max);
    }

    public static function generateBasicImageCaptchaChallange($length = 4)
    {
        $captcha = (string) self::rand((int) (10 ** ($length - 1)), (int) ((10 ** $length) - 1));

        $ch = 10;

        $h = 22;
        $pad = $ch / 4;
        $w = (int) ($ch * strlen($captcha) + 2 * $pad);
        $count = $w * $h;

        $bg = [
            [22, 10],
            [86, 15],
            [165, 20],
        ];
        \shuffle($bg);

        $im = \imagecreatetruecolor($w, $h);

        $randomGrayish = function ($min = 0, $max = 255) use (&$im) {
            $color = self::rand($min, $max);

            return \imagecolorallocate(
                $im,
                self::clampColor($color + self::rand(-10, 10)),
                self::clampColor($color + self::rand(-10, 10)),
                self::clampColor($color + self::rand(-10, 10))
            );
        };

        $bgc = \imagecolorallocate(
            $im,
            self::clampColor($bg[0][0] + self::rand(-$bg[0][1], $bg[0][1])),
            self::clampColor($bg[1][0] + self::rand(-$bg[1][1], $bg[1][1])),
            self::clampColor($bg[2][0] + self::rand(-$bg[2][1], $bg[2][1]))
        );

        \imagefill($im, 0, 0, $bgc);

        for ($i = self::rand($count * 2 / 1000, $count * 4 / 1000); $i > 0; --$i) {
            \imageline(
                $im,
                0,
                self::rand(0, $h - 1),
                $w,
                self::rand(0, $h - 1),
                $randomGrayish(100, 200)
            );
        }

        for ($i = self::rand($count * 10 / 100, $count * 20 / 100); $i > 0; --$i) {
            \imagesetpixel(
                $im,
                self::rand(0, $w - 1),
                self::rand(0, $h - 1),
                $randomGrayish()
            );
        }

        \imagerotate($im, 1, $bgc);
        \imagerotate($im, -1, $bgc);

        for ($i = \strlen($captcha) - 1; $i >= 0; --$i) {
            \imagestring(
                $im,
                (int) (self::rand(1, 2) * 3),
                (int) ($i * $ch + $pad + self::rand(-$ch / 4, $ch / 4)),
                self::rand(0, $h / 4),
                $captcha[$i],
                $randomGrayish(220)
            );
        }

        $png = Helpers::safeOb(function () use ($im) {
            \imagepng($im);
        });

        return [\base64_encode($png), $captcha];
    }

    public static function generateDayJumpDates($jump = 1, $start = null, $end = null, $maxJump = 28)
    {
        list($dateStart, $dateEnd1) = Helpers::sqlDateRange(
            $start === null ? Helpers::microTime() : $start,
            $end
        );

        $dateEnd = $dateEnd1->subDay();

        $jump = (int) $jump;
        if ($jump < 1) {
            $jump = 1;
        }
        elseif ($jump > (int) $maxJump) {
            $jump = (int) $maxJump;
        }
        --$jump;

        $loopDate = $dateStart;
        $loopNextDate = $loopDate->addDay($jump);

        $dates = [];
        while ($loopDate->lte($dateEnd)) {
            $dates[] = [$loopDate, $loopNextDate->min($dateEnd)];
            $loopDate = $loopNextDate->addDay();
            $loopNextDate = $loopDate->addDay($jump);
        }

        return [
            'start' => $dateStart,
            'end' => $dateEnd,
            'dates' => $dates,
        ];
    }

    /** @deprecated */
    public static function generateBasedTimeId($time = null, $counter = 0)
    {
        $baseTime = $time ?: micro_time();

        // 9999  7pr  [3]
        $yearPart = \str_pad(\base_convert($baseTime->format('Y'), 10, 36), 3, '0', STR_PAD_LEFT);

        // 1231  y7  [2]
        $datePart = \str_pad(\base_convert($baseTime->format('md'), 10, 36), 2, '0', STR_PAD_LEFT);

        $microsecondPart = \substr(\str_pad($baseTime->format('u'), 6, '0', \STR_PAD_LEFT), 0, 4);

        // 24*60*60*10000  u5h1en  [6]
        $timePart = \str_pad(\base_convert($baseTime->secondsSinceMidnight() . $microsecondPart, 10, 31), 6, '0', STR_PAD_LEFT);

        if (! ($counter >= 0 && $counter < 36)) {
            throw new RuntimeException('Counter must be number 0 to 36');
        }

        return $yearPart . $datePart . $timePart . \base_convert((string) $counter, 10, 36);
    }

    public static function generateBasedTimeId10000($time = null)
    {
        $baseTime = $time ?: micro_time();

        // 9999  7pr  [3]
        $yearPart = \str_pad(\base_convert($baseTime->format('Y'), 10, 36), 3, '0', STR_PAD_LEFT);

        // 1231  y7  [2]
        $datePart = \str_pad(\base_convert($baseTime->format('md'), 10, 36), 2, '0', STR_PAD_LEFT);

        $microsecondPart = \substr(\str_pad($baseTime->format('u'), 6, '0', \STR_PAD_LEFT), 0, 4);

        // 24*60*60*10000  u5h1en  [6]
        $timePart = \str_pad(\base_convert($baseTime->secondsSinceMidnight() . $microsecondPart, 10, 31), 6, '0', STR_PAD_LEFT);

        return $yearPart . $datePart . $timePart;
    }

    public static function generateBasedTimeId100($time = null)
    {
        $baseTime = $time ?: micro_time();

        // 9999  7pr  [3]
        $yearPart = \str_pad(\base_convert($baseTime->format('Y'), 10, 36), 3, '0', STR_PAD_LEFT);

        // 1231  y7  [2]
        $datePart = \str_pad(\base_convert($baseTime->format('md'), 10, 36), 2, '0', STR_PAD_LEFT);

        $microsecondPart = \substr(\str_pad($baseTime->format('u'), 6, '0', \STR_PAD_LEFT), 0, 4);

        // 24*60*60*100  m2o00  [5]
        $timePart = \str_pad(\base_convert($baseTime->secondsSinceMidnight() . $microsecondPart, 10, 25), 5, '0', STR_PAD_LEFT);

        return $yearPart . $datePart . $timePart;
    }

    // [
    //     attempts: Int,
    //     delayType: exponential|linear|constant|list,
    //       - exponential: [...delays[0] ^ (n * 2)],
    //       - linear: [...delays[0] * (n + 1)],
    //       - constant: [...delays[0]],
    //     delays: [...duration],
    //     maxDelay: duration,
    //     resetAfter: duration,
    // ]

    // rl [
    //     attempts: 600,
    //     resetAfter: 60s,
    // ]

    // retry [
    //     delayType: constant,
    //     delays: [5s],
    //     maxDelay: 5s,
    // ]

    // backoff [
    //     delayType: exponential,
    //     delays: [5s],
    //     maxDelay: 5m,
    //     resetAfter: 10m,
    // ]
}
