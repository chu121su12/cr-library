<?php

namespace CR\Library\Helpers;

use ErrorException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait ExperimentalBatchTrait
{
    protected $continueLoop = 100;

    protected $console;

    public function dbt($connectionDotTableNames)
    {
        list($connection, $table) = \explode('.', $connectionDotTableNames, 2);

        return DB::connection($connection)->table($table);
    }

    public function forkEach($concurrency = 1, $fake = false)
    {
        $outputs = [];
        do {
            $items = Collection::wrap($this->select())->values()->all();

            if (\count($items) === 0) {
                break;
            }

            $fork = new ForkEach(function ($item) {
                return $this->handle($item);
            });

            $outputs[] = $fork->fake($fake)->concurrency($concurrency)->runWithOutput($items);
        }
        while (--$this->continueLoop > 0);

        if (\count($outputs) !== 0) {
            return \array_merge(...$outputs);
        }

        return [];
    }

    public function handle($item)
    {
        throw new ErrorException(\sprintf('Class %s contains method %s that must provide an implementation', self::class, __METHOD__));
    }

    public function log($level, $message, $context = [])
    {
        Log::{$level}($message, $context);

        if (isset($this->console)) {
            $this->console->line($message, $level);
            if (\count($context)) {
                $this->console->comment(\json_encode($context));
            }
        }
    }

    public function logInfo($message, $context = [])
    {
        $this->log('info', $message, $context);
    }

    public function select()
    {
        throw new ErrorException(\sprintf('Class %s contains method %s that must provide an implementation', self::class, __METHOD__));
    }

    public function setConsole($console)
    {
        $this->console = $console;

        return $this;
    }
}
