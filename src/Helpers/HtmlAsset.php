<?php

namespace CR\Library\Helpers;

use Exception;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class HtmlAsset
{
    private static $instance = [];

    private $assets = [];

    public function __toString()
    {
        return $this->toHtmlElements();
    }

    public function addCssFile($path)
    {
        $this->addAsset($path, ['type' => 'css', 'mode' => 'file']);
    }

    public function addFontPreload($link)
    {
        $this->addAsset($link, ['type' => 'font', 'mode' => 'url']);
    }

    public function addHtmlFile($path)
    {
        $this->addAsset($path, ['type' => 'html', 'mode' => 'file']);
    }

    public function addJsFile($path)
    {
        $this->addAsset($path, ['type' => 'js', 'mode' => 'file']);
    }

    public function addRawCss($content)
    {
        $this->addAsset($content, ['type' => 'css', 'mode' => 'content']);
    }

    public function addRawHtml($content)
    {
        $this->addAsset($content, ['type' => 'html', 'mode' => 'content']);
    }

    public function addRawJs($content)
    {
        $this->addAsset($content, ['type' => 'js', 'mode' => 'content']);
    }

    public function addScript($src)
    {
        $this->addAsset($src, ['type' => 'js', 'mode' => 'url']);
    }

    public function addStylesheet($link)
    {
        $this->addAsset($link, ['type' => 'css', 'mode' => 'url']);
    }

    public function toCss()
    {
        return $this->toScript('css');
    }

    public function toHtmlElements($type = null)
    {
        if ($type === null) {
            return $this->toHtmlElementsDelegate(null);
        }

        return $this->toHtmlElementsDelegate($type);
    }

    public function toJs()
    {
        return $this->toScript('js');
    }

    public function toPrefetchHtmlElements()
    {
        $elements = '';

        foreach ($this->assets as $asset) {
            if ($asset['mode'] !== 'url') {
                continue;
            }

            switch ((string) $asset['type']) {
                case 'js':
                    $elements .= \sprintf('<link rel="prefetch" as="script" href="%s">', e($asset['content']));

                    break;

                case 'css':
                    $elements .= \sprintf('<link rel="prefetch" as="style" href="%s">', e($asset['content']));

                    break;
            }
        }

        return new HtmlString($elements);
    }

    private function addAsset($content, array $asset)
    {
        $asset['content'] = $content;
        $this->assets[\sha1($content)] = $asset;
    }

    private function linkType($filePath)
    {
        switch (\strtoupper(Str::beforeLast($filePath, '.'))) {
            case 'woff2': return 'font/woff2';
        }
    }

    private function toHtmlElementsDelegate($type)
    {
        $elements = '';

        foreach ($this->assets as $asset) {
            if ($type === null) {
                // all in order
            }
            elseif ($asset['type'] !== $type) {
                // by type
                continue;
            }

            switch ((string) $asset['mode']) {
                case 'content':
                    switch ((string) $asset['type']) {
                        case 'html':
                            $elements .= $asset['content'];

                            break;

                        case 'js':
                            $elements .= \sprintf('<script>%s</script>', $asset['content']);

                            break;

                        case 'css':
                            $elements .= \sprintf('<style>%s</style>', $asset['content']);

                            break;
                    }

                    break;

                case 'file':
                    switch ((string) $asset['type']) {
                        case 'html':
                            $elements .= \file_get_contents($asset['content']);

                            break;

                        case 'js':
                            $elements .= \sprintf('<script>%s</script>', \file_get_contents($asset['content']));

                            break;

                        case 'css':
                            $elements .= \sprintf('<style>%s</style>', \file_get_contents($asset['content']));

                            break;
                    }

                    break;

                case 'url':
                    switch ((string) $asset['type']) {
                        case 'js':
                            $elements .= \sprintf('<script src="%s"></script>', e($asset['content']));

                            break;

                        case 'css':
                            $elements .= \sprintf('<link href="%s" rel="stylesheet">', e($asset['content']));

                            break;

                        case 'font':
                            if ($type = $this->linkType($asset['content'])) {
                                $elements .= \sprintf('<link href="%s" rel="preload" as="font" type="%s" crossorigin>', e($asset['content']), e($type));
                            }

                            break;
                    }

                    break;
            }
        }

        return new HtmlString($elements);
    }

    private function toScript($type)
    {
        $script = '';

        foreach ($this->assets as $asset) {
            if ($asset['type'] === $type) {
                switch ((string) $asset['mode']) {
                    case 'content':
                        $script .= $asset['content'];

                        break;

                    case 'file':
                        $script .= \file_get_contents($asset['content']);

                        break;

                    case 'url':
                        $script .= \file_get_contents($asset['content']);

                        break;
                }
            }
        }

        return new HtmlString($script);
    }

    public static function make($key = 'default')
    {
        if (isset(self::$instance[$key])) {
            return self::$instance[$key];
        }

        return self::$instance[$key] = new self;
    }

    public static function toCssLink($key = 'default', $base = '')
    {
        if (! isset(self::$instance[$key])) {
            throw new Exception;
        }

        $element = \sprintf('<link href="%s" rel="stylesheet">', e(\sprintf(
            '%s/%s.css?cache=%s',
            $base,
            $key,
            \round(\time() / 1)
        )));

        return new HtmlString($element);
    }

    public static function toCssResponse($key = 'default')
    {
        return new Response(static::make($key)->toCss(), 200, [
            'Content-Type' => 'text/css',
        ]);
    }

    public static function toJsLink($key = 'default', $base = '')
    {
        if (! isset(self::$instance[$key])) {
            throw new Exception;
        }

        $element = \sprintf('<script src="%s"></script>', e(\sprintf(
            '%s/%s.js?cache=%s',
            $base,
            $key,
            \round(\time() / 1)
        )));

        return new HtmlString($element);
    }

    public static function toJsResponse($key = 'default')
    {
        return new Response(static::make($key)->toJs(), 200, [
            'Content-Type' => 'text/javascript',
        ]);
    }
}
