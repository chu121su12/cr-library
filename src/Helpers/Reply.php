<?php

namespace CR\Library\Helpers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

class Reply implements \Illuminate\Contracts\Support\Responsable
{
    const PRINT_FORCE = 0;

    const PRINT_PRINTABLE_PREVIEW = 1;

    const PRINT_PREVIEW_ONLY = 2;

    protected $actions = [];

    protected $httpCode = 200;

    protected $payload;

    protected $redirect;

    public static function __callStatic($method, $parameters)
    {
        return (new self)->{$method}(...$parameters);
    }

    public function addAction($payload)
    {
        $this->actions[] = $payload;

        return $this;
    }

    public function addAlert($value)
    {
        return $this->addAction([
            'type' => 'alert',
            'body' => $value,
        ]);
    }

    public function addDanger($value)
    {
        return $this->addAction([
            'type' => 'message',
            'level' => 'danger',
            'body' => $value,
        ]);
    }

    public function addDownloadContent($content, $filename, $contentType = null)
    {
        return $this->addAction([
            'type' => 'download-content',
            'body' => \base64_encode($content),
            'name' => $filename,
            'contentType' => $contentType,
        ]);
    }

    public function addDownloadUrl($url, $filename)
    {
        return $this->addAction([
            'type' => 'download-url',
            'url' => $url,
            'name' => $filename,
        ]);
    }

    public function addLinkedMessage($message, $url, $actionName = null)
    {
        return $this->addAction([
            'type' => 'message',
            'level' => 'info',
            'body' => $message,
            'url' => $url,
            'actionName' => $actionName,
        ]);
    }

    public function addMessage($value)
    {
        return $this->addAction([
            'type' => 'message',
            'level' => 'info',
            'body' => $value,
        ]);
    }

    public function addOpenInNewTab($url)
    {
        return $this->addAction([
            'type' => 'new-tab',
            'url' => $url,
        ]);
    }

    public function addPreviewHtml($dom)
    {
        return $this->addAction([
            'type' => 'preview-html',
            'body' => $dom,
            'mode' => self::PRINT_PREVIEW_ONLY,
        ]);
    }

    public function addPrintablePreviewHtml($dom)
    {
        return $this->addAction([
            'type' => 'preview-html',
            'body' => $dom,
            'mode' => self::PRINT_PRINTABLE_PREVIEW,
        ]);
    }

    public function addPrintHtml($dom)
    {
        return $this->addAction([
            'type' => 'preview-html',
            'body' => $dom,
            'mode' => self::PRINT_FORCE,
        ]);
    }

    public function addSessionMessage($value)
    {
        return $this->addAction([
            'type' => 'message',
            'session' => true,
            'body' => $value,
        ]);
    }

    public function addWarn($value)
    {
        return $this->addAction([
            'type' => 'message',
            'body' => $value,
        ]);
    }

    public function alert($value, ...$components)
    {
        return $this->addAlert(\sprintf($value, ...$components));
    }

    public function code($code)
    {
        $this->httpCode = $code;

        return $this;
    }

    public function danger($value, ...$components)
    {
        return $this->addDanger(\sprintf($value, ...$components));
    }

    public function downloadContent($content, $filename, $contentType = null)
    {
        return $this->addDownloadContent($content, $filename, $contentType);
    }

    public function downloadFile($path, $filename = null, $contentType = null)
    {
        return $this->addDownloadContent(\file_get_contents($path), $filename ?: \basename($path), $contentType);
    }

    public function downloadUrl($url, $filename)
    {
        return $this->addDownloadUrl($url, $filename);
    }

    public function message($value, ...$components)
    {
        return $this->addMessage(\sprintf($value, ...$components));
    }

    public function openInNewTab($value)
    {
        return $this->addOpenInNewTab($value);
    }

    public function payload($payload)
    {
        $this->payload = $payload;

        return $this;
    }

    public function previewHtml($dom)
    {
        return $this->addPreviewHtml($dom);
    }

    public function printablePreviewHtml($dom)
    {
        return $this->addPrintablePreviewHtml($dom);
    }

    public function printHtml($dom)
    {
        return $this->addPrintHtml($dom);
    }

    public function redirect($url, $type = 'href')
    {
        return $this->withRedirect($url, $type);
    }

    public function response($response)
    {
        return $response;
    }

    public function sessionMessage($value, ...$components)
    {
        return $this->addSessionMessage(\sprintf($value, ...$components));
    }

    public function toArray()
    {
        return \array_merge(
            [
                '_actions' => $this->actions,
                '_redirect' => $this->redirect,
            ],
            Collection::wrap($this->payload)->all()
        );
    }

    public function toResponse($request)
    {
        return new JsonResponse($this->toArray(), $this->httpCode);
    }

    public function warn($value)
    {
        return $this->addWarn($value);
    }

    public function withoutAction()
    {
        $this->actions = [];

        return $this;
    }

    public function withoutRedirect()
    {
        $this->redirect = null;

        return $this;
    }

    public function withRedirect($url, $type = 'href')
    {
        $this->redirect = [
            'type' => $type,
            'url' => $url,
        ];

        return $this;
    }

    public function withReplaceSelf($url = null)
    {
        return $this->withRedirect($url, 'replace');
    }
}
