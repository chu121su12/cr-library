<?php

namespace CR\Library\Helpers;

use Exception;
use Illuminate\Support\Facades\Cache;

class SequenceGenerator
{
    const CUSTOM_EPOCH = '1577836800000';

    const NODE_ID = 0;

    const NODE_ID_BITS = 10;

    const SEQUENCE_BITS = 10;

    private static $cacheKey;

    private static $customEpoch;

    private static $instance;

    private static $maxSequence;

    private $lastTimestamp;

    private $sequence;

    public function nextId()
    {
        $currentTimestamp = self::timestamp();

        if ($currentTimestamp < $this->lastTimestamp) {
            throw new Exception('Invalid System Clock!');
        }

        if ($currentTimestamp === $this->lastTimestamp) {
            $this->saveSequence(($this->sequence + 1) & self::$maxSequence);
            if ($this->sequence === 0) {
                $currentTimestamp = $this->waitNextMillis($currentTimestamp);
            }
        }
        else {
            $this->saveSequence(0);
        }

        $this->lastTimestamp = $currentTimestamp;

        $id = $currentTimestamp << (self::NODE_ID_BITS + self::SEQUENCE_BITS);
        $id |= (self::NODE_ID << self::SEQUENCE_BITS);
        $id |= $this->sequence;

        return $id;
    }

    private function saveSequence($sequence)
    {
        Cache::store('internal')->put(self::$cacheKey, $this->sequence = $sequence, Helpers::FOREVER_SECONDS);
    }

    private function waitNextMillis($currentTimestamp)
    {
        while ($currentTimestamp === $this->lastTimestamp) {
            $currentTimestamp = self::timestamp();
        }

        return $currentTimestamp;
    }

    public static function make()
    {
        if (! isset(self::$instance)) {
            if (\strlen(\decbin(~0)) < 64) {
                return SequenceGeneratorBigInt::make();
            }

            self::$cacheKey = 'global.sequence-generator.' . self::CUSTOM_EPOCH;

            self::$customEpoch = self::CUSTOM_EPOCH;

            self::$instance = tap(new self, static function ($instance) {
                $instance->sequence = Cache::store('internal')
                    ->get(self::$cacheKey);

                $instance->lastTimestamp = -1;

                $instance->sequence = 0;
            });

            self::$maxSequence = (int) (2 ** 10 - 1);
        }

        return self::$instance;
    }

    private static function timestamp()
    {
        $microtime = \microtime();
        $space = \strpos($microtime, ' ');
        $timestamp = \substr($microtime, $space + 1) . \substr($microtime, 2, 3);

        return ((int) $timestamp) - self::$customEpoch;
    }
}
