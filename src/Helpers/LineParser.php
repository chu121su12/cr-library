<?php

namespace CR\Library\Helpers;

use DateTimeInterface;
use Exception;

class LineParser
{
    public static function build($data, $howto, $newlineReplacement = ' ')
    {
        $return = [];
        $key = null;

        try {
            $i = 0;
            foreach ($howto as $key => $rules) {
                ++$i;

                $value = isset($data[$key]) ? $data[$key] : '';

                $lineValue = self::buildRule3x1($value, $rules);

                if ($newlineReplacement && \is_string($lineValue)) {
                    $lineValue = \preg_replace('/[\r\n]/', $newlineReplacement, $lineValue);
                }

                self::parseRule2($lineValue, $rules);
                $paddedValue = self::buildRule1x0($lineValue, $rules);

                $return[] = $paddedValue;
            }
        }
        catch (Exception $e) {
            throw new Exception("Error on rule #{$i} [{$key}]: {$e->getMessage()}", $e->getCode(), $e);
        }

        return \implode('', $return);
    }

    // $howto:
    // 'field' => [            // field name
    //   4*35,                 // length              \  trim/ltrim/rtrim
    //   [' ', STR_PAD_RIGHT], // strpad spec         /  spec
    //   '\d+',                // regex match / null
    //   [                     // chunk + length / date + date format
    //     'chunk', 35
    //     ||
    //     'date', 'Y-m-d'
    //     ||
    //     'callback', function
    //   ]
    // ],

    public static function parse($line, $howto)
    {
        $position = 0;
        $return = [];
        $key = null;

        try {
            $i = 0;
            foreach ($howto as $key => $rules) {
                ++$i;

                $rawValue = self::parseRule0($line, $position, $rules);
                $trimmedValue = self::parseRule1($rawValue, $rules);
                self::parseRule2($trimmedValue, $rules);
                $normalizedValue = self::parseRule3($trimmedValue, $rules);

                $return[$key] = $normalizedValue;
                $position += $rules[0];
            }
        }
        catch (Exception $e) {
            throw new Exception("Error on rule #{$i} [{$key}]: {$e->getMessage()}", $e->getCode(), $e);
        }

        return $return;
    }

    protected static function buildRule1x0($value, $rule)
    {
        return self::strPad($value, $rule[0], $rule[1]);
    }

    protected static function buildRule3x1($value, $rule)
    {
        if (isset($rule[3])) {
            switch ((string) $rule[3][0]) {
                case 'date':
                    if ($value instanceof DateTimeInterface) {
                        $value = self::dateFormat($value, $rule[3][1]);
                    }
                    elseif (\is_string($value)) {
                        $date = self::dateCreateFromFormat($rule[3][1], $value);
                        if (! $date) {
                            throw new Exception("Wrong date format: '{$value}'; expected: '{$rule[3][1]}'.");
                        }
                    }

                    break;

                case 'chunk':
                    if (\mb_strpos($value, "\n") !== false) {
                        $value = \explode("\n", $value);
                    }

                    if (\is_array($value)) {
                        foreach ($value as $k1 => $v1) {
                            $value[$k1] = self::strPad($v1, $rule[3][1], $rule[1]);
                        }

                        $value = \implode('', $value);
                    }

                    break;
            }
        }

        return $value;
    }

    protected static function parseRule0($line, $position, $rule)
    {
        return \mb_substr($line, $position, $rule[0]);
    }

    protected static function parseRule1($value, $rule)
    {
        return self::trim($value, $rule[1]);
    }

    protected static function parseRule2($value, $rule)
    {
        if ($rule[2]) {
            if (! \preg_match("~{$rule[2]}~", $value)) {
                throw new Exception("Value '{$value}' doesn't match regex rule '{$rule[2]}'.");
            }
        }
    }

    protected static function parseRule3($value, $rule)
    {
        if (isset($rule[3])) {
            switch ((string) $rule[3][0]) {
                case 'date':
                    $date = self::dateCreateFromFormat($rule[3][1], $value);
                    if (! $date) {
                        throw new Exception("Wrong date format: '{$value}'; expected: '{$rule[3][1]}'.");
                    }

                    break;

                case 'chunk':
                    $value = \str_split($value, $rule[3][1]);
                    if ($value !== false) {
                        foreach ($value as $k1 => $v1) {
                            $value[$k1] = self::trim($v1, $rule[1]);
                        }
                        $value = \array_filter($value);
                    }

                    break;

                case 'callback':
                    $value = $rule[3][1]($value);

                    break;
            }
        }

        return $value;
    }

    private static function dateCreateFromFormat($format, $value)
    {
        if (\mb_strpos($format, 'v') !== false && \mb_substr(\PHP_VERSION, 0, 1) < 7) {
            $format = \str_replace('v', 'u', $format);
        }

        return \date_create_immutable_from_format($format, $value);
    }

    private static function dateFormat($value, $format)
    {
        if (\mb_strpos($format, 'v') !== false && \mb_substr(\PHP_VERSION, 0, 1) < 7) {
            $formatters = \explode('v', $format);
            $values = [];
            foreach ($formatters as $formatToken) {
                $values[] = $value->format($formatToken);
            }

            return \implode(\mb_substr($value->format('u'), 0, 3), $values);
        }

        return $value->format($format);
    }

    private static function strPad($value, $length, $rule = [])
    {
        $length = (int) $length;

        if (\is_array($rule) && \count($rule)) {
            if (\in_array($rule[1], [\STR_PAD_LEFT, \STR_PAD_RIGHT], true)) {
                if (\mb_strlen($value) > $length) {
                    if ($rule[1] === \STR_PAD_LEFT) {
                        return \mb_substr($value, -$length);
                    }

                    return \mb_substr($value, 0, $length);
                }

                return \str_pad($value, $length, $rule[0], $rule[1]);
            }

            throw new Exception('Bad STR_PAD rule.');
        }

        if (\mb_strlen($value) > $length) {
            return \mb_substr($value, 0, $length);
        }

        return \str_pad($value, $length, ' ', \STR_PAD_RIGHT);
    }

    private static function trim($value, $rule = [])
    {
        if (\is_array($rule) && \count($rule)) {
            if ($rule[1] === \STR_PAD_LEFT) {
                return \ltrim($value, $rule[0]);
            }

            if ($rule[1] === \STR_PAD_RIGHT) {
                return \rtrim($value, $rule[0]);
            }

            throw new Exception('Bad STR_PAD rule.');
        }

        return $value;
    }
}
