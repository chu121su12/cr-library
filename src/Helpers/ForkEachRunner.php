<?php

namespace CR\Library\Helpers;

use Exception;
use Illuminate\Support\Collection;
use React\EventLoop\Factory;
use Throwable;

class ForkEachRunner
{
    protected $eventLoop;

    protected $concurrency = 8;

    protected $delay = 10;

    protected $fake = false;

    protected $monitorCallback;

    protected $processorCallback;

    protected $retrieverCallback;

    private $loopCount = 0;

    private $watchDogCount = 0;

    private $watchDogLimit = 10;

    public function __construct($retrieverCallback, $processorCallback)
    {
        $this->eventLoop = Factory::create();

        $this->retrieverCallback = $retrieverCallback;

        $this->processorCallback = $processorCallback;
    }

    public function concurrency($set)
    {
        $this->concurrency = $set;

        return $this;
    }

    public function delay($set)
    {
        $this->delay = $set;

        return $this;
    }

    public function fake($set = true)
    {
        $this->fake = $set;

        return $this;
    }

    public function immediate()
    {
        value($this->eventLoopCallback());

        $this->eventLoop->run();
    }

    public function monitorOutput($callback)
    {
        $this->monitorCallback = $callback;

        return $this;
    }

    public function run()
    {
        $this->eventLoopRegisterCallback();

        $this->eventLoop->run();
    }

    public function runOnce()
    {
        ++$this->loopCount;

        try {
            value($this->monitorCallback, 1, "retrieving data (loop #{$this->loopCount})");

            $collection = Collection::wrap(value($this->retrieverCallback));

            value($this->monitorCallback, 2, "count {$collection->count()}");

            $fork = new ForkEach(function ($item, $index) {
                value($this->monitorCallback, 3, "processing {$index}");

                return value($this->processorCallback, $item);
            });

            $fork->fake($this->fake)
                ->concurrency($this->concurrency)
                ->runWithoutOutput($collection->all());

            value($this->monitorCallback, 0, 'done');

            if ($this->watchDogCount > 0) {
                --$this->watchDogCount;
            }
        }
        catch (Exception $e) {
        }
        catch (Throwable $e) {
        }

        if (isset($e)) {
            Helpers::reportExceptionStack($e);

            value($this->monitorCallback, 10, $e->getMessage());

            $this->watchDogCount += 2;

            if ($this->watchDogCount >= $this->watchDogLimit) {
                throw new Exception('Watchdog limit reached.', 0, $e);
            }
        }
    }

    private function eventLoopCallback()
    {
        return function () {
            if ($this->runOnce() !== false) {
                $this->eventLoopRegisterCallback();
            }
        };
    }

    private function eventLoopRegisterCallback()
    {
        $this->eventLoop->addTimer($this->delay, $this->eventLoopCallback());
    }
}
