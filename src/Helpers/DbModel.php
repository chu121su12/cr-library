<?php

namespace CR\Library\Helpers;

class DbModel extends \Illuminate\Database\Eloquent\Model
{
    use \CR\Library\Laravel\Concerns\EloquentTracksPreviousAttributes;

    protected function initializeStandalone($table, array $options)
    {
        $this->setConnection(isset($options['connection']) ? $options['connection'] : null);

        $this->exists = (bool) (isset($options['exists']) ? $options['exists'] : true);

        $this->timestamps = (bool) (isset($options['timestamps']) ? $options['timestamps'] : false);

        $this->setTable($table);

        $this->setKeyName(isset($options['key']) ? $options['key'] : 'id');

        $this->setKeyType(
            isset($options['keyType'])
            ? $options['keyType']
            : (\is_int($this->getKey()) ? 'int' : 'string')
        );

        $this->mergeCasts(isset($options['casts']) ? (array) $options['casts'] : []);

        if (isset($options['incrementing'])) {
            $this->setIncrementing((bool) $options['incrementing']);
        }

        if (isset($options['dateFormat'])) {
            $this->setDateFormat($options['dateFormat']);
        }

        foreach ([
            'hidden' => 'setHidden',
            'visible' => 'setVisible',
            'fillable' => 'fillable',
            'guarded' => 'guard',
        ] as $option => $method) {
            if (isset($options[$option])) {
                $this->{$method}((array) $options[$option]);
            }
        }
    }

    public static function getPrimitiveCastTypes()
    {
        return static::$primitiveCastTypes;
    }

    public static function newStandalone($table, $attributes = [], array $options = [])
    {
        $model = new self;

        $model->initializeStandalone($table, $options);

        $model->bootIfNotBooted();

        $model->initializeTraits();

        $model->forceFill((array) $attributes);

        $model->syncOriginal();

        $model->syncPrevious();

        return $model;
    }
}
