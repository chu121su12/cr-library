<?php

namespace CR\Library\Helpers;

use duncan3dc\Forker\Fork as Forker;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Throwable;

class ForkEach
{
    const WATCH_DOG_LIMIT = 30;

    private $concurrency = 1;

    private $fake = false;

    private $handler;

    private $watchDog = 0;

    public function __construct(callable $handler)
    {
        $this->handler = $handler;
    }

    public function concurrency($concurrency)
    {
        $this->concurrency = $concurrency;

        return $this;
    }

    public function fake($set = true)
    {
        $this->fake = $set;

        return $this;
    }

    public function runWithoutOutput(array $items)
    {
        $origin = new Exception('Origin');

        try {
            $this->runFork($items, false);
        }
        catch (Exception $e) {
            throw new AsyncException($e, [$origin]);
        }
    }

    public function runWithOutput(array $items)
    {
        $origin = new Exception('Origin');

        try {
            return $this->runFork($items, true);
        }
        catch (Exception $e) {
            throw new AsyncException($e, [$origin]);
        }
    }

    protected function forkGuardedWait($fork)
    {
        $callable = $this->watchDogCallable(static function () use ($fork) {
            $fork->wait();
        });

        $callable();
    }

    protected function runFork(array $items, $retrieveOutput)
    {
        $length = \count($items);

        $items = \array_reverse($items);

        $forks = [];

        $results = [];

        while (\count($items) !== 0) {
            if (\count($forks) < $this->concurrency) {
                $index = $length - \count($items);

                $item = \array_pop($items);

                $key = 'fork-' . Str::random();

                $results[] = $key;

                $fork = new Forker;

                $callable = $this->watchDogCallable(function () use ($retrieveOutput, $item, $index, $key, $fork) {
                    $result = value($this->handler, $item, $index, $fork);

                    if ($retrieveOutput) {
                        Cache::store('internal')->put($key, $result, Helpers::FOREVER_SECONDS);
                    }
                });

                if ($this->fake) {
                    $callable();
                }
                else {
                    $forks[] = $fork;

                    $fork->call($callable);
                }
            }
            else {
                $hasUnset = false;

                foreach ($forks as $key => $fork) {
                    unset($forks[$key]);

                    $hasUnset = true;

                    $this->forkGuardedWait($fork);

                    break;
                }

                if (! $hasUnset) {
                    \sleep(1);
                }
            }
        }

        foreach ($forks as $key => $fork) {
            $this->forkGuardedWait($fork);
        }

        $cached = [];

        if ($retrieveOutput) {
            foreach ($results as $key) {
                $cached[] = Cache::pull($key);
            }
        }

        return \array_reverse($cached);
    }

    protected function watchDogCallable($callable)
    {
        return function () use ($callable) {
            try {
                $callable();

                if ($this->watchDog > 0) {
                    --$this->watchDog;
                }
            }
            catch (Exception $e) {
            }
            catch (Throwable $e) {
            }

            if (isset($e)) {
                $this->watchDog += 3;

                Log::info(\sprintf('fork: exception [%s] %s', $this->watchDog, $e->getMessage()));

                if ($this->watchDog >= self::WATCH_DOG_LIMIT) {
                    throw new Exception('Watchdog limit reached', 0, $e);
                }

                Helpers::reportIgnoredException('Error running fork callback.', $e);
            }
        };
    }
}
