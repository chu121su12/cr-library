<?php

namespace CR\Library\Helpers;

use Brick\Math\BigInteger;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use RuntimeException;

class Locations
{
    public static function viaIp($ip)
    {
        try {
            if (Helpers::checkPrivateIp($ip)) {
                return;
            }
        }
        catch (RuntimeException $_e) {
            return;
        }

        return \str_contains($ip, ':')
            ? self::viaIpv6($ip)
            : self::viaIpv4($ip);
    }

    public static function viaLatLong($latitude, $longitude, $distanceLimit = 100)
    {
        $latitude = \is_numeric($latitude) ? (float) $latitude : 0;

        $longitude = \is_numeric($longitude) ? (float) $longitude : 0;

        if (! (\abs($latitude) < 0.001 && \abs($longitude) < 0.001)) {
            return;
        }

        // https://download.geonames.org/export/dump/cities*.zip file

        $cities = self::collectCacheLine(
            Config::get('app.cr.locations.cities'),
            '_cache-file:::location-ll:::',
            60 * 60 * 24 * 30,
            function ($line) {
                $data = \explode("\t", $line);

                if (isset($data[4]) && isset($data[5])) {
                    $localName = data_get($data, '1');
                    $name = data_get($data, '2');
                    $region = null; // from?
                    $fallbackName = $localName === null ? $name : $localName;

                    return [
                        // 'localName' => $localName,
                        // 'name' => $name,
                        // 'region' => $region,
                        'latitude' => (float) $data[4],
                        'longitude' => (float) $data[5],
                        'normalizedName' => $fallbackName && $region ? "{$fallbackName}, {$region}" : ($fallbackName ?: $region ?: 'Unknown'),
                    ];
                }
            }
        );

        return $cities
            ->map(function ($row) use ($latitude, $longitude) {
                return [
                    'name' => $row['normalizedName'],
                    'distance' => Helpers::haversineLatLong(
                        $row['latitude'],
                        $row['longitude'],
                        $latitude,
                        $longitude
                    ),
                ];
            })
            ->sortBy('distance')
            ->where('distance', '<', $distanceLimit)
            ->pluck('name')
            ->first();
    }

    private static function collectCacheLine($relativePath, $prefix, $ttl, $callback)
    {
        $filePath = $relativePath ? base_path($relativePath) : '';

        if (\file_exists($filePath)) {
            $cacheKey = $prefix . $relativePath;

            $list = Cache::remember($cacheKey, $ttl, function () use ($filePath, $callback) {
                $out = [];

                foreach (\file($filePath) ?: [] as $line) {
                    $out[] = $callback($line);
                }

                return collect($out)->filter();
            });

            if ($list->isEmpty()) {
                Cache::pull($cacheKey);
            }

            return $list;
        }

        return collect();
    }

    private static function ipv4Int($ip)
    {
        $ips = \explode('.', $ip, 5);

        if (\count($ips) === 4) {
            return \array_sum([
                ((int) $ips[0]) * (256 ** 3),
                ((int) $ips[1]) * (256 ** 2),
                ((int) $ips[2]) * (256 ** 1),
                ((int) $ips[3]) * (256 ** 0),
            ]);
        }
    }

    private static function ipv6Int($ip)
    {
        if (! \function_exists('inet_pton')) {
            return;
        }

        $bin = '';
        foreach (\str_split(\inet_pton($ip)) as $char) {
            $bin .= \sprintf('%08b', \ord($char));
        }

        $dec = BigInteger::zero();
        foreach (\str_split($bin) as $bit) {
            $dec = $dec->multipliedBy(2)->plus($bit);
        }

        return $dec;
    }

    private static function viaIpv4($ip)
    {
        // https://github.com/sapics/ip-location-db

        $ips = self::collectCacheLine(
            Config::get('app.cr.locations.ipv4'),
            '_cache-file:::location-i4:::',
            60 * 60 * 24 * 30,
            function ($line) {
                $data = \explode(',', $line);
                $start = self::ipv4Int(data_get($data, '0'));
                $end = self::ipv4Int(data_get($data, '1'));
                $countryCode = data_get($data, '2');

                if ($start !== null && $end !== null && $countryCode !== null) {
                    return [
                        'start' => $start,
                        'end' => $end,
                        'countryCode' => $countryCode,
                    ];
                }
            }
        );

        $ip = self::ipv4Int($ip);

        if ($ip === null) {
            return;
        }

        return $ips
            ->where(function ($line) use ($ip) {
                return $ip >= $line['start'] && $ip <= $line['end'];
            })
            ->pluck('countryCode')
            ->first();
    }

    private static function viaIpv6($ip)
    {
        // https://github.com/sapics/ip-location-db

        $ips = self::collectCacheLine(
            Config::get('app.cr.locations.ipv6'),
            '_cache-file:::location-i6:::',
            60 * 60 * 24 * 30,
            function ($line) {
                $data = \explode(',', $line);
                $start = self::ipv6Int(data_get($data, '0'));
                $end = self::ipv6Int(data_get($data, '1'));
                $countryCode = data_get($data, '2');

                if ($start !== null && $end !== null && $countryCode !== null) {
                    return [
                        'start' => $start,
                        'end' => $end,
                        'countryCode' => $countryCode,
                    ];
                }
            }
        );

        $ip = self::ipv6Int($ip);

        if ($ip === null) {
            return;
        }

        return $ips
            ->where(function ($line) use ($ip) {
                return $ip->isGreaterThanOrEqualTo($line['start']) && $ip->isLessThanOrEqualTo($line['end']);
            })
            ->pluck('countryCode')
            ->first();
    }
}
