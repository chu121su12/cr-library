<?php

namespace CR\Library\Providers;

use CR\Library\Administration\HttpController;
use CR\Library\Administration\ThrowExceptionController;
use Illuminate\Support\Facades\Route;

class AdministrationServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public static $prefix = 'r';

    private $allowIp = [
    ];

    private $blacklistIp = [
    ];

    private $cookieName = '_r_admin';

    private $users;

    public function boot()
    {
        $this->configureRoutes();
    }

    private function configureRoutes()
    {
        Route::group([
            'middleware' => ['throttle:60,1', $this->auth()],
            'prefix' => static::$prefix,
        ], function () {
            Route::post('logout', $this->logout());

            // Route::get('/', HttpController\Home::class);
            // Route::post('/', HttpController\Home::class);

            // Route::get('info', HttpController\Info::class);
            // Route::get('phpinfo', HttpController\Phpinfo::class);
            // Route::get('opcache', HttpController\Opcache::class);

            // Route::get('throw', ThrowExceptionController::class);

            // Route::get('terminal', [HttpController\Terminal::class, 'index']);
            // Route::options('terminal', [HttpController\Terminal::class, 'options']);
            // Route::post('terminal', [HttpController\Terminal::class, 'verify']);

            // Route::get('tinker', [HttpController\Tinker::class, 'index']);
            // Route::options('tinker', [HttpController\Tinker::class, 'options']);
            // Route::post('tinker', [HttpController\Tinker::class, 'verify']);

            // Route::get('status', [HttpController\Status::class, 'index']);
            // Route::options('status', [HttpController\Status::class, 'options']);

            // Route::get('log', [HttpController\FileLogs::class, 'index']);
            // Route::get('log/{name}', [HttpController\FileLogs::class, 'log']);

            // Route::get('file', [HttpController\FileManager::class, 'index']);
            // Route::get('file/asset/{file}', [HttpController\FileManager::class, 'asset']);
            // Route::get('file/fonts/{file}', [HttpController\FileManager::class, 'asset']);

            // Route::get('adminer-compile', [HttpController\Adminer::class, 'compile']);
            // Route::get('adminer', [HttpController\Adminer::class, 'index']);

            // Route::get('routes', HttpController\Routes::class);

            // Route::get('maintenance', [HttpController\Maintenance::class, 'index']);
            // Route::get('maintenance/{command}', [HttpController\Maintenance::class, 'call']);

            // Route::match(['get', 'post'], 'adminer/{id}', [HttpController\Adminer::class, 'database']);
            // Route::match(['get', 'post'], 'file/{section}', [HttpController\FileManager::class, 'section']);
        });
    }

    private function auth()
    {
        return function ($request, $next) {
            return $this->authMiddlewareCallback($request, $next);
        };
    }

    private function authMiddlewareCallback($request, $next)
    {
        $htmlHeader = '<!doctype html><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1">';

        if (\count($this->users()) === 0) {
            return response(
                $htmlHeader
                . 'No users registered'
                . '<style>@media (prefers-color-scheme:dark){'
                    . 'body{background:#212529;color:#f8f9fa}'
                . '}'
            );
        }

        $ip = $request->server->get('REMOTE_ADDR');

        if (\count($this->allowIp) && ! \in_array($ip, $this->allowIp, true)) {
            abort(404);
        }

        if (\in_array($ip, $this->blacklistIp, true)) {
            abort(404);
        }

        $message = '';

        $hash = static function ($str) {
            return \mb_substr(\md5($str), 0, 10);
        };

        $userdata = collect(\json_decode($request->cookie($this->cookieName)));

        if ($userdata->count()) {
            if (($user = $userdata->get('user')) && ($password = $userdata->get('password'))) {
                if (isset($this->users()[$user]) && $hash($this->users()[$user]) === $password) {
                    return $next($request);
                }
            }

            $message = 'Bad authentication';
        }

        if (($user = $request->get('user')) && ($password = $request->get('password'))) {
            if (isset($this->users()[$user]) && $this->users()[$user] === $password) {
                return redirect(static::$prefix)
                    ->cookie($this->cookieName, \json_encode([
                        'user' => $user,
                        'password' => $hash($password),
                    ]), 0, '', '', false, true);
            }

            $message = 'Cannot authenticate';
        }

        if ($request->query('file') === 'app.webmanifest') {
            // return $next($request);
        }

        return response(
            $htmlHeader
            . '<form method="post" action="' . e(url(static::$prefix)) . '">'
            . 'Username: <input name="user" value="' . e($user) . '"><br>'
            . 'Password: <input type="password" name="password"><br>'
            . '<button name="_token" value="' . csrf_token() . '">login</button>'
            . ($message ? '<br><br>' . $message : '')
            . '</form>'
            . '<style>@media (prefers-color-scheme:dark){'
                . 'body,input{background:#212529;color:#f8f9fa}'
            . '}'
        );
    }

    private function logout()
    {
        return function () {
            return back()->cookie($this->cookieName, null, -1, '', '', false, true);
        };
    }

    private function users()
    {
        if (! $this->users) {
            if (config('app.debug')) {
                $this->users = ['debug' => 'debug'];
            }
            else {
                $this->users = config('app.app-administration.passwords');
            }
        }

        return $this->users;
    }
}
