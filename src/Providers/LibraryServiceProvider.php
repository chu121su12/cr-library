<?php

namespace CR\Library\Providers;

use CR\Library\Laravel\EventLogger;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;

class LibraryServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CACHE_NAMES = ['tmp', 'internal', 'runtime'];

    public function boot()
    {
        if (Config::get('app.cr.configure.library') === false) {
            return;
        }

        $this->configureCaches();
        $this->configureEventListeners();
        $this->configureMailers();

        if ($this->app->runningInConsole()) {
            $this->configureConsoleCommands();
        }
        elseif ($kernel = app(\Illuminate\Contracts\Http\Kernel::class)) {
            $kernel->prependMiddleware(\CR\Library\Laravel\HttpMiddleware\LogHttpAccess::class);
            $kernel->prependMiddleware(\CR\Library\Laravel\HttpMiddleware\BlockRootPhpServe::class);
        }
    }

    private function configureConsoleCommands()
    {
        $this->commands([
        ]);
    }

    private function configureEventListeners()
    {
        // auth
        Event::listen(\Illuminate\Auth\Events\Attempting::class, EventLogger\LogAuthAttempt::class);
        Event::listen(\Illuminate\Auth\Events\Authenticated::class, EventLogger\LogAuthSuccess::class);

        // database
        Event::listen(\Illuminate\Database\Events\QueryExecuted::class, EventLogger\LogSqlQuery::class);
        Event::listen(\Illuminate\Database\Events\TransactionCommitted::class, EventLogger\LogTransactionCommitted::class);
        Event::listen(\Illuminate\Database\Events\TransactionRolledBack::class, EventLogger\LogTransactionRollBack::class);
        Event::listen(\Illuminate\Database\Events\TransactionBeginning::class, EventLogger\LogTransactionStart::class);

        // http
        Event::listen(\Illuminate\Http\Client\Events\ConnectionFailed::class, EventLogger\LogHttpClientFailure::class);
        Event::listen(\Illuminate\Http\Client\Events\RequestSending::class, EventLogger\LogHttpClientRequest::class);
        Event::listen(\Illuminate\Http\Client\Events\ResponseReceived::class, EventLogger\LogHttpClientResponse::class);

        // cache
        Event::listen(\Illuminate\Cache\Events\CacheHit::class, EventLogger\LogCacheHit::class);
        Event::listen(\Illuminate\Cache\Events\CacheMissed::class, EventLogger\LogCacheMiss::class);

        // command
        Event::listen(\Illuminate\Console\Events\CommandStarting::class, EventLogger\LogCommandStart::class);
        Event::listen(\Illuminate\Console\Events\CommandFinished::class, EventLogger\LogCommandFinished::class);

        // scheduled task
        Event::listen(\Illuminate\Console\Events\ScheduledTaskStarting::class, EventLogger\LogScheduledTaskStarting::class);
        Event::listen(\Illuminate\Console\Events\ScheduledTaskFinished::class, EventLogger\LogScheduledTaskFinished::class);
        Event::listen(\Illuminate\Console\Events\ScheduledTaskFailed::class, EventLogger\LogScheduledTaskFailed::class);

        // misc
        if ('cli-server' === \PHP_SAPI) {
            Event::listen(\Illuminate\Foundation\Http\Events\RequestHandled::class, EventLogger\RequestHandledServe::class);
        }
    }

    private function configureCaches()
    {
        $cacheList = [
            'tmp' => [
                'driver' => 'file',
                'path' => storage_path('tmp'),
            ],

            'internal' => [
                'driver' => 'file',
                'path' => storage_path('framework/cache/internal'),
            ],

            'runtime' => [
                'driver' => 'array',
                'serialize' => false,
            ],
        ];

        $defaultCaches = (array) Config::get('cache.stores');

        foreach (self::CACHE_NAMES as $key) {
            if (! isset($defaultCaches[$key])) {
                Config::set(["cache.stores.{$key}" => $cacheList[$key]]);
            }
        }
    }

    private function configureMailers()
    {
        $mailerList = [];

        if ($value = \trim(env('MAIL_URL'))) {
            $mailerList[] = [
                'name' => 'smtp',
                'config' => ['mail.mailers.smtp' => ['url' => $value]],
            ];
        }

        $mailerKeys = \explode(',', \trim(env('MAILER_KEYS') ?: ''));

        foreach ($mailerKeys as $key) {
            $upperKey = \strtoupper($key);

            if (\preg_match('/^[A-Z]+([-_A-Z0-9]*[A-Z]+)?$/', $upperKey)) {
                if ($value = \trim(env("MAILER_{$upperKey}_URL"))) {
                    $mailerList[] = [
                        'name' => $key,
                        'config' => ["mail.mailers.{$key}" => ['url' => $value]],
                    ];
                }
            }
        }

        $collection = new Collection($mailerList);

        $names = $collection->pluck('name')->all();

        Config::set(
            $collection->pluck('config')->collapse()
                ->merge([
                    'mail.mailers.failover.transport' => 'failover',
                    'mail.mailers.failover.mailers' => $names,
                    'mail.mailers.roundrobin.transport' => 'roundrobin',
                    'mail.mailers.roundrobin.mailers' => $names,
                ])
                ->all()
        );
    }
}
