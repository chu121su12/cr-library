<?php

namespace CR\Library\Avon\Auth;

class User extends \Illuminate\Database\Eloquent\Model implements \Illuminate\Contracts\Auth\Authenticatable
{
    public function getAuthIdentifierName()
    {
        return 'username';
    }

    public function getAuthIdentifier()
    {
        return $this->{$this->getAuthIdentifierName()};
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        return '';
    }

    public function setRememberToken($value) {}

    public function getRememberTokenName()
    {
        return '';
    }

    public function getAccountName()
    {
        return $this->name;
    }

    public function getAccountId()
    {
        return $this->user_id;
    }

    public function getAccountTeam()
    {
        return $this->team_name;
    }

    public function getAccountType()
    {
        switch ((int) ($r = $this->role)) {
            case 1: return 'Type 1';
            default: return $r;
        }
    }
}
