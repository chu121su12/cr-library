<?php

namespace CR\Library\Avon;

use Illuminate\Support\Arr;

class Panel
{
    use Concerns\AuthorizedToSee;
    use Concerns\Component;

    public $fields;

    public $key;

    public $limit;

    public $toolbar = false;

    protected $component = 'panel';

    protected $name;

    public function __construct($name, $fields = null)
    {
        $this->name = $name;

        foreach ($this->fields = Arr::wrap($fields) as $field) {
            $field->panel = $this->name;
        }
    }

    public function fields()
    {
        return $this->fields;
    }

    public function limit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    public function setName($value)
    {
        $this->name = $value;

        return $this;
    }

    public function toJson()
    {
        return [
            'component' => $this->resolveComponent(),
            'name' => $this->name(),
            'limit' => $this->limit,
            'toolbar' => $this->toolbar,
            'isResourceTool' => false,
            'builtInComponent' => static::class === self::class,
        ];
    }
}
