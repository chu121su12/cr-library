<?php

namespace CR\Library\Avon;

use CR\Library\Helpers\Helpers as LibHelpers;
use Exception;
use Illuminate\Foundation\Application as LaravelApplication;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

/* Installing

    App\Providers\AppServiceProvider

    register()
        $this->app->singleton(Application::class, function () {
            Application::setupAuth('auth-name', function ($request) {
                return User::whereRememberToken($request->bearerToken())->sole();
            });

            return (new Application)
                ->withAuthMiddleware('auth:auth-name')
                ->withPrefix('avon')
                ->withResources([
                    'user' => UserResource::class,
                ]);
        });

    boot()
        app(Application::class)->start();
*/

class Application
{
    public $themeColor = '#000';

    protected $appData = [];

    protected $authCallback;

    protected $authMiddleware;

    protected $authUserClass;

    protected $menuCallback;

    protected $pages;

    protected $prefix = 'avon';

    protected $resourcePrefix = '';

    protected $scripts = [];

    protected $styles = [];

    private $prepared = false;

    private function getAuthenticatableClass()
    {
        if ($class = $this->authUserClass) {
            return $class;
        }

        if ($class = config('auth.providers.users.model')) {
            return $class;
        }

        if (\class_exists('App\User')) {
            return \App\User::class;
        }

        if (\class_exists('App\Models\User')) {
            return \App\Models\User::class;
        }

        return Auth\User::class;
    }

    public function closestIndexUrl()
    {
        if ($result = $this->closestIndexUrlWith(request()->decodedPath(), request()->segments())) {
            return $result;
        }

        if ($result = $this->closestIndexUrlWith(preg_replace('~^[^/]+//[^/]+/~', '', request()->header('referer')), LibHelpers::urlSegments(request()->header('referer')))) {
            return $result;
        }

        return $this->getBaseUrl('/');
    }

    protected function closestIndexUrlWith($url, $routeSegments)
    {
        $resegment = \implode('/', $routeSegments);

        $baseUrl = \trim($this->getBaseUrl('/'), '/');

        if (\str_starts_with($resegment, $baseUrl)) {
            $resegment = \ltrim(\mb_substr($resegment, \mb_strlen($baseUrl)), '/');
        }

        $routeSegments = \explode('/', $resegment);

        if (isset($routeSegments[0]) && $routeSegments[0] === 'api') {
            \array_shift($routeSegments);
        }

        $segmentPosition = \array_search('lens', $routeSegments, true);

        if ($segmentPosition !== false && \count($routeSegments) > $segmentPosition + 1) {
            return $this->getBaseUrl('/' . \implode('/', \array_slice($routeSegments, 0, $segmentPosition + 2)) . '/');
        }

        $segmentPosition = \array_search('resource', $routeSegments, true);

        if ($segmentPosition !== false && \count($routeSegments) > $segmentPosition + 1) {
            return $this->getBaseUrl('/' . \implode('/', \array_slice($routeSegments, 0, $segmentPosition + 2)) . '/');
        }
    }

    public function login($request)
    {
        if (! $this->authCallback) {
            $this->authCallback = Helpers::getDefaultAuthCallback(
                app($this->getAuthenticatableClass())
            );
        }

        return value($this->authCallback, $request->username, $request->password, $request, $this);
    }

    public function globallySearchableResources()
    {
        return $this->getPages()->filter(static function ($page) {
            $pageable = $page['class'];

            return $page['type'] === 'resource' && $pageable::$globallySearchable && \count($pageable::$search);
        });
    }

    public function getAppData()
    {
        $isMainSite = $this->url($this->getPrefix()) === $this->url('/');

        return $this->prepareAppData([
            'globalSearchDebounce' => 0.5,
            'globalSearchResources' => $this->globallySearchableResources()
                ->map(static function ($pages) {
                    return [
                        'resourceName' => $pages['route'],
                        'label' => $pages['class']::label(),
                    ];
                })
                ->values()->all(),
            'headerFollower' => '',
            'logoutMethod' => 'delete',
            'logoutUrl' => $this->getBaseUrl('/login'),
            // 'multipleView' => true,
            'showClock' => true,
            'showFullScreenControl' => false,
            'showLocalMessages' => true,
            'showThemeControl' => true,
            'siteName' => $isMainSite ? null : config('app.name', 'Avon'),
            'siteUrl' => $isMainSite ? null : $this->url('/'),
        ]);
    }

    public function getAssetCache()
    {
        return (bool) config('app.av.asset.cache');
    }

    public function getAssetCacheDuration()
    {
        $cache = config('app.av.asset.cache-duration');

        if ($cache !== null) {
            return (int) $cache;
        }

        return app()->isProduction() ? 60 * 5 : 60;
    }

    public function getAssetCachePrefix()
    {
        return (string) config('app.av.asset.cache-prefix');
    }

    public function getAssetRevision()
    {
        return (string) config('app.av.asset.revision', '20240718');
    }

    public function getBaseAssetUrl($path)
    {
        return $this->getBaseUrl("/application/asset/{$this->getAssetRevision()}{$path}");
    }

    public function getBaseUrl($path = '')
    {
        return \rtrim(\implode('/', [
            \rtrim(request()->getBaseUrl(), '/'),
            \trim($this->getPrefix(), '/'),
        ]), '/') . $path;
    }

    public function getDashboard($dashboard)
    {
        if ($page = $this->getPages()->where('type', 'dashboard')->firstWhere('route', $dashboard)) {
            return $page['class'];
        }
    }

    public function getField($field)
    {
        if ($page = $this->getPages()->where('type', 'field')->firstWhere('route', $field)) {
            return $page['class'];
        }
    }

    public function getMainScript()
    {
        $debug = app()->hasDebugModeEnabled();

        $js = \trim(\file_get_contents(__DIR__ . (
            $debug ? '/res/asset/avon/main.js' : '/res/asset/avon/main.min.js'
        )));

        if (! app()->isLocal()) {
            $js .= ';' . \trim(\file_get_contents(__DIR__ . (
                $debug ? '/res/asset/avon/block.js' : '/res/asset/avon/block.min.js'
            )));
        }

        return $js;
    }

    public function getMainStyle()
    {
        $debug = app()->hasDebugModeEnabled();

        $css = \trim(\file_get_contents(__DIR__ . (
            $debug ? '/res/asset/avon/main.css' : '/res/asset/avon/main.min.css'
        )));

        if (! app()->isLocal()) {
            $css .= \trim(\file_get_contents(__DIR__ . (
                $debug ? '/res/asset/avon/block.css' : '/res/asset/avon/block.min.css'
            )));
        }

        return $css;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function getPublicAssetUrl($type, $path)
    {
        return \preg_replace('~^[^/]+//[^/]+~', '', asset("asset/{$type}{$path}"));
    }

    public function getRateLimitHalting($key)
    {
        if (RateLimiter::attempt("{$key}:l1", 100, LibHelpers::noopFunction(), \mt_rand(55, 75)) === false) {
            return RateLimiter::availableIn("{$key}:l1");
        }

        if (RateLimiter::attempt("{$key}:l2", 10, LibHelpers::noopFunction(), \mt_rand(5, 7)) === false) {
            return RateLimiter::availableIn("{$key}:l2");
        }

        return false;
    }

    public function getRateLimitCaptcha($key)
    {
        if (RateLimiter::attempt($key, 6, LibHelpers::noopFunction(), \mt_rand(55, 75)) === false) {
            return RateLimiter::availableIn($key);
        }

        return false;
    }

    public function getResource($resource)
    {
        if ($page = $this->getPages()->where('type', 'resource')->firstWhere('route', $resource)) {
            return $page['class'];
        }
    }

    public function getResourcePrefix()
    {
        return $this->resourcePrefix;
    }

    public function getTool($tool)
    {
        if ($page = $this->getPages()->where('type', 'tool')->firstWhere('route', $tool)) {
            return $page['class'];
        }
    }

    public function getResourceByClass($resource)
    {
        if ($page = $this->getPages()->where('type', 'resource')->firstWhere('class', \is_object($resource) ? \get_class($resource) : $resource)) {
            return $page['route'];
        }
    }

    public function getVendorScripts()
    {
        return $this->scripts;
    }

    public function getVendorStyles()
    {
        return $this->styles;
    }

    public function getMenu()
    {
        $pages = $this->getPages();

        return with(value($this->menuCallback, $pages) ?: $pages)
            ->filter(static function ($page) {
                return ! \in_array($page['type'], ['field'], true);
            })
            ->map(static function ($page) {
                $pageable = $page['class'];
                $group = Arr::wrap($pageable::group());
                $label = $pageable::label();

                return \array_merge($page, [
                    'group' => $group,
                    'label' => $label,
                    'mayHaveBadge' => false,
                    'notification' => isset($page['notification'])
                        ? $page['notification']
                        : null,
                    'searchKeyword' => (string) $pageable::fuseKeyword(),

                    'fullLabel' => static function () use ($group, $label) {
                        $group[] = $label;

                        return \implode(' / ', $group);
                    },
                    'available' => static function ($request) use ($pageable, $label) {
                        return $label && $pageable::availableForNavigation($request);
                    },
                ]);
            });
    }

    public function getAvailableMenu($request)
    {
        return $this->getMenu()
            ->filter(static function ($page) use ($request) {
                return $page['available']($request);
            })
            ->map(static function ($page) {
                return Arr::only($page, [
                    'group',
                    'label',
                    'mayHaveBadge',
                    'notification',
                    'route',
                    'type',
                    'searchKeyword',
                ]);
            })
            ->groupBy('group')
            ->flatten(1)
            ->values()
            ->all();
    }

    public function getVersion()
    {
        if (app()->isProduction()) {
            return '';
        }

        return \sprintf(
            'Fw.%s En.%s',
            LaravelApplication::VERSION,
            \preg_replace(['/^\D*?(\d+([-.]\d+)*).*/', '/\D+/'], ['$1', ''], \PHP_VERSION)
        );
    }

    public function getViewData()
    {
        $isMainSite = $this->url($this->getPrefix()) === $this->url('/');

        $appId = \substr(\base64_encode(\sha1(config('app.key'), true)), 0, 8);
        $env = \trim(\mb_strtolower(config('app.env') ?: 'production'));
        $env = $env === 'production' ? '' : ".{$env}";
        $version = \trim($this->getVersion());
        $version = $version === '' ? '' : " • {$version}";

        $result = $this->prepareAppData([
            'appId' => $appId,
            'appName' => ($isMainSite ? config('app.name') : '') ?: 'Avon',
            'assetId' => $this->getAssetRevision(),
            'baseUrl' => $this->getBaseUrl(''),
            'footer' => "© 2022–2024 • r.{$appId}{$env}{$version}",
            'forceLogin' => false,
            'locale' => config('app.locale') ?: 'id',
            'loginBg' => '',
            'loginUrl' => $this->url('login'),
            'logoImageUrl' => '',
            'useServiceWorker' => false,
            'publicKey' => null,
        ]);

        if (app()->hasDebugModeEnabled()) {
            $result['debug'] = true;
        }

        return $result;
    }

    public function start()
    {
        View::addNamespace('cr-avon2', __DIR__ . '/res/view');

        $this->prepareResources();
        $this->routesDefinition();
    }

    public function url($path = null, $secure = null)
    {
        if ($secure === null) {
            $configSecureUrl = config('app.av.url.secure');

            if ($configSecureUrl !== null) {
                $secure = (bool) $configSecureUrl;
            }
            elseif ($referrer = request()->headers->get('referer')) {
                $secure = \str_starts_with($referrer, 'https') && \str_starts_with(
                    \preg_replace('/^https?/', '', $referrer),
                    \preg_replace('/^https?/', '', url('/'))
                );
            }
            else {
                $secure = true;
            }
        }

        return url($path, [], $secure);
    }

    public function withAppData(array $appData)
    {
        $this->appData = \array_merge($this->appData, Arr::wrap($appData));

        return $this;
    }

    public function withAuthMiddleware($middleware)
    {
        $this->authMiddleware = $middleware;

        return $this;
    }

    public function withDashboards($dashboards)
    {
        $this->preparePageable('dashboard', $dashboards);

        return $this;
    }

    public function withFields($fields)
    {
        $this->preparePageable('field', $fields, true);

        return $this;
    }

    public function withFieldsBase($fields)
    {
        $this->preparePageable('field', $fields, false);

        return $this;
    }

    public function withAuthenticationUserClass($authUserClass)
    {
        $this->authUserClass = $authUserClass;

        return $this;
    }

    public function withAuthenticationCallback($authCallback)
    {
        $this->authCallback = $authCallback;

        return $this;
    }

    public function withPrefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    public function withResourcePrefix($prefix)
    {
        $this->resourcePrefix = $prefix;

        return $this;
    }

    public function withResources($resources)
    {
        $this->preparePageable('resource', $resources);

        return $this;
    }

    public function withScripts($scripts)
    {
        $this->scripts = \array_merge($this->scripts, Arr::wrap($scripts));

        return $this;
    }

    public function withStyles($styles)
    {
        $this->styles = \array_merge($this->styles, Arr::wrap($styles));

        return $this;
    }

    public function withTools($tools)
    {
        $this->preparePageable('tool', $tools);

        return $this;
    }

    public function withPages($pages)
    {
        Collection::wrap($pages)->each(function ($page) {
            if (\is_a($page, Resource::class, true)) {
                $this->withResources($page);
            }
            elseif (\is_a($page, Tool::class, true)) {
                $this->withTools($page);
            }
            elseif (\is_a($page, Dashboard::class, true)) {
                $this->withDashboards($page);
            }
            elseif (\is_a($page, Fields\Field::class, true)) {
                $this->withFields($page);
            }
            else {
                $pageName = \is_object($page) ? \get_class($page) : $page;

                throw new Exception('Could not add incompatible class of ' . $pageName);
            }
        });

        return $this;
    }

    protected function getPages()
    {
        if (! $this->pages) {
            $this->pages = collect();
        }

        return $this->pages;
    }

    protected function prepareAppData(array $overrides)
    {
        $overrides = \array_merge($overrides, Arr::only($this->appData, \array_keys($overrides)));

        return collect($overrides)->map(function ($value) {
            return value($value, $this);
        })->all();
    }

    protected function prepareResources()
    {
        if ($this->prepared) {
            return;
        }

        $this->prepared = true;

        if (isset($this->resources)) {
            $this->withResources($this->resources);
        }

        if (isset($this->dashboards)) {
            $this->withDashboards($this->dashboards);
        }

        if (isset($this->fields)) {
            $this->withFields($this->fields);
        }

        if (isset($this->tools)) {
            $this->withTools($this->tools);
        }
    }

    protected function preparePageable($type, $pageables, $leafClass = false)
    {
        $pageables = Collection::wrap($pageables)
            ->map(function ($pageable, $key) use ($type, $leafClass) {
                if (! \is_int($key)) {
                    $pageable::aliasUriKey($key);
                }
                elseif ($leafClass) {
                    $pageable::aliasUriKey(Helpers::classToUriKey(Str::afterLast($pageable, '\\')));
                }

                $uriKey = $pageable::uriKey();

                return [
                    'uriKey' => $uriKey,
                    'class' => $pageable,
                    'route' => \str_starts_with($uriKey, $this->resourcePrefix)
                        ? \mb_substr($uriKey, \mb_strlen($this->resourcePrefix))
                        : $uriKey,
                    'type' => $type,
                ];
            });

        $this->pages = $this->getPages()->merge($pageables);
    }

    protected function routesDefinition()
    {
        Route::group([
            'prefix' => $this->prefix,
        ], static function () {
            Route::get('api/application/time', [Controllers\ApiApplicationController::class, 'time']);
            Route::get('api/application/public-key/key', [Controllers\ApiApplicationController::class, 'publicKey']);
            Route::get('api/application/public-key/signature', [Controllers\ApiApplicationController::class, 'publicKeySignature']);
            Route::post('api/application/captcha/request', [Controllers\ApiApplicationController::class, 'requestCaptcha']);
            Route::post('api/application/captcha/solve', [Controllers\ApiApplicationController::class, 'solveCaptcha']);
            Route::post('application/error', [Controllers\ApiApplicationController::class, 'error']);

            Route::group([
                'middleware' => [
                    HttpMiddlewares\Caching::class,
                ],
            ], static function () {
                Route::get('application/version', [Controllers\ViewAssetController::class, 'version']);
                Route::get('application/asset/{id}/script.js', [Controllers\ViewAssetController::class, 'script']);
                Route::get('application/asset/{id}/style.css', [Controllers\ViewAssetController::class, 'style']);
                Route::get('application/asset/{id}/app-scripts.js', [Controllers\ViewAssetController::class, 'appScripts']);
                Route::get('application/asset/{id}/app-styles.css', [Controllers\ViewAssetController::class, 'appStyles']);
                Route::get('application/asset/{id}/vendor-scripts.js', [Controllers\ViewAssetController::class, 'vendorScripts']);
                Route::get('application/asset/{id}/vendor-styles.css', [Controllers\ViewAssetController::class, 'vendorStyles']);
                Route::get('application/asset/{id}/icons/{collection}.json', [Controllers\ViewAssetController::class, 'icons']);

                Route::get('humans.txt', [Controllers\ViewStaticFileController::class, 'humans']);
                Route::get('sw.js', [Controllers\ViewStaticFileController::class, 'serviceWorker']);
                Route::get('application/asset/{id}/favicon.ico', [Controllers\ViewStaticFileController::class, 'favicon']);
                Route::get('application/asset/{id}/icon.png', [Controllers\ViewStaticFileController::class, 'iconTransparent']);
                Route::get('application/asset/{id}/icon-white.png', [Controllers\ViewStaticFileController::class, 'iconWhite']);
                Route::get('application/asset/{id}/site.webmanifest', [Controllers\ViewStaticFileController::class, 'webManifest']);
                Route::get('application/asset/{id}/{path?}', [Controllers\ViewAssetController::class, 'otherPath'])->where('path', '(.+)');
            });

            Route::get('/', [Controllers\ViewController::class, 'home']);
            Route::get('resource/{resource}', [Controllers\ViewController::class, 'index']);
            Route::get('resource/{resource}/new', [Controllers\ViewController::class, 'create']);
            Route::get('resource/{resource}/detail/{resourceId}', [Controllers\ViewController::class, 'detail']);
            Route::get('resource/{resource}/edit/{resourceId}', [Controllers\ViewController::class, 'edit']);
            Route::get('resource/{resource}/replicate/{resourceId}', [Controllers\ViewController::class, 'replicate']);
            Route::get('resource', [Controllers\ViewController::class, 'noResource']);

            Route::get('tool/{tool}', [Controllers\ViewController::class, 'tool']);
            Route::get('dashboard/{dashboard}', [Controllers\ViewController::class, 'dashboard']);

            // Route::get('application/user/logout', [Controllers\UserController::class, 'logoutView']);
            Route::get('application/user/profile', [Controllers\UserController::class, 'profileView']);
            Route::get('application/user/verify-account', [Controllers\UserController::class, 'verifyAccountView']);
            Route::get('application/user/confirm-password', [Controllers\UserController::class, 'confirmPasswordView']);
        });

        Route::group([
            'prefix' => $this->prefix,
        ], static function () {
            Route::get('login', [Controllers\LoginController::class, 'loginView']);
        });

        Route::group([
            'prefix' => $this->prefix,
            'middleware' => [
                'web',
            ],
        ], static function () {
            Route::get('login/csrf', [Controllers\LoginController::class, 'csrf']);
        });

        Route::group([
            'prefix' => $this->prefix,
            'middleware' => [
                HttpMiddlewares\Encrypter::class,
                'web',
            ],
        ], static function () {
            Route::post('login', [Controllers\LoginController::class, 'loginApi']);
        });

        Route::group([
            'prefix' => $this->prefix,
            'middleware' => \array_filter([
                HttpMiddlewares\Encrypter::class,
                $this->authMiddleware,
                'web',
            ]),
        ], static function () {
            Route::delete('login', [Controllers\LoginController::class, 'logoutApi']);

            Route::get('api/tool/{tool}/view', [Controllers\ApiToolController::class, 'view']);
        });

        Route::group([
            'prefix' => $this->prefix,
            'middleware' => \array_filter([
                HttpMiddlewares\Encrypter::class,
                $this->authMiddleware,
                'web',
                HttpMiddlewares\SessionLifetime::class,
            ]),
        ], static function () {
            Route::get('api/application/app-data', [Controllers\ApiApplicationController::class, 'appData']);
            Route::get('api/application/menu', [Controllers\ApiApplicationController::class, 'menu']);
            Route::get('api/application/user', [Controllers\ApiApplicationController::class, 'user']);

            Route::post('api/application/user/update-password', [Controllers\UserController::class, 'updatePassword']);
            Route::post('api/application/user/verify-account', [Controllers\UserController::class, 'verifyAccount']);
            Route::post('api/application/user/confirm-password', [Controllers\UserController::class, 'confirmPassword']);

            Route::get('api/resource/{resource}', [Controllers\ApiResourceController::class, 'index']);
            Route::get('api/resource/{resource}/search', [Controllers\ApiResourceController::class, 'search']);
            Route::get('api/resource/{resource}/count', [Controllers\ApiResourceController::class, 'count']);
            Route::get('api/resource/{resource}/support', [Controllers\ApiResourceController::class, 'support']);
            Route::get('api/resource/{resource}/badge', [Controllers\ApiResourceController::class, 'badge']);
            Route::post('api/resource/{resource}/action/{action}', [Controllers\ApiResourceController::class, 'action']);
            Route::get('api/resource/{resource}/detail/{resourceId}', [Controllers\ApiResourceController::class, 'detail']);
            Route::post('api/resource/{resource}/edit/{resourceId}', [Controllers\ApiResourceController::class, 'edit']);
            Route::post('api/resource/{resource}/replicate/{resourceId}', [Controllers\ApiResourceController::class, 'replicate']);
            Route::post('api/resource/{resource}/update/{resourceId}', [Controllers\ApiResourceController::class, 'update']);
            Route::post('api/resource/{resource}/delete', [Controllers\ApiResourceController::class, 'delete']);
            Route::post('api/resource/{resource}/delete/{resourceId}', [Controllers\ApiResourceController::class, 'deleteById']);
            Route::post('api/resource/{resource}/create', [Controllers\ApiResourceController::class, 'create']);
            Route::post('api/resource/{resource}/store', [Controllers\ApiResourceController::class, 'store']);

            Route::get('api/resource/{resource}/relatable', [Controllers\ApiResourceController::class, 'relatable']);

            Route::get('api/tool/{tool}/data/{key}', [Controllers\ApiToolController::class, 'data']);
            Route::post('api/tool/{tool}/action/{key}', [Controllers\ApiToolController::class, 'action']);
            Route::get('api/tool/{tool}/badge', [Controllers\ApiToolController::class, 'badge']);

            Route::get('api/dashboard/{dashboard}', [Controllers\ApiDashboardController::class, 'metrics']);
            Route::get('api/dashboard/{dashboard}/metric/{metric}', [Controllers\ApiDashboardController::class, 'metric']);
            Route::get('api/dashboard/{dashboard}/badge', [Controllers\ApiDashboardController::class, 'badge']);

            // Route::get('api/tool/{tool}', [Controllers\ToolController::class, 'view']);

            // Route::post('resource/{resource}/destroy/{resourceId}', [Controllers\ResourceController::class, 'destroy']);

            // Route::get('resource/{resource}/edit/{resourceId}/attach/{relation}', [Controllers\ResourceRelationController::class, 'create']);
            // Route::post('resource/{resource}/edit/{resourceId}/attach/{relation}', [Controllers\ResourceRelationController::class, 'attach']);
            // Route::get('resource/{resource}/edit/{resourceId}/edit-attached/{relation}/{relationId}', [Controllers\ResourceRelationController::class, 'edit']);
            // Route::post('resource/{resource}/edit/{resourceId}/edit-attached/{relation}/{relationId}', [Controllers\ResourceRelationController::class, 'update']);
            // Route::post('resource/{resource}/edit/{resourceId}/remove-attached/{relation}/{relationId}', [Controllers\ResourceRelationController::class, 'detach']);
            // Route::post('resource/{resource}/edit/{resourceId}/remove-many-attached/{relation}', [Controllers\ResourceRelationController::class, 'detachMany']);

            Route::get('api/resource/{resource}/detail/{resourceId}/field/{fieldName}', [Controllers\ApiResourceController::class, 'fileDownload']);
            Route::get('api/resource/{resource}/detail/{resourceId}/field/{fieldName}/preview', [Controllers\ApiResourceController::class, 'filePreview']);
            Route::get('api/resource/{resource}/detail/{resourceId}/field/{fieldName}/thumbnail', [Controllers\ApiResourceController::class, 'fileThumbnail']);
            Route::delete('api/resource/{resource}/detail/{resourceId}/field/{fieldName}', [Controllers\ApiResourceController::class, 'fileDeleteApi']);

            Route::get('api/field/{fieldName}/view', [Controllers\ApiFieldController::class, 'view']);
            Route::get('api/field/{fieldName}/data/{key}', [Controllers\ApiFieldController::class, 'data']);
            Route::post('api/field/{fieldName}/action/{key}', [Controllers\ApiFieldController::class, 'action']);

            // Route::get('resource/{resource}/metric/{metric}', [Controllers\ResourceComponentController::class, 'metric']);
            // Route::post('resource/{resource}/action/{action}', [Controllers\ResourceComponentController::class, 'action']);

            // Route::get('resource/{resource}/lens/{lens}', [Controllers\ResourceLensComponentController::class, 'lensView']);
            // Route::get('resource/{resource}/lens/{lens}/index', [Controllers\ResourceLensComponentController::class, 'lens']);
            // Route::post('resource/{resource}/lens/{lens}/action/{action}', [Controllers\ResourceLensComponentController::class, 'action']);
            // Route::get('resource/{resource}/lens/{lens}/detail/{resourceId}', [Controllers\ResourceLensComponentController::class, 'show']);

            // Route::post('tool/{tool}', [Controllers\ToolController::class, 'action']);
        });

        Route::group([
            'prefix' => $this->prefix,
            'middleware' => [
                HttpMiddlewares\Caching::class,
            ],
        ], static function () {
            Route::get('/asset/{any}', [Controllers\AssetController::class, 'publicAssetPath'])->where('any', '.*');
        });
    }
}
