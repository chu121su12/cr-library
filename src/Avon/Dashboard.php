<?php

namespace CR\Library\Avon;

class Dashboard
{
    use Concerns\AuthorizedToSee;
    use Concerns\Component;
    use Concerns\Pageable;

    public function cards($request)
    {
        return [];
    }

    public function getViewData($request)
    {
        return [
            'label' => static::label(),
            'metrics' => collect($this->cards($request))
                ->filter()
                ->filter->authorizedToSee($request)
                ->map->toJson(),
        ];
    }

    public static function group()
    {
        return isset(static::$group) ? static::$group : 'Dashboard';
    }
}
