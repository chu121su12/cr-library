<?php

namespace CR\Library\Avon;

use CR\Library\Avon\Fields\FieldsCollection;
use CR\Library\Helpers\Helpers as LibHelpers;
use Error;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ItemNotFoundException;
use Illuminate\Support\MultipleItemsFoundException;
use Illuminate\Support\Str;

class Helpers
{
    public static function abstractErrorMessage($class, $method)
    {
        return \sprintf(
            'Class %s contains abstract method %s and must therefore be declared abstract or implement the remaining methods',
            $class,
            $method
        );
    }

    public static function classToHtmlId($class)
    {
        $str = \preg_replace(
            '/(?<=[a-z])([A-Z])|(?<=[A-Z])([A-Z])(?=[a-z])/',
            '_\1\2',
            \mb_substr($class, \mb_strrpos($class, '\\') + 1)
        );

        return \mb_strtolower("{$str}");
    }

    public static function getDefaultAuthCallback($userObject)
    {
        return static function ($username, $password, $request, $app) use ($userObject) {
            Log::info("account-default: query attempt ({$username})");

            try {
                $userInstance = $userObject
                    ->where($userObject->getAuthIdentifierName(), $username)
                    ->sole();
            }
            catch (QueryException $e) {
                if (\method_exists($userObject, 'getConnection')) {
                    $config = (array) $userObject->getConnection()->getConfig();
                }
                else {
                    $connectionName = \method_exists($userObject, 'getConnectionName') ? $userObject->getConnectionName() : $e->connectionName;

                    $config = (array) Config::get("database.connections.{$connectionName}");
                }

                if (($host = data_get($config, 'host')) && ($port = data_get($config, 'port'))) {
                    Log::info('account-default: log in db possibly down');

                    $socketOpen = false;

                    try {
                        $socketOpen = LibHelpers::checkSocket($host, $port, (int) data_get($config, 'connect_timeout', 5));
                    }
                    catch (Error $error) {
                        LibHelpers::reportExceptionStack($error);
                    }

                    if (! $socketOpen) {
                        Log::info('account-default: log in db possibly unreachable');
                    }
                }
            }
            catch (ItemNotFoundException $e) {
                Log::info("account-default: missing user ({$username})");
            }
            catch (MultipleItemsFoundException $e) {
                Log::info('account-default: log in db may have more than 1 results');
            }
            catch (Exception $e) {
                throw $e;
            }

            if (! isset($userInstance) || ! $userInstance) {
                return;
            }

            if (isset($e)) {
                throw $e;
            }

            $userInstaceId = $userInstance->getAuthIdentifier();

            if ($userInstaceId !== $username) {
                if (\is_string($userInstaceId) && \trim($userInstaceId) === '') {
                    $idMethod = \get_class($userInstance) . '::' . $userInstance->getAuthIdentifierName();

                    Log::info("account-default: wrong identifier ({$username} != {$userInstaceId}; {$idMethod})");
                }
                else {
                    Log::info("account-default: wrong user ({$username} != {$userInstaceId})");
                }

                return;
            }

            if ($password === '') {
                Log::warning('account-default: password is empty');
            }

            $savedHash = $userInstance->getAuthPassword();

            if (Hash::check($password, LibHelpers::normalizeBcryptHashBeforeChecking($savedHash))) {
                Log::info('account-default: password match (Hash::class)');

                return $userInstance;
            }

            Log::info('account-default: wrong password (Hash::class)');

            if (\md5($password) === $savedHash) {
                Log::warning('account-default: password match (md5)');

                if (config('app.av.insecure-fallback-md5-auth')) {
                    Log::warning('account-default: password allowed from md5');

                    return $userInstance;
                }
            }

            if (app()->isLocal()) {
                Log::warning('account-default: not in production, login bypassed');

                return $userInstance;
            }
        };
    }

    public static function getDefaultResetPasswordCallback($userObject)
    {
        return static function ($_user, $_newPassword, $_currentPassword, $_request, $_app) {
            return false;
        };
    }

    public static function classToProperName($class)
    {
        return \preg_replace(
            [
                '~.*\\\\([^\\\\]+)$~',
                '/(?<=[a-z])([A-Z])|(?<=[A-Z])([A-Z])(?=[a-z])/',
            ],
            [
                '\1',
                ' \1\2',
            ],
            $class
        );
    }

    public static function classToUriKey($class)
    {
        return \mb_strtolower(
            \preg_replace(
                [
                    '/(?<=[a-z])(?=[A-Z])/',
                    '/\\\\+/',
                ],
                [
                    '-',
                    '-',
                ],
                $class
            )
        );
    }

    public static function guessResourceModel($resource)
    {
        if ($resource instanceof Resource) {
            if (\class_exists($model = $resource::resourceModel())) {
                return new $model;
            }
        }
    }

    public static function generateLikeSearchParameter($type, $key, $value)
    {
        switch ((string) $type) {
            case '%':
            case '_%':
            case '%_': return [$key, 'like', LibHelpers::sqlLikeValue($value, (string) $type)];

            case '=':
            default: return [$key, '=', $value];
        }
    }

    public static function generateResourcePreview($request, $resource, $fromSearch = false)
    {
        $app = app(Application::class);
        $uriKey = $app->getResourceByClass($resource);

        if (! $resource->resource()) {
            return [
                'label' => $resource::singularLabel(),
                'resourceName' => $uriKey,

                'id' => null,
                'title' => null,
                'subtitle' => null,
                'retrieved_at' => micro_time(),

                'url' => null,
                'avatarUrl' => null,
            ];
        }

        $indexFields = new FieldsCollection($resource->fieldsForIndex($request));

        $modelKey = $indexFields->idField($request)->resolveToValue($resource) ?: $resource->getIdFieldValue();

        if ($fromSearch) {
            $url = $resource->globalSearchLink($request);

            if ($url === null) {
                $url = 'view';
            }

            switch ($url) {
                case 'view':
                    $url = $app->url("{$app->getPrefix()}/resource/{$uriKey}/detail/{$modelKey}");

                    break;

                case 'edit':
                    $url = $app->url("{$app->getPrefix()}/resource/{$uriKey}/edit/{$modelKey}");

                    break;
            }
        }
        else {
            $url = $resource->authorizedToView($request)
                ? $app->url("{$app->getPrefix()}/resource/{$uriKey}/detail/{$modelKey}")
                : null;
        }

        $avatarField = $modelKey ? $indexFields->avatarField($request) : null;

        return [
            'label' => $resource::singularLabel(),
            'resourceName' => $uriKey,

            'id' => $modelKey,
            'title' => $resource->title(),
            'subtitle' => $resource->subtitle(),
            'retrieved_at' => micro_time(),

            'url' => $modelKey ? $url : null,
            'avatarUrl' => $avatarField
                ? tap($avatarField)
                    ->resolveToValue($resource)
                    ->resolveThumbnailUrl()
                : null,
        ];
    }

    public static function resourceSearch($query, $model, $searchColumns, $searchString)
    {
        if ((string) $searchString === '') {
            return $query;
        }

        return $query->where(
            static function ($where) use ($model, $searchColumns, $searchString) {
                if ($model && $model instanceof Model) {
                    $keyType = $model->getKeyType();
                    if ($keyType === 'int' && backport_is_numeric($searchString)) {
                        $where->orWhere($model->getKeyName(), (int) $searchString);
                    }
                    elseif ($keyType === 'string') {
                        $where->orWhere($model->getKeyName(), $searchString);
                    }
                }

                $searchColumns = $searchColumns->mapWithKeys(static function ($field, $key) {
                    if (\is_int($key) && \is_string($field)) {
                        return [$field => ['operator' => '_%']];
                    }

                    if (\is_string($key) && \is_string($field)) {
                        return [$key => ['operator' => $field]];
                    }

                    return [$key => $field];
                });

                if ($searchColumns->count()) {
                    foreach ($searchColumns as $key => $option) {
                        $parameters = static::generateLikeSearchParameter(
                            $option['operator'],
                            $model && $model instanceof Model ? $model->qualifyColumn($key) : $key,
                            $searchString
                        );

                        $where->orWhere(...$parameters);
                    }
                }
                else {
                    $where->orWhereRaw('0 = 1');
                }
            }
        );
    }

    public static function serverFileUploadLimit()
    {
        // https://stackoverflow.com/a/25370978
        $maxSize = -1;

        $postMaxSize = static::parseSizeString(\ini_get('post_max_size'));
        if ($postMaxSize > 0) {
            $maxSize = $postMaxSize;
        }

        $uploadMaxSize = static::parseSizeString(\ini_get('upload_max_filesize'));
        if ($uploadMaxSize > 0 && $uploadMaxSize < $maxSize) {
            $maxSize = $uploadMaxSize;
        }

        return $maxSize;
    }

    public static function transformCastsToFields($casts)
    {
        return collect($casts)
            ->map(static function ($cast, $attribute) {
                if ($cast instanceof Fields\Field) {
                    return $cast;
                }

                $name = Str::headline($attribute);

                switch ((string) $cast) {
                    case 'int':
                    case 'integer':
                        return Fields\Number::make($name, $attribute);
                    case 'real':
                    case 'float':
                    case 'double':
                        return Fields\Number::make($name, $attribute);
                    case 'decimal':
                        return Fields\Number::make($name, $attribute);
                    case 'string':
                        return Fields\Text::make($name, $attribute);
                    case 'bool':
                    case 'boolean':
                        return Fields\Boolean::make($name, $attribute);
                    case 'object':
                        return Fields\Code::make($name, $attribute)->json();
                    case 'array':
                    case 'json':
                        return Fields\Code::make($name, $attribute)->json();
                    case 'collection':
                        return Fields\Code::make($name, $attribute)->json();
                    case 'date':
                        return Fields\Date::make($name, $attribute)->resolveAsDateTime();
                    case 'datetime':
                    case 'custom_datetime':
                        return Fields\DateTime::make($name, $attribute)->resolveAsDateTime();
                    case 'immutable_date':
                        return Fields\Date::make($name, $attribute)->resolveAsDateTime();
                    case 'immutable_custom_datetime':
                    case 'immutable_datetime':
                        return Fields\DateTime::make($name, $attribute)->resolveAsDateTime();
                    case 'timestamp':
                        return Fields\DateTime::make($name, $attribute)
                            ->resolveAsDateTime()
                            ->exceptOnForms();

                    case 'id':
                        return Fields\ID::make($name, $attribute);
                    default:
                        return Fields\Text::make($name, $attribute);
                }
            })
            ->filter()
            ->values()
            ->all();
    }

    protected static function parseSizeString($size)
    {
        $unit = \preg_replace('/[^bkmgtpezy]/i', '', $size);
        $size = \preg_replace('/[^0-9\.]/', '', $size);

        if ($unit) {
            return \round($size * (1024 ** \stripos('bkmgtpezy', $unit[0])));
        }

        return \round((float) $size);
    }

    public static function authUserLsKey($user = null)
    {
        $user = $user ?: auth()->user();

        if (! $user) {
            return null;
        }

        $id = $user->getAuthIdentifier();
        $remember = '';

        return [
            'base' => 'last-activity:user-id-time::' . $remember . '::' . $id,
            'lock' => 'last-activity:user-id-lock::' . $remember . '::' . $id,
        ];
    }

    public static function logoutUser($request)
    {
        if ($lsKeys = self::authUserLsKey()) {
            Cache::forget($lsKeys['base']);
            Cache::forget($lsKeys['lock']);
        }

        Log::info('account: logging out');

        auth()->logout();

        Log::info('account: logged out - model');

        $request->session()->invalidate();
        $request->session()->regenerateToken();
    }
}
