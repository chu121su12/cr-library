<?php

namespace CR\Library\Avon\Actions;

use CR\Library\Avon\Fields;

class ActionResource extends \CR\Library\Avon\Resource
{
    public static $model = ActionModel::class;

    public static $polling = true;

    public function authorizedToCreate($request)
    {
        return false;
    }

    public function authorizedToDelete($request)
    {
        return false;
    }

    public function authorizedToUpdate($request)
    {
        return false;
    }

    public function fields($r)
    {
        return [
            Fields\ID::make()->asBigInt(),

            Fields\Text::make('Action Name', 'name'),

            Fields\Text::make('User', static function ($x) {
                dd($x);
            }),
        ];
    }
}
