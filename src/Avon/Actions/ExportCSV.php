<?php

namespace CR\Library\Avon\Actions;

use Closure;
use CR\Library\Helpers\Helpers;
use CR\Library\Helpers\Reply;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;

class ExportCSV extends Action
{
    protected $filename = 'file.csv';

    protected $headingCallback;

    protected $modelCallback;

    protected $modelsCallback;

    protected $rowCallback;

    public function __construct()
    {
        $this->headingCallback = static function ($headings) {
            return \array_map(static function ($heading) {
                return \ucwords(\str_replace('_', ' ', $heading));
            }, \array_keys((array) $headings));
        };

        $this->modelCallback = static function ($model) {
            if ($model instanceof Model) {
                return $model->getAttributes();
            }

            if ($model instanceof Arrayable) {
                return $model->toArray();
            }

            if (\is_object($model)) {
                return (array) $model;
            }

            return Arr::wrap($model);
        };

        $this->modelsCallback = static function ($models) {
            return $models;
        };

        $this->rowCallback = static function ($model) {
            return \array_map(static function ($value) {
                return \is_scalar($value) ? $value : \json_encode($value);
            }, (array) $model);
        };
    }

    public function downloadFilename($callback)
    {
        $this->filename = $callback;

        return $this;
    }

    public function handle($fields, $models)
    {
        return $models;
    }

    public function handleResult($fields, $results)
    {
        return (new Reply)->downloadContent(
            $this->makeCsv(collect(Arr::flatten($results))),
            $this->filenameResolver(request(), $fields, $results)
        );
    }

    public function heading($callback)
    {
        $this->headingCallback = $callback;

        return $this;
    }

    public function model($callback)
    {
        $this->modelCallback = $callback;

        return $this;
    }

    public function models($callback)
    {
        $this->modelsCallback = $callback;

        return $this;
    }

    public function row($callback)
    {
        $this->rowCallback = $callback;

        return $this;
    }

    protected function authorizesModel($request, $model)
    {
        if ($this->authorizedToRun($request, $model)) {
            return true;
        }

        if (! \method_exists(Gate::getPolicyFor($model), 'viewAny')) {
            return true;
        }

        return Gate::check('viewAny', $model);
    }

    protected function filenameResolver($request, $fields, $results)
    {
        if (($callback = $this->filename) instanceof Closure) {
            return $callback($request, $fields, $results);
        }

        return $callback;
    }

    protected function makeCsv($models)
    {
        $transform = $this->modelsCallback;

        $models = $transform($models)->map($this->modelCallback);

        return Helpers::safeOb(function () use ($models) {
            tap(\fopen('php://output', 'wb'), function ($handle) use ($models) {
                if (\is_array($heading = $this->resolveHeading($models->first()))) {
                    \fputcsv($handle, $heading);
                }

                foreach ($models as $model) {
                    \fputcsv($handle, $this->resolveRow($model));
                }

                \fclose($handle);
            });
        });
    }

    protected function resolveHeading($firstModel)
    {
        if (isset($this->headingCallback)) {
            $callback = $this->headingCallback;

            return $callback($firstModel);
        }

        return $firstModel;
    }

    protected function resolveRow($model)
    {
        if (isset($this->rowCallback)) {
            $callback = $this->rowCallback;

            return $callback($model);
        }

        return $model;
    }
}
