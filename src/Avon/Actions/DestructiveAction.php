<?php

namespace CR\Library\Avon\Actions;

use Illuminate\Support\Facades\Gate;

abstract class DestructiveAction extends Action
{
    protected $confirmButtonClass = 'text-white bg-red-500 hover:bg-red-600 dark:bg-red-600 dark:hover:bg-red-700';

    protected function authorizesModel($request, $model)
    {
        return $this->authorizedToRun($request, $model) || Gate::check('delete', $model);
    }
}
