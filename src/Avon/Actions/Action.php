<?php

namespace CR\Library\Avon\Actions;

use CR\Library\Avon\Application;
use CR\Library\Avon\Concerns;
use CR\Library\Avon\Fields\FieldsCollection;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Fluent;
use Illuminate\Support\Str;

abstract class Action
{
    use ActionReply;
    use Concerns\ActionVisibility;
    use Concerns\AuthorizedToRun;
    use Concerns\AuthorizedToSee;
    use Concerns\Component;
    use ExecutesAction;

    public $pivotGroup = 'Actions';

    protected $actionFields;

    protected $batchId;

    protected $cancelButtonText = 'Cancel';

    protected $chunkCount = 2;

    protected $chunkLimit = false;

    protected $component = 'default-action';

    protected $confirmButtonClass = 'text-white bg-blue-500 hover:bg-blue-600 dark:bg-blue-600 dark:hover:bg-blue-700';

    protected $confirmButtonText = 'Run';

    protected $confirmText = '';

    protected $pivot = false;

    protected $pivotName = 'pivot';

    protected $prepareQueryCallback;

    protected $ready = true;

    protected $soleModelMessage = '';

    protected $standalone = false; // false, 'none', 'all', 'matching'

    protected $thenCallback;

    protected $transactionProviderCallback;

    protected $withoutActionEvents = true;

    protected $withoutChunk = false;

    protected $withoutConfirmation = false;

    abstract public function handle($fields, $models);

    public function actionClass()
    {
        return $this->confirmButtonClass;
    }

    public function actionKey()
    {
        $prefix = app(Application::class)->getResourcePrefix() . 'resources-actions-';

        $key = $this->key();

        if (\str_starts_with($key, $prefix)) {
            $key = \mb_substr($key, \mb_strlen($prefix));
        }

        return Str::slug(\sprintf(
            '%s %s %s',
            $this->pivot ? 'pivot' : 'action',
            $this->pivotGroup,
            $key
        ));
    }

    public function asPivotAction()
    {
        $this->pivot = true;
    }

    public function cancelButtonText($text)
    {
        $this->cancelButtonText = $text;

        return $this;
    }

    public function confirmButtonText($text)
    {
        $this->confirmButtonText = $text;

        return $this;
    }

    public function confirmText($text)
    {
        $this->confirmText = $text;

        return $this;
    }

    public function fields()
    {
        return [];
    }

    public function getStandalone()
    {
        return $this->standalone;
    }

    public function key()
    {
        return static::uriKey();
    }

    public function pivotName()
    {
        return $this->pivot ? $this->pivotName : false;
    }

    public function prepareQueryWith($callback)
    {
        $this->prepareQueryCallback = $callback;

        return $this;
    }

    public function notReady($set = false)
    {
        $this->ready = (bool) $set;

        return $this;
    }

    public function soleModel($message = true)
    {
        if ($message === true) {
            $message = 'Action can only be executed on single model.';
        }

        $this->soleModelMessage = (string) $message;

        return $this;
    }

    public function standalone($value = true)
    {
        $this->standalone = $value;

        return $this;
    }

    public function then($callback)
    {
        $this->thenCallback = $callback;

        return $this;
    }

    public function toJson()
    {
        $request = request();

        $fields = $this->actionFields()->collection();

        return [
            'component' => $this->resolveComponent(),
            'name' => $this->name(),
            'uriKey' => $this->key(),
            'actionKey' => $this->actionKey(),

            'group' => $this->pivotGroup,
            'pivot' => (bool) $this->pivot,
            'fields' => $fields
                ->map->prepareDependants($request, $fields)
                ->map->toDisplayJson($request, (object) [])
                ->all(),

            'confirmText' => $this->confirmText,
            'confirmButtonText' => $this->confirmButtonText,
            'confirmButtonClass' => $this->actionClass(),
            'cancelButtonText' => $this->cancelButtonText,

            'indexVisibility' => (bool) $this->resolveIndexVisibility($request),
            'inlineVisibility' => (bool) $this->resolveInlineVisibility($request),
            'detailVisibility' => (bool) $this->resolveDetailVisibility($request),

            'withoutConfirmation' => (bool) $this->withoutConfirmation, // TODO
            'ready' => (bool) $this->ready,
            'standalone' => (bool) $this->standalone,
        ];
    }

    public function withoutActionEvents($value = true)
    {
        $this->withoutActionEvents = $value;

        return $this;
    }

    public function withoutChunk($value = true)
    {
        $this->withoutChunk = $value;

        return $this;
    }

    public function withoutConfirmation($value = true)
    {
        $this->withoutConfirmation = $value;

        return $this;
    }

    public function withTransaction($callback = true)
    {
        $this->transactionProviderCallback = $callback;

        return $this;
    }

    protected function actionFields()
    {
        if (isset($this->actionFields)) {
            return $this->actionFields;
        }

        return $this->actionFields = (new FieldsCollection($this->fields()));
    }

    protected function authorizesModel($request, $model)
    {
        return $this->authorizedToRun($request, $model) || Gate::check('update', $model);
    }

    protected function handlingFields($request)
    {
        $fields = $this->actionFields()->allfields()
            ->filter->authorizedToSee($request)
            ->filter(
                static function ($field) {
                    if ($field->computedResolver) {
                        return false;
                    }

                    return true;
                }
            );

        $fields = $fields->map->prepareDependants($request, $fields);

        $validated = Validator::make(
            $request->all(),
            $fields->mapWithKeys(static function ($field) use ($request) {
                return $field->getCreationValidationRules($request);
            })->all()
        )->validate();

        $object = (object) [];
        $fields->each->fillForAction($request, $object);

        return new Fluent((array) $object);
    }

    protected function markAsFailed($model, $e = null)
    {
        if ($this->batchId) {
            ActionModel::markAsFailed($this->batchId, $model, $e);
        }
    }

    protected function markAsFinished($model)
    {
        if ($this->batchId) {
            ActionModel::markAsFinished($this->batchId, $model);
        }
    }

    protected function prepareQuery($query)
    {
        if ($callback = $this->prepareQueryCallback) {
            return $callback($query);
        }

        return $query;
    }
}
