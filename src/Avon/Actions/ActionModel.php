<?php

namespace CR\Library\Avon\Actions;

use CR\Library\Avon\Auth\Models\Concerns;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Str;

class ActionModel extends \Illuminate\Database\Eloquent\Model
{
    // use Concerns\DelegatesDatabaseConnection;
    // use Concerns\InsertsWithCompositeKey;

    const STATUS_FAILED = 'failed';

    const STATUS_FINISHED = 'finished';

    const STATUS_RUNNING = 'running';

    protected $casts = [
        'batch_id' => 'integer',
        'user_id' => 'integer',
        'actionable_id' => 'integer',
        'model_id' => 'integer',
        'fields' => 'array',
        'original' => 'array',
        'changes' => 'array',
    ];

    protected $guarded = [];

    // public function getTable()
    // {
    //     return config('avon.action.table', 'avon_actions');
    // }

    public function model()
    {
        return $this->morphTo('related', 'related_type', 'related_id');
    }

    public static function forAction(Action $action, $batchId, $fields, Authenticatable $user, $models)
    {
        if (! static::enabled()) {
            return collect();
        }

        $fields = static::resolveFields($fields);

        return $models->map(static function ($model) use ($action, $batchId, $fields, $user) {
            // $action->pivotName()

            return static::makeNewModel([
                'batch_id' => $batchId,
                'user_id' => $user->getAuthIdentifier(),
                'name' => $action->name(),
                'actionable_type' => $model->getMorphClass(),
                'actionable_id' => $model->getKey(),
                'model_type' => $model->getMorphClass(),
                'model_id' => $model->getKey(),
                'fields' => $fields,
                'original' => $model->getOriginal(),
                'changes' => '',
                'status' => self::STATUS_RUNNING,
                'exception' => '',
            ]);
        });
    }

    public static function forActionHandled($eventModels, $handledModels, $exception)
    {
        if (static::enabled()) {
            $handledModels = $handledModels->values()->all();

            $exceptionStatus = $exception ? self::STATUS_FAILED : self::STATUS_FINISHED;
            $exceptionText = (string) $exception;

            foreach ($eventModels->values() as $key => $event) {
                $event->status = $exceptionStatus;
                $event->exception = $exceptionText;
                $event->changes = $handledModels[$key]->getOriginal();
            }
        }

        return $eventModels;
    }

    public static function forCreate(Authenticatable $user, $models)
    {
        if (! static::enabled()) {
            return collect();
        }

        $batchId = Str::orderedUuid()->toString();

        return $models->map(static function ($model) use ($batchId, $user) {
            return static::makeNewModel([
                'batch_id' => $batchId,
                'user_id' => $user->getAuthIdentifier(),
                'name' => 'Create',
                'actionable_type' => $model->getMorphClass(),
                'actionable_id' => $model->getKey(),
                'model_type' => $model->getMorphClass(),
                'model_id' => $model->getKey(),
                'fields' => '',
                'original' => '',
                'changes' => $model->attributesToArray(),
                'status' => self::STATUS_FINISHED,
                'exception' => '',
            ]);
        });
    }

    public static function forDelete(Authenticatable $user, $models)
    {
        if (! static::enabled()) {
            return collect();
        }

        $batchId = Str::orderedUuid()->toString();

        return $models->map(static function ($model) use ($batchId, $user) {
            return static::makeNewModel([
                'batch_id' => $batchId,
                'user_id' => $user->getAuthIdentifier(),
                'name' => 'Delete',
                'actionable_type' => $model->getMorphClass(),
                'actionable_id' => $model->getKey(),
                'model_type' => $model->getMorphClass(),
                'model_id' => $model->getKey(),
                'fields' => '',
                'original' => $model->attributesToArray(),
                'changes' => '',
                'status' => self::STATUS_FINISHED,
                'exception' => '',
            ]);
        });
    }

    public static function forStandaloneAction(Action $action, $batchId, $fields, Authenticatable $user)
    {
        if (! static::enabled()) {
            return optional(null);
        }

        return static::makeNewModel([
            'batch_id' => $batchId,
            'user_id' => $user->getAuthIdentifier(),
            'name' => $action->name(),
            'actionable_type' => '',
            'actionable_id' => 0,
            'model_type' => '',
            'model_id' => 0,
            'fields' => static::resolveFields($fields),
            'original' => '',
            'changes' => '',
            'status' => self::STATUS_RUNNING,
            'exception' => '',
        ]);
    }

    public static function forStandaloneActionHandled($eventModel, $exception)
    {
        if (static::enabled()) {
            $eventModel->status = $exception ? self::STATUS_FAILED : self::STATUS_FINISHED;
            $eventModel->exception = (string) $exception;
        }

        return $eventModel;
    }

    public static function forUpdate(Authenticatable $user, $models)
    {
        if (! static::enabled()) {
            return collect();
        }

        $batchId = Str::orderedUuid()->toString();

        return $models->map(static function ($model) use ($batchId, $user) {
            return static::makeNewModel([
                'batch_id' => $batchId,
                'user_id' => $user->getAuthIdentifier(),
                'name' => 'Update',
                'actionable_type' => $model->getMorphClass(),
                'actionable_id' => $model->getKey(),
                'model_type' => $model->getMorphClass(),
                'model_id' => $model->getKey(),
                'fields' => '',
                'original' => \array_intersect_key($model->getOriginal(), $model->getDirty()),
                'changes' => $model->getDirty(),
                'status' => self::STATUS_FINISHED,
                'exception' => '',
            ]);
        });
    }

    public static function markAsFailed($batchId, $model, $e = null)
    {
        if (! static::enabled()) {
            return 0;
        }

        return static::query()
            ->where('batch_id', $batchId)
            ->where('model_type', $model->getMorphClass())
            ->where('model_id', $model->getKey())
            ->update([
                'status' => self::STATUS_FAILED,
                'exception' => (string) $e,
            ]);
    }

    public static function markAsFinished($batchId, $model)
    {
        if (! static::enabled()) {
            return 0;
        }

        return static::query()
            ->where('batch_id', $batchId)
            ->where('model_type', $model->getMorphClass())
            ->where('model_id', $model->getKey())
            ->update([
                'status' => self::STATUS_FINISHED,
            ]);
    }

    protected static function enabled()
    {
        return true;
    }

    protected static function makeNewModel(array $properties)
    {
        return new static($properties);
    }

    protected static function resolveFields($fields)
    {
        return collect((array) $fields)->map(static function ($fieldValue) {
            // TODO: account for file field

            return $fieldValue;
        })->all();
    }
}
