<?php

namespace CR\Library\Avon\Actions;

use CR\Library\Helpers\Helpers;
use Exception;
use Illuminate\Support\Enumerable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Throwable;

trait ExecutesAction
{
    public function execute($request, $selectQuery, $options)
    {
        $fields = $this->handlingFields($request);

        $executeCallback = function () use ($request, $fields, $selectQuery, $options) {
            return $this->standalone === 'none'
                ? $this->executeStandalone($request, $fields, $this->prepareQuery($selectQuery))
                : $this->executeWithModels($request, $fields, $this->prepareQuery($selectQuery), $options);
        };

        $connection = $this->transactionProviderCallback === true
            ? $selectQuery->getConnection()
            : value($this->transactionProviderCallback);

        $this->batchId = Str::uuid()->toString();

        Log::info(\sprintf(
            'avon: action - executing - id: %s - standalone: %s - transaction: %s',
            $this->batchId,
            $this->standalone === false ? 'false' : $this->standalone,
            $connection ? 'true' : 'false'
        ));

        $start = (float) micro_time()->format('U.u');

        try {
            return $connection
                ? $connection->transaction($executeCallback)
                : $executeCallback();
        }
        finally {
            $finish = \round(((float) micro_time()->format('U.u') - $start) * 1000, 2);

            Log::info(\sprintf(
                'avon: action - executed - id: %s - time: %s',
                $this->batchId,
                $finish
            ));

            $this->batchId = null;
        }
    }

    public function handleResult($fields, $results)
    {
        return \count($results) ? \end($results) : null;
    }

    protected function actionModelsUnauthorizedResponse()
    {
        return static::danger('Anda tidak dapat menjalankan aksi ini');
    }

    protected function actionWithoutResultResponse()
    {
        return static::message('Tidak ada hasil untuk aksi ini');
    }

    protected function executeChunk($models, $request, $fields, $aeData, $options)
    {
        $modelsCount = $models->count();

        Log::info(\sprintf(
            'avon: action - chunk - id: %s - chunk count: %s - loop: %s',
            $this->batchId,
            $modelsCount,
            $aeData->loopCount
        ));

        ++$aeData->loopCount;
        $aeData->loadCount += $modelsCount;

        if (! $aeData->wasSelected && $modelsCount > 0) {
            $aeData->wasSelected = true;
        }

        $models = $models->filter(function ($model) use ($request) {
            return $this->authorizesModel($request, $model);
        });

        if (! $aeData->wasExecuted && $modelsCount > 0) {
            $aeData->wasExecuted = true;
        }

        Log::info(\sprintf(
            'avon: action - chunk - id: %s - filtered count: %s',
            $this->batchId,
            $models->count()
        ));

        if ($beforeCallback = (isset($options['beforeCallback']) ? $options['beforeCallback'] : null)) {
            $result = $models->map(function ($model) use ($beforeCallback) {
                return $beforeCallback($model, $this);
            })->filter();

            if (! $result->isEmpty()) {
                return $result;
            }
        }

        $actionEvents = null;

        if (! $this->withoutActionEvents) {
            $actionEvents = ActionModel::forAction(
                $this,
                $this->batchId,
                $fields,
                $request->user(),
                $models
            );

            $actionEvents->each->save();
        }

        $exception = null;

        try {
            if ($this->soleModelMessage !== '' && $aeData->loadCount !== 1) {
                if (! \count($aeData->handledResult)) {
                    Log::info(\sprintf(
                        'avon: action - chunk - id: %s - not one model',
                        $this->batchId
                    ));

                    $aeData->handledResult[] = $this->message($this->soleModelMessage);
                }
            }
            else {
                $aeData->handledResult[] = $this->handle(
                    $fields,
                    $this->pivot
                        ? $models->map->{$this->pivotName}
                        : $models
                );
            }
        }
        catch (Exception $exception) {
        }
        catch (Throwable $exception) {
        }

        if (isset($exception)) {
            Helpers::reportIgnoredException('Error on Action::handle call.', $exception);
        }

        if (! $this->withoutActionEvents) {
            ActionModel::forActionHandled($actionEvents, $models, $exception)
                ->each->save();
        }

        if ($afterCallback = (isset($options['afterCallback']) ? $options['afterCallback'] : null)) {
            $models->each(function ($model) use ($afterCallback) {
                $afterCallback($model, $this);
            });
        }

        $aeData->modelsProcessed[] = $models->all();
    }

    protected function executeStandalone($request, $fields, $selectQuery)
    {
        $actionEvent = null;

        if (! $this->withoutActionEvents) {
            $actionEvent = ActionModel::forStandaloneAction(
                $this,
                $this->batchId,
                $fields,
                $request->user()
            );

            $actionEvent->save();
        }

        $exception = null;
        $result = null;

        try {
            if ($this->soleModelMessage !== '') {
                Log::info(\sprintf(
                    'avon: action - chunk - id: %s - not one model',
                    $this->batchId
                ));

                $result = $this->message($this->soleModelMessage);
            }
            else {
                $result = $this->handle($fields, collect([]));
            }
        }
        catch (Exception $exception) {
        }
        catch (Throwable $exception) {
        }

        if (isset($exception)) {
            Helpers::reportIgnoredException('Error on Action::handle call.', $exception);
        }

        if (! $this->withoutActionEvents) {
            ActionModel::forStandaloneActionHandled($actionEvent, $exception)->save();
        }

        if ($thenCallback = $this->thenCallback) {
            Helpers::safeDispatchAfterResponse(static function () use ($thenCallback) {
                value($thenCallback, collect([]));
            });
        }

        return $this->handleResultWithProcessed(
            $fields,
            [$result],
            collect([])
        );
    }

    protected function executeWithModels($request, $fields, $selectQuery, $options)
    {
        $aeData = (object) [
            'loopCount' => 1,
            'handledResult' => [],
            'loadCount' => 0,
            'modelsProcessed' => [],
            'wasExecuted' => false,
            'wasSelected' => false,
        ];

        if ($selectQuery instanceof Enumerable) {
            $this->executeChunk($selectQuery, $request, $fields, $aeData, $options);
        }
        elseif ($this->withoutChunk) {
            $this->executeChunk($selectQuery->get(), $request, $fields, $aeData, $options);
        }
        else {
            $selectQuery->chunk($this->chunkCount < 2 ? 2 : $this->chunkCount, function ($models) use ($request, $fields, $aeData, $options) {
                $this->executeChunk($models, $request, $fields, $aeData, $options);

                if (\is_int($this->chunkLimit) && $aeData->loadCount > $this->chunkLimit) {
                    return false;
                }
            });
        }

        if (! $aeData->wasExecuted) {
            return $aeData->wasSelected
                ? $this->actionModelsUnauthorizedResponse()
                : $this->actionWithoutResultResponse();
        }

        $aeData->modelsProcessed = collect(\array_merge(...$aeData->modelsProcessed));

        if ($thenCallback = $this->thenCallback) {
            $modelsProcessed = $aeData->modelsProcessed;

            Helpers::safeDispatchAfterResponse(static function () use ($thenCallback, $modelsProcessed) {
                value($thenCallback, $modelsProcessed);
            });
        }

        return $this->handleResultWithProcessed($fields, $aeData->handledResult, $aeData->modelsProcessed);
    }

    protected function handleResultWithProcessed($fields, $results, $processed)
    {
        return $this->handleResult($fields, $results);
    }
}
