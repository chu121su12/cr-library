<?php

namespace CR\Library\Avon\Actions;

use CR\Library\Avon\Fields\Text;
use CR\Library\Helpers\Helpers;
use CR\Library\Helpers\Reply;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;

class ExportAsCsv extends Action
{
    protected $nameable = false;

    protected $nameCallback;

    protected $formatterCallback;

    public static function make()
    {
        return new static;
    }

    public function filename($callback)
    {
        $this->nameCallback = $callback;

        return $this;
    }

    public function nameable($name = true)
    {
        $this->nameable = $name;

        return $this;
    }

    public function fields()
    {
        if (! $this->nameable) {
            return [];
        }

        return [
            Text::make(
                \is_string($this->nameable) ? $this->nameable : 'Name',
                'name'
            ),
        ];
    }

    public function withFormat($callback)
    {
        $this->formatterCallback = $callback;

        return $this;
    }

    public function __construct()
    {
        $this->formatterCallback = static function ($model) {
            if ($model instanceof Model) {
                return $model->getAttributes();
            }

            if ($model instanceof Arrayable) {
                return $model->toArray();
            }

            if (\is_object($model)) {
                return (array) $model;
            }

            return Arr::wrap($model);
        };
    }

    public function handle($fields, $models)
    {
        return $models;
    }

    public function handleResult($fields, $results)
    {
        $name = '';

        if ((string) $fields->name !== '') {
            $name = $fields->name;
        }

        if ($name === '') {
            if (\is_callable($this->nameCallback)) {
                $name = value($this->nameCallback);
            }
            else {
                $name = (string) $this->nameCallback;
            }
        }

        return (new Reply)->downloadContent(
            $this->makeCsv(collect(Arr::flatten($results))),
            ! $name || $name === '' ? 'file.csv' : $name
        );
    }

    protected function authorizesModel($request, $model)
    {
        if ($this->authorizedToRun($request, $model)) {
            return true;
        }

        if (! \method_exists(Gate::getPolicyFor($model), 'viewAny')) {
            return true;
        }

        return Gate::check('viewAny', $model);
    }

    protected function makeCsv($models)
    {
        return Helpers::safeOb(function () use ($models) {
            tap(\fopen('php://output', 'wb'), function ($handle) use ($models) {
                $i = 0;
                foreach ($models as $model) {
                    $row = value($this->formatterCallback, $model, $i);

                    if ($row === false) {
                        break;
                    }

                    $row = Arr::wrap($row);

                    if ($i++ === 0) {
                        \fputcsv($handle, \array_keys($row));
                    }

                    \fputcsv($handle, \array_values($row));
                }

                \fclose($handle);
            });
        });
    }
}
