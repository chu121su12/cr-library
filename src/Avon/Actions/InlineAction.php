<?php

namespace CR\Library\Avon\Actions;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Throwable;

class InlineAction extends Action
{
    protected $errorHandler;

    protected $fields = [];

    protected $handler;

    protected $name;

    public function authorizedToRun(Request $request, $model)
    {
        return true;
    }

    public function authorizedToSee(Request $request)
    {
        return true;
    }

    public function __construct($name, $handler = null)
    {
        $this->name = $name;

        if ($handler) {
            $this->withHandler($handler);
        }
    }

    public function destructive()
    {
        $this->confirmButtonClass = 'text-white bg-red-500 hover:bg-red-600 dark:bg-red-600 dark:hover:bg-red-700';

        return $this;
    }

    public function key()
    {
        return Str::slug($this->name(), '_') . '_inline_action';
    }

    public function withFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    public function withHandler($callback)
    {
        $this->handler = $callback;

        return $this;
    }

    public function withErrorHandler($callback)
    {
        $this->errorHandler = $callback;

        return $this;
    }

    public function fields()
    {
        return Arr::wrap(value($this->fields));
    }

    public function handle($fields, $models)
    {
        try {
            return value($this->handler, $fields, $models, $this);
        }
        catch (Exception $e) {
        }
        catch (Throwable $e) {
        }

        if (isset($e)) {
            if (! $this->errorHandler) {
                throw $e;
            }

            return value($this->errorHandler, $e, $fields, $models, $this);
        }
    }
}
