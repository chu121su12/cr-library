<?php

namespace CR\Library\Avon\Actions;

class DeleteResource extends DestructiveAction
{
    protected function authorizesModel($request, $model)
    {
        return true;
    }

    public function handle($fields, $models)
    {
        $models->each->delete();
    }
}
