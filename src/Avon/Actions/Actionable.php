<?php

namespace CR\Library\Avon\Actions;

trait Actionable
{
    public function actions()
    {
        return $this->morphMany(ActionModel::class, 'actionable');
    }
}
