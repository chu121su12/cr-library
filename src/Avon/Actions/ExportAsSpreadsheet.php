<?php

namespace CR\Library\Avon\Actions;

use CR\Library\Avon\Fields\Select;
use CR\Library\Avon\Fields\Text;
use CR\Library\Helpers\Helpers;
use CR\Library\Helpers\Reply;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;

class ExportAsSpreadsheet extends Action
{
    protected $nameable = false;

    protected $formatterCallback;

    public static function make()
    {
        return new static;
    }

    public function nameable($name = true)
    {
        $this->nameable = $name;

        return $this;
    }

    public function fields()
    {
        return \array_filter([
            ! $this->nameable ? null : Text::make(
                \is_string($this->nameable) ? $this->nameable : 'Name',
                'name'
            )->default_('file'),

            Select::make('Format')
                ->useRadioOnForms()
                ->options([
                    'xlsx' => 'Excel (.xlsx)',
                    'xls' => 'Excel (.xls)',
                    'csv' => 'CSV',
                ]),
        ]);
    }

    public function withFormat($callback)
    {
        $this->formatterCallback = $callback;

        return $this;
    }

    public function __construct()
    {
        $this->formatterCallback = static function ($model) {
            if ($model instanceof Model) {
                return $model->getAttributes();
            }

            if ($model instanceof Arrayable) {
                return $model->toArray();
            }

            if (\is_object($model)) {
                return (array) $model;
            }

            return Arr::wrap($model);
        };
    }

    public function handle($fields, $models)
    {
        return $models;
    }

    public function handleResult($fields, $results)
    {
        return (new Reply)->downloadContent(
            $this->makeSpreadsheet($fields->format, collect(Arr::flatten($results))),
            Str::finish((string) $fields->name === '' ? 'file' : $fields->name, ".{$fields->format}")
        );
    }

    protected function authorizesModel($request, $model)
    {
        if ($this->authorizedToRun($request, $model)) {
            return true;
        }

        if (! \method_exists(Gate::getPolicyFor($model), 'viewAny')) {
            return true;
        }

        return Gate::check('viewAny', $model);
    }

    protected function makeSpreadsheet($format, $models)
    {
        switch ((string) $format) {
            case 'csv': return $this->makeCsv($models);

            case 'xls':
            case 'xlsx': return $this->makePhpSpreadsheet($format, $models);
        }
    }

    protected function makeCsv($models)
    {
        return Helpers::safeOb(function () use ($models) {
            tap(\fopen('php://output', 'wb'), function ($handle) use ($models) {
                $i = 0;
                foreach ($models as $model) {
                    $row = value($this->formatterCallback, $model, $i);

                    if ($row === false) {
                        break;
                    }

                    $row = Arr::wrap($row);

                    if ($i++ === 0) {
                        \fputcsv($handle, \array_keys($row));
                    }

                    \fputcsv($handle, \array_values($row));
                }

                \fclose($handle);
            });
        });
    }

    protected function makePhpSpreadsheet($format, $models)
    {
        $aoa = [];
        $i = 0;
        foreach ($models as $model) {
            $row = value($this->formatterCallback, $model, $i);

            if ($row === false) {
                break;
            }

            $row = Arr::wrap($row);

            if ($i++ === 0) {
                $aoa[] = \array_keys($row);
            }

            $aoa[] = \array_values($row);
        }

        $file = Helpers::excelAoa($aoa, $format);

        return \file_get_contents($file);
    }
}
