<?php

namespace CR\Library\Avon\Actions;

use CR\Library\Helpers\Helpers;
use CR\Library\Helpers\Reply;
use Exception;

trait ActionReply
{
    public static function danger($value)
    {
        return (new Reply)->danger($value);
    }

    public static function download($url, $fileName = null)
    {
        return (new Reply)->downloadUrl($url, $fileName);
    }

    public static function message($value)
    {
        return (new Reply)->message($value);
    }

    public static function modal($value, $data = null)
    {
        return (new Reply)->addAction([
            'type' => 'modal',
            'component' => $value,
            'data' => $data,
        ]);
    }

    public static function openInNewTab($value)
    {
        return (new Reply)->openInNewTab($value);
    }

    public static function push($route, $params = [])
    {
        Helpers::reportExceptionStack(new Exception('Action::push not implemented.'));

        return (new Reply)->sessionMessage('Aksi selesai')->withReplaceSelf();
    }

    public static function redirect($value)
    {
        return (new Reply)->redirect($value);
    }
}
