<?php

namespace CR\Library\Avon\Components;

use Carbon\CarbonImmutable;
use CR\Library\Helpers\Helpers;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class MonthlyFilter extends \CR\Library\Avon\Filters\FieldOptionsFilter
{
    protected $defaultThisMonthOnly;

    protected $fromDate;

    protected $toDate;

    public function __construct($name, $column = null)
    {
        $this->name = $name;
        $this->filterColumns = Arr::wrap($column ?: Str::slug($name, '_'));
        $this->applyResolver = function ($r, $query, $value) {
            if (! isset($value)) {
                if (! $this->defaultThisMonthOnly) {
                    return $query;
                }

                $value = micro_time()->format('Y-m');
            }

            try {
                $date = new CarbonImmutable($value . '-01');
            }
            catch (Exception $e) {
                Helpers::reportIgnoredException('Error parsing date.', $e);

                return $query->whereRaw('0 = 1');
            }

            $date = $date->startOfMonth();

            return $query->where(function ($q) use ($date) {
                foreach ($this->filterColumns as $column) {
                    $q = $q->orWhere(static function ($q) use ($date, $column) {
                        return $q
                            ->where($column, '>=', $date->toDateString())
                            ->where($column, '<', $date->addMonth()->startOfMonth()->toDateString());
                    });
                }

                return $q;
            });
        };

        $this->from('2000-01-01');
        $this->to(micro_time());
    }

    public function defaultThisMonthOnly($set = true)
    {
        $this->defaultThisMonthOnly = $set;

        return $this;
    }

    public function from($from)
    {
        $this->fromDate = new CarbonImmutable($from);

        $this->updateOptions();

        return $this;
    }

    public function to($to)
    {
        $this->toDate = new CarbonImmutable($to);

        $this->updateOptions();

        return $this;
    }

    protected function updateOptions()
    {
        $dates = [];
        $date = $this->fromDate->startOfMonth();

        while ($date->isBefore($this->toDate)) {
            $dates[$date->format('F Y')] = $date->format('Y-m');
            $date = $date->addMonth();
        }

        $this->withOptions(\array_reverse($dates));
    }
}
