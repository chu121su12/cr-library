<?php

namespace CR\Library\Avon\Components;

use CR\Library\Avon\Helpers;
use CR\Library\Helpers\DbModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use RuntimeException;

class SimpleResource extends \CR\Library\Avon\Resource
{
    protected static $connection;

    protected static $table;

    protected static $casts;

    protected static function getConnection()
    {
        return isset(static::$connection) ? static::$connection : DB::getDefaultConnection();
    }

    protected static function getTable()
    {
        return isset(static::$table) ? static::$table : Helpers::classToHtmlId(static::class);
    }

    protected static function dbt($table = null)
    {
        if ($table === null || $table === '') {
            return static::resourceQuery();
        }

        if (\is_string($table) && \str_contains($table, '.')) {
            list($connection, $tableName) = \explode('.', $table, 2);
        }
        else {
            list($connection, $tableName) = [static::getConnection(), $table];
        }

        return DB::connection($connection)->table($tableName);
    }

    public static function fromModel($model)
    {
        $instance = new static;

        $instance->resource = $model instanceof Model
            ? $model
            : static::newResourceFromModel($model);

        return $instance;
    }

    public static function newResourceFromModel($model)
    {
        $casts = collect(isset(static::$casts) ? static::$casts : ['id' => 'id']);

        $firstIdAttribute = $casts->filter(static function ($cast) {
            return $cast === 'id';
        })->keys()->first();

        if (! isset($firstIdAttribute)) {
            throw new RuntimeException('Missing Primary key');
        }

        return DbModel::newStandalone(static::getTable(), $model, [
            'exists' => data_get($model, $firstIdAttribute) !== null,
            'connection' => static::getConnection(),
            'key' => $firstIdAttribute,
            'casts' => $casts->filter(static function ($cast, $attribute) {
                return \in_array($cast, DbModel::getPrimitiveCastTypes(), true)
                    && ! \is_int($attribute);
            })->all(),
        ]);
    }

    public static function resourceModel()
    {
        return null;
    }

    public static function resourceQuery($options = null)
    {
        return DB::connection(static::getConnection())->table(static::getTable());
    }

    public function fields($r)
    {
        return Helpers::transformCastsToFields(static::$casts);
    }
}
