<?php

namespace CR\Library\Avon\Components;

use Illuminate\Support\Str;

class EmptiableBooleanFilter extends \CR\Library\Avon\Filters\BooleanFilter
{
    protected $name;

    protected $databaseTrimFunction = 'trim';

    protected $exclusiveLabel;

    protected $filterColumn;

    protected $inclusiveLabel;

    protected function __construct($name, $column = null)
    {
        $this->name = $name;

        $this->filterColumn = $column ?: Str::slug($name, '_');

        $this->inclusiveLabel = "With {$name}";

        $this->exclusiveLabel = "Without {$name}";
    }

    public static function forField($name, $column = null)
    {
        return tap(new static($name, $column), static function ($instance) {
            $instance->applyResolver = static function ($r, $query, $value) use ($instance) {
                if (! isset($value)) {
                    return $query;
                }

                $column = \sprintf('%s(%s)', $instance->databaseTrimFunction, $instance->filterColumn);

                if ($value['with']) {
                    $query = $query->whereRaw("({$column} is not null and {$column} != ?)", ['']);
                }

                if ($value['without']) {
                    $query = $query->whereRaw("({$column} is null or {$column} = ?)", ['']);
                }

                return $query;
            };
        });
    }

    public static function forRelation($name, $column = null)
    {
        return tap(new static($name, $column), static function ($instance) {
            $instance->applyResolver = static function ($r, $query, $value) use ($instance) {
                if (! isset($value)) {
                    return $query;
                }

                if ($value['with']) {
                    $query = $query->has($instance->filterColumn);
                }

                if ($value['without']) {
                    $query = $query->doesntHave($instance->filterColumn);
                }

                return $query;
            };
        });
    }

    public function exclusiveLabel($options)
    {
        $this->exclusiveLabel = $options;

        return $this;
    }

    public function inclusiveLabel($options)
    {
        $this->inclusiveLabel = $options;

        return $this;
    }

    public function withTrimFunction($fnName)
    {
        $this->databaseTrimFunction = $fnName;

        return $this;
    }

    public function options($request)
    {
        return [
            $this->inclusiveLabel => 'with',
            $this->exclusiveLabel => 'without',
        ];
    }
}
