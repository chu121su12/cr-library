<?php

namespace CR\Library\Avon\Components;

use CR\Library\Avon\Fields\FieldsCollection;
use CR\Library\Helpers\Reply;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Fluent;

abstract class FormTool extends \CR\Library\Avon\Tool
{
    use \CR\Library\Avon\Actions\ActionReply;
    use \CR\Library\Avon\Concerns\WithMeta;

    abstract public function handle($fields);

    public function preventSubmit($prevent = true)
    {
        $this->withMeta([
            'preventSubmit' => $prevent,
        ]);

        return $this;
    }

    public function fields()
    {
        return [];
    }

    protected function resolveFields()
    {
        return new FieldsCollection($this->fields());
    }

    protected function actionFields($request)
    {
        $baseFields = $this->resolveFields();

        $fields = $baseFields->editFields($request)
            ->filter->authorizedToSee($request);

        return [
            'label' => static::label(),

            'fields' => $fields
                ->map->prepareDependants($request, $fields)
                ->map->toDisplayJson($request, (object) [])
                ->all(),

            'panels' => $baseFields
                ->defaultPanelLabel($this->name())
                ->allPanels()
                ->map->toJson()
                ->all(),

            'actionMeta' => $this->resolveMeta($request),
        ];
    }

    protected function actionSave($request)
    {
        $fields = $this->resolveFields()
            ->updateFields($request)
            ->filter->authorizedToSee($request)
            ->filter(
                static function ($field) {
                    if ($field->computedResolver) {
                        return false;
                    }

                    return true;
                }
            );

        $fields = $fields->map->prepareDependants($request, $fields);

        $validated = Validator::make(
            $request->all(),
            $fields->mapWithKeys(static function ($field) use ($request) {
                return $field->getCreationValidationRules($request);
            })->all()
        )->validate();

        $object = (object) [];
        $fields->each->fillForAction($request, $object);

        $result = $this->handle(new Fluent((array) $object));

        if ($result) {
            return $result;
        }

        return (new Reply)->message('Tidak ada hasil untuk aksi ini');
    }

    public static function renderView()
    {
        return <<<'JAVASCRIPT'
((callback, ...args) => {
    callback(...args);
})(function (
    vueInstance,
    context,
    options,
) {
    const {
        createFormToolComponent,
        componentName,
    } = context;

    vueInstance.component(componentName, {
        name: `Form Tool: ${componentName}`,
        ...createFormToolComponent(context),
    });
}, ...arguments);
JAVASCRIPT;
    }
}
