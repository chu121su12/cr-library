<?php

namespace CR\Library\Avon;

class ResourceTool extends Panel
{
    use Concerns\MethodMetable;
    use Concerns\RendersAssets;

    protected $component;

    public function __construct($name = null)
    {
        parent::__construct(
            $name ?: Helpers::classToProperName(static::class),
            []
        );
    }

    public function toJson()
    {
        return \array_merge(
            parent::toJson(),
            [
                'isResourceTool' => true,
                'builtInComponent' => static::class === self::class,
            ],
            $this->resolveMeta(request())
        );
    }

    protected function resolveComponent()
    {
        if (static::class === self::class) {
            return 'avon-resources-panels-default-resource-tool';
        }

        return parent::resolveComponent();
    }
}
