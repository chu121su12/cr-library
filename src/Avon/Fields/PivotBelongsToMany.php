<?php

namespace CR\Library\Avon\Fields;

class PivotBelongsToMany extends RelationshipField
{
    // use \CR\Library\Avon\Concerns\ResourcePreview;
    use SearchableRelation;

    public $allowDuplicates = false;

    public $component = 'belongs-to-field';

    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        if ($request->has($attribute)) {
            $model->{$attribute}()->attach($value);
        }
    }

    protected function prepareResolvedValue($values, $model)
    {
        $request = request();

        $resource = $this->resourceClass;

        if ($values === null) {
            $this->withMeta([
                'relation' => [
                    'type' => 'belongsTo',
                    'indexRelation' => false,
                    'resource' => $resource::uriKey(),
                    'label' => $resource::label(),

                    'data' => [
                    ],

                    'locked' => false,
                    'options' => [],
                ],
            ]);

            return;
        }

        $value = $this->relatedValue($values, $request);

        $data = static::resourcePreviewJson($value, $request);

        $this->withMeta([
            'relation' => [
                'type' => 'belongsTo',
                'indexRelation' => false,
                'resource' => $resource::uriKey(),
                'label' => $resource::label(),

                'data' => \array_merge($data, [
                    'authorizedToView' => $value->authorizedToView($request),
                ]),

                'locked' => (bool) $request->relationId, // TODO: when locked, do not request for new data
                'options' => [$data],
            ],
        ]);

        $this->resourceValue = $value;
    }

    protected function resolveDisplayValue($value, $request)
    {
        return $this->resolveRelationshipDisplayValue($value);
    }

    protected function _resolveValueViaAttribute($model)
    {
        return $this->resourceInstance = $this->generateNewResourceModel($model);
    }

    private function relatedValue($resource, $request)
    {
        $model = $resource->model()->first(static function ($item) use ($request) {
            return (string) $item->getKey() === (string) $request->relationId;
        });

        return (new $resource)->useWithModel($model);
    }
}
