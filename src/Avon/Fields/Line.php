<?php

namespace CR\Library\Avon\Fields;

class Line extends Field
{
    public $component = 'line-field';

    public function asBase($set = true)
    {
        return $this->withMeta(['asBase' => (bool) $set]);
    }

    public function asHeading($set = true)
    {
        return $this->withMeta(['asHeading' => (bool) $set]);
    }

    public function asSmall($set = true)
    {
        return $this->withMeta(['asSmall' => (bool) $set]);
    }

    public function asSubTitle($set = true)
    {
        return $this->withMeta(['asSubTitle' => (bool) $set]);
    }

    public function extraClasses($classes)
    {
        return $this->withMeta(['extraClasses' => $classes]);
    }

    public function withoutLabel($set = true)
    {
        return $this->withMeta(['withoutLabel' => (bool) $set]);
    }

    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        // none
    }
}
