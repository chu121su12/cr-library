<?php

namespace CR\Library\Avon\Fields;

use CR\Library\Avon\Helpers;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ID extends Field
{
    public $component = 'line-field';

    protected $creationVisibility = false;

    protected $sortableResolver = true;

    public function asBigInt()
    {
        return $this->resolveUsing(static function ($value) {
            return (string) $value;
        });
    }

    public function asInt()
    {
        return $this->resolveUsing(static function ($value) {
            return $value;
        });
    }

    public function invisible()
    {
        $this->indexVisibility = false;
        $this->detailVisibility = false;
        $this->creationVisibility = false;
        $this->updatingVisibility = false;

        return $this;
    }

    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        // none
    }

    public static function make(...$arguments)
    {
        $maybeParentResource = Arr::get(\debug_backtrace(\DEBUG_BACKTRACE_IGNORE_ARGS, 2), '1.class');
        $guessedModel = Helpers::guessResourceModel($maybeParentResource);
        $guessedIdAttribute = $guessedModel ? $guessedModel->getKeyName() : null;

        $instance = new static(
            isset($arguments[0]) ? $arguments[0] : ($guessedIdAttribute ? \str_replace('_', ' ', \ucfirst($guessedIdAttribute)) : 'ID'),
            isset($arguments[1]) ? $arguments[1] : (isset($arguments[0]) ? Str::slug($arguments[0], '_') : ($guessedIdAttribute ?: 'id')),
            isset($arguments[2]) ? $arguments[2] : null
        );

        $meta = [];

        if ($guessedModel && $guessedModel->getKeyType() === 'int') {
            $instance->textAlign('right');

            $meta = [
                'type' => 'number',
                'step' => 1,
            ];
        }

        $meta['idField'] = true;

        return $instance->withMeta($meta)->asBigInt();
    }
}
