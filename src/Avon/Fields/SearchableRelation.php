<?php

namespace CR\Library\Avon\Fields;

use Closure;

trait SearchableRelation
{
    public function searchable($searchable = true)
    {
        return $this->withMeta([
            'searchable' => (bool) (
                $searchable instanceof Closure
                    ? $searchable(request())
                    : $searchable
            ),
        ]);
    }

    public function withSubtitles($withSubtitles = true)
    {
        return $this->withMeta([
            'withSubtitles' => (bool) (
                $withSubtitles instanceof Closure
                    ? $withSubtitles(request())
                    : $withSubtitles
            ),
        ]);
    }
}
