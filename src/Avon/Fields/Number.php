<?php

namespace CR\Library\Avon\Fields;

class Number extends Field
{
    public $component = 'text-field';

    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);

        $this->textAlign('right')
            ->withMeta([
                'type' => 'number',
                'step' => 1,
            ]);
    }

    public function max($max)
    {
        return $this->withMeta(['max' => $max]);
    }

    public function min($min)
    {
        return $this->withMeta(['min' => $min]);
    }

    public function step($step)
    {
        return $this->withMeta(['step' => $step]);
    }
}
