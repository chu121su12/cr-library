<?php

namespace CR\Library\Avon\Fields;

use Closure;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class RelationshipField extends Field
{
    protected $indexVisibility = false;

    protected $resourceClass;

    protected $resourceInstance;

    protected $queryResolver;

    public function getResourceClass()
    {
        return $this->resourceClass;
    }

    public function resolveQueryUsing($callback)
    {
        $this->queryResolver = $callback;

        return $this;
    }

    // $options see Resource::resourceQuery
    public function resolveResourceQuery($request, $parentResourceInstance, $idField, $options)
    {
        return value($this->queryResolver ?: static function ($request, $resource, $idField, $field) {
            return $resource->resource()->{$field->getAttribute()}();
        }, $request, $parentResourceInstance, $idField, $this);
    }

    public function __construct($name = null, $attribute = null, $resourceClass = null)
    {
        if ($name === null) {
            throw new Exception(\sprintf('Name not defined. (%s)', static::class));
        }

        if ($attribute !== null && $resourceClass === null && \class_exists($attribute)) {
            $resourceClass = $attribute;
            $attribute = null;
        }

        if ($resourceClass === null) {
            $resourceClass = self::guessRelationshipResource($name);
        }

        $this->name = $name;
        $this->resourceClass = $resourceClass;

        if (! \is_string($attribute) && $attribute instanceof Closure) {
            $this->computedResolver = $attribute;
        }
        elseif (isset($attribute)) {
            $this->attribute = $attribute;
        }
        else {
            $this->attribute = Str::camel(Str::lower($name));
        }
    }

    private static function guessRelationshipResource($name)
    {
        $trace = \debug_backtrace(\DEBUG_BACKTRACE_IGNORE_ARGS, 4)[3];

        $guessedClass = \sprintf(
            '%s\\%s',
            Str::beforeLast(Arr::get($trace, 'class'), '\\'),
            \preg_replace('/[^a-zA-Z0-9]/', '', $name)
        );

        if (\class_exists($guessedClass)) {
            return $guessedClass;
        }

        throw new Exception(\sprintf('Resource %s (guessed from %s) not defined (@%s).', $guessedClass, $name, static::class));
    }
}
