<?php

namespace CR\Library\Avon\Fields;

use Closure;
use CR\Library\Avon\Contracts\Cover;
use CR\Library\Avon\Helpers;
use Illuminate\Support\Facades\Storage;

class File extends Field
{
    public $component = 'file-field';

    public $deletable = true;

    public $deleteResolver; // v

    public $disk = 'public';

    public $downloadable = true;

    public $downloadResolver; // v

    // public $indexVisibility = false;

    public $originalNameColumn;

    public $path = '/';

    public $previewResolver; // ?

    public $prunable = false;

    public $sizeColumn;

    public $storeResolver; // v

    public $storeValueResolver; // ?

    public $thumbnailResolver; // ?

    protected $thumbnailBorderStyle = 'rounded-full';

    private $fillingForAction = false;

    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        parent::__construct(...\func_get_args());

        $this->disk($this->disk);

        $this->store(function ($request) {
            $file = $request->file($this->attribute);

            if (! $file) {
                return [];
            }

            $attributes = [
                $this->attribute => ($storeValueResolver = $this->storeValueResolver) instanceof Closure
                    ? $file->storeAs(
                        $this->path,
                        $storeValueResolver($request),
                        $this->disk
                    )

                    : $file->store(
                        $this->path,
                        $this->disk
                    ),
            ];

            if ($this->originalNameColumn) {
                $attributes[$this->originalNameColumn] = $file->getClientOriginalName();
            }

            if ($this->sizeColumn) {
                $attributes[$this->sizeColumn] = $file->getSize();
            }

            return $attributes;
        });

        $this->download(function ($request, $model) {
            $originalName = $this->originalNameColumn
                ? $model->{$this->originalNameColumn}
                : null;

            return Storage::disk($this->disk)
                ->download($this->value, $originalName);
        });

        $this->delete(function ($request, $model) {
            if ($this->value) {
                Storage::disk($this->disk)->delete($this->value);
            }

            $attributes = [$this->attribute => null];

            if ($this->originalNameColumn) {
                $attributes[$this->originalNameColumn] = null;
            }

            if ($this->sizeColumn) {
                $attributes[$this->sizeColumn] = null;
            }

            return $attributes;
        });

        $this->preview(static function ($value, $disk) {
            return null;
        });

        $this->thumbnail(static function ($value, $disk) {
            return null;
        });
    }

    public function deletable($value = true)
    {
        $this->deletable = $value;

        return $this;
    }

    public function delete($callback)
    {
        $this->deleteResolver = $callback;

        return $this;
    }

    public function deleteFile($request, $model)
    {
        if (\is_string($this->attribute)) {
            static::fillFromResolver($this->attribute, $this->deleteFileForDatabase($request, $model), $model);
        }
    }

    public function disableDownload()
    {
        $this->downloadable = false;

        return $this;
    }

    public function disk($value)
    {
        $this->disk = $value;

        return $this;
    }

    public function download($callback)
    {
        $this->downloadResolver = $callback;

        return $this;
    }

    public function downloadFile($request, $model)
    {
        $downloadResolver = $this->downloadResolver;

        return $downloadResolver($request, $model);
    }

    public function maxWidth($width)
    {
        // TODO: implement functionality

        return $this->withMeta(['maxWidth' => $width]);
    }

    public function path($value)
    {
        $this->path = $value;

        return $this;
    }

    public function preview($callback)
    {
        $this->previewResolver = $callback;

        return $this;
    }

    public function prunable($value = true)
    {
        $this->prunable = $value;

        return $this;
    }

    public function resolvePreviewUrl()
    {
        return value($this->previewResolver, $this->value, $this->disk);
    }

    public function resolveThumbnailUrl()
    {
        return value($this->thumbnailResolver, $this->value, $this->disk);
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'uploadLimit' => Helpers::serverFileUploadLimit(),
                'showPreview' => (bool) ($this instanceof Image),
                'showThumbnail' => (bool) (($this instanceof Cover) || ($this instanceof Avatar)),
                'thumbnailBorderStyle' => $this->thumbnailBorderStyle,
                'previewUrl' => $this->resolvePreviewUrl(),
                'thumbnailUrl' => $this->resolveThumbnailUrl(),
                'downloadable' => $this->downloadable,
                'deletable' => $this->deletable,
            ],
            parent::resourceToJson($request, $resource)
        );
    }

    public function rounded()
    {
        $this->thumbnailBorderStyle = 'rounded-full';

        return $this;
    }

    public function squared()
    {
        $this->thumbnailBorderStyle = 'rounded-sm';

        return $this;
    }

    public function store($callback)
    {
        $this->storeResolver = $callback;

        return $this;
    }

    public function storeAs($callback)
    {
        $this->storeValueResolver = $callback;

        return $this;
    }

    public function storeOriginalName($value = null)
    {
        if (null === $value || $value === true) {
            $this->originalNameColumn = "{$this->attribute}_original_name";
        }
        elseif ($value === false) {
            $this->originalNameColumn = null;
        }
        else {
            $this->originalNameColumn = $value;
        }

        return $this;
    }

    public function storeSize($value = null)
    {
        if (null === $value || $value === true) {
            $this->sizeColumn = "{$this->attribute}_size";
        }
        elseif ($value === false) {
            $this->sizeColumn = null;
        }
        else {
            $this->sizeColumn = $value;
        }

        return $this;
    }

    public function thumbnail($callback)
    {
        $this->thumbnailResolver = $callback;

        return $this;
    }

    protected function deleteFileForDatabase($request, $model)
    {
        return value($this->deleteResolver, $request, $model);
    }

    protected function getFillValue($request)
    {
        return $this->resolveFieldValue(
            $request->file($this->attribute)
        );
    }

    protected function storeFileForDatabase($request, $model)
    {
        return value($this->storeResolver, $request, $model);
    }

    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        if (! $request->has($attribute)) {
            return;
        }

        if ($this->fillingForAction) {
            return static::fillFromResolver($attribute, $value, $model);
        }

        if ($this->prunable) {
            $this->deleteFile($request, $model);
        }

        static::fillFromResolver($attribute, $this->storeFileForDatabase($request, $model), $model);
    }

    public function fillForAction($request, $model)
    {
        $this->fillingForAction = true;

        $result = parent::fillForAction($request, $model);

        $this->fillingForAction = false;

        return $result;
    }

    protected static function fillFromResolver($attribute, $fill, $model)
    {
        foreach (\is_array($fill) ? $fill : [$attribute => $fill] as $key => $value) {
            $model->{$key} = $value;
        }
    }
}
