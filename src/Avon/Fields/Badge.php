<?php

namespace CR\Library\Avon\Fields;

use Closure;

class Badge extends Field
{
    public $component = 'badge-field';

    protected $creationVisibility = false;

    protected $labelCallback;

    protected $labels = [];

    protected $map = [];

    protected $types = [
        'danger' => 'bg-red-400 border-red-600 dark:bg-red-600 dark:border-red-400',
        'info' => 'bg-blue-400 border-blue-600 dark:bg-blue-600 dark:border-blue-400',
        'success' => 'bg-green-400 border-green-600 dark:bg-green-600 dark:border-green-400',
        'warning' => 'bg-yellow-400 border-yellow-600 dark:bg-yellow-600 dark:border-yellow-400',
    ];

    protected $updatingVisibility = false;

    public function addTypes(array $types)
    {
        $this->types = \array_merge($this->types, $types);

        return $this;
    }

    public function label($callback)
    {
        $this->labelCallback = $callback;

        return $this;
    }

    public function labels(array $map)
    {
        $this->labels = $map;

        return $this;
    }

    public function map(array $map)
    {
        $this->map = $map;

        return $this;
    }

    public function types(array $types)
    {
        $this->types = $types;

        return $this;
    }

    protected function resourceToJson($request, $resource)
    {
        $value = $this->value;

        $label = $this->labelCallback instanceof Closure
            ? value($this->labelCallback, $value)
            : (isset($this->labels[$value]) ? $this->labels[$value] : $value);

        $typeClass = with(isset($this->map[$value]) ? $this->map[$value] : $value, function ($map) {
            return isset($this->types[$map]) ? $this->types[$map] : '';
        });

        return \array_merge(
            [
                'label' => $label,
                'typeClass' => $typeClass,
            ],
            parent::resourceToJson($request, $resource)
        );
    }
}
