<?php

namespace CR\Library\Avon\Fields;

use DateTimeZone;

class Timezone extends Select
{
    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        $tz = DateTimeZone::listIdentifiers();

        parent::__construct($name, $attribute, $resolveCallback);

        $this->options(\array_combine($tz, $tz));
    }
}
