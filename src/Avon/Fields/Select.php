<?php

namespace CR\Library\Avon\Fields;

use CR\Library\Avon\Concerns\OptionableField;
use Illuminate\Validation\Rule;

class Select extends Field
{
    use OptionableField;

    public $component = 'select-field';

    protected $displayUsingLabels = false;

    protected $useRadioOnForms = false;

    protected $searchable = false;

    protected $validateWithOptions = false;

    protected $stringValue = true;

    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);

        $this->option(function ($value, $key) {
            return \array_merge(
                [
                    'key' => $this->stringValue ? (string) $key : $key,
                ],
                \is_array($value) ? $value : [
                    'label' => (string) $value,
                    'group' => null,
                ]
            );
        });
    }

    public function displayUsingLabels($value = true)
    {
        $this->displayUsingLabels = $value;

        return $this;
    }

    public function displayAsBadge($set = true)
    {
        $this->withMeta([
            'displayAsBadge' => $set,
        ]);

        return $this;
    }

    public function useRadioOnForms($value = true)
    {
        $this->useRadioOnForms = $value;

        return $this;
    }

    public function resolveFilterableFilters()
    {
        $options = $this->getOptions()
            ->mapWithKeys(static function ($option) {
                return [\sprintf('%s (%s)', $option['label'], $option['key']) => $option['key']];
            })
            ->all();

        return FieldsCollection::filterableOption($this->name(), $this->attribute, $this->filterResolver, \array_merge(['—' => null], $options));
    }

    public function searchable($value = true)
    {
        $this->searchable = $value;

        return $this;
    }

    public function validateWithOptions($validate = true)
    {
        $this->validateWithOptions = $validate;

        return $this;
    }

    protected function normalizeValidationRules($batches)
    {
        $rules = parent::normalizeValidationRules($batches);

        if ($this->validateWithOptions) {
            $rules['rules'][] = Rule::in($this->getOptions()->pluck('key')->all());
        }

        return $rules;
    }

    protected function prepareValue($request, $resource, $value)
    {
        if (isset($value)) {
            return $this->stringValue ? (string) $value : $value;
        }
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'searchable' => (bool) $this->searchable,
                'displayUsingLabels' => (bool) $this->displayUsingLabels,
                'useRadioOnForms' => (bool) $this->useRadioOnForms,
                'options' => $this->getOptions()->all(),
            ],
            parent::resourceToJson($request, $resource)
        );
    }
}
