<?php

namespace CR\Library\Avon\Fields;

use CR\Library\Avon\Helpers;
use Exception;

class BelongsTo extends RelationshipField
{
    use SearchableRelation;

    public $component = 'belongs-to-field';

    protected $updatingVisibility = true;

    protected $withoutTrashed = false;

    public function fillDirectlyUsingAttribute($overrideAttribute = null)
    {
        return $this->fillUsing(static function ($request, $model, $attribute, $_attribute, $value) use ($overrideAttribute) {
            $attribute = isset($overrideAttribute) ? $overrideAttribute : $attribute;

            if ($request->has($attribute)) {
                $model->{$attribute} = $value;
            }
        });
    }

    public function withoutTrashed($value = true)
    {
        $this->withoutTrashed = $value;

        return $this;
    }

    public static function dynamicMake($label, $attribute, $relatedAttribute = null, $relatedClass = null)
    {
        $instance = new static($label, $attribute, $relatedClass);

        $relatedClass = $relatedClass ?: $instance->resourceClass;

        return $instance->resolveUsing(static function ($_, $resource) use (
            $label,
            $attribute,
            $relatedAttribute,
            $relatedClass
        ) {
            if (! $relatedAttribute) {
                $request = request();
                $idField = (new FieldsCollection($resource->fields($request)))->idField($request);
                if ($idField) {
                    $relatedAttribute = $idField->getAttribute();
                }
            }

            if (! $relatedAttribute) {
                throw new Exception(\sprintf('Cannot guess related attribute. (%s, %s, %s)', $label, $attribute, static::class));
            }

            return $relatedClass::resourceQuery()
                ->where($relatedAttribute, $resource->{$attribute})
                ->first();
        });
    }

    protected function resolve($resource, $attribute = null)
    {
        parent::resolve($resource, $attribute);

        $resourceClass = $this->resourceClass;

        $this->resourceInstance = $resourceClass::fromModel($this->value);
    }

    public function toDisplayJson($request, $resource)
    {
        $result = parent::toDisplayJson($request, $resource);

        $result['value'] = Helpers::generateResourcePreview($request, $this->resourceInstance);

        return $result;
    }

    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        if (! $request->has($attribute)) {
            return;
        }

        // if (! ($model instanceof \Illuminate\Database\Eloquent\Model) &&
        //     ! method_exists($model, $attribute)) {
        //     $model->$attribute = $value;

        //     return;
        // }

        $relationQuery = $model->{$attribute}();
        $foreignKeyName = $relationQuery->getForeignKeyName();

        // null no fill
        if ($value === null && $model->{$foreignKeyName} === null) {
            return;
        }

        $valueModel = $relationQuery->getRelated()->find($value);
        $ownerKeyName = $relationQuery->getOwnerKeyName();
        $relatedValue = $valueModel->{$ownerKeyName};

        if (\preg_match('/^-?\d*\.?\d+$/', $relatedValue)) {
            $relatedValue = (string) $relatedValue;
        }

        $model->{$foreignKeyName} = $relatedValue;
    }
}
