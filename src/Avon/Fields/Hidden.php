<?php

namespace CR\Library\Avon\Fields;

use CR\Library\Avon\Helpers;

class Hidden extends Field
{
    public $component = 'hidden-field';

    protected $indexVisibility = false;

    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        parent::__construct(
            $name ?: Helpers::classToProperName(static::class),
            $attribute,
            $resolveCallback
        );
    }

    public static function createFill($callback)
    {
        return tap(new static, static function ($instance) use ($callback) {
            $instance->onlyOnForms()->fillUsing($callback);
            $instance->creationVisibility = true;
            $instance->updatingVisibility = false;
        });
    }

    public static function updateFill($callback)
    {
        return tap(new static, static function ($instance) use ($callback) {
            $instance->onlyOnForms()->fillUsing($callback);
            $instance->creationVisibility = false;
            $instance->updatingVisibility = true;
        });
    }
}
