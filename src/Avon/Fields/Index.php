<?php

namespace CR\Library\Avon\Fields;

use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Support\Collection;

class Index extends Field
{
    public $component = 'index-field';

    protected $captionCallback;

    protected $fields;

    protected $map;

    protected $perPageOptions = [3, 10];

    protected $searchable = false;

    protected $summarizeCallback;

    public function __construct($name, $attribute = null, $fields = null)
    {
        if ($fields === null && \is_array($attribute)) {
            parent::__construct($name);
            $fields = $attribute;
        }
        else {
            parent::__construct($name, $attribute);
        }

        $this->fields = value($fields);
    }

    public function caption($caption)
    {
        $this->captionCallback = $caption;

        return $this;
    }

    public function mapValues($callback)
    {
        $this->map = $callback;

        return $this;
    }

    public function summary($summary)
    {
        $this->summarizeCallback = $summary;

        return $this;
    }

    public function toDisplayJson($request, $resource)
    {
        return \array_merge(
            [
                'caption' => (string) value($this->captionCallback, $resource, $request),
                'summary' => (string) value($this->summarizeCallback, $resource, $request),
            ],
            parent::toDisplayJson($request, $resource)
        );
    }

    protected function prepareValue($request, $resource, $value)
    {
        if (! isset($value)) {
            return;
        }

        if ($value instanceof Builder) {
            $value = $value->get();
        }

        $fieldsCollection = new FieldsCollection(value($this->fields));
        $idField = $fieldsCollection->idField($request);
        $indexFields = $fieldsCollection->indexFields($request);

        return [
            'searchable' => (bool) $this->searchable,
            'perPageOptions' => $this->perPageOptions,
            'resources' => Collection::wrap($value)
                ->map($this->map ?: static function ($resource) { return $resource; })
                ->map(static function ($resource) use ($request, $idField, $indexFields) {
                    if ($idField && $indexFields->search($idField, true) === false) {
                        $idField->toDisplayJson($request, $resource);
                    }

                    return [
                        'id' => optional($idField)->value,
                        'fields' => $indexFields
                            ->map->prepareDependants($request, $indexFields)
                            ->map->toDisplayJson($request, $resource)
                            ->all(),
                    ];
                })->values()->all(),
        ];
    }
}
