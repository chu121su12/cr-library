<?php

namespace CR\Library\Avon\Fields;

class Currency extends Field
{
    public $component = 'currency-field';

    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);

        $this->textAlign('right');
    }

    public function decimalCharacter($decimalCharacter)
    {
        return $this->withMeta(['decimalCharacter' => $decimalCharacter]);
    }

    public function digitGroupSeparator($digitGroupSeparator)
    {
        return $this->withMeta(['digitGroupSeparator' => $digitGroupSeparator]);
    }

    public function indexName($name)
    {
        return $this->withMeta(['indexName' => $name]);
    }

    public function max($max)
    {
        return $this->withMeta(['max' => $max]);
    }

    public function min($min)
    {
        return $this->withMeta(['min' => $min]);
    }

    public function prefix($prefix)
    {
        return $this->withMeta(['prefix' => $prefix]);
    }

    public function sufix($sufix)
    {
        return $this->withMeta(['sufix' => $sufix]);
    }
}
