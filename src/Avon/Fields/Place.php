<?php

namespace CR\Library\Avon\Fields;

class Place extends Field
{
    public $component = 'place-field';

    protected $addressLine = 'address_line';

    protected $city = 'city';

    protected $country = 'country';

    protected $latitude = 'latitude';

    protected $longitude = 'longitude';

    protected $postalCode = 'postal_code';

    protected $secondAddressLine = 'second_address_line';

    protected $state = 'state';

    protected $suburb = 'suburb';

    public function addressLine($column)
    {
        $this->addressLine = $column;

        return $this;
    }

    public function city($column)
    {
        $this->city = $column;

        return $this;
    }

    public function country($column)
    {
        $this->country = $column;

        return $this;
    }

    public function latitude($column)
    {
        $this->latitude = $column;

        return $this;
    }

    public function longitude($column)
    {
        $this->longitude = $column;

        return $this;
    }

    public function postalCode($column)
    {
        $this->postalCode = $column;

        return $this;
    }

    public function _resourceToJson($request, $resource, $model)
    {
        return \array_merge(parent::resourceToJson($request, $resource, $model), [
            'addressLine' => (string) $this->addressLine,
            'city' => (string) $this->city,
            'country' => (string) $this->country,
            'latitude' => (string) $this->latitude,
            'longitude' => (string) $this->longitude,
            'postalCode' => (string) $this->postalCode,
            'secondAddressLine' => (string) $this->secondAddressLine,
            'state' => (string) $this->state,
            'suburb' => (string) $this->suburb,
        ]);
    }

    public function secondAddressLine($column)
    {
        $this->secondAddressLine = $column;

        return $this;
    }

    public function state($column)
    {
        $this->state = $column;

        return $this;
    }

    public function suburb($column)
    {
        $this->suburb = $column;

        return $this;
    }
}
