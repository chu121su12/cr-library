<?php

namespace CR\Library\Avon\Fields;

use Illuminate\Support\Collection;

class Stack extends Field
{
    public $component = 'stack-field';

    protected $creationVisibility = false;

    protected $stacks;

    protected $updatingVisibility = false;

    public function __construct($name = null, $stacks = null)
    {
        parent::__construct($name, null);

        $this->attribute = null;
        $this->stacks = $stacks;
    }

    public function fillForAction($request, $model)
    {
        // none
    }

    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        // none
    }

    protected function prepareValue($request, $resource, $value)
    {
        return Collection::wrap(value($this->stacks, $resource, $value))
            ->map(static function ($field, $key) {
                if ($field instanceof Field) {
                    return $field;
                }

                return Line::make(\is_int($key) ? 'Line' : $key, static function () use ($field) {
                    return value($field);
                });
            })
            ->map->toDisplayJson($request, $resource)
            ->values()
            ->all();
    }
}
