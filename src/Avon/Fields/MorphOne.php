<?php

namespace CR\Library\Avon\Fields;

class MorphOne extends HasOne
{
    protected function updateRelation($relatedModel, $newRelatedModel, $model, $relationQuery)
    {
        $relatedModel->{$relationQuery->getForeignKeyName()} = null;
        $relatedModel->{$relationQuery->getMorphType()} = null;

        $newRelatedModel->{$relationQuery->getForeignKeyName()} = $model->getKey();
        $newRelatedModel->{$relationQuery->getMorphType()} = $relationQuery->getMorphClass();

        return static function () use ($relatedModel, $newRelatedModel) {
            $relatedModel->save();
            $newRelatedModel->save();
        };
    }
}
