<?php

namespace CR\Library\Avon\Fields;

class PasswordConfirmation extends Password
{
    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        // none
    }

    protected function prepareValue($request, $resource, $value)
    {
        return '';
    }
}
