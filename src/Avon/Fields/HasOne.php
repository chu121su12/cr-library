<?php

namespace CR\Library\Avon\Fields;

class HasOne extends HasMany
{
    protected function resourceToJson($request, $resource)
    {
        $app = app(\CR\Library\Avon\Application::class);
        $resourceClass = $this->resourceClass;

        return \array_merge(
            parent::resourceToJson($request, $resource),
            [
                'relation' => [
                    // 'type' => 'hasOne',
                    'full' => true,

                    'label' => $resourceClass::label(),
                    'routes' => [
                        'resource' => $app->getResourceByClass($resourceClass),
                        'viaResource' => $app->getResourceByClass($resource),
                        'viaResourceId' => $resource->getIdFieldValue(),
                        'viaRelationship' => $this->attribute,
                    ],
                ],
            ]
        );
    }
}
