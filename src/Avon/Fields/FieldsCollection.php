<?php

namespace CR\Library\Avon\Fields;

use CR\Library\Avon\Contracts\Cover;
use CR\Library\Avon\Filters\FieldDayFilter;
use CR\Library\Avon\Filters\FieldOptionsFilter;
use CR\Library\Avon\Filters\FieldTextFilter;
use CR\Library\Avon\Panel;
use Illuminate\Support\Collection;

class FieldsCollection
{
    protected $collection;

    protected $fields;

    protected $panels;

    public function __construct($fields, $label = 'Detail')
    {
        $this->collection = Collection::wrap($fields)
            ->filter();

        $this->fields = $this->collection
            ->map(
                static function ($fieldOrPanel) {
                    if ($fieldOrPanel instanceof Panel) {
                        return $fieldOrPanel->fields();
                    }

                    return [$fieldOrPanel];
                }
            )
            ->flatten(1)
            ->values();

        $this->panels = $this->collection
            ->filter(
                static function ($fieldOrPanel) {
                    return $fieldOrPanel instanceof Panel;
                }
            )

            ->prepend($this->defaultPanel(static function () use ($label) {
                return value($label);
            }))
            ->values();
    }

    public function allfields()
    {
        return $this->fields;
    }

    public function allPanels()
    {
        return $this->panels;
    }

    public function avatarField($request)
    {
        return $this->fields->first(static function ($field) {
            return $field instanceof Cover; // Avatar;
        });
    }

    public function collection()
    {
        return $this->collection;
    }

    public function creationFields($request)
    {
        return $this->fields->filter(function ($field) use ($request) {
            if ($this->indexingFields($field)) {
                return false;
            }

            if (! $field->authorizedToSee($request)) {
                return false;
            }

            if ($field->isIndexField()) {
                return false;
            }

            return $field->resolveCreationVisibility($request);
        })->values();
    }

    public function defaultPanelLabel($label)
    {
        $this->panels->first()->setName(value($label));

        return $this;
    }

    public function deleteFileFields($request, $fileField)
    {
        return $this->fields->filter(function ($field) use ($request, $fileField) {
            if ($this->indexingFields($field)) {
                return false;
            }

            if ($field->attribute !== $fileField) {
                return false;
            }

            if (! $field->authorizedToSee($request)) {
                return false;
            }

            return $field->resolveUpdatingVisibility($request);
        })->values();
    }

    public function detailFields($request)
    {
        return $this->fields->filter(static function ($field) use ($request) {
            if (! $field->authorizedToSee($request)) {
                return false;
            }

            return $field->resolveDetailVisibility($request);
        })->values();
    }

    public function downloadFileFields($request, $fileField)
    {
        return $this->fields->filter(function ($field) use ($request, $fileField) {
            if ($this->indexingFields($field)) {
                return false;
            }

            if ($field->attribute !== $fileField) {
                return false;
            }

            if (! $field->authorizedToSee($request)) {
                return false;
            }

            return $field->resolveDetailVisibility($request);
        })->values();
    }

    public function editFields($request)
    {
        return $this->fields->filter(function ($field) use ($request) {
            if ($this->indexingFields($field)) {
                return false;
            }

            if (! $field->authorizedToSee($request)) {
                return false;
            }

            if ($field->isIndexField()) {
                return false;
            }

            return $field->resolveUpdatingVisibility($request);
        })->values();
    }

    public function idField($request)
    {
        return $this->fields->first(static function ($field) {
            return $field instanceof ID;
        });
    }

    public function indexFields($request)
    {
        return $this->fields->filter(static function ($field) use ($request) {
            $field->panel = null;

            if (! $field->authorizedToSee($request)) {
                return false;
            }

            if ($field->isIndexField()) {
                return false;
            }

            return $field->resolveIndexVisibility($request) || $field->resolvePreviewVisibility($request);
        })->values();
    }

    public function storeFields($request)
    {
        return $this->fields->filter(function ($field) use ($request) {
            if ($this->indexingFields($field)) {
                return false;
            }

            if (! $field->authorizedToSee($request)) {
                return false;
            }

            if ($field->isIndexField()) {
                return false;
            }

            if ($field->computedResolver) {
                return false;
            }

            return $field->resolveCreationVisibility($request);
        })->values();
    }

    public function updateFields($request)
    {
        return $this->fields->filter(function ($field) use ($request) {
            if ($this->indexingFields($field)) {
                return false;
            }

            if (! $field->authorizedToSee($request)) {
                return false;
            }

            if ($field->isIndexField()) {
                return false;
            }

            if ($field->computedResolver) {
                return false;
            }

            return $field->resolveUpdatingVisibility($request);
        })->values();
    }

    private function defaultPanel($label)
    {
        $panel = new Panel($label, []);
        $panel->key = null;
        $panel->toolbar = true;

        return $panel;
    }

    private function indexingFields($field)
    {
        return $field instanceof BelongsToMany;
    }

    public static function forDetails($models, $fields, $request = null)
    {
        $request = $request ?: request();
        $instance = new self($fields);

        return Collection::wrap($models)->map(static function ($model) use ($request, $instance) {
            $idField = $instance->idField($request);
            $detailFields = $instance->detailFields($request);

            if ($idField && $detailFields->search($idField, true) === false) {
                $idField->toDisplayJson($request, $model);
            }

            // do not inline! this needs to be separate to resove potential id field
            $fields = $detailFields
                ->map->toDisplayJson($request, $model)
                ->all();

            return [
                'id' => optional($idField)->value,
                'fields' => $fields,
            ];
        });
    }

    public static function filterableDate($name, $attribute, $resolver)
    {
        if (! \is_string($attribute) || ! $resolver) {
            return [];
        }

        if ($resolver === true) {
            return [
                (new FieldDayFilter("{$name} - From", $attribute))->from(),
                (new FieldDayFilter("{$name} - To", $attribute))->to(),
            ];
        }

        if ($resolver === 'daily') {
            return [
                new FieldDayFilter($name, $attribute),
            ];
        }

        return [
            self::filterableCallback(
                new FieldDayFilter($name, $attribute),
                $attribute,
                $resolver
            ),
        ];
    }

    public static function filterableOption($name, $attribute, $resolver, $options)
    {
        if (! \is_string($attribute) || ! $resolver) {
            return [];
        }

        $filter = (new FieldOptionsFilter($name, $attribute))
            ->withOptions($options);

        return [
            $resolver === true
                ? $filter
                : self::filterableCallback($filter, $attribute, $resolver),
        ];
    }

    public static function filterableText($name, $attribute, $resolver, $inline, $pre)
    {
        if (! \is_string($attribute) || ! $resolver) {
            return [];
        }

        $filter = new FieldTextFilter($name, $attribute); // start

        $filter->inline($inline);

        $filter->asPreFilter($pre);

        if (\in_array($resolver, ['anywhere', 'end', 'equal', 'start'], true)) {
            $method = 'match' . \ucfirst($resolver);

            return [
                $filter->{$method}(),
            ];
        }

        return [
            $resolver === true
                ? $filter
                : self::filterableCallback($filter, $attribute, $resolver),
        ];
    }

    private static function filterableCallback($filter, $attribute, $resolver)
    {
        return $filter->applyWith(static function ($r, $q, $v) use ($resolver, $attribute) {
            return value($resolver, $r, $q, $v, $attribute);
        });
    }
}
