<?php

namespace CR\Library\Avon\Fields;

use Illuminate\Support\Str;

class Slug extends Text
{
    // public $component = 'text-field';

    protected $from;

    protected $separator = '-';

    public function from($column)
    {
        $this->from = $column;

        return $this;
    }

    public function separator($separator)
    {
        $this->separator = $separator;

        return $this;
    }

    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        if ($request->has($attribute)) {
            if (isset($this->from)
                && (string) $value === ''
                && (string) $model->{$attribute} === ''
                && (string) $model->{$this->from} !== '') {
                $model->{$attribute} = Str::slug($model->{$this->from}, $this->separator);
            }
        }
    }
}
