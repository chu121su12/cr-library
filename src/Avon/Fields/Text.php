<?php

namespace CR\Library\Avon\Fields;

use Illuminate\Support\Arr;

class Text extends Field
{
    public $component = 'text-field';

    protected $asHtml = false;

    protected $copyable = false;

    protected $suggestions = [];

    public function asHtml($value = true)
    {
        $this->asHtml = $value;

        return $this;
    }

    public function copyable($value = true)
    {
        $this->copyable = $value;

        return $this;
    }

    public function suggestions($value)
    {
        $this->suggestions = $value;

        return $this;
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'suggestions' => Arr::wrap($this->suggestions),
                'asHtml' => (bool) $this->asHtml,
                'copyable' => (bool) $this->copyable,
            ],
            parent::resourceToJson($request, $resource)
        );
    }
}
