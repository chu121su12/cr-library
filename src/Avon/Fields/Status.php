<?php

namespace CR\Library\Avon\Fields;

use Illuminate\Support\Arr;

class Status extends Field
{
    public $component = 'status-field';

    protected $failedWhen = [];

    protected $loadingWhen = [];

    public function failedWhen()
    {
        $texts = \func_get_args();

        $this->failedWhen = \count($texts) > 0 ? $texts : Arr::wrap($texts);

        return $this;
    }

    public function loadingWhen()
    {
        $texts = \func_get_args();

        $this->loadingWhen = \count($texts) > 0 ? $texts : Arr::wrap($texts);

        return $this;
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'failedTexts' => \array_values((array) $this->failedWhen),
                'loadingTexts' => \array_values((array) $this->loadingWhen),
            ],
            parent::resourceToJson($request, $resource)
        );
    }
}
