<?php

namespace CR\Library\Avon\Fields;

class Avatar extends Image implements \CR\Library\Avon\Contracts\Cover
{
    public $downloadable = false;

    public function resolveThumbnailUrl()
    {
        if ($url = parent::resolveThumbnailUrl()) {
            return $url;
        }

        $hash = \substr(\md5($this->value . \date('Y-m-d')), 0, 6);

        return "https://picsum.photos/seed/{$hash}/72/72";
    }
}
