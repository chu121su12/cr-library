<?php

namespace CR\Library\Avon\Fields;

class Code extends Field
{
    public $component = 'code-field';

    protected $indexVisibility = false;

    protected $json = false;

    public function getFillValue($request)
    {
        $value = $request->get($this->attribute);

        if ($this->json) {
            return $this->resolveFieldValue(\json_decode($value, true));
        }

        return $this->resolveFieldValue($value);
    }

    public function json($json = true)
    {
        $this->json = $json;

        return $this->language('javascript')->withMeta(['json' => $json]);
    }

    public function language($language)
    {
        return $this->withMeta(['language' => $language]);
    }

    protected function resolve($resource, $attribute = null)
    {
        parent::resolve($resource, $attribute);

        if ($this->json) {
            $this->value = \is_array($this->value) || \is_object($this->value)
                ? \json_encode($this->value, \JSON_PRETTY_PRINT)
                : \json_encode(\json_decode((string) $this->value), \JSON_PRETTY_PRINT);
        }
    }
}
