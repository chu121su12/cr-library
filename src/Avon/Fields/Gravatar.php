<?php

namespace CR\Library\Avon\Fields;

class Gravatar extends Avatar
{
    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        $args = \func_get_args();
        if (! isset($args[0])) {
            $args[0] = 'Email';
        }

        parent::__construct(...\func_get_args());

        $avatarResolver = static function ($value) {
            return \sprintf('https://www.gravatar.com/avatar/%s?s=256', \md5(\mb_strtolower(\trim($value))));
        };

        $this->preview($avatarResolver)
            ->thumbnail($avatarResolver);
    }
}
