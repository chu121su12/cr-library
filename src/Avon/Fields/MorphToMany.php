<?php

namespace CR\Library\Avon\Fields;

class MorphToMany extends BelongsToMany
{
    // use SearchableRelation;

    public $component = 'morph-to-many-field';

    protected function prepareResolvedValue($value, $model)
    {
        // should not resolve value

        $parentResource = app(\CR\Library\Avon\Resources::class)->getResourceFromModel($model);

        $resource = $this->resourceClass;

        $this->withMeta([
            'relation' => [
                'type' => 'morphToMany',
                'indexRelation' => true,
                'resource' => $resource::uriKey(),
                'label' => $resource::label(),

                'viaResource' => $parentResource::uriKey(),
                'viaResourceId' => (string) $model->getKey(),
                'viaRelationship' => $this->attribute,
            ],
        ]);
    }
}
