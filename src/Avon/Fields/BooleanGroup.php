<?php

namespace CR\Library\Avon\Fields;

use CR\Library\Avon\Concerns\OptionableField;
use Exception;

class BooleanGroup extends Field
{
    use OptionableField;

    public $component = 'boolean-group-field';

    protected $hideFalseValues = false;

    protected $hideTrueValues = false;

    protected $noValueText = 'No Data';

    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);

        $this->option(static function ($value, $key) {
            return \array_merge(
                [
                    'key' => (string) $key,
                ],
                \is_array($value) ? $value : [
                    'label' => (string) $value,
                    'group' => null,
                ]
            );
        });
    }

    public function resolveAsJson()
    {
        return $this->resolveUsing(static function ($v) {
            return $v === '' ? null : (\is_string($v) ? \json_decode($v, true) : $v);
        });
    }

    public function hideFalseValues($flag = true)
    {
        $this->hideFalseValues = $flag;

        return $this;
    }

    public function hideTrueValues($flag = true)
    {
        $this->hideTrueValues = $flag;

        return $this;
    }

    public function noValueText($text)
    {
        $this->noValueText = $text;

        return $this;
    }

    protected function resolveFieldValue($value)
    {
        $keyValue = \json_decode($value, true);
        \json_decode('');

        $options = [];

        foreach ($this->getOptions() as $option) {
            $options[$option['key']] = isset($keyValue[$option['label']]) && $keyValue[$option['label']];
        }

        return parent::resolveFieldValue($options);
    }

    protected function prepareValue($request, $resource, $keyValue)
    {
        if ($keyValue === null) {
            $keyValue = [];
        }
        elseif (! \is_array($keyValue) && ! \is_object($keyValue)) {
            throw new Exception(\sprintf('Resolved values must be in in json/array. (%s)', $this->attribute));
        }

        foreach ($keyValue as $key => $value) {
            if (! \is_scalar($value)) {
                $keyValue[$key] = true;
                // throw new Exception('values in json/array can only be a scalar value (string/integer/etc.)');
            }
        }

        $options = [];

        foreach ($this->getOptions()->pluck('label', 'key') as $key => $label) {
            $options[$label] = isset($keyValue[$key]) && $keyValue[$key];
        }

        // to display
        return collect($options)
            ->reject(function ($value) {
                return ($this->hideFalseValues && ! $value)
                    || ($this->hideTrueValues && $value);
            })->all();
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'noValueText' => (string) $this->noValueText,
            ],
            parent::resourceToJson($request, $resource)
        );
    }
}
