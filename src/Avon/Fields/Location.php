<?php

namespace CR\Library\Avon\Fields;

use CR\Library\Helpers\Helpers;
use Exception;
use Illuminate\Support\Arr;

class Location extends Field
{
    public $component = 'location-field';

    protected $ipLocationResolver;

    public function locateIpUsing($callback)
    {
        $this->ipLocationResolver = $callback;

        return $this;
    }

    protected function approximateClientIpLocation()
    {
        try {
            if ($location = value($this->ipLocationResolver, self::getClientIp())) {
                return Arr::wrap($location);
            }
        }
        catch (Exception $e) {
            Helpers::reportExceptionStack($e);
        }

        return [];
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'approximatedClientIpLocation' => \implode(',', $this->approximateClientIpLocation()),
            ],
            parent::resourceToJson($request, $resource)
        );
    }

    // see https://github.com/Torann/laravel-geoip/blob/master/src/GeoIP.php#L230
    private static function getClientIp()
    {
        $remotesKeys = [
            'HTTP_X_FORWARDED_FOR',
            'HTTP_CLIENT_IP',
            'HTTP_X_REAL_IP',
            'HTTP_X_FORWARDED',
            'HTTP_FORWARDED_FOR',
            'HTTP_FORWARDED',
            'REMOTE_ADDR',
            'HTTP_X_CLUSTER_CLIENT_IP',
        ];

        foreach ($remotesKeys as $key) {
            if ($address = \getenv($key)) {
                foreach (\explode(',', $address) as $ip) {
                    if (self::isValidIp($ip)) {
                        return $ip;
                    }
                }
            }
        }

        return '127.0.0.1';
    }

    private static function isValidIp($ip)
    {
        return ! (
            ! \filter_var($ip, \FILTER_VALIDATE_IP, \FILTER_FLAG_IPV4 | \FILTER_FLAG_NO_PRIV_RANGE | \FILTER_FLAG_NO_RES_RANGE)
            && ! \filter_var($ip, \FILTER_VALIDATE_IP, \FILTER_FLAG_IPV6 | \FILTER_FLAG_NO_PRIV_RANGE)
        );
    }
}
