<?php

namespace CR\Library\Avon\Fields;

use Closure;
use CR\Library\Avon\Concerns;
use CR\Library\Avon\Helpers;
use CR\Library\Avon\Resource;
use Exception;
use Illuminate\Support\Fluent;
use Illuminate\Support\Str;

class Field
{
    use Concerns\AuthorizedToSee;
    use Concerns\Component;
    use Concerns\FieldVisibility;
    // use Concerns\RendersAssets;
    use Concerns\WithMeta;

    public $computedResolver;

    public $panel;

    public $resource;

    public $value;

    protected $attribute;

    protected $attributeFillResolver;

    protected $creationRules = [];

    protected $defaultFormValueResolver;

    protected $dependentResolverData;

    protected $displayValueResolver;

    protected $filterAsPre = false;

    protected $filterInline = false;

    protected $filterResolver;

    protected $name;

    protected $modelValueResolver;

    protected $nullableFlag = false;

    protected $nullableResolver = [null, ''];

    protected $nullFillResolver;

    protected $pivot = false;

    protected $readonlyResolver = false;

    protected $requiredResolver = false;

    protected $sortableResolver = false;

    protected $updateRules = [];

    protected $validationRules = [];

    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        if ($name === null) {
            if (\mb_strpos($class = static::class, 'CR\Library\Avon\\') === 0) {
                throw new Exception(\sprintf('Name not defined. (%s)', $class));
            }

            $name = Helpers::classToProperName($class);
        }

        $this->name = $name;
        $this->modelValueResolver = $resolveCallback;

        if (\is_string($attribute)) {
            $this->attribute = $attribute;
        }
        elseif ($attribute) {
            $this->computedResolver = $attribute;
        }
        else {
            $this->attribute = Str::slug($name, '_');
        }
    }

    public function asPivotField()
    {
        $this->pivot = true;

        return $this;
    }

    public function attribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function computeValueUsing($callback)
    {
        $this->computedResolver = $callback;

        return $this;
    }

    public function creationRules()
    {
        $this->creationRules[] = \func_get_args();

        return $this;
    }

    public function default_($callback)
    {
        $this->defaultFormValueResolver = $callback;

        return $this;
    }

    public function dependsOn(array $attributes, $callback)
    {
        $this->dependentResolverData = [
            $attributes,
            $callback,
        ];

        return $this;
    }

    public function displayUsing($callback)
    {
        $this->displayValueResolver = $callback;

        return $this;
    }

    public function fillModel($request, $model)
    {
        return $this->fill($request, $model);
    }

    public function getAttribute()
    {
        return $this->attribute;
    }

    public function getCreationValidationRules($request)
    {
        $rules = $this->normalizeValidationRules([
            $this->validationRules,
            $this->creationRules,
        ]);

        return [$rules['attribute'] => $rules['rules']];
    }

    public function getUpdateValidationRules($request)
    {
        $normalized = $this->normalizeValidationRules([
            $this->validationRules,
            $this->updateRules,
        ]);

        foreach ($normalized['rules'] as $key => $rule) {
            if (\is_string($rule)) {
                $normalized['rules'][$key] = \preg_replace('/{{\s*resourceId\s*}}/', $request->resourceId, $rule);
            }
        }

        return [$normalized['attribute'] => $normalized['rules']];
    }

    public function filterable($callback = true)
    {
        $this->filterResolver = $callback;

        return $this;
    }

    public function filterAsPre($set = true)
    {
        $this->filterAsPre = (bool) $set;

        return $this;
    }

    public function inlineFilter($set = true)
    {
        $this->filterInline = (bool) $set;

        return $this;
    }

    public function resolveFilterableFilters()
    {
        return FieldsCollection::filterableText($this->name(), $this->attribute, $this->filterResolver, $this->filterInline, $this->filterAsPre);
    }

    public function help($string)
    {
        return $this->withMeta(['helpText' => $string]);
    }

    public function isIndexField()
    {
        return $this instanceof HasMany || $this instanceof BelongsToMany;
    }

    public function isPivotField()
    {
        return $this->pivot;
    }

    public function nullable($flag = true)
    {
        $this->nullableFlag = $flag;

        return $this;
    }

    public function fillNullUsing($callback)
    {
        $this->nullFillResolver = $callback;

        return $this;
    }

    public function nullValues($callback)
    {
        $this->nullableResolver = $callback;

        return $this;
    }

    public function placeholder($string)
    {
        return $this->withMeta(['placeholder' => $string]);
    }

    public function prepareDependants($request, $otherFields)
    {
        if ($this->dependentResolverData) {
            $object = (object) [];

            list($attributes, $callback) = $this->dependentResolverData;

            $otherFields
                ->filter(static function ($field) use ($attributes) {
                    return \in_array($field->getAttribute(), $attributes, true);
                })
                ->each->fillForAction($request, $object);

            $callback($this, $request, new Fluent((array) $object));
        }

        return $this;
    }

    public function readonly($callback = true)
    {
        $this->readonlyResolver = $callback;

        return $this;
    }

    public function required($callback = true)
    {
        $this->requiredResolver = $callback;

        return $this;
    }

    public function resolveUsing($callback)
    {
        $this->modelValueResolver = $callback;

        return $this;
    }

    public function rules()
    {
        $this->validationRules[] = \func_get_args();

        return $this;
    }

    public function sortable($callback = true)
    {
        $this->sortableResolver = $callback;

        return $this;
    }

    public function stacked($flag = true)
    {
        return $this->withMeta(['stacked' => $flag]);
    }

    public function textAlign($align)
    {
        return $this->withMeta(['textAlign' => $align]);
    }

    public function toDisplayJson($request, $resource)
    {
        $value = $this->resolveToValue($resource);

        $fillValue = $this->prepareValue(
            $request,
            $resource,
            isset($value) ? $value : value($this->defaultFormValueResolver, $value)
        );

        return \array_merge(
            $this->resourceToJson($request, $resource),
            [
                'value' => $fillValue,
            ],
            isset($this->displayValueResolver) ? [
                'displayValue' => value($this->displayValueResolver, $resource, $value, $fillValue),
            ] : []
        );
    }

    public function updateRules()
    {
        $this->updateRules[] = \func_get_args();

        return $this;
    }

    protected function fill($request, $model)
    {
        if ($this->attributeFillResolver instanceof Closure) {
            return value(
                $this->attributeFillResolver,
                $request,
                $model,
                $this->attribute,
                $this->attribute /* 2? */ ,
                $this->getFillValue($request),
                $this
            );
        }

        if (null === $this->attribute) {
            return;
        }

        if ($this->isReadonly($request)) {
            return;
        }

        return $this->fillModelAttribute(
            $request,
            $model,
            $this->attribute,
            $this->getFillValue($request)
        );
    }

    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        if ($request->has($attribute)) {
            $model->{$attribute} = $value;
        }
    }

    public function fillForAction($request, $model)
    {
        if (! isset($this->attribute)) {
            throw new Exception(\sprintf('Cannot fill with empty attribute. (%s @%s)', $this->name, static::class));
        }

        if (! isset($model->{$this->attribute})) {
            $model->{$this->attribute} = null;
        }

        return $this->fill($request, $model);
    }

    public function fillUsing($callback)
    {
        $this->attributeFillResolver = $callback;

        return $this;
    }

    protected function getFillValue($request)
    {
        return $this->resolveFieldValue(
            $request->get($this->attribute)
        );
    }

    protected function isNullable()
    {
        return $this->nullableFlag;
    }

    protected function isReadonly($request)
    {
        return (bool) value($this->readonlyResolver, $request);
    }

    protected function isRequired($request)
    {
        if ($this->resolveRequiredCallback()) {
            return true;
        }

        $validationRules = $this->normalizeValidationRules([
            $this->validationRules,
            $request ? $this->creationRules : $this->updateRules,
        ])['rules'];

        return \array_search('required', $validationRules, true) !== false;
    }

    public function isSortable()
    {
        return value($this->sortableResolver);
    }

    protected function normalizeValidationRules($batches)
    {
        $validationRules = [];

        if ($this->resolveRequiredCallback()) {
            $validationRules[] = ['required'];
        }

        foreach ($batches as $batch) {
            foreach ($batch as $rules) {
                foreach ($rules as $rule) {
                    if (\is_string($rule)) {
                        $validationRules[] = \explode('|', $rule);
                    }
                    elseif (\is_array($rule)) {
                        $validationRules[] = $rule;
                    }
                    elseif ($rule) {
                        $validationRules[] = [$rule];
                    }
                }
            }
        }

        return [
            'name' => $this->name(),
            'indexName' => $this->name(),
            'attribute' => $this->attribute,
            'rules' => \count($validationRules)
                ? \array_merge(...$validationRules)
                : [],
        ];
    }

    protected function prepareResolvedValue($value, $model)
    {
        return $value;
    }

    protected function prepareValue($request, $resource, $value)
    {
        return $value;
    }

    protected function resolve($resource, $attribute = null)
    {
        $this->resource = $resource;

        if ($this->computedResolver) {
            $this->value = value($this->computedResolver, $resource);

            return;
        }

        $attribute = isset($attribute) ? $attribute : $this->attribute;

        if (! isset($this->modelValueResolver)) {
            $this->value = $this->resolveValueViaAttribute($resource, $attribute);
        }
        elseif (\is_callable($this->modelValueResolver)) {
            $this->value = value(
                $this->modelValueResolver,
                $this->resolveValueViaAttribute($resource, $attribute),
                $resource,
                $attribute
            );
        }
    }

    public function resolveToValue($resource)
    {
        $this->resolve($resource, $this->getAttribute());

        return $this->value;
    }

    public function resolveDataApi($request, $key)
    {
        $method = Str::camel("data-{$key}");

        if (\method_exists($this, $method)) {
            return $this->{$method}($request);
        }
    }

    public function resolveActionApi($request, $key)
    {
        $method = Str::camel("action-{$key}");

        if (\method_exists($this, $method)) {
            return $this->{$method}($request);
        }
    }

    public function resolveViewApi($request)
    {
        $data = \method_exists(static::class, 'renderView') ? (string) static::renderView() : '';

        $signature = \md5($data);

        if ($request->query('signature') === $signature) {
            return response()->json([
                'payloadSignature' => $signature,
            ], 200, [], \JSON_UNESCAPED_UNICODE);
        }

        return response()->json([
            'component' => $this->resolveComponent(),
            'payload' => $data,
            'payloadSignature' => $signature,
        ], 200, [], \JSON_UNESCAPED_UNICODE);
    }

    protected function resolveFieldValue($value)
    {
        $isNullValue = function ($value) {
            return \is_array($nullableResolver = $this->nullableResolver)
                ? \in_array($value, $nullableResolver, true)
                : ($nullableResolver instanceof Closure) && $nullableResolver($value);
        };

        $fillValue = $isNullValue($value) ? null : $value;

        if ($fillValue !== null) {
            return $fillValue;
        }

        if (isset($this->nullFillResolver)) {
            return \is_callable($this->nullFillResolver)
                ? value($this->nullFillResolver, $value)
                : $this->nullFillResolver;
        }

        return null;
    }

    protected function resolveRequiredCallback()
    {
        return (bool) value($this->requiredResolver);
    }

    protected function resolveValueViaAttribute($resource, $attribute)
    {
        $resource = $resource instanceof Resource ? $resource->resource() : $resource;

        if (isset($attribute)) {
            return data_get($resource, $attribute);
        }
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'component' => $this->resolveComponent(),
                'name' => $this->name(),
                'indexName' => $this->name(),
                'pivot' => $this->isPivotField(),
                'attribute' => $this->getAttribute(),
                'sortable' => (bool) $this->isSortable(),
                'readonly' => (bool) $this->isReadonly($request),
                'nullable' => (bool) $this->isNullable(),
                'required' => (bool) $this->isRequired($request),
                'panel' => $this->panel,
                'stacked' => false,
                'visibleOnIndex' => $this->resolveIndexVisibility($request),
                'visibleOnPreview' => $this->resolvePreviewVisibility($request),
                'builtInComponent' => $this->isBuiltInComponent(),
            ],
            $this->resolveMeta($request)
        );
    }

    public static function make(...$arguments)
    {
        return new static(...$arguments);
    }
}
