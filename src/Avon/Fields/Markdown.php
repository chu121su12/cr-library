<?php

namespace CR\Library\Avon\Fields;

class Markdown extends Textarea
{
    public $component = 'markdown-field';

    public function inlineElementOnly($set = true)
    {
        return $this->withMeta(['inline' => (bool) $set]);
    }
}
