<?php

namespace CR\Library\Avon\Fields;

use Closure;

class BelongsToMany extends RelationshipField
{
    public $allowDuplicates = false;

    public $component = 'belongs-to-many-field';

    protected $actions = [];

    protected $pivotAlias = 'Pivot';

    protected $pivotFields = [];

    protected $pivotSearchable = false;

    protected $resourceFields = [];

    public function actions($callback)
    {
        $this->actions = $callback;

        return $this;
    }

    public function allowDuplicateRelations($set = true)
    {
        $this->allowDuplicates = $set;

        return $this;
    }

    public function fields($callback)
    {
        $this->pivotFields = $callback;

        return $this;
    }

    public function _getCreationField()
    {
        return tap(
            PivotBelongsToMany::make(
                $this->name(),
                request()->viaRelationship,
                app(\CR\Library\Avon\Resources::class)->getResourceFromUri(request()->relation)
            ),
            function ($field) {
                $field->allowDuplicates = $this->allowDuplicates;
                $field->searchable($this->pivotSearchable);
            }
        );
    }

    public function _getUpdateField()
    {
        return tap(
            PivotBelongsToMany::make(
                $this->name(),
                request()->viaRelationship,
                app(\CR\Library\Avon\Resources::class)->getResourceFromUri(request()->relation)
            ),
            function ($field) {
                $field->allowDuplicates = true;
                $field->searchable($this->pivotSearchable);
                $field->readonly();
            }
        );
    }

    public function referToPivotAs($alias)
    {
        $this->pivotAlias = $alias;

        return $this;
    }

    public function _resolveActions()
    {
        if (\is_array($this->actions)) {
            return [];
        }

        if (($actions = $this->actions) instanceof Closure) {
            return collect($actions())->each(function ($action) {
                $action->pivotGroup = $this->pivotAlias;
                $action->asPivotAction();
            })->all();
        }

        return [];
    }

    public function resolvePivotFields()
    {
        if (\is_array($this->pivotFields)) {
            return $this->pivotFields;
        }

        if (($pivotFields = $this->pivotFields) instanceof Closure) {
            return collect($pivotFields())->each->asPivotField()->all();
        }

        return [];
    }

    public function _searchable($searchable = true)
    {
        $this->pivotSearchable = $searchable;

        return $this;
    }

    protected function _buildRelationQuery($request)
    {
        $resourceClass = $this->resourceClass;

        return $this->queryRelatableResource($request, $resourceClass, $resourceClass::resourceQuery())->get()
            ->map(static function ($model) use ($resourceClass) {
                $resource = (new $resourceClass)->useWithModel($model);

                return [
                    'id' => (string) $model->getKey(),
                    'title' => $resource->title(),
                    'subtitle' => $resource->subtitle(),
                ];
            })->sortBy('title')->values();
    }

    protected function _fillModelAttribute($request, $model, $attribute, $value)
    {
        if (! $request->has($attribute)) {
            return;
        }

        // TODO: if via not match, error?
        // on create, if via, check for $value === $request->get('viaResourceId')

        $relationQuery = $model->{$attribute}();
        $foreignKeyName = $relationQuery->getForeignKeyName();

        if ($value === $model->{$foreignKeyName}) {
            return;
        }

        $model->{$foreignKeyName} = $value;
    }

    protected function _prepareResolvedValue($value, $model)
    {
        // should not resolve value

        $parentResource = app(\CR\Library\Avon\Resources::class)->getResourceFromModel($model);

        $resource = $this->resourceClass;

        $this->withMeta([
            'relation' => [
                'type' => 'belongsToMany',
                'indexRelation' => true,
                'resource' => $resource::uriKey(),
                'label' => $resource::label(),

                'viaResource' => $parentResource::uriKey(),
                'viaResourceId' => (string) $model->getKey(),
                'viaRelationship' => $this->attribute,
            ],
        ]);
    }

    protected function _resolveValueViaAttribute($model)
    {
        $resource = $this->resourceClass;

        $relation = $model->{$this->attribute};

        return $this->resourceInstance = $resource::newFromModel($relation);
    }

    protected function resourceToJson($request, $resource)
    {
        $app = app(\CR\Library\Avon\Application::class);
        $resourceClass = $this->resourceClass;

        return \array_merge(
            parent::resourceToJson($request, $resource),
            [
                'relation' => [
                    // 'type' => 'hasMany',
                    'full' => false,

                    'label' => $resourceClass::label(),
                    'routes' => [
                        'resource' => $app->getResourceByClass($resourceClass),
                        'viaResource' => $app->getResourceByClass($resource),
                        'viaResourceId' => $resource->getIdFieldValue(),
                        'viaRelationship' => $this->attribute,
                    ],
                ],
            ]
        );
    }
}
