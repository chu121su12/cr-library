<?php

namespace CR\Library\Avon\Fields;

use CR\Library\Avon\Application;
use Illuminate\Support\Facades\Storage;

class Image extends File
{
    public $downloadable = true;

    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        parent::__construct(...\func_get_args());

        $this->preview(function ($value, $disk) {
            if (isset($value)) {
                if (app()->isLocal()) {
                    $request = request();
                    $resourceName = $request->route('resource');
                    $resourceId = $this->resource->getIdFieldValue();

                    if ($resourceName && $resourceId) {
                        $app = app(Application::class);
                        $prefix = $app->getPrefix();
                        $fieldName = $this->getAttribute();

                        return $app->url("{$prefix}/api/resource/{$resourceName}/detail/{$resourceId}/field/{$fieldName}/preview");
                    }
                }

                return Storage::disk($disk)->url($value);
            }
        });

        $this->thumbnail(function ($value, $disk) {
            if (isset($value)) {
                if (app()->isLocal()) {
                    $request = request();
                    $resourceName = $request->route('resource');
                    $resourceId = $this->resource->getIdFieldValue();

                    if ($resourceName && $resourceId) {
                        $app = app(Application::class);
                        $prefix = $app->getPrefix();
                        $fieldName = $this->getAttribute();

                        return $app->url("{$prefix}/api/resource/{$resourceName}/detail/{$resourceId}/field/{$fieldName}/thumbnail");
                    }
                }

                return Storage::disk($disk)->url($value);
            }
        });
    }

    public function downloadPreview($request, $model)
    {
        $originalName = $this->originalNameColumn
            ? $model->{$this->originalNameColumn}
            : null;

        return Storage::disk($this->disk)
            ->response($this->value, $originalName);
    }

    public function downloadThumbnail($request, $model)
    {
        $originalName = $this->originalNameColumn
            ? $model->{$this->originalNameColumn}
            : null;

        return Storage::disk($this->disk)
            ->response($this->value, $originalName);
    }
}
