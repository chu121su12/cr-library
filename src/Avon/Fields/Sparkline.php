<?php

namespace CR\Library\Avon\Fields;

use Closure;
use CR\Library\Avon\Metrics\Trend;
use CR\Library\Avon\Metrics\TrendResult;
use Illuminate\Support\Arr;

class Sparkline extends Field
{
    public $component = 'sparkline-field';

    protected $asBarChart = false;

    protected $creationVisibility = false;

    protected $data;

    protected $height;

    protected $indexVisibility = false;

    protected $updatingVisibility = false;

    protected $width;

    public function asBarChart($flag = true)
    {
        $this->asBarChart = $flag;

        return $this;
    }

    public function data($data)
    {
        $this->data = $data;

        return $this;
    }

    public function height($height)
    {
        $this->height = $height;

        return $this;
    }

    public function _resourceToJson($request, $resource, $model)
    {
        return \array_merge(parent::resourceToJson($request, $resource, $model), [
            'data' => $this->resolveData($request, $model),
            'height' => (int) $this->height,
            'width' => (int) $this->width,
        ]);
    }

    public function width($width)
    {
        $this->width = $width;

        return $this;
    }

    protected function makeTrendData($data)
    {
        $trend = \array_map(static function ($value) {
            if (\is_scalar($value)) {
                return ['value' => $value];
            }

            if (isset($value['value'])) {
                return $value;
            }

            return ['value' => 0];
        }, \array_values(Arr::wrap($data)));

        return (new TrendResult)->trend($trend)->asBarChart($this->asBarChart);
    }

    protected function resolveData($request, $model)
    {
        if ($this->data instanceof Trend) {
            return $this->data->generateResult($request);
        }

        if (($data = $this->data) instanceof Closure) {
            return $this->makeTrendData($data());
        }

        return $this->makeTrendData($data);
    }
}
