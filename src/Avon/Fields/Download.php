<?php

namespace CR\Library\Avon\Fields;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Download extends Field
{
    public $component = 'download-field';

    protected $links;

    protected $downloadResolver;

    public function __construct($name = null, $links = null)
    {
        parent::__construct($name, null);

        $this->attribute = null;

        $this->links = collect(Arr::wrap($links))
            ->map(static function ($callback, $name) {
                return [
                    'key' => Str::kebab($name),
                    'name' => $name,
                    'callback' => $callback,
                ];
            })
            ->values();
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'links' => $this->links->map(static function ($v) {
                    return [
                        'key' => $v['key'],
                        'name' => $v['name'],
                    ];
                }),
            ],
            parent::resourceToJson($request, $resource)
        );
    }

    public function downloadFile($request, $model)
    {
        dd($request->get('download'));
    }
}
