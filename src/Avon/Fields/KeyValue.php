<?php

namespace CR\Library\Avon\Fields;

use Exception;

class KeyValue extends Field
{
    public $component = 'key-value-field';

    protected $actionText = 'Add Row';

    protected $disableAddingRows = false;

    protected $disableDeletingRows = false;

    protected $disableEditingKeys = false;

    protected $indexVisibility = false;

    protected $keyLabel = 'Key';

    protected $valueLabel = 'Value';

    public function actionText($label)
    {
        $this->actionText = $label;

        return $this;
    }

    public function disableAddingRows($state = true)
    {
        $this->disableAddingRows = $state;

        return $this;
    }

    public function disableDeletingRows($state = true)
    {
        $this->disableDeletingRows = $state;

        return $this;
    }

    public function disableEditingKeys($state = true)
    {
        $this->disableEditingKeys = $state;

        return $this;
    }

    public function getFillValue($request)
    {
        $value = $request->get($this->attribute);

        return $this->resolveFieldValue(\json_decode($value, true));
    }

    public function keyLabel($label)
    {
        $this->keyLabel = $label;

        return $this;
    }

    public function resolveAsJson()
    {
        return $this->resolveUsing(static function ($v) {
            return $v === '' ? null : (\is_string($v) ? \json_decode($v, true) : $v);
        });
    }

    public function valueLabel($label)
    {
        $this->valueLabel = $label;

        return $this;
    }

    protected function prepareValue($request, $resource, $keyValue)
    {
        if ($keyValue === null) {
            return [];
        }

        if (! \is_array($keyValue) && ! \is_object($keyValue)) {
            throw new Exception(\sprintf('Prepared values must be iterable key-value. (%s)', $this->attribute));
        }

        foreach ($keyValue as $key => $value) {
            if (! \is_scalar($value)) {
                $keyValue[$key] = \json_encode($value);
                // throw new Exception('values in json/array can only be a scalar value (string/integer/etc.)');
            }
        }

        return $keyValue;
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'keyLabel' => (string) $this->keyLabel,
                'valueLabel' => (string) $this->valueLabel,
                'actionText' => (string) $this->actionText,
                'disableEditingKeys' => (bool) $this->disableEditingKeys,
                'disableAddingRows' => (bool) $this->disableAddingRows,
                'disableDeletingRows' => (bool) $this->disableDeletingRows,
            ],
            parent::resourceToJson($request, $resource)
        );
    }
}
