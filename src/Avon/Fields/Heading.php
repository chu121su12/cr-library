<?php

namespace CR\Library\Avon\Fields;

class Heading extends Field
{
    public $component = 'heading-field';

    protected $asHtml = false;

    protected $indexVisibility = false;

    protected $nullableFlag = true;

    public function asHtml($value = true)
    {
        $this->asHtml = $value;

        return $this;
    }

    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        // none
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'asHtml' => (bool) $this->asHtml,
            ],
            parent::resourceToJson($request, $resource)
        );
    }
}
