<?php

namespace CR\Library\Avon\Fields;

use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;
use DateTimeInterface;
use Exception;

class DateTime extends Field
{
    public $component = 'date-time-field';

    protected $firstDayOfWeek = 1;

    protected $format = 'dddd, D MMMM YYYY, HH:mm:ss';

    protected $pickerFormat = 'l, j F Y, H:i:S'; // flatpickr, capital S

    protected $timezone = false;

    protected $useTime = true;

    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);

        $this->textAlign('right');
    }

    public function firstDayOfWeek($format)
    {
        // TODO: implement functionality

        $this->firstDayOfWeek = $format;

        return $this;
    }

    public function format($format)
    {
        $this->format = $format;

        return $this;
    }

    public function pickerDisplayFormat($format)
    {
        $this->pickerFormat = $format;

        return $this;
    }

    public function resolveFilterableFilters()
    {
        return FieldsCollection::filterableDate($this->name(), $this->attribute, $this->filterResolver);
    }

    public function resolveAsDateTime($parseFormat = true)
    {
        return $this->resolveUsing(static function ($v) use ($parseFormat) {
            if (! isset($v)) {
                return;
            }

            if (! $parseFormat) {
                return $v;
            }

            return \is_string($parseFormat)
                ? CarbonImmutable::createFromFormat($parseFormat, $v)
                : new CarbonImmutable($v);
        });
    }

    public function useClientTimezone($set = true)
    {
        $this->timezone = (bool) $set;

        return $this;
    }

    public function useTimezone($set)
    {
        $this->timezone = (string) $set;

        return $this;
    }

    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        if (! $request->has($attribute)) {
            return;
        }

        $set = $value === null || $value === ''
            ? value($this->nullFillResolver)
            : CarbonImmutable::make($value);

        $hasAttribute = isset($model->{$attribute}) && $model->{$attribute};

        if (! $hasAttribute || ! $set || $set->ne($model->{$attribute})) {
            $model->{$attribute} = $set;
        }
    }

    protected function prepareValue($request, $resource, $value)
    {
        if (! (($value instanceof DateTimeInterface) || $value === null)) {
            throw new Exception(\sprintf('Value of %s (%s: %s) must be null or an instance of \DateTimeInterface', $this->name(), $this->getAttribute(), $value));
        }

        if ($value !== null && ! ($value instanceof CarbonInterface)) {
            $value = new CarbonImmutable($value);
        }

        return $value;
    }

    protected function resolveTimezone()
    {
        if (\is_bool($this->timezone)) {
            return $this->timezone ? null : config('app.timezone');
        }

        return (string) $this->timezone;
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'useTime' => $this->useTime,
                'timezone' => $this->resolveTimezone(),
                'pickerFormat' => (string) $this->pickerFormat,
                'format' => (string) $this->format,
                'firstDayOfWeek' => (int) $this->firstDayOfWeek,
            ],
            parent::resourceToJson($request, $resource)
        );
    }
}
