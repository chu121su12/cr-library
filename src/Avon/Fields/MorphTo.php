<?php

namespace CR\Library\Avon\Fields;

use Closure;
use Exception;
use Illuminate\Support\Str;

class MorphTo extends RelationshipField
{
    // use \CR\Library\Avon\Concerns\ResourcePreview;
    use SearchableRelation;

    public $component = 'morph-to-field';

    protected $indexVisibility = false;

    protected function fillModelAttribute($request, $model, $attribute, $jsonString)
    {
        if (! $request->has($attribute)) {
            return;
        }

        // TODO: if via not match, error?
        // on create, if via, check for $value === $request->get('viaResourceId')

        $relationQuery = $model->{$attribute}();

        $foreignKeyName = $relationQuery->getForeignKeyName();

        $morphType = $relationQuery->getMorphType();

        if ($jsonString === null) {
            $model->{$foreignKeyName} = null;
            $model->{$morphType} = null;

            return;
        }

        $value = \json_decode($jsonString, true);

        abort_unless(
            $value && \is_array($value) && \count($value) === 2,
            422,
            'Invalid morph-to value.'
        );

        list($morphId, $morphResource) = $value;

        if ($morphId === $model->{$foreignKeyName}) {
            return;
        }

        $resource = app(\CR\Library\Avon\Resources::class)->getResourceFromUri($morphResource);

        $model->{$foreignKeyName} = $morphId;
        $model->{$morphType} = $resource::resourceModel();
    }

    protected function prepareResolvedValue($value, $model)
    {
        $request = request();

        $locked = false;

        $id = null;

        if ($via = $this->handlingRelatedResource($request)) {
            // TODO: if via not match, error?

            if ($model === null) {
                $id = $via['viaResourceId'];

                $locked = true;
            }
        }
        elseif ($model === null) {
            $value = optional();
        }
        elseif ($value !== null) {
            $id = $value->model()->getKey();
            if ($id !== null) {
                $id = (string) $id;
            }
        }

        if ($model === null || $id === null) {
            $this->withMeta([
                'relation' => [
                    'type' => 'morphTo',
                    'indexRelation' => false,
                    'resource' => null,
                    'label' => null,

                    'data' => [
                    ],

                    'options' => [],
                ],
            ]);

            return;
        }

        $resource = $this->resourceClass;

        $data = static::resourcePreviewJson($value, $request);

        $this->withMeta([
            'relation' => [
                'type' => 'morphTo',
                'indexRelation' => false,
                'resource' => $resource::uriKey(),
                'label' => $resource::label(),

                'data' => \array_merge($data, [
                    'authorizedToView' => $value->authorizedToView($request),
                ]),

                'locked' => $locked,
                'options' => [$data],
            ],
        ]);

        $this->resourceValue = $value;
    }

    protected function resolveDisplayValue($value, $request)
    {
        return $this->resolveRelationshipDisplayValue($value);
    }

    protected function _resolveValueViaAttribute($model)
    {
        $relation = $model->{$this->attribute};

        $resource = $this->resourceClass = app(\CR\Library\Avon\Resources::class)
            ->getResourceFromModel($relation);

        return $this->resourceInstance = $resource ? $resource::newFromModel($relation) : null;
    }

    public static function _make($name = null, $attribute = null, $resource = null)
    {
        if ($name === null) {
            throw new Exception(\sprintf('Name not defined. (%s)', static::class));
        }

        $instance = new static;
        $instance->name = $name;

        if (! \is_string($attribute) && $attribute instanceof Closure) {
            $instance->computedValueResolver = $attribute;
        }
        elseif (isset($attribute)) {
            $instance->attribute = $attribute;
        }
        else {
            $instance->attribute = Str::slug($name, '_');
        }

        $instance->initializeAfterMake();

        return $instance;
    }
}
