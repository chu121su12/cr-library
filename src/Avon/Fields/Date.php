<?php

namespace CR\Library\Avon\Fields;

class Date extends DateTime
{
    protected $format = 'dddd, D MMMM YYYY';

    protected $pickerFormat = 'l, j F Y';

    protected $useTime = false;
}
