<?php

namespace CR\Library\Avon\Fields;

use Illuminate\Support\Facades\Hash;

class Password extends Field
{
    public $component = 'password-field';

    protected $detailVisibility = false;

    protected $indexVisibility = false;

    protected $nullableFlag = true;

    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        if ($request->has($attribute)) {
            if ((string) $value !== '') {
                $model->{$attribute} = Hash::make($value);
            }
        }
    }

    protected function prepareValue($request, $resource, $value)
    {
        return $value === null ? null : '';
    }
}
