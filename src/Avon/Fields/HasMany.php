<?php

namespace CR\Library\Avon\Fields;

use Exception;

class HasMany extends RelationshipField
{
    public $component = 'has-many-field';

    public static function dynamicMake($label, $attribute, $relatedAttribute = null, $relatedClass = null)
    {
        $instance = new static($label, $attribute, $relatedClass);

        $relatedClass = $relatedClass ?: $instance->resourceClass;

        return $instance->resolveQueryUsing(static function ($request, $resource) use (
            $label,
            $attribute,
            $relatedClass,
            $relatedAttribute
        ) {
            if (! $relatedAttribute) {
                $idField = (new FieldsCollection($resource->fields($request)))->idField($request);
                if ($idField) {
                    $relatedAttribute = $idField->getAttribute();
                }
            }

            if (! $relatedAttribute) {
                throw new Exception(\sprintf('Cannot guess related attribute. (%s, %s, %s)', $label, $attribute, static::class));
            }

            return $relatedClass::resourceQuery()
                ->where($attribute, $resource->{$relatedAttribute});
        });
    }

    protected function fill($request, $model)
    {
        // noop
    }

    protected function resolve($resource, $attribute = null)
    {
        // noop
    }

    protected function resourceToJson($request, $resource)
    {
        $app = app(\CR\Library\Avon\Application::class);
        $resourceClass = $this->resourceClass;

        return \array_merge(
            parent::resourceToJson($request, $resource),
            [
                'relation' => [
                    // 'type' => 'hasMany',
                    'full' => false,

                    'label' => $resourceClass::label(),
                    'routes' => [
                        'resource' => $app->getResourceByClass($resourceClass),
                        'viaResource' => $app->getResourceByClass($resource),
                        'viaResourceId' => $resource->getIdFieldValue(),
                        'viaRelationship' => $this->attribute,
                    ],
                ],
            ]
        );
    }
}
