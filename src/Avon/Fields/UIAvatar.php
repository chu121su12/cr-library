<?php

namespace CR\Library\Avon\Fields;

use Illuminate\Support\Arr;

class UIAvatar extends Avatar
{
    protected $backgroundColor;

    protected $bold = false;

    protected $color;

    protected $fontSize;

    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        $args = \func_get_args();
        if (! isset($args[0])) {
            $args[0] = 'Name';
        }

        parent::__construct(...$args);

        $avatarResolver = function ($value) {
            return \sprintf('https://ui-avatars.com/api/?%s', Arr::query(Arr::whereNotNull([
                'background' => $this->backgroundColor,
                'bold' => $this->bold,
                'color' => $this->color,
                'font-size' => $this->fontSize,
                'name' => $value,
                'size' => 256,
            ])));
        };

        $this->preview($avatarResolver)
            ->thumbnail($avatarResolver);
    }

    public function backgroundColor($hexColor)
    {
        $this->backgroundColor = $hexColor;

        return $this;
    }

    public function bold($set = true)
    {
        $this->bold = $set;

        return $this;
    }

    public function color($hexColor)
    {
        $this->color = $hexColor;

        return $this;
    }

    public function fontSize($fontSize)
    {
        $this->fontSize = $fontSize;

        return $this;
    }
}
