<?php

namespace CR\Library\Avon\Fields;

use Illuminate\Support\Arr;

class Boolean extends Field
{
    public $component = 'boolean-field';

    protected $falseValue = false;

    protected $falseValues = ['false', false, 0];

    protected $inlineLabel = false;

    protected $trueValue = true;

    protected $trueValues = ['true', true, 1];

    public function __construct($name = null, $attribute = null, $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);

        $this->textAlign('center');
    }

    public function falseValue($value)
    {
        $this->falseValue = $value;

        return $this;
    }

    public function falseValues($value)
    {
        $this->falseValues = $value;

        return $this;
    }

    public function inlineLabel($value = true)
    {
        $this->inlineLabel = $value;

        return $this;
    }

    public function trueValue($value)
    {
        $this->trueValue = $value;

        return $this;
    }

    public function trueValues($value)
    {
        $this->trueValues = $value;

        return $this;
    }

    protected function getFillValue($request)
    {
        return $this->resolveFieldValue(
            $this->convertBooleanValue($request->get($this->attribute))
        );
    }

    protected function fillModelAttribute($request, $model, $attribute, $value)
    {
        if ($request->has($attribute)) {
            $model->{$attribute} = $value === null ? null : ($value ? $this->trueValue : $this->falseValue);
        }
    }

    protected function prepareValue($request, $resource, $value)
    {
        return $this->convertBooleanValue($value);
    }

    private function convertBooleanValue($value)
    {
        if ($this->falseValue === $value || \in_array($value, Arr::wrap(value($this->falseValues)), true)) {
            return false;
        }

        if ($this->trueValue === $value || \in_array($value, Arr::wrap(value($this->trueValues)), true)) {
            return true;
        }
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'inlineLabel' => (bool) $this->inlineLabel,
            ],
            parent::resourceToJson($request, $resource)
        );
    }
}
