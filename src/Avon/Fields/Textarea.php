<?php

namespace CR\Library\Avon\Fields;

class Textarea extends Field
{
    public $component = 'textarea-field';

    protected $alwaysShow = false;

    protected $displayExpanded = false;

    protected $indexVisibility = false;

    protected $rows = 3;

    public function alwaysShow($value = true)
    {
        $this->alwaysShow = $value;

        return $this;
    }

    public function displayExpanded($value = true)
    {
        $this->displayExpanded = $value;

        return $this;
    }

    public function rows($value = 3)
    {
        $this->rows = $value;

        return $this;
    }

    protected function resourceToJson($request, $resource)
    {
        return \array_merge(
            [
                'alwaysShow' => (bool) $this->alwaysShow,
                'displayExpanded' => (bool) $this->displayExpanded,
                'rows' => (int) $this->rows,
            ],
            parent::resourceToJson($request, $resource)
        );
    }
}
