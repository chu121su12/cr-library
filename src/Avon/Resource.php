<?php

namespace CR\Library\Avon;

use CR\Library\Avon\Fields\FieldsCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Traits\ForwardsCalls;

class Resource
{
    use Concerns\AuthorizesResource;
    use Concerns\Pageable;
    use ForwardsCalls;

    public static $debounce = 0.5;

    public static $defaultSort = true;

    public static $globallySearchable = true;

    public static $globalSearchLink = 'view'; // 'edit'

    public static $globalSearchResults = 5;

    public static $indexSelects;

    public static $model;

    public static $pagination = 'links'; // none. simple, links, load-more

    public static $perPageOptions = [5, 25, 100];

    public static $polling = false;

    public static $pollingInterval = 5;

    public static $preventFormAbandonment = false;

    public static $preventIndexSelection = false;

    public static $relatableSearchResults = 200;

    public static $relatedPerPageOptions = [3, 10, 25];

    public static $search = [];

    public static $showColumnBorders = false;

    public static $showNumberings = true;

    public static $showPollingToggle = false;

    public static $showRefreshToggle = false;

    public static $tableStyle = 'default'; // default, tight

    public static $title = 'id';

    public static $trafficCop = true;

    public static $with = [];

    protected $resource;

    public function __call($method, $parameters)
    {
        return $this->forwardCallTo($this->resource, $method, $parameters);
    }

    public function __get($key)
    {
        return optional($this->resource)->{$key};
    }

    public function __isset($key)
    {
        return isset($this->resource) && isset($this->resource->{$key});
    }

    public function __unset($key)
    {
        if (isset($this->resource)) {
            unset($this->resource->{$key});
        }
    }

    public function actions($request)
    {
        return [];
    }

    // public function beforeDelete($request, $model)
    // {
    //     //
    // }

    // public function beforeInsert($request, $model)
    // {
    //     //
    // }

    // public function beforeUpdate($request, $model)
    // {
    //     //
    // }

    public function cards($request)
    {
        return [];
    }

    public function fields($request)
    {
        return [];
    }

    public function fieldsForCreate($request)
    {
        return $this->fields($request);
    }

    public function fieldsForDetail($request)
    {
        return $this->fields($request);
    }

    public function fieldsForIndex($request)
    {
        return $this->fields($request);
    }

    public function fieldsForUpdate($request)
    {
        return $this->fields($request);
    }

    public function filtersForIndex($request, $indexFields)
    {
        $filters = $this->filters($request);

        $fieldFilters = $indexFields
            ->map->resolveFilterableFilters()
            ->flatten(1)->filter()->all();

        return \array_merge($filters, $fieldFilters);
    }

    public function filters($request)
    {
        return [];
    }

    public function getIdFieldValue()
    {
        $request = request();

        $indexFields = new FieldsCollection($this->fields($request));

        $idField = $indexFields->idField($request);

        $id = $idField->resolveToValue($this->resource);

        if (! isset($id)) {
            if ($this->resource instanceof Model) {
                $id = $this->resource->getKey();
            }
            elseif (\property_exists($this->resource, 'id')) {
                $id = $this->resource->id;
            }
        }

        return isset($id) ? (string) $id : null;
    }

    public function globalSearchLink($request)
    {
        return static::$globalSearchLink;
    }

    public function lenses($request)
    {
        return [];
    }

    public function offsetExists($offset)
    {
        return isset($this->resource[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->resource[$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->resource[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->resource[$offset]);
    }

    public function resource()
    {
        return $this->resource;
    }

    public function subtitle()
    {
        return null;
    }

    public function title()
    {
        $title = optional($this->resource)->{static::$title};

        return isset($title) ? (string) $title : $title;
    }

    public static function afterCreationValidationCallback($request, $validator)
    {
        static::afterValidation($request, $validator);
        static::afterCreationValidation($request, $validator);
    }

    public static function afterUpdateValidationCallback($request, $validator)
    {
        static::afterValidation($request, $validator);
        static::afterUpdateValidation($request, $validator);
    }

    public static function authorizable()
    {
        return Gate::getPolicyFor(static::resourceModel()) !== null;
    }

    public static function findResource($request, $resourceId, $baseQuery, $idField)
    {
        return $baseQuery
            ->where($idField->getAttribute(), $resourceId)
            ->sole();
    }

    public static function fromModel($model)
    {
        $instance = new static;

        if (!($model instanceof Model) && \is_a($modelClass = static::resourceModel(), Model::class, true)) {
            $model = $modelClass::hydrate([$model])[0];
        }

        $instance->resource = $model;

        return $instance;
    }

    public static function globallySearchable()
    {
        return isset(static::$globallySearchable) ? static::$globallySearchable : false;
    }

    public static function group()
    {
        return isset(static::$group) ? static::$group : 'Resource';
    }

    public static function countQuery($request, $unsortedIndexQuery)
    {
        return $unsortedIndexQuery;
    }

    public static function indexQuery($request, $query)
    {
        return $query;
    }

    public static function perPageOptions($related = false)
    {
        return $related
            ? static::$relatedPerPageOptions
            : static::$perPageOptions;
    }

    public static function redirectAfterCreate($request, $resource)
    {
        //
    }

    public static function redirectAfterDelete($request)
    {
        //
    }

    public static function redirectAfterUpdate($request, $resource)
    {
        //
    }

    public static function relatableQuery($request, $query)
    {
        // TODO: incoming args: $request, $query, $field

        return $query;
    }

    public static function relationQuery($request, $query, $viaResourceId)
    {
        return $query;
    }

    public static function resourceModel()
    {
        if (isset(static::$model)) {
            return static::$model;
        }

        return \preg_replace(
            '/\\\\\w+(\\\\\w+)$/',
            '\Models$1',
            static::class
        );
    }

    /*
        $options = [
            'index' => true/false,
            'id' => $resourceId, // index === false

            'single' => ?true,
            'search' => '',
            'filters' => [],
        ]
    */

    public static function resourceQuery($options = null)
    {
        $model = static::resourceModel();

        if (\is_a($model, Model::class, true) || ((new $model) instanceof Model)) {
            return $model::query();
        }

        if ($model instanceof \Illuminate\Contracts\Database\Query\Builder) {
            return $model;
        }
    }

    public static function scoutQuery($request, $query)
    {
        return $query;
    }

    public static function singularLabel()
    {
        return static::label();
    }

    public static function trafficCop($request)
    {
        return static::$trafficCop;
    }

    public static function uriKey()
    {
        return Helpers::classToUriKey(static::class);
    }

    protected static function afterCreationValidation($request, $validator)
    {
        //
    }

    protected static function afterUpdateValidation($request, $validator)
    {
        //
    }

    protected static function afterValidation($request, $validator)
    {
        //
    }

    public static function afterCreate($request, $model)
    {
        //
    }

    public static function afterUpdate($request, $model)
    {
        //
    }

    public static function afterDelete($request, $model)
    {
        //
    }

    public static function afterForceDelete($request, $model)
    {
        //
    }

    public static function beforeCreate($request, $model)
    {
        //
    }

    public static function beforeUpdate($request, $model)
    {
        //
    }

    public static function beforeDelete($request, $model)
    {
        //
    }

    public static function beforeForceDelete($request, $model)
    {
        //
    }
}
