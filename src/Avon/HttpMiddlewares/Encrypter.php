<?php

namespace CR\Library\Avon\HttpMiddlewares;

use CR\Library\Helpers\Encryption;
use CR\Library\Helpers\Helpers as LibHelpers;
use CR\Library\Helpers\Reply;
use Exception;
use Illuminate\Http\Response as IlluminateResponse;

class Encrypter
{
    public function handle($request, $next)
    {
        $key = $request->headers->get('x-encrypt-key');

        if ($key) {
            $request->headers->remove('x-encrypt-key');

            $key = \base64_decode(Encryption::rsaDecryptString($key), true);

            if ($payload = $request->json('cipher')) {
                try {
                    $data = \json_decode(
                        Encryption::aesDecryptString($payload, $key),
                        true
                    );
                }
                catch (Exception $e) {
                    LibHelpers::reportIgnoredException('Intended exception marker', $e);

                    $reply = (new Reply)->message('Invalid key')->withReplaceSelf();

                    return response([
                        'cipher' => Encryption::aesEncryptString(\json_encode($reply), $key),
                    ], 400);
                }

                $request->replace($data);

                $payload = null;
                $data = null;
            }
        }

        $response = $next($request);

        if ($key) {
            if (\in_array($response->getContent(), [null, 'null'], true)) {
                $payload = [
                    'cipher' => null,
                ];
            }
            else {
                $payload = [
                    'cipher' => Encryption::aesEncryptString($response->getContent(), $key),
                ];
            }

            $response->setContent(
                $response instanceof IlluminateResponse
                ? $payload
                : \json_encode($payload)
            );
        }

        return $response;
    }
}
