<?php

namespace CR\Library\Avon\HttpMiddlewares;

use CR\Library\Avon\Helpers;
use Illuminate\Cache\CacheManager;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class SessionLifetime
{
    public function handle($request, $next)
    {
        $lsKeys = Helpers::authUserLsKey();

        $nowTime = now()->format('U');

        $lifetimeSeconds = 60 * config('session.lifetime');

        if ((int) Cache::get($lsKeys['base']) >= $nowTime - $lifetimeSeconds) {
            Cache::put($lsKeys['base'], $nowTime, CacheManager::FOREVER_SECONDS);

            Cache::add($lsKeys['lock'], 0, CacheManager::FOREVER_SECONDS);

            Cache::increment($lsKeys['lock']);

            try {
                return $next($request);
            }
            finally {
                $decremented = (int) Cache::decrement($lsKeys['lock']);

                if ($decremented <= 0) {
                    Cache::put($lsKeys['base'], $nowTime, 60 + $lifetimeSeconds);

                    if ($decremented < 0) {
                        Cache::put($lsKeys['lock'], 0);
                    }
                }
            }
        }

        Helpers::logoutUser($request);

        Log::info('account: logged out - expired');

        abort(401);
    }
}
