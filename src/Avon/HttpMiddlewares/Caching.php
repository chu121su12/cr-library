<?php

namespace CR\Library\Avon\HttpMiddlewares;

use CR\Library\Avon\Application;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\StreamedResponse;

class Caching
{
    public function handle($request, $next)
    {
        $app = app(Application::class);
        $duration = $app->getAssetCacheDuration();

        if (! $duration || ! \is_numeric($duration)) {
            return $next($request);
        }

        $duration = (int) $duration;
        $prefix = $app->getAssetCachePrefix();

        $cacheKey = "_avon:asset:{$prefix}:{$request->path()}";

        $response = Cache::get($cacheKey);

        if (! $response) {
            $response = $next($request);

            if (! ($response instanceof StreamedResponse || isset($response->exception))) {
                Cache::put($cacheKey, $response, $duration);
            }
        }

        return $response;
    }
}
