<?php

namespace CR\Library\Avon\Concerns;

use Closure;

trait MetricVisibility
{
    protected $detailVisibility = false;

    protected $indexVisibility = true;

    public function exceptOnDetail()
    {
        $this->indexVisibility = true;
        $this->detailVisibility = false;

        return $this;
    }

    public function exceptOnIndex()
    {
        $this->indexVisibility = false;
        $this->detailVisibility = true;

        return $this;
    }

    public function onlyOnDetail()
    {
        $this->indexVisibility = false;
        $this->detailVisibility = true;

        return $this;
    }

    public function onlyOnIndex()
    {
        $this->indexVisibility = true;
        $this->detailVisibility = false;

        return $this;
    }

    public function resolveDetailVisibility($request)
    {
        if (($detailVisibility = $this->detailVisibility) instanceof Closure) {
            return (bool) $detailVisibility($request);
        }

        return (bool) $detailVisibility;
    }

    public function resolveIndexVisibility($request)
    {
        if (($indexVisibility = $this->indexVisibility) instanceof Closure) {
            return (bool) $indexVisibility($request);
        }

        return (bool) $indexVisibility;
    }

    public function showOnDetail($callable = null)
    {
        $this->detailVisibility = $callable !== null ? $callable : true;

        return $this;
    }

    public function showOnIndex($callable = null)
    {
        $this->indexVisibility = $callable !== null ? $callable : true;

        return $this;
    }
}
