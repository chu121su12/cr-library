<?php

namespace CR\Library\Avon\Concerns;

use Closure;
use Illuminate\Http\Request;

trait AuthorizedToSee
{
    protected $canSeeCallback = true;

    public function authorizedToSee(Request $request)
    {
        if (($canSeeCallback = $this->canSeeCallback) instanceof Closure) {
            return (bool) $canSeeCallback($request);
        }

        return (bool) $canSeeCallback;
    }

    public function canSee($callback = true)
    {
        $this->canSeeCallback = $callback;

        return $this;
    }

    public function canSeeWhen($gate, $class = null)
    {
        return $this->canSee(static function ($request) use ($gate, $class) {
            return $request->user()->can($gate, $class);
        });
    }
}
