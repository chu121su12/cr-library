<?php

namespace CR\Library\Avon\Concerns;

trait WithMeta
{
    protected $metaCallbacks = [];

    protected $metaData = [];

    public function getMeta()
    {
        return $this->metaData;
    }

    public function withMeta($meta)
    {
        $this->metaData = \array_merge($this->metaData, $meta);

        return $this;
    }

    public function withMetaCallback($meta)
    {
        $this->metaCallbacks = \array_merge($this->metaCallbacks, $meta);

        return $this;
    }

    protected function resolveMeta($request)
    {
        return \array_merge(\array_map(static function ($callback) use ($request) {
            return $callback ? $callback($request) : $callback;
        }, $this->metaCallbacks), $this->metaData);
    }
}
