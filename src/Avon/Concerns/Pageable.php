<?php

namespace CR\Library\Avon\Concerns;

use CR\Library\Avon\Helpers;

trait Pageable
{
    public static function authorizable()
    {
        return true;
    }

    public static function availableForNavigation($request)
    {
        return isset(static::$displayInNavigation) ? static::$displayInNavigation : true;
    }

    public static function badge($request)
    {
        return null;
    }

    public static function description()
    {
        return null;
    }

    public static function fuseKeyword()
    {
        return null;
    }

    public static function group()
    {
        return isset(static::$group) ? static::$group : null;
    }

    public static function label()
    {
        return isset(static::$label) ? static::$label : Helpers::classToProperName(static::class);
    }
}
