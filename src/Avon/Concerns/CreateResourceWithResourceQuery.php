<?php

namespace CR\Library\Avon\Concerns;

trait CreateResourceWithResourceQuery
{
    public static function beforeCreate($request, $model)
    {
        static::resourceQuery()->insert((array) $model);
    }
}
