<?php

namespace CR\Library\Avon\Concerns;

trait FindResourceWithIndexQuery
{
    public static function findResource($request, $resourceId, $baseQuery, $idField)
    {
        $result = static::indexQuery($request, $baseQuery)
            ->where($idField->getAttribute(), $resourceId)
            ->sole();

        if (\method_exists($class = static::class, 'newResourceFromModel')) {
            return $class::newResourceFromModel((array) $result);
        }

        return $result;
    }
}
