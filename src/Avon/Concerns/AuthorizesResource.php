<?php

namespace CR\Library\Avon\Concerns;

use CR\Library\Avon\Resource;
use Exception;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;

trait AuthorizesResource
{
    public function authorizedToViewAny($request)
    {
        if (! static::authorizable()) {
            return true;
        }

        $model = static::resourceModel();

        if (! \method_exists(Gate::getPolicyFor($model), 'viewAny')) {
            return true;
        }

        return Gate::check('viewAny', $model);
    }

    public function authorizedToView($request)
    {
        return $this->authorizedTo($request, 'view');
    }

    public function authorizedToCreate($request)
    {
        if (! static::authorizable()) {
            return true;
        }

        return Gate::check('create', static::resourceModel());
    }

    //

    public function authorizedToReplicate($request)
    {
        return $this->authorizedTo($request, 'replicate');
    }

    public function authorizedToUpdate($request)
    {
        return $this->authorizedTo($request, 'update');
    }

    public function authorizedToDelete($request)
    {
        return $this->authorizedTo($request, 'delete');
    }

    public function authorizedToRestore($request)
    {
        return $this->authorizedTo($request, 'restore');
    }

    public function authorizedToForceDelete($request)
    {
        return $this->authorizedTo($request, 'forcedelete');
    }

    //

    public function authorizedToAdd($request, $model)
    {
        if (! static::authorizable()) {
            return true;
        }

        $resourceModel = $this->resource;
        $method = 'add' . $this->getEloquentModelBaseClass($model);

        if (! \method_exists(Gate::getPolicyFor($resourceModel), $method)) {
            return true;
        }

        return Gate::check($method, $resourceModel);
    }

    //

    public function authorizedToAttachAny($request, $model)
    {
        if (! static::authorizable()) {
            return true;
        }

        $resourceModel = $this->resource;
        $method = 'attachAny' . $this->getEloquentModelBaseClass($model);

        if (! \method_exists(Gate::getPolicyFor($resourceModel), $method)) {
            return true;
        }

        return Gate::check($method, [$resourceModel]);
    }

    public function authorizedToAttach($request, $model)
    {
        if (! $this->authorizedToAttachAny($request, $model)) {
            return false;
        }

        if (! static::authorizable()) {
            return true;
        }

        $resourceModel = $this->resource;
        $method = 'attach' . $this->getEloquentModelBaseClass($model);

        if (! \method_exists(Gate::getPolicyFor($resourceModel), $method)) {
            return true;
        }

        return Gate::check($method, [$resourceModel, $model]);
    }

    public function authorizedToDetach($request, $model)
    {
        if (! static::authorizable()) {
            return true;
        }

        $resourceModel = $this->resource;
        $method = 'detach' . $this->getEloquentModelBaseClass($model);

        if (! \method_exists(Gate::getPolicyFor($resourceModel), $method)) {
            return true;
        }

        return Gate::check($method, [$resourceModel, $model]);
    }

    protected function authorizedTo($request, $gate)
    {
        if (! static::authorizable()) {
            return true;
        }

        return Gate::check($gate, $this->resource);
    }

    private function authorizedToUpdateAttached($request, $model)
    {
        return $this->authorizedToAttach($request, $model);
    }

    private function getEloquentModelBaseClass($model)
    {
        return Str::afterLast($this->getEloquentModelClass($model), '\\');
    }

    private function getEloquentModelClass($model)
    {
        if ($model instanceof Resource) {
            return $model::resourceModel();
        }

        if ($model instanceof EloquentModel) {
            return \get_class($model);
        }

        if (\is_string($model)) {
            return $model;
        }

        throw new Exception(\sprintf('Cannot resolve model to classname. (%s)', \serialize($model)));
    }
}
