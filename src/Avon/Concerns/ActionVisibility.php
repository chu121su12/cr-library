<?php

namespace CR\Library\Avon\Concerns;

use Closure;

trait ActionVisibility
{
    public $detailVisibility = false;

    public $indexVisibility = true;

    public $inlineVisibility = false;

    public function exceptOnDetail()
    {
        $this->indexVisibility = true;
        $this->detailVisibility = false;
        $this->inlineVisibility = true;

        return $this;
    }

    public function exceptOnIndex()
    {
        $this->indexVisibility = false;
        $this->detailVisibility = true;
        $this->inlineVisibility = true;

        return $this;
    }

    public function exceptInline()
    {
        $this->indexVisibility = true;
        $this->detailVisibility = true;
        $this->inlineVisibility = false;

        return $this;
    }

    public function onlyOnDetail()
    {
        $this->indexVisibility = false;
        $this->detailVisibility = true;
        $this->inlineVisibility = false;

        return $this;
    }

    public function onlyOnIndex()
    {
        $this->indexVisibility = true;
        $this->detailVisibility = false;
        $this->inlineVisibility = false;

        return $this;
    }

    public function onlyInline()
    {
        $this->indexVisibility = false;
        $this->detailVisibility = false;
        $this->inlineVisibility = true;

        return $this;
    }

    public function resolveDetailVisibility($request)
    {
        if (($detailVisibility = $this->detailVisibility) instanceof Closure) {
            return (bool) $detailVisibility($request);
        }

        return (bool) $detailVisibility;
    }

    public function resolveBatchIndexVisibility($request, $preventIndexSelection)
    {
        $visibility = false;

        if (($indexVisibility = $this->indexVisibility) instanceof Closure) {
            $visibility = (bool) $indexVisibility($request);
        }
        else {
            $visibility = (bool) $indexVisibility;
        }

        if (! $preventIndexSelection || ! $visibility) {
            return $visibility;
        }

        return \in_array($this->standalone, ['all', 'matching'], true);
    }

    public function resolveIndexVisibility($request)
    {
        if (($indexVisibility = $this->indexVisibility) instanceof Closure) {
            return (bool) $indexVisibility($request);
        }

        return (bool) $indexVisibility;
    }

    public function resolveInlineVisibility($request)
    {
        if (($inlineVisibility = $this->inlineVisibility) instanceof Closure) {
            return (bool) $inlineVisibility($request);
        }

        return (bool) $inlineVisibility;
    }

    public function showOnDetail($callable = null)
    {
        $this->detailVisibility = $callable !== null ? $callable : true;

        return $this;
    }

    public function showOnIndex($callable = null)
    {
        $this->indexVisibility = $callable !== null ? $callable : true;

        return $this;
    }

    public function showInline($callable = null)
    {
        $this->inlineVisibility = $callable !== null ? $callable : true;

        return $this;
    }
}
