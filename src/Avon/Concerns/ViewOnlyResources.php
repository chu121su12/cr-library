<?php

namespace CR\Library\Avon\Concerns;

trait ViewOnlyResources
{
    public function authorizedToCreate($request)
    {
        return false;
    }

    public function authorizedToReplicate($request)
    {
        return false;
    }

    public function authorizedToUpdate($request)
    {
        return false;
    }

    public function authorizedToDelete($request)
    {
        return false;
    }

    public function authorizedToRestore($request)
    {
        return false;
    }

    public function authorizedToForceDelete($request)
    {
        return false;
    }

    public function authorizedToAdd($request, $model)
    {
        return false;
    }

    public function authorizedToAttachAny($request, $model)
    {
        return false;
    }

    public function authorizedToAttach($request, $model)
    {
        return false;
    }

    public function authorizedToDetach($request, $model)
    {
        return false;
    }
}
