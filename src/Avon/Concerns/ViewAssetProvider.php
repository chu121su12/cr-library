<?php

namespace CR\Library\Avon\Concerns;

use CR\Library\Helpers\Helpers;

class ViewAssetProvider
{
    private $arguments;

    private $type;

    public function __construct($type, ...$arguments)
    {
        $this->type = $type;

        $this->arguments = $arguments;
    }

    public function __invoke()
    {
        switch ($this->type) {
            case 'icons': return $this->serveIcons();
            default: abort(400);
        }
    }

    public static function icons($collection)
    {
        return new self('icons', $collection);
    }

    private function serveIcons()
    {
        $collection = Helpers::ensureNoDirectoryClimb($this->arguments[0]);

        return \file_get_contents(base_path("vendor/cr/icons/icons/{$collection}.json"));
    }
}
