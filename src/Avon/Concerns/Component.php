<?php

namespace CR\Library\Avon\Concerns;

use CR\Library\Avon\Helpers;
use Illuminate\Support\Str;

trait Component
{
    public function name()
    {
        return isset($this->name)
            ? value($this->name)
            : Helpers::classToProperName(static::class);
    }

    protected static $uriKeyAlias;

    public static function aliasUriKey($key)
    {
        static::$uriKeyAlias = $key;
    }

    public static function uriKey()
    {
        if (isset(static::$uriKeyAlias) && static::$uriKeyAlias) {
            return static::$uriKeyAlias;
        }

        return Helpers::classToUriKey(static::class);
    }

    protected function isBuiltInComponent()
    {
        return \mb_strpos(static::class, 'CR\Library\Avon\\') === 0;
    }

    protected function resolveComponent()
    {
        return isset($this->component)
            ? $this->component
            : Helpers::classToUriKey(Str::afterLast(static::class, '\\'));
    }
}
