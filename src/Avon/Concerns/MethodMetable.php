<?php

namespace CR\Library\Avon\Concerns;

trait MethodMetable
{
    use WithMeta;

    public function __call($method, $params)
    {
        $params = \count($params) ? $params : [true];

        return $this->withMeta([$method => $params[0]]);
    }
}
