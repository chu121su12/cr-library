<?php

namespace CR\Library\Avon\Concerns;

use Closure;
use Illuminate\Http\Request;

trait AuthorizedToRun
{
    protected $canRunCallback = true;

    public function authorizedToRun(Request $request, $model)
    {
        if (($canRunCallback = $this->canRunCallback) instanceof Closure) {
            return (bool) $canRunCallback($request, $model);
        }

        return (bool) $canRunCallback;
    }

    public function canRun($callback = true)
    {
        $this->canRunCallback = $callback;

        return $this;
    }
}
