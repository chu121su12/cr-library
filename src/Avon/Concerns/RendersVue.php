<?php

namespace CR\Library\Avon\Concerns;

trait RendersVue
{
    public static function renderView()
    {
        $iife = <<<'JS'
(callback, template, ...args) => {
    const [vueInstance, { componentName }, { cssExtract }] = args;

    vueInstance.component(componentName, Vue.defineAsyncComponent(async () => {
        const promise = Promise.all([
            cssExtract(template),
            cssExtract(callback.toString()),
        ]);

        const component = await callback(...args);
        component.template = template;

        await promise;
        return component;
    }));
}
JS;

        return \sprintf(
            '//
            (%s)(
                (() => {
                    try { return %s; } catch (e) { __WARN(e); return {}; }
                })(),
                (() => {
                    try { return `%s`; } catch (e) { __WARN(e); return ""; }
                })(),
                ...arguments,
            );',
            $iife,
            static::renderVueComponentFunction(),
            static::renderVueTemplateString()
        );
    }
}
