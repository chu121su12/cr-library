<?php

namespace CR\Library\Avon\Concerns;

use CR\Library\Avon\Helpers;

trait BaseUserable
{
    public function getAccountId()
    {
        return $this->getAuthIdentifier();
    }

    public function getAccountName()
    {
        return $this->name;
    }

    public function getAccountTeam()
    {
        return 'No team';
    }

    public function getAccountType()
    {
        return 'Default type';
    }

    public function getAuthPasswordName()
    {
        return 'password';
    }

    public function shouldResetPassword()
    {
        return false;
    }

    public function shouldVerifyAccount()
    {
        return false;
    }

    public function shouldConfirmPassword()
    {
        return false;
    }

    public static function authenticationCallback()
    {
        return Helpers::getDefaultAuthCallback(
            app(static::class)
        );
    }

    public static function resetPasswordCallback()
    {
        return Helpers::getDefaultResetPasswordCallback(
            app(static::class)
        );
    }
}
