<?php

namespace CR\Library\Avon\Concerns;

trait RendersAssets
{
    public static function renderView()
    {
        $iife = <<<'JS'
(callback, template, ...args) => {
    const [vueInstance, { componentName }, { cssExtract }] = args;

    vueInstance.component(componentName, Vue.defineAsyncComponent(async () => {
        const promise = Promise.all([
            cssExtract(template),
            cssExtract(callback.toString()),
        ]);

        const component = await callback(...args);
        component.template = template;

        await promise;
        return component;
    }));
}
JS;

        $callback = <<<'JS'
                    (vueInstance, context, options) => ({
                        setup() {
                            const componentName: context.componentName;

                            console.log('setup', {
                                options,
                                vueInstance,

                                componentName,
                                toolBaseUrl: context.toolBaseUrl,

                                // global
                                Vue,
                                VueUse,
                            });

                            return {
                                componentName,
                            }''
                    })
JS;

        $template = <<<'HTML'
            < {{ componentName }} />
HTML;

        return \sprintf(
            '(%s)(%s, `%s`, ...arguments);',
            $iife,
            $callback,
            $template
        );
    }
}
