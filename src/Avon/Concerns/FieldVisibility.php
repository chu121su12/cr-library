<?php

namespace CR\Library\Avon\Concerns;

use Closure;

trait FieldVisibility
{
    protected $creationVisibility = true;

    protected $detailVisibility = true;

    protected $indexVisibility = true;

    protected $previewVisibility = false;

    protected $updatingVisibility = true;

    public function exceptOnForms()
    {
        $this->indexVisibility = true;
        $this->detailVisibility = true;
        $this->creationVisibility = false;
        $this->updatingVisibility = false;
        // $this->previewVisibility = false;

        return $this;
    }

    public function hideFromDetail($callable = null)
    {
        $this->detailVisibility = static::makeVisibilityCallback($callable, $hide = true);

        return $this;
    }

    public function hideFromIndex($callable = null)
    {
        $this->indexVisibility = static::makeVisibilityCallback($callable, $hide = true);

        return $this;
    }

    public function hideWhenCreating($callable = null)
    {
        $this->creationVisibility = static::makeVisibilityCallback($callable, $hide = true);

        return $this;
    }

    public function hideWhenUpdating($callable = null)
    {
        $this->updatingVisibility = static::makeVisibilityCallback($callable, $hide = true);

        return $this;
    }

    public function onlyOnDetail($callable = null)
    {
        $this->indexVisibility = false;
        $this->creationVisibility = false;
        $this->updatingVisibility = false;
        // $this->previewVisibility = false;

        return $this->showOnDetail($callable);
    }

    public function onlyOnForms()
    {
        $this->indexVisibility = false;
        $this->detailVisibility = false;
        $this->creationVisibility = true;
        $this->updatingVisibility = true;
        // $this->previewVisibility = false;

        return $this;
    }

    public function onlyOnIndex($callable = null)
    {
        $this->detailVisibility = false;
        $this->creationVisibility = false;
        $this->updatingVisibility = false;
        // $this->previewVisibility = false;

        return $this->showOnIndex($callable);
    }

    public function onlyOnPreview($callable = null)
    {
        $this->detailVisibility = false;
        $this->creationVisibility = false;
        $this->updatingVisibility = false;
        $this->indexVisibility = false;

        return $this->showOnPreview($callable);
    }

    public function onlyWhenCreating($callable = null)
    {
        $this->indexVisibility = false;
        $this->detailVisibility = false;
        $this->updatingVisibility = false;
        // $this->previewVisibility = false;

        return $this->showOnCreating($callable);
    }

    public function onlyWhenUpdating($callable = null)
    {
        $this->indexVisibility = false;
        $this->detailVisibility = false;
        $this->creationVisibility = false;
        // $this->previewVisibility = false;

        return $this->showOnUpdating($callable);
    }

    public function resolveCreationVisibility($request)
    {
        if (($creationVisibility = $this->creationVisibility) instanceof Closure) {
            return (bool) $creationVisibility($request);
        }

        return (bool) $creationVisibility;
    }

    public function resolveDetailVisibility($request)
    {
        if (($detailVisibility = $this->detailVisibility) instanceof Closure) {
            return (bool) $detailVisibility($request);
        }

        return (bool) $detailVisibility;
    }

    public function resolveIndexVisibility($request)
    {
        if (($indexVisibility = $this->indexVisibility) instanceof Closure) {
            return (bool) $indexVisibility($request);
        }

        return (bool) $indexVisibility;
    }

    public function resolvePreviewVisibility($request)
    {
        if (($previewVisibility = $this->previewVisibility) instanceof Closure) {
            return (bool) $previewVisibility($request);
        }

        return (bool) $previewVisibility;
    }

    public function resolveUpdatingVisibility($request)
    {
        if (($updatingVisibility = $this->updatingVisibility) instanceof Closure) {
            return (bool) $updatingVisibility($request);
        }

        return (bool) $updatingVisibility;
    }

    public function showOnCreating($callable = null)
    {
        $this->creationVisibility = static::makeVisibilityCallback($callable, $hide = false);

        return $this;
    }

    public function showOnDetail($callable = null)
    {
        $this->detailVisibility = static::makeVisibilityCallback($callable, $hide = false);

        return $this;
    }

    public function showOnIndex($callable = null)
    {
        $this->indexVisibility = static::makeVisibilityCallback($callable, $hide = false);

        return $this;
    }

    public function showOnPreview($callable = null)
    {
        $this->previewVisibility = static::makeVisibilityCallback($callable, $hide = false);

        return $this;
    }

    public function showOnUpdating($callable = null)
    {
        $this->updatingVisibility = static::makeVisibilityCallback($callable, $hide = false);

        return $this;
    }

    protected static function makeVisibilityCallback($callable, $hide)
    {
        if (\is_bool($callable)) {
            return $hide ? ! $callable : $callable;
        }

        if ($callable instanceof Closure) {
            return static function ($request) use ($callable, $hide) {
                $callableResult = $callable($request);

                return $hide ? ! $callableResult : $callableResult;
            };
        }

        return ! $hide;
    }
}
