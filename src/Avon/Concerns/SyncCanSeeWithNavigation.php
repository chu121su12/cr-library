<?php

namespace CR\Library\Avon\Concerns;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;

trait SyncCanSeeWithNavigation
{
    public function authorizedToSee(Request $request)
    {
        return (bool) static::availableForNavigation($request);
    }

    public function authorizedToViewAny($request)
    {
        return (bool) static::availableForNavigation($request);
    }

    public static function availableForNavigation($request)
    {
        if (! isset(static::$displayInNavigation)) {
            return true;
        }

        if (\is_bool(static::$displayInNavigation)) {
            return static::$displayInNavigation;
        }

        return Gate::any(Arr::wrap(static::$displayInNavigation), [$request, static::class]);
    }
}
