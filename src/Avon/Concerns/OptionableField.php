<?php

namespace CR\Library\Avon\Concerns;

use Illuminate\Support\Collection;

trait OptionableField
{
    protected $optionResolver;

    protected $options;

    protected $optionsCache;

    public function getOptions()
    {
        if (! $this->optionsCache) {
            $options = Collection::wrap(value($this->options, $this));

            return $this->optionResolver
                ? $options->map($this->optionResolver)->values()
                : $options->values();
        }

        return $this->optionsCache;
    }

    public function option($callable)
    {
        $this->optionResolver = $callable;

        return $this;
    }

    public function options($options)
    {
        $this->options = $options;

        return $this;
    }

    protected function resolve($resource, $attribute = null)
    {
        return tap(parent::resolve($resource, $attribute), function () {
            $this->getOptions();
        });
    }
}
