<?php

namespace CR\Library\Avon\Concerns;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;

trait SyncActionAuthorizations
{
    public function authorizedToRun(Request $request, $model)
    {
        if (! isset(static::$authorizations)) {
            return parent::authorizedToRun($request, $model);
        }

        return (bool) $this->authorizedToSee($request);
    }

    public function authorizedToSee(Request $request)
    {
        if (! isset(static::$authorizations)) {
            return parent::authorizedToSee($request);
        }

        return (bool) Gate::any(Arr::wrap(static::$authorizations), $request);
    }
}
