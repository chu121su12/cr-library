(function(document) {
    function isContextable(event) {
        return ['A', 'INPUT', 'TEXTAREA'].indexOf(event.target.tagName) !== -1;
    }

    document.oncontextmenu = function(event) {
        if (!isContextable(event)) {
            return false;
        }
    };

    document.onkeydown = function(event) {
        var key = event.code;
        var ctrl = event.ctrlKey;
        var shift = event.shiftKey;

        var fn = function(str) {
            return key === 'F' + str;
        };

        var char = function(str) {
            return key === 'Key' + str;
        };

        if (
            (!ctrl && !shift && fn('12')) ||
            (ctrl && !shift && (char('P') || char('S') || char('U'))) ||
            (!ctrl && shift && (fn('4') || fn('5') || fn('7') || fn('8') || fn('9') || fn('12'))) ||
            (ctrl && shift && (char('C') || char('E') || char('I') || char('J') || char('K') || char('M') || char('S')))
        ) {
            return false;
        }
    };
})(window.document);
