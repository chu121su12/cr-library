(()=>__avon._cacheStyle=["antialiased","text-right","hidden","block","float-right","text-center","inline-block","flex","items-center","justify-center","stroke-join-round","stroke-cap-round","resize","transform","select-none","font-italic","filter","table","inline-flex","appearance-none","outline-none","flex-col","pointer-events-none","pointer-events-auto","flex-row","justify-end","cursor-pointer","flex-none","cursor-default","invisible","items-start","items-end","table-cell","table-row","table-header-group","table-row-group","items-stretch","cursor-auto","justify-between","inline","items-baseline","italic","justify-start","flex-wrap","break-words","border-separate","text-left","text-justify","float-left","cursor-not-allowed","transition-none","visible","contents","border-collapse","outline","list-item","[width:100%]","[border-top-width:1px]","[white-space:normal]","font-sans","leading-none","min-w-screen-md","min-w-[160px]","transition","property-all","leading-tight","fixed","inset-0","z-50","relative","top-4","left-4","rounded-lg","shadow-lg","max-w-3/5","whitespace-pre-wrap","p-4","border","px-4","py-2","mb-0","shadow","absolute","h1","h2","h3","h4","rounded-6","m-2","p-3","ml-4","bottom-0","left-0","z-100","b","m-4","z-1","h-[32px]","w-auto","min-h-screen","pt-[12vh]","max-w-127","mx-auto","p-8","shadow-md","mb-2","w-full","scale-200","stroke-green-700","transition-all-300","bg-gray-200/50","pt-[13vh]","pt-8","px-16","min-w-auto","h-10","w-3/4","px","bg-gray-500/100","font-mono","tabular-nums","slashed-zero","w-4","ml-0.5","px-3","h-full","px-2","py-1","m-1","z-60","blur","transition-opacity","opacity-0","backdrop-blur","opacity-60","property-opacity,transform","scale-90","rounded","overflow-hidden","min-w-[72vw]","max-h-[72vh]","mb-4","backdrop-blur-8","rounded-md","w-5","h-5","rounded-full","rounded-3xl","pl-8","inset-y-0","ml-2","min-w-[32rem]","rounded-xl","shadow-xl","p-2","flex-grow","min-h-[1rem]","max-h-[40vh]","opacity-1","py-3","w-8","tracking-[0.1ch]","right-0","p-1","mr-2","w-px","h-px","min-h-40","border-dashed","px-1","overflow-x-auto","h-12","w-12","p-0","w-6","whitespace-normal","sticky","top-0","whitespace-nowrap","min-w-full","tracking-wider","max-w-full","h-24","rounded-none","min-w-[4.5ch]","leading-[1.125]","mx-6","py-4","mx-4","w-1/4","shrink-0","pr-2","pb-0","pb-2","h-8","leading-snug","m--1","min-w-8","h-[1em]","mr-1","ml-1","max-w-[50vw]","shadow-sm","align-middle","overflow-auto","tracking-tight","tracking-wide","my--2","px-6","z-0","h-64","mx--2","my-1","leading-normal","p-px","w-1/3","rounded-tl-md","rounded-tr-md","rounded-0","h-4","rounded-b-md","indent","max-h-[70vh]","whitespace-pre","my-4","max-h-[60vh]","my-6","mt-8","mb-6","leading-inherit","min-h-[40vh]","mx-1","pr-10","h-60","h-40","z-20","opacity-25","my-2","pl-2","opacity-50","w-3","h-[3rem]","px-0","pr-1","mb-1","min-w-48","mt-2","bg-white/90","bg-white/99","right-3","top-9","w-22","h22","top-11","h-14","mb-3","origin-center","rounded-l-none","rotate-90","pt-2","rounded-r-none","pt-16","pb-12","gap-2","rotate--90","mx-8","px-12","py-8","transition-300","rounded-l-[0.5em]","rounded-r-[0.5em]","py-[0.5em]","px-[0.75em]","leading-[1.5em]","p-[1em]","h-7","w-7","whitespace-pre-line","overflow-y-auto","w-[30rem]","pt-10","pb-6","mt-3","mr-3","scale-98","h-screen","overflow-y-hidden","overflow-y-scroll","z-30","ml-64","ml-0","h-0","mt-12","mt-6","min-h-0","w-64","z-40","min-h-auto","static","left-auto","top-auto","h-auto","border-none","pt-6","px-8","max-h-12","mx-14","mt--2","pb-8","mt-4","underline","tab","h6","flex-shrink","text-gray-800","bg-white","bg-gray-50","bg-transparent","font-medium","border-gray-300","text-black","bg-gray-200","border-gray-400","text-gray-400","stroke-2","stroke-offset-[18px]","stroke-dash-[18px]","bg-black","border-gray-200","font-semibold","bg-gray-100","text-white","bg-blue-500","bg-blue-200","font-normal","text-gray-600","bg-blue-50","border-blue-600","text-red-600","text-blue-600","text-green-600","text-green-400","text-red-400","text-gray-500","text-gray-900","text-gray-700","bg-red-500","border-red-600","bg-red-50","bg-gray-600","bg-gray-500","font-light","border-white","color-gray-400","bg-blue-600","text-green-700","text-red-700","bg-red-600","font-bold","border-transparent","border-gray-700","bg-green-200","bg-red-200","bg-gray-300","border-gray-500","border-3","space-y-6","text-2xl","text-xs","space-y-4","text-base","text-lg","space-y-2","space-y-0","space-x-4","divide-gray-200","space-x-2","border-4","space-y-1","border-2","space-x-1","border-t","border-b","text-sm","text-xl","border-l","border-r","border-0","text-4xl","divide-gray-300","divide-gray-400","space-y-8","border-y","text-size-[1em]","space-y-[1em]","divide-y","divide-x","border-spacing-0","animate-spin","animate-duration-500","border-l-0","-mx-1","-my-2","-mb-px","-my-4","-mx-6","-mx-2","-m-2","!hidden","!justify-start","important-z-100","important-py-0","important-pb-1","!ml-4","print:hidden","!stroke-offset-[0]","!border-r","lg:grid","lg:h-120","lg:h-80","lg:grid-cols-6","lg:grid-flow-row-dense","all-transition-none","md:w-[80vw]","all:select-none","uno-layer-important-important-bg-white","all-[.CodeMirror-lines]:py-3","all:stroke-blue-500","children-[.CodeMirror]:transition","children-[.CodeMirror]:h-auto","children-[.CodeMirror]:rounded-md","children-[.CodeMirror]:border","children-[.CodeMirror-focused]:ring","children-[.CodeMirror-focused]:ring-blue-300","children:py-4","children:px-4","children:py-1","children:px-3","children:py-2","children-[.active]:stroke-blue-500","all:stroke-4","children-stroke-3","children-[.CodeMirror]:border-gray-400","children-[.CodeMirror-focused]:ring-size-[0.25rem]","children-[.CodeMirror-focused]:border-blue-300","before:block","before:content-empty","focus:ring-inset","hover:bg-black/5","focus:ring","focus:ring-blue-300","before:pt-[56.25%]","focus:ring-red-600","focus:relative","focus:z-1","hover:opacity-100","focus:outline-0","focus:ring-size-[0.25rem]","focus:border-blue-300","focus:bg-blue-600","hover:bg-blue-700","hover:text-gray-900","hover:bg-inherit","hover:text-blue-700","focus:bg-red-600","hover:bg-red-700","hover:bg-gray-300","focus:ring-size-[0.125rem]","hover:bg-gray-900","hover:bg-gray-50","hover:bg-gray-200","hover:bg-red-600","hover:bg-blue-600","hover:bg-blue-300","hover:bg-gray-600","hover:text-white","focus:ring-size-[0.25em]","hover:text-blue-600","hover:border-blue-600","group-[.dark-hour]:bg-gray-700","parent-hover:bg-gray-50","parent-hover:border-gray-200","parent-hover:border-t-gray-200","dark:bg-gray-800","dark:bg-gray-600","dark:border-gray-900","hover:children-[.active]:stroke-gray-800","hover:children-[.hover]:stroke-blue-700","hover:children-[.hover.clear]:stroke-red-700","[*>&.ct-line]:[stroke-width:.125rem]","[*>&.ct-point]:[stroke-width:.375rem]","[*_&.masonry]:[grid-template-rows:masonry]","active:not-focus:bg-black/10","dark:hover:bg-gray-800","[antialiased=\"\"]","[text-right=\"\"]","[pointer-events-none=\"\"]","[flex=\"\"]","[items-center=\"\"]","[hidden=\"\"]","[table=\"\"]","[content~=\"$__avon[\"]","[content~=\"$__avon_app->themeColor\"]","[font-sans=\"\"]","[leading-tight=\"\"]","[fixed=\"\"]","[inset-0=\"\"]","[relative=\"\"]","[top-4=\"\"]","[left-4=\"\"]","[rounded-lg=\"\"]","[shadow-lg=\"\"]","[max-w-3=\"\"]","[whitespace-pre-wrap=\"\"]","[px-4=\"\"]","[py-2=\"\"]","[mb-0=\"\"]","[shadow=\"\"]","[h1=\"\"]","[h2=\"\"]","[h3=\"\"]","[h4=\"\"]","[scale~=\"4\"]","[bg-transparent~=\"([a,\"]","[bg-transparent~=\"=>\"]","[bg-transparent~=\"{\"]","[bg-transparent~=\"=\"]","[bg-transparent~=\"${animationName}{0%{${a}}100%{${b}}}\"]","[bg-transparent~=\"keyframes.value\"]","[bg-transparent~=\"${k}@keyframes\"]","[bg-transparent~=\"${k}\"]","[bg-transparent~=\"}\"]","[bg-transparent~=\")\"]","[bg-transparent~=\"useStyleTag(keyframes)\"]","[bg-transparent~=\"onBeforeUnmount(styleUnload)\"]","[bg-transparent~=\"computed(()\"]","[bg-transparent~=\"parseFloat(scale.value).toFixed(precision.value)\"]","[scale~=\"2\"]","[transition=\"\"]","[rows~=\"1\"]","[indent=\"\"]","[flex-grow=\"\"]","[w-full=\"\"]","[h-60=\"\"]","[h-40=\"\"]","[scale~=\"1.5\"]","[px=\"\"]","[stroke~=\"gray\"]","[stroke~=\"white\"]","[b=\"\"]","[text-gray-800=\"\"]","[bg-transparent=\"\"]","[bg-white=\"\"]","[bg-gray-500=\"\"]","[bg-transparent~=\"const\"]","[bg-transparent~=\"k\"]","[bg-transparent~=\"styleUnload\"]","[bg-transparent~=\"return\"]","[bg-transparent~=\"size\"]","[font-normal=\"\"]","[font-semibold=\"\"]","[bg-blue-600=\"\"]","[text-white~=\"queryString.page\"]","[text-white~=\"&&\"]","[text-white~=\"1,\"]","[bg-red-600=\"\"]","[text-white~=\"queryFilterSize,\"]","[stroke-width~=\"1.5\"]","[border-color~=\"transparent\"]","[stroke-width~=\"2\"]","[uno-layer-base~=\"antialiased\"]","[uno-layer-default~=\"float-right\"]","[uno-layer-default~=\"text-center\"]","[uno-layer-default~=\"block\"]","[uno-layer-default~=\"inline-block\"]","[uno-layer-default~=\"flex\"]","[uno-layer-default~=\"items-center\"]","[uno-layer-default~=\"justify-center\"]","[uno-layer-default~=\"select-none\"]","[uno-layer-base~=\"inline-flex\"]","[uno-layer-base~=\"items-center\"]","[uno-layer-base~=\"justify-center\"]","[uno-layer-base~=\"appearance-none\"]","[uno-layer-base~=\"outline-none\"]","[uno-layer-default~=\"pointer-events-auto\"]","[uno-layer-default~=\"outline-none\"]","[uno-layer-default~=\"hidden\"]","[uno-layer-default~=\"flex-col\"]","[uno-layer-default~=\"cursor-pointer\"]","[uno-layer-default~=\"flex-none\"]","[uno-layer-default~=\"cursor-default\"]","[uno-layer-default~=\"invisible\"]","[uno-layer-default~=\"items-start\"]","[uno-layer-default~=\"items-end\"]","[uno-layer-base~=\"table-cell\"]","[uno-layer-base~=\"table-row\"]","[uno-layer-default~=\"table\"]","[uno-layer-default~=\"table-header-group\"]","[uno-layer-default~=\"table-row-group\"]","[uno-layer-default~=\"items-stretch\"]","[uno-layer-default~=\"items-baseline\"]","[uno-layer-default~=\"inline-flex\"]","[uno-layer-default~=\"italic\"]","[uno-layer-default~=\"flex-wrap\"]","[uno-layer-default~=\"border-separate\"]","[uno-layer-default~=\"justify-between\"]","[uno-layer-default~=\"justify-end\"]","[uno-layer-default~=\"cursor-auto\"]","[uno-layer-default~=\"float-left\"]","[uno-layer-default~=\"break-words\"]","[uno-layer-default~=\"transition-none\"]","[uno-layer-default~=\"text-right\"]","[uno-layer-base~=\"font-sans\"]","[uno-layer-base~=\"leading-none\"]","[uno-layer-base~=\"min-w-screen-md\"]","[uno-layer-base~=\"transition\"]","[uno-layer-base~=\"property-all\"]","[uno-layer-default~=\"relative\"]","[uno-layer-default~=\"m-4\"]","[uno-layer-default~=\"z-1\"]","[uno-layer-default~=\"h-[32px]\"]","[uno-layer-default~=\"w-auto\"]","[uno-layer-default~=\"min-h-screen\"]","[uno-layer-default~=\"pt-[12vh]\"]","[uno-layer-default~=\"px-4\"]","[uno-layer-default~=\"max-w-127\"]","[uno-layer-default~=\"mx-auto\"]","[uno-layer-default~=\"p-8\"]","[uno-layer-default~=\"shadow-md\"]","[uno-layer-default~=\"rounded-lg\"]","[uno-layer-default~=\"space-y-6\"]","[uno-layer-default~=\"mb-2\"]","[uno-layer-default~=\"absolute\"]","[uno-layer-default~=\"inset-0\"]","[uno-layer-default~=\"bg-gray-200/50\"]","[uno-layer-default~=\"pt-[13vh]\"]","[uno-layer-default~=\"pt-8\"]","[uno-layer-default~=\"px-16\"]","[uno-layer-default~=\"w-4\"]","[uno-layer-default~=\"ml-0.5\"]","[uno-layer-base~=\"rounded-lg\"]","[uno-layer-base~=\"py-2\"]","[uno-layer-base~=\"px-3\"]","[uno-layer-default~=\"transition\"]","[uno-layer-default~=\"backdrop-blur\"]","[uno-layer-default~=\"opacity-60\"]","[uno-layer-default~=\"border\"]","[uno-layer-default~=\"rounded\"]","[uno-layer-default~=\"shadow\"]","[uno-layer-default~=\"overflow-hidden\"]","[uno-layer-default~=\"min-w-[72vw]\"]","[uno-layer-default~=\"max-h-[72vh]\"]","[uno-layer-default~=\"mb-4\"]","[uno-layer-default~=\"space-y-2\"]","[uno-layer-default~=\"p-4\"]","[uno-layer-default~=\"backdrop-blur-8\"]","[uno-layer-base~=\"rounded-md\"]","[uno-layer-base~=\"w-5\"]","[uno-layer-base~=\"h-5\"]","[uno-layer-base~=\"border\"]","[uno-layer-default~=\"inset-y-0\"]","[uno-layer-default~=\"left-0\"]","[uno-layer-default~=\"ml-2\"]","[uno-layer-base~=\"min-w-[32rem]\"]","[uno-layer-default~=\"rounded-xl\"]","[uno-layer-default~=\"shadow-xl\"]","[uno-layer-default~=\"divide-gray-200\"]","[uno-layer-default~=\"p-2\"]","[uno-layer-default~=\"space-x-2\"]","[uno-layer-default~=\"flex-grow\"]","[uno-layer-default~=\"py-3\"]","[uno-layer-default~=\"w-8\"]","[uno-layer-default~=\"right-0\"]","[uno-layer-default~=\"p-1\"]","[uno-layer-default~=\"mr-2\"]","[uno-layer-base~=\"w-full\"]","[uno-layer-default~=\"rounded-md\"]","[uno-layer-base~=\"leading-tight\"]","[uno-layer-default~=\"opacity-0\"]","[uno-layer-default~=\"w-px\"]","[uno-layer-default~=\"h-px\"]","[uno-layer-default~=\"w-full\"]","[uno-layer-default~=\"space-y-1\"]","[uno-layer-default~=\"px-1\"]","[uno-layer-default~=\"overflow-x-auto\"]","[uno-layer-default~=\"h-full\"]","[uno-layer-default~=\"h-12\"]","[uno-layer-default~=\"space-x-1\"]","[uno-layer-base~=\"sticky\"]","[uno-layer-base~=\"top-0\"]","[uno-layer-base~=\"whitespace-nowrap\"]","[uno-layer-default~=\"tracking-wider\"]","[uno-layer-default~=\"max-w-full\"]","[uno-layer-default~=\"h-10\"]","[uno-layer-default~=\"whitespace-nowrap\"]","[uno-layer-default~=\"h-24\"]","[uno-layer-default~=\"mx-6\"]","[uno-layer-default~=\"py-4\"]","[uno-layer-default~=\"w-1/4\"]","[uno-layer-default~=\"shrink-0\"]","[uno-layer-default~=\"pr-2\"]","[uno-layer-default~=\"pb-0\"]","[uno-layer-default~=\"rounded-full\"]","[uno-layer-default~=\"h-8\"]","[uno-layer-default~=\"py-1\"]","[uno-layer-default~=\"px-2\"]","[uno-layer-default~=\"min-w-8\"]","[uno-layer-default~=\"align-middle\"]","[uno-layer-default~=\"overflow-auto\"]","[uno-layer-default~=\"font-mono\"]","[uno-layer-default~=\"px-6\"]","[uno-layer-default~=\"z-0\"]","[uno-layer-default~=\"h-64\"]","[uno-layer-default~=\"top-0\"]","[uno-layer-default~=\"bottom-0\"]","[uno-layer-default~=\"my-1\"]","[uno-layer-base~=\"p-px\"]","[uno-layer-base~=\"p-0\"]","[uno-layer-base~=\"px-4\"]","[uno-layer-default~=\"py-2\"]","[uno-layer-default~=\"space-y-4\"]","[uno-layer-default~=\"max-h-[70vh]\"]","[uno-layer-default~=\"px-3\"]","[uno-layer-default~=\"mx-4\"]","[uno-layer-default~=\"my-4\"]","[uno-layer-default~=\"max-h-[60vh]\"]","[uno-layer-default~=\"my-6\"]","[uno-layer-default~=\"leading-snug\"]","[uno-layer-default~=\"mt-8\"]","[uno-layer-default~=\"mb-6\"]","[uno-layer-default~=\"shadow-lg\"]","[uno-layer-default~=\"pr-10\"]","[uno-layer-default~=\"z-20\"]","[uno-layer-default~=\"opacity-25\"]","[uno-layer-default~=\"leading-none\"]","[uno-layer-default~=\"my-2\"]","[uno-layer-default~=\"leading-tight\"]","[uno-layer-default~=\"pl-2\"]","[uno-layer-default~=\"h-5\"]","[uno-layer-default~=\"w-5\"]","[uno-layer-default~=\"opacity-50\"]","[uno-layer-default~=\"p-3\"]","[uno-layer-default~=\"tabular-nums\"]","[uno-layer-extra~=\"px-0\"]","[uno-layer-default~=\"min-w-48\"]","[uno-layer-default~=\"mt-2\"]","[uno-layer-default~=\"p-px\"]","[uno-layer-default~=\"bg-white/90\"]","[uno-layer-default~=\"bg-white/99\"]","[uno-layer-default~=\"right-3\"]","[uno-layer-default~=\"top-9\"]","[uno-layer-default~=\"w-22\"]","[uno-layer-default~=\"h22\"]","[uno-layer-default~=\"top-11\"]","[uno-layer-default~=\"mb-3\"]","[uno-layer-default~=\"divide-gray-300\"]","[uno-layer-default~=\"ml-4\"]","[uno-layer-default~=\"pt-2\"]","[uno-layer-default~=\"space-x-4\"]","[uno-layer-default~=\"divide-gray-400\"]","[uno-layer-default~=\"pt-16\"]","[uno-layer-default~=\"pb-12\"]","[uno-layer-default~=\"min-h-[40vh]\"]","[uno-layer-default~=\"gap-2\"]","[uno-layer-default~=\"space-y-8\"]","[uno-layer-default~=\"px-12\"]","[uno-layer-default~=\"py-8\"]","[uno-layer-default~=\"p-[1em]\"]","[uno-layer-default~=\"space-y-[1em]\"]","[uno-layer-default~=\"h-7\"]","[uno-layer-default~=\"w-7\"]","[uno-layer-default~=\"whitespace-pre-line\"]","[uno-layer-default~=\"shadow-sm\"]","[uno-layer-default~=\"overflow-y-auto\"]","[uno-layer-default~=\"w-[30rem]\"]","[uno-layer-default~=\"pt-10\"]","[uno-layer-default~=\"pb-6\"]","[uno-layer-default~=\"mt-3\"]","[uno-layer-default~=\"mr-3\"]","[uno-layer-default~=\"sticky\"]","[uno-layer-default~=\"z-30\"]","[uno-layer-default~=\"ml-64\"]","[uno-layer-default~=\"h-0\"]","[uno-layer-default~=\"mt-12\"]","[uno-layer-default~=\"min-h-0\"]","[uno-layer-default~=\"fixed\"]","[uno-layer-default~=\"w-64\"]","[uno-layer-default~=\"h-screen\"]","[uno-layer-default~=\"z-40\"]","[uno-layer-default~=\"pt-6\"]","[uno-layer-default~=\"px-8\"]","[uno-layer-default~=\"max-h-12\"]","[uno-layer-default~=\"mx-14\"]","[uno-layer-default~=\"pb-8\"]","[uno-layer-base~=\"text-gray-800\"]","[uno-layer-base~=\"bg-white\"]","[uno-layer-base~=\"bg-gray-50\"]","[uno-layer-default~=\"bg-white\"]","[uno-layer-default~=\"font-medium\"]","[uno-layer-default~=\"text-gray-400\"]","[uno-layer-base~=\"border-gray-400\"]","[uno-layer-default~=\"bg-black\"]","[uno-layer-default~=\"border-gray-200\"]","[uno-layer-default~=\"font-semibold\"]","[uno-layer-default~=\"bg-gray-100\"]","[uno-layer-base~=\"divide-y\"]","[uno-layer-default~=\"divide-y\"]","[uno-layer-default~=\"text-gray-600\"]","[uno-layer-default~=\"border-gray-400\"]","[uno-layer-base~=\"children-[.CodeMirror-focused]:ring-size-[0.25rem]\"]","[uno-layer-base~=\"border-gray-200\"]","[uno-layer-base~=\"font-medium\"]","[uno-layer-base~=\"bg-gray-100\"]","[uno-layer-base~=\"divide-x\"]","[uno-layer-default~=\"text-gray-500\"]","[uno-layer-default~=\"bg-gray-200\"]","[uno-layer-default~=\"border-gray-300\"]","[uno-layer-default~=\"bg-gray-500\"]","[uno-layer-default~=\"text-white\"]","[uno-layer-default~=\"font-light\"]","[uno-layer-default~=\"text-gray-700\"]","[uno-layer-default~=\"border-white\"]","[uno-layer-default~=\"font-bold\"]","[uno-layer-default~=\"border-transparent\"]","[uno-layer-default~=\"bg-gray-600\"]","[uno-layer-default~=\"border-gray-700\"]","[uno-layer-default~=\"bg-gray-50\"]","[uno-layer-default~=\"bg-gray-300\"]","[uno-layer-default~=\"text-2xl\"]","[uno-layer-default~=\"text-xs\"]","[uno-layer-base~=\"text-base\"]","[uno-layer-default~=\"text-lg\"]","[uno-layer-default~=\"border-2\"]","[uno-layer-base~=\"border-t\"]","[uno-layer-base~=\"border-b\"]","[uno-layer-default~=\"text-sm\"]","[uno-layer-default~=\"text-xl\"]","[uno-layer-extra~=\"border-b\"]","[uno-layer-base~=\"border-l\"]","[uno-layer-base~=\"text-sm\"]","[uno-layer-default~=\"border-t\"]","[uno-layer-default~=\"text-4xl\"]","[uno-layer-default~=\"border-b\"]","[uno-layer-default~=\"border-y\"]","[uno-layer-default~=\"border-r\"]","[uno-layer-default~=\"border-spacing-0\"]","[uno-layer-extra~=\"border-l-0\"]","[text-white~=\"!=\"]","[uno-layer-default~=\"-mx-1\"]","[uno-layer-default~=\"-mb-px\"]","[uno-layer-default~=\"-my-4\"]","[uno-layer-default~=\"-mx-6\"]","[uno-layer-default~=\"-mx-2\"]","[uno-layer-default~=\"-m-2\"]","[sm~=\"flex-row\"]","[sm~=\"justify-end\"]","[sm~=\"space-y-0\"]","[sm~=\"space-x-4\"]","mix-shade-10:active:ring-blue-300","mix-shade-10:active:ring-red-300","mix-shade-20:active:ring-red-300","[uno-layer-default~=\"lg:grid\"]","[uno-layer-default~=\"lg:grid-cols-6\"]","[uno-layer-default~=\"lg:grid-flow-row-dense\"]","[uno-layer-default~=\"md:w-[80vw]\"]","[uno-layer-base~=\"all-[.CodeMirror-lines]:py-3\"]","[uno-layer-base~=\"children-[.CodeMirror]:transition\"]","[uno-layer-base~=\"children-[.CodeMirror]:h-auto\"]","[uno-layer-base~=\"children-[.CodeMirror]:rounded-md\"]","[uno-layer-base~=\"children-[.CodeMirror]:border\"]","[uno-layer-base~=\"children-[.CodeMirror-focused]:ring\"]","[uno-layer-base~=\"children-[.CodeMirror-focused]:ring-blue-300\"]","[uno-layer-base~=\"children-[.CodeMirror]:border-gray-400\"]","[uno-layer-base~=\"children-[.CodeMirror-focused]:border-blue-300\"]","[uno-layer-default~=\"before:block\"]","[uno-layer-default~=\"before:content-empty\"]","[uno-layer-base~=\"hover:bg-black/5\"]","[uno-layer-base~=\"focus:ring\"]","[uno-layer-base~=\"focus:ring-blue-300\"]","[uno-layer-default~=\"before:pt-[56.25%]\"]","[uno-layer-default~=\"focus:ring\"]","[uno-layer-default~=\"focus:ring-blue-300\"]","[uno-layer-default~=\"hover:opacity-100\"]","[uno-layer-base~=\"focus:ring-size-[0.25rem]\"]","[uno-layer-base~=\"focus:border-blue-300\"]","[uno-layer-default~=\"hover:text-gray-900\"]","[uno-layer-default~=\"focus:ring-size-[0.25rem]\"]","[uno-layer-default~=\"focus:border-blue-300\"]","[uno-layer-default~=\"hover:bg-gray-300\"]","[uno-layer-default~=\"hover:bg-gray-50\"]","[uno-layer-default~=\"hover:bg-gray-200\"]","[uno-layer-base~=\"parent-hover:bg-gray-50\"]","[uno-layer-base~=\"parent-hover:border-gray-200\"]","[uno-layer-base~=\"parent-hover:border-t-gray-200\"]","[lg:h-80~=\"default:\"]","[uno-layer-default~=\"dark:border-gray-900\"]","mix-shade-20:active:focus:ring-blue-300","[uno-layer-base~=\"active:not-focus:bg-black/10\"]","[uno-layer-base~=\"mix-shade-10:active:ring-blue-300\"]","container","[uno-layer-base~=\"mix-shade-20:active:focus:ring-blue-300\"]","[container=\"\"]"])();

((window, runtime) => {
    window.__avon_runtime = runtime();

    const {
        login,
        application,
        resolvePublicKey,

        unocssConfig,
        unocssRuntimeConfig,
    } = window.__avon_runtime;
    window.__avon_runtime.resolvePublicKey = null;

    const createVueApp = (promises) => {
        const app = application();

        const create = App => Vue.createApp({
            components: { App },
            setup: () => ({ promises }),
            template: `<App :promises="promises" />`,
        });

        if (window.__avon._component === 'login') {
            const {
                appComponent,
            } = login(app.options);

            return create(appComponent);
        }

        const {
            appComponent,
            bootstrap,
            options,
        } = app;

        const vueApp = create(appComponent);

        vueApp.use(primevue.config.default);

        vueApp.component('Hide', {
            template: `<template v-if="false"><slot /></template>`,
        });

        window.__avon_runtime.app = vueApp;
        window.__avon_runtime.configured = options;
        bootstrap(vueApp);
        return vueApp;
    };

    const domPromise = new Promise((ready) => (window.addEventListener(
        'DOMContentLoaded',
        ready,
        { once: true }
    )));

    const domReady = (callback) => new Promise((resolve) => domPromise.then(() => callback(resolve)));

    const promises = [
        domReady((resolve) => resolve(createVueApp(promises).mount('#app'))),

        domReady((resolve) => resolve(resolvePublicKey())),

        new Promise((resolve) => (window.__unocss = {
            ...unocssConfig(),
            runtime: {
                ...unocssRuntimeConfig(),
                ready({ uno, extract, extractAll }) {
                    (window.__avon._cacheStyle ?? []).forEach(token => uno.blocked.add(token));

                    setTimeout(() => {
                        Promise.all([
                            // domReady((resolve) => extractAll().then(resolve)),
                            extract(JSON.stringify(window.__avon)),
                            extract(window.__avon.loginBg),
                            // extract(runtime.toString()),
                        ]).then(resolve);
                    }, 1);

                    return false;
                },
            },
        })),
    ];
})(window, () => {
    const __avon = { ...(JSON.parse(JSON.stringify(window.__avon)) || {}) };

    const isString = (obj) => typeof obj === 'string';
    const isArray = (obj) => Array.isArray(obj);

    const __DEBUG__ = !! __avon.debug;
    const __DEBUG = (...obj) => {
        let [name] = obj || [];
        console.groupCollapsed(`${(new Date).toISOString()}${isString(name) ? ` - ${name}` : ''}`);
        console.log(...obj);
        console.trace();
        console.groupEnd();
    };
    const __LOG = (...obj) => __DEBUG__ && __DEBUG(...obj);
    const __WARN = (...obj) => __DEBUG__ && console.warn(...obj);

    const DEFAULT_FILLED_CASES = [undefined, null, '', [], {}];
    const EN_DASH = '–';
    const EM_DASH = '—';
    const PASSWORD_BULLET = '•';
    const ZWJ = '‍';
    const NBSP = ' ';

    function noop() { __LOG('noop', arguments); };

    const debounce = (() => {
        function runSource(source, that, args) {
            try {
                source && source.apply && source.apply(that, args);
            }
            catch (_error) {
            }
        }
    
        function runInspect(source, inspect, mode, that, args) {
            try {
                return inspect && inspect.apply && inspect.apply(that, [
                    mode,
                    () => runSource(source, that, args),
                ]) === false;
            }
            catch (_error) {
                return false;
            }
        }
    
        return (options, source) => {
            options = parseFloat(options) == options ? { delay: options, trailing: true } : (options || {});
            const inspect = options.inspect;
            const leading = !!options.leading;
            const trailing = !!options.trailing;
            const delay = options.delay;
            source = source || options.source || (() => {});
    
            let timeoutID = null;
            let didLeading = false;
    
            function reset() {
                clearTimeout(timeoutID);
                timeoutID = null;
            }
    
            function bounced() {
                const args = arguments;
                const that = this;
                reset();
    
                if (runInspect(source, inspect, 1, that, args)) {
                    return;
                }
    
                if (!didLeading) {
                    didLeading = true;
                    if (runInspect(source, inspect, 2, that, args)) {
                        return;
                    }
                    if (leading) {
                        runSource(source, that, args);
                    }
                }
    
                timeoutID = setTimeout(() => {
                    if (!runInspect(source, inspect, 3, that, args)) {
                        if (trailing) {
                            runSource(source, that, args);
                        }
                    }
                    didLeading = false;
                }, delay);
            }
    
            bounced.reset = reset;
            bounced.original = source;
            bounced.bounced = bounced;
    
            return bounced;
        };
    })();

    const jsonParse = (data) => {
        try {
            if (data != null) {
                return JSON.parse(data);
            }
        } catch (error) {
            __WARN({ data, error });
        }

        return undefined;
    };

    const randomStringDefaultPool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    const randomString = (length, pool) => {
        length = length || 16;
        pool = pool || randomStringDefaultPool;
        const charactersLength = pool.length;
        let result = '';

        for (let i = 0; i < length; ++i) {
            result += pool.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
    };

    const formatBytes = (bytes, decimals) => {
        if (bytes === 0) return '0 Bytes';
        if (bytes == 1) return '1 Byte';
        decimals = decimals || 2;

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    };

    // https://stackoverflow.com/a/18071824
    const smoothScroll = (target) => {
        const normalize = (target) => (target === document || target === window ? __html() : target);
        target = normalize(target);
        let scrollContainer = target;
        do {
            scrollContainer = scrollContainer.parentNode;
            if (!scrollContainer) {
                return Promise.reject();
            }
            scrollContainer.scrollTop += 1;
        } while (scrollContainer.scrollTop === 0);

        scrollContainer = normalize(scrollContainer);
        let targetY = 0;
        do {
            if (target === scrollContainer) {
                break;
            }
            targetY += target.offsetTop;
        } while ((target = target.offsetParent));

        const easeOut = (val) => val;
        const easeOutPx = (step, total, progress) =>
            (1 - easeOut(progress / total)) * (4 * step) + (step > 0 ? 0.1 : -0.1);

        let top = scrollContainer.scrollTop || 0;
        const upward = targetY < top;
        const totalScroll = upward ? top : targetY;
        const pixelsPerStep = Math.max(16, totalScroll / 30) * (upward ? -1 : 1);
        let iterations = 0;
        let breakScroll = false;
        const breakEvents = ['keydown', 'wheel', 'mousedown', 'touchstart'];
        function breakScrollFn() {
            breakScroll = true;
            [window, target]
                .filter(Boolean)
                .forEach((t) => breakEvents.forEach((e) => t.removeEventListener(e, breakScrollFn, { once: true, passive: true })));
        }
        [window, target]
            .filter(Boolean)
            .forEach((t) => breakEvents.forEach((e) => t.addEventListener(e, breakScrollFn, { once: true, passive: true })));

        return new Promise((resolve, reject) => {
            (function scroll() {
                top = scrollContainer.scrollTop = (upward ? Math.max : Math.min)(
                    targetY,
                    top + easeOutPx(pixelsPerStep, totalScroll, upward ? totalScroll - top : top)
                );

                ++iterations;

                if (
                    breakScroll ||
                    iterations > 300 ||
                    top === targetY ||
                    (upward && top < targetY) ||
                    (!upward && top > targetY)
                ) {
                    breakScrollFn();
                    return (breakScroll || iterations > 300 ? reject : resolve)();
                }

                requestAnimationFrame(scroll);
            })();
        });
    };

    const checkFile = (file, loadPreview) => new Promise((resolve) => {
        const emptyImageDataUrl = '';

        if (!file) {
            return resolve({
                dataUrl: emptyImageDataUrl,
                error: true,
                file,
                height: 0,
                size: 0,
                width: 0,

                maybeFile: false,
                maybeFolder: false,
            });
        }

        loadPreview = loadPreview ?? true;

        Promise.all([
            new Promise((resolve) => {
                const reader = new FileReader();
                reader.onloadend = () => {
                    resolve(reader.error);
                };
                reader.readAsBinaryString(file);
            }),

            new Promise((resolve) => {
                if (!loadPreview) {
                    return resolve(emptyImageDataUrl);
                }

                const reader = new FileReader();
                reader.onload = () => {
                    resolve(reader.result);
                };
                reader.readAsDataURL(file);
            }),

            new Promise((resolve) => {
                if (!loadPreview) {
                    return resolve({
                        height: 0,
                        width: 0,
                    });
                }

                const URL = window.URL || window.webkitURL;
                const image = new Image();
                image.onload = (e) => {
                    const { height, width } = image;
                    resolve({
                        height,
                        width,
                    });
                    URL.revokeObjectURL(image.src);
                };
                image.onerror = (e) => {
                    resolve({
                        height: 0,
                        width: 0,
                    });
                    URL.revokeObjectURL(image.src);
                };
                image.src = URL.createObjectURL(file);
            }),
        ]).then(([readerError, dataUrl, { width, height }]) => {
            const maybeFile = !(readerError && readerError.name === 'NotFoundError');

            resolve({
                dataUrl: height > 0 && width > 0 ? dataUrl : emptyImageDataUrl,
                error: maybeFile ? false : (readerError || true),
                file,
                height,
                size: formatBytes(file.size),
                width,

                maybeFile,
                maybeFolder: file.size === 0
                    && file.type === ''
                    && !file.name.match(/\..+$/),
            });
        });
    });

    const __html = () => document.documentElement;
    const __body = () => document.body;

    const basicAlert = (text) => new Promise((resolve) => {
        const html = __html();
        const body = __body();
        if (! (html && body)) {
            window.alert(text);
            resolve(undefined);
            return;
        }

        let container = document.createElement('div');
        container.classList.add(...'font-sans leading-tight antialiased text-gray-800 fixed inset-0 bg-transparent z-50'.split(' '));

        const alertDiv = document.createElement('div');
        alertDiv.classList.add(...'relative top-4 left-4 bg-white rounded-lg shadow-lg max-w-3/5'.split(' '));

        const textElement = document.createElement('div');
        textElement.classList.add(...'whitespace-pre-wrap p-4'.split(' '));
        textElement.innerHTML = text;

        const divider = document.createElement('hr');
        divider.classList.add(...'border'.split(' '));

        const buttonContainer = document.createElement('div');
        buttonContainer.classList.add(...'text-right p-4'.split(' '));

        const closeButton = document.createElement('button');
        closeButton.classList.add(...'px-4 py-2 mb-0 rounded-lg shadow font-medium'.split(' '));
        closeButton.innerHTML = 'OK';
        closeButton.setAttribute('type', 'button');
        closeButton.addEventListener('click', () => {
            body.removeAttribute('inert');
            container.parentElement.removeChild(container);
            container = null;
            resolve(true);
        }, { once: true });

        alertDiv.appendChild(textElement);
        alertDiv.appendChild(divider);
        alertDiv.appendChild(buttonContainer);
        buttonContainer.appendChild(closeButton);
        container.appendChild(alertDiv);
        html.appendChild(container);
        body.setAttribute('inert', '');
        setTimeout(() => {
            closeButton.focus();
        }, 1);
    });

    const getDomDocumentSizes = () => {
        const htmlEl = __html();
        const bodyEl = __body();
        const max = Math.max;
        getComputedStyle(bodyEl);

        return {
            scrollTop: window.pageYOffset || htmlEl.scrollTop,
            scrollLeft: window.pageXOffset || htmlEl.scrollLeft,
            documentHeight: max(
                bodyEl.scrollHeight,
                bodyEl.offsetHeight,
                htmlEl.clientHeight,
                htmlEl.scrollHeight,
                htmlEl.offsetHeight
            ),
            documentWidth: max(
                bodyEl.scrollWidth,
                bodyEl.offsetWidth,
                htmlEl.clientWidth,
                htmlEl.scrollWidth,
                htmlEl.offsetWidth
            ),
            viewportHeight: max(htmlEl.clientHeight || 0, window.innerHeight || 0),
            viewportWidth: max(htmlEl.clientWidth || 0, window.innerWidth || 0),
        };
    };

    const getDomElementSizes = (element) => {
        const { height: originalHeight, width: originalWidth } = getComputedStyle(element);

        const {
            width,
            position,
            visibility,
            height,
        } = element.style;

        Object.assign(element.style.width, {
            width: 'unset',
            position: 'absolute',
            visibility: 'hidden',
            height: 'unset',
        });

        const { height: resetHeight, width: resetWidth } = getComputedStyle(element);

        Object.assign(element.style.width, {
            width,
            position,
            visibility,
            height,
        });

        getComputedStyle(element);

        return {
            height: resetHeight,
            width: resetWidth,
            originalHeight,
            originalWidth,
        };
    };

    const pagerUrlWindow = (lastPage, currentPage, onEachSide, onSideEnd) => {
        currentPage = currentPage || 1;
        onEachSide = onEachSide || 3;
        onSideEnd = onSideEnd || 2;

        const windowing = (() => {
            const getUrlRange = (start, end) => {
                const range = [];
                while (start <= end) {
                    range.push(start++);
                }
                return range;
            };

            const getSmallSlider = () => ({
                first: getUrlRange(1, lastPage),
                slider: [],
                last: [],
            });

            if (lastPage < onEachSide * 2 + onSideEnd * 2 + 2) {
                return getSmallSlider();
            }

            const getStart = () => getUrlRange(1, onSideEnd);

            const getFinish = () => getUrlRange(lastPage + 1 - onSideEnd, lastPage);

            const getFullSlider = (onEachSide) => ({
                first: getStart(),
                slider: getUrlRange(currentPage - onEachSide, currentPage + onEachSide),
                last: getFinish(),
            });

            const getUrlSlider = (onEachSide) => {
                const window = onEachSide + 4;

                if (!(lastPage > 1)) {
                    return {
                        first: [],
                        slider: [],
                        last: [],
                    };
                }

                if (currentPage <= window) {
                    return {
                        first: getUrlRange(1, window + onEachSide),
                        slider: [],
                        last: getFinish(),
                    };
                }

                if (currentPage > lastPage - window) {
                    return {
                        first: getStart(),
                        slider: [],
                        last: getUrlRange(lastPage - (window + (onEachSide - 1)), lastPage),
                    };
                }

                return getFullSlider(onEachSide);
            };

            return getUrlSlider(onEachSide);
        })();

        return [
            ...windowing.first,
            ...(windowing.slider.length ? ['...'] : []),
            ...windowing.slider,
            ...(windowing.last.length ? ['...'] : []),
            ...windowing.last,
        ].filter((e) => e != null);
    };

    const instantLink = (options, callback) => {
        const link = document.createElement('a');
        Object.entries(options).map(([key, value]) => {
            link[key] = value;
        });

        const body = __body();
        body.appendChild(link);
        setTimeout(() => {
            callback ? callback(link) : link.click();
            setTimeout(() => body.removeChild(link), 1);
        }, 1);
    };

    const locale = (() => {
        const NUMBER_DIGIT_LOOKUP = [
            '', 'satu', 'dua', 'tiga',
                'empat', 'lima', 'enam',
                'tujuh', 'delapan', 'sembilan',
                'sepuluh', 'sebelas',
        ];

        const NUMBER_OF_THOUSANDS_LOOKUP = [
            'ribu',
            'juta',
            'miliar',
            'triliun',
            'kuardriliun',
            'kuintiliun',
            'sektiliun',
            'septiliun',
            'oktiliun',
            'noniliun',
            'desiliun',
            'undesiliun',
            'duodesiliun',
            'tredesiliun',
            'kuattodesiliun',
            'kuindesiliun',
            'sekdesiliun',
            'septemdesiliun',
            'oktodesiliun',
            'novemdesiliun',
            'vigintiliun',
            'unvigintiliun',
            'duovigintiliun',
            'trevigintiliun',
            'kuattuovigintriliun',
            'kuinvigintiliun',
            'sekvigintiliun',
            'septenvigintriliun',
            'oktovigintriliun',
            'novemvigintriliun',
            'trigintiliun',
        ];

        const makeBigJs = (number, dp, rm) => {
            const s = Big.strict;
            try {
                Big.strict = true;
                const n = Big(`${number}`);
                const r = Big(`${number}`).round(dp === undefined ? 20 : dp, rm === undefined ? Big.roundHalfUp : rm);
                if (!n.eq(r)) {
                    throw new Error('Invalid rounding');
                }
                return n;
            } catch (e) {
                __WARN({number});
                throw e;
            } finally {
                Big.strict = s;
            }
        };

        const bigJsDivideRoundDown = (number, divisor) => {
            const original = Big.RM;
            const result = number.div(divisor);
            Big.RM = original;
            return result;
        };

        const spellInteger = (number) => {
            if (number.lt(12)) {
                return ' ' + NUMBER_DIGIT_LOOKUP[parseInt(number.toString(), 10)];
            }

            if (number.lt(20)) {
                return spellInteger(number.minus(10)) + ' belas';
            }

            if (number.lt(100)) {
                return spellInteger(bigJsDivideRoundDown(number, 10)) + ' puluh' + spellInteger(number.mod(10));
            }

            if (number.lt(200)) {
                return ' seratus' + spellInteger(number.minus(100));
            }

            if (number.lt(1000)) {
                return spellInteger(bigJsDivideRoundDown(number, 100)) + ' ratus' + spellInteger(number.mod(100));
            }

            if (number.lt(2000)) {
                return ' seribu' + spellInteger(number.minus(1000));
            }

            let ofThousands = makeBigJs(1000);

            for (const thousandName of NUMBER_OF_THOUSANDS_LOOKUP) {
                const nextThousands = ofThousands.times(1000);

                if (number.lt(nextThousands)) {
                    const left = spellInteger(bigJsDivideRoundDown(number, ofThousands));
                    const right = spellInteger(number.mod(ofThousands));
                    return `${left} ${thousandName}${right}`;
                }

                ofThousands = nextThousands;
            }

            throw new Error(`The number ${number.toString()} cannot be formatted.`);
        };

        const decimalRe = /0+$/;
        const spellDecimal = (decimals, keepZeroDecimals) => {
            let ds = decimals.join('');
            if (! keepZeroDecimals) {
                ds = ds.replace(decimalRe, '');
            }

            if (ds === '') {
                return '';
            }

            const decimalSpells = decimals.map((digit) => {
                return digit ? NUMBER_DIGIT_LOOKUP[digit] : 'nol';
            });

            return ' koma ' + decimalSpells.join(' ');
        };

        const spacesRe = /\s\s+/;
        const spellNumber = (number, keepZeroDecimals) => {
            if (number.eq(0)) {
                return 'nol';
            }

            if (number.lt(0)) {
                return 'negatif ' + spellNumber(number.abs(), keepZeroDecimals);
            }

            const integral = number.round(Big.roundDown);
            const left = integral.eq(0) ? 'nol' : spellInteger(integral).trim().replace(spacesRe, ' ');
            const right = spellDecimal(number.minus(integral).c, keepZeroDecimals);

            return `${left}${right}`;
        };

        const terbilang = (angka, keepZeroDecimals) => {
            if (angka == null || angka === '' || Number.isNaN(angka)) {
                return '';
            }

            if (isString(angka) && Number.isNaN(parseFloat(angka))) {
                return '';
            }

            if (angka === 0 || angka === '-0') {
                angka = '0';
            }

            return spellNumber(new Big(angka), keepZeroDecimals ?? false);
        };

        return {
            spellNumber: terbilang,
            bigJs: makeBigJs,
            toLocalDateTimeString: (date) => date.toString().split(/ GMT\b/)[0],
        };
    })();

    const css = (() => {
        const replace1Re = /([,+~>]|\|\|)\s+/;
        const replace2Re = /\s+([,+~>]|\|\|)/;
        const replace3Re = /\s\s+/;
        const normalizeSelector = (selector) => selector
            .replace(replace1Re, '$1')
            .replace(replace2Re, '$1')
            .replace(replace3Re, ' ')
            .trim().toLowerCase();

        const search = (selector) => {
            const matches = [];
            const selectorIsFunction = typeof selector === 'function';
            const normalizedSelector = selectorIsFunction ? null : normalizeSelector(selector);
            const matcher = selectorIsFunction ? selector : (s, n) => normalizedSelector === n(s);

            [...document.styleSheets].forEach((stylesheet) => {
                try {
                    [...(stylesheet.cssRules || stylesheet.rules)].forEach((rule) => {
                        const selectorText = rule.selectorText;
                        if (selectorText) {
                            const possibleSelectors = selectorText.indexOf(',') === -1 ?
                                [] :
                                selectorText.split(',');

                            possibleSelectors.unshift(selectorText);

                            for (let possibleSelector of possibleSelectors) {
                                if (matcher(possibleSelector, normalizeSelector)) {
                                    matches.push({
                                        stylesheet: stylesheet,
                                        selectorText: selectorText,
                                        selector: possibleSelector,
                                        rule: rule,
                                    });

                                    break;
                                }
                            }
                        }
                    });
                } catch (e) {}
            });

            return matches;
        };

        const cascade = (rules) => {
            if (!(rules.length && rules.length > 0)) {
                return {};
            }

            const first = rules[0];
            if (first.rule && first.stylesheet && first.selector && first.selectorText) {
                return cascade(rules.map(({ rule }) => rule));
            }

            const cascaded = {};

            rules.forEach(({ style }) => {
                Object.entries(style).forEach(([_, property]) => {
                    cascaded[property.trim()] = style[property].trim();
                });
            });

            return cascaded;
        };

        return {
            cascade,
            search,
            searchStyle: (x) => cascade(search(x) || []),
        };
    })();

    const [scrypt, acrypt] = (() => {
        const aesComponentSize = 3;

        const searchDash = /-/g;
        const searchUnderscore = /_/g;
        const b64UrlStringToBase64String = (string) => string.replace(searchDash, '+').replace(searchUnderscore, '/');

        const searchEq = /=+$/g;
        const searchPlus = /\+/g;
        const searchForwardSlash = /\//g;
        const base64StringToB64UrlStringNoPad = (string) => string.replace(searchEq, '').replace(searchPlus, '-').replace(searchForwardSlash, '_');

        const searchNonKey1 = /-.*/g;
        const searchNonKey2 = /\s+/g;
        const splitter = (key) => {
            const keyLength = key.replace(searchNonKey1, '').replace(searchNonKey2, '').length;

            let size = 384;

            switch (true) {
                case keyLength <= 128: size = 512; break;
                case keyLength <= 220: size = 1024; break;
                case keyLength <= 392: size = 2048; break;
                case keyLength <= 564: size = 3072; break;
                default: /* case keyLength <= 736 */ size = 4096; break;
            }

            return new RegExp('.{1,' + (size / 8 - 1) + '}', 'g');
        };

        const scryptFactory = () => {
            const cryptojs = CryptoJS;
            const base64 = cryptojs.enc.Base64;
            const randomBytes = (size) => cryptojs.lib.WordArray.random(size);
            const hmac = (value, appKey) => cryptojs.HmacSHA256(value, appKey);
            const hash = (iv, value, appKey) => hmac(randomBytes(0).concat(iv).concat(value), appKey);
            const aesIv = () => randomBytes(16);

            return {
                randomBytes(size) {
                    const bytes = randomBytes(size);

                    return [bytes, bytes.toString(base64)];
                },

                decryptString(cipher, appKey) {
                    const payload = cipher.split('.', aesComponentSize + 1);

                    if (! (Array.isArray(payload) && payload.length === aesComponentSize)) {
                        throw new Error('The payload is invalid.');
                    }

                    const checkIvBytes = aesIv();

                    const [payloadIv, payloadMac, payloadData] = payload.map(b64UrlStringToBase64String);

                    const decodedIv = base64.parse(payloadIv);

                    const hmacInput = hmac(base64.parse(payloadMac), checkIvBytes);

                    const hmacPayload = hmac(
                        hash(decodedIv, base64.parse(payloadData), appKey),
                        checkIvBytes
                    );

                    if (hmacInput.toString() !== hmacPayload.toString()) {
                        throw new Error('The MAC is invalid.');
                    }

                    const decrypted = cryptojs.AES
                        .decrypt(payloadData, appKey, { iv: decodedIv })
                        .toString(cryptojs.enc.Utf8);

                    if (decrypted === false) {
                        throw new Error('Could not decrypt the data.');
                    }

                    return decrypted;
                },

                encryptString(message, appKey) {
                    const rawIv = aesIv();

                    const cipher = cryptojs.AES.encrypt(message, appKey, { iv: rawIv });

                    if (cipher === false) {
                        throw new Error('Could not encrypt the data.');
                    }

                    const b64Cipher = cipher.toString();

                    return [
                        base64StringToB64UrlStringNoPad(rawIv.toString(base64)),
                        base64StringToB64UrlStringNoPad(hash(rawIv, base64.parse(b64Cipher), appKey).toString(base64)),
                        base64StringToB64UrlStringNoPad(b64Cipher),
                    ].join('.');
                },
            };
        };

        const acryptFactory = () => {
            const cryptojs = CryptoJS;
            const Jsencrypt = JSEncrypt;
            const makeJsEncrypt = (options) => new Jsencrypt(options);

            return {
                encryptString(message, publicKey) {
                    const crypt = makeJsEncrypt();
                    crypt.setPublicKey(publicKey);

                    const b64Payload = cryptojs.enc.Utf8.parse(message)
                        .toString(cryptojs.enc.Base64);

                    return base64StringToB64UrlStringNoPad(b64Payload)
                        .match(splitter(publicKey))
                        .map((part) => base64StringToB64UrlStringNoPad(crypt.encrypt(part)))
                        .join('.');
                },

                verifyString(signature, message, publicKey) {
                    const crypt = makeJsEncrypt();
                    crypt.setPublicKey(publicKey);

                    return crypt.verify(
                        message,
                        b64UrlStringToBase64String(signature),
                        cryptojs.SHA256
                    );
                },
            };
        };

        let scryptCache = null;
        let acryptCache = null;

        return [
            () => {
                if (!scryptCache) {
                    scryptCache = scryptFactory();
                }

                return scryptCache;
            },
            () => {
                if (!acryptCache) {
                    acryptCache = acryptFactory();
                }

                return acryptCache;
            },
        ];
    })();

    const fp2 = async () => ({
        thumbmark: [await ThumbmarkJS.getFingerprint(), await ThumbmarkJS.getFingerprintData()],
        fingerprint2: await new Promise((resolve) => new Fingerprint2().get((result, components) => resolve([
            result,
            components.map((value) => {
                if (value.key === 'canvas') {
                    value.value = (value.value || '').replace(/,.*/, '');
                }
                else if (value.key === 'webgl') {
                    value.value = (value.value || '').replace(/.*~extensions:/, '');
                }
                return value;
            }),
        ]))),
        fp2b: await new Promise((resolve) => new Fingerprint2({
            excludeAdBlock: true,
            excludeAudioFP: true,
            excludeCanvas: true,
            excludeDoNotTrack: true,
            excludeHasLiedBrowser: true,
            excludeHasLiedLanguages: true,
            excludeHasLiedOs: true,
            excludeHasLiedResolution: true,
            excludeIEPlugins: true,
            excludeIndexedDB: true,
            excludeJsFonts: true,
            excludeOpenDatabase: true,
            excludePlugins: true,
            excludeSessionStorage: true,
            excludeUserAgent: true,
            excludeWebGL: true,
        }).get((result) => resolve([result, {}]))),
    });

    const actionProcessor = (() => {
        function phpBase64Decode(encodedData) {
            if (typeof window !== 'undefined') {
                if (typeof window.atob !== 'undefined') {
                    // return decodeUTF8string(window.atob(encodedData));
                    var byteString = window.atob(encodedData);
                    var ia = new Uint8Array(new ArrayBuffer(byteString.length));
                    for (var i = 0; i < byteString.length; ++i) {
                        ia[i] = byteString.charCodeAt(i);
                    }
                    return ia;
                }
            } else {
                return new Buffer(encodedData, 'base64').toString('utf-8');
            }

            var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
            var o1;
            var o2;
            var o3;
            var h1;
            var h2;
            var h3;
            var h4;
            var bits;
            var i = 0;
            var ac = 0;
            var dec = '';
            var tmpArr = [];

            if (!encodedData) {
                return encodedData;
            }

            encodedData += '';

            do {
                // unpack four hexets into three octets using index points in b64
                h1 = b64.indexOf(encodedData.charAt(i++));
                h2 = b64.indexOf(encodedData.charAt(i++));
                h3 = b64.indexOf(encodedData.charAt(i++));
                h4 = b64.indexOf(encodedData.charAt(i++));
                bits = (h1 << 18) | (h2 << 12) | (h3 << 6) | h4;
                o1 = (bits >> 16) & 0xff;
                o2 = (bits >> 8) & 0xff;
                o3 = bits & 0xff;
                if (h3 === 64) {
                    tmpArr[ac++] = String.fromCharCode(o1);
                } else if (h4 === 64) {
                    tmpArr[ac++] = String.fromCharCode(o1, o2);
                } else {
                    tmpArr[ac++] = String.fromCharCode(o1, o2, o3);
                }
            } while (i < encodedData.length);
            dec = tmpArr.join('');

            // Adapted from Solution #1 at https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding
            // Going backwards: from bytestream, to percent-encoding, to original string.
            return decodeURIComponent(
                dec
                    .replace(/\0+$/, '')
                    .split('')
                    .map(function (c) {
                        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                    })
                    .join('')
            );
        }

        const _processTraceableError = (data, response) => {
            const filteredData = Object.fromEntries(
                Object
                    .entries(data)
                    .filter(([key]) => ['message', 'exception', 'file', 'line'].includes(key))
            );

            return {
                title: 'Error: ' + data.message,
                body: 'Code: ' + response.status +
                    "\n" + 'Exception: ' + data.exception +
                    "\n" + 'File: ' + data.file +
                    "\n" + 'Line: ' + data.line +
                    (Object.keys(filteredData).length === 0
                        ? ''
                        : "\n" + 'Detail: ' + JSON.stringify(filteredData, null, ' ')
                    ),
            };
        };

        const _processMessageError = (data, response) => {
            const filteredData = Object.fromEntries(
                Object
                    .entries(data)
                    .filter(([key]) => ['message'].includes(key))
            );

            return {
                title: 'Error: ' + data.message,
                body: 'Code: ' + response.status +
                    (Object.keys(filteredData).length === 0
                        ? ''
                        : "\n" + 'Detail: ' + JSON.stringify(filteredData, null, ' ')
                    ),
            };
        };

        const _processUnknownErrorObject = (data, response) => ({
            title: 'Unknown Error!',
            body: 'Code: ' + response.status +
                (Object.keys(data).length === 0
                    ? ''
                    : "\n" + 'Detail: ' + JSON.stringify(data, null, ' ')
                ),
        });

        const _processUnknownErrorNonObject = (data, response) => ({
            title: 'Unknown Error!',
            body:
                'Code: ' + response.status +
                "\n" + 'Detail: ' + JSON.stringify(data, null, ' '),
        });

        const _processMissingErrorResponse = (response) => {
            const alertMessages = [];

            if (response) {
                alertMessages.push(response.statusText
                    ? 'Http error: ' + response.statusText
                    : 'Http error'
                );

                alertMessages.push(response.status
                    ? 'Code: ' + response.status
                    : 'Unknown code'
                );
            } else {
                alertMessages.push('Unknown error');
            }

            return { body: alertMessages.join("\n") };
        };

        const isObject = (x) => x && typeof x === 'object';

        const makeErrorAction = (data, response) => {
            if (isObject(data) && (isArray(data._actions) || isObject(data._redirect))) {
                return;
            }

            if (!data) {
                return _processMissingErrorResponse(response);
            }

            response = response || { status: 0 };

            if ('exception' in data && 'file' in data && 'line' in data && 'message' in data && 'trace' in data) {
                return _processTraceableError(data, response);
            }

            if ('message' in data && Object.prototype.toString.call(data.message).trim().length > 0) {
                return _processMessageError(data, response);
            }

            if (isObject(data)) {
                return _processUnknownErrorObject(data, response);
            }

            return _processUnknownErrorNonObject(data, response);
        };

        const detachNode = (elements) => elements.forEach((e) => e.parentNode && e.parentNode.removeChild(e));

        const disableEvent = (event) => {
            event.preventDefault();
            return false;
        };

        const cssExtract = x => window.__unocss_runtime.extract(x);

        const processPreview = (domContent, mode) => {
            const config = {
                doneButtonText: 'Selesai',
                printButtonText: 'Cetak',
                manualPrintText: 'Silahkan cetak halaman ini.',
            };
            mode = parseInt(mode, 10) || 0;

            cssExtract(domContent);

            return new Promise((resolve) => {
                const html = __html();
                const body = __body();
                body.classList.add('uno-layer-important-important-bg-white');

                const bodyChildren = Array.prototype.slice.call(body.children);
                const attrStorage = 'data-' + randomString();
                const buttonClass = 'bg-white rounded-6 m-2 p-3 border-3 border-gray-300 shadow'.split(' ');

                const printBlocker = document.createElement('style');
                printBlocker.innerHTML = mode === 1 ? '' : '@media print{*,* *{display:none!important}}';

                const blocker = document.createElement('div');
                blocker.classList.add(...'fixed inset-0 bg-white z-50'.split(' '));

                const content = document.createElement('div');
                let contentInnerHTMLCache;
                const inject = (force) => {
                    if (force || contentInnerHTMLCache !== content.innerHTML) {
                        content.innerHTML = domContent;
                        contentInnerHTMLCache = content.innerHTML;
                    }
                };
                inject(1);

                const reinjectInterval = setInterval(() => {
                    inject();
                }, 1000);

                const previewDoneButton = document.createElement('button');
                previewDoneButton.setAttribute('type', 'button');
                previewDoneButton.classList.add(...buttonClass);
                previewDoneButton.innerHTML = config.doneButtonText;

                const printButton = document.createElement('button');
                printButton.setAttribute('type', 'button');
                printButton.classList.add('ml-4', ...buttonClass);
                printButton.innerHTML = config.printButtonText;

                const buttonContainer = document.createElement('div');
                buttonContainer.classList.add(...'print:hidden text-black bottom-0 left-0 fixed'.split(' '));
                buttonContainer.appendChild(previewDoneButton);
                if (mode === 1) {
                    buttonContainer.appendChild(printButton);
                }

                const globalPrint = () => {
                    inject(1);
                    print();
                };

                const previewDone = () => {
                    body.appendChild(blocker);
                    body.classList.remove('uno-layer-important-important-bg-white');
                    detachNode([content, buttonContainer]);
                    bodyChildren.forEach((child) => {
                        child.style.cssText = child.getAttribute(attrStorage);
                        child.removeAttribute(attrStorage);
                    });

                    clearInterval(reinjectInterval);
                    resolve();
                    setTimeout(() => detachNode([blocker, printBlocker]), 10);
                };

                const printer = () => {
                    detachNode([printBlocker]);

                    if (!('print' in window)) {
                        basicAlert(config.manualPrintText);
                        return;
                    }

                    setTimeout(() => {
                        globalPrint();

                        const unprintTarget = [html, body];
                        const events = ['mousemove', 'keydown'];
                        let printHasFinished = false;
                        const fn = () => {
                            unprintTarget.forEach((t) => events.forEach((e) => t.removeEventListener(e, fn)));
                            if (!printHasFinished) {
                                previewDone();
                            }
                            printHasFinished = true;
                        };
                        unprintTarget.forEach((t) => events.forEach((e) => t.addEventListener(e, fn)));
                    }, 1000); // alert may still be animating...
                };

                const manualPrint = () => {
                    if ('print' in window) {
                        globalPrint();
                    } else {
                        basicAlert(config.manualPrintText);
                    }
                };

                const previewBegin = () => {
                    previewDoneButton.addEventListener('click', previewDone, { once: true });
                    printButton.addEventListener('click', manualPrint);
                    [printBlocker, blocker, content, buttonContainer].forEach((c) => body.appendChild(c));
                    bodyChildren.forEach((child) => {
                        child.setAttribute(attrStorage, child.style.cssText);
                        child.style.cssText = 'display:none!important;opacity:0!important;visibility:hidden!important';
                    });

                    setTimeout(() => {
                        detachNode([blocker]);
                        if (mode === 0) {
                            printer();
                        }
                    }, 1);
                };

                previewBegin();
            });
        };

        const processAction = (action, options) => {
            const hasAction = action && action.type;
            options = options || {};

            if (hasAction) {
                options._log && options._log(action, options);

                switch (hasAction) {
                    case 'location-hash':
                        location.hash = action.hash;
                        break;

                    case 'new-tab':
                        // window.open(action.url, '_blank');

                        instantLink({
                            href: action.url,
                            target: '_blank',
                        });
                        break;

                    case 'entry':
                        if (options._entry) {
                            options._entry(action, options);
                        }
                        break;

                    case 'alert':
                        return options._alert
                            ? options._alert(action, options)
                            : basicAlert((action.title ? action.title + '\n\n' : '') + action.body);

                    case 'message':
                        const messageHandler = options._message || options._alert;
                        return messageHandler
                            ? messageHandler(action, options)
                            : basicAlert(action.body);

                    case 'notification':
                        const notificationHandler = options._notification || options._alert;
                        return notificationHandler
                            ? notificationHandler(action, options)
                            : basicAlert((action.title ? action.title : 'Notification') + '\n\n' + action.body);

                    case 'download-url':
                        const downloadUrlMessage = {
                            body: 'Download file ' + action.name + ' at ' + action.url,
                            url: action.url,
                        };
                        options._entry ? options._entry(downloadUrlMessage, options) : basicAlert(downloadUrlMessage.body);

                        instantLink({
                            href: action.url,
                            download: action.name || 'download',
                        });
                        break;

                    case 'download-content':
                        const blob = new Blob([phpBase64Decode(action.body)], {
                            type: action.contentType || 'application/octet-stream',
                        });

                        instantLink({
                            href: window.URL.createObjectURL(blob),
                            download: action.name || 'download',
                        });

                        const downloadContentMessage = { body: 'Download file ' + action.name };
                        options._entry ? options._entry(downloadContentMessage, options) : basicAlert(downloadContentMessage.body);
                        break;

                    case 'preview-html':
                        return processPreview(action.body, action.mode);

                    default:
                        break;
                }
            }

            return Promise.resolve();
        };

        const processRedirect = ({ url, type }) => {
            url = url || location.href;
            switch (type) {
                case 'reload':
                    location.reload();
                    break;

                case 'assign':
                    location.assign(url);
                    break;

                case 'replace':
                    location.replace(url); // no history
                    break;

                case 'href':
                default:
                    location.href = url; // adds history
                    break;
            }
        };

        const blockForRedirect = () => {
            const attrStorage = 'data-' + randomString();
            const body = __body();

            const div = document.createElement('div');
            div.classList.add(...'fixed inset-0 important-z-100'.split(' '));

            div.addEventListener('click', () => location.reload(), { once: true });
            ['focusin', 'focusout'].forEach((event) => div.addEventListener(event, disableEvent, true));

            [
                'contextmenu', 'wheel',
                'dragend', 'dragstart', 'select',
                // 'keydown', 'keyup',
                // 'mousedown', 'mouseup',
                // 'touchend', 'touchstart',
            ].forEach((event) => window.addEventListener(event, disableEvent, true));

            body.appendChild(div);
            body.setAttribute(attrStorage, body.style.cssText);
            body.style.cssText = 'display:none!important;opacity:0!important;visibility:hidden!important';

            window.addEventListener(
                'unload',
                () => {
                    body.style.cssText = body.getAttribute(attrStorage);
                    body.removeAttribute(attrStorage);
                    body.removeChild(div);
                },
                { once: true }
            );
        };

        function processActions(options, data, response, error, unloadingCallback) {
            let { _actions, _redirect } = data;

            if (error) {
                if ((response && [401].includes(response.status)) || data.message === 'Unauthenticated.') {
                    _redirect = _redirect || {
                        url: __avon.loginUrl,
                    };
                } else {
                    const unloading = unloadingCallback();

                    for (const element of [data, error]) {
                        const errorAction = makeErrorAction(element, response);

                        if (! errorAction) {
                            continue;
                        }

                        if (unloading) {
                            if (options._skipUnloadingError === undefined || options._skipUnloadingError) {
                                return;
                            }

                            const title = errorAction.title || '';
                            errorAction.title = title ? `${title} (unload event)` : '(Unload event)'
                        }

                        _actions = _actions || [];
                        _actions.push({
                            ...errorAction,
                            type: unloading || options._skipErrorHandling ? 'entry' : 'alert',
                        });

                        break;
                    }
                }
            }

            let promise;

            if (_actions && _actions.length) {
                promise = Promise.race([
                    Promise.allSettled(_actions.map((a) => processAction(a || {}, options))),
                    new Promise((resolve) => setTimeout(resolve, options._processActionsTimeout || 30)),
                ]).then(() => {
                    if (_redirect) {
                        blockForRedirect();
                        setTimeout(() => processRedirect(_redirect), 1);
                    }
                });
            } else if (_redirect) {
                blockForRedirect();
                setTimeout(() => processRedirect(_redirect), 1);
            }

            return error ? Promise.reject(error) : (promise || Promise.resolve());
        }

        return {
            processAction,
            processActions,
        };
    })();

    const axiosWrapper = ((axiosResolver, createWrapper) => {
        let axiosMemo;

        function callback(config) {
            if (!axiosMemo) {
                axiosMemo = axiosResolver();
            }

            const [axios, processActionsWrapper] = createWrapper(config, axiosMemo);
            const processor = config._processor ?? actionProcessor.processActions;

            return axios(config).then((response) => {
                return processActionsWrapper(processor, config, response.data || {}, response);
            }, (error) => {
                const response = (error && error.response) || {};
                return processActionsWrapper(processor, config, response.data || {}, response, error);
            });
        }

        callback._wrap = (config) => {
            const result = callback(config);
            return {
                abort: config._abortRequest,
                axios: result,
                control: config._abortControllers,
            };
        };

        callback._eventStream = (config) => {
            let params = config.params;
            if (params && Object.entries(params).length) {
                const p = Object.fromEntries(Object.entries(params).filter(([k, v]) => v != null));
                params = `?${new URLSearchParams(p).toString()}`;
            }
            else {
                params = '';
            }

            const processActionsWrapper = createWrapper(config)[1];
            const processor = config._processor ?? actionProcessor.processActions;
            const commandUrl = `${config.url}${params}`;
            const eventSource = new EventSource(commandUrl);
            const processEvent = (event) => processActionsWrapper(processor, config, JSON.parse(event.data) || {});

            eventSource.onerror = (err) => {
                processActionsWrapper(processor, config, {}, null, err);
                eventSource.close();
            };

            eventSource.addEventListener('_finish', async (event) => {
                await processEvent(event);
                eventSource.close();
            });

            eventSource.addEventListener('_reply', (event) => processEvent(event));

            return {
                eventSource,
                on(event, callback) {
                    eventSource.addEventListener(event, callback);
                },
            };
        };

        return callback;
    })(() => window.axios, (axiosConfig, axios) => {
        let cancellationReason;
        let hasSettled;
        let wasAborted;
        let blockElement = null;
        let overlayElement = null;
        const before = axiosConfig._beforeHook || noop;
        const aborted = axiosConfig._abortHook || noop;
        const settled = axiosConfig._settleHook || noop;
        const block = axiosConfig._block;
        const overlay = axiosConfig._overlay;
        const prevent = !!axiosConfig._preventUnload;
        const encrypt = !!axiosConfig._encrypt;
        const cryptRandomPool = randomStringDefaultPool + '0123456789';

        let unloading;
        const unloadingCallback = () => {
            unloading = true;
        };
        const isUnloading = () => unloading;
        window.addEventListener('beforeunload', unloadingCallback);

        const preventUnload = (e) => {
            e.preventDefault();
            return (e.returnValue = '');
        };

        const processActionsWrapper = (processor, options, data, response, error) => {
            hasSettled = true;

            if (wasAborted) {
                response._abortReason = cancellationReason;
            }

            const anySettle = () => {
                window.removeEventListener('beforeunload', unloadingCallback);
                prevent && window.removeEventListener('beforeunload', preventUnload);
                settled(data, response, error);
                if (overlayElement) {
                    const parent = overlayElement.parentNode;
                    if (parent) {
                        parent.removeChild(overlayElement);
                    }
                    overlayElement = null;
                }
                if (blockElement) {
                    blockElement.removeAttribute('inert');
                    blockElement = null;
                }
            };

            const result = processor(options, data, response, error, isUnloading);
            if (! result) {
                const reply = {
                    data: undefined,
                };
                if (wasAborted) {
                    reply._abortReason = cancellationReason;
                }
                anySettle();
                return reply;
            }

            return result.then(
                (x) => {
                    anySettle();
                    return x || response;
                },
                (x) => {
                    if (x && x.code === 'ERR_NETWORK' && x.name === 'AxiosError' && x.message === 'Network Error') {
                        return processor({}, {
                            _actions: [{
                                title: 'Network error: Server tidak dapat dijangkau atau sertifikat server berubah. Silakan muat kembali laman ini',
                                type: 'alert',
                            }],
                        })
                            .then(result => {
                                anySettle();
                                return result || {
                                    data: undefined,
                                };
                            });
                    }

                    anySettle();

                    throw x || error;
                }
            );
        };

        let abortController;
        let cancelTokenSource;

        if (axiosConfig._abortControllers) {
            const c = axiosConfig._abortControllers();

            abortController = c.abortController;
            cancelTokenSource = c.cancelTokenSource;
        }
        else {
            abortController = new AbortController();
            if (axios) {
                cancelTokenSource = axios.CancelToken.source();
            }

            axiosConfig._abortControllers = () => ({
                abortController,
                cancelTokenSource,
            });
        }

        axiosConfig._abortRequest = (reason) => {
            if (!hasSettled) {
                hasSettled = true;
                wasAborted = true;
                abortController && abortController.abort();
                cancelTokenSource.cancel();
                aborted(cancellationReason = reason ?? 'Request aborted');
            }
        };

        if (axios && cancelTokenSource && !axiosConfig.cancelToken) {
            axiosConfig.cancelToken = cancelTokenSource.token;
        }

        if (abortController && !abortController.signal) {
            axiosConfig.signal = abortController.signal;
        }

        prevent && window.addEventListener('beforeunload', preventUnload);

        if (block === true) {
            blockElement = document.getElementById('app');
        } else if (block && block.setAttribute && block.removeAttribute) {
            blockElement = block;
        }

        if (blockElement) {
            blockElement.setAttribute('inert', '');
        }

        if (overlay === true) {
            overlayElement = document.createElement('div');
            overlayElement.classList.add(...'fixed inset-0 bg-white z-100'.split(' '));
            // overlayElement.innerHTML = text;
        } else if (overlay) {
            overlayElement = overlay;
        }

        if (overlayElement) {
            const body = __body();
            body.appendChild(overlayElement);
        }

        const axiosWrapper = async (config) => {
            if (before) {
                await before();
            }

            if (! config.headers) {
                config.headers = {};
            }

            config.headers['x-random'] = randomString(16, cryptRandomPool);

            if (! encrypt) {
                return axios(config);
            }

            const [aesOnce, aesOnceBase64] = scrypt().randomBytes(32);

            const decrypt = (response) => {
                if (! encrypt) {
                    return response;
                }

                try {
                    const cipher = response.data?.cipher;
                    if (cipher == null) {
                        return cipher;
                    }

                    return JSON.parse(scrypt().decryptString(cipher, aesOnce));
                } catch (e) {
                    return null;
                }
            };

            return __avon.publicKey.then((publicKey) => {
                config.headers['x-encrypt-key'] = acrypt().encryptString(aesOnceBase64, publicKey);

                if (config.data) {
                    config.data = {
                        ...(config.dataNoEncrypt ?? {}),
                        cipher: scrypt().encryptString(JSON.stringify(config.data), aesOnce),
                    };
                }
                else if (config.dataNoEncrypt) {
                    config.data = {
                        ...(config.dataNoEncrypt ?? {}),
                    };
                }

                return axios(config).then(
                    (response) => {
                        if (response.data) {
                            response.data = decrypt(response);
                        }
                        return response;
                    },
                    (error) => {
                        if (error && error.response && error.response.data) {
                            error.response.data = decrypt(error.response);
                        }
                        throw error;
                    }
                );
            });
        };

        return [axiosWrapper, processActionsWrapper];
    });

    let bootstrappers = [];

    const register = (callback) => {
        const { app, configured } = window.__avon_runtime;

        if (app) {
            callback(app, configured);
        } else {
            bootstrappers.push(callback);
        }
    };

    const login = (option) => {
        const {
            axiosWrapper: axiosInternal,
            components,
            processAction,
        } = option;

        const {
            reactive, ref,
        } = Vue;

        const Login = {
            components: {
                BaseModal: components.BaseModal,
                BaseSvg: components.BaseSvg,
                Btn: components.Btn,
                PasswordInput: components.PasswordInput,
                Spinner: components.Spinner,
                TextInput: components.TextInput,
            },
            props: {
                promises: Array,
            },
            setup(props) {
                const loaded = ref(false);
                Promise.all(props.promises).then(() => loaded.value = true);

                document.documentElement.classList.add('size_xs');

                const captcha = reactive({
                    loading: false,
                    dialog: false,
                    question: '',
                    solution: '',
                    solved: true,
                });

                const checkCaptcha = async () => {
                    try {
                        captcha.loading = true;

                        const { data } = await axiosInternal({
                            method: 'post',
                            url: `${__avon.baseUrl}/api/application/captcha/request`,
                            params: {
                                key: form.captcha,
                            },
                        });

                        const solved = data.solved;

                        captcha.solved = solved;

                        if (solved) {
                            captcha.question = '';
                        }
                        else {
                            captcha.question = data.question;
                            captcha.dialog = true;
                        }

                        return solved;
                    } catch (e) {
                        return false;
                    } finally {
                        captcha.loading = false;
                    }
                };

                const solveCaptcha = async () => {
                    try {
                        captcha.loading = true;

                        if (captcha.solution.trim() === '') {
                            return processAction({
                                type: 'message',
                                body: 'Isian konfirmasi tidak boleh kosong',
                            });
                        }

                        const { data } = await axiosInternal({
                            method: 'post',
                            url: `${__avon.baseUrl}/api/application/captcha/solve`,
                            data: {
                                key: form.captcha,
                                solution: captcha.solution,
                            },
                        });

                        if (data.solved) {
                            captcha.solution = '';
                            captcha.question = '';
                            captcha.dialog = false;
                        }
                        else {
                            captcha.question = data.question;
                        }
                    } catch (e) {
                        // noop
                    } finally {
                        captcha.loading = false;
                    }
                };

                const form = reactive({
                    username: '',
                    password: '',
                    captcha: '',
                    mark: '',
                });

                const refs = reactive({
                    a: null,
                    b: null,
                    c: null,
                });

                const loading = ref(false);
                const loggedIn = ref(false);
                const passwordShown = ref(false);
                const slowRedirect = ref(false);

                return {
                    avon: __avon,
                    form,
                    loaded,
                    loading,
                    loggedIn,
                    passwordShown,
                    refs,
                    slowRedirect,

                    captcha,
                    solveCaptcha,

                    async login() {
                        if (form.username.trim() === '') {
                            return processAction({
                                type: 'message',
                                body: 'Username tidak boleh kosong',
                            });
                        }

                        if (form.password === '') {
                            return processAction({
                                type: 'message',
                                body: 'Password tidak boleh kosong',
                            });
                        }

                        if (form.captcha !== '') {
                            await checkCaptcha();
                            if (captcha.dialog === true) {
                                return;
                            }
                        }

                        passwordShown.value = false;
                        loading.value = true;

                        const fingerprint = fp2();

                        try {
                            const csrfRequest = axiosInternal({
                                method: 'get',
                                url: `${__avon.baseUrl}/login/csrf`,
                            });

                            form.mark = (await Promise.all([
                                fingerprint,
                                csrfRequest,
                            ]))[0];

                            let data;
                            try {
                                const { data: tryData } = await axiosInternal({
                                    method: 'post',
                                    url: `${__avon.baseUrl}/login`,
                                    data: form,
                                    dataNoEncrypt: {
                                        username: form.username,
                                    },
                                    _encrypt: true,
                                    _settleHook() {
                                        loading.value = false;
                                    },
                                });

                                data = tryData;
                                form.mark = '';
                            } catch (loginError) {
                                const status = loginError?.response?.status;

                                if (status !== 400) {
                                    throw loginError;
                                }

                                __avon.publicKey = (publicKeyBaseResolver())();
                                await __avon.publicKey;

                                const { data: retryData } = await axiosInternal({
                                    method: 'post',
                                    url: `${__avon.baseUrl}/login`,
                                    data: form,
                                    _encrypt: true,
                                    _settleHook() {
                                        loading.value = false;
                                    },
                                });

                                data = retryData;
                                form.mark = '';
                            }

                            if (data && data.captcha && data.captcha !== form.captcha) {
                                form.captcha = data.captcha;
                                const solved = await checkCaptcha();
                                if (! solved) {
                                    return;
                                }
                            }

                            if (data && data.loggedIn === 1) {
                                loggedIn.value = true;
                                setTimeout(() => slowRedirect.value = true, 1000);
                            }
                        } catch (e) {
                            loading.value = false;
                            return;
                        }
                    },

                    keepFocus(event, index) {
                        if (event.key !== 'Enter') {
                            return;
                        }

                        const length = Object.keys(refs).length;
                        let focusIndex;

                        if (event.shiftKey) {
                            event.preventDefault();
                            focusIndex = (index + length - 1) % length;
                        } else {
                            if (index === 2 && form.username.trim() !== '' && form.password !== '') {
                                return true;
                            }

                            event.preventDefault();
                            focusIndex = (index + length + 1) % length;
                        }

                        if (focusIndex != null) {
                            const focus = refs[Object.keys(refs)[focusIndex]];
                            focus.focus && focus.focus();
                            focus.select && focus.select();
                        }
                    },
                };
            },
            template: `
                <template v-if="loaded">
                    <div
                        uno-layer-default="relative float-right m-4 z-1"
                        v-if="avon.logoImageUrl"
                    >
                        <img
                            :src="avon.logoImageUrl"
                            uno-layer-default="h-[32px] w-auto"
                        >
                    </div>

                    <div
                        v-html="avon.loginBg"
                        :class="{
                            'absolute inset-0 bg-gray-200 group-[.dark-hour]:bg-gray-700': !avon.loginBg,
                        }"
                    />

                    <main uno-layer-default="min-h-screen pt-[12vh]">
                        <div uno-layer-default="px-4 max-w-127 mx-auto relative">
                            <div uno-layer-default="
                                p-8
                                bg-white shadow-md rounded-lg
                                space-y-6
                            ">
                                <header>
                                    <h1
                                        uno-layer-default="font-medium text-2xl text-center"
                                        v-text="avon.appName"
                                    />
                                </header>

                                <fieldset uno-layer-default="space-y-6">
                                    <label uno-layer-default="block">
                                        <span uno-layer-default="inline-block mb-2">Username:</span>
                                        <TextInput
                                            :disabled="loading || loggedIn"
                                            :ref="(el) => refs.a = el.$el"
                                            @keydown="keepFocus($event, 0)"
                                            autocapitalize="off"
                                            autocomplete="off"
                                            autofill="off"
                                            class="w-full"
                                            v-model="form.username"
                                        />
                                    </label>

                                    <label uno-layer-default="block">
                                        <span uno-layer-default="inline-block mb-2">Password:</span>
                                        <PasswordInput
                                            :disabled="loading || loggedIn"
                                            :ref="(el) => refs.b = el.$el.firstChild"
                                            @keydown="keepFocus($event, 1)"
                                            v-model="form.password"
                                            v-model:toggle="passwordShown"
                                        />
                                    </label>

                                    <Btn
                                        :disabled="loading || loggedIn"
                                        :ref="(el) => refs.c = el.$el"
                                        @click="login()"
                                        @keydown="keepFocus($event, 2)"
                                        class="border-gray-400 border"
                                    >Login</Btn>
                                </fieldset>

                                <footer
                                    uno-layer-default="text-center text-xs text-gray-400"
                                    v-text="avon.footer"
                                />
                            </div>

                            <div
                                uno-layer-default="absolute inset-0 z-1 flex items-center justify-center"
                                v-if="loading || slowRedirect"
                            >
                                <Spinner scale="4" />
                            </div>

                            <div
                                uno-layer-default="absolute inset-0 z-1 flex items-center justify-center"
                                v-if="loading || loggedIn"
                            >
                                <BaseSvg
                                    class="scale-200"
                                    viewBox="0 0 20 20"
                                >
                                    <polyline
                                        points="4 11 8 15 16 6"
                                        :class="{
                                            '!stroke-offset-[0]': loggedIn,
                                            'stroke-2 stroke-green-700': true,
                                            'stroke-join-round stroke-cap-round': true,
                                            'stroke-offset-[18px] stroke-dash-[18px]': true,
                                            'transition-all-300': true,
                                        }"
                                    />
                                </BaseSvg>
                            </div>
                        </div>
                    </main>

                    <div
                        uno-layer-default="absolute inset-0 bg-gray-200/50 min-h-screen pt-[13vh]"
                        v-if="captcha.dialog"
                    >
                        <div uno-layer-default="pt-8 px-16 max-w-127 mx-auto relative">
                            <BaseModal class="min-w-auto">
                                <template #heading>
                                    Verifikasi
                                </template>

                                <template #body>
                                    <div class="m-4 space-y-4">
                                        <p>Ketik kembali gambar berikut ini:</p>

                                        <img
                                            class="h-10"
                                            v-if="captcha.question"
                                            :src="'data:image/jpeg;base64,' + captcha.question"
                                        />

                                        <TextInput
                                            class="w-full"
                                            ss-xl="w-3/4"
                                            v-model="captcha.solution"
                                        />
                                    </div>
                                </template>

                                <template #footer>
                                    <Btn @click="captcha.dialog = false">Batal</Btn>
                                    <Btn @click="solveCaptcha">Konfirmasi</Btn>
                                </template>
                            </BaseModal>
                        </div>
                    </div>
                </template>
            `,
        };

        return {
            appComponent: Login,
        };
    };

    const application = () => {
        const __tmp_appName = document.head.querySelector('meta[name="application-name"]').content;

        const {
            computed, reactive, ref, watch,
            defineAsyncComponent, getCurrentInstance,
            nextTick,
            onBeforeMount, onBeforeUnmount, onMounted, onUnmounted,
            toRef, toRefs,
        } = Vue;

        const {
            refDebounced,
            useIdle,
            useKeyModifier,
            useStyleTag,
            useTitle,
        } = VueUse;

        const {
            extract: cssExtract,
            uno,
        } = window.__unocss_runtime;

        const primeVueComponents = (() => {
            const pv = primevue;
            let original;
            let prefixed;

            const make = (prefix) => {
                return Object.fromEntries(Object.values(pv).map((maybeComponent) => {
                    const name = maybeComponent?.name;
                    return name ? [`${prefix}${name}`, maybeComponent] : false;
                }).filter(Boolean));
            };

            return {
                original() {
                    if (! original) {
                        original = make('');
                    }
                    return original;
                },
                prefixed() {
                    if (! prefixed) {
                        prefixed = make('P');
                    }
                    return prefixed;
                },
            };
        })();

        const hashCode = (s) => {
            const l = (s || '').length;
            let h = 0;
            if (l > 0) {
                let i = 0;
                while (i < l) {
                    h = (h << 5) - h + s.charCodeAt(i++) | 0;
                }
            }
            return h;
        };

        const timeoutRaf = (callback, timeout) => {
            let timeoutRef;
            setTimeout(() => requestAnimationFrame(() => timeoutRef = setTimeout(callback, 0)), timeout || 0);
            return () => {
                if (timeoutRef) {
                    clearTimeout(timeoutRef);
                    timeoutRef = null;
                }
            };
        };

        const globalBus = ((mitt) => {
            if (! mitt) {
                return {};
            }

            const bus = mitt();
            window.addEventListener('unload', () => bus.all.clear(), { once: true });
            return bus;
        })(window.mitt);

        const { idle: globalIdle } = useIdle(10 * 1000);

        const globalStore = ((localforage) => {
            const baseData = () => ({
                data: {},
                loggedIn: false,
                user: null,
            });

            const store = ref(baseData());

            const globalKey = '__avon-userid';

            const key = (id) => {
                return id ? '__avon-user-' + id : '__avon-guest';
            };

            const encrypt = (key, payload) => {
                return JSON.parse(JSON.stringify(payload));
                // return CryptoJS.AES.encrypt(JSON.stringify(payload), key).toString();
            };

            const decrypt = (key, payload) => {
                return payload;
                // try {
                //     return JSON.parse(CryptoJS.AES.decrypt(payload, key).toString(CryptoJS.enc.Utf8));
                // } catch (e) {
                //     __LOG('load store data error', e);
                //     return {};
                // }
            };

            const storeData = (data) => {
                try {
                    data.hash = hashCode(JSON.stringify(data.data));
                } catch (e) {}
                const loggedIn = store.value.loggedIn;
                const newUserId = store.value.user && store.value.user.id;
                __LOG('save store data', previousUserId, newUserId, data);
                const storeKey = key(newUserId);
                return Promise.all([
                    localforage.setItem(storeKey, encrypt(storeKey, data)),
                    localforage.setItem(globalKey, loggedIn && newUserId ? encrypt(globalKey, newUserId) : null),
                ]).then(([stored]) => stored);
            };

            const loadStore = (userId) => Promise.all([
                localforage.getItem(key(userId)).then((payload) => {
                    const data = decrypt(key(userId), payload);
                    __LOG('load store data', userId, payload, data);
                    return data;
                }),
                localforage.getItem(globalKey).then((payload) => decrypt(globalKey, payload)),
            ]);

            const resetStoreData = (resetPayload) => {
                const reset = resetPayload || {};
                const resetData = reset.data || {};

                const newUser = Object.assign({}, reset.user);
                const newPayload = {
                    user: Object.keys(newUser).length ? newUser : null,
                    loggedIn: reset.loggedIn ?? store.value.loggedIn,
                    data: {
                        // http
                        app: Object.assign({}, resetData.app),
                        menu: resetData.menu || [],

                        // state
                        theme: Object.assign({}, resetData.theme),

                        // storage
                        localMessages: resetData.localMessages || [],
                        sessionMessages: resetData.sessionMessages || [],
                    },
                };

                store.value = newPayload;
            };

            let loader = Promise.resolve();
            const resetLoader = (userId) => loader = new Promise((resolve) => loadStore(userId).then(([data]) => {
                __LOG('load reset', userId, data);
                resetStoreData(data);
                resolve();
            }));

            const getLoader = () => loader;
            store.load = getLoader;

            store.store = (callback) =>
                new Promise((resolve) =>
                    localforage.getItem && localforage.setItem
                        ? getLoader()
                            .then(() => Promise.resolve(callback(store)))
                            .then(() => storeData(store.value))
                            .then(() => resolve(store.value))
                        : Promise.resolve()
                );

            let initialized;
            store.reset = async() => {
                if (!initialized) {
                    initialized = true;

                    createIdleableInterval(() => {
                        return getLoader().then(() => loadStore(previousUserId).then(([currentStore, currentUser]) => {
                            if (currentStore.user && currentStore.user.id === previousUserId) {
                                if (store.value.hash !== currentStore.hash) {
                                    resetStoreData(currentStore);
                                }
                            } else {
                                // reset
                            }
                        }));
                    }, 3000, 3 * 3000);

                    await resetLoader();
                }

                return getLoader();
            };

            let previousUserId;
            watch(
                store,
                async (data, old) => {
                    if (data.user && previousUserId != data.user.id) {
                        if (! previousUserId) {
                            await storeData(baseData());
                        }
                        previousUserId = data.user.id;
                        await resetLoader(previousUserId);
                    }
                    await getLoader().then(async() => await storeData(data));
                },
                { deep: true }
            );

            return store;
        })(window.localforage || {});

        const globalShiftModifier = useKeyModifier('Shift');

        const uiBootstrappers = ref([]);

        const booleanValue = (v) => computed(() => !!(v.value || v.value === ''));

        const createIdleableInterval = (callback, timeout, idle) => {
            let updater;
            function update() {
                timeoutRaf(() =>
                    Promise.resolve(callback()).then(() => {
                        clearTimeout(updater);
                        updater = setTimeout(update, globalIdle.value ? (idle || timeout) : timeout)
                    })
                );
            }

            watch(globalIdle, (idle) => {
                if (idle) {
                    __LOG('idle start');
                } else {
                    __LOG('idle stop');

                    clearTimeout(updater);
                    updater = null;
                    update();
                }
            });

            update();
        };

        const generateCssBody = (tokens) =>
            Promise.all(
                (isString(tokens) ? tokens.split(' ') : tokens).map((t) => uno.parseToken(t.trim()))
            ).then((tokens) => tokens.map((token) => token?.map((t) => t[2])).join('') ?? '');

        const transitionWait = (css, callback) => {
            const resizeEvent = () => timeoutRaf(() => window.dispatchEvent(new Event('resize')));

            const body = __body();
            const target = body.parentNode;

            const style = document.createElement('style');
            target.insertBefore(style, body);

            const finish = () => {
                target.removeChild(style);
                resizeEvent();
            };

            style.innerHTML = css;
            getComputedStyle(body);
            timeoutRaf(() => callback(finish));
        };

        const Lazy = {
            props: {
                disabled: Boolean,
                temporary: Boolean,
            },
            setup(props, { emit }) {
                const root = ref();
                const interacting = ref(false);
                const size = reactive({
                    height: null,
                    width: null,
                });

                onMounted(() => {
                    let intersecting = false;

                    const observer = new IntersectionObserver(([{ isIntersecting }]) => {
                        setIntersecting(isIntersecting);
                    });

                    const setIntersecting = debounce(100, (set) => {
                        if (!props.disabled) {
                            if (props.temporary || set) {
                                interacting.value = set;
                            }
                        }

                        if (set) {
                            size.height = null;
                            size.width = null;
                        } else if (root.value) {
                            let { width, height } = root.value.getBoundingClientRect();
                            size.height = `${height}px`;
                            size.width = `${width}px`;
                        }
                    });

                    observer.observe(root.value);
                    onBeforeUnmount(() => observer.disconnect());
                });

                return {
                    interacting,
                    root,
                    size,
                };
            },
            template: `
                <div ref="root" :style="size">
                    <slot v-bind="$attrs" v-if="interacting" />
                </div>
            `,
        };

        const Spinner = {
            props: {
                color: { default: null },
                blade: { default: 12 },
                bladeHeightRatio: { default: 0.555 },
                bladeWidth: { default: 0.074 },
                delay: { default: 1.2 },
                deviate: { default: 0.2 },
                duration: { default: 1 },
                precision: { default: 6 },
                scale: { default: 1 },
            },
            setup(props) {
                const {
                    color,
                    blade,
                    bladeHeightRatio,
                    bladeWidth,
                    delay,
                    deviate,
                    duration,
                    precision,
                    scale,
                } = toRefs(props);

                const animationName = 'loader-spinner-fade-' + randomString();
                const bladeColor = computed(() => color.value || '#69717d');
                const keyframes = ref('');
                Promise.all([generateCssBody('bg-gray-500/100'), generateCssBody('bg-transparent')]).then(
                    ([a, b]) => {
                        const k = `${animationName}{0%{${a}}100%{${b}}}`;
                        keyframes.value = `@-webkit-keyframes ${k}@keyframes ${k}`;
                    }
                );

                const { unload: styleUnload } = useStyleTag(keyframes);
                onBeforeUnmount(styleUnload);

                return {
                    wrapperStyle: computed(() => {
                        const size = parseFloat(scale.value).toFixed(precision.value);
                        return {
                            display: 'inline-block',
                            height: `${size}em`,
                            position: 'relative',
                            width: `${size}em`,
                        };
                    }),
                    bladeStyle: (bladeIndex) => ({
                        animation: `${animationName} ${duration.value.toFixed(precision.value)}s infinite linear`,
                        animationDelay: (bladeIndex * (duration.value / blade.value)).toFixed(precision.value) + 's',
                        backgroundColor: 'transparent',
                        borderRadius: '1em',
                        bottom: 0,
                        height: (scale.value * (0.5 * bladeHeightRatio.value)).toFixed(precision.value) + 'em',
                        left: (scale.value * (0.5 - bladeWidth.value / 2)).toFixed(precision.value) + 'em',
                        position: 'absolute',
                        transform: 'rotate(' + ((180 + (bladeIndex * (360 / blade.value))) % 360).toFixed(0) + 'deg)',
                        transformOrigin: 'center -' + (scale.value * (0.5 - 0.5 * bladeHeightRatio.value)).toFixed(precision.value) + 'em',
                        width: (scale.value * bladeWidth.value).toFixed(precision.value) + 'em',
                    }),
                };
            },
            template: `<div :style="wrapperStyle"><div v-for="i in blade" :style="bladeStyle(i - 1)" /></div>`,
        };

        const VirtualScroller = {
            components: {
                DynamicScroller: window.Vue3VirtualScroller ? Vue3VirtualScroller.DynamicScroller : {},
                DynamicScrollerItem: window.Vue3VirtualScroller ? Vue3VirtualScroller.DynamicScrollerItem : {},
                ResizeObserver: window.Vue3Resize ? Vue3Resize.ResizeObserver : {},
            },
            props: {
                // direction = vertical/horizontal
                keyField: String,
                list: Array,
                minItemSize: { default: 5 },
                sizeDependencyKeys: Array,
            },
            methods: {
                internalList() {
                    const list = this.list || [];
                    return this.keyField ? list : list.map((item, index) => ({ index, item }));
                },
                scroller() {
                    return this.$refs ? this.$refs.base : null;
                },
                dsiBind(p) {
                    const keys = this.sizeDependencyKeys || [];
                    const dependencies = keys.map((key) => p.item?.[key]).filter(Boolean);
                    return {
                        active: p.active,
                        dataIndex: p.index,
                        item: p.item,
                        sizeDependencies: dependencies.length ? dependencies : undefined,
                        watchData: keys.length === 0,
                    };
                },
                slotData(p) {
                    return this.keyField ? p.item : p.item.item;
                },
            },
            template: `<DynamicScroller ref="base" :items="internalList()" :min-item-size="minItemSize" v-bind="$attrs" #="p" :keyField="keyField || 'index'"
            ><DynamicScrollerItem v-bind="dsiBind(p)"><slot :data="slotData(p)" :index="p.index" /></DynamicScrollerItem></DynamicScroller>`,
        };

        const TransitionExpand = {
            props: {
                direction: String,
                transitionDuration: String,
            },
            setup(props) {
                const { direction, transitionDuration } = toRefs(props);
                const name = `expand_${direction.value}`;
                const prefix = 'uno-layer-base:selector-';
                const duration = 'important-duration-';

                cssExtract([
                    `${prefix}[.${name}-root]:[will-change:height,width]`,
                    `${prefix}[.${name}-root]:[transform:translateZ(0)]`,
                    `${prefix}[.${name}-root]:[backface-visibility:hidden]`,
                    `${prefix}[.${name}-root]:[perspective:1000px]`,

                    `${prefix}[.${name}-root.${name}-enter-active]:transition`,
                    `${prefix}[.${name}-root.${name}-enter-active]:important-[transition-property:height,width]`,
                    `${prefix}[.${name}-root.${name}-enter-active]:[overflow:hidden]`,

                    `${prefix}[.${name}-root.${name}-leave-active]:transition`,
                    `${prefix}[.${name}-root.${name}-leave-active]:important-[transition-property:height,width]`,
                    `${prefix}[.${name}-root.${name}-leave-active]:[overflow:hidden]`,

                    `${duration}${transitionDuration.value ?? ''}`,
                ]);

                if (direction.value === 'horizontal') {
                    cssExtract([
                        `${prefix}[.${name}-root.${name}-enter]:[width:0]`,
                        `${prefix}[.${name}-root.${name}-leave-to]:[width:0]`,
                    ]);
                } else if (direction.value === 'vertical') {
                    cssExtract([
                        `${prefix}[.${name}-root.${name}-enter]:[height:0]`,
                        `${prefix}[.${name}-root.${name}-leave-to]:[height:0]`,
                    ]);
                } else { // if (direction.value === 'both')
                    cssExtract([
                        `${prefix}[.${name}-root.${name}-enter]:[height:0]`,
                        `${prefix}[.${name}-root.${name}-enter]:[width:0]`,

                        `${prefix}[.${name}-root.${name}-leave-to]:[height:0]`,
                        `${prefix}[.${name}-root.${name}-leave-to]:[width:0]`,
                    ]);
                }

                let el;

                watch(transitionDuration, (td, old) => {
                    cssExtract(`${duration}${td}`);
                    if (el) {
                        el.classList.remove(`${duration}${old ?? ''}`);
                        el.classList.add(`${duration}${td}`);
                    }
                });

                const initElement = (element) => {
                    el = element;
                    if (element && element.classList && element.classList.add) {
                        element.classList.add(`${name}-root`);
                        element.classList.add(`${duration}${transitionDuration.value ?? ''}`);
                    }
                };

                onMounted(() => {
                    const {
                        ctx: { $el },
                    } = getCurrentInstance();
                    initElement($el);
                });

                const setElementSize = (element, height, width) => {
                    if (direction.value === 'horizontal') {
                        element.style.width = width;
                    } else if (direction.value === 'vertical') {
                        element.style.height = height;
                    } else { // if (direction.value === 'both')
                        element.style.height = height;
                        element.style.width = width;
                    }
                };

                return {
                    setups_: (attrs) => ({
                        name,
                        ...attrs,
                        onBeforeEnter: initElement,
                        onEnter(element) {
                            const { height, width } = getDomElementSizes(element);

                            setElementSize(element, 0, 0);
                            const { height: h, width: w } = getComputedStyle(element);

                            element.setAttribute('inert', '');
                            timeoutRaf(() => setElementSize(element, height, width));
                        },
                        onAfterEnter(element) {
                            setElementSize(element, null, null);
                            element.removeAttribute('inert');
                        },
                        onLeave(element) {
                            element.setAttribute('inert', '');

                            const { height, width } = getDomElementSizes(element);

                            setElementSize(element, height, width);
                            const { height: h, width: w } = getComputedStyle(element);

                            timeoutRaf(() => setElementSize(element, 0, 0));

                            attrs.onLeave && attrs.onLeave(element);
                        },
                    }),
                };
            },
            template: `<Transition v-bind="setups_($attrs)"><slot /></Transition>`,
        };

        const TransitionWrapper = {
            components: { TransitionExpand },
            props: {
                name: String,
                none: Boolean,
            },
            setup: () => ({
                setups_: (attrs) => {
                    const {
                        enterFromClass,
                        enterActiveClass,
                        enterToClass,
                        leaveFromClass,
                        leaveActiveClass,
                        leaveToClass,
                    } = attrs;

                    cssExtract(
                        [
                            enterFromClass,
                            enterActiveClass,
                            enterToClass,
                            leaveFromClass,
                            leaveActiveClass,
                            leaveToClass,
                        ]
                            .map((cls) => (cls || '').split(' ').filter(Boolean))
                            .flat(1)
                    );

                    return attrs;
                },
            }),
            template: `
                <slot v-if="none" />
                <Transition v-else-if="name" v-bind="setups_($attrs)" :name="name"><slot /></Transition>
                <TransitionExpand v-else v-bind="setups_($attrs)"><slot /></TransitionExpand>
            `,
        };

        const HtmlDecode = {
            props: {
                as: { default: null },
                value: { default: null },
            },
            setup(props) {
                const textareaDecoder = document.createElement('textarea');
                return {
                    decoded: computed(() => {
                        textareaDecoder.innerHTML = props.value;
                        const html = textareaDecoder.value;
                        textareaDecoder.innerHTML = '';
                        return html;
                    }),
                };
            },
            template: `
                <component v-bind="$attrs" v-if="as" :is="as">{{ decoded }}</component>
                <template v-else>{{ decoded }}</template>
            `,
        };

        const SimpleObjectDisplay = {
            props: {
                as: { default: null },
                emptyValue: { default: EM_DASH },
                emptyValues: { default: DEFAULT_FILLED_CASES },
                trimSpaces: { default: true },
                value: { default: null },
            },
            setup(props) {
                const { value } = toRefs(props);
                const isFilled = computed(
                    () =>
                        props.emptyValues.indexOf(
                            isString(value.value) && props.trimSpaces ? value.value.trim() : value.value
                        ) === -1
                );
                const isObject = (x) => x && typeof x === 'object';

                return {
                    as: props.as,
                    emptyValue: props.emptyValue,
                    isFilled,
                    isList: computed(() => (isFilled.value && isArray(value.value)) || isObject(value.value)),
                    isNumeric: computed(() => {
                        const v = value.value;

                        if (typeof v === "number" && isFinite(v) && Math.floor(v) === v) { // int
                            return true;
                        }

                        if (!!(v % 1)) { // float
                            return true;
                        }

                        return isString(v) && v.match(/^[-+]?[0-9]+(\.[0-9]+)?$/);
                    }),
                    isUnft: computed(() => value.value === undefined || value.value === null || value.value === false || value.value === true),
                    value,
                    list: computed(() => {
                        if (isArray(value.value)) {
                            return value.value;
                        }

                        if (isObject(value.value)) {
                            return Object.entries(value.value).map(([k, v]) => `${k}: ${v}`);
                        }

                        return [];
                    }),
                    unftString: computed(() => {
                        if (value.value === undefined) {
                            return 'undefined';
                        }
                        if (value.value === null) {
                            return 'null';
                        }
                        if (value.value === false) {
                            return 'false';
                        }
                        if (value.value === true) {
                            return 'true';
                        }
                        return props.emptyValue;
                    }),
                };
            },
            template: `
                <span
                    uno-layer-default="select-none"
                    v-if="!isFilled"
                    v-text="emptyValue"
                />
                <slot v-else :value="value">
                    <ul v-bind="$attrs" v-if="isList">
                        <li
                            v-for="value in list"
                            v-text="value"
                        />
                    </ul>
                    <component v-bind="$attrs" v-else-if="as" :is="as">{{ value }}</component>
                    <span v-else-if="isNumeric" class="font-mono tabular-nums slashed-zero">{{ value }}</span>
                    <code v-else-if="isUnft" class="font-mono font-italic">{{ unftString }}</code>
                    <template v-else>{{ value }}</template>
                </slot>
            `,
        };

        // https://github.com/AustinGil/vuetensils/blob/production/src/components/VDialog/VDialog.vue v0.12.2

        const FOCUSABLE_SELECTORS = [
            'a[href]:not([tabindex^="-"])',
            'area[href]:not([tabindex^="-"])',
            'input:not([disabled]):not([type="hidden"]):not([aria-hidden]):not([tabindex^="-"])',
            'select:not([disabled]):not([aria-hidden]):not([tabindex^="-"])',
            'textarea:not([disabled]):not([aria-hidden]):not([tabindex^="-"])',
            'button:not([disabled]):not([aria-hidden]):not([tabindex^="-"]):not([tabindex^="-"])',
            'iframe:not([tabindex^="-"])',
            'object:not([tabindex^="-"])',
            'embed:not([tabindex^="-"])',
            '[contenteditable]:not([tabindex^="-"])',
            '[tabindex]:not([tabindex^="-"])',
        ];

        const KEY_CODES = {
            TAB: 9,
            ENTER: 13,
            ESC: 27,
            SPACE: 32,
            PAGEUP: 33,
            PAGEDOWN: 34,
            END: 35,
            HOME: 36,
            LEFT: 37,
            UP: 38,
            RIGHT: 39,
            DOWN: 40,
        };

        const BaseDialog = {
            name: 'VDialog',
            inheritAttrs: false,
            model: {
                prop: 'showing',
                event: 'update:modelValue',
            },
            props: {
                modelValue: Boolean,
                /**
                 * @model
                 */
                showing: Boolean,
                /**
                 * HTML component for the dialog content.
                 */
                tag: {
                    type: String,
                    default: 'div',
                },
                /**
                 * Flag to enable/prevent the dialog from being closed.
                 */
                dismissible: {
                    type: Boolean,
                    default: true,
                },
                /**
                 * CSS width to set the dialog to.
                 */
                width: {
                    type: String,
                    default: '',
                },
                /**
                 * CSS width to set the dialog to.
                 */
                inlineSize: {
                    type: String,
                    default: '',
                },
                /**
                 * CSS max-width to set the dialog to.
                 */
                maxWidth: {
                    type: String,
                    default: '',
                },
                /**
                 * CSS max-width to set the dialog to.
                 */
                maxInlineSize: {
                    type: String,
                    default: '',
                },
                /**
                 * Prevents the page from being scrolled while the dialog is open.
                 */
                noScroll: Boolean,
                /**
                 * Transition name to apply to the dialog.
                 */
                transition: {
                    type: String,
                    default: '',
                },
                /**
                 * Transition name to apply to the background.
                 */
                contentTransition: {
                    type: String,
                    default: '',
                },
                classes: {
                    type: Object,
                    default: () => ({}),
                },
            },
            emits: ['update', 'update:modelValue', 'open', 'close'],
            data() {
                return {
                    localShow: this.modelValue || this.showing,
                    activeElement: null,
                };
            },
            computed: {
                slots() {
                    return this.$slots;
                },
            },
            watch: {
                showing(next) {
                    this.localShow = next;
                },
                modelValue(next) {
                    this.localShow = next;
                },
                localShow: {
                    handler(next, prev) {
                        if (typeof window === 'undefined') return;
                        if (next && next != prev) {
                            this.activeElement = document.activeElement;
                            this.onOpen();
                        } else {
                            this.onClose();
                            const { activeElement } = this;
                            if (activeElement && activeElement.focus) {
                                this.$nextTick(() => {
                                    activeElement.focus();
                                });
                            }
                        }
                        this.$emit('update', next);
                        this.$emit('update:modelValue', next);
                    },
                },
            },
            destroyed() {
                this.onClose();
            },
            methods: {
                onOpen() {
                    const { onClick, onKeydown, noScroll } = this;
                    window.addEventListener('click', onClick);
                    window.addEventListener('keydown', onKeydown);
                    noScroll && document.body.style.setProperty('overflow', 'hidden');
                    this.$nextTick(() => this.$refs.content.focus());
                    this.$emit('open');
                },
                onClose() {
                    const { onClick, onKeydown, noScroll } = this;
                    window.removeEventListener('click', onClick);
                    window.removeEventListener('keydown', onKeydown);
                    noScroll && document.body.style.removeProperty('overflow');
                    this.$emit('close');
                },
                onClick(event) {
                    if (event.target.classList.contains('vts-dialog') && this.dismissible) {
                        this.localShow = false;
                    }
                },
                onKeydown(event) {
                    if (event.keyCode === KEY_CODES.ESC && this.dismissible) {
                        this.localShow = false;
                    }
                    if (event.keyCode === KEY_CODES.TAB) {
                        const content = this.$refs.content;
                        if (!content) return;
                        const focusable = Array.from(content.querySelectorAll(FOCUSABLE_SELECTORS));
                        if (!focusable.length) {
                            event.preventDefault();
                            return;
                        }
                        if (!content.contains(document.activeElement)) {
                            event.preventDefault();
                            focusable[0].focus();
                        } else {
                            const focusedItemIndex = focusable.indexOf(document.activeElement);
                            if (event.shiftKey && focusedItemIndex === 0) {
                                focusable[focusable.length - 1].focus();
                                event.preventDefault();
                            }
                            if (!event.shiftKey && focusedItemIndex === focusable.length - 1) {
                                focusable[0].focus();
                                event.preventDefault();
                            }
                        }
                    }
                },
            },
            template: `
                <template v-if="localShow || slots.toggle">
                    <slot
                        v-if="slots.toggle"
                        name="toggle"
                        v-bind="{
                            on: {
                                click: () => (localShow = !localShow),
                            },
                            bind: {
                                type: 'button',
                                role: 'button',
                                'aria-haspopup': true,
                                'aria-expanded': '' + localShow,
                            },
                        }"
                    />
                    <transition :name="transition" :appear="true">
                        <template v-if="localShow">
                            <div
                                class="flex items-center justify-center fixed z-100 inset-0"
                                :class="[classes.root, classes.bg, $attrs.class]"
                            >
                                <transition :name="contentTransition">
                                    <component
                                        :is="tag"
                                        ref="content"
                                        class="focus:outline-0"
                                        :class="[classes.content]"
                                        :style="{
                                            width: width,
                                            'inline-size': inlineSize,
                                            'max-width': maxWidth,
                                            'max-inline-size': maxInlineSize,
                                        }"
                                        tabindex="-1"
                                        role="dialog"
                                        aria-modal="true"
                                    >
                                        <slot v-bind="{ close: () => (localShow = !localShow) }" />
                                    </component>
                                </transition>
                            </div>
                        </template>
                    </transition>
                </template>
            `,
        };

        const BaseSvg = {
            props: {
                fill: Boolean,
                stroke: { default: true },
                viewBox: { default: '0 0 24 24' },
            },
            setup(props) {
                const { fill, stroke } = toRefs(props);
                return {
                    fill: computed(() =>
                        fill.value === '' || fill.value === true ? 'currentColor' : fill.value || 'none'
                    ),
                    stroke: computed(() =>
                        stroke.value === '' || stroke.value === true ? 'currentColor' : stroke.value || 'none'
                    ),
                    fillAttribute: {
                        'clip-rule': 'evenodd',
                        'fill-rule': 'evenodd',
                    },
                    pathAttribute: {
                        'stroke-linecap': 'round',
                        'stroke-linejoin': 'round',
                        'stroke-width': 2,
                    },
                };
            },
            template: `
                <svg
                    :fill="fill"
                    :stroke="stroke"
                    :viewBox="viewBox"
                    width="16"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <slot
                        :attrs="$attrs"
                        :fillAttribute="fillAttribute"
                        :pathAttribute="pathAttribute"
                    />
                </svg>
            `,
        };

        const Icon = {
            components: { BaseSvg },
            props: {
                name: String,
            },
            template: `
                <BaseSvg #="{ fillAttribute, pathAttribute }">
                    <path v-bind="fillAttribute" v-if="name === 'mod-circle-fill'" d="M12 20a8 8 0 100-16 8 8 0 000 16z"/>

                    <path v-bind="pathAttribute" v-if="name === 'eye'" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"/>
                    <path v-bind="pathAttribute" v-if="name === 'eye'" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"/>

                    <path v-bind="pathAttribute" v-if="name === 'location-marker'" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"/>
                    <path v-bind="pathAttribute" v-if="name === 'location-marker'" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"/>

                    <path v-bind="pathAttribute" v-if="name === 'cr-middot'" d="M14 12a2 2 0 11-4 0 2 2 0 014 0z"/>

                    <path v-bind="pathAttribute" v-if="name === 'mod-chevron-left-right'" d="M16 8l4 4l-4 4M8 16l-4-4l4-4"/>
                    <path v-bind="pathAttribute" v-if="name === 'mod-exclamation-right'" d="M15 12m-10-6.938 0 13.856c0 1.54 1.667 2.502 3 1.732L20 13.732c1.333-.77 1.333-2.694 0-3.464L8 3.34c-1.333-.77-3 .192-3 1.732z"/>
                    <path v-bind="pathAttribute" v-if="name === 'mod-list-clear'" d="M3 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2M13 6h7M13 12h7m-7 6h7"/>
                    <path v-bind="pathAttribute" v-if="name === 'mod-presentation-chart-blank'" d="M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z"/>
                    <path v-bind="pathAttribute" v-if="name === 'mod-refresh'" d="M20 4v5h-.582m.518 4A8.001 8.001 0 1 1 19.418 9m0 0H15"/>
                    <path v-bind="pathAttribute" v-if="name === 'mod-tag-add'" d="M7 7h.01M7 3h5a1.99 1.99 0 011.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z"/>
                    <path v-bind="pathAttribute" v-if="name === 'mod-tag-delete'" d="M7 7h.01M7 3h5a1.99 1.99 0 011.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z"/>
                    <path v-bind="pathAttribute" v-if="name === 'mod-tag-edit'" d="M7 7h.01M7 3h5a1.99 1.99 0 011.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z"/>

                    <path v-bind="pathAttribute" v-if="name === 'arrow-left'" d="M10 19l-7-7m0 0l7-7m-7 7h18"/>
                    <path v-bind="pathAttribute" v-if="name === 'arrow-sm-right'" d="M13 7l5 5m0 0l-5 5m5-5H6"/>
                    <path v-bind="pathAttribute" v-if="name === 'bell'" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"/>
                    <path v-bind="pathAttribute" v-if="name === 'check'" d="M5 13l4 4L19 7"/>
                    <path v-bind="pathAttribute" v-if="name === 'check-circle'" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                    <path v-bind="pathAttribute" v-if="name === 'chevron-double-left'" d="M11 19l-7-7 7-7m8 14l-7-7 7-7"/>
                    <path v-bind="pathAttribute" v-if="name === 'chevron-double-up'" d="M5 11l7-7 7 7M5 19l7-7 7 7"/>
                    <path v-bind="pathAttribute" v-if="name === 'chevron-down'" d="M19 9l-7 7-7-7"/>
                    <path v-bind="pathAttribute" v-if="name === 'chevron-left'" d="M15 19l-7-7 7-7"/>
                    <path v-bind="pathAttribute" v-if="name === 'chevron-right'" d="M9 5l7 7-7 7"/>
                    <path v-bind="pathAttribute" v-if="name === 'chevron-up'" d="M5 15l7-7 7 7"/>
                    <path v-bind="pathAttribute" v-if="name === 'clipboard'" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2"/>
                    <path v-bind="pathAttribute" v-if="name === 'clock'" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"/>
                    <path v-bind="pathAttribute" v-if="name === 'document'" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"/>
                    <path v-bind="pathAttribute" v-if="name === 'document-add'" d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"/>
                    <path v-bind="pathAttribute" v-if="name === 'dots-horizontal'" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                    <path v-bind="pathAttribute" v-if="name === 'download'"  d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"/>
                    <path v-bind="pathAttribute" v-if="name === 'eye-off'" d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21"/>
                    <path v-bind="pathAttribute" v-if="name === 'filter'" d="M3 4a1 1 0 011-1h16a1 1 0 011 1v2.586a1 1 0 01-.293.707l-6.414 6.414a1 1 0 00-.293.707V17l-4 4v-6.586a1 1 0 00-.293-.707L3.293 7.293A1 1 0 013 6.586V4z"/>
                    <path v-bind="pathAttribute" v-if="name === 'menu'" d="M4 6h16M4 12h16M4 18h16"/>
                    <path v-bind="pathAttribute" v-if="name === 'moon'" d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z"/>
                    <path v-bind="pathAttribute" v-if="name === 'pencil-alt'" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"/>
                    <path v-bind="pathAttribute" v-if="name === 'pencil-square'" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"/>
                    <path v-bind="pathAttribute" v-if="name === 'plus'" d="M12 6v6m0 0v6m0-6h6m-6 0H6"/>
                    <path v-bind="pathAttribute" v-if="name === 'power'" d="M5.636 5.636a9 9 0 1012.728 0M12 3v9"/>
                    <path v-bind="pathAttribute" v-if="name === 'refresh'" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"/>
                    <path v-bind="pathAttribute" v-if="name === 'search'" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
                    <path v-bind="pathAttribute" v-if="name === 'selector'" d="M8 9l4-4 4 4m0 6l-4 4-4-4"/>
                    <path v-bind="pathAttribute" v-if="name === 'sun'" d="M12 3v1m0 16v1m9-9h-1M4 12H3m15.364 6.364l-.707-.707M6.343 6.343l-.707-.707m12.728 0l-.707.707M6.343 17.657l-.707.707M16 12a4 4 0 11-8 0 4 4 0 018 0z"/>
                    <path v-bind="pathAttribute" v-if="name === 'switch-horizontal'" d="M8 7h12m0 0l-4-4m4 4l-4 4m0 6H4m0 0l4 4m-4-4l4-4"/>
                    <path v-bind="pathAttribute" v-if="name === 'table'" d="M3 10h18M3 14h18m-9-4v8m-7 0h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v8a2 2 0 002 2z"/>
                    <path v-bind="pathAttribute" v-if="name === 'trash'" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                    <path v-bind="pathAttribute" v-if="name === 'x'" d="M6 18L18 6M6 6l12 12"/>
                    <path v-bind="pathAttribute" v-if="name === 'x-circle'" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                </BaseSvg>
            `,
        };

        const ShiftableText = {
            components: {
                Icon,
            },
            props: {
                always: Boolean,
            },
            setup: (props) => ({
                shift: computed(() => props.always || globalShiftModifier.value),
            }),
            template: `<slot /><Icon uno-layer-default="w-4 ml-0.5" :name="shift ? 'arrow-sm-right' : 'dots-horizontal'" />`,
        };

        const Btn = {
            props: {
                class: [String, Array, Object],
                external: Boolean,
                href: String,
                lock: Boolean,
                prefetch: Boolean,
            },
            setup(props) {
                const { class: className, external, href, lock, prefetch } = toRefs(props);
                let lastTouchTimestamp;
                let prefetched = false;
                const componentRef = ref();

                const removeHash = (url) => {
                    const index = url.indexOf('#');
                    return index < 0 ? url : url.substr(0, index);
                };

                const tryPreload = () => {
                    if (prefetched) return;
                    prefetched = true;

                    const a = componentRef.value;
                    const href = a.href;
                    if (
                        href &&
                        !a.hasAttribute('download') &&
                        href.indexOf('#') > -1 &&
                        removeHash(href) === removeHash(window.location.href)
                    ) {
                        const link = document.createElement('link');
                        link.setAttribute('rel', 'prefetch');
                        link.setAttribute('href', href);

                        const parent = __body() || __html();
                        parent.appendChild(link);
                        window.addEventListener('unload', () => parent.removeChild(link), { once: true });
                    }
                };

                const setupAttributes = (attrs, extras) => ({
                    onClick() {
                        if (lock.value) {
                            // sfa.ux.overlayLock();
                        }
                    },
                    onTouchstart() {
                        if (prefetch.value) {
                            lastTouchTimestamp = new Date().getTime();
                            tryPreload();
                        }
                    },
                    onMousedown() {
                        if (prefetch.value) {
                            if (lastTouchTimestamp > new Date().getTime() - 500) {
                                return;
                            }
                            tryPreload();
                        }
                    },
                    target: external.value ? '_blank' : undefined,
                    ...attrs,
                    ...extras,
                    rel: external.value ? 'noopener noreferrer' : undefined,
                });
                return {
                    className,
                    componentRef,
                    is_: computed(() => (href.value ? 'a' : 'button')),
                    setups_: (attrs) =>
                        setupAttributes(attrs, href.value ? { href: href.value } : { type: 'button' }),
                };
            },
            template: `<component
                :class="className"
                :is="is_"
                ref="componentRef"
                v-bind="setups_($attrs)"
                uno-layer-base="
                    transition inline-flex items-center justify-center rounded-lg
                    text-gray-800 hover:bg-black/5
                    appearance-none border-gray-400 py-2 px-3 text-base
                    outline-none
                    focus:ring focus:ring-size-[0.25rem] focus:ring-blue-300 focus:border-blue-300
                    active:not-focus:bg-black/10
                    mix-shade-10:active:ring-blue-300 mix-shade-20:active:focus:ring-blue-300
                "
            ><slot /></component>`,
        };

        const ComponentLoader = {
            components: {
                Btn,
                Spinner,
            },
            emits: ['status'],
            props: {
                errorMessage: String,
                refreshMessage: String,
                resolved: Function,
                resolver: Function,
                screen: Boolean,
                unmounted: Boolean,
            },
            setup(props, { emit }) {
                const { resolved, resolver, unmounted } = toRefs(props);
                const status = ref(0);
                watch(status, (status) => emit('status', status));

                const reload = async() => {
                    setTimeout(() => {
                        if (status.value === 0) {
                            status.value = 1;
                        }
                    }, 500);

                    try {
                        if (resolver.value) {
                            const result = await resolver.value();
                            if (!result) {
                                status.value = 0;
                                return;
                            }
                        }
                        status.value = 2;

                        if (resolved.value) {
                            await resolved.value();
                        }
                        status.value = 3;
                    } catch (e) {
                        status.value = 4;
                        throw e;
                    }
                };

                reload();

                return {
                    reload,
                    status,
                };
            },
            template: `
                <div
                    :class="{ 'min-h-screen': screen }"
                    uno-layer-default="h-full flex items-center justify-center"
                    v-if="status === 0"
                />

                <div
                    :class="{ 'min-h-screen': screen }"
                    uno-layer-default="h-full flex items-center justify-center"
                    v-if="status.value === 1 || status.value === 2 || unmounted.value"
                >
                    <Spinner scale="2" />
                </div>

                <slot v-if="status === 3" />

                <div
                    :class="{ 'min-h-screen': screen }"
                    uno-layer-default="h-full flex flex-col items-center justify-center"
                    v-if="status === 4"
                >
                    <span v-text="errorMessage || 'Error loading'" />
                    <Btn
                        class="px-2 py-1 m-1"
                        @click="reload"
                    >
                        {{ refreshMessage || 'Refresh' }}
                    </Btn>
                </div>
            `,
        };

        const OverlayBlocker = {
            props: {
                parentClass: String,
            },
            setup(props, { emit }) {
                const focusTrapDom = ref();
                let focusTrapInstance = null;

                onMounted(() => {
                    focusTrapInstance = focusTrap.createFocusTrap(focusTrapDom.value, {
                        allowOutsideClick: false,
                    });

                    try {
                        focusTrapInstance.activate();
                    } catch (error) {
                        __WARN({ error });
                    }
                });

                onBeforeUnmount(() => {
                    if (focusTrapInstance) {
                        try {
                            focusTrapInstance.deactivate();
                            focusTrapInstance = null;
                        } catch (error) {
                            __WARN({ error });
                        }
                    }
                });

                return {
                    focusTrapDom,
                    overlayTarget: document.getElementById('modal-overlay'),
                };
            },
            template: `
                <teleport :to="overlayTarget">
                    <div
                        ref="focusTrapDom"
                        :class="{
                            'fixed inset-0 z-60': !parentClass,
                            [parentClass]: parentClass,
                        }"
                    />
                </teleport>
            `,
        };

        const PopperTeleport = {
            components: {
                TransitionWrapper,
            },
            emits: ['update:toggle'],
            props: {
                blur: Boolean,
                modal: Boolean,
                parentClass: String,
                persist: Boolean,
                placement: String,
                strategy: String,
                target: { default: undefined },
                toggle: Boolean,
                transition: String,
                untrap: Boolean,
                wrapperClass: String,
            },
            setup(props, { emit }) {
                const { modal, toggle } = toRefs(props);
                const focusTrapDom = ref(null);
                const overlayTarget = computed(() =>
                    modal.value
                        ? document.getElementById('modal-overlay')
                        : document.getElementById('panel-overlay')
                );
                const popperDom = ref(null);
                let focusTrapInstance = null;
                let popperInstance = null;

                const focusTrapDeactivate = () => {
                    if (focusTrapInstance) {
                        try {
                            focusTrapInstance.deactivate();
                            focusTrapInstance = null;
                        } catch (error) {
                            __WARN({ error });
                        }
                    }
                };

                const popperDeactivate = () => {
                    if (popperInstance) {
                        popperInstance.destroy();
                        popperInstance = null;
                    }
                };

                const createPopper = (target, popperDom) => {
                    if (modal.value || !target || !popperDom) {
                        return;
                    }

                    popperDeactivate();

                    return (popperInstance = Popper.createPopper(
                        // TODO: support modalless popper
                        target ? (target.$ && target._ && target.$el ? target.$el : target) : null,
                        popperDom,
                        {
                            strategy: props.strategy || 'fixed',
                            placement: props.placement || 'bottom-start',
                            modifiers: [
                                {
                                    name: 'offset',
                                    options: { offset: [0, 8] },
                                },
                                {
                                    name: 'flip',
                                    options: {
                                        fallbackPlacements: ['top-start', 'bottom-end', 'top-end', 'auto'],
                                    },
                                },
                            ],
                        }
                    ));
                };

                watch(popperDom, (dom) => createPopper(props.target, dom));

                watch(
                    () => props.target,
                    (target) => createPopper(target, popperDom.value)
                );

                const internalParentToggle = ref(toggle.value);
                const internalToggle = ref(toggle.value);
                const internalBackgroundToggle = ref(toggle.value);
                const internalPanelToggle = ref(toggle.value);

                const emitUpdateToggle = (value) => {
                    if (modal.value) {
                        globalBus.emit('global-modal', { value });
                    }
                    emit('update:toggle', value);
                };

                const closeToggle = () => {
                    if (!props.persist) {
                        internalToggle.value = false;
                        // focusTrapDeactivate();
                    }
                };

                watch(toggle, (value) => {
                    if (value) {
                        internalParentToggle.value = true;
                        internalToggle.value = true;
                        internalBackgroundToggle.value = true;
                        internalPanelToggle.value = true;
                        emitUpdateToggle(true);

                        nextTick(() => {
                            if (!props.untrap && !focusTrapInstance) {
                                focusTrapInstance = focusTrap.createFocusTrap(focusTrapDom.value, {
                                    allowOutsideClick: true,
                                });
                            }

                            try {
                                if (focusTrapInstance) {
                                    focusTrapInstance.activate();
                                }
                            } catch (error) {
                                __WARN({ error });
                            }

                            if (popperInstance) {
                                popperInstance.forceUpdate();
                            }
                        });
                    } else {
                        internalToggle.value = false;
                        focusTrapDeactivate();
                    }
                });

                watch(
                    () => internalBackgroundToggle.value === false && internalPanelToggle.value === false,
                    (allClosed) => {
                        if (allClosed) {
                            internalParentToggle.value = false;
                        }
                    }
                );

                watch(internalParentToggle, (value) => {
                    if (!value) {
                        emitUpdateToggle(false);
                    }
                });

                onBeforeUnmount(() => {
                    focusTrapDeactivate();
                    popperDeactivate();
                });

                return {
                    closeToggle,
                    focusTrapDom,
                    overlayTarget,
                    popperDom,
                    slotBinding: { toggle: internalToggle, closeToggle },
                    internalParentToggle,
                    internalToggle,
                    backgroundLeave() {
                        internalBackgroundToggle.value = false;
                    },
                    panelLeave() {
                        internalPanelToggle.value = false;
                    },
                };
            },
            template: `
                <teleport :to="overlayTarget">
                    <div
                        v-if="internalParentToggle"
                        :class="{
                            'fixed inset-0 z-50': !parentClass,
                            [parentClass]: parentClass,
                        }"
                    >
                        <TransitionWrapper
                            @afterLeave="backgroundLeave($event)"
                            name="fade"
                            enterActiveClass="transition-opacity"
                            leaveActiveClass="transition-opacity"
                            enterFromClass="opacity-0"
                            leaveToClass="opacity-0"
                        >
                            <slot
                                v-if="internalToggle"
                                name="background"
                                v-bind="slotBinding"
                            >
                                <div
                                    uno-layer-default="absolute inset-0 transition"
                                    @click="closeToggle()"
                                >
                                    <template v-if="blur">
                                        <div uno-layer-default="absolute inset-0 backdrop-blur"></div>
                                        <div uno-layer-default="absolute inset-0 bg-black opacity-60"></div>
                                    </template>
                                </div>
                            </slot>
                        </TransitionWrapper>

                        <div
                            ref="focusTrapDom"
                            :class="{
                                'fixed inset-0 pointer-events-none flex items-center justify-center': !wrapperClass,
                                [wrapperClass]: wrapperClass,
                            }"
                        >
                            <div
                                @keydown.esc="closeToggle()"
                                uno-layer-default="pointer-events-auto outline-none"
                                ref="popperDom"
                                tabindex="0"
                            >
                                <button
                                    uno-layer-default="hidden"
                                    type="button"
                                    v-if="internalToggle" @click="closeToggle()"
                                >close</button>

                                <TransitionWrapper v-if="modal"
                                    name="scale"
                                    enterActiveClass="transition property-opacity,transform"
                                    leaveActiveClass="transition property-opacity,transform"
                                    enterFromClass="opacity-0 scale-90"
                                    leaveToClass="opacity-0 scale-90"
                                    @afterLeave="panelLeave($event)"
                                >
                                    <slot
                                        v-if="internalToggle"
                                        v-bind="slotBinding"
                                    />
                                </TransitionWrapper>
                                <TransitionWrapper v-else
                                    name="fade"
                                    enterActiveClass="transition-opacity"
                                    leaveActiveClass="transition-opacity"
                                    enterFromClass="opacity-0"
                                    leaveToClass="opacity-0"
                                    @afterLeave="panelLeave($event)"
                                >
                                    <slot
                                        v-if="internalToggle"
                                        v-bind="slotBinding"
                                    />
                                </TransitionWrapper>
                            </div>
                        </div>
                    </div>
                </teleport>
            `,
        };

        const PopperButtoned = {
            components: {
                Btn,
                PopperTeleport,
            },
            emits: ['update:toggle', 'shift-click'],
            props: {
                blur: Boolean,
                buttonClass: [String, Array, Object],
                disabled: Boolean,
                hidden: Boolean,
                modal: Boolean,
                parentClass: String,
                persist: Boolean,
                preventOnShiftClick: Boolean,
                toggle: Boolean,
                transition: String,
                unbuttoned: Boolean,
                wrapperClass: String,
            },
            setup(props, { emit }) {
                const button = ref();
                const internalToggle = ref(props.toggle);

                watch(
                    () => props.toggle,
                    (t) => (internalToggle.value = t)
                );
                watch(internalToggle, (t) => emit('update:toggle', t));

                return {
                    button,
                    buttonClick(event) {
                        if (event.shiftKey) {
                            emit('shift-click', event);
                            if (props.preventOnShiftClick) {
                                return;
                            }
                        }
                        if (!props.disabled) {
                            internalToggle.value = true;
                        }
                    },
                    internalToggle,
                    updateToggle(update) {
                        internalToggle.value = update === undefined
                            ? !internalToggle.value
                            : !!update;
                    },
                };
            },
            template: `
                <component
                    :class="buttonClass"
                    :is="unbuttoned ? 'div' : 'Btn'"
                    @click="buttonClick($event)"
                    ref="button"
                    v-show="!hidden"
                >
                    <slot name="button" />

                    <PopperTeleport
                        :blur="blur"
                        :modal="modal"
                        :parent-class="parentClass"
                        :persist="persist"
                        :target="button"
                        :transition="transition || 'panel-transition'"
                        :wrapper-class="wrapperClass"
                        v-model:toggle="internalToggle"
                    >
                        <slot
                            :target="button"
                            :updateToggle="updateToggle"
                            name="popper"
                        />
                    </PopperTeleport>
                </component>
            `,
        };

        const BaseModal = {
            components: {
                Spinner,
                TransitionWrapper,
            },
            props: {
                loading: Boolean,
            },
            template: `
                <div uno-layer-default="border border-gray-200 rounded shadow overflow-hidden bg-white min-w-[72vw] relative">
                    <div
                        class="component-modal-wrapper"
                        uno-layer-default="relative max-h-[72vh] flex flex-col"
                    >
                        <h4
                            uno-layer-default="text-lg font-semibold m-4 mb-4"
                        >
                            <slot name="heading" />
                        </h4>

                        <slot name="body" />

                        <div
                            uno-layer-default="flex flex-col space-y-2 p-4 bg-gray-100"
                            sm="flex-row space-y-0 space-x-4 justify-end"
                        >
                            <slot name="footer" />
                        </div>
                    </div>

                    <TransitionWrapper
                        name="fade"
                        enterActiveClass="transition-opacity"
                        leaveActiveClass="transition-opacity"
                        enterFromClass="opacity-0"
                        leaveToClass="opacity-0"
                    >
                        <div v-if="loading" uno-layer-default="absolute inset-0 rounded-lg bg-gray-100 backdrop-blur-8 opacity-60 flex items-center justify-center">
                            <Spinner scale="2" />
                        </div>
                    </TransitionWrapper>
                </div>
            `,
        };

        const ConfirmModal = {
            components: {
                Btn,
                BaseModal,
                PopperButtoned,
                ShiftableText,
            },
            emits: ['update:toggle'],
            props: {
                buttonClass: [String, Array, Object],
                confirm: Boolean,
                confirmText: String,
                shiftable: Boolean,
                shiftButtonClass: [String, Array, Object],
                unshiftButtonClass: [String, Array, Object],
                onClick: Function,
                text: String,
                toggle: Boolean,
                noCancel: Boolean,
            },
            setup(props, { emit }) {
                const internalToggle = ref(props.toggle);
                watch(
                    () => props.toggle,
                    (t) => (internalToggle.value = t)
                );
                watch(internalToggle, (t) => emit('update:toggle', t));

                const clickWrapper = (event) => {
                    Promise
                        .resolve((props.onClick || noop)(event))
                        .then(() => internalToggle.value = false);
                };

                return {
                    shift: computed(() => globalShiftModifier.value),
                    internalToggle,
                    clickWrapper,

                    setToggle(value) {
                        internalToggle.value = value;
                    },
                    shiftClick(event) {
                        if (props.confirm) {
                            internalToggle.value = true;
                        } else {
                            clickWrapper(event);
                        }
                    },
                };
            },
            template: `
                <PopperButtoned
                    :blur="true"
                    :buttonClass="[buttonClass, shift ? shiftButtonClass : unshiftButtonClass]"
                    :modal="true"
                    :preventOnShiftClick="true"
                    :toggle="internalToggle"
                    @shift-click="shiftClick($event)"
                    @update:toggle="setToggle($event)"
                >
                    <template #button>
                        <slot name="button" :shift="shift">
                            <ShiftableText v-if="shiftable">{{ text }}</ShiftableText>
                            <template v-else>{{ text }}</template>
                        </slot>
                    </template>

                    <template #popper>
                        <BaseModal>
                            <template #heading>
                                <slot name="heading">
                                    {{ text }}
                                </slot>
                            </template>
                            <template #body>
                                <slot />
                            </template>
                            <template #footer>
                                <Btn
                                    v-if="!noCancel && !confirm"
                                    class="px-4"
                                    @click="setToggle(false)"
                                >Cancel</Btn>
                                <Btn
                                    class="px-4 text-white bg-blue-500 focus:bg-blue-600 hover:bg-blue-700"
                                    @click="clickWrapper($event)"
                                >{{ confirmText || (confirm ? 'Close' : text) }}</Btn>
                            </template>
                        </BaseModal>
                    </template>
                </PopperButtoned>
            `,
        };

        const TextInput = {
            emits: ['update:modelValue'],
            props: {
                datalist: Array,
                modelValue: String,
                type: String,
            },
            setup(props, { emit }) {
                const { modelValue, type } = toRefs(props);
                const value = ref(modelValue.value ?? '');
                watch(modelValue, (v) => value.value = v);
                watch(value, (v) => emit('update:modelValue', v));
                const listId = randomString(32);
                return {
                    type: computed(() => type.value || 'text'),
                    value,
                    listId,
                };
            },
            template: `
                <input
                    :list="listId"
                    :type="type"
                    class="form-input"
                    uno-layer-base="rounded-md transition border-gray-400"
                    v-bind="$attrs"
                    v-model="value"
                >
                <datalist
                    :id="listId"
                    v-if="datalist && datalist.length"
                >
                    <option v-for="dl in datalist" :value="dl" />
                </datalist>
            `,
        };

        const InputChip = {
            emits: ['update:modelValue'],
            props: {
                disabled: [Boolean, String],
                type: { default: 'checkbox' },
                modelValue: [String, Number, Boolean],
            },
            setup(props, { emit }) {
                const { disabled, modelValue } = toRefs(props);
                const value = ref(modelValue.value);
                watch(modelValue, (v) => value.value = v);
                watch(value, (v) => emit('update:modelValue', v));
                return {
                    disabled: booleanValue(disabled),
                    value,
                };
            },
            template: `
                <input
                    :disabled="disabled"
                    :type="type"
                    class="form-control"
                    uno-layer-base="w-5 h-5 border border-gray-400 transition"
                    v-model="value"
                    :class="{
                        'cursor-pointer': !disabled,
                        'rounded-md': type === 'checkbox',
                        'rounded-full': type === 'radio',
                    }"
                >
            `,
        };

        const InputSearch = {
            components: {
                Icon,
                TextInput,
            },
            emits: ['update:modelValue'],
            props: {
                modelValue: String,
                wrapperClass: String,
            },
            setup(props, { emit }) {
                const { modelValue } = toRefs(props);
                const inputRef = ref();
                const inputValue = ref(modelValue.value);
                watch(inputValue, (inputValue) => emit('update:modelValue', inputValue));
                return {
                    inputRef,
                    inputValue,
                    iconClick() {
                        inputRef.value.$el.focus();
                        emit('update:modelValue', inputValue.value);
                    },
                };
            },
            template: `
                <div
                    :class="wrapperClass"
                    uno-layer-default="relative flex items-center"
                >
                    <TextInput
                        class="rounded-3xl py-1 pl-8"
                        placeholder="search"
                        ref="inputRef"
                        type="search"
                        v-bind="$attrs"
                        v-model="inputValue"
                    />

                    <div uno-layer-default="absolute inset-y-0 left-0 flex items-center ml-2 select-none">
                        <Icon
                            name="search"
                            class="w-5"
                            @click="iconClick($event)"
                        />
                    </div>
                </div>
            `,
        };

        const SelectList = {
            components: {
                Btn,
                Icon,
                TextInput,
                VirtualScroller,
            },
            emits: ['model', 'update:search', 'update:value', 'wants-close'],
            props: {
                fuseKeys: { default: undefined },
                labelKey: { default: 'label' },
                missingModel: { default: { value: undefined, label: EM_DASH } },
                options: Array,
                optionsCallback: Function,
                clearable: Boolean,
                enterConfirmFirstSearch: Boolean,
                search: { default: '' },
                searchable: Boolean,
                searchDebounce: { default: 100 },
                selectedKey: { default: 'selected' },
                target: Object,
                value: { default: undefined },
                valueNormalizer: Function,
                valueKey: { default: 'value' },
            },
            setup(props, { emit }) {
                const { labelKey, options, target, valueKey, value } = toRefs(props);
                const fuseInstance = new Fuse([], {
                    keys: props.fuseKeys || [`model.${labelKey.value}`, `model.${valueKey.value}`],
                    ignoreDiacritics: true,
                });
                const fuseSearchDom = ref();
                const internalOptions = ref([]);
                const lastSelected = ref();
                const optionsIsLoading = ref(true);
                const searchInput = ref(props.search);
                const shouldScrollToSelected = ref(true);
                const valueNormalizer = toRef(props, 'valueNormalizer');
                const virtualScrollerDomInstance = ref();
                let optionsLoadedOnce = false;

                const selectOptions = computed(() => {
                    if (searchInput.value.length > 0) {
                        return fuseInstance.search(searchInput.value).map((item) => {
                            item.item.refIndex = item.refIndex;
                            return item.item;
                        });
                    }

                    return internalOptions.value.map((item, index) => {
                        item.refIndex = index;
                        return item;
                    });
                });

                const applyOptionValue = (value) => (valueNormalizer.value ? valueNormalizer.value(value) : value);

                const applyOptions = (data) => {
                    let options = [];

                    if (data && data.length && data.length > 0) {
                        optionsLoadedOnce = true;
                        let hasSelected = false;
                        options = data.map((item, index) => {
                            const selected = !hasSelected && item[props.selectedKey];

                            if (selected) {
                                hasSelected = true;
                            }

                            item[valueKey.value] = applyOptionValue(item[valueKey.value]);

                            return {
                                model: item,
                                selected,
                                hovered: false,
                            };
                        });
                    }

                    optionsIsLoading.value = false;
                    fuseInstance.setCollection(options);
                    internalOptions.value = options;
                };

                const refreshOptions = (force) => {
                    optionsIsLoading.value = true;
                    const callback = props.optionsCallback;
                    if (callback) {
                        callback(force).then(applyOptions);
                    } else {
                        applyOptions(props.options || []);
                    }
                };

                watch(options, () => refreshOptions(false));
                refreshOptions(false);

                const hasScrolledToselectedItem = ref(false);
                const virtualScrollerVisible = () => {
                    if (shouldScrollToSelected.value) {
                        shouldScrollToSelected.value = false;
                        nextTick(() => timeoutRaf(scrollToSelectedItem));
                    } else {
                        timeoutRaf(() => hasScrolledToselectedItem.value = true);
                    }
                };

                let virtualScroller;
                watch(virtualScrollerDomInstance, (instance) => {
                    if (instance) {
                        virtualScroller = instance.scroller()
                    }
                });

                const scrollToSelectedItem = () => {
                    if (virtualScroller && lastSelected.value && lastSelected.value.virtualIndex) {
                        virtualScroller.scrollToItem(lastSelected.value.virtualIndex);
                    }
                    timeoutRaf(() => hasScrolledToselectedItem.value = true);
                };

                const clearSelection = () => {
                    lastSelected.value = null;
                    selectOptionFromSelection(null);
                };

                const searchEscapeKey = (event) => {
                    event.target.value !== '' && event.stopPropagation();
                };

                const searchEnterKey = (event) => {
                    if (event.target.value !== '' && props.enterConfirmFirstSearch) {
                        const availableOptions = selectOptions.value ?? [];

                        const selectOption = false
                            || availableOptions.find(({ hovered }) => hovered)
                            || availableOptions.find(({ selected }) => selected)
                            || availableOptions[0];

                        if (selectOption) {
                            selectOptionFromValue(selectOption.model[valueKey.value]);
                            emit('wants-close');
                        }
                    }
                };

                const listDoSearch = debounce(
                    props.searchDebounce,
                    (event) => emit('update:search', (searchInput.value = event.target.value))
                );

                const optionLabel = (optionProps) => optionProps.data.model[labelKey.value];

                const optionHover = (props, hover) => {
                    if (hover) {
                        props.data.hovered = true;
                        unhoverOption(props.index);
                    } else {
                        props.data.hovered = false;
                    }
                };

                const unhoverOption = debounce(
                    100,
                    (hovered) =>
                        internalOptions.value.forEach((item, index) => {
                            if (index !== hovered && item.hovered) {
                                item.hovered = false;
                            }
                        })
                );

                const optionSelected = (optionProps) => {
                    selectOptionFromValue(optionProps.data.model[valueKey.value]);
                    shouldScrollToSelected.value = false;
                };

                const selectOptionFromValue = (value) => selectOptionFromValueDelegate(applyOptionValue(value));

                let selectValue = null;
                const selectOptionFromValueDelegate = (value) => {
                    let selection = null;
                    selectValue = value;

                    internalOptions.value.forEach((item, index) => {
                        if (item.selected) {
                            item.selected = false;
                        }

                        if (selection == null && value === item.model[valueKey.value]) {
                            item.selected = true;
                            lastSelected.value = item;
                            selection = item.model;
                        }
                    });

                    if (lastSelected.value) {
                        lastSelected.value.virtualIndex = selectOptions.value.findIndex(
                            (item) => item.refIndex === lastSelected.value.refIndex
                        );
                        shouldScrollToSelected.value = true;
                    }

                    selectOptionFromSelection(selection);
                };

                const selectOptionFromSelection = (selection) => {
                    if (!selection) {
                        selection = props.missingModel;
                    }

                    if (optionsLoadedOnce) {
                        emit('model', selection);
                        emit('update:value', selection ? selection[valueKey.value] : selection);
                    }
                };

                watch(value, selectOptionFromValue);

                watch(internalOptions, () => selectOptionFromValue(selectValue));

                selectOptionFromValue(value.value);

                const minWidth = ref();

                onBeforeMount(() => {
                    const parent = target.value && target.value.$el;
                    if (parent) {
                        minWidth.value = getDomElementSizes(parent).width;
                    }
                });

                return {
                    clearSelection,
                    fuseSearchDom,
                    internalOptions,
                    lastSelected,
                    listDoSearch,
                    listLimitSearch: computed(() => internalOptions.value.length === 0 && `${searchInput.value}` == ''),
                    minWidth,
                    optionHover,
                    optionLabel,
                    optionSelected,
                    optionsIsLoading,
                    refreshOptions,
                    scrollToSelectedItem,
                    searchEscapeKey,
                    searchEnterKey,
                    searchInput,
                    selectOptions,
                    virtualScrollerDomInstance,
                    virtualScrollerVisible,
                    hasScrolledToselectedItem,

                    selectOptionClass: ({ data: { hovered, selected } }) => ({
                        'bg-blue-200': hovered,
                        'font-semibold': selected,
                        'font-normal': !selected,
                    }),
                };
            },
            template: `
                <div
                    uno-layer-base="divide-y min-w-[32rem]"
                    uno-layer-default="bg-white rounded-xl shadow-xl border border-gray-200 divide-y divide-gray-200 overflow-hidden"

                    :style="{
                        minWidth: minWidth,
                    }"
                >
                    <div
                        uno-layer-default="flex p-2 space-x-2 bg-gray-100"
                    >
                        <TextInput
                            :readonly="listLimitSearch"
                            @input="listDoSearch($event)"
                            @keydown.esc="searchEscapeKey($event)"
                            @keydown.enter="searchEnterKey($event)"
                            class="w-full py-1"
                            ref="fuseSearchDom"
                            v-if="searchable"
                        />
                        <div v-else uno-layer-default="flex-grow" />

                        <Btn
                            class="px-2"
                            v-if="optionsCallback && !optionsIsLoading"
                            @click="refreshOptions(true)"
                        ><Icon name="mod-refresh" class="w-4" /></Btn>
                        <Btn
                            class="px-2"
                            v-if="lastSelected && lastSelected.refIndex"
                            @click="scrollToSelectedItem()"
                        ><Icon name="eye" class="w-4" /></Btn>
                        <Btn
                            class="px-2"
                            v-if="lastSelected && clearable"
                            @click="clearSelection()"
                        ><Icon name="mod-list-clear" class="w-4" /></Btn>
                    </div>
                    <div
                        v-if="
                            optionsIsLoading
                            || internalOptions.length === 0
                            || selectOptions.length === 0
                        "
                        uno-layer-default="flex items-center justify-center p-8"
                    >
                        <template v-if="optionsIsLoading">loading...</template>
                        <template v-else-if="internalOptions.length === 0">empty</template>
                        <template v-else-if="selectOptions.length === 0">no result</template>
                    </div>
                    <template v-else>
                        <VirtualScroller
                            #="props"
                            :list="selectOptions"
                            @visible="virtualScrollerVisible()"
                            class="min-h-[1rem] max-h-[40vh]"
                            keyField="refIndex"
                            ref="virtualScrollerDomInstance"
                            :class="{
                                'opacity-1': !hasScrolledToselectedItem
                            }"
                        >
                            <div
                                :class="selectOptionClass(props)"
                                @click="optionSelected(props)"
                                @keydown.enter.prevent="optionSelected(props)"
                                @mouseout="optionHover(props, false)"
                                @mouseover="optionHover(props, true)"
                                uno-layer-default="flex cursor-pointer px-4 py-3 items-center"
                                tabindex="0"
                            >
                                <slot name="option" v-bind="props">
                                    <div uno-layer-default="flex-none w-8 select-none">
                                        <Icon
                                            class="w-4 children-stroke-3"
                                            name="check"
                                            v-if="props.data.selected"
                                        />
                                    </div>
                                    <div><slot
                                        name="default" v-bind="props"><div
                                            uno-layer-default="select-none"><slot
                                                name="label" v-bind="props">{{
                                                    optionLabel(props)
                                                }}</slot></div></slot></div>
                                </slot>
                            </div>
                        </VirtualScroller>
                    </template>
                </div>
            `,
        };

        const DayjsDisplay = {
            components: { SimpleObjectDisplay },
            props: {
                as: { default: 'time' },
                displayMode: { default: 'formatted' }, // datetime, duration
                displayTime: { default: true },
                duration: Boolean,
                emptyValue: { default: EM_DASH },
                format: { default: 'dddd, D MMMM YYYY, HH:mm:ss' },
                locale: { default: __avon.locale },
                parseFormat: String,
                timezone: String,
                value: { default: null },
            },
            setup(props) {
                const {
                    as,
                    displayMode,
                    displayTime,
                    duration,
                    emptyValue,
                    format,
                    locale,
                    parseFormat,
                    timezone,
                    value,
                } = toRefs(props);

                const hasDuration = !!dayjs.duration;
                const mode = ref(hasDuration && duration.value ? 'duration' : displayMode.value);

                const normalizedFormat = computed(() =>
                    format.value === 'dddd, D MMMM YYYY, HH:mm:ss'
                        ? (displayTime.value
                            ? format.value
                            : 'dddd, D MMMM YYYY'
                        ) : format.value
                );

                const dayJsInstance = computed(() => {
                    try {
                        if (value.value == null) {
                            return null;
                        }

                        const instance = parseFormat.value ? dayjs(value.value, parseFormat.value) : dayjs(value.value);

                        return timezone.value ? instance.tz(timezone.value) : instance;
                    } catch (error) {
                        __WARN({ value: value.value, parseFormat: parseFormat.value, error });
                        return null;
                    }
                });

                const dateTimeUnformatted = computed(() =>
                    dayJsInstance.value
                        ? dayJsInstance.value.format(displayTime.value ? 'YYYY-MM-DD HH:mm:ss' : 'YYYY-MM-DD')
                        : null
                );

                const dateTimeFormatted = computed(() =>
                    dayJsInstance.value
                        ? dayJsInstance.value.locale(locale.value).format(normalizedFormat.value)
                        : null
                );

                const dateTimeDurationFormat = computed(() =>
                    dayJsInstance.value && hasDuration
                        ? dayjs.duration(dayJsInstance.value.diff(dayjs())).locale(locale.value).humanize(true)
                        : null
                );

                return {
                    as,
                    dateTimeUnformatted,
                    dayJsInstance,

                    title: computed(() =>
                        dayJsInstance.value
                            ? (hasDuration
                                ? [
                                      dateTimeUnformatted.value || emptyValue.value,
                                      dateTimeFormatted.value || emptyValue.value,
                                      dateTimeDurationFormat.value || emptyValue.value,
                                  ].join('; ')
                                : [
                                      dateTimeUnformatted.value || emptyValue.value,
                                      dateTimeFormatted.value || emptyValue.value,
                                  ].join('; ')
                            ) : (value.value == null
                                ? emptyValue.value
                                : `Invalid value: ${value.value}`
                            )
                    ),

                    display: computed(() => {
                        if (!dayJsInstance.value) {
                            return null;
                        }

                        switch (mode.value) {
                            case 'formatted':
                                return dateTimeFormatted.value;
                            case 'duration':
                                return dateTimeDurationFormat.value;
                            case 'datetime':
                            default:
                                return dateTimeUnformatted.value;
                        }
                    }),

                    toggleModes: () => {
                        switch (mode.value) {
                            case 'formatted':
                                return (mode.value = hasDuration ? 'duration' : 'datetime');
                            case 'duration':
                                return (mode.value = 'datetime');
                            case 'datetime':
                            default:
                                return (mode.value = 'formatted');
                        }
                    },
                };
            },
            template: `<component :is="as" :datetime="dateTimeUnformatted" :title="title"
                ><SimpleObjectDisplay :value="display"
                /><span v-if="dayJsInstance" @click="toggleModes" uno-layer-default="text-gray-600 cursor-default select-none"
                    ><slot name="toggle"> ⇄</slot></span></component>`,
        };

        const AutonumericDisplay = {
            components: { SimpleObjectDisplay },
            props: {
                as: { default: 'span' },
                decimal: { default: 2 },
                decimalCharacter: { default: '.' },
                digitGroupSeparator: { default: ',' },
                formatted: Boolean,
                prefix: String,
                suffix: String,
                toggle: { default: true },
                value: null,
            },
            setup(props) {
                const { value } = toRefs(props);

                const defaultConfig = () => {
                    const config = {
                        currencySymbol: props.prefix || '',
                        suffixText: props.suffix || '',
                        decimalPlaces: props.decimal,
                        // maximumValue: '9999999999999.99',
                        // minimumValue: '-9999999999999.99',
                    };

                    if (props.digitGroupSeparator !== props.decimalCharacter) {
                        config.digitGroupSeparator = props.digitGroupSeparator;
                        config.decimalCharacter = props.decimalCharacter;
                    }

                    return config;
                };

                const showFormatted = ref(true);

                const numberFormatted = computed(() =>
                    isNaN(value.value) ? value.value : AutoNumeric.format(value.value, defaultConfig())
                );

                return {
                    as: props.as,
                    showToggle: computed(() => props.toggle),
                    title: computed(() => `${numberFormatted.value} (${props.value})`),
                    toggle: () => (showFormatted.value = !showFormatted.value),
                    value,

                    display: computed(() =>
                        props.formatted
                            ? numberFormatted.value
                            : (showFormatted.value
                                ? numberFormatted.value
                                : props.value
                            )
                    ),
                };
            },
            template: `<component :is="as" :title="title"
                ><SimpleObjectDisplay :value="display"
                /><span v-if="showToggle" @click="toggle" uno-layer-default="text-gray-600 cursor-default select-none"
                    ><slot name="toggle"> ⇄</slot></span></component>`,
        };

        const PasswordInput = {
            components: {
                Icon,
                TextInput,
            },
            emits: ['update:modelValue', 'update:toggle'],
            props: {
                inputClass: String,
                modelValue: String,
                toggle: Boolean,
                wrapperClass: String,
            },
            setup(props, { emit }) {
                const { inputClass, modelValue, toggle } = toRefs(props);

                const inputModel = ref(modelValue.value);
                watch(modelValue, (v) => (inputModel.value = v));
                watch(inputModel, (v) => emit('update:modelValue', v));

                const toggleValue = ref(toggle.value);
                watch(toggle, (v) => (toggleValue.value = v));
                watch(toggleValue, (v) => emit('update:toggle', v));

                const inputRef = ref();
                const cutCopyFn = (event) => {
                    if (!toggleValue.value) {
                        event.preventDefault();
                    }
                };
                onMounted(() => {
                    const input = inputRef.value.$el;
                    input.addEventListener('copy', cutCopyFn);
                    input.addEventListener('cut', cutCopyFn);
                });

                return {
                    inputModel,
                    inputRef,
                    toggleValue,
                    eyeIcon: computed(() => (toggleValue.value ? 'eye' : 'eye-off')),

                    inputClass: computed(() => [
                        inputClass.value,
                        {
                            'font-mono': toggleValue.value,
                            'password-bullet tracking-[0.1ch]': !toggleValue.value,
                        },
                    ]),

                    eyeToggle() {
                        const t = !toggleValue.value;
                        toggleValue.value = t;
                        const input = inputRef.value;
                        if (input) {
                            input.$el.focus();
                        }
                    },
                };
            },
            template: `
                <span
                    :class="wrapperClass"
                    uno-layer-default="flex relative items-center"
                >
                    <TextInput
                        :class="inputClass"
                        autocomplete="off"
                        autofill="off"
                        class="w-full"
                        ref="inputRef"
                        v-model="inputModel"
                    />

                    <div
                        uno-layer-default="absolute inset-y-0 flex items-center right-0"
                    >
                        <button
                            @click="eyeToggle()"
                            uno-layer-default="text-gray-600 hover:text-gray-900 p-1 mr-2"
                            type="button"
                        >
                            <Icon
                                :name="eyeIcon"
                                class="w-4"
                            />
                        </button>
                    </div>
                </span>
            `,
        };

        const AutosizeInput = {
            emits: ['update:modelValue'],
            props: { modelValue: String },
            setup(props, { emit, expose }) {
                const { modelValue } = toRefs(props);
                const value = ref(modelValue.value ?? '');
                const input = ref();
                watch(modelValue, modelValue => {
                    value.value = modelValue;
                });
                watch(value, value => {
                    autosize.update(input.value);
                    nextTick(() => autosize.update(input.value));
                    emit('update:modelValue', value);
                });
                onMounted(() => autosize(input.value));
                onBeforeUnmount(() => autosize.destroy(input.value));
                expose({
                    autosizeUpdate() {
                        autosize.update(input.value);
                    },
                });

                return { input, value };
            },
            template: `
                <textarea
                    class="form-input"
                    ref="input"
                    rows="1"
                    uno-layer-base="rounded-md w-full transition border-gray-400"
                    v-model="value"
                />
            `,
        };

        const AutonumericInput = {
            emits: ['update:modelValue'],
            props: {
                class: String,
                config: Object,
                decimal: { default: 2 },
                decimalCharacter: { default: '.' },
                digitGroupSeparator: { default: ',' },
                modelValue: String,
                prefix: String,
                suffix: String,
                min: String,
                max: String,
            },
            setup(props, { emit }) {
                const { class: inputClass } = toRefs(props);
                const hidden = ref(null);
                const input = ref(null);
                let anHidden = null;
                let autonumeric = null;

                const defaultConfig = () => {
                    const config = {
                        currencySymbol: props.prefix || '',
                        suffixText: props.suffix || '',
                        decimalPlaces: props.decimal,
                        // maximumValue: '9999999999999.99',
                        // minimumValue: '-9999999999999.99',
                    };

                    if (props.digitGroupSeparator !== props.decimalCharacter) {
                        config.digitGroupSeparator = props.digitGroupSeparator;
                        config.decimalCharacter = props.decimalCharacter;
                    }

                    if (props.max != null && props.max !== '') {
                        config.maximumValue = props.max;
                    }

                    if (props.min != null && props.min !== '') {
                        config.minimumValue = props.min;
                    }

                    return config;
                };

                const emitModelValueSlow = debounce(1000, (value) => emit('update:modelValue', value));
                const emitModelValueQuick = debounce(300, (value) => emit('update:modelValue', value));

                const setValueWatch = (value) => {
                    try {
                        if (autonumeric) {
                            autonumeric.set(value);
                            emitModelValueSlow(autonumeric.getNumericString());
                        }
                    } catch (e) {}
                };

                const setValueInput = (value) => {
                    try {
                        if (anHidden && autonumeric) {
                            anHidden.set(value);
                            emitModelValueQuick(autonumeric.getNumericString());
                        }
                    } catch (e) {
                        nextTick(() => setValueWatch(anHidden.getFormatted()));
                    }
                };

                watch(() => props.modelValue, setValueWatch);

                onMounted(() => {
                    const cfg = { ...defaultConfig(), ...props.config };

                    anHidden = new AutoNumeric(hidden.value, { ...cfg, overrideMinMaxLimits: null });

                    autonumeric = new AutoNumeric(input.value, cfg);

                    input.value.addEventListener('input', (event) => {
                        setValueInput(event.target.value);
                    });

                    setValueWatch(props.modelValue);
                });

                return {
                    hidden,
                    input,
                    inputClass,

                    hiddenStyle: {
                        height: '1px',
                        left: '-1px',
                        position: 'absolute',
                        top: '-1px',
                        visibility: 'hidden',
                        width: '1px',
                    },
                };
            },
            template: `<input class="form-input" type="text" v-bind="$attrs" ref="input" :class="inputClass"
                ><input tabindex="-1" readonly ref="hidden" :style="hiddenStyle">`,
        };

        const FlatPickrInput = {
            components: {
                Icon,
            },
            emits: ['update:modelValue', 'date-time-value', 'invalid-date'],
            props: {
                class: String,
                enableTime: { default: true },
                firstDayOfWeek: { default: 1 },
                flatpickr: { default: {} },
                format: { default: 'l, j F Y, H:i:S' },
                wrapperClass: String,
                locale: { default: window.flatpickr ? window.flatpickr.l10ns.id : {} },
                modelValue: String,
                nullable: { default: true },
                readonly: Boolean,
                target: { default: null },
            },
            setup(props, { emit }) {
                const {
                    class: inputClass,
                    enableTime,
                    firstDayOfWeek,
                    flatpickr: flatpickrOption,
                    format,
                    locale,
                    modelValue,
                    target,

                    readonly, // not supported
                } = toRefs(props);

                const datePreformat = (value) => {
                    if (!value) {
                        return null;
                    }

                    if (!isString(value)) {
                        return value;
                    }

                    let date = new Date(value);
                    if (date.getTime() === date.getTime()) {
                        return date;
                    }

                    date = new Date(value.replace(/-/g, '/'));
                    if (date.getTime() === date.getTime()) {
                        return date;
                    }

                    return value;
                };

                const toStrValue = (date) => {
                    if (!date) {
                        return '';
                    }

                    const year = date.getFullYear();
                    const parts = Object.fromEntries(
                        [
                            ['month', date.getMonth() + 1],
                            ['day', date.getDate()],
                            ['hour', date.getHours()],
                            ['minute', date.getMinutes()],
                            ['second', date.getSeconds()],
                        ].map(([key, part]) => [key, ('0' + part).slice(-2)])
                    );

                    return [
                        [year, parts.month, parts.day].join('-'),
                        enableTime.value ? [parts.hour, parts.minute, parts.second].join(':') : '',
                    ]
                        .join(' ')
                        .trim();
                };

                const dateValue = ref();
                const dateStrValue = ref('');
                const dateFormat = computed(() => format.value.replace(enableTime.value ? '' : /, H:i:S$/, ''));

                watch(dateValue, (date) => {
                    const strVal = toStrValue(date);
                    dateStrValue.value = strVal;
                    emit('update:modelValue', strVal);
                    emit('date-time-value', {
                        date,
                        stringValue: strVal,
                    });
                });

                let flatpickrInstance;

                watch(modelValue, (value) => {
                    if (flatpickrInstance) {
                        try {
                            flatpickrInstance.setDate(datePreformat(modelValue.value), true, dateFormat.value);
                        } catch (e) {
                            emit('invalid-date', {
                                date: modelValue.value,
                                stringValue: '',
                            });
                        }
                    }
                });

                const config = () => ({
                    defaultDate: datePreformat(modelValue.value),
                    dateFormat: dateFormat.value,
                    appendTo: target.value ? target.value : undefined,
                    closeOnSelect: false,
                    // positionElement: target.value ? target.value : undefined,

                    locale: Object.assign({}, locale.value, {
                        firstDayOfWeek: firstDayOfWeek.value,
                    }),

                    enableTime: enableTime.value,
                    enableSeconds: true,

                    onClose(selectedDates, dateStr, instance) {
                        if (!readonly.value) {
                            dateValue.value = selectedDates[0] || null;
                        }
                    },
                });

                const input = ref();

                onMounted(
                    () =>
                        (flatpickrInstance = flatpickr(input.value, {
                            ...config(),
                            ...(flatpickrOption.value || {}),
                        }))
                );

                onBeforeUnmount(() => {
                    if (flatpickrInstance) {
                        flatpickrInstance.destroy();
                    }
                });

                return {
                    input,
                    inputClass,
                    dateStrValue,
                    clearInput() {
                        if (flatpickrInstance) {
                            flatpickrInstance.input.value = '';
                        }
                        dateValue.value = null;
                    },
                };
            },
            template: `
                <time
                    :class="wrapperClass"
                    :datetime="dateStrValue"
                    uno-layer-default="flex relative items-center"
                >
                    <input
                        :class="inputClass"
                        class="form-input"
                        ref="input"
                        type="text"
                        uno-layer-default="[width:100%] rounded-md transition border-gray-400"
                        v-bind="$attrs"
                    >

                    <div
                        uno-layer-default="absolute inset-y-0 flex items-center right-0"
                        v-if="nullable && !readonly && dateStrValue !== ''"
                    >
                        <button
                            @click="clearInput()"
                            uno-layer-default="text-gray-600 hover:text-gray-900 p-1 mr-2"
                            type="button"
                        >
                            <Icon
                                name="x"
                                class="w-4"
                            />
                        </button>
                    </div>
                </time>
            `,
        };

        const CodeMirrorInput = {
            emits: ['update:modelValue'],
            props: {
                modelValue: String,
                readonly: Boolean,
            },
            setup(props, { emit }) {
                const { modelValue, readonly } = toRefs(props);
                const input = ref();
                let codeMirrorInstance;
                let updating = false;

                watch(modelValue, (v) => {
                    if (codeMirrorInstance) {
                        if (updating === true) {
                            updating = false;
                        } else {
                            codeMirrorInstance.setValue(v);
                        }
                    }
                });

                watch(readonly, (v) => {
                    if (codeMirrorInstance) {
                        codeMirrorInstance.setOption('readOnly', !!v);
                    }
                });

                onMounted(() => {
                    codeMirrorInstance = CodeMirror.fromTextArea(input.value, {
                        lineNumbers: true,
                        readOnly: !!readonly.value,
                    });

                    codeMirrorInstance.setValue(modelValue.value);

                    setTimeout(() => codeMirrorInstance.on('change', function (instance) {
                        updating = true;
                        emit('update:modelValue', instance.getValue());
                    }), 1);
                });

                return { input };
            },
            template: `
                <div
                    uno-layer-base="
                        leading-tight text-gray-800
                        all-[.CodeMirror-lines]:py-3
                        children-[.CodeMirror]:transition
                            children-[.CodeMirror]:h-auto
                            children-[.CodeMirror]:rounded-md
                        children-[.CodeMirror]:border
                            children-[.CodeMirror]:border-gray-400
                        children-[.CodeMirror-focused]:ring
                            children-[.CodeMirror-focused]:ring-size-[0.25rem]
                            children-[.CodeMirror-focused]:ring-blue-300
                            children-[.CodeMirror-focused]:border-blue-300
                    "
                >
                    <textarea class="form-input" ref="input" />
                </div>
            `,
        };

        const FileInput = {
            components: {
                Btn,
                Icon,
                Lazy,
                Spinner,
            },
            emits: ['update:files', 'values'],
            props: {
                files: Array,
                limit: Number,
            },
            setup(props, { emit }) {
                const { files, limit } = toRefs(props);

                const supportsDragAndDrop = (() => {
                    const div = document.createElement('div');
                    return ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div);
                })();

                const documentUnderDrag = ref(false);
                const domDropZone = ref();
                const dragAndDropInputPlaceholder = ref();
                const dropZoneUnderDrag = ref(false);

                const processingFiles = ref(0);
                const processFiles = (files) => {
                    files = [...(files || [])].slice(0, limit.value || 1);
                    processingFiles.value = files.length;
                    return Promise.all(
                        files.map((file) =>
                            checkFile(file).then((result) => {
                                --processingFiles.value;
                                return result;
                            }))
                    ).then((result) => {
                        processingFiles.value = 0;
                        return result;
                    });
                };

                const internalFiles = ref([]);
                processFiles(files.value).then((files) => internalFiles.value = files);
                watch(files, (files) => processFiles(files).then((files) => internalFiles.value = files));
                watch(internalFiles, (files) => {
                    emit('update:files', files.map((file) => file.file));
                    emit('values', files);
                });

                const acceptEventFiles = (f) => {
                    processFiles(f).then((files) => internalFiles.value = files);
                };

                if (supportsDragAndDrop) {
                    watch(domDropZone, (dom) => {
                        ['drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop']
                            .forEach((eventName) => dom.addEventListener(eventName, (e) => {
                                e.preventDefault();
                                e.stopPropagation();
                            }));

                        ['dragover', 'dragenter']
                            .forEach((eventName) => dom.addEventListener(eventName, () => {
                                dropZoneUnderDrag.value = true;
                            }));

                        ['dragleave', 'dragend', 'drop']
                            .forEach((eventName) => dom.addEventListener(eventName, () => {
                                dropZoneUnderDrag.value = false;
                            }));

                        ['drop']
                            .forEach((eventName) => dom.addEventListener(eventName, (event) => {
                                acceptEventFiles(event.dataTransfer.files);
                            }));
                    });

                    const html = __html();
                    const setDrag = () => { documentUnderDrag.value = true; };
                    const setDrop = () => { documentUnderDrag.value = false; };

                    onMounted(() => {
                        ['dragover', 'dragenter'].forEach((eventName) => html.addEventListener(eventName, setDrag));
                        ['dragleave', 'dragend', 'drop'].forEach((eventName) => html.addEventListener(eventName, setDrop));
                    });

                    onBeforeUnmount(() => {
                        ['dragover', 'dragenter'].forEach((eventName) => html.removeEventListener(eventName, setDrag));
                        ['dragleave', 'dragend', 'drop'].forEach((eventName) => html.removeEventListener(eventName, setDrop));
                    });
                }

                return {
                    internalFiles,
                    internalFilesSize: computed(() => formatBytes(internalFiles.value.reduce((b, a) => a.file.size + b, 0))),

                    removeFile(index) {
                        internalFiles.value.splice(index, 1);
                    },

                    acceptInputFile(event) {
                        acceptEventFiles(event.target.files);
                    },

                    documentUnderDrag,
                    domDropZone,
                    dragAndDropInputPlaceholder,
                    dropZoneUnderDrag,
                    processingFiles,
                    supportsDragAndDrop,
                };
            },
            template: `
                <div v-if="supportsDragAndDrop">
                    <input
                        @change="acceptInputFile($event)"
                        class="form-file"
                        ref="dragAndDropInputPlaceholder"
                        type="file"
                        uno-layer-default="opacity-0 invisible w-px h-px absolute"
                    >

                    <div
                        ref="domDropZone"

                        :class="{
                            'rounded-md transition min-h-40 border-4 flex': true,
                            'border-dashed': !dropZoneUnderDrag,
                            'bg-blue-50': dropZoneUnderDrag,
                            'border-blue-600': documentUnderDrag || dropZoneUnderDrag,
                            'border-gray-400': !(documentUnderDrag || dropZoneUnderDrag),
                        }"
                    >
                        <div
                            uno-layer-default="flex justify-center items-center w-full space-x-2"
                            v-if="processingFiles"
                        >
                            <Spinner scale="2" />
                            <span>Processing {{ processingFiles }} file(s).</span>
                        </div>

                        <div
                            uno-layer-default="flex flex-col w-full p-1 space-y-1"
                            v-else-if="internalFiles.length"
                        >
                            <slot :files="internalFiles">
                                <div uno-layer-default="overflow-hidden -mx-1 px-1 flex-grow">
                                    <div
                                        uno-layer-default="overflow-x-auto flex flex-no-wrap h-full space-x-2 justify-center"
                                        :class="{
                                            'items-center': internalFiles.length === 1,
                                        }"
                                    >
                                        <slot name="file" v-for="(file, index) in internalFiles" :file="file" :index="index" :removeFile="removeFile">
                                            <div uno-layer-default="flex flex-col items-center space-y-1">
                                                <Lazy v-if="file.dataUrl">
                                                    <img
                                                        :src="file.dataUrl"
                                                        uno-layer-default="h-12 border border-2 border-gray-200 rounded-lg"
                                                    >
                                                </Lazy>

                                                <Icon
                                                    class="h-12 w-12"
                                                    name="document"
                                                    v-else
                                                />

                                                <div uno-layer-default="flex items-start space-x-1">
                                                    <Btn
                                                        @click="removeFile(index)"
                                                        class="select-none p-0 rounded-full"
                                                    >
                                                        <Icon
                                                            class="w-4 text-red-600"
                                                            name="x-circle"
                                                        />
                                                    </Btn>
                                                    <span>{{ file.file.name }}</span>
                                                </div>
                                                <small>{{ file.size }}</small>
                                            </div>
                                        </slot>
                                    </div>
                                </div>
                            </slot>

                            <div uno-layer-default="flex items-end">
                                <Btn
                                    @click="() => internalFiles = []"
                                    class="p-1 flex space-x-1 items-center"
                                    v-if="internalFiles.length > 1"
                                >
                                    <Icon name="x-circle" class="w-6" />
                                    <span>Clear</span>
                                </Btn>
                                <Btn
                                    @click="dragAndDropInputPlaceholder.click()"
                                    class="p-1 flex space-x-1 items-center"
                                >
                                    <Icon name="document-add" class="w-6" />
                                    <span>Replace file</span>
                                </Btn>

                                <div uno-layer-default="flex-grow" />

                                <small v-if="internalFiles.length > 1">
                                    Total
                                    {{ internalFilesSize }}
                                    bytes in
                                    {{ internalFiles.length }}
                                    file(s).
                                </small>
                            </div>
                        </div>

                        <div
                            @click="dragAndDropInputPlaceholder.click()"
                            uno-layer-default="flex flex-col justify-center items-center w-full space-y-2"
                            v-else
                        >
                            <Icon name="document-add" class="w-12" />
                            <span>Browse file to upload</span>
                            <span>or drag and drop</span>
                        </div>
                    </div>
                </div>

                <input
                    @change="acceptInputFile($event)"
                    class="form-file"
                    type="file"
                    v-else
                >
            `,
        };

        const TableDisplay = {
            components: {
                Btn,
                Icon,
                SimpleObjectDisplay,
                TableCell: {
                    template: `<td
                        uno-layer-base="table-cell parent-hover:bg-gray-50 parent-hover:border-t-gray-200 border-t border-gray-200"
                        ss-xs="block whitespace-normal"
                    ><slot /></td>`,
                },
                TableHeading: {
                    template: `<th
                        uno-layer-base="table-cell border-b border-gray-200 font-medium sticky top-0 bg-gray-100 whitespace-nowrap"
                        ss-xs="hidden"
                    ><slot /></th>`,
                },
                TableRow: {
                    template: `<tr
                        uno-layer-base="table-row"
                        ss-xs="block relative"
                    ><slot /></tr>`,
                },
            },
            props: {
                rowKey: String,
                list: Array,
                spacious: Boolean,
            },
            setup(props) {
                const {
                    rowKey,
                    list,
                } = toRefs(props);

                const listArray = computed(() => list.value == null || !isArray(list.value) ? [] : list.value);
                const perPage = ref(10);
                const pageNumber = ref(1);
                const lastPage = computed(() => {
                    const ceil = Math.ceil(listArray.value.length / perPage.value);
                    return parseInt(ceil.toFixed(0), 10);
                });

                return {
                    listArray,
                    pageNumber,

                    showPage(page) {
                        if (lastPage.value <= 1 || page <= 0) {
                            pageNumber.value = 1;
                        }
                        else if (page > lastPage.value) {
                            pageNumber.value = lastPage.value;
                        }
                        else {
                            pageNumber.value = page;
                        }
                    },

                    pageWindow: computed(() => {
                        return pagerUrlWindow(lastPage.value, pageNumber.value);
                    }),

                    paged: computed(() => {
                        return listArray.value.slice(
                            (pageNumber.value - 1) * perPage.value,
                            pageNumber.value * perPage.value
                        );
                    }),

                    firstItem: computed(() => {
                        return listArray.value[0] ?? {};
                    }),

                    pageKey(row, index) {
                        const rkv = rowKey.value;

                        if (rkv != null && (rkv in row)) {
                            return row[rkv];
                        }

                        return index;
                    },
                };
            },
            template: `
                <div class="overflow-x-auto">
                    <table
                        uno-layer-default="table"
                        ss-xs="block min-w-full"
                    >
                        <thead
                            uno-layer-default="table-header-group text-sm tracking-wider bg-gray-100"
                            ss-xs="block"
                        >
                            <TableRow
                                class="relative"
                                :class="{
                                    'children:py-4 children:px-4': spacious,
                                    'children:py-1 children:px-3': !spacious,
                                    'z-1': true,
                                }"
                            >
                                <slot name="headings" :headings="firstItem">
                                    <TableHeading
                                        v-for="(value, key) in firstItem"
                                    >
                                        {{ key }}
                                    </TableHeading>
                                </slot>
                            </TableRow>
                        </thead>

                        <tbody
                            uno-layer-default="table-row-group"
                            ss-xs="block"
                        >
                            <TableRow v-for="(row, rowIndex) in paged" :key="pageKey(row, rowIndex)"
                                class="parent"
                                :class="{
                                    'children:py-4 children:px-4': spacious,
                                    'children:py-2 children:px-3': !spacious,
                                    'ss-xs:pt-[3rem]': true,
                                }"
                            >
                                <slot name="row" :row="row" :index="rowIndex">
                                    <TableCell
                                        v-for="(value) in (row || {})"
                                    ><SimpleObjectDisplay :value="value" /></TableCell>
                                </slot>
                            </TableRow>
                        </tbody>
                    </table>
                </div>
                <div uno-layer-default="flex items-center" class="[border-top-width:1px]" v-if="listArray.length > 0">
                    <nav uno-layer-default="flex-grow max-w-full overflow-hidden h-10">
                        <ul
                            uno-layer-base="divide-x"
                            uno-layer-default="overflow-x-auto whitespace-nowrap flex h-24 divide-gray-200"
                        >
                            <li uno-layer-default="h-10 flex items-stretch">
                                <Btn
                                    @click="showPage(pageNumber - 1)"
                                    class="whitespace-nowrap px-4 rounded-none flex items-center min-w-[4.5ch]"
                                >
                                    <Icon name="chevron-left" class="w-4" />
                                </Btn>
                            </li>
                            <li v-for="page in pageWindow"
                                uno-layer-default="h-10 flex items-stretch"
                            >
                                <span
                                    class="whitespace-nowrap px-4 rounded-none flex items-center min-w-[4.5ch]"
                                    v-if="page === '...'"
                                >
                                    <Icon name="dots-horizontal" class="w-4" />
                                </span>
                                <Btn
                                    @click="showPage(page)"
                                    class="whitespace-nowrap px-4 rounded-none flex items-center min-w-[4.5ch]"
                                    v-else
                                    :class="[
                                        pageNumber === page ? 'font-semibold bg-blue-200 cursor-auto' : '',
                                    ]"
                                >
                                    {{ page }}
                                </Btn>
                            </li>
                            <li uno-layer-default="h-10 flex items-stretch">
                                <Btn
                                    @click="showPage(pageNumber + 1)"
                                    class="whitespace-nowrap px-4 rounded-none flex items-center min-w-[4.5ch]"
                                >
                                    <Icon name="chevron-right" class="w-4" />
                                </Btn>
                            </li>
                        </ul>
                    </nav>
                </div>
            `,
        };

        const SelectInputButton = {
            name: 'SelectInputButton',
            components: {
                Icon,
                PopperButtoned,
                SelectList,
            },
            emits: SelectList.emits,
            props: SelectList.props,
            setup(props) {
                const selectList = ref();

                watch(selectList, () => {
                    const el = selectList.value?.$el;
                    if (el) {
                        const focusable = Array.from(el.querySelectorAll(FOCUSABLE_SELECTORS));
                        if (focusable.length) {
                            focusable[0].focus();
                        }
                    }
                });

                return {
                    selectList,

                    currentLabel: computed(() => {
                        const { options, labelKey, missingModel, value, valueKey } = props;

                        let v = missingModel?.label ?? missingModel?.[labelKey];
                        if (options.length) {
                            const found = options.find((item) => item[valueKey] === value);
                            v = found?.[labelKey] ?? v;
                        }

                        return v;
                    }),
                };
            },
            template: `
                <PopperButtoned
                    button-class="
                        border rounded-md px-3 w-full justify-between
                        hover:bg-inherit
                        ss-xl-w-3/4
                        leading-[1.125]
                    "
                >
                    <template #button>
                        <span v-text="currentLabel" />
                        <Icon name="selector" class="w-4" />
                    </template>

                    <template #popper="{ target, updateToggle }">
                        <SelectList
                            ref="selectList"
                            ${Object.keys(SelectList.props).map((k) => `:${k}="${k}"`).join(' ')}
                            ${SelectList.emits.map((k) => `@${k}="$emit('${k}', $event)"`).join(' ')}
                            @wantsClose="() => updateToggle()"
                        />
                    </template>
                </PopperButtoned>
            `,

        };

        const MarkdownPreview = {
            props: {
                inline: Boolean,
                text: String,
            },
            setup(props) {
                return {
                    markdown: computed(() => {
                        const v = props.text == null ? '' : props.text;
                        return props.inline ? marked.parseInline(v) : marked.parse(v);
                    }),
                };
            },
            template: `
                <div class="prose" v-html="markdown" />
            `,
        };

        const Panel = {
            props: {
                panel: Object,
            },
            template: `
                <div uno-layer-default="space-y-2">
                    <slot name="heading">
                        <h3
                            uno-layer-default="text-xl"
                            v-if="panel && panel.name"
                            v-text="panel.name"
                        />
                    </slot>
                    <div uno-layer-default="border rounded-lg shadow bg-white border-gray-200">
                        <div
                            uno-layer-base="divide-y"
                            uno-layer-default="divide-gray-200"
                        >
                            <slot />
                        </div>
                    </div>
                </div>
            `,
        };

        const FormGroup = {
            props: {
                field: Object,
                text: String,
                helpText: String,
                withoutLabel: Boolean,
            },
            template: `
                <div
                    uno-layer-default="mx-6 py-4"
                    ss-xs="mx-4"
                >
                    <div
                        uno-layer-default="flex items-baseline"
                        ss-xs="block"
                    >
                        <slot name="label">
                            <div
                                v-if="!withoutLabel"
                                uno-layer-default="w-1/4 shrink-0 pr-2 pb-0"
                                ss-xs="w-full pb-2"
                            >
                                <div v-text="field && field.name" />
                            </div>
                        </slot>
                        <div
                            uno-layer-default="flex flex-col"
                            ss-xs="w-full"
                            :class="{
                                'w-3/4': !withoutLabel,
                                'w-full': withoutLabel,
                            }"
                        >
                            <div
                                v-if="text"
                                v-text="text"
                            />
                            <template v-else>
                                <slot>
                                    <div><slot name="text"></slot></div>
                                </slot>
                            </template>
                            <div v-if="helpText" v-html="helpText"></div>
                        </div>
                    </div>
                </div>
            `,
        };

        const DynamicField = {
            components: {
                ComponentLoader,
            },
            props: {
                action: Object,
                field: Object,
                resource: Object,
            },
            setup(props) {
                const { app, configured } = window.__avon_runtime;
                const { field } = toRefs(props);
                const componentName = ref();

                const fieldBaseUrl = `${__avon.baseUrl}/api/field/${field.value.component}`;
                let currentSignature = '';

                const viewProcessor = (response) => {
                    if (! response?.data) {
                        throw new Error;
                    }

                    const { component, payload, payloadSignature } = response.data || {};

                    if (!payloadSignature) {
                        throw new Error;
                    }

                    if (currentSignature === payloadSignature) {
                        return false;
                    }

                    const cName = `${component}-${payloadSignature}`;

                    try {
                        new Function('app', 'context', 'options', payload)(
                            app,
                            {
                                __avon,
                                __tmp_appName,
                                createFormToolComponent,
                                componentName: cName,
                                fieldBaseUrl,
                            },
                            configured,
                        );
                    } catch (error) {
                        if (currentSignature === '') {
                            throw error;
                        }

                        return false;
                    }

                    currentSignature = payloadSignature;

                    componentName.value = cName;

                    return true;
                };

                let viewResolver = new Promise(noop);
                (function reloadView() {
                    viewResolver = axiosInternal({
                        method: 'get',
                        url: `${fieldBaseUrl}/view`,
                        params: currentSignature ? { signature: currentSignature } : {},
                    })
                        .catch(noop)
                        .then(viewProcessor)
                        .then(() => timeoutRaf(reloadView, globalIdle.value ? 30000 : 5000));
                })();

                return {
                    componentName,
                    loader: {
                        resolver: () => viewResolver,
                    },
                };
            },
            template: `
                <ComponentLoader v-bind="loader">
                    <component
                        :is="componentName"
                        v-if="componentName"
                        v-bind="{ ...$attrs, ...$props }"
                    />
                </ComponentLoader>
            `,
        };

        const prepareFieldsCollection = (fields, panels, detailView) => {
            const sections = {};

            (fields ?? []).forEach((field) => {
                // prepareFieldComponent(field);

                const { attribute, name: fieldName, panel, relation } = field;
                if (relation) {
                    const fieldKey = detailView ? `index.${attribute}.${fieldName}` : `index.${attribute}`;
                    return (sections[fieldKey] = {
                        isResourceTool: false,
                        isPanel: false,
                        data: field,
                    });
                }

                const fieldPanel = panels.find(({ name }) => name === panel) || panels[0];
                const { isResourceTool, name: panelName } = fieldPanel;
                const panelKey = `panel.${panelName}`;
                if (!(panelKey in sections)) {
                    sections[panelKey] = {
                        isResourceTool,
                        isPanel: true,
                        data: fieldPanel,
                    };
                }

                if (!('fields' in fieldPanel)) {
                    fieldPanel.fields = [];
                }

                fieldPanel.fields.push(field);
            });

            let toolbar = true;
            (panels || []).forEach((panel) => {
                panel._ = reactive({ toolbar });
                toolbar = false;

                const { isResourceTool, name: panelName } = panel;
                const panelKey = `panel.${panelName}`;
                if (!(panelKey in sections)) {
                    sections[panelKey] = {
                        isResourceTool,
                        isPanel: false,
                        data: panel,
                    };
                }
            });

            return sections;
        };

        const ResourceModelHeading = {
            props: { resource: Object },
            template: `
                <h2
                    uno-layer-default="text-2xl"
                    v-if="resource.title"
                    v-text="resource.title"
                />
                <h3
                    uno-layer-default="text-xl"
                    v-if="resource.subtitle"
                    v-text="resource.subtitle"
                />
            `,
        };

        const ResourceModelPreview = {
            components: {
                Btn,
                Lazy,
            },
            props: {
                model: Object,
                noSelect: Boolean,
                plainText: Boolean,
            },
            template: `
                <div
                    uno-layer-default="inline-flex items-center space-x-2"
                    v-if="model && model.id"
                    :class="{
                        'select-none': noSelect
                    }"
                >
                    <Lazy
                        class="shrink-0"
                        v-if="model.avatarUrl"
                    >
                        <img
                            :src="model.avatarUrl"
                            uno-layer-default="rounded-full border border-gray-200 w-8 h-8"
                        />
                    </Lazy>
                    <component
                        :is="plainText ? 'span' : 'Btn'"
                        :href="model.url"
                        :class="{
                            'leading-snug flex-col items-start': true,
                            'flex': plainText,
                            'p-1 font-semibold text-blue-600 m--1': !plainText,
                        }"
                    >
                        <span v-text="model.title ?? '${EM_DASH}'" />
                        <template v-if="model.subtitle">
                            <small v-text="model.subtitle" />
                        </template>
                    </component>
                </div>

                <span
                    v-else
                    v-text="'${EM_DASH}'"
                    :class="{
                        'select-none': noSelect
                    }"
                />
            `,
        };

        const indexFieldComponents = {
            BadgeField: {
                props: { field: Object },
                template: `
                    <span>
                        <template v-if="field.value == null">${EM_DASH}</template>
                        <span
                            :class="field.typeClass"
                            uno-layer-default="border rounded-lg text-sm font-semibold py-1 px-2 inline-block min-w-8 text-center"
                            v-else
                            v-text="field.label"
                        />
                    </span>
                `,
            },
            BooleanField: {
                components: {
                    Icon,
                },
                props: { field: Object },
                setup: (props) => ({
                    value: computed(() => props.field.displayValue ?? props.field.value),
                }),
                template: `
                    <span>
                        <template v-if="value == null">${EM_DASH}</template>
                        <Icon v-else
                            :name="value ? 'check-circle' : 'x-circle'"
                            :class="value ? 'text-green-600' : 'text-red-600'"
                            class="w-6 inline-block"
                        />
                    </span>
                `,
            },
            BooleanGroupField: {
                components: {
                    Icon,
                },
                props: { field: Object },
                setup: (props) => ({
                    entries: computed(() => Object.entries((props.field.displayValue ?? props.field.value) || {})),
                }),
                template: `
                    <span>
                        <span
                            uno-layer-default="italic"
                            v-if="entries.length === 0"
                        >{{ field.noValueText }}</span>
                        <span v-else v-for="([rowKey, rowValue], index) in entries">
                            <Icon
                                class="inline h-[1em] mr-1"
                                :class="rowValue ? 'text-green-400' : 'text-red-400'"
                                :name="rowValue ? 'check-circle' : 'x-circle'"
                            />
                            <span
                                v-if="rowValue"
                                v-text="rowKey"
                            />
                            <s v-else><span
                                uno-layer-default="text-gray-500"
                                v-text="rowKey"
                            /></s>
                            <template v-if="index + 1 < entries.length">, </template>
                        </span>
                    </span>
                `,
            },
            CodeField: {
                components: {
                    Btn,
                    CodeMirrorInput,
                },
                props: { field: Object },
                setup: () => ({
                    toggle: ref(false),
                }),
                template: `
                    <div>
                        <CodeMirrorInput
                            :modelValue="field?.displayValue ?? field?.value"
                            :readonly="true"
                            class="mb-4"
                            v-if="toggle"
                        />

                        <Btn
                            @click="toggle = !toggle"
                            class="p-0 font-semibold text-blue-600 -my-2"
                        >{{ toggle ? 'Hide' : 'Show' }}</Btn>
                    </div>
                `,
            },
            CurrencyField: {
                components: {
                    AutonumericDisplay,
                    Icon,
                },
                props: { field: Object },
                setup: (props) => ({
                    digitGroupSeparator: computed(() => (props.field && props.field.digitGroupSeparator) || ','),
                    decimalCharacter: computed(() => (props.field && props.field.decimalCharacter) || ','),
                    value: computed(() => props.field.displayValue ?? props.field.value),
                }),
                template: `
                    <span>
                        <template v-if="value == null">${EM_DASH}</template>
                        <AutonumericDisplay v-else
                            #toggle
                            :decimalCharacter="field.decimalCharacter || '.'"
                            :digitGroupSeparator="field.digitGroupSeparator || ','"
                            :prefix="field.prefix || ''"
                            :suffix="field.sufix || ''"
                            :value="value"
                            class="tabular-nums"
                        >
                            <Icon
                                class="w-4 ml-1 inline-block cursor-pointer"
                                name="switch-horizontal"
                            />
                        </AutonumericDisplay>
                    </span>
                `,
            },
            DateTimeField: {
                components: {
                    DayjsDisplay,
                    Icon,
                },
                props: { field: Object },
                setup(props) {
                    const { field } = toRefs(props);
                    const useTime = computed(() => field.value.useTime);
                    return {
                        format: computed(
                            () =>
                                field.value.format ||
                                (useTime.value ? 'dddd, D MMMM YYYY, HH:mm:ss' : 'dddd, D MMMM YYYY')
                        ),
                        useTime,
                    };
                },
                template: `
                    <DayjsDisplay
                        #toggle
                        :displayTime="useTime"
                        :format="format"
                        :timezone="field.timezone"
                        :value="field.displayValue ?? field.value"
                    >
                        <Icon
                            class="w-4 ml-1 inline-block cursor-pointer"
                            name="switch-horizontal"
                        />
                    </DayjsDisplay>
                `,
            },
            DownloadField: {
                components: {
                    Btn,
                    Icon,
                },
                props: {
                    field: Object,
                    resource: Object,
                },
                setup(props) {
                    const { field, resource } = toRefs(props);

                    console.log({
                        resource:resource.value,
                        links:field.value.links,
                    });

                    return {
                        links: computed(() => isArray(field.value.links) ? field.value.links : []),

                        downloadUrl: (key) => [
                            `${__avon.baseUrl}`,
                            'api',
                            'resource',
                            `${resource.value.resourceName}`,
                            'detail',
                            `${resource.value._.resourceId}`,
                            'field',
                            `${field.value.attribute}?download=${key}`,
                        ].join('/'),
                    };
                },
                template: `
                    <div uno-layer-default="flex">
                        <a
                            v-for="link in links"
                            :href="downloadUrl(link.key)"
                        >{{ link.name }}</a>
                    </div>
                `,
            },
            FileField: {
                components: {
                    Btn,
                    Icon,
                    Lazy,
                },
                props: {
                    field: Object,
                    resource: Object,
                    showPreview: Boolean,
                },
                setup(props) {
                    const { field, resource, showPreview } = toRefs(props);

                    return {
                        downloadUrl: computed(() => [
                            `${__avon.baseUrl}`,
                            'api',
                            'resource',
                            `${resource.value.resourceName}`,
                            'detail',
                            `${resource.value._.resourceId}`,
                            'field',
                            `${field.value.attribute}`,
                        ].join('/')),

                        shouldShowImage: computed(() =>
                            showPreview.value
                                ? field.value.showPreview || field.value.showThumbnail
                                : field.value.showThumbnail
                        ),

                        imageAttributes: computed(() => {
                            const usePreview = showPreview.value && field.value.showPreview && field.value.previewUrl;
                            return {
                                src: usePreview ? field.value.previewUrl || field.value.thumbnailUrl : field.value.thumbnailUrl,
                                class: {
                                    'h-8 w-8': !usePreview,
                                    'max-w-[50vw]': usePreview,
                                    'shadow-sm': true,
                                    [field.value.thumbnailBorderStyle ?? 'rounded-lg']: !usePreview,
                                },
                            };
                        }),
                    };
                },
                template: `
                    <div
                        uno-layer-default="flex"
                        :class="{
                            'space-y-2 flex-col': showPreview,
                            'space-x-2 items-center': !showPreview,
                        }"
                    >
                        <div v-if="field.downloadable">
                            <Btn
                                :href="downloadUrl"
                                class="p-1 inline-flex space-x-1 items-center justify-start"
                                download="download"
                                target="_blank"
                            >
                                <Icon name="download" class="w-4" />
                                <span>Download file</span>
                            </Btn>
                        </div>

                        <Lazy v-if="shouldShowImage">
                            <img v-bind="imageAttributes">
                        </Lazy>

                        <span
                            v-else
                            v-text="field.displayValue ?? field.value ?? '${EM_DASH}'"
                        />
                    </div>
                `,
            },
            HeadingField: {
                template: ``,
            },
            HiddenField: {
                template: ``,
            },
            IndexField: {
                // components: { ResourceTable },
                props: { field: Object },
                setup: (props) => ({
                    resources: computed(() => (props.field.value ? props.field.value.resources : [])),
                }),
                template: `
                    <div
                        v-if="resources && resources.length"
                        uno-layer-default="align-middle inline-block w-full rounded-lg border border-gray-200 shadow overflow-hidden"
                    >
                        <div uno-layer-default="overflow-auto bg-white">
                            <ResourceTable
                                :resources="resources"
                                class="whitespace-nowrap"
                            />
                        </div>
                    </div>
                `,
            },
            InputField: {
                components: { TextInput },
                props: { field: Object },
                template: `
                    <TextInput
                        :readonly="field.readonly"
                        :value="field.value"
                        class="w-full"
                        ss-xl="w-3/4"
                    />
                `,
            },
            KeyValueField: {
                components: {
                    Btn,
                    // ResourceTable,
                },
                props: { field: Object },
                setup(props) {
                    const { field } = toRefs(props);
                    const value = computed(() => field.value.displayValue || field.value.value || []);
                    return {
                        value,
                        toggle: ref(false),
                        resources: computed(() => Object.entries(value).map(([key, value]) => ({
                            fields: [
                                {
                                    component: 'text-field',
                                    name: field.value.keyLabel,
                                    value: key,
                                },
                                {
                                    component: 'text-field',
                                    name: field.value.valueLabel,
                                    value,
                                },
                            ],
                        }))),
                    };
                },
                template: `
                    <span>
                        <template v-if="value.length === 0">${EM_DASH}</template>
                        <div
                            v-else
                            uno-layer-default="flex flex-col"
                        >
                            <div
                                uno-layer-default="align-middle inline-block w-full rounded-lg border border-gray-200 shadow overflow-hidden mb-4"
                                v-if="toggle"
                            >
                                <div uno-layer-default="overflow-auto bg-white">
                                    <ResourceTable
                                        :resources="resources"
                                        class="whitespace-nowrap"
                                    />
                                </div>
                            </div>

                            <div>
                                <Btn
                                    @click="toggle = !toggle"
                                    class="p-0 font-semibold text-blue-600 -my-2"
                                >{{ toggle ? 'Hide' : 'Show' }}</Btn>
                            </div>
                        </div>
                    </span>
                `,
            },
            LineField: {
                props: { field: Object },
                setup: (props) => ({
                    lineClass: computed(() => {
                        const { asBase, asHeading, asSmall, asSubTitle, extraClasses } = props.field || {};
                        return {
                            // 'leading-snug': true,
                            'text-base': asBase,
                            'text-lg font-medium tracking-tight text-gray-900': asHeading,
                            'text-sm tracking-wide': asSmall,
                            'text-base font-semibold text-gray-700': asSubTitle,
                            [extraClasses]: extraClasses,
                        };
                    }),
                }),
                template: `
                    <div
                        :class="lineClass"
                        :title="field.indexName"
                        v-text="field.displayValue ?? field.value ?? '${EM_DASH}'"
                    />
                `,
            },
            LocationField: {
                components: {
                    Btn,
                    Icon,
                },
                props: { field: Object },
                setup: (props) => ({
                    mapUrl: computed(() => {
                        const value = props.field.displayValue || props.field.value || '';
                        if (value) {
                            const [lat, lon] = value.split(',');
                            return 'https://www.openstreetmap.org/?mlat=' + lat + '&mlon=' + lon;
                        }
                    }),
                }),
                template: `
                    <span>
                        <Btn v-if="mapUrl"
                            :href="mapUrl"
                            class="leading-snug inline-block text-blue-600 hover:text-blue-700"
                        >
                            <Icon name="location-marker" class="w-5 inline-block" />
                        </Btn>
                        <template v-else>${EM_DASH}</template>
                    </span>
                `,
            },
            MarkdownField: {
                components: {
                    Btn,
                    MarkdownPreview,
                },
                props: { field: Object },
                setup: (props) => ({
                    markdownish: computed(() => {
                        const v = props.field.displayValue ?? props.field.value;
                        if (v == null) return ZWJ;
                        if (v === '') return 'kosong';
                        return v;
                    }),
                    toggle: ref(false),
                }),
                template: `
                    <div>
                        <div
                            uno-layer-default="mb-4 rounded-lg"
                            v-if="toggle"
                        >
                            <MarkdownPreview :text="markdownish" />
                        </div>

                        <Btn
                            @click="toggle = !toggle"
                            class="p-0 font-semibold text-blue-600 -my-2"
                        >{{ toggle ? 'Hide' : 'Show' }}</Btn>
                    </div>
                `,
            },
            PasswordField: {
                props: { field: Object },
                setup: () => ({
                    password: '123456'.replace(/./g, PASSWORD_BULLET),
                }),
                template: `
                    <span>
                        <template v-if="field == null">${EM_DASH}</template>
                        <span
                            uno-layer-default="cursor-default font-mono"
                            v-else
                            v-text="password"
                        />
                    </span>
                `,
            },
            PlaceField: {
                props: { field: Object },
                template: `{{ field.displayValue ?? field.value ?? '${EM_DASH}' }}`,
            },
            SelectField: {
                props: { field: Object },
                setup(props) {
                    const activeOption = computed(() => {
                        const { value, options } = props.field || {};
                        return options.find((option) => option.key === value);
                    });
                    return {
                        activeOption,

                        displayValue: computed(() => {
                            const { displayValue = null, displayUsingLabels } = props.field || {};

                            if (displayValue) {
                                return displayValue;
                            }

                            const option = activeOption.value;
                            return option
                                ? (displayUsingLabels
                                    ? option.label
                                    : option.key
                                ) : EM_DASH;
                        }),
                    };
                },
                template: `
                    <div :class="{
                        'border rounded-lg text-sm font-semibold py-1 px-2 inline-block min-w-8 text-center': activeOption && field.displayAsBadge,
                    }">
                        {{ displayValue }}
                    </div>
                `,
            },
            StackField: {
                // components: { IndexFieldProxy },
                props: {
                    field: Object,
                    resource: Object,
                },
                template: `
                    <IndexFieldProxy
                        :field="stackField"
                        :resource="resource"
                        v-for="stackField in field.displayValue ?? field.value"
                    />
                `,
            },
            StatusField: {
                components: { Spinner },
                props: { field: Object },
                setup(props) {
                    const { field } = toRefs(props);
                    const value = computed(() => props.field.displayValue ?? props.field.value);
                    return {
                        failedState: computed(() => (props.field.failedTexts || []).includes(value.value)),
                        loadingState: computed(() => (props.field.loadingTexts || []).includes(value.value)),
                        value,
                    };
                },
                template: `
                    <span>
                        <Spinner v-if="loadingState" />
                        <span
                            v-else
                            v-text="value ?? '${EM_DASH}'"
                            :class="{
                                'leading-snug': true,
                                'text-red-600': failedState,
                            }"
                        />
                    </span>
                `,
            },
            TextField: {
                components: {
                    Btn,
                    Icon,
                },
                props: { field: Object },
                setup(props) {
                    const value = computed(() => props.field.displayValue ?? props.field.value);
                    const copyStatus = ref(0);

                    return {
                        value,
                        copyIcon: computed(() => (['clipboard', 'check', 'x',][copyStatus.value])),
                        copyToClipboard() {
                            navigator.clipboard
                                .writeText(value.value)
                                .then(
                                    () => copyStatus.value = 1,
                                    () => copyStatus.value = 2
                                )
                                .then(() => setTimeout(() => copyStatus.value = 0, 3000));
                        },
                    };
                },
                template: `
                    <span>
                        <div v-if="field.asHtml" v-html="value"></div>
                        <small
                            uno-layer-default="select-none"
                            v-else-if="value === ''"
                        ><i>kosong</i></small>
                        <span v-else-if="value">
                            <Btn
                                :disabled="copyStatus"
                                @click="copyToClipboard"
                                class="float-right p-2 my--2 font-semibold"
                                v-if="field.copyable"
                            >
                                <Icon :name="copyIcon" class="w-4 inline-block" />
                            </Btn>{{
                                value
                            }}</span>
                        <template v-else>${EM_DASH}</template>
                    </span>
                `,
            },
            TextareaField: {
                components: {
                    Btn,
                },
                props: { field: Object },
                setup: (props) => ({
                    toggle: ref(false),
                    value: computed(() => props.field.displayValue ?? props.field.value),
                }),
                template: `
                    <small
                        uno-layer-default="select-none"
                        v-if="value === ''"
                    ><i>kosong</i></small>
                    <div v-else-if="value">
                        <div
                            uno-layer-default="mb-4 rounded-lg"
                            v-if="toggle"
                            v-text="value"
                        />

                        <Btn
                            @click="toggle = !toggle"
                            class="p-0 font-semibold text-blue-600 -my-2"
                        >{{ toggle ? 'Hide' : 'Show' }}</Btn>
                    </div>
                    <template v-else>${EM_DASH}</template>
                `,
            },
            // relations
            BelongsToField: {
                components: { ResourceModelPreview },
                props: { field: Object },
                template: `<ResourceModelPreview :model="field.value" />`,
            },
            DynamicField,
        };

        const baseDetailFieldProxy = (Field) => ({
            components: {
                Field,
                FormGroup,
            },
            props: {
                field: Object,
                resource: Object,
            },
            template: `
                <FormGroup :field="field" #text>
                    <Field
                        :field="field"
                        :resource="resource"
                    />
                </FormGroup>
            `,
        });

        const HeadingField = {
            props: { field: Object },
            template: `
                <div
                    uno-layer-default="py-4 px-6 -mb-px bg-gray-200 text-sm font-semibold"
                    uno-layer-extra="border-b"
                    ss-xs="px-4"
                    v-text="field.name"
                />
            `,
        };

        const HiddenField = {
            props: { field: Object },
            template: `<input hidden type="hidden" :value="field.value" />`,
        };

        const detailFieldComponents = {
            ...Object.fromEntries(
                Object.entries(indexFieldComponents).map(([component, definition]) => [
                    component,
                    baseDetailFieldProxy(definition),
                ])
            ),

            FileField: {
                // components: { DetailFieldProxy },
                components: {
                    FormGroup,
                    IndexFileField: indexFieldComponents.FileField,
                },
                props: {
                    field: Object,
                    resource: Object,
                },
                template: `
                    <FormGroup :field="field" #text>
                        <IndexFileField
                            :field="field"
                            :resource="resource"
                            :showPreview="true"
                        />
                    </FormGroup>
                `,
            },
            HeadingField,
            HiddenField,
            LineField: {
                components: {
                    FormGroup,
                    IndexLineField: indexFieldComponents.LineField,
                },
                props: {
                    field: Object,
                    resource: Object,
                },
                template: `
                    <FormGroup
                        #text
                        :field="field"
                        :withoutLabel="field.withoutLabel"
                    >
                        <IndexLineField
                            :field="field"
                            :resource="resource"
                            :showPreview="true"
                        />
                    </FormGroup>
                `,
            },
            LocationField: {
                components: {
                    Btn,
                    FormGroup,
                    Icon,
                },
                props: { field: Object },
                setup(props) {
                    const mapDom = ref();
                    const { field } = toRefs(props);

                    const mapInstance = computed(() => {
                        const container = mapDom.value;

                        if (!container) {
                            return null;
                        }

                        let defaultLatLng = field.value.value ? field.value.value.split(',').slice(0, 2) : [];
                        defaultLatLng = defaultLatLng.length === 2 ? defaultLatLng : ['', ''];

                        if (!('L' in window)) {
                            return;
                        }

                        const url =
                            'https://api.mapbox.com/styles/v1/folders501/cjhg51v5k44wi2srr6tnlb211/tiles/256/{z}/{x}/{y}?access_token=' +
                            'pk.eyJ1IjoiZm9sZGVyczUwMSIsImEiOiJjamhnNGV1ZnIxNHF0M2NtbHh3eWQzaGJpIn0.dtq9TiT-NNW9jwPvwx6vAQ';
                        const map = L.map(container, { attributionControl: false }).setView(defaultLatLng, 17);
                        const marker = L.marker(defaultLatLng, { draggable: true }).addTo(map);
                        marker.dragging.disable();

                        L.control.attribution({ prefix: '' }).addTo(map);
                        L.tileLayer(url, {
                            maxZoom: 18,
                            attribution: '<a href="#">©</a>',
                            id: 'mapbox.streets',
                        }).addTo(map);
                        return map;
                    });

                    return {
                        mapDom,
                        mapInstance,
                        mapUrl: computed(() => {
                            const [lat, lon] = field.value.value.split(',');
                            return `https://www.openstreetmap.org/?mlat=${lat}&mlon=${lon}#map=18`;
                        }),
                    };
                },
                template: `
                    <FormGroup :field="field" #text>
                        <template v-if="field.value == null">${EM_DASH}</template>
                        <div v-else>
                            <div
                                v-if="mapInstance"
                                uno-layer-default="
                                    mb-2 rounded-md w-full transition relative z-0 h-64 overflow-hidden
                                    before:block before:content-empty
                                "
                                ss-xl="w-3/4"
                            >
                                <div
                                    ref="mapDom"
                                    uno-layer-default="
                                        absolute top-0 left-0 bottom-0 w-full h-full
                                        before:pt-[56.25%]
                                    "
                                />
                            </div>

                            <Btn
                                :external="true"
                                :href="mapUrl"
                                class="leading-snug inline-block text-blue-600 hover:text-blue-700 p-0"
                            >
                                <Icon name="location-marker" class="w-5 inline-block" />
                            </Btn>
                        </div>
                    </FormGroup>
                `,
            },
            StackField: {
                // components: { DetailFieldProxy },
                components: {
                    FormGroup,
                },
                props: {
                    field: Object,
                    resource: Object,
                },
                template: `
                    <FormGroup :field="field">
                        <div
                            uno-layer-base="divide-y"
                            uno-layer-default="-my-4 -mx-6 divide-gray-200"
                            ss-xs="mx--2"
                        >
                            <DetailFieldProxy
                                :field="stackField"
                                :resource="resource"
                                v-for="stackField in field.value"
                            />
                        </div>
                    </FormGroup>
                `,
            },

            DynamicField,
        };

        const formFieldEvent = (field, value, immediate) => {
            globalBus.emit('form-field-update', {
                attribute: field.value.attribute,
                value: value.value,
                immediate: !!immediate,
            });
        };

        const formFieldFill = (formData, dirty, field, value) => {
            if (dirty) {
                formData.append(
                    field.value.attribute,
                    value.value == null ? '' : value.value
                );
            }
        };

        const baseFormFieldSetup = (props, { expose }, immediate) => {
            const { field } = toRefs(props);
            const value = ref();
            let dirty = false;
            watch(value, () => {
                dirty = true;
                formFieldEvent(field, value, immediate);
            });
            nextTick(() => value.value = field.value.value);
            expose({
                fill: (formData) => formFieldFill(formData, dirty, field, value),
            });
            return { value };
        };

        const formFieldComponents = {
            DownloadField: baseDetailFieldProxy(indexFieldComponents.DownloadField),

            BadgeField: {
                components: { FormGroup },
                props: { field: Object },
                setup: (props, ctx) => baseFormFieldSetup(props, ctx),
                template: `
                    <FormGroup :field="field">
                        <input
                            :readonly="!!field.readonly"
                            class="form-input"
                            type="text"
                            uno-layer-default="rounded-md w-full transition border-gray-400"
                            ss-xl="w-3/4"
                            v-model="value"
                        >
                    </FormGroup>
                `,
            },
            BooleanField: {
                components: {
                    FormGroup,
                    InputChip,
                },
                props: { field: Object },
                setup: (props, { expose }) => {
                    const { field } = toRefs(props);
                    const value = ref();
                    let dirty = false;
                    watch(value, () => {
                        dirty = true;
                        formFieldEvent(field, value);
                    });
                    nextTick(() => value.value = field.value.value);
                    expose({
                        fill: (formData) => formFieldFill(formData, dirty, field, value),
                    });
                    return {
                        computedField: computed(() => {
                            if (field.value.inlineLabel) {
                                field.value.inlineName = field.value.name;
                                field.value.name = '';
                            }

                            return field.value;
                        }),
                        value,
                        click(event) {
                            value.value = event.target.checked;
                        },
                    };
                },
                template: `
                    <FormGroup :field="computedField">
                        <div uno-layer-default="flex flex-wrap my-1 space-x-2">
                            <InputChip
                                :readonly="!!field.readonly"
                                :checked="value"
                                @click="click($event)"
                            />
                            <span
                                v-if="computedField.inlineName"
                                v-text="computedField.inlineName"
                            />
                        </div>
                    </FormGroup>
                `,
            },
            BooleanGroupField: {
                components: {
                    FormGroup,
                    InputChip,
                },
                props: { field: Object },
                setup(props, { expose }) {
                    const { field } = toRefs(props);
                    const value = ref({});
                    let dirty = false;
                    let shouldWatch = false;
                    const assignToValue = () => {
                        let dVal = field.value.displayValue;
                        dVal = isArray(dVal) ? null : dVal;

                        let fVal = field.value.value;
                        fVal = isArray(fVal) ? null : fVal;

                        value.value = dVal ?? fVal ?? {};
                    };
                    const updateValueManually = () => {
                        dirty = true;
                        formFieldEvent(field, value, true);
                    };
                    watch(() => ({ ...value.value }), () => {
                        if (shouldWatch) {
                            updateValueManually();
                        }
                    });
                    watch(field, () => {
                        if (!dirty) {
                            assignToValue();
                        }
                    });
                    nextTick(() => assignToValue);
                    expose({
                        fill: (formData) => {
                            if (dirty) {
                                formData.append(
                                    field.value.attribute,
                                    JSON.stringify(value.value == null ? {} : value.value)
                                );
                            }
                        },
                    });
                    return {
                        value,
                        updateValue(key, val) {
                            shouldWatch = true;
                            value.value[key] = !!val;
                            updateValueManually();
                        },
                    };
                },
                template: `
                    <FormGroup :field="field">
                        <div uno-layer-default="flex flex-wrap my-1 -mx-2">
                            <label
                                uno-layer-default="p-2 cursor-pointer"
                                v-for="(rowValue, rowKey) in value"
                            >
                                <InputChip
                                    :modelValue="value[rowKey]"
                                    @update:modelValue="updateValue(rowKey, $event)"
                                />
                                {{ rowKey }}
                            </label>
                        </div>
                    </FormGroup>
                `,
            },
            CodeField: {
                components: {
                    CodeMirrorInput,
                    FormGroup,
                },
                props: { field: Object },
                setup(props, { expose }) {
                    const { field } = toRefs(props);
                    const value = ref(field.value.value ?? '');
                    let dirty = false;
                    watch(value, () => {
                        dirty = true;
                        formFieldEvent(field, value, immediate);
                    });
                    nextTick(() => value.value = (field.value.value ?? ''));
                    expose({
                        fill: (formData) => formFieldFill(formData, dirty, field, value),
                    });
                    return { value };
                },
                template: `
                    <FormGroup :field="field">
                        <CodeMirrorInput
                            :readonly="false"
                            v-model="value"
                        />
                    </FormGroup>
                `,
            },
            CurrencyField: {
                components: {
                    AutonumericInput,
                    FormGroup,
                    Icon,
                },
                props: { field: Object },
                setup: (props, ctx) => baseFormFieldSetup(props, ctx),
                template: `
                    <FormGroup :field="field">
                        <AutonumericInput
                            #toggle
                            :decimalCharacter="field.decimalCharacter || '.'"
                            :digitGroupSeparator="field.digitGroupSeparator || ','"
                            :prefix="field.prefix || ''"
                            :suffix="field.sufix || ''"
                            v-model="value"
                            class="tabular-nums rounded-md w-full transition border border-gray-400 py-2 px-3"
                            ss-xl="w-3/4"
                        >
                            <Icon
                                class="w-4 ml-1 inline-block cursor-pointer"
                                name="switch-horizontal"
                            />
                        </AutonumericInput>
                    </FormGroup>
                `,
            },
            DateTimeField: {
                components: {
                    FlatPickrInput,
                    FormGroup,
                },
                props: { field: Object },
                setup: (props, ctx) => baseFormFieldSetup(props, ctx, true),
                template: `
                    <FormGroup :field="field">
                        <FlatPickrInput
                            class="py-2 px-3 border"
                            :enable-time="field.useTime"
                            :nullable="field.nullable"
                            v-model="value"
                        />
                    </FormGroup>
                `,
            },
            FileField: {
                components: {
                    Btn,
                    ConfirmModal,
                    Icon,
                    Lazy,
                    FileInput,
                    FormGroup,
                },
                props: {
                    field: Object,
                    resource: Object,
                },
                setup(props, { expose }) {
                    const { field, resource } = toRefs(props);
                    const value = ref();
                    let dirty = false;
                    expose({
                        fill: (formData) => formFieldFill(formData, dirty, field, value),
                    });

                    const file = ref();
                    watch(file, (file) => {
                        dirty = true;
                        if (file) {
                            value.value = file.file;
                            formFieldEvent(field, value, true);
                        }
                    });

                    return {
                        file,
                        deleteFile() {
                            axiosInternal({
                                method: 'delete',
                                url: [
                                    `${__avon.baseUrl}`,
                                    'api',
                                    'resource',
                                    `${resource.value.resourceName}`,
                                    'detail',
                                    `${resource.value._.resourceId}`,
                                    'field',
                                    `${field.value.attribute}`,
                                ].join('/'),
                            }).then(() => {
                                // reload
                            });
                        },
                    };
                },
                template: `
                    <FormGroup :field="field">
                        <div v-if="field.value">
                            <Lazy v-if="field.previewUrl">
                                <img
                                    :src="field.previewUrl"
                                    :title="field.value"
                                >
                            </Lazy>

                            <div
                                v-else
                                class="rounded-md border px-3 py-2 leading-normal border-gray-400"
                            >
                                <div class="break-words flex-grow">{{ field.value }}</div>
                            </div>

                            <ConfirmModal
                                :shiftable="true"
                                @click="deleteFile()"
                                button-class="px-2 focus:ring-red-600 mix-shade-10:active:ring-red-300 mix-shade-20:active:ring-red-300"
                                shift-button-class="text-white bg-red-500 focus:bg-red-600 hover:bg-red-700"
                                text="Delete"
                                unshift-button-class="text-red-600"
                                v-if="field.deletable"
                            >
                                <template #heading>Delete file?</template>
                                <template #button><Icon name="trash" class="w-4" /></template>
                            </ConfirmModal>
                        </div>

                        <template v-if="!field.readonly">
                            <FileInput @values="($event) => file = $event[0]" />

                            <template v-if="file && file.maybeFolder">
                                Please ensure the file is not a folder.
                            </template>
                        </template>
                    </FormGroup>
                `,
            },
            HeadingField,
            HiddenField,
            IndexField: {
                components: { FormGroup },
                props: { field: Object },
                setup: (props, ctx) => baseFormFieldSetup(props, ctx),
                template: `
                    <FormGroup :field="field">
                        <input
                            :readonly="!!field.readonly"
                            class="form-input"
                            type="text"
                            uno-layer-default="rounded-md w-full transition border-gray-400"
                            ss-xl="w-3/4"
                            v-model="value"
                        >
                    </FormGroup>
                `,
            },
            InputField: {
                components: {
                    FormGroup,
                    TextInput,
                },
                props: { field: Object },
                setup: (props, ctx) => baseFormFieldSetup(props, ctx),
                template: `
                    <FormGroup :field="field">
                        <TextInput
                            :readonly="!!field.readonly"
                            class="w-full"
                            ss-xl="w-3/4"
                            v-model="value"
                        />
                    </FormGroup>
                `,
            },
            KeyValueField: {
                components: {
                    AutosizeInput,
                    FormGroup,
                    Icon,
                    TableCell: {
                        template: `<td uno-layer-base="parent-hover:bg-gray-50 parent-hover:border-gray-200 border-gray-200 p-px border-t border-l"><slot /></td>`,
                    },
                    TableHeading: {
                        template: `<th uno-layer-base="bg-gray-100 whitespace-nowrap p-0 border-t border-l border-gray-200 px-4 py-2 sticky top-0 bg-gray-100 text-sm"><slot /></th>`,
                    },
                    TextInput,
                },
                props: { field: Object },
                setup(props, { expose }) {
                    const { field } = toRefs(props);
                    const value = ref();
                    let dirty = false;
                    watch(value, () => {
                        dirty = true;
                        formFieldEvent(field, value);
                    });
                    nextTick(() => value.value = field.value.value);
                    expose({
                        fill: (formData) => formFieldFill(formData, dirty, field, value),
                    });

                    const valueArray = reactive([]);
                    let updating = false;
                    watch(valueArray, valueArray => {
                        if (updating) {
                            return;
                        }
                        updating = true;

                        const newValue = {};

                        valueArray.forEach((keyValue) => {
                            keyValue.double = false;

                            if (keyValue.key in newValue) {
                                keyValue.double = true;
                            } else {
                                newValue[keyValue.key] = keyValue.value;
                            }
                        });

                        value.value = newValue;

                        nextTick(() => {
                            valueArray.forEach((keyValue) => {
                                if (keyValue.ref && keyValue.ref.autosizeUpdate) {
                                    keyValue.ref.autosizeUpdate();
                                }
                            });

                            nextTick(() => {
                                updating = false;
                            });
                        });
                    });

                    const updateVa = (fieldValue) => {
                        Object.entries(fieldValue).forEach(([key, value]) => {
                            valueArray.push({
                                key,
                                value,
                                double: false,
                                ref: null,
                            });
                        });
                    };
                    updateVa(value.value || {});

                    return {
                        value,
                        valueArray,

                        deleteKeyValue(index) {
                            valueArray.splice(index, 1);
                        },
                        addKeyValue() {
                            valueArray.push({
                                key: '',
                                value: '',
                                double: false,
                                ref: null,
                            });
                        },
                    };
                },
                template: `
                    <FormGroup :field="field">
                        <div
                            uno-layer-default="overflow-x-auto"
                        >
                            <table
                                uno-layer-default="border-separate border-spacing-0 w-full"
                            >
                                <thead>
                                    <tr>
                                        <TableHeading class="w-1/3 rounded-tl-md">{{ field.keyLabel }}</TableHeading>
                                        <TableHeading class="border-r rounded-tr-md">{{ field.valueLabel }}</TableHeading>
                                        <th class="w-12" />
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(keyValue, index) in valueArray">
                                        <TableCell>
                                            <TextInput
                                                :class="keyValue.double ? 'border border-red-600' : 'border-0'"
                                                class="w-full px-4 py-2 rounded-0 focus:relative focus:z-1"
                                                v-model="keyValue.key"
                                            />
                                        </TableCell>
                                        <TableCell class="p-px border-t border-l border-r">
                                            <AutosizeInput
                                                :class="keyValue.double ? 'bg-red-50' : ''"
                                                :readonly="keyValue.double"
                                                class="border-0 rounded-0 focus:relative focus:z-1"
                                                :ref="(el) => keyValue.ref = el"
                                                v-model="keyValue.value"
                                            />
                                        </TableCell>
                                        <td class="px-4 py-2 text-center">
                                            <button @click="deleteKeyValue(index)">
                                                <Icon name="trash" class="h-4" />
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <th colspan="2" class="border rounded-b-md bg-gray-100 border-gray-200">
                                            <button
                                                uno-layer-default="py-2 w-full"
                                                @click="addKeyValue"
                                            >
                                                <Icon name="plus" class="h-4 inline-block" />
                                                <span
                                                    uno-layer-default="font-medium"
                                                    v-text="field.actionText"
                                                />
                                            </button>
                                        </th>
                                        <th />
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </FormGroup>
                `,
            },
            LineField: {
                components: {
                    DetailLineField: detailFieldComponents.LineField,
                },
                props: { field: Object },
                template: `<DetailLineField :field="field" />`,
            },
            LocationField: {
                components: { FormGroup },
                props: { field: Object },
                setup: (props, ctx) => baseFormFieldSetup(props, ctx),
                template: `
                    <FormGroup :field="field">
                        <input
                            :readonly="!!field.readonly"
                            class="form-input"
                            type="text"
                            uno-layer-default="rounded-md w-full transition border-gray-400"
                            ss-xl="w-3/4"
                            v-model="value"
                        >
                    </FormGroup>
                `,
            },
            MarkdownField: {
                components: {
                    AutosizeInput,
                    Btn,
                    ConfirmModal,
                    FormGroup,
                    MarkdownPreview,
                },
                props: { field: Object },
                setup(props, { expose }) {
                    const { field } = toRefs(props);
                    const value = ref();
                    let dirty = false;
                    watch(value, () => {
                        dirty = true;
                        formFieldEvent(field, value);
                    });
                    nextTick(() => value.value = field.value.value);
                    expose({
                        fill: (formData) => formFieldFill(formData, dirty, field, value),
                    });

                    return {
                        markdownish: computed(() => {
                            const v = value.value;
                            if (v == null) return ZWJ;
                            if (v === '') return 'kosong';
                            return v;
                        }),
                        toggle: ref(false),
                        value,

                        parse: (text) => marked.parse(text)
                            .replace(new RegExp(`(<a) (href=\\")(?!${location.origin}${__avon.baseUrl})`, 'g'), '$1 x $2')
                            .replace(/(<a) x (href=\"[^h])/g, '$1 $2')
                            .replace(/(<a) x (href)/g, '$1 rel="noopener noreferrer" target="_blank" $2'),

                        markdownHelp: `See [daringfireball.net](https://daringfireball.net/projects/markdown/) and [commonmark.org](https://commonmark.org/).`,
                        examples: [
                            ['*Italic*', '_Italic_'],
                            ['**Bold**', '__Bold__'],
                            ['# Heading 1', `Heading 1\n=========`],
                            ['## Heading 2', `Heading 2\n==========`],

                            [
                                '[Link](http://a.com)',
                                `[Link][1]\n\n...\n\n[1]: http://b.org`,
                                '[Link](./)',
                            ],

                            [
                                '![Image](http://url/a.png)',
                                `![Image][1]\n\n...\n\n[1]: http://url/b.jpg`,
                                `![Image](https://commonmark.org/help/images/favicon.png)`,
                            ],

                            ['> Blockquote'],

                            [
                                `* List\n* And\n* Another`,
                                `- List\n- And\n- Another`,
                            ],

                            [
                                `1. One\n2. Two\n3. Three`,
                                `1) One\n2) Two\n3) Three`,
                            ],

                            [
                                `Horizontal rule:\n---`,
                                `Horizontal rule:\n***`,
                            ],

                            ['Inline `code` with backticks'],

                            [
                                [
                                    '```',
                                    '# code block',
                                    `print '3 backticks'`,
                                    '```',
                                ].join(`\n`),

                                [
                                    '',
                                    '····# code block',
                                    `····print 'indent 4 spaces'`,
                                ].join(`\n`),
                            ],
                        ],
                    };
                },
                template: `
                    <FormGroup :field="field">
                        <div uno-layer-default="
                            flex justify-between mb-2
                            border rounded-lg
                            border-gray-400
                            focus:ring focus:ring-size-[0.25rem] focus:ring-blue-300 focus:border-blue-300
                        ">
                            <Btn
                                :disabled="!toggle"
                                @click="toggle = !toggle"
                                :class="{
                                    'flex-grow font-semibold text-center': true,
                                    'text-blue-600': toggle,
                                }"
                            >Edit</Btn>
                            <Btn
                                :disabled="toggle"
                                @click="toggle = !toggle"
                                :class="{
                                    'flex-grow font-semibold text-center': true,
                                    'text-blue-600': !toggle,
                                }"
                            >Preview</Btn>
                            <ConfirmModal
                                confirmText="Close"
                                text="?"
                                :noCancel="true"
                            >
                                <template #heading>
                                    Markdown
                                </template>

                                <div
                                    uno-layer-default="space-y-4 overflow-x-auto max-h-[70vh] px-4 py-2"
                                >
                                    <p class="prose">
                                        <div v-html="parse(markdownHelp)"></div>
                                    </p>
                                    <table class="w-full">
                                        <thead class="sticky top-0">
                                            <tr>
                                                <th>Type</th>
                                                <th>Or</th>
                                                <th>... to Get</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="example in examples">
                                                <td class="font-mono tracking-wider whitespace-pre">{{ example[0] }}</td>
                                                <td class="font-mono tracking-wider whitespace-pre">{{ example?.[1] ?? '' }}</td>
                                                <td class="prose"><div v-html="parse(example?.[2] ?? example[0])"></div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </ConfirmModal>
                        </div>

                        <div
                            v-if="toggle"
                            uno-layer-default="
                                px-3 py-2
                                border rounded-lg
                                border-gray-400
                            ">
                            <MarkdownPreview :text="markdownish" :inline="field.inline" />
                        </div>
                        <AutosizeInput
                            :readonly="!!field.readonly"
                            v-else
                            v-model="value"
                        />
                    </FormGroup>
                `,
            },
            PasswordField: {
                components: {
                    FormGroup,
                    PasswordInput,
                },
                props: { field: Object },
                setup: (props, ctx) => baseFormFieldSetup(props, ctx),
                template: `
                    <FormGroup :field="field">
                        <PasswordInput
                            :readonly="!!field.readonly"
                            input-class="font-mono"
                            v-model="value"
                            wrapper-class="ss-xl-w-3/4"
                        />
                    </FormGroup>
                `,
            },
            PlaceField: {
                components: { FormGroup },
                props: { field: Object },
                setup: (props, ctx) => baseFormFieldSetup(props, ctx),
                template: `
                    <FormGroup :field="field">
                        <input
                            :readonly="!!field.readonly"
                            class="form-input"
                            type="text"
                            uno-layer-default="rounded-md w-full transition border-gray-400"
                            ss-xl="w-3/4"
                            v-model="value"
                        >
                    </FormGroup>
                `,
            },
            SelectField: {
                components: {
                    FormGroup,
                    Icon,
                    InputChip,
                    SelectList,
                    PopperButtoned,
                },
                props: { field: Object },
                setup(props, { expose }) {
                    const { field } = toRefs(props);
                    const value = ref();
                    let dirty = false;
                    watch(value, () => {
                        dirty = true;
                        formFieldEvent(field, value, true);
                    });
                    nextTick(() => value.value = field.value.value);
                    expose({
                        fill: (formData) => formFieldFill(formData, dirty, field, value),
                    });
                    return {
                        value,
                        label: computed(() => {
                            const val = value.value;
                            const model = (field.value.options || []).find((o) => o.key === val);
                            return model == null ? 'Pilih' : `${model.label} (${val})`;
                        }),
                    };
                },
                template: `
                    <FormGroup :field="field">
                        <div
                            uno-layer-default="-mx-2"
                            v-if="field.useRadioOnForms"
                        >
                            <label
                                uno-layer-default="p-2 cursor-pointer"
                                v-for="({ key, label }, rowKey) in field.options"
                            >
                                <InputChip
                                    v-model="value"
                                    type="radio"
                                    :value="key"
                                />
                                {{ label }} ({{ key }})
                            </label>
                        </div>
                        <PopperButtoned
                            v-else
                            button-class="
                                border rounded-md px-3 w-full justify-between
                                hover:bg-inherit
                                ss-xl-w-3/4
                            "
                        >
                            <template #button>
                                <span v-text="label" />
                                <Icon name="selector" class="w-4" />
                            </template>

                            <template #popper="{ target }">
                                <SelectList
                                    :clearable="field.nullable"
                                    :options="field.options"
                                    :searchable="field.searchable"
                                    :target="target"
                                    v-model:value="value"
                                    value-key="key"
                                />
                            </template>
                        </PopperButtoned>
                    </FormGroup>
                `,
            },
            StackField: {
                components: { FormGroup },
                props: {
                    field: Object,
                    resource: Object,
                },
                setup: (props, ctx) => baseFormFieldSetup(props, ctx),
                template: `
                    <FormGroup :field="field">
                        <fieldset
                            uno-layer-base="divide-y"
                            uno-layer-default="-my-4 -mx-6 divide-gray-200"
                            ss-xs="-mx-2"
                        >
                            <FormFieldProxy
                                :field="stackField"
                                :resource="resource"
                                v-for="stackField in field.value"
                            />
                        </fieldset>
                    </FormGroup>
                `,
            },
            StatusField: {
                components: {
                    DetailStatusField: detailFieldComponents.StatusField,
                },
                props: { field: Object },
                template: `<DetailStatusField :field="field" />`,
            },
            TextField: {
                components: {
                    FormGroup,
                    TextInput,
                },
                props: { field: Object },
                setup: (props, ctx) => ({
                    ...baseFormFieldSetup(props, ctx),
                    id: randomString(40),
                }),
                template: `
                    <FormGroup :field="field">
                        <TextInput
                            :readonly="!!field.readonly"
                            class="w-full"
                            ss-xl="w-3/4"
                            v-model="value"
                            :list="id"
                        />
                        <datalist :id="id" v-if="field.suggestions?.length">
                            <option v-for="s in field.suggestions" :value="s">{{ s }}</option>
                        </datalist>
                    </FormGroup>
                `,
            },
            TextareaField: {
                components: {
                    AutosizeInput,
                    FormGroup,
                },
                props: { field: Object },
                setup: (props, ctx) => baseFormFieldSetup(props, ctx),
                template: `
                    <FormGroup :field="field">
                        <AutosizeInput
                            :readonly="!!field.readonly"
                            v-model="value"
                        />
                    </FormGroup>
                `,
            },
            // relations
            BelongsToField: {
                components: {
                    FormGroup,
                    Icon,
                    SelectList,
                    PopperButtoned,
                    ResourceModelPreview,
                },
                props: {
                    field: Object,
                    resource: Object,
                },
                setup(props, { expose }) {
                    const { field, resource } = toRefs(props);
                    const value = ref();
                    let dirty = false;
                    watch(value, () => {
                        dirty = true;
                        formFieldEvent(field, value, true);
                    });
                    nextTick(() => value.value = field.value.value.id || null);
                    expose({
                        fill: (formData) => formFieldFill(formData, dirty, field, value),
                    });

                    const search = ref('');
                    const options = ref([]);
                    const searchable = ref(true);
                    const searchCallback = (search) => {
                        if (! searchable.value) {
                            return;
                        }

                        // nonPolymorphicQuery
                        axiosInternal({
                            method: 'get',
                            // url: `${__avon.baseUrl}/api/resource/${resource.value.resourceName}/relatable`,
                            url: `${__avon.baseUrl}/api/resource/${field.value.value.resourceName}/relatable`,
                            params: {
                                search: search ?? null,
                                viaResource: resource.value.resourceName,
                                viaResourceId: resource.value._.resourceId ?? null,
                                viaRelationship: field.value.attribute,
                            },
                        })
                            .catch(noop)
                            .then(({ data }) => {
                                searchable.value = data.searchable;
                                options.value = data.relatedOptions;
                            });
                    };

                    watch(refDebounced(search, 1000), (value) => searchCallback(value));
                    searchCallback();

                    return {
                        search,
                        options,
                        value,
                        label: computed(() => [...options.value, field.value.value || {}].find((option) => option.id === value.value)),
                    };
                },
                template: `
                    <FormGroup :field="field">
                        <PopperButtoned
                            button-class="
                                overflow-hidden
                                border rounded-md px-3 w-full justify-between
                                hover:bg-inherit
                                ss-xl-w-3/4
                            "
                        >
                            <template #button>
                                <ResourceModelPreview
                                    :model="label"
                                    :plainText="true"
                                />
                                <Icon name="selector" class="w-4" />
                            </template>

                            <template #popper="{ target }">
                                <SelectList
                                    #="{ data: { model } }"
                                    :fuseKeys="['model.id', 'model.title', 'model.subtitle']"
                                    :options="options"
                                    :searchable="true"
                                    :target="target"
                                    v-model:search="search"
                                    v-model:value="value"
                                    value-key="id"
                                >
                                    <ResourceModelPreview
                                        :model="model"
                                        :plainText="true"
                                        :noSelect="true"
                                    />
                                </SelectList>
                            </template>
                        </PopperButtoned>
                    </FormGroup>
                `,
            },
            DynamicField,
        };

        const IndexFieldProxy = {
            components: indexFieldComponents,
            props: {
                field: Object,
                resource: Object,
            },
            template: `<component
                :field="field"
                :is="field.component"
                :resource="resource"
            />`,
        };

        const DetailFieldProxy = {
            components: detailFieldComponents,
            props: {
                field: Object,
                resource: Object,
            },
            template: `<component
                :field="field"
                :is="field.component"
                :resource="resource"
            />`,
        };

        const FormFieldProxy = {
            components: formFieldComponents,
            props: {
                action: Object,
                field: Object,
                resource: Object,
            },
            setup(props, { expose }) {
                const internalRef = ref();

                expose({
                    fill: computed(() => internalRef.value.fill),
                });

                return {
                    internalRef,
                    component: computed(() => {
                        const field = props.field;
                        return field.builtInComponent
                            ? field.component
                            : 'DynamicField';
                    }),
                };
            },
            template: `<component
                :action="action"
                :field="field"
                :is="component"
                :resource="resource"
                ref="internalRef"
            />`,
        };

        const actionComponents = {
            DefaultAction: {
                components: {
                    Btn,
                    FormFieldProxy,
                    BaseModal,
                    OverlayBlocker,
                },
                emits: ['cancelled', 'action-submitted'],
                props: {
                    action: Object,
                    immediate: Boolean,
                    params: Object,
                    selection: { default: undefined },
                },
                setup(props, { emit }) {
                    const { action, immediate, params, selection } = toRefs(props);
                    const originalReady = !!action.value._.ready;
                    const isImmediate = immediate.value;
                    action.value._.selection = selection.value;
                    action.value._.immediate = immediate.value;

                    const loadingData = ref(false);
                    let fieldsRef = [];

                    const setFieldRef = (field) => {
                        fieldsRef.push(field);
                    };

                    const submit = () => {
                        const formData = new FormData();

                        fieldsRef
                            .map((ref) => ref?.fill)
                            .filter(Boolean)
                            .forEach((fill) => fill(formData));

                        const resources = Object.values(selection.value.resources).map((r) => [
                            r.id,
                            r.retrieved_at,
                        ]);

                        formData.append('_resources', JSON.stringify(resources));
                        formData.append('_selectAll', selection.value.selectAll || 0);

                        loadingData.value = true;

                        axiosInternal({
                            method: 'post',
                            url: `${__avon.baseUrl}/api/resource/${selection.value.resourceName}/action/${action.value.actionKey}`,
                            data: formData,
                            params: params.value,
                            _preventUnload: true,
                        })
                            .catch(noop)
                            .then((response) => {
                                loadingData.value = false;
                                emit('action-submitted', {
                                    action,
                                    selection,
                                    response,
                                });
                                if (isImmediate) {
                                    emit('cancelled');
                                }
                            });
                    };

                    if (isImmediate) {
                        submit();
                    }

                    const fields = computed(() => {
                        const fields = action.value.fields || [];
                        // fields.forEach(field => prepareFieldComponent(field));
                        return fields;
                    });

                    onBeforeUnmount(() => {
                        action.value._.ready = originalReady;
                    });

                    return {
                        loadingData,

                        fields,
                        setFieldRef,

                        // 0 none 1 selections 2 filtered 3 all
                        warnMultipleItems: () =>
                            [2, 3].includes(selection.value.selectAll) ||
                            (selection.value.selectAll > 1 && Object.keys(selection.value.resources).length > 1),

                        confirmButtonClass: computed(() => {
                            const { ready } = action.value._;
                            return {
                                'text-gray-500': !ready,
                                [action.value.confirmButtonClass]: ready,
                            };
                        }),

                        cancel() {
                            emit('cancelled');
                        },

                        submit,
                    };
                },
                template: `
                    <BaseModal :loading="loadingData">
                        <template #heading>
                            {{ action.name }}
                        </template>

                        <template #body>
                            <OverlayBlocker v-if="loadingData" />

                            <fieldset uno-layer-default="mx-4 my-4 overflow-x-auto rounded-md max-h-[60vh]">
                                <FormFieldProxy
                                    :action="action"
                                    :field="field"
                                    :ref="setFieldRef"
                                    v-for="field in fields"
                                />
                            </fieldset>

                            <hr v-if="fields.length > 0 && action.confirmText"
                                uno-layer-default="mx-4 border-gray-400"
                            >

                            <div>
                                <p
                                    uno-layer-default="mx-4 my-6 leading-snug"
                                    v-text="action.confirmText"
                                />
                            </div>

                            <div v-if="warnMultipleItems()">
                                <p uno-layer-default="text-sm mx-4 mt-8 mb-6">Note: action will be done on multiple rows.</p>
                            </div>
                        </template>

                        <template #footer>
                            <Btn
                                @click="cancel()"
                                uno-layer-default="px-4 hover:bg-gray-300"
                            >
                                {{ action.cancelButtonText || 'Dismiss' }}
                            </Btn>
                            <Btn
                                :class="confirmButtonClass"
                                :disabled="!action._.ready"
                                @click="submit"
                                uno-layer-default="px-4"
                            >
                                {{ action.confirmButtonText || 'Run action' }}
                            </Btn>
                        </template>
                    </BaseModal>
                `,
            },
        };

        const ActionProxy = {
            components: actionComponents,
            emits: ['cancelled', 'action-submitted'],
            props: {
                action: Object,
                immediate: Boolean,
                params: Object,
                selection: { default: undefined },
            },
            setup: (_, { emit }) => ({ emit }),
            template: `<component
                :action="action"
                :immediate="immediate"
                :is="action.component"
                :params="params"
                :selection="selection"
                @action-submitted="emit('action-submitted', $event)"
                @cancelled="emit('cancelled', $event)"
            />`,
        };

        const PanelButton = {
            components: { Btn },
            template: `<Btn
                class="
                    py-2 px-4 rounded-0 leading-inherit
                    flex w-full justify-between space-x-4
                    focus:ring-inset focus:ring-size-[0.125rem]
                "
            ><slot /></Btn>`,
        };

        const SmallPanelLabel = {
            template: `<small uno-layer-default="block text-semibold bg-gray-200 py-2 px-4"><slot /></small>`,
        };

        const SmallPanel = {
            template: `<div uno-layer-default="max-h-[70vh] rounded-lg shadow-lg overflow-x-auto border border-gray-300 bg-gray-200 py-1"><div uno-layer-default="bg-white"><slot /></div></div>`,
        };

        const createFormToolComponent = ({ toolBaseUrl }) => ({
            components: {
                ConfirmModal,
                FormFieldProxy,
                Panel,
                Spinner,
            },

            setup() {
                const pageTitle = useTitle();
                const hasLoadedOnce = ref(false);
                const toolData = ref({
                    _: reactive({}),
                });

                let fieldsRef = [];

                const setFieldRef = (field) => {
                    fieldsRef.push(field);
                };

                const prepareResource = (oldValue, newValue) => {
                    cssExtract(JSON.stringify(newValue));

                    fieldsRef = [];

                    const {
                        label,
                        fields,
                        panels,
                    } = newValue;

                    const sections = prepareFieldsCollection(fields, panels);

                    const res = reactive({
                        ...newValue,
                        _: reactive({
                            sections: Object.entries(sections).map(([kind, value]) => ({ ...value, kind })),
                        }),
                    });

                    pageTitle.value = `${label} Tool | ${__tmp_appName}`;

                    return res;
                };

                const navigateBase = (query, cancel) => {
                    const { abort, axios } = axiosWrapper._wrap({
                        method: 'post',
                        data: gatherForm(),
                        url: `${toolBaseUrl}/action/fields`,
                    });

                    if (cancel && 'cancel' in cancel) {
                        cancel.cancel = abort;
                    }

                    return axios.then(({ _abortReason, data }) => {
                        const subtasks = [];
                        if (_abortReason) return { subtasks };

                        hasLoadedOnce.value = true;
                        toolData.value = prepareResource(toolData.value || {}, data || {});

                        return { subtasks };
                    });
                };

                const gatherForm = () => {
                    const formData = new FormData();

                    fieldsRef
                        .map((ref) => ref?.fill)
                        .filter(Boolean)
                        .forEach((fill) => fill(formData));

                    // (this.resourceData('panels') || []).forEach(function(panel) {
                    //     if (panel.isResourceTool && panel.fill) {
                    //         panel.fill(formData);
                    //     }

                    return formData;
                };

                formRenavigate(gatherForm, navigateBase);

                return {
                    canSubmit: computed(() => {
                        return hasLoadedOnce.value && toolData.value.actionMeta?.preventSubmit !== true;
                    }),
                    hasLoadedOnce,
                    resource: toolData,

                    setFieldRef,

                    save(event) {
                        axiosWrapper({
                            method: 'post',
                            url: `${toolBaseUrl}/action/save`,
                            data: gatherForm(),
                            _preventUnload: true,
                        });
                    },
                };
            },

            template: `
                <div
                    uno-layer-default="space-y-4"
                    v-if="hasLoadedOnce"
                >
                    <fieldset
                        uno-layer-default="space-y-4"
                        v-if="resource._.sections"
                    >
                        <template v-for="section in resource._.sections">
                            <component
                                :is="section.data.component"
                                :resource="resource"
                                :tool="section.data"
                                v-if="section.isResourceTool"
                            />

                            <Panel
                                :panel="section.data"
                                v-else-if="section.isPanel"
                            >
                                <FormFieldProxy
                                    :field="field"
                                    :ref="setFieldRef"
                                    :resource="resource"
                                    v-for="field in section.data.fields"
                                />
                            </Panel>
                        </template>
                    </fieldset>

                    <div
                        v-if="canSubmit" uno-layer-default="flex items-center justify-end space-x-2"
                    >
                        <ConfirmModal
                            :shiftable="true"
                            @click="save($event)"
                            button-class="px-4 text-white bg-gray-600 hover:bg-gray-900"
                            text="Simpan"
                        />
                    </div>
                </div>
                <div
                    class="flex items-center justify-center min-h-[40vh]"
                    v-else
                >
                    <Spinner scale="2" />
                </div>
            `,
        });

        const ActionsList = {
            emits: ['action-picked'],
            components: {
                PanelButton,
                ShiftableText,
                SmallPanel,
                SmallPanelLabel,
            },
            props: {
                groupedActions: Object,
            },
            setup: (_, { emit }) => ({
                pickAction(event, action) {
                    emit('action-picked', {
                        event,
                        action,
                        shiftKey: event.shiftKey,
                    });
                },
            }),
            template: `
                <SmallPanel>
                    <template v-for="(group, groupName) in groupedActions">
                        <SmallPanelLabel>{{ groupName }}</SmallPanelLabel>
                        <PanelButton
                            @click="pickAction($event, action)"
                            class="!justify-start"
                            v-for="action in group"
                        >
                            <ShiftableText :always="action.withoutConfirmation">
                                {{ action.name }}
                            </ShiftableText>
                        </PanelButton>
                    </template>
                </SmallPanel>
            `,
        };

        const ResourceActions = {
            components: {
                ActionProxy,
                ActionsList,
                Icon,
                PopperButtoned,
                PopperTeleport,
            },
            emits: ['action-submitted'],
            props: {
                buttonClass: String,
                buttonLabel: String,
                groupedActions: Object,
                resource: Object,
                resourceName: String,
            },
            setup(props, { emit }) {
                const { resource, resourceName } = toRefs(props);
                const activeAction = ref();
                const toggle = ref(false);

                return {
                    activeAction,
                    toggle,
                    prepareAction({ action, shiftKey }) {
                        if (action) {
                            toggle.value = true;
                            activeAction.value = {
                                action,
                                immediate: shiftKey || action.withoutConfirmation,
                                selection: {
                                    resourceName: resourceName.value,
                                    selectAll: 1,
                                    resources: { [resource.value.id]: resource.value },
                                },
                            };
                        }
                    },
                    actionSubmitted(event) {
                        emit('action-submitted', event);
                        toggle.value = false;
                    },
                    cancelled() {
                        activeAction.value = undefined;
                        toggle.value = false;
                    },
                };
            },
            template: `
                <PopperButtoned
                    :button-class="buttonClass"
                >
                    <template #button>
                        <span
                            v-if="buttonLabel"
                            v-text="buttonLabel"
                        />
                        <Icon
                            name="mod-exclamation-right"
                            class="w-4"
                        />
                    </template>

                    <template #popper>
                        <ActionsList
                            :grouped-actions="groupedActions"
                            @action-picked="prepareAction($event)"
                        />
                    </template>
                </PopperButtoned>

                <PopperTeleport
                    :blur="true"
                    :modal="true"
                    v-model:toggle="toggle"
                >
                    <ActionProxy
                        @action-submitted="actionSubmitted($event)"
                        @cancelled="cancelled($event)"
                        v-bind="activeAction"
                    />
                </PopperTeleport>
            `,
        };

        const ResourcePager = {
            components: {
                Btn,
                Icon,
                PopperButtoned,
                SmallPanel,
                TextInput,
            },
            props: {
                navigate: Function,
                resources: Object,
                totalResources: Number,
            },
            setup(props) {
                const {
                    navigate,
                    resources,
                    totalResources,
                } = toRefs(props);

                const getPager = (pager) => ({
                    ...pager,
                    htmlClass: {
                        'cursor-auto': pager.active || !pager.page,
                        'font-mono': isNaN(parseInt(pager.label, 10)),
                        'font-semibold bg-blue-200': pager.active,
                        'text-gray-500': !pager.active && !pager.page,
                        'whitespace-nowrap px-4 rounded-none flex items-center min-w-[4.5ch]': true,
                        'focus:ring-inset focus:ring-size-[0.125rem]': true,
                    },
                });

                const withoutTotalResource = computed(() => totalResources.value === -1);

                const pagerData = computed(() => {
                    const pager = resources.value && resources.value.pager;

                    if (!pager) {
                        return {
                            component: '',
                            pager: [],
                        };
                    }

                    const { currentPage, hasMore, perPage } = pager;
                    const firstInView = (currentPage - 1) * perPage + 1;
                    const lastInView = (currentPage - 1) * perPage + resources.value.resources.length;
                    const lastPage = !withoutTotalResource.value && totalResources.value
                        ? parseInt(Math.ceil(totalResources.value / perPage), 10)
                        : currentPage + (hasMore ? 1 : 0);
                    const estimatedTotal = !withoutTotalResource.value && totalResources.value
                        ? totalResources.value
                        : (hasMore ? '...' : lastInView);

                    return {
                        ...pager,

                        component: '',
                        firstInView: isNaN(firstInView) ? EN_DASH : firstInView,
                        lastInView: isNaN(lastInView) ? EN_DASH : lastInView,
                        lastPage,
                        estimatedTotal,

                        pager: [
                            getPager({
                                active: false,
                                label: '<',
                                icon: 'chevron-left',
                                page: currentPage === 1 ? false : currentPage - 1,
                            }),
                            ...pagerUrlWindow(lastPage, currentPage).map((page) =>
                                getPager({
                                    active: currentPage === page,
                                    label: page,
                                    icon: page === '...' ? 'dots-horizontal' : null,
                                    page: page === '...' ? false : page,
                                })
                            ),
                            withoutTotalResource.value
                                ? getPager({
                                    active: false,
                                    label: '>',
                                    icon: 'chevron-right',
                                    page: hasMore ? currentPage + 1 : false,
                                })
                                : (totalResources.value || !hasMore
                                    ? getPager({
                                        active: false,
                                        label: '>',
                                        icon: 'chevron-right',
                                        page: currentPage < lastPage ? currentPage + 1 : false,
                                    })
                                    : getPager({
                                        active: false,
                                        label: '...',
                                        icon: 'dots-horizontal',
                                        page: false,
                                    })),
                        ],
                    };
                });

                return {
                    withoutTotalResource,
                    pagerData,
                    pagerNavigate(event, { page }) {
                        navigate.value({ page });
                    },
                };
            },
            template: `
                <div v-if="resources.resources && resources.resources.length"
                    uno-layer-default="flex items-center"
                >
                    <nav uno-layer-default="flex-grow max-w-full overflow-hidden h-10">
                        <ul
                            uno-layer-base="divide-x"
                            uno-layer-default="overflow-x-auto whitespace-nowrap flex h-24 divide-gray-200"
                        >
                            <li v-for="pager in pagerData.pager"
                                uno-layer-default="h-10 flex items-stretch"
                            >
                                <span v-if="pager.page === false"
                                    :class="pager.htmlClass"
                                >
                                    <Icon
                                        :name="pager.icon"
                                        class="w-4"
                                        v-if="pager.icon"
                                    />
                                    <div
                                        v-else
                                        v-text="pager.label"
                                    />
                                </span>
                                <Btn v-else
                                    :class="pager.htmlClass"
                                    :href="pager.url"
                                    @click="pagerNavigate($event, pager)"
                                >
                                    <Icon
                                        :name="pager.icon"
                                        class="w-4"
                                        v-if="pager.icon"
                                    />
                                    <div
                                        v-else
                                        v-text="pager.label"
                                    />
                                </Btn>
                            </li>
                        </ul>
                    </nav>

                    <PopperButtoned
                        :disabled="!totalResources || (pagerData.currentPage === 1 && !pagerData.hasMore)"
                        button-class="whitespace-nowrap mx-1 py-1"
                        v-if="!withoutTotalResource"
                    >
                        <template #button>
                            {{ pagerData.firstInView }}
                            ${EN_DASH}
                            {{ pagerData.lastInView }}
                            /
                            {{ pagerData.estimatedTotal }}
                        </template>

                        <template #popper>
                            <SmallPanel>
                                <label uno-layer-default="py-2 px-4 flex items-center space-x-2">
                                    <span>Enter page</span>
                                    <TextInput
                                        pattern="[0-9]*"
                                        :max="pagerData.lastPage || 1"
                                        :value="pagerData.currentPage"
                                        class="py-1 px-2"
                                        min="1"
                                        step="1"
                                        type="number"
                                    />

                                    <span>or pick</span>

                                    <select
                                        @change="navigate({ page: $event.target.value })"
                                        class="form-select"
                                        uno-layer-default="rounded-md transition py-1 px-2 pr-10"
                                    >
                                        <option
                                            :selected="page === pagerData.currentPage"
                                            v-for="page in [...Array(pagerData.lastPage || 1).keys()].map(i => i + 1)"
                                            v-text="page"
                                        />
                                    </select>
                                </label>
                            </SmallPanel>
                        </template>
                    </PopperButtoned>
                </div>
            `,
        };

        const DefaultMetric = {
            components: {
                Icon,
                Lazy,
                PopperTeleport,
                Spinner,
            },

            emits: ['result'],

            props: {
                routes: Object,
                metric: Object,
                dashboard: Object,
                resource: Object,
                load: Function,
            },

            setup(props, { emit }) {
                const { dashboard, load, metric, resource, routes } = toRefs(props);

                const anchor = ref();
                const helpToggle = ref(false);
                const loading = ref(false);
                const firstLoad = ref(true);
                const loaderParams = ref({});

                const defaultLoader = async () => {
                    // dashboard.value / resource.resourceName

                    return await axiosWrapper({
                        method: 'get',
                        url: `${__avon.baseUrl}/api/dashboard/${routes.value.dashboard}/metric/${metric.value.uriKey}`,
                        params: loaderParams.value,
                    });
                };

                let loadingTimer;
                const loader = async () => {
                    try {
                        loading.value = true;
                        const callback = load?.value || defaultLoader;
                        const result = await callback();
                        emit('result', result);
                    } catch (e) {
                        emit('result', false);
                    } finally {
                        loading.value = false;
                        firstLoad.value = false;
                    }

                    timeoutRaf(() => {
                        loadingTimer = setTimeout(loader, 30 * 1000 + Math.random() * 1000);
                    });
                };

                onMounted(() => {
                    timeoutRaf(() => {
                        loadingTimer = setTimeout(loader, Math.floor(Math.random() * 1000));
                    });
                });

                onBeforeUnmount(() => {
                    clearTimeout(loadingTimer);
                });

                return {
                    rangeChanged({ target: { value: range } }) {
                        loaderParams.value = { range };
                        if (loadingTimer) {
                            clearTimeout(loadingTimer);
                            loadingTimer = null;
                        }
                        firstLoad.value = true;
                        loader();
                    },

                    anchor,
                    helpToggle,
                    loading,
                    firstLoad,

                    toggleHelp(set) {
                        helpToggle.value = set;
                    },

                    size: computed(() => {
                        const cls = {
                            'w-full p-2': true,
                        };

                        const width = metric.value?.width;
                        if (['1/2', '1/3', '2/3'].indexOf(width) !== -1) {
                            cls['lg:w-' + width] = true;
                        }

                        switch (metric.value?.height) {
                            case '3':
                                cls['h-60 lg:h-120'] = true;
                                break;

                            case '2':
                                cls['h-60 lg:h-80'] = true;
                                break;

                            default:
                                cls['h-40'] = true;
                        }

                        return cls;
                    }),
                };
            },

            template: `
                <div :class="size" class="default-metrics">
                    <Lazy class="
                        rounded-lg border border-gray-200 shadow h-full px-4 py-3 overflow-hidden relative bg-white
                    ">
                        <transition name="fade">
                            <div
                                uno-layer-default="absolute inset-0 z-20"
                                v-if="loading && firstLoad"
                            >
                                <div uno-layer-default="relative h-full w-full">
                                    <div uno-layer-default="absolute h-full w-full bg-black opacity-25"></div>
                                    <div uno-layer-default="absolute h-full w-full flex justify-center items-center">
                                        <Spinner scale="2" />
                                    </div>
                                </div>
                            </div>
                        </transition>

                        <p uno-layer-default="font-semibold text-lg text-gray-600 mb-2 leading-none">{{ metric.name }}</p>

                        <slot />

                        <div
                            uno-layer-default="absolute top-0 right-0 my-2 mx-4"
                            v-if="metric.ranges?.length"
                        >
                            <select
                                @change="rangeChanged($event)"
                                class="form-select"
                                uno-layer-default="leading-tight inline-block w-full border border-gray-200 rounded-md pl-2 py-1 transition"
                            >
                                <option
                                    v-for="option in metric.ranges"
                                    :selected="metric.defaultRange === option.key"
                                    :value="option.key"
                                >{{ option.value }}</option>
                            </select>
                        </div>

                        <div
                            @mouseleave="toggleHelp(false)"
                            @mouseover="toggleHelp(true)"
                            ref="anchor"
                            uno-layer-default="cursor-auto absolute bottom-0 right-0 my-2 mx-4 bg-gray-500 text-white border rounded-full h-5 w-5 text-center select-none opacity-50 hover:opacity-100 transition"
                            v-if="metric.helpText"
                        >
                            <div uno-layer-default="relative text-xs leading-tight">?</div>
                        </div>

                        <PopperTeleport
                            :target="anchor"
                            v-model:toggle="helpToggle"
                        >
                            <div uno-layer-default="p-3 text-sm border border-gray-400 rounded-lg shadow bg-white">{{ metric.helpText }}</div>
                        </PopperTeleport>
                    </Lazy>
                </div>
            `,
        };

        const Clock = {
            components: { Icon },
            setup() {
                const reference = {
                    server: new Date(),
                    local: new Date(),
                };

                setTimeout(() => {
                    createIdleableInterval(() => {
                        return axios({
                            method: 'get',
                            url: `${__avon.baseUrl}/api/application/time`,
                        })
                            .then(({ headers }) => {
                                const date = headers['x-date'];
                                if (date) {
                                    const newClock = new Date(date);
                                    if (newClock) {
                                        reference.local = new Date();
                                        reference.server = newClock;
                                    }
                                }
                            }, noop);
                    }, 111 * 1000, 333 * 1000);
                }, 1000);

                const clock = ref(new Date());

                createIdleableInterval(() => {
                    const { server, local } = reference;
                    clock.value = new Date(new Date().getTime() + (server - local));
                }, 300, 3000);

                const toggle = ref(false);
                const dayJsValue = computed(() => dayjs(clock.value).locale(__avon.locale).format('dddd, D MMMM YYYY, HH:mm:ss'));
                const titleValue = computed(() => dayjs(clock.value).locale(__avon.locale).format('dddd, D MMMM YYYY, HH:mm'));

                return {
                    toggle,
                    clock,
                    clockText: computed(() => toggle.value ? dayJsValue.value : clock.value.toLocaleString()),
                    titleValue,
                };
            },
            template: `<time
                :datetime="clock.toISOString()"
                :title="titleValue"
                uno-layer-default="tabular-nums"
            >
                {{ clockText }}<Icon
                    @click="toggle = !toggle"
                    class="w-3 ml-1 inline-block cursor-pointer"
                    name="switch-horizontal"
                />
            </time>`,
        };

        const ResourceTable = {
            components: {
                IndexFieldProxy,
                TableCell: {
                    template: `<td
                        uno-layer-base="table-cell parent-hover:bg-gray-50 parent-hover:border-t-gray-200 border-t border-gray-200"
                        ss-xs="block whitespace-normal"
                    ><slot /></td>`,
                },
                TableCellAction: {
                    template: `<td
                        uno-layer-base="table-cell parent-hover:bg-gray-50 parent-hover:border-t-gray-200 border-t border-gray-200"
                        ss-xs="flex items-center absolute top-0 h-[3rem] important-py-0"
                    ><slot /></td>`,
                },
                TableHeading: {
                    template: `<th
                        uno-layer-base="table-cell border-b border-gray-200 font-medium sticky top-0 bg-gray-100 whitespace-nowrap"
                        ss-xs="hidden"
                    ><slot /></th>`,
                },
                TableRow: {
                    template: `<tr
                        uno-layer-base="table-row"
                        ss-xs="block relative"
                    ><slot /></tr>`,
                },
            },
            props: {
                fieldResolver: Function,
                resource: Boolean,
                resources: Array,
                spacious: Boolean,
            },
            setup(props) {
                const textAligns = ['text-left', 'text-center', 'text-right', 'text-justify'];

                const getTextAlignClass = (value) => {
                    // ss-md:whitespace-nowrap
                    return textAligns.find((x) => x === `text-${value}`) ?? textAligns[0];
                };

                return {
                    fieldResolverCallback: (resource) => props.fieldResolver
                        ? props.fieldResolver(resource)
                        : resource.fields,
                    headingClass: (field) => ({
                        [getTextAlignClass(field.textAlign)]: true,
                    }),
                    cellClass: (field, cell) => ({
                        [getTextAlignClass(field.textAlign)]: true,
                    }),
                };
            },
            template: `
                <table
                    uno-layer-default="table"
                    ss-xs="block min-w-full"
                >
                    <thead
                        uno-layer-default="table-header-group text-sm tracking-wider bg-gray-100"
                        ss-xs="block"
                    >
                        <TableRow
                            class="relative"
                            :class="{
                                'children:py-4 children:px-4': spacious,
                                'children:py-1 children:px-3': !spacious,
                                'z-1': resource,
                            }"
                        >
                            <TableHeading
                                v-if="resource"
                                class="w-px"
                            />
                            <TableHeading
                                v-for="(field, index) in fieldResolverCallback(resources[0])"
                                :class="headingClass(field)"
                            >
                                <slot
                                    :field="field"
                                    :index="index"
                                    :label="field.indexName ?? field.name"
                                    name="heading"
                                >
                                    {{ field.indexName ?? field.name }}
                                </slot>
                            </TableHeading>
                            <TableHeading
                                class="w-full"
                                uno-layer-extra="border-l-0 px-0"
                            />
                            <TableHeading
                                v-if="resource"
                                class="w-px"
                            />
                        </TableRow>
                    </thead>

                    <tbody
                        uno-layer-default="table-row-group"
                        ss-xs="block"
                    >
                        <TableRow v-for="(row, rowIndex) in resources"
                            class="parent"
                            :class="{
                                'children:py-4 children:px-4': spacious,
                                'children:py-2 children:px-3': !spacious,
                                'ss-xs:pt-[3rem]': resource,
                            }"
                        >
                            <TableCellAction
                                class="ss-xs:w-full"
                                v-if="resource"
                            >
                                <slot name="checkbox" :row="row" :index="rowIndex" />
                            </TableCellAction>

                            <TableCell
                                :class="cellClass(field, row)"
                                v-for="(field, index) in fieldResolverCallback(row)"
                            >
                                <div
                                    uno-layer-default="hidden font-light float-left"
                                    ss-xs="flex items-baseline pr-1 mb-1"
                                >{{ field.indexName ?? field.name }}:</div>
                                <IndexFieldProxy
                                    :field="field"
                                    :resource="row"
                                />
                            </TableCell>

                            <TableCell
                                uno-layer-extra="border-l-0 px-0"
                                ss-xs="block important-py-0"
                            />

                            <TableCellAction
                                class="ss-xs:right-0"
                                v-if="resource"
                            >
                                <slot name="action" :row="row" :index="rowIndex" />
                            </TableCellAction>
                        </TableRow>
                    </tbody>
                </table>
            `,
        };

        const prepareErrorMessage = (error) => {
            let message = error.message;

            if (error.isAxiosError) {
                message = error.response.statusText;
            }

            return message;
        };

        const formRenavigate = (gatherForm, navigate) => {
            let oldForm = null;

            const getFormObject = () => Object.fromEntries(gatherForm().entries());

            (function renavigate() {
                const currentForm = getFormObject();
                if (JSON.stringify(oldForm) === JSON.stringify(currentForm)) {
                    setTimeout(renavigate, globalIdle.value ? 10000 : 5000);
                } else {
                    oldForm = currentForm;
                    navigate().then(() => {
                        setTimeout(renavigate, globalIdle.value ? 10000 : 5000);
                    });
                }
            })();

            const formUpdate = debounce({
                delay: 3000,
                leading: true,
                trailing: true,
                source: () => {
                    oldForm = getFormObject();
                    navigate();
                },
            });

            globalBus.on('form-field-update', (eventValue) => {
                if (eventValue.immediate) {
                    formUpdate.reset();
                    formUpdate.original();
                }
                else {
                    formUpdate.bounced();
                }
            });
            onBeforeUnmount(() => {
                globalBus.off('form-field-update', formUpdate);
            });
        };

        const prepareNavigateQuery = (resourceName, query, queryString, queryExclude) => {
            const { filters, ...queries } = { filters: {}, ...(query || {}) };

            const filterEntries =
                filters == null
                    ? []
                    : Object.entries({
                          ...(queryString.filters || {}),
                          ...filters,
                      }).filter((entry) => entry[1] != null);

            const newQueryString = {
                ...queryString,
                ...queries,
                filters: filterEntries.length ? Object.fromEntries(filterEntries) : null,
            };

            const resourceParams = {};
            const params = Object.fromEntries(
                Object.entries(newQueryString).map(([key, value]) => {
                    value = value === '' || value == null ? null : value;
                    if (['filters'].includes(key) && value != null) {
                        value = JSON.stringify(value);
                    }
                    if (key === 'page' && value === 1) {
                        value = null;
                    }
                    if (! (queryExclude || ['viaResource', 'viaResourceId', 'viaRelationship']).includes(key)) {
                        resourceParams[`${resourceName}_${key}`] = value;
                    }
                    return [key, value];
                })
            );

            const resourceUrlParams = new URLSearchParams(
                Object.fromEntries(Object.entries(resourceParams).filter((entry) => entry[1] != null))
            );

            new URLSearchParams(window.location.search).forEach((value, key) => {
                if (!(key in resourceParams) && value != null) {
                    resourceUrlParams.append(key, value);
                }
            });

            const resourceQueryString = [window.location.pathname, resourceUrlParams.toString()]
                .filter((s) => s !== '')
                .join('?');

            return {
                newQueryString,
                params,
                resourceQueryString,
            };
        };

        const filterComponents = {
            SelectFilter: {
                components: {
                    Icon,
                    PopperButtoned,
                    SelectList,
                    SmallPanelLabel,
                },
                props: {
                    filter: Object,
                    navigate: Function,
                },
                setup(props) {
                    const { filter, navigate } = toRefs(props);
                    const { resourceName } = filter.value._;
                    const filtersQuery =
                        jsonParse(new URLSearchParams(window.location.search).get(`${resourceName}_filters`)) || {};
                    const value = ref(filtersQuery[filter.value.key]);
                    watch(value, (value) =>
                        navigate.value({
                            filters: {
                                [filter.value.key]: value === '' ? null : value,
                            },
                        })
                    );
                    return {
                        value,
                    };
                },
                template: `
                    <div
                        uno-layer-default="py-2 px-3 min-w-48"
                        ss-xs="min-w-full"
                    >
                        <PopperButtoned
                            button-class="
                                border rounded-md py-1 px-2 w-full justify-between
                                text-sm
                                hover:bg-inherit
                            "
                        >
                            <template #button>
                                <span v-text="value ?? '${EM_DASH}'" />
                                <Icon name="selector" class="w-4" />
                            </template>

                            <template #popper="{ target }">
                                <SelectList
                                    :options="filter.options"
                                    :searchable="filter.options.length > 5"
                                    :target="target"
                                    v-model:value="value"
                                    label-key="value"
                                />
                            </template>
                        </PopperButtoned>
                    </div>
                `,
            },
            BooleanFilter: {
                components: {
                    InputChip,
                    SmallPanelLabel,
                },
                props: {
                    filter: Object,
                    navigate: Function,
                },
                setup(props) {
                    const { filter, navigate } = toRefs(props);
                    const { resourceName } = filter.value._;
                    const options = {};
                    const updating = reactive({});

                    return {
                        updating,
                        changed({ value }, { checked }) {
                            options[value] = checked ? 1 : 0;

                            const values = Object.entries(options)
                                .filter((o) => o[1])
                                .map((o) => o[0]);

                            updating[value] = true;
                            navigate
                                .value({
                                    filters: {
                                        [filter.value.key]: values.length ? JSON.stringify(values) : null,
                                    },
                                })
                                .catch(noop)
                                .then(() => (updating[value] = false));
                        },
                        checked({ value }) {
                            if (updating[value]) {
                                return false;
                            }

                            const filtersQuery =
                                jsonParse(
                                    new URLSearchParams(window.location.search).get(`${resourceName}_filters`)
                                ) || {};
                            const filtersChecked = jsonParse(filtersQuery[filter.value.key]) || [];
                            return filtersChecked.includes(value);
                        },
                    };
                },
                template: `
                    <div uno-layer-default="py-1">
                        <label v-for="option in filter.options"
                            uno-layer-default="block w-full cursor-pointer whitespace-nowrap inline-flex items-center hover:bg-gray-50 px-3 py-1"
                        >
                            <InputChip
                                :name="option.key"
                                :checked="checked(option)"
                                :indeterminate="updating[option.key]"
                                @change="changed(option, $event.target)"
                            />

                            <span
                                uno-layer-default="ml-2"
                                v-text="option.value"
                            />
                        </label>
                    </div>
                `,
            },
            DateFilter: {
                components: {
                    FlatPickrInput,
                    SmallPanelLabel,
                },
                props: {
                    filter: Object,
                    navigate: Function,
                },
                setup(props) {
                    const { filter, navigate } = toRefs(props);
                    const { resourceName } = filter.value._;
                    const filtersQuery =
                        jsonParse(new URLSearchParams(window.location.search).get(`${resourceName}_filters`)) || {};
                    const value = ref(filtersQuery[filter.value.key]);
                    const target = ref();
                    watch(value, (value) =>
                        navigate.value({
                            filters: {
                                [filter.value.key]: value === '' ? null : value,
                            },
                        })
                    );
                    return { target, value };
                },
                template: `
                    <div uno-layer-default="py-2 px-3" ref="target">
                        <FlatPickrInput
                            class="py-1 px-2 border text-sm"
                            :enable-time="false"
                            :nullable="true"
                            :target="target"
                            v-model="value"
                        />
                    </div>
                `,
            },
            TextFilter: {
                components: {
                    SmallPanelLabel,
                    TextInput,
                },
                props: {
                    filter: Object,
                    navigate: Function,
                },
                setup(props) {
                    const { filter, navigate } = toRefs(props);
                    const { resourceName } = filter.value._;
                    const filtersQuery =
                        jsonParse(new URLSearchParams(window.location.search).get(`${resourceName}_filters`)) || {};
                    const value = ref(filtersQuery[filter.value.key]);

                    watch(refDebounced(value, (filter.value.debounce || 0.5) * 1000), (value) =>
                        navigate.value({
                            filters: {
                                [filter.value.key]: value === '' ? null : value,
                            },
                        })
                    );
                    return { value };
                },
                template: `
                    <div uno-layer-default="py-2 px-3">
                        <TextInput
                            uno-layer-default="w-full py-1 px-2 border text-sm"
                            v-model="value"
                        />
                    </div>
                `,
            },
        };

        const dashboardComponents = {
            PartitionMetric: {
                components: {
                    DefaultMetric,
                    Icon,
                },
                props: {
                    routes: Object,
                    metric: Object,
                    dashboard: Object,
                    resource: Object,
                },
                setup() {
                    let chartistInstance = null;

                    const chart = ref();
                    const result = ref({});

                    const baseValue = computed(() => (result.value.value || []).map((item) => {
                        item._className = randomString(8);
                        const label = item.label;
                        item._label = label === '' ? '—' : label;

                        return item;
                    }));

                    const total = computed(() => baseValue.value.reduce((acc, item) => acc + item.value, 0));

                    const values = computed(() => baseValue.value.map((item, index) => {
                        const percentage = ((item.value / total.value) * 100).toFixed(2);

                        const valueFormatted = AutoNumeric.format(item.value, {
                            allowDecimalPadding: 'floats',
                        });

                        item._labelFormatted = `${item._label} (${valueFormatted} · ${percentage}%)`;

                        if (!item.color && index < 25) {
                            const serie = String.fromCharCode('a'.charCodeAt(0) + index);
                            const styles = css.searchStyle(`.ct-series-${serie} .ct-slice-donut`);
                            item.color = styles.fill || styles.stroke;
                        }

                        if (item.color) {
                            item._iconColor = item.color;
                            if (index < 25) {
                                item._chartistClass = `[&_${item._className}_.ct-slice-donut-solid]:[fill:${item.color}]`;
                            }
                        }

                        return item;
                    }));

                    const makeChartist = () => new Chartist.PieChart(
                        chart.value,
                        {
                            series: values.value.map((item, index) => ({
                                value: item.value,
                                name: item._label,
                                className: `${item._className}${index < 25 ? ' ct-series-' + String.fromCharCode(97 + index) : ''}`,
                            })),
                        },
                        {
                            donut: true,
                            donutWidth: 15,
                            donutSolid: true,
                            showLabel: false,
                        }
                    );

                    const persistChartist = () => {
                        if (chart.value) {
                            chartistInstance = makeChartist();
                        } else {
                            nextTick(() => requestAnimationFrame(persistChartist));
                        }
                    };

                    watch(result, () => persistChartist());

                    return {
                        resultLoaded(response) {
                            const data = response?.data;
                            if (data) {
                                result.value = data;
                            }
                        },

                        chart,
                        result,

                        top5Values: computed(() => values.value.slice(0, 5)),

                        totalFormatted: computed(() => AutoNumeric.format(total.value, {
                            allowDecimalPadding: 'floats',
                        })),
                    };
                },
                template: `
                    <DefaultMetric
                        :dashboard="dashboard"
                        :metric="metric"
                        :resource="resource"
                        :routes="routes"
                        @result="resultLoaded($event)"
                    >
                        <template v-if="top5Values.length">
                            <ul uno-layer-default="mt-2 leading-tight relative text-gray-700 z-1">
                                <li v-for="item in top5Values" uno-layer-default="whitespace-nowrap flex items-center">
                                    <Icon
                                        :stroke="false"
                                        :style="{ color: item._iconColor }"
                                        class="select-none mr-1 w-4 shrink-0"
                                        fill
                                        name="mod-circle-fill"
                                    />
                                    <span uno-layer-default="inline-block m--px p-px bg-white/90 border border-white rounded [white-space:normal]">{{ item._labelFormatted }}</span>
                                </li>
                            </ul>

                            <p uno-layer-default="absolute top-0 right-0 my-4 mx-4 text-gray-400 inline-block m--px p-px bg-white/99 border border-white rounded">({{ totalFormatted }} total)</p>

                            <div uno-layer-default="absolute right-3 top-9">
                                <div ref="chart" uno-layer-default="w-22 h22" />
                            </div>
                        </template>
                        <div v-else-if="'value' in result">
                            {{ result?.noDataText || '${EM_DASH}' }}
                        </div>
                    </DefaultMetric>
                `,
            },
            IndexMetric: {
                components: {
                    DefaultMetric,
                    ResourceTable,
                },
                props: {
                    routes: Object,
                    metric: Object,
                    dashboard: Object,
                    resource: Object,
                },
                setup() {
                    const result = ref({});

                    return {
                        resultLoaded(response) {
                            const data = response?.data;
                            if (data) {
                                result.value = data;
                            }
                        },

                        resources: computed(() => result.value.data || []),
                    };
                },
                template: `
                    <DefaultMetric
                        :dashboard="dashboard"
                        :metric="metric"
                        :resource="resource"
                        :routes="routes"
                        @result="resultLoaded($event)"
                        class="default-index-metric"
                    >
                        <div
                            uno-layer-default="absolute inset-0 top-11 border-t border-gray-200"
                            v-if="resources && resources.length"
                        >
                            <div uno-layer-default="flex flex-col h-full">
                                <div uno-layer-default="align-middle inline-block w-full overflow-auto">
                                    <div uno-layer-default="bg-white">
                                        <ResourceTable
                                            :resources="resources"
                                            class="whitespace-nowrap"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </DefaultMetric>
                `,
            },
            TrendMetric: {
                components: {
                    DefaultMetric,
                    Icon,
                },
                props: {
                    routes: Object,
                    metric: Object,
                    dashboard: Object,
                    resource: Object,
                },
                setup() {
                    let chartistInstance = null;

                    const chart = ref();
                    const result = ref({});

                    const trend = computed(() => result.value?.trend || []);

                    const makeChartist = () => new Chartist.LineChart(
                        chart.value,
                        {
                            series: [
                                (trend.value || []).map(({ date, value }) => ({
                                    ...(date ? { x: new Date(date) } : {}),
                                    y: value,
                                })),
                            ],
                        },
                        {
                            fullWidth: true,
                            showArea: true,
                            axisX: {
                                showGrid: false,
                                showLabel: false,
                                offset: 0,
                            },
                            axisY: {
                                showGrid: false,
                                showLabel: false,
                                offset: 0,
                            },
                            chartPadding: {
                                top: 5,
                                right: 0,
                                bottom: 0,
                                left: 0,
                            },
                            plugins: [
                                Chartist.plugins.tooltip({
                                    appendToBody: true,
                                    class: 'rounded-lg text-sm important-pb-1',
                                    tooltipFnc: (meta, raw) => {
                                        const metric = result.value || {};
                                        const [time, value] = raw.split(',');
                                        const number = AutoNumeric.format(value, {
                                            allowDecimalPadding: 'floats',
                                        });
                                        let dateInstance = dayjs.unix(time / 1000).locale(metric.locale);
                                        dateInstance = metric.timezone ? dateInstance.tz(metric.timezone) : dateInstance;
                                        const date = dateInstance
                                            .format(metric.timeFormat || 'dddd, D MMMM YYYY, HH:mm:ss')
                                            .replace(/, 00:00:00$/, '');
                                        return [
                                            `${date}:`,
                                            metric.prefix,
                                            number,
                                            metric.suffix,
                                        ].filter(Boolean).join(' ');
                                    },
                                }),
                            ],
                        }
                    );

                    const persistChartist = () => {
                        if (chart.value) {
                            chartistInstance = makeChartist();
                        } else {
                            nextTick(() => requestAnimationFrame(persistChartist));
                        }
                    };

                    watch(result, () => persistChartist());

                    return {
                        resultLoaded(response) {
                            const data = response?.data;
                            if (data) {
                                result.value = data || {};
                            }
                        },

                        chart,
                        result,

                        formattedValue() {
                            const r = result.value;

                            if (!r) {
                                return AutoNumeric.format(0, {
                                    allowDecimalPadding: 'floats',
                                });
                            }

                            const displayValue = r.showSumValue
                                ? trend.value.reduce((acc, trend) => acc + trend.value, 0)
                                : r.value;

                            return AutoNumeric.format(displayValue, {
                                allowDecimalPadding: 'floats',
                            });
                        },

                    };
                },
                template: `
                    <DefaultMetric
                        :dashboard="dashboard"
                        :metric="metric"
                        :resource="resource"
                        :routes="routes"
                        @result="resultLoaded($event)"
                    >
                        <p
                            uno-layer-default="font-semibold text-4xl mt-2"
                            v-if="result.showLatestValue"
                        >
                            <div uno-layer-default="mt-2">
                                <small v-if="result.prefix" uno-layer-default="">{{ result.prefix }}</small>
                                {{ formattedValue }}
                                <small v-if="result.suffix" uno-layer-default="">{{ result.suffix }}</small>
                            </div>
                        </p>

                        <div
                            :class="{ 'h-24': !result.showLatestValue }"
                            class="h-14"
                            uno-layer-default="
                                absolute right-0 left-0 bottom-0
                                [*>&.ct-line]:[stroke-width:.125rem]
                                [*>&.ct-point]:[stroke-width:.375rem]
                            "
                        >
                            <div ref="chart" class="h-14" :class="{ 'h-24': !result.showLatestValue }"></div>
                        </div>
                    </DefaultMetric>
                `,
            },
            ValueMetric: {
                components: {
                    DefaultMetric,
                    HtmlDecode,
                },
                props: {
                    routes: Object,
                    metric: Object,
                    dashboard: Object,
                    resource: Object,
                },
                setup() {
                    const result = ref({});

                    const value = computed(() => result.value.value || 0);
                    const previous = computed(() => result.value.previous == null ? null : result.value.previous);

                    const delta = computed(() => {
                        const val = value.value;
                        const prv = previous.value;

                        if (!prv || !val) {
                            return 0;
                        }

                        return (((val - prv) / prv) * 100).toFixed(2);
                    });

                    const resultTextRef = ref();
                    const resultText = computed(() => {
                        if (! resultTextRef.value) {
                            return '';
                        }

                        return resultTextRef.value.innerText;
                    });

                    return {
                        resultText,
                        resultTextRef,

                        resultLoaded(response) {
                            const data = response?.data;
                            if (data) {
                                result.value = data;
                            }
                        },

                        result,
                        value,
                        previous,

                        formattedValue: computed(() => AutoNumeric.format(value.value, {
                            allowDecimalPadding: 'floats',
                        })),

                        delta,
                        deltaAbsolute: computed(() => Math.abs(delta.value)),
                    };
                },
                template: `
                    <DefaultMetric
                        :dashboard="dashboard"
                        :metric="metric"
                        :resource="resource"
                        :routes="routes"
                        @result="resultLoaded($event)"
                    >
                        <p uno-layer-default="font-semibold text-4xl flex-grow">
                            <div uno-layer-default="mt-2 text-gray-700" v-if="value == 0 && !result.allowZeroResult">
                                {{ result.noDataText || '${EM_DASH}' }}
                            </div>
                            <div
                                :title="resultText"
                                ref="resultTextRef"
                                uno-layer-default="mt-2"
                                v-else
                            >
                                <HtmlDecode
                                    :value="result.prefix"
                                    as="small"
                                    v-if="result.prefix"
                                />{{
                                    formattedValue
                                }}<HtmlDecode
                                    :value="result.suffix"
                                    as="small"
                                    v-if="result.suffix"
                                />
                            </div>
                        </p>

                        <div uno-layer-default="absolute bottom-0 left-0 mx-4 mb-3">
                            <p v-if="delta == 0 && !result.allowZeroResult" uno-layer-default="font-semibold text-lg text-gray-600">
                                No previous data
                            </p>
                            <p v-else-if="previous != null" uno-layer-default="font-semibold text-lg text-gray-500">
                                {{ deltaAbsolute }}% {{ delta > 0 ? 'increase' : 'decrease' }}
                            </p>
                        </div>
                    </DefaultMetric>
                `,
            },
        };

        const FilterProxy = {
            components: filterComponents,
            props: {
                filter: Object,
                navigate: Function,
            },
            // template: `<component :is="filter.component" v-bind="$attrs" />`,
            template: `<component
                :filter="filter"
                :is="filter.component"
                :navigate="navigate"
            />`,
        };

        [
            ['Index', indexFieldComponents],
            ['Detail', detailFieldComponents],
            ['Form', formFieldComponents],
        ].forEach(([prefix, components]) => Object.keys(components).forEach(
            (key) => (components[key].name = `${prefix}${key}`)
        ));

        indexFieldComponents.IndexField.components = {
            ...indexFieldComponents.IndexField.components,
            ResourceTable,
        };
        indexFieldComponents.KeyValueField.components = {
            ...indexFieldComponents.KeyValueField.components,
            ResourceTable,
        };
        indexFieldComponents.StackField.components = {
            ...indexFieldComponents.StackField.components,
            IndexFieldProxy,
        };
        detailFieldComponents.StackField.components = {
            ...detailFieldComponents.StackField.components,
            DetailFieldProxy,
        };
        formFieldComponents.StackField.components = {
            ...formFieldComponents.StackField.components,
            FormFieldProxy,
        };

        const ResourceIndex = {
            components: {
                ActionProxy,
                ActionsList,
                Btn,
                ConfirmModal,
                DetailFieldProxy,
                FilterProxy,
                Icon,
                InputChip,
                InputSearch,
                PanelButton,
                PopperButtoned,
                PopperTeleport,
                ResourceActions,
                ResourceModelHeading,
                ResourcePager,
                ResourceTable,
                SmallPanel,
                SmallPanelLabel,
                Spinner,
                TransitionWrapper,

                ResourceTableHeading: {
                    components: {
                        BaseSvg,
                    },
                    props: {
                        field: Object,
                    },
                    setup: (props) => ({
                        field: computed(() => (props.field || { _: reactive({}) })),
                    }),
                    template: `
                        <span
                            uno-layer-default="block flex"
                            v-if="field.sortable"
                        >
                            <span>{{ field.indexName ?? field.name }}</span>

                            <BaseSvg
                                #="{ pathAttribute }"
                                :title="field._.title"
                                @click="field._.navigate()"

                                class="
                                    my--2 py-2 px-2 w-8
                                    transition cursor-pointer
                                    children-[.active]:stroke-blue-500
                                    hover:children-[.active]:stroke-gray-800
                                    hover:children-[.hover]:stroke-blue-700
                                    hover:children-[.hover.clear]:stroke-red-700
                                "
                            >
                                <path v-bind="pathAttribute" :class="field._.iconAsc" v-if="field._.iconAsc" d="M6 14V2m0 0L2 6m4-4 4 4" />
                                <path v-bind="pathAttribute" :class="field._.iconDesc" v-if="field._.iconDesc" d="M12 10v12m0 0 4-4m-4 4-4-4" />
                                <path v-bind="pathAttribute" :class="field._.iconClear" class="clear" v-if="field._.iconClear" d="M16 11l3-3m0 0 3-3m-3 3-3-3m3 3 3 3" />
                            </BaseSvg>
                        </span>

                        <template v-else>{{ field.indexName ?? field.name }}</template>
                    `,
                },
            },
            emits: ['model:toggle', 'model:navigate'],
            props: {
                field: Object,
                label: String,
                routes: Object,
            },
            setup(props, { emit }) {
                const { field: parentField, label, routes } = toRefs(props);

                const resourceName = computed(() => routes.value.resource);
                const pageTitle = useTitle();
                const returns = [];

                // load query string
                const usp = new URLSearchParams(window.location.search);
                const queryString = ref({
                    direction: usp.get(`${resourceName.value}_direction`) || '',
                    filters: jsonParse(usp.get(`${resourceName.value}_filters`)) || null,
                    order: usp.get(`${resourceName.value}_order`) || '',
                    page: parseInt(usp.get(`${resourceName.value}_page`), 10) || null,
                    perPage: parseInt(usp.get(`${resourceName.value}_perPage`), 10) || null,
                    search: usp.get(`${resourceName.value}_search`) || null,
                    viaResource: routes.value.viaResource || null,
                    viaResourceId: routes.value.viaResourceId || null,
                    viaRelationship: routes.value.viaRelationship || null,
                });

                returns.push({
                    resourceName,
                    queryString,
                });

                // selections
                const selectAll = ref(0);
                const selections = ref({});

                returns.push({
                    selectAll,
                    selectionSize: computed(() => Object.entries(selections.value).filter(([_, c]) => c).length),
                    clearSelection() {
                        selections.value = {};
                        selectAll.value = 0;
                    },
                });

                // resources
                const prepareHeading = (fields) => fields.map((f) => {
                    const sortQuery = {
                        order: f.attribute,
                        direction: '',
                    };
                    const sort = {
                        iconAsc: '',
                        iconDesc: '',
                        iconClear: '',
                        title: undefined,
                        navigate: () => navigate(sortQuery),
                    };

                    if (f.attribute !== queryString.value.order) {
                        sortQuery.direction = 'asc';
                        sort.iconAsc = 'hover';
                        sort.iconDesc = 'show';
                        sort.title = 'Sort ascending';
                    } else {
                        switch ((queryString.value.direction || '').toLowerCase()) {
                            case 'asc':
                                sortQuery.direction = 'desc';
                                sort.iconAsc = 'active';
                                sort.iconDesc = 'hover';
                                sort.title = 'Sort descending';
                                break;

                            case 'desc':
                                sortQuery.order = '';
                                sort.iconAsc = 'show';
                                sort.iconDesc = 'active';
                                sort.iconClear = 'hover';
                                sort.title = 'Clear sort';
                                break;

                            default:
                                sortQuery.direction = 'asc';
                                sort.iconAsc = 'hover';
                                sort.iconDesc = 'show';
                                sort.title = 'Sort ascending';
                                break;
                        }
                    }

                    return {
                        ...f,
                        _: reactive({
                            ...(f.sortable ? sort : {}),
                        }),
                    };
                });

                const prepareResource = (oldValue, newValue) => {
                    cssExtract(JSON.stringify(newValue));

                    let fields = null;

                    if (newValue.count) {
                        totalResources.value = newValue.count;
                    } else if (newValue.count === false) {
                        totalResources.value = -1;
                    }

                    if (newValue.fields) {
                        fields = prepareHeading(newValue.fields);
                    }

                    if (newValue.resources) {
                        const { authorizedToCreate } = {
                            ...oldValue,
                            ...newValue,
                        };

                        newValue.resources = newValue.resources.map((d) => {
                            const {
                                authorizedToDelete,
                                authorizedToReplicate,
                                authorizedToUpdate,
                                authorizedToView,
                                id,
                            } = d;
                            const deleteUrl = `${__avon.baseUrl}/api/resource/${resourceName.value}/delete/${id}`;

                            if (fields === null) {
                                fields = prepareHeading(d.fields);
                            }

                            return {
                                ...d,
                                _: reactive({
                                    resourceId: id,
                                    resourceName: resourceName.value,

                                    indexFields: d.fields.filter((field) => field.visibleOnIndex),
                                    previewFields: d.fields.filter((field) => field.visibleOnPreview),

                                    deleteUrl: authorizedToDelete ? deleteUrl : undefined,
                                    detailUrl: authorizedToView
                                        ? `${__avon.baseUrl}/resource/${resourceName.value}/detail/${id}`
                                        : undefined,
                                    editUrl: authorizedToUpdate
                                        ? `${__avon.baseUrl}/resource/${resourceName.value}/edit/${id}`
                                        : undefined,
                                    replicateUrl: authorizedToCreate && authorizedToReplicate
                                        ? `${__avon.baseUrl}/resource/${resourceName.value}/replicate/${id}`
                                        : undefined,
                                    shouldShowActions:
                                        id == null ? false : (authorizedToDelete || authorizedToUpdate || authorizedToView),
                                    deleteResource: () => axiosInternal({
                                        method: 'post',
                                        url: deleteUrl,
                                        _preventUnload: true,
                                    }),

                                    checkbox: reactive({
                                        checked: computed(
                                            () => (id && selections.value[id]) || selectAll.value > 1
                                        ),
                                        class: computed(() => ({
                                            'color-gray-400': selectAll.value > 1,
                                        })),
                                        disabled: computed(() => selectAll.value > 1),
                                        onInput(event) {
                                            if (id) {
                                                selections.value[id] = event.target.checked ? { ...d } : false;
                                            }
                                            emit('model:toggle', { item: d, event, selections });
                                        },
                                    }),
                                }),
                            };
                        });
                    }

                    if (newValue.actions) {
                        newValue.actions.forEach((a) => {
                            const ready = ref(a.ready);
                            a._ = reactive({
                                selection: undefined,
                                immediate: false,
                                toggleReady(set) {
                                    ready.value = set == null ? !ready.value : !!set;
                                },
                                ready,
                            });
                        });
                    }

                    if (newValue.filters) {
                        newValue.filters = newValue.filters.map((f) => ({
                            ...f,
                            _: reactive({
                                resourceName: resourceName.value,
                            }),
                        }));
                    }

                    fields = fields || oldValue.fields || [];
                    const res = reactive({
                        ...oldValue,
                        ...newValue,
                        fields,
                    });
                    res._ = reactive({
                        createUrl: res.authorizedToCreate
                            ? `${__avon.baseUrl}/resource/${resourceName.value}/new`
                            : undefined,

                        indexFields: fields.filter((field) => field.visibleOnIndex),
                        previewFields: fields.filter((field) => field.visibleOnPreview),

                        relatedOneToOne: computed(() => {
                            const pf = parentField.value;
                            const fullable = pf && pf.relation && pf.relation.full;
                            if (! fullable) {
                                return false;
                            }

                            const r = resources.value;
                            if (r == null) {
                                return false;
                            }

                            return r.resources && r.resources.length > 0;
                        }),
                    });

                    if (!parentField.value) {
                        const titleLabel = label.value || `${res.label} Resources`;
                        pageTitle.value = `${titleLabel} | ${__tmp_appName}`;
                    }

                    __LOG('resources updated', res);

                    pollingToggle.value = res.pollingInterval !== 0;

                    return res;
                };

                const hasLoadedOnce = ref(false);
                const resources = ref({
                    _: reactive({}),
                });
                const totalResources = ref();

                const loadingData = ref(false);
                const navigating = ref(false);
                const navigateBase = (query, cancel) => {
                    const { newQueryString, params, resourceQueryString } = prepareNavigateQuery(
                        resourceName.value,
                        query,
                        queryString.value
                    );

                    // if (firstNav === 0) {
                    //     firstNav = 1;
                    //     params.firstNav = true;
                    // }

                    queryString.value = newQueryString;

                    const support = axiosInternal._wrap({
                        method: 'get',
                        url: `${__avon.baseUrl}/api/resource/${resourceName.value}/support`,
                        params,
                    });

                    if (cancel && 'cancel' in cancel) {
                        cancel.cancel = () => {
                            support.abort();
                        };
                    }

                    const supportAxios = support.axios.then(({ _abortReason, data }) => {
                        if (_abortReason) return;
                        resources.value = prepareResource(resources.value || {}, data || {});
                    });

                    return axiosInternal({
                        _abortControllers: support.control,
                        method: 'get',
                        url: `${__avon.baseUrl}/api/resource/${resourceName.value}`,
                        params,
                    }).then(({ _abortReason, data }) => {
                        const subtasks = [];
                        if (_abortReason) return { subtasks };

                        hasLoadedOnce.value = true;
                        resources.value = prepareResource(resources.value || {}, data || {});
                        window.history.replaceState({}, '', resourceQueryString);

                        const total = data?.count;
                        if (!(total || total === false || total === 0)) {
                            subtasks.push(
                                axiosInternal({
                                    _abortControllers: support.control,
                                    method: 'get',
                                    url: `${__avon.baseUrl}/api/resource/${resourceName.value}/count`,
                                    params,
                                }).then(({ _abortReason, data }) => {
                                    if (_abortReason) return;
                                    resources.value = prepareResource(resources.value || {}, data || {});
                                })
                            );
                        }

                        subtasks.push(supportAxios);

                        return { subtasks };
                    });
                };

                const pollNavigateCanceller = { cancel: noop };
                const navigate = (query) => {
                    loadingData.value = true;
                    navigating.value = true;
                    pollNavigateCanceller.cancel();

                    return navigateBase(query).then((resolved) => {
                        loadingData.value = false;

                        Promise.all(resolved.subtasks).then(() => {
                            navigating.value = false;
                        });

                        return resolved;
                    }, function (e) {
                        hasLoadedOnce.value = prepareErrorMessage(e);
                        loadingData.value = false;
                        throw e;
                    });
                };

                const pagerData = computed(() => (resources.value && resources.value.pager) || {});

                returns.push({
                    hasLoadedOnce,
                    resources,
                    resourceIndexFieldMap: (resource) => resource._.indexFields,
                    resourceLabel: computed(() => label.value || resources.value.label),
                    showRefreshToggle: computed(
                        () => (resources.value && resources.value.showRefreshToggle) || hasLoadedOnce.value !== true
                    ),
                    totalResources,
                    loadingData,
                    navigating,
                    navigate,
                    pagerData,
                    perPageOptionSize: computed(() => pagerData.value && pagerData.value.perPageOptions && pagerData.value.perPageOptions.length),

                    impersonate(row) {},
                });

                // etc
                const pollingToggle = ref(null);
                const canPoll = () =>
                    !globalIdle.value && !navigating.value && resources.value.pollingInterval && pollingToggle.value;
                const navigatePoll = () => {
                    return navigateBase({}, pollNavigateCanceller).then((resolved) =>
                        Promise.all(resolved.subtasks)
                    );
                };
                let pollingTimer;
                watch(pollingToggle, (state) => {
                    if (state) {
                        (function createPollingCallback() {
                            clearTimeout(pollingTimer);
                            pollingTimer = setTimeout(
                                () =>
                                    (canPoll() ? navigatePoll() : Promise.resolve()).then(() =>
                                        timeoutRaf(createPollingCallback)
                                    ),
                                (resources.value.pollingInterval || 1) * 1000
                            );
                        })();
                    } else {
                        clearTimeout(pollingTimer);
                    }
                });

                returns.push({
                    pollingToggle,

                    refresh({ target }) {
                        if (target.tagName === 'BUTTON') {
                            target = target.children[0];
                        }

                        const animation = ['animate-spin', 'origin-center', 'animate-duration-500'];
                        target.classList.add(...animation);
                        const finishAnimation = () =>
                            target.addEventListener(
                                'animationiteration',
                                () => target.classList.remove(...animation),
                                { once: true }
                            );

                        navigate().catch(noop).then(finishAnimation);
                    },
                });

                // search
                const searchValue = ref('');
                watch(refDebounced(searchValue, (resources.value.debounce || 0.7) * 1000), (value) =>
                    navigate({
                        search: value === '' ? null : value,
                    })
                );

                returns.push({
                    searchValue,
                });

                // actions
                const indexActionToggle = ref(false);
                const indexActionTarget = ref();
                const indexSelectedAction = ref();
                const indexActiveAction = computed(() => {
                    const selectedAction = indexSelectedAction.value;

                    if (selectedAction && !selectedAction.action.standalone) {
                        const selectionSize = Object.entries(selections.value).filter(([_, c]) => c).length;
                        if (selectionSize === 0) {
                            return;
                        }
                    }

                    return selectedAction;
                });
                const runIndexAction = ({ shiftKey }, action) => {
                    if (action) {
                        indexSelectedAction.value = {
                            action,
                            toggle: true,
                            params: queryString.value,
                            immediate: shiftKey || action.withoutConfirmation,
                            selection: {
                                resourceName,
                                selectAll,
                                resources:
                                    Object.fromEntries(
                                        Object.entries(selections.value).filter((entry) => entry[1])
                                    ) || {},
                            },
                        };
                    } else {
                        indexActionToggle.value = true;
                    }
                };
                const indexActionButtonAttributes = computed(() => {
                    const activeAction = indexActiveAction.value;
                    return {
                        disabled: !activeAction,
                        class: {
                            'px-2 border border-gray-400 text-sm h-full rounded-l-none': true,
                            'parent !border-r': true,
                            'cursor-not-allowed': !activeAction,
                            'bg-blue-600 text-white hover:bg-blue-700': activeAction && (activeAction.action.withoutConfirmation || globalShiftModifier.value),
                            'all:stroke-4 all:stroke-blue-500': activeAction && !(activeAction.action.withoutConfirmation || globalShiftModifier.value),
                        },
                    };
                });

                returns.push({
                    indexActionToggle,
                    indexActionTarget,
                    indexActiveAction,
                    indexSelectedAction,
                    runIndexAction,
                    indexActionButtonAttributes,

                    hasActionsByLocation: (type, row) => {
                        if (type === 'inlineVisibility') {
                            if (!row || row.id == null) {
                                return false;
                            }
                        }

                        return (resources.value.actions || []).filter((a) => a[type]).length > 0;
                    },

                    actionsByLocation: (type) =>
                        (resources.value.actions || [])
                            .filter((a) => a[type])
                            .reduce((r, a) => {
                                const group = a.group;
                                r[group] = r[group] || [];
                                r[group].push(a);
                                return r;
                            }, {}),

                    prepareIndexAction({ action, shiftKey }) {
                        if (action) {
                            indexActionToggle.value = false;
                            indexSelectedAction.value = {
                                action,
                                toggle: false,
                                immediate: shiftKey || action.withoutConfirmation,
                            };
                        }
                    },

                    actionSubmitted(event) {
                        navigate();
                    },

                    actionCancelled(event, check) {
                        if ((check && !event) || !check) {
                            indexSelectedAction.value.toggle = false;
                        }
                    },
                });

                // filters
                const inlineFilters = computed(() => ((resources.value && resources.value.filters) || []).filter((f) => f.inline));
                const menuFilters = computed(() => ((resources.value && resources.value.filters) || []).filter((f) => !f.inline));
                returns.push({
                    inlineFilters,
                    menuFilters,
                    filterSize: computed(() => menuFilters.value.length),
                    queryFilterSize: computed(() => Object.entries(queryString.value.filters || {}).filter((e) => e[1]).length),
                    resetFilters() {
                        navigate({
                            search: null,
                            filters: null,
                        });
                    },
                });

                // 'tableStyle' => $resourceClass::$tableStyle,
                // 'showColumnBorders' => (bool) $resourceClass::$showColumnBorders,

                // Visibility
                const interactTarget = ref();
                const resourcesToggle = ref(!parentField.value);
                let intersectionObserver = new IntersectionObserver(debounce(100, (entries) => {
                    if (entries[0].intersectionRatio <= 0) return;
                    resourcesToggle.value = true;
                    intersectionObserver.disconnect();
                }));
                onMounted(() => intersectionObserver.observe(interactTarget.value));
                onBeforeUnmount(() => intersectionObserver.disconnect());

                returns.push({
                    interactTarget,
                    parentField,
                    resourcesToggle,
                });

                if (resourcesToggle.value) {
                    navigate();
                } else {
                    let navigated = false;
                    watch(resourcesToggle, (v) => {
                        if (navigated === false && v) {
                            navigated = true;
                            navigate();
                        }
                    });
                }

                return Object.assign({}, ...returns);
            },
            template: `
                <div uno-layer-default="space-y-2">
                    <div uno-layer-default="flex items-baseline space-x-2" ref="interactTarget">
                        <h2
                            uno-layer-default="text-2xl"
                            v-if="resourceLabel"
                        >{{ resourceLabel }}</h2>
                        <Btn
                            @click="resourcesToggle = !resourcesToggle"
                            v-if="field"
                        >
                            <Icon
                                name="chevron-right"
                                :class="{
                                    'w-4 transition': true,
                                    'rotate-90': resourcesToggle,
                                }"
                            />
                        </Btn>
                    </div>

                    <div
                        uno-layer-default="flex flex-wrap items-center space-x-2"
                        v-if="resourcesToggle && !resources._.relatedOneToOne && (resources.searchable || resources.authorizedToCreate)"
                    >
                        <InputSearch
                            v-if="resources.searchable"
                            v-model="searchValue"
                            class="border-gray-400"
                        />

                        <div uno-layer-default="flex-grow" />

                        <Btn
                            :href="resources._.createUrl"
                            class="py-2 px-4 text-white space-x-2 bg-gray-600 hover:bg-gray-900"
                            v-if="resources.authorizedToCreate"
                        >
                            <span>Create</span>
                            <Icon
                                name="plus"
                                class="w-5"
                            />
                        </Btn>
                    </div>

                    <div
                        uno-layer-base="divide-y"
                        uno-layer-default="bg-white border border-gray-200 divide-gray-300 rounded-lg shadow-md relative"
                        v-if="resourcesToggle"
                    >
                        <div v-if="inlineFilters.length" uno-layer-base="divide-y">
                            <h3 class="px-4 py-2 font-medium text-lg">Filters</h3>
                            <label uno-layer-default="flex items-center" v-for="filter in inlineFilters">
                                <div uno-layer-default="w-1/4 ml-4">{{ filter.name }}</div>
                                <div>
                                    <FilterProxy
                                        :filter="filter"
                                        :navigate="navigate"
                                    />
                                </div>
                            </label>
                        </div>

                        <nav
                            uno-layer-default="overflow-hidden h-12"
                            v-if="
                                (!resources.preventIndexSelection && !parentField)
                                || showRefreshToggle
                                || (resources.showPollingToggle && resources.pollingInterval)
                                || (hasActionsByLocation('indexVisibility'))
                                || filterSize
                            "
                        >
                            <nav uno-layer-default="overflow-x-auto h-24 pt-2">
                                <nav uno-layer-default="flex space-x-4 h-8 px-2">
                                    <!-- selects -->
                                    <PopperButtoned
                                        button-class="space-x-2 h-full px-2"
                                        v-if="!resources.preventIndexSelection && !parentField"
                                    >
                                        <template #button>
                                            <InputChip
                                                :indeterminate="selectAll <= 1 && selectionSize !== 0"
                                                :checked="1 < selectAll"
                                                disabled
                                                class="pointer-events-none"
                                            />

                                            <Icon
                                                name="selector"
                                                class="w-4"
                                            />
                                        </template>

                                        <template #popper>
                                            <SmallPanel>
                                                <PanelButton
                                                    @click="selectAll = selectAll === 3 ? 0 : 3"
                                                    class="items-center"
                                                >
                                                    <span>Select all</span>
                                                    <InputChip
                                                        :checked="selectAll === 3"
                                                        class="ml-2 pointer-events-none"
                                                        disabled
                                                    />
                                                </PanelButton>

                                                <PanelButton
                                                    @click="selectAll = selectAll === 2 ? 0 : 2"
                                                    class="items-center"
                                                >
                                                    <span>Select matching</span>
                                                    <InputChip
                                                        :checked="selectAll === 2"
                                                        class="ml-2 pointer-events-none"
                                                        disabled
                                                    />
                                                </PanelButton>

                                                <PanelButton
                                                    @click="clearSelection()"
                                                >
                                                    Clear
                                                    <template v-if="selectionSize && selectAll <= 1">
                                                        {{ selectionSize }} selected
                                                    </template>
                                                    <template v-else>
                                                        selection
                                                    </template>
                                                </PanelButton>
                                            </SmallPanel>
                                        </template>
                                    </PopperButtoned>
                                    <div
                                        uno-layer-default="flex items-center"
                                        v-if="selectionSize && selectAll <= 1"
                                    >
                                        {{ selectionSize }} selected
                                    </div>

                                    <!-- refresh -->

                                    <button
                                        @click="refresh($event)"
                                        uno-layer-default="rounded-md block py-2 px-2 flex items-center hover:bg-gray-200"
                                        title="Refresh"
                                        type="button"
                                        v-if="showRefreshToggle"
                                    >
                                        <Icon
                                            name="mod-refresh"
                                            class="w-4"
                                        />
                                    </button>

                                    <!-- spacer -->
                                    <div uno-layer-default="flex-grow" />

                                    <!-- polling -->
                                    <button
                                        :disabled="pollingToggle === null"
                                        @click="pollingToggle = !pollingToggle"
                                        uno-layer-default="rounded-md block py-2 px-2 flex items-center hover:bg-gray-200"
                                        title="Toggle polling"
                                        type="button"
                                        v-if="resources.showPollingToggle && resources.pollingInterval"

                                        :class="{
                                            'cursor-not-allowed text-gray-400': pollingToggle === null,
                                            'text-green-700': pollingToggle,
                                            'text-red-700': pollingToggle !== null && !pollingToggle,
                                        }"
                                    >
                                        <Icon
                                            name="clock"
                                            class="w-4"
                                        />
                                    </button>

                                    <!-- actions -->
                                    <div
                                        v-if="hasActionsByLocation('indexVisibility')"
                                        uno-layer-base="divide-x"
                                        uno-layer-default="divide-gray-400 flex items-center shrink-0"
                                    >
                                        <PopperButtoned
                                            v-model:toggle="indexActionToggle"
                                            button-class="border border-gray-400 text-sm h-full space-x-2 px-2 flex shrink-0 items-center rounded-r-none"
                                        >
                                            <template #button>
                                                <span>
                                                    <template v-if="indexSelectedAction">
                                                        {{ indexSelectedAction.action.name }}
                                                    </template>
                                                    <template v-else>
                                                        Select action
                                                    </template>
                                                </span>

                                                <Icon
                                                    name="selector"
                                                    class="w-4"
                                                />
                                            </template>

                                            <template #popper>
                                                <ActionsList
                                                    :grouped-actions="actionsByLocation('indexVisibility')"
                                                    @action-picked="prepareIndexAction($event)"
                                                />
                                            </template>
                                        </PopperButtoned>

                                        <Btn
                                            @click="runIndexAction($event, indexActiveAction && indexActiveAction.action)"
                                            v-bind="indexActionButtonAttributes"
                                        >
                                            <Icon
                                                class="w-4"
                                                name="mod-exclamation-right"
                                            />
                                        </Btn>

                                        <PopperTeleport
                                            :blur="true"
                                            :modal="true"
                                            :toggle="indexActiveAction && indexActiveAction.toggle"
                                            @update:toggle="actionCancelled($event, true)"
                                        >
                                            <ActionProxy
                                                @action-submitted="actionSubmitted($event)"
                                                @cancelled="actionCancelled($event)"
                                                v-bind="indexActiveAction"
                                            />
                                        </PopperTeleport>
                                    </div>

                                    <!-- lenses -->
                                    <!-- // -->

                                    <!-- filters -->
                                    <PopperButtoned
                                        v-if="filterSize || perPageOptionSize"
                                        :button-class="{
                                            'px-2 space-x-2 h-full shrink-0 border overflow-hidden': true,
                                            'bg-blue-600 hover:bg-blue-700 text-white': queryFilterSize,
                                        }"
                                    >
                                        <template #button>
                                            <Icon
                                                name="filter"
                                                class="w-4"
                                            />

                                            <span v-if="queryFilterSize"
                                                uno-layer-default="text-sm font-medium"
                                            >
                                                {{ queryFilterSize }}
                                            </span>

                                            <Icon
                                                name="selector"
                                                class="w-4"
                                            />
                                        </template>

                                        <template #popper>
                                            <SmallPanel>
                                                <SmallPanelLabel
                                                    @click="resetFilters()"
                                                    v-if="filterSize"
                                                    :class="{
                                                        'font-medium text-center border-b border-gray-300': true,
                                                        'hover:bg-gray-300': queryFilterSize,
                                                        'text-gray-500': !queryFilterSize,
                                                    }"
                                                >
                                                    Reset filters
                                                </SmallPanelLabel>

                                                <label v-for="filter in menuFilters">
                                                    <SmallPanelLabel>{{ filter.name }}</SmallPanelLabel>
                                                    <FilterProxy
                                                        :filter="filter"
                                                        :navigate="navigate"
                                                    />
                                                </label>

                                                <template v-if="perPageOptionSize">
                                                    <SmallPanelLabel>Per page:</SmallPanelLabel>
                                                    <div uno-layer-default="py-2 px-3">
                                                        <select
                                                            @change="navigate({ perPage: parseInt($event.target.value, 10) })"
                                                            class="form-select"
                                                            uno-layer-default="rounded-md w-full transition p-1 border text-sm border-gray-400"
                                                        >
                                                            <option v-for="perPage in pagerData.perPageOptions"
                                                                :selected="perPage === pagerData.perPage"
                                                            >
                                                                {{ perPage }}
                                                            </option>
                                                        </select>
                                                    </div>
                                                </template>
                                            </SmallPanel>
                                        </template>
                                    </PopperButtoned>
                                </nav>
                            </nav>
                        </nav>

                        <div v-if="resources.resources && resources.resources.length"
                            uno-layer-default="align-middle inline-block w-full overflow-auto max-h-[70vh]"
                        >
                            <ResourceTable
                                :fieldResolver="resourceIndexFieldMap"
                                :resource="true"
                                :resources="resources.resources"
                                :spacious="true"
                                class="
                                    ss-md:whitespace-nowrap
                                    ss-xl:whitespace-nowrap
                                "
                            >
                                <template #heading="{ index }">
                                    <ResourceTableHeading :field="resources._.indexFields[index]" />
                                </template>

                                <template #checkbox="{ row, index }">
                                    <label uno-layer-default="flex items-center space-x-2">
                                        <InputChip
                                            v-bind="row._.checkbox"
                                            v-if="!resources.preventIndexSelection && row.id != null"
                                        />
                                        <span v-if="resources.showNumberings">
                                            {{ pagerData.currentPage * pagerData.perPage - pagerData.perPage + index + 1 }}
                                        </span>
                                    </label>
                                </template>

                                <template #action="{ row }">
                                    <div
                                        uno-layer-default="flex items-center space-x-1"
                                        v-if="
                                            row._.shouldShowActions
                                            || hasActionsByLocation('inlineVisibility', row)
                                            || row.authorizedToImpersonate
                                            || row.authorizedToReplicate
                                            || row._.previewFields.length
                                        "
                                    >
                                        <ResourceActions
                                            :grouped-actions="actionsByLocation('inlineVisibility')"
                                            :resource-name="resourceName"
                                            :resource="row"
                                            @action-submitted="actionSubmitted($event)"
                                            button-class="px-2"
                                            v-if="hasActionsByLocation('inlineVisibility', row)"
                                        />

                                        <PopperButtoned
                                            button-class="space-x-2 h-full px-2"
                                            v-if="
                                                row.authorizedToImpersonate
                                                || row.authorizedToReplicate
                                                || row._.previewFields.length
                                            "
                                        >
                                            <template #button>
                                                <Icon
                                                    class="w-4"
                                                    name="dots-horizontal"
                                                />
                                            </template>

                                            <template #popper>
                                                <SmallPanel>
                                                    <ConfirmModal
                                                        v-if="row._.previewFields.length"
                                                        :confirm="true"
                                                        text="Preview"
                                                        button-class="
                                                            py-1 px-4 w-full justify-start
                                                            block rounded-0 overflow-hidden bg-white flex
                                                            focus:ring-inset focus:ring-size-[0.125rem]
                                                        "
                                                    >
                                                        <template #heading>
                                                            <ResourceModelHeading :resource="row" />
                                                        </template>

                                                        <div
                                                            uno-layer-base="divide-y"
                                                            uno-layer-default="divide-gray-200 overflow-x-auto max-h-[70vh]"
                                                        >
                                                            <DetailFieldProxy
                                                                :field="field"
                                                                :resource="row"
                                                                v-for="field in row._.previewFields"
                                                            />
                                                        </div>
                                                    </ConfirmModal>

                                                    <PanelButton
                                                        :href="row._.replicateUrl"
                                                        v-if="row.authorizedToReplicate"
                                                    >
                                                        Replicate
                                                    </PanelButton>

                                                    <PanelButton
                                                        @click="impersonate(row)"
                                                        v-if="row.authorizedToImpersonate"
                                                    >
                                                        Impersonate
                                                    </PanelButton>
                                                </SmallPanel>
                                            </template>
                                        </PopperButtoned>

                                        <Btn
                                            :href="row._.detailUrl"
                                            class="px-2"
                                            v-if="row.authorizedToView"
                                        >
                                            <Icon
                                                name="eye"
                                                class="w-4"
                                            />
                                        </Btn>

                                        <Btn
                                            :href="row._.editUrl"
                                            class="px-2"
                                            v-if="row.authorizedToUpdate"
                                        >
                                            <Icon
                                                name="pencil-alt"
                                                class="w-4"
                                            />
                                        </Btn>

                                        <ConfirmModal
                                            :shiftable="true"
                                            @click="row._.deleteResource()"
                                            button-class="px-2 focus:ring-red-600 mix-shade-10:active:ring-red-300 mix-shade-20:active:ring-red-300"
                                            shift-button-class="text-white bg-red-500 focus:bg-red-600 hover:bg-red-700"
                                            text="Delete"
                                            unshift-button-class="text-red-600"
                                            v-if="row.authorizedToDelete"
                                        >
                                            <template #heading>Delete resources?</template>
                                            <template #button><Icon name="trash" class="w-4" /></template>
                                        </ConfirmModal>
                                    </div>
                                </template>
                            </ResourceTable>
                        </div>

                        <div v-else
                            uno-layer-default="bg-gray-100 flex flex-col items-center justify-center space-y-6 pt-16 pb-12"
                        >
                            <template v-if="hasLoadedOnce">
                                <span uno-layer-default="text-xl">
                                    {{ hasLoadedOnce === true ? 'No Item' : hasLoadedOnce }}
                                </span>
                                <div
                                    uno-layer-default="flex space-x-4"
                                    v-if="hasLoadedOnce === true"
                                >
                                    <button
                                        @click="navigate({ page: 1 })"
                                        uno-layer-default="block rounded-lg overflow-hidden border border-gray-400 flex items-center space-x-2 py-2 px-2"
                                        :class="{
                                            'bg-blue-600 text-white': queryString.page && queryString.page != 1,
                                            'cursor-default': !(queryString.page && queryString.page != 1),
                                        }"
                                    >
                                        Go to page 1
                                    </button>

                                    <button
                                        @click="resetFilters()"
                                        uno-layer-default="block rounded-lg overflow-hidden border border-gray-400 flex items-center space-x-2 py-2 px-2"
                                        :class="{
                                            'bg-red-600 text-white': queryFilterSize,
                                            'cursor-default': !queryFilterSize,
                                        }"
                                    >
                                        <Icon
                                            name="filter"
                                            class="w-4"
                                        />

                                        <span>Reset filter</span>
                                    </button>
                                </div>
                            </template>
                        </div>

                        <ResourcePager
                            :navigate="navigate"
                            :resources="resources"
                            :totalResources="totalResources"
                        />

                        <TransitionWrapper
                            name="fade"
                            enterActiveClass="transition-opacity"
                            leaveActiveClass="transition-opacity"
                            enterFromClass="opacity-0"
                            leaveToClass="opacity-0"
                        >
                            <div v-if="loadingData" uno-layer-default="absolute inset-0 rounded-lg bg-gray-100 backdrop-blur-8 opacity-60 flex items-center justify-center">
                                <Spinner scale="2" />
                            </div>
                        </TransitionWrapper>
                    </div>
                </div>
            `,
        };

        const ResourceDetail = {
            components: {
                Btn,
                ConfirmModal,
                DetailFieldProxy,
                Icon,
                Panel,
                ResourceActions,
                ResourceIndex,
                ResourceModelHeading,
                Spinner,
            },
            props: {
                routes: Object,
            },
            setup(props, { emit }) {
                const { routes } = toRefs(props);
                const resourceName = computed(() => routes.value.resource);
                const resourceId = computed(() => routes.value.resourceId);
                const pageTitle = useTitle();

                // load query string
                const usp = new URLSearchParams(window.location.search);
                const queryString = ref({
                    // filters: jsonParse(usp.get(`${resourceName.value}_filters`)) || null,
                    // page: parseInt(usp.get(`${resourceName.value}_page`), 10) || null,
                    // perPage: parseInt(usp.get(`${resourceName.value}_perPage`), 10) || null,
                    // search: usp.get(`${resourceName.value}_search`) || null,
                });

                const hasLoadedOnce = ref(false);
                const resource = ref({
                    _: reactive({}),
                    resource: reactive({}),
                });

                const prepareResource = (oldValue, newValue) => {
                    cssExtract(JSON.stringify(newValue));

                    const {
                        actions,
                        authorizedToViewAny,
                        label,
                        resource,
                    } = newValue;

                    const {
                        authorizedToDelete,
                        authorizedToUpdate,
                        fields,
                        id,
                        panels,
                        title,
                    } = resource || {};

                    (actions || []).forEach((a) => {
                        const ready = ref(a.ready);
                        a._ = reactive({
                            selection: undefined,
                            immediate: false,
                            toggleReady(set) {
                                ready.value = set == null ? !ready.value : !!set;
                            },
                            ready,
                        });
                    });

                    const sections = prepareFieldsCollection(fields, panels, true);
                    const deleteUrl = `${__avon.baseUrl}/api/resource/${resourceName.value}/delete/${id}`;

                    const res = reactive({
                        ...newValue,
                        _: reactive({
                            resourceId: resource?.id,

                            deleteUrl: authorizedToDelete ? deleteUrl : undefined,
                            editUrl: authorizedToUpdate
                                ? `${__avon.baseUrl}/resource/${resourceName.value}/edit/${id}`
                                : undefined,
                            indexUrl: authorizedToViewAny
                                ? `${__avon.baseUrl}/resource/${resourceName.value}`
                                : undefined,
                            sections: Object.entries(sections).map(([kind, value]) => ({ ...value, kind })),
                            shouldShowActions:
                                (id == null ? false : (authorizedToDelete || authorizedToUpdate)) || authorizedToViewAny,
                            deleteResource: () => axiosInternal({
                                method: 'post',
                                url: deleteUrl,
                                _preventUnload: true,
                            }),
                        }),
                    });

                    pageTitle.value = `${
                        title ?? EM_DASH
                    } ${EN_DASH} ${label} Resource | ${__tmp_appName}`;

                    __LOG('resource updated', res);

                    return res;
                };

                const navigateBase = (query, cancel) => {
                    const { abort, axios } = axiosInternal._wrap({
                        method: 'get',
                        url: `${__avon.baseUrl}/api/resource/${resourceName.value}/detail/${resourceId.value}`,
                        // params,
                    });

                    if (cancel && 'cancel' in cancel) {
                        cancel.cancel = abort;
                    }

                    return axios.then(({ _abortReason, data }) => {
                        const subtasks = [];
                        if (_abortReason) return { subtasks };

                        hasLoadedOnce.value = true;
                        resource.value = prepareResource(resource.value || {}, data || {});
                        // window.history.replaceState({}, '', resourceQueryString);

                        return { subtasks };
                    });
                };

                navigateBase();

                return {
                    hasLoadedOnce,
                    resourceName,
                    resource,

                    hasActionsByLocation: (type) =>
                        resource.value
                        && resource.value.resource != null
                        && resource.value.resource.id != null
                        && (resource.value.actions || []).filter((a) => a[type]).length > 0,

                    actionsByLocation: (type) =>
                        ((resource.value && resource.value.actions) || [])
                            .filter((a) => a[type])
                            .reduce((r, a) => {
                                const group = a.group;
                                r[group] = r[group] || [];
                                r[group].push(a);
                                return r;
                            }, {}),

                    actionSubmitted(event) {
                        navigateBase();
                    },
                };
            },
            template: `
                <div
                    uno-layer-default="space-y-4"
                    v-if="hasLoadedOnce"
                >
                    <div
                        uno-layer-default="flex items-center space-x-1"
                        v-if="resource.label || resource._.shouldShowActions || hasActionsByLocation('detailVisibility')"
                    >
                        <h2
                            uno-layer-default="text-2xl"
                            v-if="resource.label"
                            v-text="resource.label"
                        />

                        <div uno-layer-default="flex-grow" />

                        <ResourceActions
                            :grouped-actions="actionsByLocation('detailVisibility')"
                            :resource-name="resourceName"
                            :resource="resource.resource"
                            @action-submitted="actionSubmitted($event)"
                            button-class="space-x-2 bg-gray-200 hover:bg-gray-300"
                            button-label="Actions"
                            v-if="hasActionsByLocation('detailVisibility')"
                        />

                        <Btn
                            :href="resource._.indexUrl"
                            class="py-3 bg-gray-200 hover:bg-gray-300"
                            v-if="resource.authorizedToViewAny"
                        >
                            <Icon
                                name="table"
                                class="w-4"
                            />
                        </Btn>

                        <Btn
                            :href="resource._.editUrl"
                            class="py-3 bg-gray-200 hover:bg-gray-300"
                            v-if="resource.resource.authorizedToUpdate"
                        >
                            <Icon
                                name="pencil-alt"
                                class="w-4"
                            />
                        </Btn>

                        <ConfirmModal
                            :shiftable="true"
                            @click="resource._.deleteResource()"
                            button-class="py-3 focus:ring-red-600 mix-shade-10:active:ring-red-300 mix-shade-20:active:ring-red-300"
                            unshift-button-class="text-red-600 bg-gray-200 hover:bg-gray-300"
                            shift-button-class="text-white bg-red-500 focus:bg-red-600 hover:bg-red-600"
                            text="Delete"
                            v-if="resource.resource.authorizedToDelete"
                        >
                            <template #heading>Delete resources?</template>
                            <template #button><Icon name="trash" class="w-4" /></template>
                        </ConfirmModal>
                    </div>

                    <div
                        uno-layer-default="space-y-4"
                        v-if="resource._.sections"
                    >
                        <template v-for="section in resource._.sections">
                            <component
                                :is="section.data.component"
                                :resource="resource"
                                :tool="section.data"
                                v-if="section.isResourceTool"
                            />

                            <Panel
                                :panel="section.data"
                                v-else-if="section.isPanel"
                            >
                                <template
                                    #heading
                                    v-if="section.data._.toolbar"
                                >
                                    <ResourceModelHeading :resource="resource.resource" />
                                </template>

                                <DetailFieldProxy
                                    :field="field"
                                    :resource="resource"
                                    v-for="field in section.data.fields"
                                />
                            </Panel>

                            <ResourceIndex
                                :field="section.data"
                                :label="section.data.name ?? section.data.indexName"
                                :routes="section.data.relation.routes"
                                v-else-if="section.data.relation && section.data.relation.routes"
                            />
                        </template>
                    </div>
                </div>
                <div
                    class="flex items-center justify-center min-h-[40vh]"
                    v-else
                >
                    <Spinner scale="2" />
                </div>
            `,
        };

        const ResourceEdit = {
            components: {
                Btn,
                ConfirmModal,
                FormFieldProxy,
                Icon,
                Panel,
                ResourceModelHeading,
                Spinner,
            },
            props: {
                routes: Object,
            },
            setup(props, { emit }) {
                const { routes } = toRefs(props);
                const resourceName = computed(() => routes.value.resource);
                const resourceId = computed(() => routes.value.resourceId);
                const pageTitle = useTitle();

                const hasLoadedOnce = ref(false);
                const resource = ref({
                    _: reactive({}),
                    resource: reactive({}),
                });

                let fieldsRef = [];

                const setFieldRef = (field) => {
                    fieldsRef.push(field);
                };

                const prepareResource = (oldValue, newValue) => {
                    cssExtract(JSON.stringify(newValue));

                    fieldsRef = [];

                    const {
                        authorizedToViewAny,
                        label,
                        resource,
                    } = newValue;

                    const {
                        authorizedToDelete,
                        authorizedToView,
                        fields,
                        id,
                        panels,
                        title,
                    } = resource || {};

                    const sections = prepareFieldsCollection(fields, panels);
                    const deleteUrl = `${__avon.baseUrl}/api/resource/${resourceName.value}/delete/${id}`;

                    const res = reactive({
                        ...newValue,
                        _: reactive({
                            resourceId: resource.id,
                            resourceName: resourceName.value,

                            deleteUrl: authorizedToDelete ? deleteUrl : undefined,
                            detailUrl: authorizedToView
                                ? `${__avon.baseUrl}/resource/${resourceName.value}/detail/${id}`
                                : undefined,
                            indexUrl: authorizedToViewAny
                                ? `${__avon.baseUrl}/resource/${resourceName.value}`
                                : undefined,
                            sections: Object.entries(sections).map(([kind, value]) => ({ ...value, kind })),
                            shouldShowActions:
                                (id == null ? false : (authorizedToDelete || authorizedToView)) || authorizedToViewAny,
                            deleteResource: () => axiosInternal({
                                method: 'post',
                                url: deleteUrl,
                                _preventUnload: true,
                            }),
                        }),
                    });

                    pageTitle.value = `Editing: ${
                        title ?? EM_DASH
                    } ${EN_DASH} ${label} Resource | ${__tmp_appName}`;

                    __LOG('resource updated', res);

                    return res;
                };

                const navigateBase = (query, cancel) => {
                    if (cancel && 'cancel' in cancel) {
                        cancel.cancel = () => {
                            cancelled = true;
                            canceller();
                        };
                    }

                    return axiosInternal({
                        method: 'post',
                        data: gatherForm(),
                        url: `${__avon.baseUrl}/api/resource/${resourceName.value}/edit/${resourceId.value}`,
                        // params,
                    }).then(({ _abortReason, data }) => {
                        const subtasks = [];
                        if (_abortReason) return { subtasks };

                        hasLoadedOnce.value = true;
                        resource.value = prepareResource(resource.value || {}, data || {});
                        // window.history.replaceState({}, '', resourceQueryString);

                        return { subtasks };
                    });
                };

                const gatherForm = () => {
                    const formData = new FormData();

                    fieldsRef
                        .map((ref) => ref?.fill)
                        .filter(Boolean)
                        .forEach((fill) => fill(formData));

                    // (this.resourceData('panels') || []).forEach(function(panel) {
                    //     if (panel.isResourceTool && panel.fill) {
                    //         panel.fill(formData);
                    //     }

                    return formData;
                };

                formRenavigate(gatherForm, navigateBase);

                const abandonement = (e) => {
                    if (resource.value.preventFormAbandonment && [...gatherForm().keys()].length) {
                        e.preventDefault();
                        return (e.returnValue = '');
                    }
                };

                window.addEventListener('beforeunload', abandonement);
                onBeforeUnmount(() => window.removeEventListener('beforeunload', abandonement));

                return {
                    hasLoadedOnce,
                    resourceName,
                    resource,

                    setFieldRef,

                    save(event, continueEdit) {
                        const formData = gatherForm();

                        formData.append('_retrieved_at', resource.value.resource.retrieved_at);
                        // formData.append('_submit', continueEdit ? 'edit' : 'detail');

                        axiosInternal({
                            method: 'post',
                            url: `${__avon.baseUrl}/api/resource/${resourceName.value}/update/${resourceId.value}`,
                            data: formData,
                            _preventUnload: true,
                        }).then((response) => {
                            //
                        });
                    },
                };
            },
            template: `
                <div
                    uno-layer-default="space-y-4"
                    v-if="hasLoadedOnce"
                >
                    <div
                        uno-layer-default="flex items-center space-x-1"
                        v-if="resource.label || resource._.shouldShowActions"
                    >
                        <h2
                            uno-layer-default="text-2xl"
                            v-if="resource.label"
                            v-text="resource.label"
                        />

                        <div uno-layer-default="flex-grow" />

                        <Btn
                            :href="resource._.indexUrl"
                            class="py-3 bg-gray-200 hover:bg-gray-300"
                            v-if="resource.authorizedToViewAny"
                        >
                            <Icon
                                name="table"
                                class="w-4"
                            />
                        </Btn>

                        <Btn
                            :href="resource._.detailUrl"
                            class="py-3 bg-gray-200 hover:bg-gray-300"
                            v-if="resource.resource.authorizedToView"
                        >
                            <Icon
                                name="eye"
                                class="w-4"
                            />
                        </Btn>

                        <ConfirmModal
                            :shiftable="true"
                            @click="resource._.deleteResource()"
                            button-class="py-3 focus:ring-red-600 mix-shade-10:active:ring-red-300 mix-shade-20:active:ring-red-300"
                            shift-button-class="text-white bg-red-500 focus:bg-red-600 hover:bg-red-600"
                            text="Delete"
                            unshift-button-class="text-red-600 bg-gray-200 hover:bg-gray-300"
                            v-if="resource.resource.authorizedToDelete"
                        >
                            <template #heading>Delete resources?</template>
                            <template #button><Icon name="trash" class="w-4" /></template>
                        </ConfirmModal>
                    </div>

                    <fieldset
                        uno-layer-default="space-y-4"
                        v-if="resource._.sections"
                    >
                        <template v-for="section in resource._.sections">
                            <component
                                :is="section.data.component"
                                :resource="resource"
                                :tool="section.data"
                                v-if="section.isResourceTool"
                            />

                            <Panel
                                :panel="section.data"
                                v-else-if="section.isPanel"
                            >
                                <template
                                    #heading
                                    v-if="section.data._.toolbar"
                                >
                                    <ResourceModelHeading :resource="resource.resource" />
                                </template>

                                <FormFieldProxy
                                    :field="field"
                                    :ref="setFieldRef"
                                    :resource="resource"
                                    v-for="field in section.data.fields"
                                />
                            </Panel>
                        </template>
                    </fieldset>

                    <div
                        v-if="hasLoadedOnce && resource && resource.resource.id != null" uno-layer-default="flex items-center justify-end space-x-2"
                    >
                        <ConfirmModal
                            :shiftable="true"
                            @click="save($event, 1)"
                            button-class="px-4 text-white bg-gray-600 hover:bg-gray-900"
                            text="Update & Continue Edit"
                        >
                            <div uno-layer-default="p-4">Konfirmasi update?</div>
                        </ConfirmModal>
                        <ConfirmModal
                            :shiftable="true"
                            @click="save($event, 0)"
                            button-class="px-4 text-white bg-gray-600 hover:bg-gray-900"
                            text="Update"
                        >
                            <div uno-layer-default="p-4">Konfirmasi update?</div>
                        </ConfirmModal>
                    </div>
                </div>
                <div
                    uno-layer-default="flex items-center justify-center min-h-[40vh]"
                    v-else
                >
                    <Spinner scale="2" />
                </div>
            `,
        };

        const ResourceCreate = {
            components: {
                Btn,
                ConfirmModal,
                FormFieldProxy,
                Icon,
                Panel,
                PopperButtoned,
                ResourceModelHeading,
                Spinner,
            },
            props: {
                routes: Object,
                replicateId: [String, Number],
            },
            setup(props, { emit }) {
                const { replicateId, routes } = toRefs(props);

                const resourceName = computed(() => routes.value.resource);
                const pageTitle = useTitle();

                const hasLoadedOnce = ref(false);
                const resource = ref({
                    _: reactive({}),
                    resource: reactive({}),
                });

                let fieldsRef = [];

                const setFieldRef = (field) => {
                    fieldsRef.push(field);
                };

                const prepareResource = (oldValue, newValue) => {
                    cssExtract(JSON.stringify(newValue));

                    fieldsRef = [];

                    const {
                        authorizedToViewAny,
                        label,
                        resource,
                    } = newValue;

                    const {
                        fields,
                        id,
                        panels,
                        title,
                    } = resource || {};

                    const sections = prepareFieldsCollection(fields, panels);

                    const res = reactive({
                        ...newValue,
                        _: reactive({
                            indexUrl: authorizedToViewAny
                                ? `${__avon.baseUrl}/resource/${resourceName.value}`
                                : undefined,
                            sections: Object.entries(sections).map(([kind, value]) => ({ ...value, kind })),
                            shouldShowActions: authorizedToViewAny,
                        }),
                    });

                    pageTitle.value = `New ${label} Resource | ${__tmp_appName}`;

                    __LOG('resource updated', res);

                    return res;
                };

                const navigateBase = (query, cancel) => {
                    const { abort, axios } = axiosInternal._wrap({
                        method: 'post',
                        data: gatherForm(),
                        url: replicateId.value == null
                            ? `${__avon.baseUrl}/api/resource/${resourceName.value}/create`
                            : `${__avon.baseUrl}/api/resource/${resourceName.value}/replicate/${replicateId.value}`,
                        // params,
                    });

                    if (cancel && 'cancel' in cancel) {
                        cancel.cancel = abort;
                    }

                    return axios.then(({ _abortReason, data }) => {
                        const subtasks = [];
                        if (_abortReason) return { subtasks };

                        hasLoadedOnce.value = true;
                        resource.value = prepareResource(resource.value || {}, data || {});
                        // window.history.replaceState({}, '', resourceQueryString);

                        return { subtasks };
                    });
                };

                const gatherForm = () => {
                    const formData = new FormData();

                    fieldsRef
                        .map((ref) => ref?.fill)
                        .filter(Boolean)
                        .forEach((fill) => fill(formData));

                    // (this.resourceData('panels') || []).forEach(function(panel) {
                    //     if (panel.isResourceTool && panel.fill) {
                    //         panel.fill(formData);
                    //     }

                    return formData;
                };

                formRenavigate(gatherForm, navigateBase);

                const abandonement = (e) => {
                    if (resource.value.preventFormAbandonment && [...gatherForm().keys()].length) {
                        e.preventDefault();
                        return (e.returnValue = '');
                    }
                };

                window.addEventListener('beforeunload', abandonement);
                onBeforeUnmount(() => window.removeEventListener('beforeunload', abandonement));

                return {
                    hasLoadedOnce,
                    resourceName,
                    resource,

                    setFieldRef,

                    save(event, continueAdd) {
                        const formData = gatherForm();

                        formData.append('_retrieved_at', resource.value.resource.retrieved_at);
                        // formData.append('_submit', continueAdd ? 'add' : 'detail');

                        axiosInternal({
                            method: 'post',
                            url: `${__avon.baseUrl}/api/resource/${resourceName.value}/store`,
                            data: formData,
                            _preventUnload: true,
                        }).then((response) => {
                            //
                        });
                    },
                };
            },
            template: `
                <div
                    uno-layer-default="space-y-4"
                    v-if="hasLoadedOnce"
                >
                    <div
                        uno-layer-default="flex items-center space-x-1"
                        v-if="resource.label || resource._.shouldShowActions"
                    >
                        <h2
                            uno-layer-default="text-2xl"
                            v-if="resource.label"
                        >
                            <small v-if="replicateId" uno-layer-default="font-medium">Replicating: </small>
                            {{ resource.label }}
                        </h2>

                        <div uno-layer-default="flex-grow" />

                        <Btn
                            :href="resource._.indexUrl"
                            class="py-3 bg-gray-200 hover:bg-gray-300"
                            v-if="resource.authorizedToViewAny"
                        >
                            <Icon
                                name="table"
                                class="w-4"
                            />
                        </Btn>
                    </div>

                    <fieldset
                        uno-layer-default="space-y-4"
                        v-if="resource._.sections"
                    >
                        <template v-for="section in resource._.sections">
                            <component
                                :is="section.data.component"
                                :resource="resource"
                                :tool="section.data"
                                v-if="section.isResourceTool"
                            />

                            <Panel
                                :panel="section.data"
                                v-else-if="section.isPanel"
                            >
                                <template
                                    #heading
                                    v-if="section.data._.toolbar"
                                >
                                    <ResourceModelHeading :resource="resource.resource" />
                                </template>

                                <FormFieldProxy
                                    :field="field"
                                    :ref="setFieldRef"
                                    :resource="resource"
                                    v-for="field in section.data.fields"
                                />
                            </Panel>
                        </template>
                    </fieldset>

                    <div
                        v-if="hasLoadedOnce" uno-layer-default="flex items-center justify-end space-x-2"
                    >
                        <ConfirmModal
                            :shiftable="true"
                            @click="save($event, 1)"
                            button-class="px-4 text-white bg-gray-600 hover:bg-gray-900"
                            text="Create & Add New"
                        >
                            <div uno-layer-default="p-4">Konfirmasi buat baru?</div>
                        </ConfirmModal>
                        <ConfirmModal
                            :shiftable="true"
                            @click="save($event, 0)"
                            button-class="px-4 text-white bg-gray-600 hover:bg-gray-900"
                            text="Create"
                        >
                            <div uno-layer-default="p-4">Konfirmasi buat baru?</div>
                        </ConfirmModal>
                    </div>
                </div>
                <div
                    class="flex items-center justify-center min-h-[40vh]"
                    v-else
                >
                    <Spinner scale="2" />
                </div>
            `,
        };

        const ResourceReplicate = {
            components: {
                ResourceCreate,
            },
            props: {
                routes: Object,
            },
            template: `
                <ResourceCreate
                    :routes="routes"
                    :replicateId="routes.resourceId"
                />
            `,
        };

        const Dashboard = {
            components: {
                Btn,
                Icon,
                ...dashboardComponents,
            },
            props: {
                label: String,
                routes: Object,
            },
            setup(props) {
                const { app, configured } = window.__avon_runtime;
                const { label, routes } = toRefs(props);
                const dashboardName = computed(() => routes.value.dashboard);
                const pageTitle = useTitle();
                const title = ref('');

                watch(title, (title) => {
                    pageTitle.value = `${
                        title || EM_DASH
                    } ${EN_DASH} Dashboard | ${__tmp_appName}`;
                });

                title.value = label.value;

                const dashboardBaseUrl = `${__avon.baseUrl}/api/dashboard/${dashboardName.value}`;

                const dashboardData = ref({});
                const metricsList = ref([]);

                let loadingTimer;
                const loader = async (refresh) => {
                    try {
                        const { data } = await axiosInternal({
                            method: 'get',
                            url: dashboardBaseUrl,
                        });

                        const { label, metrics } = data || {};
                        dashboardData.value = data || {};
                        title.value = label;
                        if (refresh) {
                            metricsList.value = [];
                            nextTick(() => metricsList.value = metrics);
                        } else {
                            metricsList.value = metrics;
                        }
                    } catch (e) {
                    }

                    timeoutRaf(() => {
                        loadingTimer = setTimeout(loader, 30 * 1000 + Math.random() * 1000);
                    });
                };

                onMounted(() => {
                    timeoutRaf(() => {
                        loadingTimer = setTimeout(loader, Math.floor(Math.random() * 100));
                    });
                });

                onBeforeUnmount(() => {
                    clearTimeout(loadingTimer);
                });

                return {
                    title,
                    dashboardData,
                    metricsList,
                    refresh() {
                        clearTimeout(loadingTimer);
                        loader(true);
                    },
                };
            },
            template: `
                <div
                    uno-layer-default="flex items-center gap-2 mb-2"
                    v-if="title"
                >
                    <h2
                        uno-layer-default="text-2xl"
                        v-text="title"
                    />
                    <Btn
                        @click="refresh($event)"
                    ><Icon name="refresh" /></Btn>
                </div>

                <div v-if="metricsList.length">
                    <div
                        uno-layer-default="flex items-stretch flex-wrap -m-2 lg:grid lg:grid-cols-6 lg:grid-flow-row-dense"
                        class="[*_&.masonry]:[grid-template-rows:masonry]"
                    >
                        <component
                            :dashboard="dashboardData"
                            :is="metric.component"
                            :metric="metric"
                            :resource="null"
                            :routes="routes"
                            v-for="metric in metricsList"
                        />
                    </div>
                </div>
            `,
        };

        const Tool = {
            components: {
                ComponentLoader,
            },
            props: {
                label: String,
                routes: Object,
            },
            setup(props) {
                const { app, configured } = window.__avon_runtime;
                const { label, routes } = toRefs(props);
                const toolName = computed(() => routes.value.tool);
                const pageTitle = useTitle();
                const componentName = ref();

                const toolBaseUrl = `${__avon.baseUrl}/api/tool/${toolName.value}`;
                let currentSignature = '';

                pageTitle.value = `${
                    label.value ?? EM_DASH
                } ${EN_DASH} Tool | ${__tmp_appName}`;

                const viewProcessor = (response) => {
                    if (! response?.data) {
                        throw new Error;
                    }

                    const { component, label, payload, payloadSignature } = response.data || {};

                    if (!payloadSignature) {
                        throw new Error;
                    }

                    if (currentSignature === payloadSignature) {
                        return false;
                    }

                    currentSignature = payloadSignature;

                    const cName = `${component}-${currentSignature}`;

                    new Function('app', 'context', 'options', payload)(
                        app,
                        {
                            __avon,
                            __tmp_appName,
                            createFormToolComponent,
                            componentName: cName,
                            routes: routes.value,
                            toolBaseUrl,
                        },
                        configured,
                    );

                    componentName.value = cName;

                    pageTitle.value = `${
                        label ?? EM_DASH
                    } ${EN_DASH} Tool | ${__tmp_appName}`;

                    return true;
                };

                let viewResolver = new Promise(noop);
                (function reloadView() {
                    viewResolver = axiosInternal({
                        method: 'get',
                        url: `${toolBaseUrl}/view`,
                        params: currentSignature ? { signature: currentSignature } : {},
                    })
                        .catch(noop)
                        .then(viewProcessor)
                        .then(() => timeoutRaf(reloadView, globalIdle.value ? 10000 : 5000));
                })();

                return {
                    componentName,
                    loader: {
                        resolver: () => viewResolver,
                    },
                };
            },
            template: `
                <ComponentLoader v-bind="loader">
                    <component
                        :is="componentName"
                        v-if="componentName"
                        v-bind="{ ...$attrs, ...$props }"
                    />
                </ComponentLoader>
            `,
        };

        const UserPage = {
            components: {
                Btn,
                FormGroup,
                Panel,
                PasswordInput,
            },
            setup() {
                const user = computed(() => globalStore.value.user || {});

                const passwordForm = reactive({
                    currentPassword: '',
                    newPassword: '',
                    newPasswordConfirmation: '',
                });

                const defaultKeyMap = {
                    id: 'ID',
                    name: 'Nama',
                    type: 'Tipe',
                    team: 'Grup',
                };

                const formMessage = ref();
                watch(refDebounced(computed(() => {
                    switch (true) {
                        case passwordForm.currentPassword === '':
                            return 'Password lama tidak boleh kosong.';
                        case passwordForm.newPassword === '':
                            return 'Password baru tidak boleh kosong.';
                        case passwordForm.currentPassword === passwordForm.newPassword:
                            return 'Password baru tidak boleh sama dengan password lama.';
                        case passwordForm.newPasswordConfirmation === '':
                            return 'Konfirmasi password baru tidak boleh kosong.';
                        case passwordForm.newPassword !== passwordForm.newPasswordConfirmation:
                            return 'Konfirmasi password baru tidak cocok.';
                    }
                }), 500), (v) => formMessage.value = v);

                return {
                    passwordForm,

                    userDataList: computed(() => Object.entries(user.value)
                        // .sort(([a], [b]) => Object.keys(defaultKeyMap)[a] - Object.keys(defaultKeyMap)[b])
                        .map(([key, text]) => ({
                            field: { name: defaultKeyMap[key] ?? key },
                            text,
                        }))
                    ),

                    submit() {
                        axiosInternal({
                            method: 'post',
                            url: `${__avon.baseUrl}/api/application/user/update-password`,
                            data: passwordForm,
                            _encrypt: true,
                        });
                    },

                    formMessage,
                };
            },
            template: `
                <div uno-layer-default="space-y-8">
                    <Panel :panel="{ name: 'Data User' }">
                        <FormGroup
                            v-bind="field"
                            v-for="field in userDataList"
                        />
                    </Panel>

                    <Panel :panel="{ name: 'Update Password' }">
                        <FormGroup v-bind="{
                            field: { name: 'Password Sebelumya' },
                        }">
                            <PasswordInput
                                v-model="passwordForm.currentPassword"
                            />
                        </FormGroup>

                        <FormGroup v-bind="{
                            field: { name: 'Password Baru' },
                        }">
                            <PasswordInput
                                v-model="passwordForm.newPassword"
                            />
                        </FormGroup>

                        <FormGroup v-bind="{
                            field: { name: 'Konfirmasi Password Baru' },
                        }">
                            <PasswordInput
                                v-model="passwordForm.newPasswordConfirmation"
                            />
                        </FormGroup>

                        <FormGroup v-bind="{
                            field: { name: '' },
                        }">
                            <div>
                                <div v-if="formMessage">{{ formMessage }}</div>
                                <Btn
                                    class="border"
                                    @click="submit($event)"
                                    v-else
                                >Ganti password</Btn>
                            </div>
                        </FormGroup>
                    </Panel>
                </div>
            `,
        };

        const VerifyAccount = {
            template: `-`,
        };

        const ConfirmPassword = {
            template: `-`,
        };

        const HomePage = {
            template: `
                Hello
            `,
        };

        const MainMenu = {
            components: {
                TextInput,
                GroupMenu: {
                    components: {
                        Icon,
                        TransitionWrapper,
                        MenuItem: {
                            components: { Btn },
                            props: {
                                menu: Object,
                            },
                            setup(props) {
                                const badge = ref();

                                if (props.menu.mayHaveBadge) {
                                    let abort;
                                    const cancel = timeoutRaf(() => {
                                        const wrapped = axiosInternal._wrap({
                                            method: 'get',
                                            url: props.menu._.badgeLink,
                                        });
                                        abort = wrapped.abort;
                                    }, 3000 + Math.random() * 500);

                                    onBeforeUnmount(() => {
                                        cancel();
                                        if (abort) {
                                            abort();
                                        }
                                    });
                                }

                                return { badge };
                            },
                            template: `
                                <Btn
                                    :href="menu._.link"
                                    :class="{
                                        'bg-gray-600 hover:bg-blue-600 text-white': menu._.active,
                                        'hover:bg-blue-300': !menu._.active,
                                        'w-full py-2 px-3 break-words block rounded font-medium': true,
                                    }"
                                >
                                    {{ menu.label }}
                                </Btn>
                            `,
                        },
                    },
                    props: {
                        children: Array,
                        label: String,
                    },
                    setup() {
                        const collapsed = ref(false);
                        return { collapsed };
                    },
                    template: `
                        <div
                            @click="collapsed = !collapsed"
                            uno-layer-default="py-2 flex justify-between cursor-pointer"
                        >
                            <span uno-layer-default="break-words font-bold">{{ label }}</span>
                            <Icon
                                name="chevron-down"
                                :class="{
                                    'w-4 transition': true,
                                    'rotate--90': collapsed,
                                }"
                            />
                        </div>

                        <TransitionWrapper direction="vertical">
                            <ul v-if="!collapsed">
                                <li v-for="menu in children">
                                    <MenuItem :menu="menu" />
                                </li>
                            </ul>
                        </TransitionWrapper>
                    `,
                },
            },
            setup() {
                const fuseInstance = new Fuse([], {
                    keys: [
                        { name: 'label', weight: 3 },
                        'groupJoined',
                    ],
                    ignoreDiacritics: true,
                });

                const internalList = computed(() => {
                    const list = globalStore.value.data.menu || [];
                    list.forEach((menu) => {
                        menu.groupJoined = menu.group.join ? menu.group.join('/') : menu.group;
                    });
                    fuseInstance.setCollection(list);
                    return list;
                });

                const search = ref('');
                const filteredList = computed(() => {
                    if (search.value.trim() === '') {
                        return internalList.value;
                    }

                    return fuseInstance.search(search.value).map((item) => item.item);
                });

                return {
                    searchable: computed(() => internalList.value.length > 10),
                    search,
                    internalList,
                    filteredList,
                    mainMenu: computed(() => {
                        const groupedMenu = {};

                        filteredList.value.forEach((menu) => {
                            const group = menu.groupJoined;
                            if (!(group in groupedMenu)) {
                                groupedMenu[group] = [];
                            }

                            const path = location.pathname;
                            const link =`${__avon.baseUrl}/${menu.type}/${menu.route}`;

                            groupedMenu[group].push({
                                ...menu,
                                _: reactive({
                                    link,
                                    badgeLink: `${__avon.baseUrl}/api/${menu.type}/${menu.route}/badge`,
                                    active: path === link || path.startsWith(`${link}/`),
                                }),
                            });
                        });

                        return groupedMenu;
                    }),
                };
            },
            template: `
                <nav>
                    <template v-if="internalList.length">
                        <template v-if="searchable">
                            <div>
                                <TextInput
                                    v-model="search"
                                    autocapitalize="off"
                                    autocomplete="off"
                                    autofill="off"
                                    class="w-full"
                                    placeholder="Cari menu"
                                />
                            </div>

                            <hr class="mx-8 border-t border-gray-400 my-6">
                        </template>

                        <ul
                            v-if="filteredList.length"
                            v-for="(children, group) in mainMenu"
                            :key="group"
                        >
                            <li>
                                <GroupMenu
                                    :label="group"
                                    :children="children"
                                />
                            </li>
                        </ul>
                        <div
                            v-else
                            class="text-center font-italic"
                        >
                            Menu tidak ditemukan
                        </div>
                    </template>
                    <div
                        v-else
                        class="text-center font-italic"
                    >
                        ~
                    </div>
                </nav>
            `,
        };

        const MainPageProxy = {
            components: {
                HomePage: defineAsyncComponent(() => {
                    const instance = getCurrentInstance();
                    return Promise.resolve(instance.appContext.components.HomePage || HomePage);
                }),

                ResourceCreate,
                ResourceDetail,
                ResourceEdit,
                ResourceIndex,
                ResourceReplicate,
                Dashboard,
                Tool,

                UserPage,
                ConfirmPassword,
                VerifyAccount,
            },
            setup() {
                const getRoutes = () => {
                    let routeString = window.location.pathname ?? `${window.location}`.replace(/^[^/]+\/\/[^/]+/, '').replace(/\?.*$/, '');
                    if (routeString.startsWith(__avon.baseUrl)) {
                        routeString = routeString.replace(__avon.baseUrl, '');
                    }

                    return routeString.replace(/^\//, '').split(/\//);
                };

                const routeData = () => {
                    const routes = getRoutes();
                    __LOG('routes', routes);

                    switch (routes[0]) {
                        case 'resource':
                            return {
                                routes: {
                                    resource: routes[1],
                                    resourceId: [
                                        'detail',
                                        'edit',
                                        'replicate',
                                    ].includes(routes[2]) ? routes[3] : undefined,
                                },
                            };

                        case 'dashboard':
                        case 'tool':
                            return {
                                routes: {
                                    [routes[0]]: routes[1],
                                },
                            };

                        default:
                            return {};
                    }
                };

                return {
                    component: __avon._component,
                    page: computed(routeData),
                };
            },
            template: `<component
                :is="component"
                v-bind="page"
            />`,
        };

        const GlobalSearch = {
            components: {
                Icon,
                InputSearch,
                PanelButton,
                PopperTeleport,
                ResourceModelPreview,
                SmallPanel,
                SmallPanelLabel,
                Spinner,
            },
            props: {
                resources: Array,
            },
            setup(props) {
                const { resources } = toRefs(props);

                const popperToggle = ref(false);
                const inputValue = ref('');
                const searchQueries = ref({});
                const searchResults = ref({});
                const searching = ref(false);
                let lastSearchValue = '';

                const inputRef = ref();
                const debounceTime = computed(() => (globalStore.value.data.app.globalSearchDebounce || 0.5) * 1000);

                let requestControl;
                const searchResource = (r, q) => {
                    const { axios, ...controls } = axiosInternal._wrap({
                        method: 'get',
                        url: `${__avon.baseUrl}/api/resource/${r}/search`,
                        params: { q },
                        _abortControllers: requestControl && requestControl.control,
                    });

                    if (!requestControl) {
                        requestControl = controls;
                    }

                    return axios.catch(noop).then(({ data }) => {
                        searchResults.value[r] = (data && data.search) || [];
                    });
                };

                const searchResources = (query) => {
                    searchQueries.value = {};
                    searchResults.value = {};
                    requestControl = null;
                    return resources.value
                        .filter(({ resourceName }) => resourceName)
                        .map(({ resourceName }) => searchResource(resourceName, query));
                };

                const performSearch = (query) => {
                    lastSearchValue = query;
                    searching.value = true;
                    // cancel old search
                    // showSearchPanel();
                    const queries = searchResources(query);
                    Promise.allSettled(queries).then(() => {
                        searching.value = false;
                    });
                };

                watch(refDebounced(inputValue, debounceTime), (v) => {
                    v = v === "" ? null : v;
                    popperToggle.value = true;
                    if (!searching.value && v !== '' && lastSearchValue !== v) {
                        performSearch(v);
                    }
                });

                const searchResult = computed(() => {
                    const result = [];

                    resources.value.map(({ resourceName, label }) => {
                        let labelled;
                        (searchResults.value[resourceName] || []).map((item) => {
                            if (!labelled) {
                                labelled = true;
                                item._label = label;
                            }
                            result.push(item);
                        });
                    });

                    return result;
                });

                return {
                    popperToggle,
                    inputRef,
                    inputValue,
                    searching,
                    searchResult,

                    enterKeydown() {
                        const v = inputValue.value;
                        if (v !== '') {
                            popperToggle.value = true;
                        }

                        if (!searching.value && v !== '') {
                            performSearch(v);
                        }
                    },

                    focused() {
                        setTimeout(() => {
                            if (inputValue.value.trim() !== '') {
                                popperToggle.value = true;
                            }
                        }, 1000);
                    },
                };
            },
            template: `
                <InputSearch
                    @focus="focused"
                    @keydown.enter="enterKeydown()"
                    class="border-gray-400"
                    ref="inputRef"
                    v-model="inputValue"
                />

                <PopperTeleport
                    :target="inputRef"
                    :untrap="true"
                    v-model:toggle="popperToggle"
                >
                    <SmallPanel>
                        <div
                            uno-layer-default="px-12 py-8 bg-gray-200 flex items-center space-x-2"
                            v-if="!searching && searchResult.length === 0"
                        >
                            No result
                        </div>

                        <template v-else>
                            <div v-if="searching" uno-layer-default="py-2 px-3 bg-gray-200 flex items-center space-x-2">
                                <span>Searching</span>
                                <Spinner scale="1.5" />
                            </div>

                            <template v-for="resource in searchResult">
                                <SmallPanelLabel v-if="resource._label">{{ resource._label }}</SmallPanelLabel>
                                <PanelButton :href="resource.url" class="leading-snug">
                                    <ResourceModelPreview
                                        :plainText="true"
                                        :model="resource"
                                    />
                                </PanelButton>
                            </template>
                        </template>
                    </SmallPanel>
                </PopperTeleport>
            `,
        };

        const ThemeControl = {
            components: {
                Btn,
                Icon,
                PopperButtoned,
            },
            setup() {
                const resizeEvent = () => timeoutRaf(() => window.dispatchEvent(new Event('resize')));
                const theme = computed(() => globalStore.value.data.theme || {});
                const storeTheme = (component) => {
                    Object.assign(globalStore.value.data.theme, component);
                };

                const darkColorMode = computed(() => theme.value.prefersColorScheme);
                const darkColorState = ref(null);
                const darkStyleContent = generateCssBody('transition-300').then((s) => `html,[class*="dark\:"]{${s}}`);
                const darkClass = 'dark-poor';
                watch(darkColorState, (val) => {
                    const html = __html();

                    darkStyleContent.then((s) => transitionWait(s, (finish) => {
                        if (val === false) {
                            html.classList.remove(darkClass);
                        } else if (val === true) {
                            html.classList.add(darkClass);
                        }

                        setTimeout(finish, 300);
                    }));

                    sessionStorage.setItem(__avon.appId + '-global-prefersColorSchemeDark', val ? '1' : '');
                });
                let timeCheckInterval;
                const setColorSchemePreference = (mode) => {
                    storeTheme({ prefersColorScheme: mode });
                    if (timeCheckInterval) {
                        clearInterval(timeCheckInterval);
                        timeCheckInterval = null;
                    }

                    switch (mode) {
                        case 'light':
                            darkColorState.value = false;
                            return;

                        case 'dark':
                            darkColorState.value = true;
                            return;

                        case 'time':
                            const set = () => {
                                const date = new Date();
                                const hourMinute = date.getHours() * 100 + date.getMinutes();
                                darkColorState.value = hourMinute <= 500 || hourMinute >= 1900; // TODO: set by option
                            };
                            timeCheckInterval = setInterval(set, 60 * 1000);
                            set();
                            return;
                    }

                    darkColorState.value = __html().classList.contains('prefers-dark');
                };
                setColorSchemePreference(darkColorMode.value);
                watch(darkColorMode, (value) => setColorSchemePreference(value));

                // screen size
                const screenSizes = {
                    xs: 'Mobile',
                    md: 'Desktop',
                    // xl: 'Wide',
                };
                const defaultScreenSize =
                    Object.keys(screenSizes)
                        .map((e) => __html().className.includes(`size_${e}`) && e)
                        .filter(Boolean)[0] || 'md';
                const screenSize = computed(() => theme.value.screenSize || defaultScreenSize);
                const screenSizesRE = new RegExp(`\\bsize_(${Object.keys(screenSizes).join('|')})\\b`, 'g');
                const setScreenType = (val) => {
                    storeTheme({ screenSize: val });
                    const html = __html();
                    html.className = `${html.className.replace(screenSizesRE, '')} size_${val}`;
                    resizeEvent();
                    sessionStorage.setItem(__avon.appId + '-global-screenSize', val);
                };
                setScreenType(screenSize.value);
                watch(screenSize, (value) => setScreenType(value));

                // font size
                const htmlFontSize = computed(() => theme.value.htmlFontSize || (__html().style.fontSize || '').replace(/%$/, '') || 100);
                const setFontSize = (val) => {
                    storeTheme({ htmlFontSize: val || 100 });
                    const css = val ? val + '%' : '';
                    __html().style.fontSize = css || null;
                    resizeEvent();
                    sessionStorage.setItem(__avon.appId + '-global-htmlFontSize', css);
                };
                setFontSize(htmlFontSize.value);
                watch(htmlFontSize, (value) => setFontSize(value));

                return {
                    darkColorMode,
                    darkColorState,
                    setColorSchemePreference,

                    screenSize,
                    setScreenType,

                    htmlFontSize,
                    setFontSize,

                    colorSchemes: [
                        { scheme: null, label: 'System' },
                        { scheme: 'light', label: 'Light' },
                        { scheme: 'dark', label: 'Dark' },
                        { scheme: 'time', label: 'Time' },
                    ],
                    htmlFontSizes: [
                        50, 80, 100, 120, 150,
                    ],
                    screenSizes,

                    btnClass: (index, list, match) => ({
                        'bg-gray-600 text-white cursor-auto': match,
                        'dark:bg-gray-800': match,
                        'hover:bg-gray-600 hover:text-white dark:bg-gray-600 dark:hover:bg-gray-800': true,
                        'rounded-l-[0.5em] border-l': index == 0,
                        'rounded-r-[0.5em] !border-r': list.length == index + 1,
                        'transition rounded-none border-y': true,
                        'focus:ring-size-[0.25em] py-[0.5em] px-[0.75em] text-size-[1em] leading-[1.5em]': true,
                    }),

                    panelStyle: computed(() => ({
                        fontSize: `${10000 / (htmlFontSize.value || 100)}%`,
                    })),
                };
            },

            template: `
                <PopperButtoned
                    button-class="inline-block rounded-full flex justify-center items-center p-1 border"
                    wrapper-class="all-transition-none"
                >
                    <template #button>
                        <Icon v-if="darkColorState" name="moon" title="Dark theme" class="w-5" />
                        <Icon v-else name="sun" title="Light theme" class="w-5" />
                    </template>

                    <template #popper>
                        <div
                            uno-layer-default="transition-none rounded-lg bg-white border border-gray-400 p-[1em] shadow space-y-[1em]"
                            :style="panelStyle"
                        >
                            <div uno-layer-default="font-medium">Theme</div>
                            <div
                                uno-layer-base="divide-x"
                                uno-layer-default="flex"
                            >
                                <Btn v-for="(scheme, index) in colorSchemes"
                                    :disabled="darkColorMode == scheme.scheme"
                                    @click="setColorSchemePreference(scheme.scheme)"
                                    :class="btnClass(index, colorSchemes, darkColorMode == scheme.scheme)"
                                >{{ scheme.label }}</Btn>
                            </div>

                            <div uno-layer-default="font-medium">Font Size</div>
                            <div
                                uno-layer-base="divide-x"
                                uno-layer-default="flex"
                            >
                                <Btn v-for="(size, index) in htmlFontSizes"
                                    :disabled="htmlFontSize == size"
                                    @click="setFontSize(size)"
                                    :class="btnClass(index, htmlFontSizes, htmlFontSize == size)"
                                >{{ size ? size + '%' : '100%' }}</Btn>
                            </div>

                            <div uno-layer-default="font-medium">Screen</div>
                            <div
                                uno-layer-base="divide-x"
                                uno-layer-default="flex"
                            >
                                <Btn v-for="(name, size, index) in screenSizes"
                                    :disabled="screenSize == size"
                                    @click="setScreenType(size)"
                                    :class="btnClass(index, Object.keys(screenSizes), screenSize == size)"
                                >{{ name }}</Btn>
                            </div>
                        </div>
                    </template>
                </PopperButtoned>
            `,
        };

        const UserControl = {
            components: {
                ConfirmModal,
                PanelButton,
                PopperButtoned,
                SmallPanel,
                SmallPanelLabel,

                UserChip: {
                    props: {
                        name: String,
                        initial: String,
                    },
                    template: `
                        <div
                            uno-layer-default="space-x-2 flex items-center rounded-full whitespace-nowrap border border-transparent"
                        >
                            <div uno-layer-default="flex-none border rounded-full h-7 w-7 select-none text-white bg-gray-600 border-gray-700 flex items-center justify-center font-semibold">
                                {{ initial || (name ? name.slice(0, 1) : '?') }}
                            </div>

                            <span uno-layer-default="pr-2">{{ name }}</span>
                        </div>
                    `,
                },
            },
            props: {
                user: Object,
            },
            setup() {
                const logout = authenticator.logout;

                const autoLogoutCounter = ref(30);
                let autoLogoutTimer;
                watch(autoLogoutCounter, (counter) => {
                    if (counter === 0) {
                        logout();
                        clearInterval(autoLogoutTimer);
                    }
                });

                return {
                    userBaseUrl: (part) => `${__avon.baseUrl}/application/user/${part}`,

                    logout,

                    logoutConfirmation(event) {
                        autoLogoutCounter.value = 30;
                        if (event) {
                            autoLogoutTimer = setInterval(() => --autoLogoutCounter.value, 1000);
                        } else {
                            clearInterval(autoLogoutTimer);
                        }
                    },

                    autoLogoutHtml: computed(() =>
                        'Logout otomatis dalam <span class="tabular-nums">:d</span> :s.'
                            .replace(':d', autoLogoutCounter.value)
                            .replace(':s', autoLogoutCounter.value === 1 ? 'detik' : 'detik')
                    ),
                };
            },
            template: `
                <UserChip
                    v-if="!(user && user.name)"
                    name="NN"
                    initial="?"
                    class="hover:bg-gray-200"
                />
                <PopperButtoned
                    :unbuttoned="true"
                    v-else
                >
                    <template #button>
                        <UserChip
                            :name="user.name"
                            class="cursor-pointer hover:bg-gray-200"
                        />
                    </template>

                    <template #popper>
                        <SmallPanel>
                            <Hide SmallPanelLabel>Akun</Hide>
                            <PanelButton :href="userBaseUrl('profile')">Profil</PanelButton>
                            <Hide PanelButton :href="userBaseUrl('verify-account')">Verify Account</Hide>
                            <Hide PanelButton :href="userBaseUrl('confirm-password')">Confirm Password</Hide>

                            <Hide SmallPanelLabel>Logout</Hide>
                            <ConfirmModal
                                :shiftable="true"
                                @click="logout($event)"
                                @update:toggle="logoutConfirmation($event)"
                                text="Logout"
                                button-class="
                                    py-1 px-4 w-full justify-start
                                    block rounded-0 overflow-hidden bg-white flex
                                    hover:bg-red-600 hover:text-white

                                    focus:ring-inset focus:ring-size-[0.125rem]
                                "
                            >
                                <div uno-layer-default="p-4" v-html="autoLogoutHtml" />
                            </ConfirmModal>
                        </SmallPanel>
                    </template>
                </PopperButtoned>
            `,
        };

        const MessagesContainer = {
            components: {
                Btn,
                DayjsDisplay,
                Icon,
                PopperButtoned,

                MessagePreview: {
                    components: {
                        Btn,
                    },
                    props: { data: Object },
                    template: `
                        <div v-if="data.title" uno-layer-default="text-md font-semibold mb-2">{{ data.title }}</div>
                        <p uno-layer-default="break-words leading-snug whitespace-pre-line">{{ data.body }}</p>
                        <Btn
                            :href="data.url"
                            :title="data.url"
                            class="border border-gray-400 mt-2"
                            v-if="data.url"
                        >Linked content</Btn>
                    `,
                },
            },
            setup() {
                return {
                    messages: computed(() => globalStore.value.data.localMessages.map((m) => {
                        const data = m && m.data;
                        const type = data && data.type;
                        const level = data && data.level;

                        return {
                            ...m,
                            _: reactive({
                                typeCased: type ? type.charAt(0).toUpperCase() + type.slice(1) : 'Raw message',
                                typeClass: ({
                                    info: 'bg-green-200',
                                    danger: 'bg-red-200',
                                }[level] ?? 'bg-blue-200'),
                            }),
                        }
                    })),

                    removeMessage(reverseIndex) {
                        globalStore.store((store) => {
                            store.value.data.localMessages.splice(
                                store.value.data.localMessages.length - 1 - reverseIndex, 1
                            );
                        });
                    },

                    deleteAllMessages() {
                        globalStore.store((store) => store.value.data.localMessages = []);
                    },
                };
            },
            template: `
                <PopperButtoned
                    :blur="true"
                    button-class="inline-block rounded-full flex justify-center items-center p-1 border"
                >
                    <template #button>
                        <Icon name="bell" title="Messages" class="w-5" />
                    </template>

                    <template #popper>
                        <div
                            uno-layer-default="
                                bg-white rounded-lg shadow overflow-hidden
                            "
                        >
                            <div
                                uno-layer-default="
                                    px-6 py-4 flex items-center justify-between shadow-sm border-b border-gray-300
                                "
                            >
                                <span uno-layer-default="text-xl font-semibold">Messages</span>

                                <Btn
                                    @click="deleteAllMessages"
                                    class="select-none p-0 rounded-full"
                                    v-if="messages.length"
                                >
                                    <Icon
                                        class="w-6"
                                        name="x-circle"
                                    />
                                </Btn>
                            </div>

                            <div
                                uno-layer-default="
                                    overflow-y-auto h-full space-y-2 p-4 bg-gray-100
                                    w-[30rem] md:w-[80vw] min-h-[40vh] max-h-[70vh]
                                "
                            >
                                <template v-if="messages.length" v-for="(message, reverseIndex) in messages.reverse()">
                                    <div
                                        uno-layer-default="
                                            pt-10 px-4 pb-6 relative border border-gray-200 rounded-xl shadow-sm bg-white
                                        "
                                    >
                                        <div
                                            uno-layer-default="
                                                absolute mt-3 mr-3 top-0 right-0 text-xs text-gray-600 whitespace-nowrap
                                                flex space-x-2 items-center
                                            "
                                        >
                                            <span
                                                uno-layer-default="py-1 px-2 rounded-lg"
                                                :class="message._.typeClass"
                                            >{{ message._.typeCased }}</span>

                                            <DayjsDisplay
                                                #toggle
                                                :value="message.timestamp"
                                                duration
                                            >
                                                <Icon
                                                    class="w-3 ml-1 inline-block cursor-pointer"
                                                    name="switch-horizontal"
                                                />
                                            </DayjsDisplay>

                                            <Btn
                                                @click="removeMessage(reverseIndex)"
                                                class="select-none p-0 rounded-full !ml-4"
                                            >
                                                <Icon
                                                    class="w-4 text-gray-600"
                                                    name="x-circle"
                                                />
                                            </Btn>
                                        </div>

                                        <MessagePreview :data="message.data" />
                                    </div>
                                </template>

                                <div v-else uno-layer-default="p-8 text-center">No Messages</div>
                            </div>
                        </div>
                    </template>
                </PopperButtoned>
            `,
        };

        const messageHandlers = (() => {
            const handleStoreable = (type, action, callback) => {
                if (isString(action)) {
                    return processAction({ type, body: action });
                }

                const payload = {
                    data: action,
                    id: randomString(32),
                    timestamp: new Date(),
                    type: 'action',
                    url: location.href,
                };

                if (action.fromSession) {
                    return Promise.resolve(callback(action, payload));
                }

                if (action.session) {
                    return globalStore.store((store) => {
                        store.value.data.localMessages && store.value.data.localMessages.push(payload);
                        store.value.data.sessionMessages && store.value.data.sessionMessages.push(payload);
                    });
                }

                return Promise.allSettled([
                    globalStore.store((store) => {
                        store.value.data.localMessages && store.value.data.localMessages.push(payload);
                    }),
                    Promise.resolve(callback(action, payload)),
                ]);
            };

            const handlers = {
                _entry: (action) => handleStoreable('entry', action, noop),
                _alert: (action) => handleStoreable('alert', action, (action) => {
                    const title = action.title ? action.title + '\n\n' : '';
                    basicAlert(title + action.body);
                }),
                _message: (action) => handleStoreable('message', action, (action) => {
                    const title = action.title ? action.title + '\n\n' : '';
                    basicAlert(title + action.body);
                }),
                _notification: (action) => handleStoreable('notification', action, (action) => {
                    const title = action.title ? action.title + '\n\n' : '';
                    basicAlert(title + action.body);
                }),
            };

            function processAction(action) {
                return actionProcessor.processAction(action, handlers);
            }

            uiBootstrappers.value.push(() => {
                (function loop() {
                    globalStore.store((store) => {
                        const payload = store.value.data.sessionMessages.pop();
                        if (payload && payload.type === 'action') {
                            Promise
                                .resolve(processAction({
                                    ...(payload.data || {}),
                                    fromSession: true,
                                }))
                                .then(loop);
                        }
                    });
                })();
            });

            return {
                ...handlers,
                processAction,
            };
        })();

        const bootstrapOptions = {
            axiosWrapper: axiosInternal,
            cssExtract,
            globalBus,
            messageHandlers: {
                ...messageHandlers,
            },
            prepareFieldsCollection,
            processAction: (action) => actionProcessor.processAction(action, messageHandlers),
            locale,
            randomString,
            components: {
                Lazy,
                Spinner,
                VirtualScroller,
                TransitionExpand,
                TransitionWrapper,
                HtmlDecode,
                SimpleObjectDisplay,
                BaseDialog,
                BaseSvg,
                Icon,
                ShiftableText,
                Btn,
                ComponentLoader,
                OverlayBlocker,
                PopperTeleport,
                PopperButtoned,
                BaseModal,
                ConfirmModal,
                TextInput,
                InputChip,
                InputSearch,
                SelectList,
                DayjsDisplay,
                AutonumericDisplay,
                PasswordInput,
                AutosizeInput,
                AutonumericInput,
                FlatPickrInput,
                CodeMirrorInput,
                FileInput,
                TableDisplay,
                SelectInputButton,
                MarkdownPreview,
                Panel,
                FormGroup,
                // HeadingField,
                IndexFieldProxy,
                DetailFieldProxy,
                FormFieldProxy,
                // ActionProxy,
                // PanelButton,
                // SmallPanelLabel,
                // SmallPanel,
            },
            primeVueComponents,
        };

        const authenticator = (() => {
            const logout = () => {
                const app = (globalStore.value && globalStore.value.data && globalStore.value.data.app) || {}
                axiosInternal({
                    method: app.logoutMethod || 'delete',
                    url: app.logoutUrl || `${__avon.baseUrl}/login`,
                })
                    .then(() => globalStore.store((store) => {
                        store.value.loggedIn = false;
                    }).then(noop), noop)
                    .then(() => setTimeout(() => (location.href = __avon.loginUrl), 1000));
                return Promise.resolve();
            };

            let shouldRecheck = false;
            createIdleableInterval(() => {
                if (shouldRecheck) {
                    __LOG('recheck user data');
                    return check();
                }
            }, 111 * 1000, 333 * 1000);

            const check = () => {
                if (! location || location.protocol === 'file:') {
                    return Promise.resolve(null);
                }

                // request first
                const userRequest = axiosInternal({
                    method: 'get',
                    url: `${__avon.baseUrl}/api/application/user`,
                    _encrypt: true,
                });
                const appRequest = axiosInternal({
                    method: 'get',
                    url: `${__avon.baseUrl}/api/application/app-data`,
                });
                const menuRequest = axiosInternal({
                    method: 'get',
                    url: `${__avon.baseUrl}/api/application/menu`,
                });

                return userRequest.then((response) => {
                    const { data } = response;
                    const userData = (data && data.userData) || {};
                    __LOG('user data', userData);

                    if (! (![401, 403].includes(response.status) && (__avon.forceLogin ? Object.keys(userData).length > 0 : true))) {
                        return logout().then(() => false);
                    }

                    if (!data || data.userData === null) {
                        return logout().then(() => false);
                    }

                    return globalStore.store((store) => {
                        store.value.loggedIn = true;
                        store.value.user = userData;
                    }).then(() => {
                        globalStore.store((store) => {
                            appRequest.then(({ data }) => store.value.data.app = (data && data.appData) || {});
                            menuRequest.then(({ data }) => store.value.data.menu = (data && data.mainMenu) || []);
                        });

                        return true;
                    });
                }).then((result) => {
                    shouldRecheck = true;
                    return result;
                });
            };

            return {
                logout,
                check,
            };
        })();

        function axiosInternal(config) {
            return axiosWrapper(Object.assign({}, messageHandlers, config));
        }
        axiosInternal._wrap = (config) => axiosWrapper._wrap(Object.assign({}, messageHandlers, config));
        axiosInternal._eventStream = (config) => axiosWrapper._eventStream(Object.assign({}, messageHandlers, config));

        const App = {
            emits: ['app-mounted'],
            components: {
                Btn,
                Clock,
                GlobalSearch,
                Icon,
                MainMenu,
                MainPageProxy,
                MessagesContainer,
                ThemeControl,
                UserControl,
            },
            setup(_, { emit }) {
                onMounted(() => nextTick(() => {
                    emit('app-mounted');

                    const unload = () => uiBootstrappers.value
                        .splice(0)
                        .forEach((b) => b(null, bootstrapOptions));

                    unload();
                    nextTick(() => watch(uiBootstrappers, () => unload()));
                }));

                let globalModalLastTop = null;
                function globalModalFlag({ value }) {
                    const body = __body();
                    const target = document.getElementById('app') || body;
                    const wrapper = document.getElementById('wrapper') || body;
                    const parent = document.getElementById('wrapper-parent') || wrapper;
                    const scrollTop = window.scrollY || 0;
                    const { documentHeight, viewportHeight } = getDomDocumentSizes();

                    if (parent !== body) {
                        if (value) {
                            parent.setAttribute('inert', '');
                        } else {
                            parent.removeAttribute('inert');
                        }
                    }

                    body.classList[value ? 'add' : 'remove'](...'bg-black'.split(' '));
                    body.classList[!value ? 'add' : 'remove'](...'bg-white'.split(' '));
                    // parent.classList[value ? 'add' : 'remove'](...'scale-98 rounded-xl overflow-hidden'.split(' '));
                    parent.classList[value ? 'add' : 'remove'](...'rounded-xl overflow-hidden'.split(' '));
                    wrapper.classList[value ? 'add' : 'remove'](...'h-screen overflow-y-hidden w-full'.split(' '));
                    wrapper.classList[value && documentHeight > viewportHeight ? 'add' : 'remove'](
                        ...'overflow-y-scroll'.split(' ')
                    );
                    target.classList[value ? 'add' : 'remove'](...'relative'.split(' '));

                    if (value) {
                        if (globalModalLastTop == null) {
                            globalModalLastTop = scrollTop;
                        }
                        target.style.top = '-' + scrollTop + 'px';
                    } else {
                        target.style.top = '';
                        window.scrollTo(0, parseInt('' + globalModalLastTop || '0', 10));
                        globalModalLastTop = null;
                    }
                }

                globalBus.on('global-modal', globalModalFlag);
                onBeforeUnmount(() => {
                    globalBus.off('global-modal', globalModalFlag);
                    globalModalFlag({ value: false });
                });

                const application = computed(() => globalStore.value.data.app || {});
                const user = computed(() => globalStore.value.user || {});
                const fullscreenRef = ref();

                return {
                    avon: __avon,
                    application,
                    user,
                    fullscreenRef,

                    followerText: computed(() => {
                        const { id, type, team } = user.value;
                        return [id, type, team, application.value.headerFollower]
                            .filter(Boolean)
                            .join(` ${EM_DASH} `);
                    }),

                    globalModalFlag,

                    scrollToTop(event) {
                        event.preventDefault();
                        smoothScroll(document.getElementById('app')).catch(noop);
                    },

                    scrollToNavAnchor(event) {
                        event.preventDefault();
                        smoothScroll(document.getElementById('nav-container')).catch(noop);
                    },

                    requestFullscreen() {
                        fullscreenRef.value.requestFullscreen();
                    },
                };
            },
            template: `
                <div uno-layer-default="min-h-screen">
                    <section
                        id="header-container"
                        uno-layer-default="sticky top-0 z-30 ml-64 shadow-lg"
                        ss-xs="ml-0"
                    >
                        <header id="header" uno-layer-default="relative overflow-hidden h-12">
                            <div uno-layer-default="absolute h-12 w-full">
                                <div uno-layer-default="absolute inset-0 backdrop-blur"></div>
                                <div uno-layer-default="absolute inset-0 bg-white opacity-60"></div>
                            </div>

                            <div uno-layer-default="overflow-x-auto whitespace-nowrap h-24">
                                <div uno-layer-default="relative flex flex-no-wrap items-center h-12 px-2 space-x-2">
                                    <Btn
                                        @click="scrollToNavAnchor"
                                        class="hidden p-1 hover:text-blue-600 rounded-full"
                                        ss-xs="block"
                                        href="#nav-container"
                                    >
                                        <Icon
                                            class="w-5"
                                            name="menu"
                                        />
                                    </Btn>

                                    <Btn
                                        :href="application.siteUrl"
                                        class="font-semibold block py-1 hover:text-blue-600 rounded-full"
                                        rel="noopener noreferrer"
                                        target="_blank"
                                        v-if="application.siteUrl"
                                    >
                                        {{ application.siteName }}
                                    </Btn>

                                    <GlobalSearch
                                        :resources="application.globalSearchResources"
                                        v-if="application.globalSearchResources && application.globalSearchResources.length"
                                    />

                                    <div uno-layer-default="flex-grow" />

                                    <Btn
                                        @click="requestFullscreen"
                                        v-if="application.showFullScreenControl"
                                        class="inline-block rounded-full flex justify-center items-center p-1"
                                    >
                                        <Icon
                                            class="w-5 inline-block"
                                            name="mod-presentation-chart-blank"
                                        />
                                    </Btn>

                                    <ThemeControl v-if="application.showThemeControl" />

                                    <MessagesContainer v-if="application.showLocalMessages" />

                                    <UserControl :user="user" />
                                </div>
                            </div>
                        </header>

                        <div uno-layer-default="h-0">
                            <div uno-layer-default="py-1 px-3 text-sm font-medium border-y border-gray-200 shadow-sm flex justify-between bg-white">
                                <div>{{ followerText }}</div>
                                <div uno-layer-default="text-right">
                                    <Clock v-if="application.showClock" />
                                </div>
                            </div>
                        </div>
                    </section>

                    <div
                        uno-layer-default="ml-64"
                        ss-xs="min-h-screen ml-0"
                    >
                        <div id="main-container">
                            <div
                                ref="fullscreenRef"
                                uno-layer-default="mt-12 mb-4 py-4 bg-gray-50 mx-4 px-4"
                                ss-xs="mt-6 mx-1 px-1"
                            >
                                <main>
                                    <MainPageProxy />
                                </main>
                            </div>
                            <div uno-layer-default="h-24"></div>
                        </div>
                    </div>

                    <section
                        id="nav-container"
                        uno-layer-default="
                            min-h-screen overflow-auto inner-scroll-shadow inner-scroll-shadow-nav
                            min-h-0 fixed left-0 top-0 w-64 h-screen border-r z-40
                            bg-gray-300 border-gray-400
                            dark:border-gray-900
                            space-y-8
                        "
                        ss-xs="
                            min-h-auto static left-auto top-auto w-auto h-auto border-none z-0
                        "
                    >
                        <header
                            uno-layer-default="pt-6 px-8"
                            ss-xs="pt-16"
                        >
                            <component
                                :href="application.baseUrl"
                                :is="application.baseUrl ? 'a' : 'div'"
                                :title="avon.appName"
                                uno-layer-default="flex flex-col items-center gap-2"
                            >
                                <span uno-layer-default="text-2xl font-semibold">
                                    {{ avon.appName }}
                                </span>
                                <img
                                    :alt="avon.appName"
                                    :src="avon.logoImageUrl"
                                    uno-layer-default="w-full max-h-12"
                                    v-if="avon.logoImageUrl"
                                >
                            </component>
                        </header>

                        <hr uno-layer-default="mx-14 border-t border-gray-400">

                        <nav id="main-menu" uno-layer-default="px-6">
                            <MainMenu
                                class="mt--2"
                                v-bind="application"
                            />
                        </nav>

                        <hr uno-layer-default="mx-14 border-t border-gray-400">

                        <footer id="footer" uno-layer-default="pb-8 px-8">
                            <div uno-layer-default="rounded shadow p-2 text-center text-xs bg-white text-gray-400">
                                {{ avon.footer }}
                            </div>
                            <div
                                uno-layer-default="hidden relative text-right"
                                ss-xs="block"
                            >
                                <Btn
                                    @click="scrollToTop"
                                    href="#app"
                                    class="
                                        text-xs inline-block mt-4 p-2 rounded-xl border transition
                                        text-gray-500 border-gray-500
                                        hover:text-blue-600 hover:border-blue-600
                                    "
                                >
                                    go to top
                                    <Icon
                                        class="w-3 inline-block"
                                        name="chevron-double-up"
                                    />
                                </Btn>
                            </div>
                        </footer>
                    </section>
                </div>
            `,
        };

        const AppContainer = {
            components: {
                App,
                ComponentLoader,
                TransitionWrapper,
            },
            props: {
                promises: Array,
            },
            setup(props) {
                const loaded = ref(false);
                Promise.all(props.promises).then(() => loaded.value = true);

                globalStore.reset();
                const unmounted = ref(true);
                const shouldFade = ref(false);

                return {
                    shouldFade,
                    loaded,
                    unmounted,

                    loader: computed(() => ({
                        screen: true,
                        resolved: () => {
                            if (new Date().getTime() - __avon._start > 5000) {
                                shouldFade.value = true;
                            }

                            return globalStore.reset;
                        },
                        resolver: async () => {
                            await props.promises[1];
                            return authenticator.check();
                        },
                        unmounted: unmounted.value,
                    })),
                };
            },
            template: `
                <ComponentLoader v-bind="loader" v-if="loaded">
                    <TransitionWrapper
                        :none="shouldFade"
                        name="fade"
                        enterActiveClass="transition-opacity"
                        leaveActiveClass="transition-opacity"
                        enterFromClass="opacity-0"
                        leaveToClass="opacity-0"
                    >
                        <App
                            @appMounted="unmounted = false"
                            v-show="!unmounted"
                        />
                    </TransitionWrapper>
                </ComponentLoader>
            `,
        };

        (() => {
            const namedComponent = [];
            (function vueNameComponents(components) {
                if (components) {
                    Object.entries(components).forEach(([name, component]) => {
                        if (!namedComponent.includes(component)) {
                            namedComponent.push(component);
                            if (!('name' in components)) {
                                component.name = name;
                            }
                            if ('components' in component) {
                               vueNameComponents(component.components);
                            }
                        }
                    });
                }
            })({ AppContainer });
        })();

        return {
            appComponent: AppContainer,
            bootstrap(app) {
                bootstrappers
                    .splice(0)
                    .forEach((b) => b(app, bootstrapOptions));
            },
            options: bootstrapOptions,
        };
    };

    const unocssConfig = () => {
        const typographyRules = () => {
            const typographyPrefix = 'prose';
            const typographyRuleMeta = { layer: '_typography2' };

            const round = (num) => num.toFixed(7).replace(/(\.[0-9]+?)0+$/, '$1').replace(/\.0$/, '');
            const rem = (px) => `${round(px / 16)}rem`;
            const em = (px, base) => `${round(px / base)}em`;
            const baseStyles = [
                {
                    '': {
                        'color': 'var(--un-prose-body)',
                        'max-width': '65ch',
                        'font-size': rem(16),
                        'line-height': round(28 / 16),
                    },
                    '> :first-child': {
                        'margin-top': '0',
                    },
                    '> :last-child': {
                        'margin-bottom': '0',
                    },
                },
                {
                    '[class~="lead"]': {
                        color: 'var(--un-prose-lead)',
                    },
                    'a': {
                        'color': 'var(--un-prose-links)',
                        'text-decoration': 'underline',
                        'font-weight': '500',
                    },
                    'strong': {
                        'color': 'var(--un-prose-bold)',
                        'font-weight': '600',
                    },
                    'ol': {
                        'list-style-type': 'decimal',
                    },
                    'ol[type="A"]': {
                        'list-style-type': 'upper-alpha',
                    },
                    'ol[type="a"]': {
                        'list-style-type': 'lower-alpha',
                    },
                    'ol[type="A" s]': {
                        'list-style-type': 'upper-alpha',
                    },
                    'ol[type="a" s]': {
                        'list-style-type': 'lower-alpha',
                    },
                    'ol[type="I"]': {
                        'list-style-type': 'upper-roman',
                    },
                    'ol[type="i"]': {
                        'list-style-type': 'lower-roman',
                    },
                    'ol[type="I" s]': {
                        'list-style-type': 'upper-roman',
                    },
                    'ol[type="i" s]': {
                        'list-style-type': 'lower-roman',
                    },
                    'ol[type="1"]': {
                        'list-style-type': 'decimal',
                    },
                    'ul': {
                        'list-style-type': 'disc',
                    },
                    'ol > li::marker': {
                        'font-weight': '400',
                        'color': 'var(--un-prose-counters)',
                    },
                    'ul > li::marker': {
                        color: 'var(--un-prose-bullets)',
                    },
                    'hr': {
                        'border-color': 'var(--un-prose-hr)',
                        'border-top-width': 1,
                    },
                    'blockquote': {
                        'font-weight': '500',
                        'font-style': 'italic',
                        'color': 'var(--un-prose-quotes)',
                        'border-left-width': '0.25rem',
                        'border-left-color': 'var(--un-prose-quote-borders)',
                        'quotes': '"\\201C""\\201D""\\2018""\\2019"',
                    },
                    'blockquote p:first-of-type::before': {
                        content: 'open-quote',
                    },
                    'blockquote p:last-of-type::after': {
                        content: 'close-quote',
                    },
                    'h1': {
                        'color': 'var(--un-prose-headings)',
                        'font-weight': '800',
                    },
                    'h1 strong': {
                        'font-weight': '900',
                    },
                    'h2': {
                        'color': 'var(--un-prose-headings)',
                        'font-weight': '700',
                    },
                    'h2 strong': {
                        'font-weight': '800',
                    },
                    'h3': {
                        'color': 'var(--un-prose-headings)',
                        'font-weight': '600',
                    },
                    'h3 strong': {
                        'font-weight': '700',
                    },
                    'h4': {
                        'color': 'var(--un-prose-headings)',
                        'font-weight': '600',
                    },
                    'h4 strong': {
                        'font-weight': '700',
                    },
                    'figcaption': {
                        color: 'var(--un-prose-captions)',
                    },
                    'code': {
                        'color': 'var(--un-prose-code)',
                        'font-weight': '600',
                    },
                    'code::before': {
                        content: '"`"',
                    },
                    'code::after': {
                        content: '"`"',
                    },
                    'a code': {
                        color: 'var(--un-prose-links)',
                    },
                    'pre': {
                        'color': 'var(--un-prose-pre-code)',
                        'background-color': 'var(--un-prose-pre-bg)',
                        'overflow-x': 'auto',
                        'font-weight': '400',
                    },
                    'pre code': {
                        'background-color': 'transparent',
                        'border-width': '0',
                        'border-radius': '0',
                        'padding': '0',
                        'font-weight': 'inherit',
                        'color': 'inherit',
                        'font-size': 'inherit',
                        'font-family': 'inherit',
                        'line-height': 'inherit',
                    },
                    'pre code::before': {
                        content: 'none',
                    },
                    'pre code::after': {
                        content: 'none',
                    },
                    'table': {
                        'width': '100%',
                        'table-layout': 'auto',
                        'text-align': 'left',
                        'margin-top': em(32, 16),
                        'margin-bottom': em(32, 16),
                    },
                    'thead': {
                        'border-bottom-width': '1px',
                        'border-bottom-color': 'var(--un-prose-th-borders)',
                    },
                    'thead th': {
                        'color': 'var(--un-prose-headings)',
                        'font-weight': '600',
                        'vertical-align': 'bottom',
                    },
                    'tbody tr': {
                        'border-bottom-width': '1px',
                        'border-bottom-color': 'var(--un-prose-td-borders)',
                    },
                    'tbody tr:last-child': {
                        'border-bottom-width': '0',
                    },
                    'tbody td': {
                        'vertical-align': 'baseline',
                    },
                    'tfoot': {
                        'border-top-width': '1px',
                        'border-top-color': 'var(--un-prose-th-borders)',
                    },
                    'tfoot td': {
                        'vertical-align': 'top',
                    },
                },
                {
                    'p': {
                        'margin-top': em(20, 16),
                        'margin-bottom': em(20, 16),
                    },
                    '[class~="lead"]': {
                        'font-size': em(20, 16),
                        'line-height': round(32 / 20),
                        'margin-top': em(24, 20),
                        'margin-bottom': em(24, 20),
                    },
                    'blockquote': {
                        'margin-top': em(32, 20),
                        'margin-bottom': em(32, 20),
                        'padding-left': em(20, 20),
                    },
                    'h1': {
                        'font-size': em(36, 16),
                        'margin-top': '0',
                        'margin-bottom': em(32, 36),
                        'line-height': round(40 / 36),
                    },
                    'h2': {
                        'font-size': em(24, 16),
                        'margin-top': em(48, 24),
                        'margin-bottom': em(24, 24),
                        'line-height': round(32 / 24),
                    },
                    'h3': {
                        'font-size': em(20, 16),
                        'margin-top': em(32, 20),
                        'margin-bottom': em(12, 20),
                        'line-height': round(32 / 20),
                    },
                    'h4': {
                        'margin-top': em(24, 16),
                        'margin-bottom': em(8, 16),
                        'line-height': round(24 / 16),
                    },
                    'img': {
                        'margin-top': em(32, 16),
                        'margin-bottom': em(32, 16),
                    },
                    'video': {
                        'margin-top': em(32, 16),
                        'margin-bottom': em(32, 16),
                    },
                    'figure': {
                        'margin-top': em(32, 16),
                        'margin-bottom': em(32, 16),
                    },
                    'figure > *': {
                        'margin-top': '0',
                        'margin-bottom': '0',
                    },
                    'figcaption': {
                        'font-size': em(14, 16),
                        'line-height': round(20 / 14),
                        'margin-top': em(12, 14),
                    },
                    'code': {
                        'font-size': em(14, 16),
                    },
                    'h2 code': {
                        'font-size': em(21, 24),
                    },
                    'h3 code': {
                        'font-size': em(18, 20),
                    },
                    'pre': {
                        'font-size': em(14, 16),
                        'line-height': round(24 / 14),
                        'margin-top': em(24, 14),
                        'margin-bottom': em(24, 14),
                        'border-radius': rem(6),
                        'padding-top': em(12, 14),
                        'padding-right': em(16, 14),
                        'padding-bottom': em(12, 14),
                        'padding-left': em(16, 14),
                    },
                    'ol': {
                        'margin-top': em(20, 16),
                        'margin-bottom': em(20, 16),
                        'padding-left': em(26, 16),
                    },
                    'ul': {
                        'margin-top': em(20, 16),
                        'margin-bottom': em(20, 16),
                        'padding-left': em(26, 16),
                    },
                    'li': {
                        'margin-top': em(8, 16),
                        'margin-bottom': em(8, 16),
                    },
                    'ol > li': {
                        'padding-left': em(6, 16),
                    },
                    'ul > li': {
                        'padding-left': em(6, 16),
                    },
                    '> ul > li p': {
                        'margin-top': em(12, 16),
                        'margin-bottom': em(12, 16),
                    },
                    '> ul > li > *:first-child': {
                        'margin-top': em(20, 16),
                    },
                    '> ul > li > *:last-child': {
                        'margin-bottom': em(20, 16),
                    },
                    '> ol > li > *:first-child': {
                        'margin-top': em(20, 16),
                    },
                    '> ol > li > *:last-child': {
                        'margin-bottom': em(20, 16),
                    },
                    'ul ul, ul ol, ol ul, ol ol': {
                        'margin-top': em(12, 16),
                        'margin-bottom': em(12, 16),
                    },
                    'hr': {
                        'margin-top': em(48, 16),
                        'margin-bottom': em(48, 16),
                    },
                    'hr + *': {
                        'margin-top': '0',
                    },
                    'h2 + *': {
                        'margin-top': '0',
                    },
                    'h3 + *': {
                        'margin-top': '0',
                    },
                    'h4 + *': {
                        'margin-top': '0',
                    },
                    'table': {
                        'font-size': em(14, 16),
                        'line-height': round(24 / 14),
                    },
                    'thead th': {
                        'padding-right': em(8, 14),
                        'padding-bottom': em(8, 14),
                        'padding-left': em(8, 14),
                    },
                    'thead th:first-child': {
                        'padding-left': '0',
                    },
                    'thead th:last-child': {
                        'padding-right': '0',
                    },
                    'tbody td, tfoot td': {
                        'padding-top': em(8, 14),
                        'padding-right': em(8, 14),
                        'padding-bottom': em(8, 14),
                        'padding-left': em(8, 14),
                    },
                    'tbody td:first-child, tfoot td:first-child': {
                        'padding-left': '0',
                    },
                    'tbody td:last-child, tfoot td:last-child': {
                        'padding-right': '0',
                    },
                },
            ];

            return [
                [new RegExp(`^(${typographyPrefix})(?:-(\\w+))?$`), ([, prefix, color = ''], { theme }) => {
                    const themeColors = theme?.colors || {};
                    const baseColor = themeColors[color || 'gray'];
                    if (baseColor == null)
                        return;

                    const colorObject = typeof baseColor === 'object' ? baseColor : {};
                    const css = ([selectors, definitions]) => {
                        const d = Object.entries(definitions).map(([k, v]) => `${k}:${v}`).join(';');
                        const s = selectors === '' ? '' : ' ';
                        const c = color === '' ? '' : `-${color}`;
                        return `.${prefix}${c}${s}${selectors}{${d}}`;
                    };

                    const colors = {
                        '--un-prose-body': colorObject[700] ?? baseColor,
                        '--un-prose-headings': colorObject[900] ?? baseColor,
                        '--un-prose-lead': colorObject[600] ?? baseColor,
                        '--un-prose-links': colorObject[900] ?? baseColor,
                        '--un-prose-bold': colorObject[900] ?? baseColor,
                        '--un-prose-counters': colorObject[500] ?? baseColor,
                        '--un-prose-bullets': colorObject[300] ?? baseColor,
                        '--un-prose-hr': colorObject[200] ?? baseColor,
                        '--un-prose-quotes': colorObject[900] ?? baseColor,
                        '--un-prose-quote-borders': colorObject[200] ?? baseColor,
                        '--un-prose-captions': colorObject[500] ?? baseColor,
                        '--un-prose-code': colorObject[900] ?? baseColor,
                        '--un-prose-pre-code': colorObject[200] ?? baseColor,
                        '--un-prose-pre-bg': colorObject[800] ?? themeColors.white ?? 'rgba(0,0,0,30%)',
                        '--un-prose-th-borders': colorObject[300] ?? baseColor,
                        '--un-prose-td-borders': colorObject[200] ?? baseColor,

                        '--un-prose-invert-body': colorObject[300] ?? baseColor,
                        '--un-prose-invert-headings': themeColors.white ?? baseColor,
                        '--un-prose-invert-lead': colorObject[400] ?? baseColor,
                        '--un-prose-invert-links': themeColors.white ?? baseColor,
                        '--un-prose-invert-bold': themeColors.white ?? baseColor,
                        '--un-prose-invert-counters': colorObject[400] ?? baseColor,
                        '--un-prose-invert-bullets': colorObject[600] ?? baseColor,
                        '--un-prose-invert-hr': colorObject[700] ?? baseColor,
                        '--un-prose-invert-quotes': colorObject[100] ?? baseColor,
                        '--un-prose-invert-quote-borders': colorObject[700] ?? baseColor,
                        '--un-prose-invert-captions': colorObject[400] ?? baseColor,
                        '--un-prose-invert-code': themeColors.white ?? baseColor,
                        '--un-prose-invert-pre-code': colorObject[300] ?? baseColor,
                        '--un-prose-invert-pre-bg': 'rgba(0,0,0,50%)',
                        '--un-prose-invert-th-borders': colorObject[600] ?? baseColor,
                        '--un-prose-invert-td-borders': colorObject[700] ?? baseColor,
                    };

                    return [{ '': colors }, ...baseStyles]
                        .map(o => Object.entries(o).map(css).join(`\n`))
                        .join(`\n`);
                }, typographyRuleMeta],
                [new RegExp(`^${typographyPrefix}-invert$`), () => ({
                    '--un-prose-body': 'var(--un-prose-invert-body)',
                    '--un-prose-headings': 'var(--un-prose-invert-headings)',
                    '--un-prose-lead': 'var(--un-prose-invert-lead)',
                    '--un-prose-links': 'var(--un-prose-invert-links)',
                    '--un-prose-bold': 'var(--un-prose-invert-bold)',
                    '--un-prose-counters': 'var(--un-prose-invert-counters)',
                    '--un-prose-bullets': 'var(--un-prose-invert-bullets)',
                    '--un-prose-hr': 'var(--un-prose-invert-hr)',
                    '--un-prose-quotes': 'var(--un-prose-invert-quotes)',
                    '--un-prose-quote-borders': 'var(--un-prose-invert-quote-borders)',
                    '--un-prose-captions': 'var(--un-prose-invert-captions)',
                    '--un-prose-code': 'var(--un-prose-invert-code)',
                    '--un-prose-pre-code': 'var(--un-prose-invert-pre-code)',
                    '--un-prose-pre-bg': 'var(--un-prose-invert-pre-bg)',
                    '--un-prose-th-borders': 'var(--un-prose-invert-th-borders)',
                    '--un-prose-td-borders': 'var(--un-prose-invert-td-borders)',
                }), typographyRuleMeta],
            ];
        };

        const presetBase = () => window.__unocss_runtime.presets;

        const prefixRe = /^__\w|^\[\w|^\w+(?:-|:|$)/;
        const iconCollectionRe = /.*\/([^/]+)\/.*/;
        const { baseUrl, assetId } = __avon;

        return {
            layers: {
                _reset: -101,
                _forms: -15,
                _typography2: -15,
                _preOverride: -5,
            },
            rules: typographyRules(),
            preprocess: ctx => ctx.match(prefixRe) ? ctx : "",
            shortcuts: [
                ['inert', 'cursor-default pointer-events-none select-none all:select-none'],
                [
                    /^ss-(xl)([:-].+)$/,
                    ([, s, rule]) => `scope-[.size\\_${s}]${rule}`,
                    { layer: 'size_xl' },
                ],
                [
                    /^ss-(md)([:-].+)$/,
                    ([, s, rule]) => `scope-[.size\\_${s}]${rule}`,
                    { layer: 'size_md' },
                ],
                [
                    /^ss-(xs)([:-].+)$/,
                    ([, s, rule]) => `scope-[.size\\_${s}]${rule}`,
                    { layer: 'size_xs' },
                ],
            ],
            presets: [
                () => presetBase().presetAttributify(),
                () => presetBase().presetUno(),
                () => presetBase().presetIcons({
                    customFetch: (url) => {
                        const collection = url.replace(iconCollectionRe, '$1');
                        const iconUrl = `${baseUrl}/application/asset/${assetId}/icons/${collection}.json`;
                        return fetch(iconUrl).then(data => data.json());
                    },
                    scale: 1,
                }),
            ],
        };
    };

    const unocssRuntimeConfig = () => {
        const cssPreflights = [{
            layer: '_reset',
            cssTemplate: `
                /*
                1. Prevent padding and border from affecting element width. (https://github.com/mozdevs/cssremedy/issues/4)
                2. Allow adding a border to an element by just adding a border-width. (https://github.com/tailwindcss/tailwindcss/pull/116)
                */

                *,
                ::before,
                ::after {
                  box-sizing: border-box; /* 1 */
                  border-width: 0; /* 2 */
                  border-style: solid; /* 2 */
                  border-color: #e5e7eb; /* 2 */
                }

                ::before,
                ::after {
                  --tw-content: '';
                }

                /*
                1. Use a consistent sensible line-height in all browsers.
                2. Prevent adjustments of font size after orientation changes in iOS.
                3. Use a more readable tab size.
                4. Use the user's configured 'sans' font-family by default.
                */

                html {
                  line-height: 1.5; /* 1 */
                  -webkit-text-size-adjust: 100%; /* 2 */
                  -moz-tab-size: 4; /* 3 */
                  tab-size: 4; /* 3 */
                  font-family: ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; /* 4 */
                }

                /*
                1. Remove the margin in all browsers.
                2. Inherit line-height from 'html' so users can set them as a class directly on the 'html' element.
                */

                body {
                  margin: 0; /* 1 */
                  line-height: inherit; /* 2 */
                }

                /*
                1. Add the correct height in Firefox.
                2. Correct the inheritance of border color in Firefox. (https://bugzilla.mozilla.org/show_bug.cgi?id=190655)
                3. Ensure horizontal rules are visible by default.
                */

                hr {
                  height: 0; /* 1 */
                  color: inherit; /* 2 */
                  border-top-width: 1px; /* 3 */
                }

                /*
                Add the correct text decoration in Chrome, Edge, and Safari.
                */

                abbr:where([title]) {
                  text-decoration: underline dotted;
                }

                /*
                Remove the default font size and weight for headings.
                */

                h1,
                h2,
                h3,
                h4,
                h5,
                h6 {
                  font-size: inherit;
                  font-weight: inherit;
                }

                /*
                Reset links to optimize for opt-in styling instead of opt-out.
                */

                a {
                  color: inherit;
                  text-decoration: inherit;
                }

                /*
                Add the correct font weight in Edge and Safari.
                */

                b,
                strong {
                  font-weight: bolder;
                }

                /*
                1. Use the user's configured 'mono' font family by default.
                2. Correct the odd 'em' font sizing in all browsers.
                */

                code,
                kbd,
                samp,
                pre {
                  font-family: ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace; /* 1 */
                  font-size: 1em; /* 2 */
                }

                /*
                Add the correct font size in all browsers.
                */

                small {
                  font-size: 80%;
                }

                /*
                Prevent 'sub' and 'sup' elements from affecting the line height in all browsers.
                */

                sub,
                sup {
                  font-size: 75%;
                  line-height: 0;
                  position: relative;
                  vertical-align: baseline;
                }

                sub {
                  bottom: -0.25em;
                }

                sup {
                  top: -0.5em;
                }

                /*
                1. Remove text indentation from table contents in Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=999088, https://bugs.webkit.org/show_bug.cgi?id=201297)
                2. Correct table border color inheritance in all Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=935729, https://bugs.webkit.org/show_bug.cgi?id=195016)
                3. Remove gaps between table borders by default.
                */

                table {
                  text-indent: 0; /* 1 */
                  border-color: inherit; /* 2 */
                  border-collapse: collapse; /* 3 */
                }

                /*
                1. Change the font styles in all browsers.
                2. Remove the margin in Firefox and Safari.
                3. Remove default padding in all browsers.
                */

                button,
                input,
                optgroup,
                select,
                textarea {
                  font-family: inherit; /* 1 */
                  font-size: 100%; /* 1 */
                  font-weight: inherit; /* 1 */
                  line-height: inherit; /* 1 */
                  color: inherit; /* 1 */
                  margin: 0; /* 2 */
                  padding: 0; /* 3 */
                }

                /*
                Remove the inheritance of text transform in Edge and Firefox.
                */

                button,
                select {
                  text-transform: none;
                }

                /*
                1. Correct the inability to style clickable types in iOS and Safari.
                2. Remove default button styles.
                */

                button,
                [type='button'],
                [type='reset'],
                [type='submit'] {
                  -webkit-appearance: button; /* 1 */
                  background-color: transparent; /* 2 */
                  background-image: none; /* 2 */
                }

                /*
                Use the modern Firefox focus style for all focusable elements.
                */

                :-moz-focusring {
                  outline: auto;
                }

                /*
                Remove the additional ':invalid' styles in Firefox. (https://github.com/mozilla/gecko-dev/blob/2f9eacd9d3d995c937b4251a5557d95d494c9be1/layout/style/res/forms.css#L728-L737)
                */

                :-moz-ui-invalid {
                  box-shadow: none;
                }

                /*
                Add the correct vertical alignment in Chrome and Firefox.
                */

                progress {
                  vertical-align: baseline;
                }

                /*
                Correct the cursor style of increment and decrement buttons in Safari.
                */

                ::-webkit-inner-spin-button,
                ::-webkit-outer-spin-button {
                  height: auto;
                }

                /*
                1. Correct the odd appearance in Chrome and Safari.
                2. Correct the outline style in Safari.
                */

                [type='search'] {
                  -webkit-appearance: textfield; /* 1 */
                  outline-offset: -2px; /* 2 */
                }

                /*
                Remove the inner padding in Chrome and Safari on macOS.
                */

                ::-webkit-search-decoration {
                  -webkit-appearance: none;
                }

                /*
                1. Correct the inability to style clickable types in iOS and Safari.
                2. Change font properties to 'inherit' in Safari.
                */

                ::-webkit-file-upload-button {
                  -webkit-appearance: button; /* 1 */
                  font: inherit; /* 2 */
                }

                /*
                Add the correct display in Chrome and Safari.
                */

                summary {
                  display: list-item;
                }

                /*
                Removes the default spacing and border for appropriate elements.
                */

                blockquote,
                dl,
                dd,
                h1,
                h2,
                h3,
                h4,
                h5,
                h6,
                hr,
                figure,
                p,
                pre {
                  margin: 0;
                }

                fieldset {
                  margin: 0;
                  padding: 0;
                }

                legend {
                  padding: 0;
                }

                ol,
                ul,
                menu {
                  list-style: none;
                  margin: 0;
                  padding: 0;
                }

                /*
                Prevent resizing textareas horizontally by default.
                */

                textarea {
                  resize: vertical;
                }

                /*
                1. Reset the default placeholder opacity in Firefox. (https://github.com/tailwindlabs/tailwindcss/issues/3300)
                2. Set the default placeholder color to the user's configured gray 400 color.
                */

                input::placeholder,
                textarea::placeholder {
                  opacity: 1; /* 1 */
                  color: #9ca3af; /* 2 */
                }

                /*
                1. Make replaced elements 'display: block' by default. (https://github.com/mozdevs/cssremedy/issues/14)
                2. Add 'vertical-align: middle' to align replaced elements more sensibly by default. (https://github.com/jensimmons/cssremedy/issues/14#issuecomment-634934210)
                   This can trigger a poorly considered lint error in some tools but is included by design.
                */

                img,
                svg,
                video,
                canvas,
                audio,
                iframe,
                embed,
                object {
                  display: block; /* 1 */
                  vertical-align: middle; /* 2 */
                }

                /*
                Constrain images and videos to the parent width and preserve their intrinsic aspect ratio. (https://github.com/mozdevs/cssremedy/issues/14)
                */

                img,
                video {
                  max-width: 100%;
                  height: auto;
                }
            `,
        }, {
            layer: '_preOverride',
            cssTemplate: `
                html {
                  -ms-text-size-adjust: 100%;
                  -webkit-text-size-adjust: 100%;
                  -webkit-tap-highlight-color: rgba(0,0,0,0);
                }

                [inert] {
                  cursor: default;
                  pointer-events: none;
                }

                [inert],
                [inert] * {
                  user-select: none;
                  -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none;
                }

                [hidden] {
                  display: none;
                }

                button,
                [role="button"] {
                  cursor: pointer;
                }

                :disabled {
                  cursor: default;
                }

                [type='text']:where(.form-input):focus,
                [type='email']:where(.form-input):focus,
                [type='url']:where(.form-input):focus,
                [type='password']:where(.form-input):focus,
                [type='number']:where(.form-input):focus,
                [type='date']:where(.form-input):focus,
                [type='datetime-local']:where(.form-input):focus,
                [type='month']:where(.form-input):focus,
                [type='search']:where(.form-input):focus,
                [type='tel']:where(.form-input):focus,
                [type='time']:where(.form-input):focus,
                [type='week']:where(.form-input):focus,
                textarea:where(.form-input):focus,
                [multiple]:where(.form-select):focus,
                select:where(.form-select):focus {
                  --un-ring-width: 0.25rem;
                  --un-ring-color: rgba(147,197,253);
                  --un-ring-shadow: var(--un-ring-inset) 0 0 0 calc(var(--un-ring-width) + var(--un-ring-offset-width)) var(--un-ring-color);
                }

                [type='checkbox']:where(.form-control):focus,
                [type='radio']:where(.form-control):focus {
                  --un-ring-color: rgba(147,197,253);
                }

                select:where(.form-select) {
                  background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 24 24'%3e%3cpath stroke='%236b7280' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M8 9l4-4 4 4m0 6l-4 4-4-4'/%3e%3c/svg%3e");
                  background-size: 1rem 1rem;
                }
            `,
        }];

        const formPreflight = {
            layer: '_forms',
            getCSS: ({ theme }) => {
                const encodeSvg = (svg) => 'data:image/svg+xml,' + svg
                    .replace('<svg', (~svg.indexOf('xmlns') ? '<svg' : '<svg xmlns="http://www.w3.org/2000/svg"'))
                    .replace(/"/g, '\'')
                    .replace(/%/g, '%25')
                    .replace(/#/g, '%23')
                    .replace(/{/g, '%7B')
                    .replace(/}/g, '%7D')
                    .replace(/</g, '%3C')
                    .replace(/>/g, '%3E');

                const gray = theme?.colors?.gray?.[500] ?? '#6b7280';
                const blue = theme?.colors?.blue?.[600] ?? '#2563eb';
                const borderRadius = { none: 'none' };
                const borderWidth = { DEFAULT: '1px' };
                const baseFontSize = '1rem';
                const baseLineHeight = '1rem';

                const spacing = {
                    [2]: '0.5rem',
                    [3]: '0.75rem',
                    [4]: '1rem',
                    [10]: '2.5rem',
                };

                const rules = [
                    {
                        base: [
                            '.form-plain',
                            "[type='text']:where(.form-input)",
                            "[type='email']:where(.form-input)",
                            "[type='url']:where(.form-input)",
                            "[type='password']:where(.form-input)",
                            "[type='number']:where(.form-input)",
                            "[type='date']:where(.form-input)",
                            "[type='datetime-local']:where(.form-input)",
                            "[type='month']:where(.form-input)",
                            "[type='search']:where(.form-input)",
                            "[type='tel']:where(.form-input)",
                            "[type='time']:where(.form-input)",
                            "[type='week']:where(.form-input)",
                            'textarea:where(.form-input)',
                            '[multiple]:where(.form-select)',
                            'select:where(.form-select)',
                        ],
                        styles: {
                            'appearance': 'none',
                            'background-color': '#fff',
                            'border-color': gray,
                            'border-width': borderWidth['DEFAULT'],
                            'border-radius': borderRadius['none'],
                            'padding-top': spacing[2],
                            'padding-right': spacing[3],
                            'padding-bottom': spacing[2],
                            'padding-left': spacing[3],
                            'font-size': baseFontSize,
                            'line-height': baseLineHeight,
                            '--un-shadow': '0 0 #0000',
                            // '&:focus': {
                            //     'outline': '2px solid transparent',
                            //     'outline-offset': '2px',
                            //     '--un-ring-inset': 'var(--un-empty,/*!*/ /*!*/)',
                            //     '--un-ring-offset-width': '0px',
                            //     '--un-ring-offset-color': '#fff',
                            //     '--un-ring-color': blue,
                            //     '--un-ring-offset-shadow': `var(--un-ring-inset) 0 0 0 var(--un-ring-offset-width) var(--un-ring-offset-color)`,
                            //     '--un-ring-shadow': `var(--un-ring-inset) 0 0 0 calc(1px + var(--un-ring-offset-width)) var(--un-ring-color)`,
                            //     'box-shadow': `var(--un-ring-offset-shadow), var(--un-ring-shadow), var(--un-shadow)`,
                            //     'border-color': blue,
                            // },
                        },
                    },
                    {
                        base: [
                            "[type='text']:where(.form-input):focus",
                            "[type='email']:where(.form-input):focus",
                            "[type='url']:where(.form-input):focus",
                            "[type='password']:where(.form-input):focus",
                            "[type='number']:where(.form-input):focus",
                            "[type='date']:where(.form-input):focus",
                            "[type='datetime-local']:where(.form-input):focus",
                            "[type='month']:where(.form-input):focus",
                            "[type='search']:where(.form-input):focus",
                            "[type='tel']:where(.form-input):focus",
                            "[type='time']:where(.form-input):focus",
                            "[type='week']:where(.form-input):focus",
                            'textarea:where(.form-input):focus',
                            '[multiple]:where(.form-select):focus',
                            'select:where(.form-select):focus',
                        ],
                        styles: {
                            'outline': '2px solid transparent',
                            'outline-offset': '2px',
                            '--un-ring-inset': 'var(--un-empty,/*!*/ /*!*/)',
                            '--un-ring-offset-width': '0px',
                            '--un-ring-offset-color': '#fff',
                            '--un-ring-color': blue,
                            '--un-ring-offset-shadow': `var(--un-ring-inset) 0 0 0 var(--un-ring-offset-width) var(--un-ring-offset-color)`,
                            '--un-ring-shadow': `var(--un-ring-inset) 0 0 0 calc(1px + var(--un-ring-offset-width)) var(--un-ring-color)`,
                            'box-shadow': `var(--un-ring-offset-shadow), var(--un-ring-shadow), var(--un-shadow)`,
                            'border-color': blue,
                        },
                    },
                    {
                        base: ['input:where(.form-input)::placeholder', 'textarea:where(.form-input)::placeholder'],
                        styles: {
                            color: gray,
                            opacity: '1',
                        },
                    },
                    {
                        base: [':where(.form-input)::-webkit-datetime-edit-fields-wrapper'],
                        styles: {
                            padding: '0',
                        },
                    },
                    {
                        // Unfortunate hack until https://bugs.webkit.org/show_bug.cgi?id=198959 is fixed.
                        // This sucks because users can't change line-height with a utility on date inputs now.
                        // Reference: https://github.com/twbs/bootstrap/pull/31993
                        base: [':where(.form-input)::-webkit-date-and-time-value'],
                        styles: {
                            'min-height': '1.5em',
                        },
                    },
                    {
                        // In Safari on macOS date time inputs are 4px taller than normal inputs
                        // This is because there is extra padding on the datetime-edit and datetime-edit-{part}-field pseudo elements
                        // See https://github.com/tailwindlabs/tailwindcss-forms/issues/95
                        base: [
                            ':where(.form-input)::-webkit-datetime-edit',
                            ':where(.form-input)::-webkit-datetime-edit-year-field',
                            ':where(.form-input)::-webkit-datetime-edit-month-field',
                            ':where(.form-input)::-webkit-datetime-edit-day-field',
                            ':where(.form-input)::-webkit-datetime-edit-hour-field',
                            ':where(.form-input)::-webkit-datetime-edit-minute-field',
                            ':where(.form-input)::-webkit-datetime-edit-second-field',
                            ':where(.form-input)::-webkit-datetime-edit-millisecond-field',
                            ':where(.form-input)::-webkit-datetime-edit-meridiem-field',
                        ],
                        styles: {
                            'padding-top': '0',
                            'padding-bottom': '0',
                        },
                    },
                    {
                        base: ['select:where(.form-select)'],
                        styles: {
                            'background-image': `url("${encodeSvg(
                                `<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20"><path stroke="${
                                    gray
                                }" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M6 8l4 4 4-4"/></svg>`
                            )}")`,
                            'background-position': `right ${spacing[2]} center`,
                            'background-repeat': `no-repeat`,
                            'background-size': `1.5em 1.5em`,
                            'padding-right': spacing[10],
                            'print-color-adjust': `exact`,
                        },
                    },
                    {
                        base: ['[multiple]:where(.form-select)'],
                        styles: {
                            'background-image': 'initial',
                            'background-position': 'initial',
                            'background-repeat': 'unset',
                            'background-size': 'initial',
                            'padding-right': spacing[3],
                            'print-color-adjust': 'unset',
                        },
                    },
                    {
                        base: [`[type='checkbox']:where(.form-control)`, `[type='radio']:where(.form-control)`],
                        styles: {
                            'appearance': 'none',
                            'padding': '0',
                            'print-color-adjust': 'exact',
                            'display': 'inline-block',
                            'vertical-align': 'middle',
                            'background-origin': 'border-box',
                            'user-select': 'none',
                            'flex-shrink': '0',
                            'height': spacing[4],
                            'width': spacing[4],
                            'color': blue,
                            'background-color': '#fff',
                            'border-color': gray,
                            'border-width': borderWidth['DEFAULT'],
                            '--un-shadow': '0 0 #0000',
                        },
                    },
                    {
                        base: [`[type='checkbox']:where(.form-control)`],
                        styles: {
                            'border-radius': borderRadius['none'],
                        },
                    },
                    {
                        base: [`[type='radio']:where(.form-control)`],
                        styles: {
                            'border-radius': '100%',
                        },
                    },
                    {
                        base: [`[type='checkbox']:where(.form-control):focus`, `[type='radio']:where(.form-control):focus`],
                        styles: {
                            'outline': '2px solid transparent',
                            'outline-offset': '2px',
                            '--un-ring-inset': 'var(--un-empty,/*!*/ /*!*/)',
                            '--un-ring-offset-width': '2px',
                            '--un-ring-offset-color': '#fff',
                            '--un-ring-color': blue,
                            '--un-ring-offset-shadow': `var(--un-ring-inset) 0 0 0 var(--un-ring-offset-width) var(--un-ring-offset-color)`,
                            '--un-ring-shadow': `var(--un-ring-inset) 0 0 0 calc(2px + var(--un-ring-offset-width)) var(--un-ring-color)`,
                            'box-shadow': `var(--un-ring-offset-shadow), var(--un-ring-shadow), var(--un-shadow)`,
                        },
                    },
                    {
                        base: [`[type='checkbox']:where(.form-control):checked`, `[type='radio']:where(.form-control):checked`],
                        styles: {
                            'border-color': `transparent`,
                            'background-color': `currentColor`,
                            'background-size': `100% 100%`,
                            'background-position': `center`,
                            'background-repeat': `no-repeat`,
                        },
                    },
                    {
                        base: [`[type='checkbox']:where(.form-control):checked`],
                        styles: {
                            'background-image': `url("${encodeSvg(
                                `<svg viewBox="0 0 16 16" fill="white" xmlns="http://www.w3.org/2000/svg"><path d="M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z"/></svg>`
                            )}")`,
                        },
                    },
                    {
                        base: [`[type='radio']:where(.form-control):checked`],
                        styles: {
                            'background-image': `url("${encodeSvg(
                                `<svg viewBox="0 0 16 16" fill="white" xmlns="http://www.w3.org/2000/svg"><circle cx="8" cy="8" r="3"/></svg>`
                            )}")`,
                        },
                    },
                    {
                        base: [
                            `[type='checkbox']:where(.form-control):checked:hover`,
                            `[type='checkbox']:where(.form-control):checked:focus`,
                            `[type='radio']:where(.form-control):checked:hover`,
                            `[type='radio']:where(.form-control):checked:focus`,
                        ],
                        styles: {
                            'border-color': 'transparent',
                            'background-color': 'currentColor',
                        },
                    },
                    {
                        base: [`[type='checkbox']:where(.form-control):indeterminate`],
                        styles: {
                            'background-image': `url("${encodeSvg(
                                `<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 16"><path stroke="white" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 8h8"/></svg>`
                            )}")`,
                            'border-color': `transparent`,
                            'background-color': `currentColor`,
                            'background-size': `100% 100%`,
                            'background-position': `center`,
                            'background-repeat': `no-repeat`,
                        },
                    },
                    {
                        base: [`[type='checkbox']:where(.form-control):indeterminate:hover`, `[type='checkbox']:where(.form-control):indeterminate:focus`],
                        styles: {
                            'border-color': 'transparent',
                            'background-color': 'currentColor',
                        },
                    },
                    {
                        base: [`[type='file']:where(.form-file)`],
                        styles: {
                            'background': 'unset',
                            'border-color': 'inherit',
                            'border-width': '0',
                            'border-radius': '0',
                            'padding': '0',
                            'font-size': 'unset',
                            'line-height': 'inherit',
                        },
                    },
                    {
                        base: [`[type='file']:where(.form-file):focus`],
                        styles: {
                            outline: [
                                `1px solid ButtonText`,
                                `1px auto -webkit-focus-ring-color`,
                            ],
                        },
                    },
                ];

                return rules.map(({ base, styles }) => {
                    const s = base.join(',');
                    const d = Object.entries(styles).map(
                        ([k, values]) => (isArray(values) ? values : [values]).map((v) => `${k}:${v}`)
                    ).flat(1).join(';');
                    return `${s}{${d}}`;
                }).join(`\n`);
            },
        };

        const basePreflights = cssPreflights.map(({ layer, cssTemplate }) => ({
            layer,
            getCSS: () => cssTemplate.replace(/\B--tw-\b/g, '--un-').replace(/}/g, `}\n`),
        }));

        return {
            autoPrefix: true,
            configResolved(_, config) {
                config.preflights = [
                    ...basePreflights,
                    formPreflight,
                ];
            },
        };
    };

    const publicKeyBaseResolver = () => {
        let fetches = [
            fetch(`${__avon.baseUrl}/api/application/public-key/key`)
                .then(x => x.json())
                .then(x => x.key),

            fetch(`${__avon.baseUrl}/api/application/public-key/signature`)
                .then(x => x.json())
                .then(x => x.signature),
        ];

        return () => new Promise(async (resolve) => {
            let [publicKey, signature] = await Promise.all(fetches);

            if (! acrypt().verifyString(signature, publicKey, publicKey)) {
                __WARN('Cannot verify key');
            }

            fetches = null;
            signature = null;

            resolve(publicKey);
        });
    };

    const publicKeyResolver = publicKeyBaseResolver();

    return {
        app: null, // placeholder for app
        configured: {}, // placeholder for options

        register,
        login,
        application,

        // scrypt,
        // acrypt,
        resolvePublicKey() {
            if (! __avon.publicKey) {
                __avon.publicKey = publicKeyResolver();
            }

            return __avon.publicKey;
        },

        unocssConfig,
        unocssRuntimeConfig,
    };
});
