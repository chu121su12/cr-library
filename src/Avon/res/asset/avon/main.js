(function(__avon, callback) {
    callback(
        __avon.appId, // || navigator.userAgent
        __avon.baseUrl,
        __avon.useServiceWorker,
        window,
        new Date().getHours()
    );
})(__avon, function(
    appId,
    baseUrl,
    useServiceWorker,
    window,
    hour
) {
    var addEventListener = window.addEventListener;
    var console = window.console;
    var document = window.document;
    var navigator = window.navigator;
    var html = document.documentElement;
    var jsonstringify = JSON.stringify;
    var once = { once: true };
    var toString = Object.prototype.toString;

    function esSupport() {
        try {
            return (new Function('(a=0,...b)=>``')) && ['Map', 'Promise', 'Reflect', 'Set', 'Symbol']
                .reduce(function(r, x) {
                    return r && (x in window);
                }, true);
        } catch (e) {}
    }

    function sessionStore(key) {
        return window.sessionStorage.getItem(appId + '-global-' + key)
    }

    function matchScheme(scheme) {
        return window.matchMedia('(prefers-color-scheme: ' + scheme + ')').matches;
    }

    function errorEntry(object) {
        if (object) {
            return {
                c: object.columnNumber,
                f: object.fileName,
                l: object.lineNumber,
                m: object.message,
                s: object.stack
            };
        }
    }

    function dump(data) {
        var date = new Date;
        var args = [];
        for (var i = 0, l = data.length; i < l; ++i) {
            var object = data[i];
            var safeObject = object || {};
            args.push({
                v: object,
                t: typeof object,
                s: toString.call(object),
                d: safeObject.timeStamp,
                e: errorEntry(safeObject.error),
                r: errorEntry(safeObject.reason),
                t: safeObject.type
            });
        }
        try {
            return jsonstringify({
                d: args,
                s: date,
                t: new Date
            });
        } catch (e) {}
    }

    html.style.fontSize = sessionStore('htmlFontSize') || null;

    html.className = [
        html.className.replace(/\bno-js\b/g, '') + ' js',
        'ontouchstart' in document.createElement('div') || navigator.msMaxTouchPoints ? '' : 'no-touch',
        matchScheme('light') ? 'prefers-light' : (matchScheme('dark') ? 'prefers-dark' : ''),
        // sessionStore('prefersColorSchemeDark') ? 'dark' : '',
        sessionStore('prefersColorSchemeDark') ? 'dark-poor' : '',
        'size_' + (sessionStore('screenSize') || 'md'),
        hour <= 5 || hour >= 19 ? 'dark-hour' : '',
    ].join(' ');

    var esCheckTimes = 5;
    var esCheck = setInterval(function() {
        if (esSupport()) {
            esCheckTimes = 1;
            html.classList.remove('no-es');
        }
        else {
            html.className = html.className + ' no-es';
        }

        if (--esCheckTimes <= 1) {
            clearInterval(esCheck);
        }
    }, 3000);

    if ('serviceWorker' in navigator) {
        var serviceWorker = navigator.serviceWorker;
        if (useServiceWorker) {
            addEventListener('load', function() {
                serviceWorker.register(baseUrl + '/sw.js');
            }, once);
        }
        else {
            var clients = (window.clients && window.clients.matchAll()) || [];
            serviceWorker.getRegistrations().then(function(registrations) {
                registrations.forEach(function(registration) {
                    registration.unregister().then(function() {
                        clients.forEach(function(client) {
                            var url = client.url;
                            if (url && 'navigate' in client) {
                                client.navigate(url);
                            }
                        });
                    });
                });
            });
        }
    }

    (function(handler) {
        ['error', 'unhandledrejection'].forEach(function(eventName) {
            addEventListener(eventName, handler);
            addEventListener('unload', function() {
                window.removeEventListener(eventName, handler);
            }, once);
        });
    })(window.__avon.__error_report_callback = function() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', baseUrl + '/application/error', true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(dump(arguments));
    });

    if (console) {
        var log = console.log;
        setTimeout(function() {
            log('%c%s', 'color: red; background: yellow; font-size: 27px;', 'WARNING!');
            log('%c%s', 'font-size: 17px;', [
                'Using this console may allow attackers to impersonate you and steal your information.',
                'Do not enter or paste code that you do not understand.',
            ].join('\n'));
        });
    }
});
