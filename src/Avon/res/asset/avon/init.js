(() => {
    // dayjs
    ['customParseFormat', 'duration', 'relativeTime', 'timezone', 'utc']
        .forEach(plugin => dayjs.extend(window['dayjs_plugin_' + plugin]));

    Object.entries({
        M: 'satu bulan',
        d: 'satu hari',
        h: 'satu jam',
        m: 'satu menit',
        y: 'satu tahun',
    })
        .forEach(([k, v]) => dayjs_locale_id.relativeTime[k] = v);

    // vue virtualscroller
    window.vue3ObserveVisibility2_esm_js = Vue3ObserveVisibility2;

    // big.js
    Big._N = (stringNumber, decimalPoints, roundingMode) => {
        const s = Big.strict;

        try {
            Big.strict = true;

            const n = Big(stringNumber);

            const r = Big(stringNumber).round(decimalPoints == null ? 20 : decimalPoints, roundingMode == null ? Big.roundHalfUp : roundingMode);

            if (!n.eq(r)) {
                throw new Error('Invalid rounding');
            }

            return n;
        }
        catch (e) {
            throw e;
        }
        finally {
            Big.strict = s;
        }
    };
})();
