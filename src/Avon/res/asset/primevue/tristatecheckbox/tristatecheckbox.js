this.primevue = this.primevue || {};
this.primevue.tristatecheckbox = (function (CheckIcon, TimesIcon, BaseComponent, TriStateCheckboxStyle, vue) {
    'use strict';

    function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

    var CheckIcon__default = /*#__PURE__*/_interopDefaultLegacy(CheckIcon);
    var TimesIcon__default = /*#__PURE__*/_interopDefaultLegacy(TimesIcon);
    var BaseComponent__default = /*#__PURE__*/_interopDefaultLegacy(BaseComponent);
    var TriStateCheckboxStyle__default = /*#__PURE__*/_interopDefaultLegacy(TriStateCheckboxStyle);

    var script$1 = {
      name: 'BaseTriStateCheckbox',
      "extends": BaseComponent__default["default"],
      props: {
        modelValue: null,
        inputId: {
          type: String,
          "default": null
        },
        inputProps: {
          type: null,
          "default": null
        },
        disabled: {
          type: Boolean,
          "default": false
        },
        tabindex: {
          type: Number,
          "default": 0
        },
        ariaLabelledby: {
          type: String,
          "default": null
        },
        ariaLabel: {
          type: String,
          "default": null
        }
      },
      style: TriStateCheckboxStyle__default["default"],
      provide: function provide() {
        return {
          $parentInstance: this
        };
      }
    };

    var script = {
      name: 'TriStateCheckbox',
      "extends": script$1,
      emits: ['click', 'update:modelValue', 'change', 'keydown', 'focus', 'blur'],
      data: function data() {
        return {
          focused: false
        };
      },
      methods: {
        getPTOptions: function getPTOptions(key) {
          return this.ptm(key, {
            context: {
              active: this.modelValue !== null,
              focused: this.focused,
              disabled: this.disabled
            }
          });
        },
        updateModel: function updateModel() {
          if (!this.disabled) {
            var newValue;
            switch (this.modelValue) {
              case true:
                newValue = false;
                break;
              case false:
                newValue = null;
                break;
              default:
                newValue = true;
                break;
            }
            this.$emit('update:modelValue', newValue);
          }
        },
        onClick: function onClick(event) {
          this.updateModel();
          this.$emit('click', event);
          this.$emit('change', event);
          this.$refs.input.focus();
        },
        onKeyDown: function onKeyDown(event) {
          if (event.code === 'Enter' || event.code === 'NumpadEnter') {
            this.updateModel();
            this.$emit('keydown', event);
            event.preventDefault();
          }
        },
        onFocus: function onFocus(event) {
          this.focused = true;
          this.$emit('focus', event);
        },
        onBlur: function onBlur(event) {
          this.focused = false;
          this.$emit('blur', event);
        }
      },
      computed: {
        ariaValueLabel: function ariaValueLabel() {
          return this.modelValue ? this.$primevue.config.locale.aria.trueLabel : this.modelValue === false ? this.$primevue.config.locale.aria.falseLabel : this.$primevue.config.locale.aria.nullLabel;
        }
      },
      components: {
        CheckIcon: CheckIcon__default["default"],
        TimesIcon: TimesIcon__default["default"]
      }
    };

    function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
    function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
    function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
    function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
    function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : String(i); }
    function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
    var _hoisted_1 = ["id", "checked", "tabindex", "disabled", "aria-labelledby", "aria-label"];
    var _hoisted_2 = ["data-p-highlight", "data-p-disabled", "data-p-focused"];
    function render(_ctx, _cache, $props, $setup, $data, $options) {
      return vue.openBlock(), vue.createElementBlock("div", vue.mergeProps({
        "class": _ctx.cx('root'),
        onClick: _cache[3] || (_cache[3] = function ($event) {
          return $options.onClick($event);
        })
      }, _ctx.ptm('root'), {
        "data-pc-name": "tristatecheckbox"
      }), [vue.createElementVNode("div", vue.mergeProps({
        "class": "p-hidden-accessible"
      }, _ctx.ptm('hiddenInputWrapper'), {
        "data-p-hidden-accessible": true
      }), [vue.createElementVNode("input", vue.mergeProps({
        ref: "input",
        id: _ctx.inputId,
        type: "checkbox",
        checked: _ctx.modelValue === true,
        tabindex: _ctx.tabindex,
        disabled: _ctx.disabled,
        "aria-labelledby": _ctx.ariaLabelledby,
        "aria-label": _ctx.ariaLabel,
        onKeydown: _cache[0] || (_cache[0] = function ($event) {
          return $options.onKeyDown($event);
        }),
        onFocus: _cache[1] || (_cache[1] = function ($event) {
          return $options.onFocus($event);
        }),
        onBlur: _cache[2] || (_cache[2] = function ($event) {
          return $options.onBlur($event);
        })
      }, _objectSpread(_objectSpread({}, _ctx.inputProps), _ctx.ptm('hiddenInput'))), null, 16, _hoisted_1)], 16), vue.createElementVNode("span", vue.mergeProps({
        role: "status",
        "class": "p-hidden-accessible",
        "aria-live": "polite"
      }, _ctx.ptm('hiddenValueLabel'), {
        "data-p-hidden-accessible": true
      }), vue.toDisplayString($options.ariaValueLabel), 17), vue.createElementVNode("div", vue.mergeProps({
        ref: "box",
        "class": _ctx.cx('checkbox')
      }, $options.getPTOptions('checkbox'), {
        "data-p-highlight": _ctx.modelValue != null,
        "data-p-disabled": _ctx.disabled,
        "data-p-focused": $data.focused
      }), [_ctx.modelValue === true ? vue.renderSlot(_ctx.$slots, "checkicon", {
        key: 0,
        "class": vue.normalizeClass(_ctx.cx('checkIcon'))
      }, function () {
        return [(vue.openBlock(), vue.createBlock(vue.resolveDynamicComponent('CheckIcon'), vue.mergeProps({
          "class": _ctx.cx('checkIcon')
        }, _ctx.ptm('checkIcon')), null, 16, ["class"]))];
      }) : _ctx.modelValue === false ? vue.renderSlot(_ctx.$slots, "uncheckicon", {
        key: 1,
        "class": vue.normalizeClass(_ctx.cx('uncheckIcon'))
      }, function () {
        return [(vue.openBlock(), vue.createBlock(vue.resolveDynamicComponent('TimesIcon'), vue.mergeProps({
          "class": _ctx.cx('uncheckIcon')
        }, _ctx.ptm('uncheckIcon')), null, 16, ["class"]))];
      }) : vue.renderSlot(_ctx.$slots, "nullableicon", {
        key: 2,
        "class": vue.normalizeClass(_ctx.cx('nullableIcon'))
      }, function () {
        return [vue.createElementVNode("span", vue.mergeProps({
          "class": _ctx.cx('nullableIcon')
        }, _ctx.ptm('nullableIcon')), null, 16)];
      })], 16, _hoisted_2)], 16);
    }

    script.render = render;

    return script;

})(primevue.icons.check, primevue.icons.times, primevue.basecomponent, primevue.tristatecheckbox.style, Vue);
