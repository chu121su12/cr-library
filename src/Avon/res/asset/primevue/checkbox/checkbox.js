this.primevue = this.primevue || {};
this.primevue.checkbox = (function (CheckIcon, utils, BaseComponent, CheckboxStyle, vue) {
    'use strict';

    function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

    var CheckIcon__default = /*#__PURE__*/_interopDefaultLegacy(CheckIcon);
    var BaseComponent__default = /*#__PURE__*/_interopDefaultLegacy(BaseComponent);
    var CheckboxStyle__default = /*#__PURE__*/_interopDefaultLegacy(CheckboxStyle);

    var script$1 = {
      name: 'BaseCheckbox',
      "extends": BaseComponent__default["default"],
      props: {
        value: null,
        modelValue: null,
        binary: Boolean,
        name: {
          type: String,
          "default": null
        },
        trueValue: {
          type: null,
          "default": true
        },
        falseValue: {
          type: null,
          "default": false
        },
        disabled: {
          type: Boolean,
          "default": false
        },
        readonly: {
          type: Boolean,
          "default": false
        },
        required: {
          type: Boolean,
          "default": false
        },
        tabindex: {
          type: Number,
          "default": null
        },
        inputId: {
          type: String,
          "default": null
        },
        inputClass: {
          type: [String, Object],
          "default": null
        },
        inputStyle: {
          type: Object,
          "default": null
        },
        inputProps: {
          type: null,
          "default": null
        },
        ariaLabelledby: {
          type: String,
          "default": null
        },
        ariaLabel: {
          type: String,
          "default": null
        }
      },
      style: CheckboxStyle__default["default"],
      provide: function provide() {
        return {
          $parentInstance: this
        };
      }
    };

    function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }
    function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
    function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
    function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }
    function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }
    function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
    var script = {
      name: 'Checkbox',
      "extends": script$1,
      emits: ['click', 'update:modelValue', 'change', 'input', 'focus', 'blur'],
      data: function data() {
        return {
          focused: false
        };
      },
      methods: {
        getPTOptions: function getPTOptions(key) {
          return this.ptm(key, {
            context: {
              checked: this.checked,
              focused: this.focused,
              disabled: this.disabled
            }
          });
        },
        onClick: function onClick(event) {
          var _this = this;
          if (!this.disabled && !this.readonly) {
            var newModelValue;
            if (this.binary) {
              newModelValue = this.checked ? this.falseValue : this.trueValue;
            } else {
              if (this.checked) newModelValue = this.modelValue.filter(function (val) {
                return !utils.ObjectUtils.equals(val, _this.value);
              });else newModelValue = this.modelValue ? [].concat(_toConsumableArray(this.modelValue), [this.value]) : [this.value];
            }
            this.$emit('click', event);
            this.$emit('update:modelValue', newModelValue);
            this.$emit('change', event);
            this.$emit('input', newModelValue);
            this.$refs.input.focus();
          }
        },
        onFocus: function onFocus(event) {
          this.focused = true;
          this.$emit('focus', event);
        },
        onBlur: function onBlur(event) {
          this.focused = false;
          this.$emit('blur', event);
        }
      },
      computed: {
        checked: function checked() {
          return this.binary ? this.modelValue === this.trueValue : utils.ObjectUtils.contains(this.value, this.modelValue);
        }
      },
      components: {
        CheckIcon: CheckIcon__default["default"]
      }
    };

    function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
    function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
    function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
    function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
    function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : String(i); }
    function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
    var _hoisted_1 = ["id", "value", "name", "checked", "tabindex", "disabled", "readonly", "required", "aria-labelledby", "aria-label"];
    var _hoisted_2 = ["data-p-highlight", "data-p-disabled", "data-p-focused"];
    function render(_ctx, _cache, $props, $setup, $data, $options) {
      var _component_CheckIcon = vue.resolveComponent("CheckIcon");
      return vue.openBlock(), vue.createElementBlock("div", vue.mergeProps({
        "class": _ctx.cx('root'),
        onClick: _cache[2] || (_cache[2] = function ($event) {
          return $options.onClick($event);
        })
      }, $options.getPTOptions('root'), {
        "data-pc-name": "checkbox"
      }), [vue.createElementVNode("div", vue.mergeProps({
        "class": "p-hidden-accessible"
      }, _ctx.ptm('hiddenInputWrapper'), {
        "data-p-hidden-accessible": true
      }), [vue.createElementVNode("input", vue.mergeProps({
        ref: "input",
        id: _ctx.inputId,
        type: "checkbox",
        value: _ctx.value,
        name: _ctx.name,
        checked: $options.checked,
        tabindex: _ctx.tabindex,
        disabled: _ctx.disabled,
        readonly: _ctx.readonly,
        required: _ctx.required,
        "aria-labelledby": _ctx.ariaLabelledby,
        "aria-label": _ctx.ariaLabel,
        onFocus: _cache[0] || (_cache[0] = function ($event) {
          return $options.onFocus($event);
        }),
        onBlur: _cache[1] || (_cache[1] = function ($event) {
          return $options.onBlur($event);
        })
      }, _ctx.ptm('hiddenInput')), null, 16, _hoisted_1)], 16), vue.createElementVNode("div", vue.mergeProps({
        ref: "box",
        "class": [_ctx.cx('input'), _ctx.inputClass],
        style: _ctx.inputStyle
      }, _objectSpread(_objectSpread({}, _ctx.inputProps), $options.getPTOptions('input')), {
        "data-p-highlight": $options.checked,
        "data-p-disabled": _ctx.disabled,
        "data-p-focused": $data.focused
      }), [vue.renderSlot(_ctx.$slots, "icon", {
        checked: $options.checked,
        "class": vue.normalizeClass(_ctx.cx('icon'))
      }, function () {
        return [$options.checked ? (vue.openBlock(), vue.createBlock(_component_CheckIcon, vue.mergeProps({
          key: 0,
          "class": _ctx.cx('icon')
        }, $options.getPTOptions('icon')), null, 16, ["class"])) : vue.createCommentVNode("", true)];
      })], 16, _hoisted_2)], 16);
    }

    script.render = render;

    return script;

})(primevue.icons.check, primevue.utils, primevue.basecomponent, primevue.checkbox.style, Vue);
