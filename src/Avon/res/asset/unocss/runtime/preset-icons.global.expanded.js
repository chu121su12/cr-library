"use strict";

(() => {
    var A = Object.create;
    var o = Object.defineProperty;
    var N = Object.getOwnPropertyDescriptor;
    var L = Object.getOwnPropertyNames;
    var P = Object.getPrototypeOf, D = Object.prototype.hasOwnProperty;
    var p = (e => typeof require < "u" ? require : typeof Proxy < "u" ? new Proxy(e, {
        get: (e, t) => (typeof require < "u" ? require : e)[t]
    }) : e)(function(e) {
        if (typeof require < "u") return require.apply(this, arguments);
        throw Error('Dynamic require of "' + e + '" is not supported');
    });
    var e = (e, t) => () => (e && (t = e(e = 0)), t);
    var t = (e, t) => () => (t || e((t = {
        exports: {}
    }).exports, t), t.exports), B = (e, t) => {
        for (var n in t) o(e, n, {
            get: t[n],
            enumerable: !0
        });
    }, r = (t, n, r, s) => {
        if (n && typeof n == "object" || typeof n == "function") for (let e of L(n)) !D.call(t, e) && e !== r && o(t, e, {
            get: () => n[e],
            enumerable: !(s = N(n, e)) || s.enumerable
        });
        return t;
    };
    var n = (e, t, n) => (n = e != null ? A(P(e)) : {}, r(t || !e || !e.__esModule ? o(n, "default", {
        value: e,
        enumerable: !0
    }) : n, e)), G = e => r(o({}, "__esModule", {
        value: !0
    }), e);
    var s, i, h, a, c = e(() => {
        "use strict";
        s = Object.freeze({
            left: 0,
            top: 0,
            width: 16,
            height: 16
        }), i = Object.freeze({
            rotate: 0,
            vFlip: !1,
            hFlip: !1
        }), h = Object.freeze({
            ...s,
            ...i
        }), a = Object.freeze({
            ...h,
            body: "",
            hidden: !1
        });
    });
    var l, g, u = e(() => {
        "use strict";
        c();
        l = Object.freeze({
            width: null,
            height: null
        }), g = Object.freeze({
            ...l,
            ...i
        });
    });
    function q(e, t) {
        let n = {};
        !e.hFlip != !t.hFlip && (n.hFlip = !0), !e.vFlip != !t.vFlip && (n.vFlip = !0);
        let r = ((e.rotate || 0) + (t.rotate || 0)) % 4;
        return r && (n.rotate = r), n;
    }
    var U = e(() => {
        "use strict";
    });
    function f(t, n) {
        let r = q(t, n);
        for (let e in a) e in i ? e in t && !(e in r) && (r[e] = i[e]) : e in n ? r[e] = n[e] : e in t && (r[e] = t[e]);
        return r;
    }
    var V = e(() => {
        "use strict";
        c();
        U();
    });
    function Y(e, t) {
        let r = e.icons, s = e.aliases || Object.create(null), o = Object.create(null);
        function i(n) {
            if (r[n]) return o[n] = [];
            if (!(n in o)) {
                o[n] = null;
                let e = s[n] && s[n].parent, t = e && i(e);
                t && (o[n] = [ e ].concat(t));
            }
            return o[n];
        }
        return (t || Object.keys(r).concat(Object.keys(s))).forEach(i), o;
    }
    var W = e(() => {
        "use strict";
    });
    function d(e, t, n) {
        let r = e.icons, s = e.aliases || Object.create(null), o = {};
        function i(e) {
            o = f(r[e] || s[e], o);
        }
        return i(t), n.forEach(i), f(e, o);
    }
    function Z(e, t) {
        if (e.icons[t]) return d(e, t, []);
        let n = Y(e, [ t ])[t];
        return n ? d(e, t, n) : null;
    }
    var J = e(() => {
        "use strict";
        V();
        W();
    });
    function C(e, t, n) {
        if (t === 1) return e;
        if (n = n || 100, typeof e == "number") return Math.ceil(e * t * n) / n;
        if (typeof e != "string") return e;
        let r = e.split(m);
        if (r === null || !r.length) return e;
        let s = [], o = r.shift(), i = w.test(o);
        for (;;) {
            if (i) {
                let e = parseFloat(o);
                isNaN(e) ? s.push(o) : s.push(Math.ceil(e * t * n) / n);
            } else s.push(o);
            if (o = r.shift(), o === void 0) return s.join("");
            i = !i;
        }
    }
    var m, w, v = e(() => {
        "use strict";
        m = /(-?[0-9.]*[0-9]+[0-9.]*)/g, w = /^-?[0-9.]*[0-9]+[0-9.]*$/g;
    });
    function X(r, s = "defs") {
        let o = "", i = r.indexOf("<" + s);
        for (;i >= 0; ) {
            let e = r.indexOf(">", i), t = r.indexOf("</" + s);
            if (e === -1 || t === -1) break;
            let n = r.indexOf(">", t);
            if (n === -1) break;
            o += r.slice(e + 1, t).trim(), r = r.slice(0, i).trim() + r.slice(n + 1);
        }
        return {
            defs: o,
            content: r
        };
    }
    function H(e, t) {
        return e ? "<defs>" + e + "</defs>" + t : t;
    }
    function K(e, t, n) {
        let r = X(e);
        return H(r.defs, t + r.content + n);
    }
    var Q = e(() => {
        "use strict";
    });
    function ee(e, t) {
        let n = {
            ...h,
            ...e
        }, r = {
            ...g,
            ...t
        }, i = {
            left: n.left,
            top: n.top,
            width: n.width,
            height: n.height
        }, a = n.body;
        [ n, r ].forEach(e => {
            let t = [], n = e.hFlip, r = e.vFlip, s = e.rotate;
            n ? r ? s += 2 : (t.push("translate(" + (i.width + i.left).toString() + " " + (0 - i.top).toString() + ")"), 
            t.push("scale(-1 1)"), i.top = i.left = 0) : r && (t.push("translate(" + (0 - i.left).toString() + " " + (i.height + i.top).toString() + ")"), 
            t.push("scale(1 -1)"), i.top = i.left = 0);
            let o;
            switch (s < 0 && (s -= Math.floor(s / 4) * 4), s = s % 4, s) {
              case 1:
                o = i.height / 2 + i.top, t.unshift("rotate(90 " + o.toString() + " " + o.toString() + ")");
                break;

              case 2:
                t.unshift("rotate(180 " + (i.width / 2 + i.left).toString() + " " + (i.height / 2 + i.top).toString() + ")");
                break;

              case 3:
                o = i.width / 2 + i.left, t.unshift("rotate(-90 " + o.toString() + " " + o.toString() + ")");
                break;
            }
            s % 2 === 1 && (i.left !== i.top && (o = i.left, i.left = i.top, i.top = o), 
            i.width !== i.height && (o = i.width, i.width = i.height, i.height = o)), 
            t.length && (a = K(a, '<g transform="' + t.join(" ") + '">', "</g>"));
        });
        let s = r.width, o = r.height, c = i.width, l = i.height, u, f;
        s === null ? (f = o === null ? "1em" : o === "auto" ? l : o, u = C(f, c / l)) : (u = s === "auto" ? c : s, 
        f = o === null ? C(u, l / c) : o === "auto" ? l : o);
        let p = {}, d = (e, t) => {
            y(t) || (p[e] = t.toString());
        };
        d("width", u), d("height", f);
        let m = [ i.left, i.top, c, l ];
        return p.viewBox = m.join(" "), {
            attributes: p,
            viewBox: m,
            body: a
        };
    }
    var y, b = e(() => {
        "use strict";
        c();
        u();
        v();
        Q();
        y = e => e === "unset" || e === "undefined" || e === "none";
    });
    function te(e) {
        return e.replace(/(['"])\s*\n\s*([^>\\/\s])/g, "$1 $2").replace(/(["';{}><])\s*\n\s*/g, "$1").replace(/\s*\n\s*/g, " ").replace(/\s+"/g, '"').replace(/="\s+/g, '="').replace(/(\s)+\/>/g, "/>").trim();
    }
    var ne = e(() => {
        "use strict";
    });
    function re(e, o, i) {
        let a = e.slice(0, e.indexOf(">")), t = (e, t) => {
            let n = t.exec(a), r = n != null, s = o[e];
            return !s && !y(s) && (typeof i == "number" ? i > 0 && (o[e] = C(n?.[1] ?? "1em", i)) : n && (o[e] = n[1])), 
            r;
        };
        return [ t("width", O), t("height", x) ];
    }
    async function F(n, e, t, r, s, o) {
        let {
            scale: i,
            addXmlNs: a = !1
        } = r ?? {}, {
            additionalProps: c = {},
            iconCustomizer: l
        } = r?.customizations ?? {}, u = await s?.() ?? {};
        await l?.(e, t, u), Object.keys(c).forEach(e => {
            let t = c[e];
            t != null && (u[e] = t);
        }), o?.(u);
        let [ f, p ] = re(n, u, i);
        a && (!n.includes("xmlns=") && !u.xmlns && (u.xmlns = "http://www.w3.org/2000/svg"), 
        !n.includes("xmlns:xlink=") && n.includes("xlink:") && !u["xmlns:xlink"] && (u["xmlns:xlink"] = "http://www.w3.org/1999/xlink"));
        let d = Object.keys(u).map(e => e === "width" && f || e === "height" && p ? null : `${e}="${u[e]}"`).filter(e => e != null);
        if (d.length && (n = n.replace(k, `<svg ${d.join(" ")} `)), r) {
            let {
                defaultStyle: e,
                defaultClass: t
            } = r;
            t && !n.includes("class=") && (n = n.replace(k, `<svg class="${t}" `)), 
            e && !n.includes("style=") && (n = n.replace(k, `<svg style="${e}" `));
        }
        let m = r?.usedProps;
        return m && (Object.keys(c).forEach(e => {
            let t = u[e];
            t != null && (m[e] = t);
        }), typeof u.width < "u" && u.width !== null && (m.width = u.width), typeof u.height < "u" && u.height !== null && (m.height = u.height)), 
        n;
    }
    var O, x, k, j = e(() => {
        "use strict";
        b();
        v();
        O = /\swidth\s*=\s*["']([\w.]+)["']/, x = /\sheight\s*=\s*["']([\w.]+)["']/, 
        k = /<svg\s+/;
    });
    var se = t((e, t) => {
        "use strict";
        var s = 1e3, o = s * 60, i = o * 60, a = i * 24, c = a * 7, l = a * 365.25;
        t.exports = function(e, t) {
            t = t || {};
            var n = typeof e;
            if (n === "string" && e.length > 0) return r(e);
            if (n === "number" && isFinite(e)) return t["long"] ? f(e) : u(e);
            throw new Error("val is not a non-empty string or a valid number. val=" + JSON.stringify(e));
        };
        function r(e) {
            if (e = String(e), !(e.length > 100)) {
                var t = /^(-?(?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|weeks?|w|years?|yrs?|y)?$/i.exec(e);
                if (t) {
                    var n = parseFloat(t[1]), r = (t[2] || "ms").toLowerCase();
                    switch (r) {
                      case "years":
                      case "year":
                      case "yrs":
                      case "yr":
                      case "y":
                        return n * l;

                      case "weeks":
                      case "week":
                      case "w":
                        return n * c;

                      case "days":
                      case "day":
                      case "d":
                        return n * a;

                      case "hours":
                      case "hour":
                      case "hrs":
                      case "hr":
                      case "h":
                        return n * i;

                      case "minutes":
                      case "minute":
                      case "mins":
                      case "min":
                      case "m":
                        return n * o;

                      case "seconds":
                      case "second":
                      case "secs":
                      case "sec":
                      case "s":
                        return n * s;

                      case "milliseconds":
                      case "millisecond":
                      case "msecs":
                      case "msec":
                      case "ms":
                        return n;

                      default:
                        return;
                    }
                }
            }
        }
        function u(e) {
            var t = Math.abs(e);
            return t >= a ? Math.round(e / a) + "d" : t >= i ? Math.round(e / i) + "h" : t >= o ? Math.round(e / o) + "m" : t >= s ? Math.round(e / s) + "s" : e + "ms";
        }
        function f(e) {
            var t = Math.abs(e);
            return t >= a ? n(e, t, a, "day") : t >= i ? n(e, t, i, "hour") : t >= o ? n(e, t, o, "minute") : t >= s ? n(e, t, s, "second") : e + " ms";
        }
        function n(e, t, n, r) {
            var s = t >= n * 1.5;
            return Math.round(e / n) + " " + r + (s ? "s" : "");
        }
    });
    var E = t((e, t) => {
        "use strict";
        function n(t) {
            a.debug = a, a["default"] = a, a.coerce = c, a.disable = r, a.enable = n, 
            a.enabled = s, a.humanize = se(), a.destroy = l, Object.keys(t).forEach(e => {
                a[e] = t[e];
            }), a.names = [], a.skips = [], a.formatters = {};
            function e(t) {
                let n = 0;
                for (let e = 0; e < t.length; e++) n = (n << 5) - n + t.charCodeAt(e), 
                n |= 0;
                return a.colors[Math.abs(n) % a.colors.length];
            }
            a.selectColor = e;
            function a(e) {
                let n, t = null, r, s;
                function i(...r) {
                    if (!i.enabled) return;
                    let s = i, e = Number(new Date()), t = e - (n || e);
                    s.diff = t, s.prev = n, s.curr = e, n = e, r[0] = a.coerce(r[0]), 
                    typeof r[0] != "string" && r.unshift("%O");
                    let o = 0;
                    r[0] = r[0].replace(/%([a-zA-Z%])/g, (t, e) => {
                        if (t === "%%") return "%";
                        o++;
                        let n = a.formatters[e];
                        if (typeof n == "function") {
                            let e = r[o];
                            t = n.call(s, e), r.splice(o, 1), o--;
                        }
                        return t;
                    }), a.formatArgs.call(s, r), (s.log || a.log).apply(s, r);
                }
                return i.namespace = e, i.useColors = a.useColors(), i.color = a.selectColor(e), 
                i.extend = o, i.destroy = a.destroy, Object.defineProperty(i, "enabled", {
                    enumerable: !0,
                    configurable: !1,
                    get: () => t !== null ? t : (r !== a.namespaces && (r = a.namespaces, 
                    s = a.enabled(e)), s),
                    set: e => {
                        t = e;
                    }
                }), typeof a.init == "function" && a.init(i), i;
            }
            function o(e, t) {
                let n = a(this.namespace + (typeof t > "u" ? ":" : t) + e);
                return n.log = this.log, n;
            }
            function n(e) {
                a.save(e), a.namespaces = e, a.names = [], a.skips = [];
                let t, n = (typeof e == "string" ? e : "").split(/[\s,]+/), r = n.length;
                for (t = 0; t < r; t++) n[t] && (e = n[t].replace(/\*/g, ".*?"), 
                e[0] === "-" ? a.skips.push(new RegExp("^" + e.slice(1) + "$")) : a.names.push(new RegExp("^" + e + "$")));
            }
            function r() {
                let e = [ ...a.names.map(i), ...a.skips.map(i).map(e => "-" + e) ].join(",");
                return a.enable(""), e;
            }
            function s(e) {
                if (e[e.length - 1] === "*") return !0;
                let t, n;
                for (t = 0, n = a.skips.length; t < n; t++) if (a.skips[t].test(e)) return !1;
                for (t = 0, n = a.names.length; t < n; t++) if (a.names[t].test(e)) return !0;
                return !1;
            }
            function i(e) {
                return e.toString().substring(2, e.toString().length - 2).replace(/\.\*\?$/, "*");
            }
            function c(e) {
                return e instanceof Error ? e.stack || e.message : e;
            }
            function l() {
                console.warn("Instance method `debug.destroy()` is deprecated and no longer does anything. It will be removed in the next major version of `debug`.");
            }
            return a.enable(a.load()), a;
        }
        t.exports = n;
    });
    var oe = t((t, s) => {
        "use strict";
        t.formatArgs = n;
        t.save = r;
        t.load = o;
        t.useColors = e;
        t.storage = i();
        t.destroy = (() => {
            let e = !1;
            return () => {
                e || (e = !0, console.warn("Instance method `debug.destroy()` is deprecated and no longer does anything. It will be removed in the next major version of `debug`."));
            };
        })();
        t.colors = [ "#0000CC", "#0000FF", "#0033CC", "#0033FF", "#0066CC", "#0066FF", "#0099CC", "#0099FF", "#00CC00", "#00CC33", "#00CC66", "#00CC99", "#00CCCC", "#00CCFF", "#3300CC", "#3300FF", "#3333CC", "#3333FF", "#3366CC", "#3366FF", "#3399CC", "#3399FF", "#33CC00", "#33CC33", "#33CC66", "#33CC99", "#33CCCC", "#33CCFF", "#6600CC", "#6600FF", "#6633CC", "#6633FF", "#66CC00", "#66CC33", "#9900CC", "#9900FF", "#9933CC", "#9933FF", "#99CC00", "#99CC33", "#CC0000", "#CC0033", "#CC0066", "#CC0099", "#CC00CC", "#CC00FF", "#CC3300", "#CC3333", "#CC3366", "#CC3399", "#CC33CC", "#CC33FF", "#CC6600", "#CC6633", "#CC9900", "#CC9933", "#CCCC00", "#CCCC33", "#FF0000", "#FF0033", "#FF0066", "#FF0099", "#FF00CC", "#FF00FF", "#FF3300", "#FF3333", "#FF3366", "#FF3399", "#FF33CC", "#FF33FF", "#FF6600", "#FF6633", "#FF9900", "#FF9933", "#FFCC00", "#FFCC33" ];
        function e() {
            if (typeof window < "u" && window.process && (window.process.type === "renderer" || window.process.__nwjs)) return !0;
            if (typeof navigator < "u" && navigator.userAgent && navigator.userAgent.toLowerCase().match(/(edge|trident)\/(\d+)/)) return !1;
            let e;
            return typeof document < "u" && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance || typeof window < "u" && window.console && (window.console.firebug || window.console.exception && window.console.table) || typeof navigator < "u" && navigator.userAgent && (e = navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/)) && parseInt(e[1], 10) >= 31 || typeof navigator < "u" && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/);
        }
        function n(e) {
            if (e[0] = (this.useColors ? "%c" : "") + this.namespace + (this.useColors ? " %c" : " ") + e[0] + (this.useColors ? "%c " : " ") + "+" + s.exports.humanize(this.diff), 
            !this.useColors) return;
            let t = "color: " + this.color;
            e.splice(1, 0, t, "color: inherit");
            let n = 0, r = 0;
            e[0].replace(/%[a-zA-Z%]/g, e => {
                e !== "%%" && (n++, e === "%c" && (r = n));
            }), e.splice(r, 0, t);
        }
        t.log = console.debug || console.log || (() => {});
        function r(e) {
            try {
                e ? t.storage.setItem("debug", e) : t.storage.removeItem("debug");
            } catch {}
        }
        function o() {
            let e;
            try {
                e = t.storage.getItem("debug");
            } catch {}
            return !e && typeof process < "u" && "env" in process && (e = process.env.DEBUG), 
            e;
        }
        function i() {
            try {
                return localStorage;
            } catch {}
        }
        s.exports = E()(t);
        var {
            formatters: a
        } = s.exports;
        a.j = function(e) {
            try {
                return JSON.stringify(e);
            } catch (t) {
                return "[UnexpectedJSONParseError]: " + t.message;
            }
        };
    });
    var ie = {};
    B(ie, {
        createSupportsColor: () => _,
        "default": () => pe
    });
    function $(e, t = globalThis.Deno ? globalThis.Deno.args : I["default"].argv) {
        let n = e.startsWith("-") ? "" : e.length === 1 ? "-" : "--", r = t.indexOf(n + e), s = t.indexOf("--");
        return r !== -1 && (s === -1 || r < s);
    }
    function ae() {
        if ("FORCE_COLOR" in R) return R.FORCE_COLOR === "true" ? 1 : R.FORCE_COLOR === "false" ? 0 : R.FORCE_COLOR.length === 0 ? 1 : Math.min(Number.parseInt(R.FORCE_COLOR, 10), 3);
    }
    function ce(e) {
        return e === 0 ? !1 : {
            level: e,
            hasBasic: !0,
            has256: e >= 2,
            has16m: e >= 3
        };
    }
    function le(e, {
        streamIsTTY: t,
        sniffFlags: n = !0
    } = {}) {
        let r = ae();
        r !== void 0 && (T = r);
        let s = n ? T : r;
        if (s === 0) return 0;
        if (n) {
            if ($("color=16m") || $("color=full") || $("color=truecolor")) return 3;
            if ($("color=256")) return 2;
        }
        if ("TF_BUILD" in R && "AGENT_NAME" in R) return 1;
        if (e && !t && s === void 0) return 0;
        let o = s || 0;
        if (R.TERM === "dumb") return o;
        if (I["default"].platform === "win32") {
            let e = ue["default"].release().split(".");
            return Number(e[0]) >= 10 && Number(e[2]) >= 10586 ? Number(e[2]) >= 14931 ? 3 : 2 : 1;
        }
        if ("CI" in R) return "GITHUB_ACTIONS" in R || "GITEA_ACTIONS" in R ? 3 : [ "TRAVIS", "CIRCLECI", "APPVEYOR", "GITLAB_CI", "BUILDKITE", "DRONE" ].some(e => e in R) || R.CI_NAME === "codeship" ? 1 : o;
        if ("TEAMCITY_VERSION" in R) return /^(9\.(0*[1-9]\d*)\.|\d{2,}\.)/.test(R.TEAMCITY_VERSION) ? 1 : 0;
        if (R.COLORTERM === "truecolor" || R.TERM === "xterm-kitty") return 3;
        if ("TERM_PROGRAM" in R) {
            let e = Number.parseInt((R.TERM_PROGRAM_VERSION || "").split(".")[0], 10);
            switch (R.TERM_PROGRAM) {
              case "iTerm.app":
                return e >= 3 ? 3 : 2;

              case "Apple_Terminal":
                return 2;
            }
        }
        return /-256(color)?$/i.test(R.TERM) ? 2 : /^screen|^xterm|^vt100|^vt220|^rxvt|color|ansi|cygwin|linux/i.test(R.TERM) || "COLORTERM" in R ? 1 : o;
    }
    function _(e, t = {}) {
        let n = le(e, {
            streamIsTTY: e && e.isTTY,
            ...t
        });
        return ce(n);
    }
    var I, ue, S, R, T, fe, pe, de = e(() => {
        "use strict";
        I = n(p("node:process"), 1), ue = n(p("node:os"), 1), S = n(p("node:tty"), 1);
        ({
            env: R
        } = I["default"]);
        $("no-color") || $("no-colors") || $("color=false") || $("color=never") ? T = 0 : ($("color") || $("colors") || $("color=true") || $("color=always")) && (T = 1);
        fe = {
            stdout: _({
                isTTY: S["default"].isatty(1)
            }),
            stderr: _({
                isTTY: S["default"].isatty(2)
            })
        }, pe = fe;
    });
    var me = t((r, o) => {
        "use strict";
        var e = p("tty"), t = p("util");
        r.init = u;
        r.log = a;
        r.formatArgs = s;
        r.save = c;
        r.load = l;
        r.useColors = n;
        r.destroy = t.deprecate(() => {}, "Instance method `debug.destroy()` is deprecated and no longer does anything. It will be removed in the next major version of `debug`.");
        r.colors = [ 6, 2, 3, 4, 5, 1 ];
        try {
            let e = (de(), G(ie));
            e && (e.stderr || e).level >= 2 && (r.colors = [ 20, 21, 26, 27, 32, 33, 38, 39, 40, 41, 42, 43, 44, 45, 56, 57, 62, 63, 68, 69, 74, 75, 76, 77, 78, 79, 80, 81, 92, 93, 98, 99, 112, 113, 128, 129, 134, 135, 148, 149, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 178, 179, 184, 185, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 214, 215, 220, 221 ]);
        } catch {}
        r.inspectOpts = Object.keys(process.env).filter(e => /^debug_/i.test(e)).reduce((e, t) => {
            let n = t.substring(6).toLowerCase().replace(/_([a-z])/g, (e, t) => t.toUpperCase()), r = process.env[t];
            return /^(yes|on|true|enabled)$/i.test(r) ? r = !0 : /^(no|off|false|disabled)$/i.test(r) ? r = !1 : r === "null" ? r = null : r = Number(r), 
            e[n] = r, e;
        }, {});
        function n() {
            return "colors" in r.inspectOpts ? !!r.inspectOpts.colors : e.isatty(process.stderr.fd);
        }
        function s(r) {
            let {
                namespace: s,
                useColors: e
            } = this;
            if (e) {
                let e = this.color, t = "\x1b[3" + (e < 8 ? e : "8;5;" + e), n = `  ${t};1m${s} \x1B[0m`;
                r[0] = n + r[0].split(`\n`).join(`\n` + n), r.push(t + "m+" + o.exports.humanize(this.diff) + "\x1b[0m");
            } else r[0] = i() + s + " " + r[0];
        }
        function i() {
            return r.inspectOpts.hideDate ? "" : new Date().toISOString() + " ";
        }
        function a(...e) {
            return process.stderr.write(t.formatWithOptions(r.inspectOpts, ...e) + `\n`);
        }
        function c(e) {
            e ? process.env.DEBUG = e : delete process.env.DEBUG;
        }
        function l() {
            return process.env.DEBUG;
        }
        function u(t) {
            t.inspectOpts = {};
            let n = Object.keys(r.inspectOpts);
            for (let e = 0; e < n.length; e++) t.inspectOpts[n[e]] = r.inspectOpts[n[e]];
        }
        o.exports = E()(r);
        var {
            formatters: f
        } = o.exports;
        f.o = function(e) {
            return this.inspectOpts.colors = this.useColors, t.inspect(e, this.inspectOpts).split(`\n`).map(e => e.trim()).join(" ");
        };
        f.O = function(e) {
            return this.inspectOpts.colors = this.useColors, t.inspect(e, this.inspectOpts);
        };
    });
    var z = t((e, t) => {
        "use strict";
        typeof process > "u" || process.type === "renderer" || process.browser === !0 || process.__nwjs ? t.exports = oe() : t.exports = me();
    });
    async function he(t, n, r, s) {
        let o;
        Ce(`${n}:${r}`);
        try {
            if (typeof t == "function") o = await t(r); else {
                let e = t[r];
                o = typeof e == "function" ? await e() : e;
            }
        } catch (e) {
            console.warn(`Failed to load custom icon "${r}" in "${n}":`, e);
            return;
        }
        if (o) {
            let e = o.indexOf("<svg");
            e > 0 && (o = o.slice(e));
            let {
                transform: t
            } = s?.customizations ?? {};
            return o = typeof t == "function" ? await t(o, n, r) : o, o.startsWith("<svg") ? await F(s?.customizations?.trimCustomSvg === !0 ? te(o) : o, n, r, s, void 0) : (console.warn(`Custom icon "${r}" in "${n}" is not a valid SVG`), 
            o);
        }
    }
    var ge, Ce, we = e(() => {
        "use strict";
        ge = n(z(), 1);
        j();
        ne();
        Ce = (0, ge["default"])("@iconify-loader:custom");
    });
    async function ve(e, a, t, c) {
        let l, {
            customize: u
        } = c?.customizations ?? {};
        for (let i of t) if (l = Z(e, i), l) {
            be(`${a}:${i}`);
            let e = {
                ...g
            };
            typeof u == "function" && (l = Object.assign({}, l), e = u(e, l, `${a}:${i}`) ?? e);
            let {
                attributes: {
                    width: t,
                    height: n,
                    ...r
                },
                body: s
            } = ee(l, e), o = c?.scale;
            return await F(`<svg >${s}</svg>`, a, i, c, () => ({
                ...r
            }), s => {
                let e = (e, t) => {
                    let n = s[e], r;
                    if (!y(n)) {
                        if (n) return;
                        typeof o == "number" ? o && (r = C(t ?? "1em", o)) : r = t;
                    }
                    r ? s[e] = r : delete s[e];
                };
                e("width", t), e("height", n);
            });
        }
    }
    var ye, be, Fe = e(() => {
        "use strict";
        b();
        J();
        v();
        j();
        ye = n(z(), 1);
        u();
        be = (0, ye["default"])("@iconify-loader:icon");
    });
    var Oe, M, xe = e(() => {
        "use strict";
        we();
        Fe();
        Oe = n(z(), 1), M = async (n, r, s) => {
            let e = s?.customCollections?.[n];
            if (e) if (typeof e == "function") {
                let t;
                try {
                    t = await e(r);
                } catch (o) {
                    console.warn(`Failed to load custom icon "${r}" in "${n}":`, o);
                    return;
                }
                if (t) {
                    if (typeof t == "string") return await he(() => t, n, r, s);
                    if ("icons" in t) {
                        let e = [ r, r.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase(), r.replace(/([a-z])(\d+)/g, "$1-$2") ];
                        return await ve(t, n, e, s);
                    }
                }
            } else return await he(e, n, r, s);
        };
    });
    function ke(e) {
        return e.replace(/"/g, "'").replace(/%/g, "%25").replace(/#/g, "%23").replace(/</g, "%3C").replace(/>/g, "%3E").replace(/\s+/g, " ");
    }
    function je(e) {
        let t = e.startsWith("<svg>") ? e.replace("<svg>", "<svg >") : e;
        return !t.includes(" xmlns:xlink=") && t.includes(" xlink:") && (t = t.replace("<svg ", '<svg xmlns:xlink="http://www.w3.org/1999/xlink" ')), 
        t.includes(" xmlns=") || (t = t.replace("<svg ", '<svg xmlns="http://www.w3.org/2000/svg" ')), 
        ke(t);
    }
    xe();
    var Ee = n(z(), 1);
    var $e = new Set();
    function _e(e) {
        $e.has(e) || (console.warn("[unocss]", e), $e.add(e));
    }
    xe();
    Fe();
    var Ie = [ "academicons", "akar-icons", "ant-design", "arcticons", "basil", "bi", "bitcoin-icons", "bpmn", "brandico", "bx", "bxl", "bxs", "bytesize", "carbon", "catppuccin", "cbi", "charm", "ci", "cib", "cif", "cil", "circle-flags", "circum", "clarity", "codicon", "covid", "cryptocurrency-color", "cryptocurrency", "cuida", "dashicons", "devicon-line", "devicon-original", "devicon-plain", "devicon", "duo-icons", "ei", "el", "emblemicons", "emojione-monotone", "emojione-v1", "emojione", "entypo-social", "entypo", "eos-icons", "ep", "et", "eva", "f7", "fa-brands", "fa-regular", "fa-solid", "fa", "fa6-brands", "fa6-regular", "fa6-solid", "fad", "fe", "feather", "file-icons", "flag", "flagpack", "flat-color-icons", "flat-ui", "flowbite", "fluent-color", "fluent-emoji-flat", "fluent-emoji-high-contrast", "fluent-emoji", "fluent-mdl2", "fluent", "fontelico", "fontisto", "formkit", "foundation", "fxemoji", "gala", "game-icons", "geo", "gg", "gis", "gravity-ui", "gridicons", "grommet-icons", "guidance", "healthicons", "heroicons-outline", "heroicons-solid", "heroicons", "hugeicons", "humbleicons", "ic", "icomoon-free", "icon-park-outline", "icon-park-solid", "icon-park-twotone", "icon-park", "iconamoon", "iconoir", "icons8", "il", "ion", "iwwa", "jam", "la", "lets-icons", "line-md", "lineicons", "logos", "ls", "lsicon", "lucide-lab", "lucide", "mage", "majesticons", "maki", "map", "marketeq", "material-symbols-light", "material-symbols", "mdi-light", "mdi", "medical-icon", "memory", "meteocons", "mi", "mingcute", "mono-icons", "mynaui", "nimbus", "nonicons", "noto-v1", "noto", "octicon", "oi", "ooui", "openmoji", "oui", "pajamas", "pepicons-pencil", "pepicons-pop", "pepicons-print", "pepicons", "ph", "pixelarticons", "prime", "proicons", "ps", "quill", "radix-icons", "raphael", "ri", "rivet-icons", "si-glyph", "si", "simple-icons", "simple-line-icons", "skill-icons", "solar", "stash", "streamline-emojis", "streamline", "subway", "svg-spinners", "system-uicons", "tabler", "tdesign", "teenyicons", "token-branded", "token", "topcoat", "twemoji", "typcn", "uil", "uim", "uis", "uit", "uiw", "unjs", "vaadin", "vs", "vscode-icons", "websymbol", "weui", "whh", "wi", "wpf", "zmdi", "zondicons" ];
    var Se = 3;
    function Re(g) {
        return (c = {}) => {
            let {
                scale: r = 1,
                mode: l = "auto",
                prefix: e = "i-",
                warn: u = !1,
                collections: t,
                extraProperties: n = {},
                customizations: s = {},
                autoInstall: o = !1,
                collectionsNodeResolvePath: i,
                layer: a = "icons",
                unit: f,
                processor: p
            } = c, d = ze(), m = {
                addXmlNs: !0,
                scale: r,
                customCollections: t,
                autoInstall: o,
                cwd: i,
                warn: void 0,
                customizations: {
                    ...s,
                    additionalProps: {
                        ...n
                    },
                    trimCustomSvg: !0,
                    async iconCustomizer(e, t, n) {
                        await s.iconCustomizer?.(e, t, n), f && (n.width || (n.width = `${r}${f}`), 
                        n.height || (n.height = `${r}${f}`));
                    }
                }
            }, h;
            return {
                name: "@unocss/preset-icons",
                enforce: "pre",
                options: c,
                layers: {
                    icons: -30
                },
                api: {
                    encodeSvgForCss: je,
                    parseIconWithLoader: Me
                },
                rules: [ [ /^([\w:-]+)(?:\?(mask|bg|auto))?$/, async e => {
                    let [ t, n, r = l ] = e;
                    h = h || await g(c);
                    let s = {}, o = await Me(n, h, {
                        ...m,
                        usedProps: s
                    });
                    if (!o) {
                        u && !d.isESLint && _e(`failed to load icon "${t}"`);
                        return;
                    }
                    let i, a = `url("data:image/svg+xml;utf8,${je(o.svg)}")`;
                    return r === "auto" && (r = o.svg.includes("currentColor") ? "mask" : "bg"), 
                    r === "mask" ? i = {
                        "--un-icon": a,
                        "-webkit-mask": "var(--un-icon) no-repeat",
                        mask: "var(--un-icon) no-repeat",
                        "-webkit-mask-size": "100% 100%",
                        "mask-size": "100% 100%",
                        "background-color": "currentColor",
                        color: "inherit",
                        ...s
                    } : i = {
                        background: `${a} no-repeat`,
                        "background-size": "100% 100%",
                        "background-color": "transparent",
                        ...s
                    }, p?.(i, {
                        ...o,
                        icon: o.name,
                        mode: r
                    }), i;
                }, {
                    layer: a,
                    prefix: e
                } ] ]
            };
        };
    }
    function Te(t, n) {
        let r = new Map();
        function e(e) {
            if (Ie.includes(e)) return r.has(e) || r.set(e, t(`${n}@iconify-json/${e}/icons.json`)), 
            r.get(e);
        }
        return async (t, n, r) => {
            let s = await M(t, n, r);
            if (s) return s;
            let o = await e(t);
            if (o) {
                let e = [ n, n.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase(), n.replace(/([a-z])(\d+)/g, "$1-$2") ];
                s = await ve(o, t, e, r);
            }
            return s;
        };
    }
    function ze() {
        let e = typeof process < "u" && process.stdout && !process.versions.deno, t = e && !!process.env.VSCODE_CWD, n = e && !!process.env.ESLINT;
        return {
            isNode: e,
            isVSCode: t,
            isESLint: n
        };
    }
    async function Me(e, n, r = {}) {
        let s = "", o = "", i;
        if (e.includes(":")) [ s, o ] = e.split(":"), i = await n(s, o, r); else {
            let t = e.split(/-/g);
            for (let e = Se; e >= 1 && (s = t.slice(0, e).join("-"), o = t.slice(e).join("-"), 
            i = await n(s, o, r), !i); e--);
        }
        if (i) return {
            collection: s,
            name: o,
            svg: i
        };
    }
    window.__unocss_runtime = window.__unocss_runtime ?? {};
    window.__unocss_runtime.presets = Object.assign(window.__unocss_runtime?.presets ?? {}, (() => {
        let t = Re(async e => {
            let t = e?.customFetch ?? (e => fetch(e).then(e => e.json())), n = e?.cdn;
            return n ? Te(t, n) : M;
        });
        return {
            presetIcons: e => t(e)
        };
    })());
})();