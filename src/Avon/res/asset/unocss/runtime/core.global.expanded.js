"use strict";

(() => {
    var b = "default", $ = "preflights", e = "shortcuts", t = "imports", A = {
        [t]: -200,
        [$]: -100,
        [e]: -10,
        [b]: 0
    };
    var r = /[\\:]?[\s'"`;{}]+/g;
    function n(e) {
        return e.split(r);
    }
    var S = {
        name: "@unocss/core/extractor-split",
        order: 0,
        extract({
            code: e
        }) {
            return n(e);
        }
    };
    function C(e = []) {
        return Array.isArray(e) ? e : [ e ];
    }
    function M(e) {
        return Array.from(new Set(e));
    }
    function x(e, r) {
        return e.reduce((e, t) => (e.findIndex(e => r(t, e)) === -1 && e.push(t), 
        e), []);
    }
    function E(e) {
        return typeof e == "string";
    }
    var _ = class extends Set {
        _map;
        constructor(e) {
            super(e), this._map ??= new Map();
        }
        add(e) {
            return this._map ??= new Map(), this._map.set(e, (this._map.get(e) ?? 0) + 1), 
            super.add(e);
        }
        "delete"(e) {
            return this._map["delete"](e), super["delete"](e);
        }
        clear() {
            this._map.clear(), super.clear();
        }
        getCount(e) {
            return this._map.get(e) ?? 0;
        }
        setCount(e, t) {
            return this._map.set(e, t), super.add(e);
        }
    };
    function j(e) {
        return e instanceof _;
    }
    function i(e) {
        let t = e.length, r = -1, n, i = "", a = e.charCodeAt(0);
        for (;++r < t; ) {
            if (n = e.charCodeAt(r), n === 0) {
                i += "\ufffd";
                continue;
            }
            if (n === 37) {
                i += "\\%";
                continue;
            }
            if (n === 44) {
                i += "\\,";
                continue;
            }
            if (n >= 1 && n <= 31 || n === 127 || r === 0 && n >= 48 && n <= 57 || r === 1 && n >= 48 && n <= 57 && a === 45) {
                i += `\\${n.toString(16)} `;
                continue;
            }
            if (r === 0 && t === 1 && n === 45) {
                i += `\\${e.charAt(r)}`;
                continue;
            }
            if (n >= 128 || n === 45 || n === 95 || n >= 48 && n <= 57 || n >= 65 && n <= 90 || n >= 97 && n <= 122) {
                i += e.charAt(r);
                continue;
            }
            i += `\\${e.charAt(r)}`;
        }
        return i;
    }
    var a = i;
    function P(e) {
        return typeof e == "function" ? {
            match: e
        } : e;
    }
    function h(e) {
        return e.length === 3;
    }
    function d(e) {
        return e != null;
    }
    function I() {}
    var V = class {
        _map = new Map();
        get(e, t) {
            let r = this._map.get(e);
            if (r) return r.get(t);
        }
        getFallback(e, t, r) {
            let n = this._map.get(e);
            return n || (n = new Map(), this._map.set(e, n)), n.has(t) || n.set(t, r), 
            n.get(t);
        }
        set(e, t, r) {
            let n = this._map.get(e);
            return n || (n = new Map(), this._map.set(e, n)), n.set(t, r), this;
        }
        has(e, t) {
            return this._map.get(e)?.has(t);
        }
        "delete"(e, t) {
            return this._map.get(e)?.["delete"](t) || !1;
        }
        deleteTop(e) {
            return this._map["delete"](e);
        }
        map(n) {
            return Array.from(this._map.entries()).flatMap(([ r, e ]) => Array.from(e.entries()).map(([ e, t ]) => n(t, r, e)));
        }
    }, F = class extends Map {
        getFallback(e, t) {
            let r = this.get(e);
            return r === void 0 ? (this.set(e, t), t) : r;
        }
        map(r) {
            let n = [];
            return this.forEach((e, t) => {
                n.push(r(e, t));
            }), n;
        }
        flatMap(r) {
            let n = [];
            return this.forEach((e, t) => {
                n.push(...r(e, t));
            }), n;
        }
    };
    function m(e) {
        return E(e) ? e : (Array.isArray(e) ? e : Object.entries(e)).filter(e => e[1] != null);
    }
    function U(e) {
        return Array.isArray(e) ? e.find(e => !Array.isArray(e) || Array.isArray(e[0])) ? e.map(e => m(e)) : [ e ] : [ m(e) ];
    }
    function D(i) {
        return i.filter(([ t, r ], n) => {
            if (t.startsWith("$$")) return !1;
            for (let e = n - 1; e >= 0; e--) if (i[e][0] === t && i[e][1] === r) return !1;
            return !0;
        });
    }
    function y(e) {
        return e == null ? "" : D(e).map(([ e, t ]) => t != null && typeof t != "function" ? `${e}:${t};` : void 0).filter(Boolean).join("");
    }
    function s(e) {
        return e && typeof e == "object" && !Array.isArray(e);
    }
    function o(e, t, r = !1) {
        let n = e, i = t;
        if (Array.isArray(i)) return r && Array.isArray(i) ? [ ...n, ...i ] : [ ...i ];
        let a = {
            ...n
        };
        return s(n) && s(i) && Object.keys(i).forEach(e => {
            s(n[e]) && s(i[e]) || Array.isArray(n[e]) && Array.isArray(i[e]) ? a[e] = o(n[e], i[e], r) : Object.assign(a, {
                [e]: i[e]
            });
        }), a;
    }
    function l(e) {
        let t, r, n;
        if (Array.isArray(e)) {
            for (r = Array.from({
                length: t = e.length
            }); t--; ) r[t] = (n = e[t]) && typeof n == "object" ? l(n) : n;
            return r;
        }
        if (Object.prototype.toString.call(e) === "[object Object]") {
            r = {};
            for (t in e) t === "__proto__" ? Object.defineProperty(r, t, {
                value: l(e[t]),
                configurable: !0,
                enumerable: !0,
                writable: !0
            }) : r[t] = (n = e[t]) && typeof n == "object" ? l(n) : n;
            return r;
        }
        return e;
    }
    function H(e) {
        return E(e[0]);
    }
    function W(e) {
        return E(e[0]);
    }
    var c = {};
    function R(e = [ "-", ":" ]) {
        let t = e.join("|");
        return c[t] || (c[t] = new RegExp(`((?:[!@<~\\w+:_-]|\\[&?>?:?\\S*\\])+?)(${t})\\(((?:[~!<>\\w\\s:/\\\\,%#.$?-]|\\[.*?\\])+?)\\)(?!\\s*?=>)`, "gm")), 
        c[t].lastIndex = 0, c[t];
    }
    function z(n, o = [ "-", ":" ], e = 5) {
        let t = R(o), l, r = n.toString(), c = new Set(), f = new Map();
        do {
            l = !1, r = r.replace(t, (e, n, i, t, r) => {
                if (!o.includes(i)) return e;
                l = !0, c.add(n + i);
                let a = r + n.length + i.length + 1, s = {
                    length: e.length,
                    items: []
                };
                f.set(r, s);
                for (let r of [ ...t.matchAll(/\S+/g) ]) {
                    let e = a + r.index, t = f.get(e)?.items;
                    t ? f["delete"](e) : t = [ {
                        offset: e,
                        length: r[0].length,
                        className: r[0]
                    } ];
                    for (let e of t) e.className = e.className === "~" ? n : e.className.replace(/^(!?)(.*)/, `$1${n}${i}$2`), 
                    s.items.push(e);
                }
                return "$".repeat(e.length);
            }), e -= 1;
        } while (l && e);
        let i;
        if (typeof n == "string") {
            i = "";
            let r = 0;
            for (let [ e, t ] of f) i += n.slice(r, e), i += t.items.map(e => e.className).join(" "), 
            r = e + t.length;
            i += n.slice(r);
        } else {
            i = n;
            for (let [ e, t ] of f) i.overwrite(e, e + t.length, t.items.map(e => e.className).join(" "));
        }
        return {
            prefixes: Array.from(c),
            hasChanged: l,
            groupsByOffset: f,
            get expanded() {
                return i.toString();
            }
        };
    }
    function q(e, t = [ "-", ":" ], r = 5) {
        let n = z(e, t, r);
        return typeof e == "string" ? n.expanded : e;
    }
    var f = new Set();
    function Y(e) {
        f.has(e) || (console.warn("[unocss]", e), f.add(e));
    }
    function k(e) {
        return C(e).flatMap(e => Array.isArray(e) ? [ e ] : Object.entries(e));
    }
    var u = "_uno_resolved";
    function Z(e) {
        let r = typeof e == "function" ? e() : e;
        if (u in r) return r;
        r = {
            ...r
        }, Object.defineProperty(r, u, {
            value: !0,
            enumerable: !1
        });
        let t = r.shortcuts ? k(r.shortcuts) : void 0;
        if (r.shortcuts = t, r.prefix || r.layer) {
            let e = e => {
                e[2] || (e[2] = {});
                let t = e[2];
                t.prefix == null && r.prefix && (t.prefix = C(r.prefix)), t.layer == null && r.layer && (t.layer = r.layer);
            };
            t?.forEach(e), r.rules?.forEach(e);
        }
        return r;
    }
    function O(e) {
        let t = Z(e);
        if (!t.presets) return [ t ];
        let r = (t.presets || []).flatMap(C).flatMap(O);
        return [ t, ...r ];
    }
    function G(t) {
        if (t.length === 0) return {};
        let r = [], n = [], i = !1, a = [], s = [], o = [];
        for (let e of t) {
            if (e.pipeline === !1) {
                i = !0;
                break;
            } else e.pipeline?.include && r.push(e.pipeline.include), e.pipeline?.exclude && n.push(e.pipeline.exclude);
            e.filesystem && a.push(e.filesystem), e.inline && s.push(e.inline), 
            e.plain && o.push(e.plain);
        }
        let e = {
            pipeline: i ? !1 : {
                include: M(g(...r)),
                exclude: M(g(...n))
            }
        };
        return a.length && (e.filesystem = M(a.flat())), s.length && (e.inline = M(s.flat())), 
        o.length && (e.plain = M(o.flat())), e;
    }
    function p(e = {}, t = {}) {
        let r = Object.assign({}, t, e), n = x((r.presets || []).flatMap(C).flatMap(O), (e, t) => e.name === t.name), i = [ ...n.filter(e => e.enforce === "pre"), ...n.filter(e => !e.enforce), ...n.filter(e => e.enforce === "post") ], a = [ ...i, r ], s = [ ...a ].reverse(), o = Object.assign({}, A, ...a.map(e => e.layers));
        function l(t) {
            return M(a.flatMap(e => C(e[t] || [])));
        }
        let c = l("extractors"), f = s.find(e => e.extractorDefault !== void 0)?.extractorDefault;
        f === void 0 && (f = S), f && !c.includes(f) && c.unshift(f), c.sort((e, t) => (e.order || 0) - (t.order || 0));
        let u = l("rules"), p = {}, h = u.length, d = u.map((t, r) => {
            if (H(t)) {
                C(t[2]?.prefix || "").forEach(e => {
                    p[e + t[0]] = [ r, t[1], t[2], t ];
                });
                return;
            }
            return [ r, ...t ];
        }).filter(Boolean).reverse(), m = J(a.map(e => e.theme)), y = l("extendTheme");
        for (let e of y) m = e(m) || m;
        let g = {
            templates: M(a.flatMap(e => C(e.autocomplete?.templates))),
            extractors: a.flatMap(e => C(e.autocomplete?.extractors)).sort((e, t) => (e.order || 0) - (t.order || 0)),
            shorthands: K(a.map(e => e.autocomplete?.shorthands || {}))
        }, v = l("separators");
        v.length || (v = [ ":", "-" ]);
        let w = l("content"), b = G(w), $ = {
            mergeSelectors: !0,
            warn: !0,
            sortLayers: e => e,
            ...r,
            blocklist: l("blocklist"),
            presets: i,
            envMode: r.envMode || "build",
            shortcutsLayer: r.shortcutsLayer || "shortcuts",
            layers: o,
            theme: m,
            rulesSize: h,
            rulesDynamic: d,
            rulesStaticMap: p,
            preprocess: l("preprocess"),
            postprocess: l("postprocess"),
            preflights: l("preflights"),
            autocomplete: g,
            variants: l("variants").map(P).sort((e, t) => (e.order || 0) - (t.order || 0)),
            shortcuts: k(l("shortcuts")).reverse(),
            extractors: c,
            safelist: l("safelist"),
            separators: v,
            details: r.details ?? r.envMode === "dev",
            content: b,
            transformers: x(l("transformers"), (e, t) => e.name === t.name)
        };
        for (let e of a) e?.configResolved?.($);
        return $;
    }
    function J(e) {
        return e.map(e => e ? l(e) : {}).reduce((e, t) => o(e, t), {});
    }
    function K(e) {
        return e.reduce((e, r) => {
            let n = {};
            for (let t in r) {
                let e = r[t];
                Array.isArray(e) ? n[t] = `(${e.join("|")})` : n[t] = e;
            }
            return {
                ...e,
                ...n
            };
        }, {});
    }
    function g(...e) {
        return e.flatMap(Q);
    }
    function Q(e) {
        return Array.isArray(e) ? e : e ? [ e ] : [];
    }
    var X = "0.64.1";
    function ee() {
        return {
            events: {},
            emit(e, ...t) {
                (this.events[e] || []).forEach(e => e(...t));
            },
            on(e, t) {
                return (this.events[e] = this.events[e] || []).push(t), () => this.events[e] = (this.events[e] || []).filter(e => e !== t);
            }
        };
    }
    var v = {
        shortcutsNoMerge: "$$symbol-shortcut-no-merge",
        variants: "$$symbol-variants",
        parent: "$$symbol-parent",
        selector: "$$symbol-selector",
        layer: "$$symbol-layer"
    }, te = class {
        constructor(e = {}, t = {}) {
            this.userConfig = e;
            this.defaults = t;
            this.config = p(e, t), this.events.emit("config", this.config);
        }
        version = X;
        _cache = new Map();
        config;
        blocked = new Set();
        parentOrders = new Map();
        events = ee();
        setConfig(e, t) {
            e && (t && (this.defaults = t), this.userConfig = e, this.blocked.clear(), 
            this.parentOrders.clear(), this._cache.clear(), this.config = p(e, this.defaults), 
            this.events.emit("config", this.config));
        }
        async applyExtractors(e, t, r = new Set()) {
            let n = {
                original: e,
                code: e,
                id: t,
                extracted: r,
                envMode: this.config.envMode
            };
            for (let e of this.config.extractors) {
                let t = await e.extract?.(n);
                if (t) if (j(t) && j(r)) for (let e of t) r.setCount(e, r.getCount(e) + t.getCount(e)); else for (let e of t) r.add(e);
            }
            return r;
        }
        makeContext(e, t) {
            let r = {
                rawSelector: e,
                currentSelector: t[1],
                theme: this.config.theme,
                generator: this,
                symbols: v,
                variantHandlers: t[2],
                constructCSS: (...e) => this.constructCustomCSS(r, ...e),
                variantMatch: t
            };
            return r;
        }
        async parseToken(t, e) {
            if (this.blocked.has(t)) return;
            let r = `${t}${e ? ` ${e}` : ""}`;
            if (this._cache.has(r)) return this._cache.get(r);
            let n = t;
            for (let e of this.config.preprocess) n = e(t);
            if (this.isBlocked(n)) {
                this.blocked.add(t), this._cache.set(r, null);
                return;
            }
            let i = await this.matchVariants(t, n);
            if (!i || this.isBlocked(i[1])) {
                this.blocked.add(t), this._cache.set(r, null);
                return;
            }
            let a = this.makeContext(t, [ e || i[0], i[1], i[2], i[3] ]);
            this.config.details && (a.variants = [ ...i[3] ]);
            let s = await this.expandShortcut(a.currentSelector, a), o = s ? await this.stringifyShortcuts(a.variantMatch, a, s[0], s[1]) : (await this.parseUtil(a.variantMatch, a))?.map(e => this.stringifyUtil(e, a)).filter(d);
            if (o?.length) return this._cache.set(r, o), o;
            this._cache.set(r, null);
        }
        async generate(e, t = {}) {
            let {
                id: r,
                scope: o,
                preflights: n = !0,
                safelist: i = !0,
                minify: a = !1,
                extendedInfo: s = !1
            } = t, l = this.config.outputToCssLayers, c = E(e) ? await this.applyExtractors(e, r, s ? new _() : new Set()) : Array.isArray(e) ? new Set(e) : e;
            if (i) {
                let t = {
                    generator: this,
                    theme: this.config.theme
                };
                this.config.safelist.flatMap(e => typeof e == "function" ? e(t) : e).forEach(e => {
                    c.has(e) || c.add(e);
                });
            }
            let f = a ? "" : `\n`, u = new Set([ b ]), p = s ? new Map() : new Set(), h = new Map(), d = {}, m = Array.from(c).map(async e => {
                if (p.has(e)) return;
                let t = await this.parseToken(e);
                if (t != null) {
                    p instanceof Map ? p.set(e, {
                        data: t,
                        count: j(c) ? c.getCount(e) : -1
                    }) : p.add(e);
                    for (let r of t) {
                        let e = r[3] || "", t = r[4]?.layer;
                        h.has(e) || h.set(e, []), h.get(e).push(r), t && u.add(t);
                    }
                }
            });
            await Promise.all(m), await (async () => {
                if (!n) return;
                let r = {
                    generator: this,
                    theme: this.config.theme
                }, t = new Set([]);
                this.config.preflights.forEach(({
                    layer: e = $
                }) => {
                    u.add(e), t.add(e);
                }), d = Object.fromEntries(await Promise.all(Array.from(t).map(async t => {
                    let e = (await Promise.all(this.config.preflights.filter(e => (e.layer || $) === t).map(async e => await e.getCSS(r)))).filter(Boolean).join(f);
                    return [ t, e ];
                })));
            })();
            let y = this.config.sortLayers(Array.from(u).sort((e, t) => (this.config.layers[e] ?? 0) - (this.config.layers[t] ?? 0) || e.localeCompare(t))), g = {}, v = (i = b) => {
                if (g[i]) return g[i];
                let t = Array.from(h).sort((e, t) => (this.parentOrders.get(e[0]) ?? 0) - (this.parentOrders.get(t[0]) ?? 0) || e[0]?.localeCompare(t[0] || "") || 0).map(([ e, t ]) => {
                    let a = t.length, s = t.filter(e => (e[4]?.layer || b) === i).sort((e, t) => e[0] - t[0] || (e[4]?.sort || 0) - (t[4]?.sort || 0) || e[5]?.currentSelector?.localeCompare(t[5]?.currentSelector ?? "") || e[1]?.localeCompare(t[1] || "") || e[2]?.localeCompare(t[2] || "") || 0).map(([ , e, t, , r, , n ]) => [ [ [ (e && ie(e, o)) ?? "", r?.sort ?? 0 ] ], t, !!(n ?? r?.noMerge) ]);
                    if (!s.length) return;
                    let r = s.reverse().map(([ r, n, e ], i) => {
                        if (!e && this.config.mergeSelectors) for (let t = i + 1; t < a; t++) {
                            let e = s[t];
                            if (e && !e[2] && (r && e[0] || r == null && e[0] == null) && e[1] === n) return r && e[0] && e[0].push(...r), 
                            null;
                        }
                        let t = r ? M(r.sort((e, t) => e[1] - t[1] || e[0]?.localeCompare(t[0] || "") || 0).map(e => e[0]).filter(Boolean)) : [];
                        return t.length ? `${t.join(`,${f}`)}{${n}}` : n;
                    }).filter(Boolean).reverse().join(f);
                    if (!e) return r;
                    let n = e.split(" $$ ");
                    return `${n.join("{")}{${f}${r}${f}${"}".repeat(n.length)}`;
                }).filter(Boolean).join(f);
                if (n && (t = [ d[i], t ].filter(Boolean).join(f)), l && t) {
                    let e = typeof l == "object" ? l.cssLayerName?.(i) : void 0;
                    e !== null && (e || (e = i), t = `@layer ${e}{${f}${t}${f}}`);
                }
                let e = a ? "" : `/* layer: ${i} */${f}`;
                return g[i] = t ? e + t : "";
            }, w = (e = y, t) => e.filter(e => !t?.includes(e)).map(e => v(e) || "").filter(Boolean).join(f);
            return {
                get css() {
                    return w();
                },
                layers: y,
                matched: p,
                getLayers: w,
                getLayer: v,
                setLayer: async (e, t) => {
                    let r = await t(v(e));
                    return g[e] = r, r;
                }
            };
        }
        async matchVariants(e, t) {
            let r = new Set(), n = [], i = t || e, a = !0, s = {
                rawSelector: e,
                theme: this.config.theme,
                generator: this
            };
            for (;a; ) {
                a = !1;
                for (let t of this.config.variants) {
                    if (!t.multiPass && r.has(t)) continue;
                    let e = await t.match(i, s);
                    if (e) {
                        if (E(e)) {
                            if (e === i) continue;
                            e = {
                                matcher: e
                            };
                        }
                        i = e.matcher ?? i, n.unshift(e), r.add(t), a = !0;
                        break;
                    }
                }
                if (!a) break;
                if (n.length > 500) throw new Error(`Too many variants applied to "${e}"`);
            }
            return [ e, i, n, r ];
        }
        applyVariants(e, t = e[4], r = e[1]) {
            let n = t.slice().sort((e, t) => (e.order || 0) - (t.order || 0)).reduceRight((n, i) => e => {
                let t = i.body?.(e.entries) || e.entries, r = Array.isArray(i.parent) ? i.parent : [ i.parent, void 0 ];
                return (i.handle ?? se)({
                    ...e,
                    entries: t,
                    selector: i.selector?.(e.selector, t) || e.selector,
                    parent: r[0] || e.parent,
                    parentOrder: r[1] || e.parentOrder,
                    layer: i.layer || e.layer,
                    sort: i.sort || e.sort
                }, n);
            }, e => e)({
                prefix: "",
                selector: ae(r),
                pseudo: "",
                entries: e[2]
            }), {
                parent: i,
                parentOrder: a
            } = n;
            i != null && a != null && this.parentOrders.set(i, a);
            let s = {
                selector: [ n.prefix, n.selector, n.pseudo ].join(""),
                entries: n.entries,
                parent: i,
                layer: n.layer,
                sort: n.sort,
                noMerge: n.noMerge
            };
            for (let e of this.config.postprocess) e(s);
            return s;
        }
        constructCustomCSS(e, t, r) {
            let n = m(t);
            if (E(n)) return n;
            let {
                selector: i,
                entries: a,
                parent: s
            } = this.applyVariants([ 0, r || e.rawSelector, n, void 0, e.variantHandlers ]), o = `${i}{${y(a)}}`;
            return s ? `${s}{${o}}` : o;
        }
        async parseUtil(e, l, c = !1, f) {
            let [ u, p, h ] = E(e) ? await this.matchVariants(e) : e;
            this.config.details && (l.rules = l.rules ?? []);
            let n = this.config.rulesStaticMap[p];
            if (n && n[1] && (c || !n[2]?.internal)) {
                this.config.details && l.rules.push(n[3]);
                let e = n[0], t = m(n[1]), r = n[2];
                return E(t) ? [ [ e, t, r ] ] : [ [ e, u, t, r, h ] ];
            }
            l.variantHandlers = h;
            let {
                rulesDynamic: t
            } = this.config;
            for (let [ i, a, s, o ] of t) {
                if (o?.internal && !c) continue;
                let t = p;
                if (o?.prefix) {
                    let r = C(o.prefix);
                    if (f) {
                        let t = C(f);
                        if (!r.some(e => t.includes(e))) continue;
                    } else {
                        let e = r.find(e => p.startsWith(e));
                        if (e == null) continue;
                        t = p.slice(e.length);
                    }
                }
                let e = t.match(a);
                if (!e) continue;
                let r = await s(e, l);
                if (!r) continue;
                if (this.config.details && l.rules.push([ a, s, o ]), typeof r != "string") if (Symbol.asyncIterator in r) {
                    let t = [];
                    for await (let e of r) e && t.push(e);
                    r = t;
                } else Symbol.iterator in r && !Array.isArray(r) && (r = Array.from(r).filter(d));
                let n = U(r).filter(e => e.length);
                if (n.length) return n.map(t => {
                    if (E(t)) return [ i, t, o ];
                    let r = h;
                    for (let e of t) e[0] === v.variants ? r = [ ...C(e[1]), ...r ] : e[0] === v.parent ? r = [ {
                        parent: e[1]
                    }, ...r ] : e[0] === v.selector ? r = [ {
                        selector: e[1]
                    }, ...r ] : e[0] === v.layer && (r = [ {
                        layer: e[1]
                    }, ...r ]);
                    return [ i, u, t, o, r ];
                });
            }
        }
        stringifyUtil(e, t) {
            if (!e) return;
            if (h(e)) return [ e[0], void 0, e[1], void 0, e[2], this.config.details ? t : void 0, void 0 ];
            let {
                selector: r,
                entries: n,
                parent: i,
                layer: a,
                sort: s,
                noMerge: o
            } = this.applyVariants(e), l = y(n);
            if (!l) return;
            let {
                layer: c,
                sort: f,
                ...u
            } = e[3] ?? {}, p = {
                ...u,
                layer: a ?? c,
                sort: s ?? f
            };
            return [ e[0], r, l, i, p, this.config.details ? t : void 0, o ];
        }
        async expandShortcut(n, i, a = 5) {
            if (a === 0) return;
            let s = this.config.details ? e => {
                i.shortcuts = i.shortcuts ?? [], i.shortcuts.push(e);
            } : I, o, l;
            for (let r of this.config.shortcuts) {
                let t = n;
                if (r[2]?.prefix) {
                    let e = C(r[2].prefix).find(e => n.startsWith(e));
                    if (e == null) continue;
                    t = n.slice(e.length);
                }
                if (W(r)) {
                    if (r[0] === t) {
                        o = o || r[2], l = r[1], s(r);
                        break;
                    }
                } else {
                    let e = t.match(r[0]);
                    if (e && (l = r[1](e, i)), l) {
                        o = o || r[2], s(r);
                        break;
                    }
                }
            }
            if (l && (l = C(l).map(e => E(e) ? q(e.trim()).split(/\s+/g) : e).flat()), 
            !l) {
                let [ t, r ] = E(n) ? await this.matchVariants(n) : n;
                if (t !== r) {
                    let e = await this.expandShortcut(r, i, a - 1);
                    e && (l = e[0].map(e => E(e) ? t.replace(r, e) : e));
                }
            }
            if (l) return [ (await Promise.all(l.map(async e => (E(e) ? (await this.expandShortcut(e, i, a - 1))?.[0] : void 0) || [ e ]))).flat(1).filter(Boolean), o ];
        }
        async stringifyShortcuts(r, c, e, f = {
            layer: this.config.shortcutsLayer
        }) {
            let o = new F(), t = (await Promise.all(M(e).map(async e => {
                let t = E(e) ? await this.parseUtil(e, c, !0, f.prefix) : [ [ Number.POSITIVE_INFINITY, "{inline}", m(e), void 0, [] ] ];
                return !t && this.config.warn && Y(`unmatched utility "${e}" in shortcut "${r[1]}"`), 
                t || [];
            }))).flat(1).filter(Boolean).sort((e, t) => e[0] - t[0]), [ l, , u ] = r, p = [];
            for (let s of t) {
                if (h(s)) {
                    p.push([ s[0], void 0, s[1], void 0, s[2], c, void 0 ]);
                    continue;
                }
                let {
                    selector: e,
                    entries: t,
                    parent: r,
                    sort: n,
                    noMerge: i,
                    layer: a
                } = this.applyVariants(s, [ ...s[4], ...u ], l);
                o.getFallback(a ?? f.layer, new V()).getFallback(e, r, [ [], s[0] ])[0].push([ t, !!(i ?? s[3]?.noMerge), n ?? 0 ]);
            }
            return p.concat(o.flatMap((e, l) => e.map(([ e, a ], s, o) => {
                let r = (e, r, t) => {
                    let n = Math.max(...t.map(e => e[1])), i = t.map(e => e[0]);
                    return (e ? [ i.flat(1) ] : i).map(e => {
                        let t = y(e);
                        if (t) return [ a, s, t, o, {
                            ...f,
                            noMerge: r,
                            sort: n,
                            layer: l
                        }, c, void 0 ];
                    });
                };
                return [ [ e.filter(([ , e ]) => e).map(([ e, , t ]) => [ e, t ]), !0 ], [ e.filter(([ , e ]) => !e).map(([ e, , t ]) => [ e, t ]), !1 ] ].map(([ e, t ]) => [ ...r(!1, t, e.filter(([ e ]) => e.some(e => e[0] === v.shortcutsNoMerge))), ...r(!0, t, e.filter(([ e ]) => e.every(e => e[0] !== v.shortcutsNoMerge))) ]);
            }).flat(2).filter(Boolean)));
        }
        isBlocked(t) {
            return !t || this.config.blocklist.map(e => Array.isArray(e) ? e[0] : e).some(e => typeof e == "function" ? e(t) : E(e) ? e === t : e.test(t));
        }
        getBlocked(r) {
            let e = this.config.blocklist.find(e => {
                let t = Array.isArray(e) ? e[0] : e;
                return typeof t == "function" ? t(r) : E(t) ? t === r : t.test(r);
            });
            return e ? Array.isArray(e) ? e : [ e, void 0 ] : void 0;
        }
    };
    function re(e, t) {
        return new te(e, t);
    }
    var w = /\s\$\$\s+/g;
    function ne(e) {
        return w.test(e);
    }
    function ie(e, t) {
        return ne(e) ? e.replace(w, t ? ` ${t} ` : " ") : t ? `${t} ${e}` : e;
    }
    var L = /^\[(.+?)(~?=)"(.*)"\]$/;
    function ae(e) {
        return L.test(e) ? e.replace(L, (e, t, r, n) => `[${a(t)}${r}"${a(n)}"]`) : `.${a(e)}`;
    }
    function se(e, t) {
        return t(e);
    }
    function oe(e) {
        return e.replace(/-(\w)/g, (e, t) => t ? t.toUpperCase() : "");
    }
    function B(e) {
        return e.charAt(0).toUpperCase() + e.slice(1);
    }
    function N(e) {
        return e.replace(/(?:^|\B)([A-Z])/g, "-$1").toLowerCase();
    }
    var T = [ "Webkit", "Moz", "ms" ];
    function le(i) {
        let a = {};
        function t(r) {
            let e = a[r];
            if (e) return e;
            let n = oe(r);
            if (n !== "filter" && n in i) return a[r] = N(n);
            n = B(n);
            for (let t = 0; t < T.length; t++) {
                let e = `${T[t]}${n}`;
                if (e in i) return a[r] = N(B(e));
            }
            return r;
        }
        return ({
            entries: e
        }) => e.forEach(e => {
            e[0].startsWith("--") || (e[0] = t(e[0]));
        });
    }
    function ce(e) {
        return e.replace(/&amp;/g, "&").replace(/&gt;/g, ">").replace(/&lt;/g, "<");
    }
    function fe(e = {}) {
        if (typeof window > "u") {
            console.warn("@unocss/runtime been used in non-browser environment, skipped.");
            return;
        }
        let t = window, i = window.document, r = () => i.documentElement, n = t.__unocss || {}, a = Object.assign({}, e, n.runtime), s = a.defaults || {}, o = a.cloakAttribute ?? "un-cloak";
        a.autoPrefix && (s.postprocess = C(s.postprocess)).unshift(le(i.createElement("div").style)), 
        a.configResolved?.(n, s);
        let l = re(n, s), c = e => a.inject ? a.inject(e) : r().prepend(e), f = () => a.rootElement ? a.rootElement() : i.body, u = new Map(), p = !0, h = new Set(), d, m, y = [], g = () => new Promise(e => {
            y.push(e), m != null && clearTimeout(m), m = setTimeout(() => b().then(() => {
                let e = y;
                y = [], e.forEach(e => e());
            }), 0);
        });
        function v(e, t = !1) {
            if (e.nodeType !== 1) return;
            let r = e;
            r.hasAttribute(o) && r.removeAttribute(o), t && r.querySelectorAll(`[${o}]`).forEach(e => {
                e.removeAttribute(o);
            });
        }
        function w(e, r) {
            let n = u.get(e);
            if (!n) if (n = i.createElement("style"), n.setAttribute("data-unocss-runtime-layer", e), 
            u.set(e, n), r == null) c(n); else {
                let e = w(r), t = e.parentNode;
                t ? t.insertBefore(n, e.nextSibling) : c(n);
            }
            return n;
        }
        async function b() {
            let e = [ ...h ], r = await l.generate(e);
            return r.layers.reduce((e, t) => (w(t, e).innerHTML = r.getLayer(t) ?? "", 
            t), void 0), e.filter(e => !r.matched.has(e)).forEach(e => h["delete"](e)), 
            {
                ...r,
                getStyleElement: e => u.get(e),
                getStyleElements: () => u
            };
        }
        async function $(e) {
            let t = h.size;
            await l.applyExtractors(e, void 0, h), t !== h.size && await g();
        }
        async function A(e = f()) {
            let t = e && e.outerHTML;
            t && (await $(`${t} ${ce(t)}`), v(r()), v(e, !0));
        }
        let S = new MutationObserver(e => {
            p || e.forEach(async e => {
                if (e.target.nodeType !== 1) return;
                let r = e.target;
                for (let e of u) if (r === e[1]) return;
                if (e.type === "childList") e.addedNodes.forEach(async e => {
                    if (e.nodeType !== 1) return;
                    let t = e;
                    d && !d(t) || (await $(t.outerHTML), v(t));
                }); else {
                    if (d && !d(r)) return;
                    if (e.attributeName !== o) {
                        let e = Array.from(r.attributes).map(e => e.value ? `${e.name}="${e.value}"` : e.name).join(" "), t = `<${r.tagName.toLowerCase()} ${e}>`;
                        await $(t);
                    }
                    v(r);
                }
            });
        }), M = !1;
        function x() {
            if (M) return;
            let e = a.observer?.target ? a.observer.target() : f();
            e && (S.observe(e, {
                childList: !0,
                subtree: !0,
                attributes: !0,
                attributeFilter: a.observer?.attributeFilter
            }), M = !0);
        }
        function _() {
            a.bypassDefined && ue(l.blocked), A(), x();
        }
        function j() {
            i.readyState === "loading" ? t.addEventListener("DOMContentLoaded", _) : _();
        }
        let k = t.__unocss_runtime = t.__unocss_runtime = {
            version: l.version,
            uno: l,
            async extract(e) {
                E(e) || (e.forEach(e => h.add(e)), e = ""), await $(e);
            },
            extractAll: A,
            inspect(e) {
                d = e;
            },
            toggleObserver(e) {
                e === void 0 ? p = !p : p = !!e, !M && !p && j();
            },
            update: b,
            presets: t.__unocss_runtime?.presets ?? {}
        };
        a.ready?.(k) !== !1 && (p = !1, j());
    }
    function ue(n = new Set()) {
        for (let r = 0; r < document.styleSheets.length; r++) {
            let e = document.styleSheets[r], t;
            try {
                if (t = e.cssRules || e.rules, !t) continue;
                Array.from(t).flatMap(e => e.selectorText?.split(/,/g) || []).forEach(e => {
                    e && (e = e.trim(), e.startsWith(".") && (e = e.slice(1)), n.add(e));
                });
            } catch {
                continue;
            }
        }
        return n;
    }
    fe();
})();