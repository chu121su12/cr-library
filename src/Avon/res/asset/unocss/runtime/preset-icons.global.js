"use strict";(()=>{var He=Object.create;var G=Object.defineProperty;var Je=Object.getOwnPropertyDescriptor;var Ze=Object.getOwnPropertyNames;var Xe=Object.getPrototypeOf,Qe=Object.prototype.hasOwnProperty;var P=(e=>typeof require<"u"?require:typeof Proxy<"u"?new Proxy(e,{get:(o,t)=>(typeof require<"u"?require:o)[t]}):e)(function(e){if(typeof require<"u")return require.apply(this,arguments);throw Error('Dynamic require of "'+e+'" is not supported')});var F=(e,o)=>()=>(e&&(o=e(e=0)),o);var N=(e,o)=>()=>(o||e((o={exports:{}}).exports,o),o.exports),et=(e,o)=>{for(var t in o)G(e,t,{get:o[t],enumerable:!0})},de=(e,o,t,n)=>{if(o&&typeof o=="object"||typeof o=="function")for(let r of Ze(o))!Qe.call(e,r)&&r!==t&&G(e,r,{get:()=>o[r],enumerable:!(n=Je(o,r))||n.enumerable});return e};var T=(e,o,t)=>(t=e!=null?He(Xe(e)):{},de(o||!e||!e.__esModule?G(t,"default",{value:e,enumerable:!0}):t,e)),tt=e=>de(G({},"__esModule",{value:!0}),e);var ot,$,ee,ge,V=F(()=>{"use strict";ot=Object.freeze({left:0,top:0,width:16,height:16}),$=Object.freeze({rotate:0,vFlip:!1,hFlip:!1}),ee=Object.freeze({...ot,...$}),ge=Object.freeze({...ee,body:"",hidden:!1})});var rt,q,te=F(()=>{"use strict";V();rt=Object.freeze({width:null,height:null}),q=Object.freeze({...rt,...$})});function he(e,o){let t={};!e.hFlip!=!o.hFlip&&(t.hFlip=!0),!e.vFlip!=!o.vFlip&&(t.vFlip=!0);let n=((e.rotate||0)+(o.rotate||0))%4;return n&&(t.rotate=n),t}var Ce=F(()=>{"use strict"});function oe(e,o){let t=he(e,o);for(let n in ge)n in $?n in e&&!(n in t)&&(t[n]=$[n]):n in o?t[n]=o[n]:n in e&&(t[n]=e[n]);return t}var we=F(()=>{"use strict";V();Ce()});function xe(e,o){let t=e.icons,n=e.aliases||Object.create(null),r=Object.create(null);function i(c){if(t[c])return r[c]=[];if(!(c in r)){r[c]=null;let a=n[c]&&n[c].parent,m=a&&i(a);m&&(r[c]=[a].concat(m))}return r[c]}return(o||Object.keys(t).concat(Object.keys(n))).forEach(i),r}var ye=F(()=>{"use strict"});function be(e,o,t){let n=e.icons,r=e.aliases||Object.create(null),i={};function c(a){i=oe(n[a]||r[a],i)}return c(o),t.forEach(c),oe(e,i)}function Ie(e,o){if(e.icons[o])return be(e,o,[]);let t=xe(e,[o])[o];return t?be(e,o,t):null}var Fe=F(()=>{"use strict";we();ye()});function E(e,o,t){if(o===1)return e;if(t=t||100,typeof e=="number")return Math.ceil(e*o*t)/t;if(typeof e!="string")return e;let n=e.split(nt);if(n===null||!n.length)return e;let r=[],i=n.shift(),c=it.test(i);for(;;){if(c){let a=parseFloat(i);isNaN(a)?r.push(i):r.push(Math.ceil(a*o*t)/t)}else r.push(i);if(i=n.shift(),i===void 0)return r.join("");c=!c}}var nt,it,B=F(()=>{"use strict";nt=/(-?[0-9.]*[0-9]+[0-9.]*)/g,it=/^-?[0-9.]*[0-9]+[0-9.]*$/g});function st(e,o="defs"){let t="",n=e.indexOf("<"+o);for(;n>=0;){let r=e.indexOf(">",n),i=e.indexOf("</"+o);if(r===-1||i===-1)break;let c=e.indexOf(">",i);if(c===-1)break;t+=e.slice(r+1,i).trim(),e=e.slice(0,n).trim()+e.slice(c+1)}return{defs:t,content:e}}function ct(e,o){return e?"<defs>"+e+"</defs>"+o:o}function ve(e,o,t){let n=st(e);return ct(n.defs,o+n.content+t)}var Se=F(()=>{"use strict"});function Oe(e,o){let t={...ee,...e},n={...q,...o},r={left:t.left,top:t.top,width:t.width,height:t.height},i=t.body;[t,n].forEach(f=>{let l=[],y=f.hFlip,b=f.vFlip,C=f.rotate;y?b?C+=2:(l.push("translate("+(r.width+r.left).toString()+" "+(0-r.top).toString()+")"),l.push("scale(-1 1)"),r.top=r.left=0):b&&(l.push("translate("+(0-r.left).toString()+" "+(r.height+r.top).toString()+")"),l.push("scale(1 -1)"),r.top=r.left=0);let g;switch(C<0&&(C-=Math.floor(C/4)*4),C=C%4,C){case 1:g=r.height/2+r.top,l.unshift("rotate(90 "+g.toString()+" "+g.toString()+")");break;case 2:l.unshift("rotate(180 "+(r.width/2+r.left).toString()+" "+(r.height/2+r.top).toString()+")");break;case 3:g=r.width/2+r.left,l.unshift("rotate(-90 "+g.toString()+" "+g.toString()+")");break}C%2===1&&(r.left!==r.top&&(g=r.left,r.left=r.top,r.top=g),r.width!==r.height&&(g=r.width,r.width=r.height,r.height=g)),l.length&&(i=ve(i,'<g transform="'+l.join(" ")+'">',"</g>"))});let c=n.width,a=n.height,m=r.width,h=r.height,s,u;c===null?(u=a===null?"1em":a==="auto"?h:a,s=E(u,m/h)):(s=c==="auto"?m:c,u=a===null?E(s,h/m):a==="auto"?h:a);let p={},x=(f,l)=>{z(l)||(p[f]=l.toString())};x("width",s),x("height",u);let I=[r.left,r.top,m,h];return p.viewBox=I.join(" "),{attributes:p,viewBox:I,body:i}}var z,re=F(()=>{"use strict";V();te();B();Se();z=e=>e==="unset"||e==="undefined"||e==="none"});function Ee(e){return e.replace(/(['"])\s*\n\s*([^>\\/\s])/g,"$1 $2").replace(/(["';{}><])\s*\n\s*/g,"$1").replace(/\s*\n\s*/g," ").replace(/\s+"/g,'"').replace(/="\s+/g,'="').replace(/(\s)+\/>/g,"/>").trim()}var je=F(()=>{"use strict"});function ut(e,o,t){let n=e.slice(0,e.indexOf(">")),r=(i,c)=>{let a=c.exec(n),m=a!=null,h=o[i];return!h&&!z(h)&&(typeof t=="number"?t>0&&(o[i]=E(a?.[1]??"1em",t)):a&&(o[i]=a[1])),m};return[r("width",at),r("height",lt)]}async function W(e,o,t,n,r,i){let{scale:c,addXmlNs:a=!1}=n??{},{additionalProps:m={},iconCustomizer:h}=n?.customizations??{},s=await r?.()??{};await h?.(o,t,s),Object.keys(m).forEach(f=>{let l=m[f];l!=null&&(s[f]=l)}),i?.(s);let[u,p]=ut(e,s,c);a&&(!e.includes("xmlns=")&&!s.xmlns&&(s.xmlns="http://www.w3.org/2000/svg"),!e.includes("xmlns:xlink=")&&e.includes("xlink:")&&!s["xmlns:xlink"]&&(s["xmlns:xlink"]="http://www.w3.org/1999/xlink"));let x=Object.keys(s).map(f=>f==="width"&&u||f==="height"&&p?null:`${f}="${s[f]}"`).filter(f=>f!=null);if(x.length&&(e=e.replace(ie,`<svg ${x.join(" ")} `)),n){let{defaultStyle:f,defaultClass:l}=n;l&&!e.includes("class=")&&(e=e.replace(ie,`<svg class="${l}" `)),f&&!e.includes("style=")&&(e=e.replace(ie,`<svg style="${f}" `))}let I=n?.usedProps;return I&&(Object.keys(m).forEach(f=>{let l=s[f];l!=null&&(I[f]=l)}),typeof s.width<"u"&&s.width!==null&&(I.width=s.width),typeof s.height<"u"&&s.height!==null&&(I.height=s.height)),e}var at,lt,ie,se=F(()=>{"use strict";re();B();at=/\swidth\s*=\s*["']([\w.]+)["']/,lt=/\sheight\s*=\s*["']([\w.]+)["']/,ie=/<svg\s+/});var $e=N((mo,ke)=>{"use strict";var L=1e3,R=L*60,A=R*60,j=A*24,ft=j*7,pt=j*365.25;ke.exports=function(e,o){o=o||{};var t=typeof e;if(t==="string"&&e.length>0)return mt(e);if(t==="number"&&isFinite(e))return o.long?gt(e):dt(e);throw new Error("val is not a non-empty string or a valid number. val="+JSON.stringify(e))};function mt(e){if(e=String(e),!(e.length>100)){var o=/^(-?(?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|weeks?|w|years?|yrs?|y)?$/i.exec(e);if(o){var t=parseFloat(o[1]),n=(o[2]||"ms").toLowerCase();switch(n){case"years":case"year":case"yrs":case"yr":case"y":return t*pt;case"weeks":case"week":case"w":return t*ft;case"days":case"day":case"d":return t*j;case"hours":case"hour":case"hrs":case"hr":case"h":return t*A;case"minutes":case"minute":case"mins":case"min":case"m":return t*R;case"seconds":case"second":case"secs":case"sec":case"s":return t*L;case"milliseconds":case"millisecond":case"msecs":case"msec":case"ms":return t;default:return}}}}function dt(e){var o=Math.abs(e);return o>=j?Math.round(e/j)+"d":o>=A?Math.round(e/A)+"h":o>=R?Math.round(e/R)+"m":o>=L?Math.round(e/L)+"s":e+"ms"}function gt(e){var o=Math.abs(e);return o>=j?K(e,o,j,"day"):o>=A?K(e,o,A,"hour"):o>=R?K(e,o,R,"minute"):o>=L?K(e,o,L,"second"):e+" ms"}function K(e,o,t,n){var r=o>=t*1.5;return Math.round(e/t)+" "+n+(r?"s":"")}});var ce=N((go,Le)=>{"use strict";function ht(e){t.debug=t,t.default=t,t.coerce=m,t.disable=i,t.enable=r,t.enabled=c,t.humanize=$e(),t.destroy=h,Object.keys(e).forEach(s=>{t[s]=e[s]}),t.names=[],t.skips=[],t.formatters={};function o(s){let u=0;for(let p=0;p<s.length;p++)u=(u<<5)-u+s.charCodeAt(p),u|=0;return t.colors[Math.abs(u)%t.colors.length]}t.selectColor=o;function t(s){let u,p=null,x,I;function f(...l){if(!f.enabled)return;let y=f,b=Number(new Date),C=b-(u||b);y.diff=C,y.prev=u,y.curr=b,u=b,l[0]=t.coerce(l[0]),typeof l[0]!="string"&&l.unshift("%O");let g=0;l[0]=l[0].replace(/%([a-zA-Z%])/g,(O,k)=>{if(O==="%%")return"%";g++;let _=t.formatters[k];if(typeof _=="function"){let Ye=l[g];O=_.call(y,Ye),l.splice(g,1),g--}return O}),t.formatArgs.call(y,l),(y.log||t.log).apply(y,l)}return f.namespace=s,f.useColors=t.useColors(),f.color=t.selectColor(s),f.extend=n,f.destroy=t.destroy,Object.defineProperty(f,"enabled",{enumerable:!0,configurable:!1,get:()=>p!==null?p:(x!==t.namespaces&&(x=t.namespaces,I=t.enabled(s)),I),set:l=>{p=l}}),typeof t.init=="function"&&t.init(f),f}function n(s,u){let p=t(this.namespace+(typeof u>"u"?":":u)+s);return p.log=this.log,p}function r(s){t.save(s),t.namespaces=s,t.names=[],t.skips=[];let u,p=(typeof s=="string"?s:"").split(/[\s,]+/),x=p.length;for(u=0;u<x;u++)p[u]&&(s=p[u].replace(/\*/g,".*?"),s[0]==="-"?t.skips.push(new RegExp("^"+s.slice(1)+"$")):t.names.push(new RegExp("^"+s+"$")))}function i(){let s=[...t.names.map(a),...t.skips.map(a).map(u=>"-"+u)].join(",");return t.enable(""),s}function c(s){if(s[s.length-1]==="*")return!0;let u,p;for(u=0,p=t.skips.length;u<p;u++)if(t.skips[u].test(s))return!1;for(u=0,p=t.names.length;u<p;u++)if(t.names[u].test(s))return!0;return!1}function a(s){return s.toString().substring(2,s.toString().length-2).replace(/\.\*\?$/,"*")}function m(s){return s instanceof Error?s.stack||s.message:s}function h(){console.warn("Instance method `debug.destroy()` is deprecated and no longer does anything. It will be removed in the next major version of `debug`.")}return t.enable(t.load()),t}Le.exports=ht});var Re=N((v,Y)=>{"use strict";v.formatArgs=wt;v.save=xt;v.load=yt;v.useColors=Ct;v.storage=bt();v.destroy=(()=>{let e=!1;return()=>{e||(e=!0,console.warn("Instance method `debug.destroy()` is deprecated and no longer does anything. It will be removed in the next major version of `debug`."))}})();v.colors=["#0000CC","#0000FF","#0033CC","#0033FF","#0066CC","#0066FF","#0099CC","#0099FF","#00CC00","#00CC33","#00CC66","#00CC99","#00CCCC","#00CCFF","#3300CC","#3300FF","#3333CC","#3333FF","#3366CC","#3366FF","#3399CC","#3399FF","#33CC00","#33CC33","#33CC66","#33CC99","#33CCCC","#33CCFF","#6600CC","#6600FF","#6633CC","#6633FF","#66CC00","#66CC33","#9900CC","#9900FF","#9933CC","#9933FF","#99CC00","#99CC33","#CC0000","#CC0033","#CC0066","#CC0099","#CC00CC","#CC00FF","#CC3300","#CC3333","#CC3366","#CC3399","#CC33CC","#CC33FF","#CC6600","#CC6633","#CC9900","#CC9933","#CCCC00","#CCCC33","#FF0000","#FF0033","#FF0066","#FF0099","#FF00CC","#FF00FF","#FF3300","#FF3333","#FF3366","#FF3399","#FF33CC","#FF33FF","#FF6600","#FF6633","#FF9900","#FF9933","#FFCC00","#FFCC33"];function Ct(){if(typeof window<"u"&&window.process&&(window.process.type==="renderer"||window.process.__nwjs))return!0;if(typeof navigator<"u"&&navigator.userAgent&&navigator.userAgent.toLowerCase().match(/(edge|trident)\/(\d+)/))return!1;let e;return typeof document<"u"&&document.documentElement&&document.documentElement.style&&document.documentElement.style.WebkitAppearance||typeof window<"u"&&window.console&&(window.console.firebug||window.console.exception&&window.console.table)||typeof navigator<"u"&&navigator.userAgent&&(e=navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/))&&parseInt(e[1],10)>=31||typeof navigator<"u"&&navigator.userAgent&&navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/)}function wt(e){if(e[0]=(this.useColors?"%c":"")+this.namespace+(this.useColors?" %c":" ")+e[0]+(this.useColors?"%c ":" ")+"+"+Y.exports.humanize(this.diff),!this.useColors)return;let o="color: "+this.color;e.splice(1,0,o,"color: inherit");let t=0,n=0;e[0].replace(/%[a-zA-Z%]/g,r=>{r!=="%%"&&(t++,r==="%c"&&(n=t))}),e.splice(n,0,o)}v.log=console.debug||console.log||(()=>{});function xt(e){try{e?v.storage.setItem("debug",e):v.storage.removeItem("debug")}catch{}}function yt(){let e;try{e=v.storage.getItem("debug")}catch{}return!e&&typeof process<"u"&&"env"in process&&(e=process.env.DEBUG),e}function bt(){try{return localStorage}catch{}}Y.exports=ce()(v);var{formatters:It}=Y.exports;It.j=function(e){try{return JSON.stringify(e)}catch(o){return"[UnexpectedJSONParseError]: "+o.message}}});var _e={};et(_e,{createSupportsColor:()=>le,default:()=>Tt});function S(e,o=globalThis.Deno?globalThis.Deno.args:J.default.argv){let t=e.startsWith("-")?"":e.length===1?"-":"--",n=o.indexOf(t+e),r=o.indexOf("--");return n!==-1&&(r===-1||n<r)}function Ft(){if("FORCE_COLOR"in d)return d.FORCE_COLOR==="true"?1:d.FORCE_COLOR==="false"?0:d.FORCE_COLOR.length===0?1:Math.min(Number.parseInt(d.FORCE_COLOR,10),3)}function vt(e){return e===0?!1:{level:e,hasBasic:!0,has256:e>=2,has16m:e>=3}}function St(e,{streamIsTTY:o,sniffFlags:t=!0}={}){let n=Ft();n!==void 0&&(H=n);let r=t?H:n;if(r===0)return 0;if(t){if(S("color=16m")||S("color=full")||S("color=truecolor"))return 3;if(S("color=256"))return 2}if("TF_BUILD"in d&&"AGENT_NAME"in d)return 1;if(e&&!o&&r===void 0)return 0;let i=r||0;if(d.TERM==="dumb")return i;if(J.default.platform==="win32"){let c=Ae.default.release().split(".");return Number(c[0])>=10&&Number(c[2])>=10586?Number(c[2])>=14931?3:2:1}if("CI"in d)return"GITHUB_ACTIONS"in d||"GITEA_ACTIONS"in d?3:["TRAVIS","CIRCLECI","APPVEYOR","GITLAB_CI","BUILDKITE","DRONE"].some(c=>c in d)||d.CI_NAME==="codeship"?1:i;if("TEAMCITY_VERSION"in d)return/^(9\.(0*[1-9]\d*)\.|\d{2,}\.)/.test(d.TEAMCITY_VERSION)?1:0;if(d.COLORTERM==="truecolor"||d.TERM==="xterm-kitty")return 3;if("TERM_PROGRAM"in d){let c=Number.parseInt((d.TERM_PROGRAM_VERSION||"").split(".")[0],10);switch(d.TERM_PROGRAM){case"iTerm.app":return c>=3?3:2;case"Apple_Terminal":return 2}}return/-256(color)?$/i.test(d.TERM)?2:/^screen|^xterm|^vt100|^vt220|^rxvt|color|ansi|cygwin|linux/i.test(d.TERM)||"COLORTERM"in d?1:i}function le(e,o={}){let t=St(e,{streamIsTTY:e&&e.isTTY,...o});return vt(t)}var J,Ae,ae,d,H,Ot,Tt,Pe=F(()=>{"use strict";J=T(P("node:process"),1),Ae=T(P("node:os"),1),ae=T(P("node:tty"),1);({env:d}=J.default);S("no-color")||S("no-colors")||S("color=false")||S("color=never")?H=0:(S("color")||S("colors")||S("color=true")||S("color=always"))&&(H=1);Ot={stdout:le({isTTY:ae.default.isatty(1)}),stderr:le({isTTY:ae.default.isatty(2)})},Tt=Ot});var ze=N((w,X)=>{"use strict";var Et=P("tty"),Z=P("util");w.init=_t;w.log=Lt;w.formatArgs=kt;w.save=Rt;w.load=At;w.useColors=jt;w.destroy=Z.deprecate(()=>{},"Instance method `debug.destroy()` is deprecated and no longer does anything. It will be removed in the next major version of `debug`.");w.colors=[6,2,3,4,5,1];try{let e=(Pe(),tt(_e));e&&(e.stderr||e).level>=2&&(w.colors=[20,21,26,27,32,33,38,39,40,41,42,43,44,45,56,57,62,63,68,69,74,75,76,77,78,79,80,81,92,93,98,99,112,113,128,129,134,135,148,149,160,161,162,163,164,165,166,167,168,169,170,171,172,173,178,179,184,185,196,197,198,199,200,201,202,203,204,205,206,207,208,209,214,215,220,221])}catch{}w.inspectOpts=Object.keys(process.env).filter(e=>/^debug_/i.test(e)).reduce((e,o)=>{let t=o.substring(6).toLowerCase().replace(/_([a-z])/g,(r,i)=>i.toUpperCase()),n=process.env[o];return/^(yes|on|true|enabled)$/i.test(n)?n=!0:/^(no|off|false|disabled)$/i.test(n)?n=!1:n==="null"?n=null:n=Number(n),e[t]=n,e},{});function jt(){return"colors"in w.inspectOpts?!!w.inspectOpts.colors:Et.isatty(process.stderr.fd)}function kt(e){let{namespace:o,useColors:t}=this;if(t){let n=this.color,r="\x1B[3"+(n<8?n:"8;5;"+n),i=`  ${r};1m${o} \x1B[0m`;e[0]=i+e[0].split(`
`).join(`
`+i),e.push(r+"m+"+X.exports.humanize(this.diff)+"\x1B[0m")}else e[0]=$t()+o+" "+e[0]}function $t(){return w.inspectOpts.hideDate?"":new Date().toISOString()+" "}function Lt(...e){return process.stderr.write(Z.formatWithOptions(w.inspectOpts,...e)+`
`)}function Rt(e){e?process.env.DEBUG=e:delete process.env.DEBUG}function At(){return process.env.DEBUG}function _t(e){e.inspectOpts={};let o=Object.keys(w.inspectOpts);for(let t=0;t<o.length;t++)e.inspectOpts[o[t]]=w.inspectOpts[o[t]]}X.exports=ce()(w);var{formatters:Ne}=X.exports;Ne.o=function(e){return this.inspectOpts.colors=this.useColors,Z.inspect(e,this.inspectOpts).split(`
`).map(o=>o.trim()).join(" ")};Ne.O=function(e){return this.inspectOpts.colors=this.useColors,Z.inspect(e,this.inspectOpts)}});var D=N((ho,ue)=>{"use strict";typeof process>"u"||process.type==="renderer"||process.browser===!0||process.__nwjs?ue.exports=Re():ue.exports=ze()});async function fe(e,o,t,n){let r;Pt(`${o}:${t}`);try{if(typeof e=="function")r=await e(t);else{let i=e[t];r=typeof i=="function"?await i():i}}catch(i){console.warn(`Failed to load custom icon "${t}" in "${o}":`,i);return}if(r){let i=r.indexOf("<svg");i>0&&(r=r.slice(i));let{transform:c}=n?.customizations??{};return r=typeof c=="function"?await c(r,o,t):r,r.startsWith("<svg")?await W(n?.customizations?.trimCustomSvg===!0?Ee(r):r,o,t,n,void 0):(console.warn(`Custom icon "${t}" in "${o}" is not a valid SVG`),r)}}var De,Pt,Me=F(()=>{"use strict";De=T(D(),1);se();je();Pt=(0,De.default)("@iconify-loader:custom")});async function Q(e,o,t,n){let r,{customize:i}=n?.customizations??{};for(let c of t)if(r=Ie(e,c),r){Nt(`${o}:${c}`);let a={...q};typeof i=="function"&&(r=Object.assign({},r),a=i(a,r,`${o}:${c}`)??a);let{attributes:{width:m,height:h,...s},body:u}=Oe(r,a),p=n?.scale;return await W(`<svg >${u}</svg>`,o,c,n,()=>({...s}),x=>{let I=(f,l)=>{let y=x[f],b;if(!z(y)){if(y)return;typeof p=="number"?p&&(b=E(l??"1em",p)):b=l}b?x[f]=b:delete x[f]};I("width",m),I("height",h)})}}var Ue,Nt,pe=F(()=>{"use strict";re();Fe();B();se();Ue=T(D(),1);te();Nt=(0,Ue.default)("@iconify-loader:icon")});var Eo,M,me=F(()=>{"use strict";Me();pe();Eo=T(D(),1),M=async(e,o,t)=>{let n=t?.customCollections?.[e];if(n)if(typeof n=="function"){let r;try{r=await n(o)}catch(i){console.warn(`Failed to load custom icon "${o}" in "${e}":`,i);return}if(r){if(typeof r=="string")return await fe(()=>r,e,o,t);if("icons"in r){let i=[o,o.replace(/([a-z])([A-Z])/g,"$1-$2").toLowerCase(),o.replace(/([a-z])(\d+)/g,"$1-$2")];return await Q(r,e,i,t)}}}else return await fe(n,e,o,t)}});function Te(e){return e.replace(/"/g,"'").replace(/%/g,"%25").replace(/#/g,"%23").replace(/</g,"%3C").replace(/>/g,"%3E").replace(/\s+/g," ")}function ne(e){let o=e.startsWith("<svg>")?e.replace("<svg>","<svg >"):e;return!o.includes(" xmlns:xlink=")&&o.includes(" xlink:")&&(o=o.replace("<svg ",'<svg xmlns:xlink="http://www.w3.org/1999/xlink" ')),o.includes(" xmlns=")||(o=o.replace("<svg ",'<svg xmlns="http://www.w3.org/2000/svg" ')),Te(o)}me();var ko=T(D(),1);var Ge=new Set;function Ve(e){Ge.has(e)||(console.warn("[unocss]",e),Ge.add(e))}me();pe();var qe=["academicons","akar-icons","ant-design","arcticons","basil","bi","bitcoin-icons","bpmn","brandico","bx","bxl","bxs","bytesize","carbon","catppuccin","cbi","charm","ci","cib","cif","cil","circle-flags","circum","clarity","codicon","covid","cryptocurrency-color","cryptocurrency","cuida","dashicons","devicon-line","devicon-original","devicon-plain","devicon","duo-icons","ei","el","emblemicons","emojione-monotone","emojione-v1","emojione","entypo-social","entypo","eos-icons","ep","et","eva","f7","fa-brands","fa-regular","fa-solid","fa","fa6-brands","fa6-regular","fa6-solid","fad","fe","feather","file-icons","flag","flagpack","flat-color-icons","flat-ui","flowbite","fluent-color","fluent-emoji-flat","fluent-emoji-high-contrast","fluent-emoji","fluent-mdl2","fluent","fontelico","fontisto","formkit","foundation","fxemoji","gala","game-icons","geo","gg","gis","gravity-ui","gridicons","grommet-icons","guidance","healthicons","heroicons-outline","heroicons-solid","heroicons","hugeicons","humbleicons","ic","icomoon-free","icon-park-outline","icon-park-solid","icon-park-twotone","icon-park","iconamoon","iconoir","icons8","il","ion","iwwa","jam","la","lets-icons","line-md","lineicons","logos","ls","lsicon","lucide-lab","lucide","mage","majesticons","maki","map","marketeq","material-symbols-light","material-symbols","mdi-light","mdi","medical-icon","memory","meteocons","mi","mingcute","mono-icons","mynaui","nimbus","nonicons","noto-v1","noto","octicon","oi","ooui","openmoji","oui","pajamas","pepicons-pencil","pepicons-pop","pepicons-print","pepicons","ph","pixelarticons","prime","proicons","ps","quill","radix-icons","raphael","ri","rivet-icons","si-glyph","si","simple-icons","simple-line-icons","skill-icons","solar","stash","streamline-emojis","streamline","subway","svg-spinners","system-uicons","tabler","tdesign","teenyicons","token-branded","token","topcoat","twemoji","typcn","uil","uim","uis","uit","uiw","unjs","vaadin","vs","vscode-icons","websymbol","weui","whh","wi","wpf","zmdi","zondicons"];var Dt=3;function We(e){return(o={})=>{let{scale:t=1,mode:n="auto",prefix:r="i-",warn:i=!1,collections:c,extraProperties:a={},customizations:m={},autoInstall:h=!1,collectionsNodeResolvePath:s,layer:u="icons",unit:p,processor:x}=o,I=Mt(),f={addXmlNs:!0,scale:t,customCollections:c,autoInstall:h,cwd:s,warn:void 0,customizations:{...m,additionalProps:{...a},trimCustomSvg:!0,async iconCustomizer(y,b,C){await m.iconCustomizer?.(y,b,C),p&&(C.width||(C.width=`${t}${p}`),C.height||(C.height=`${t}${p}`))}}},l;return{name:"@unocss/preset-icons",enforce:"pre",options:o,layers:{icons:-30},api:{encodeSvgForCss:ne,parseIconWithLoader:Be},rules:[[/^([\w:-]+)(?:\?(mask|bg|auto))?$/,async y=>{let[b,C,g=n]=y;l=l||await e(o);let U={},O=await Be(C,l,{...f,usedProps:U});if(!O){i&&!I.isESLint&&Ve(`failed to load icon "${b}"`);return}let k,_=`url("data:image/svg+xml;utf8,${ne(O.svg)}")`;return g==="auto"&&(g=O.svg.includes("currentColor")?"mask":"bg"),g==="mask"?k={"--un-icon":_,"-webkit-mask":"var(--un-icon) no-repeat",mask:"var(--un-icon) no-repeat","-webkit-mask-size":"100% 100%","mask-size":"100% 100%","background-color":"currentColor",color:"inherit",...U}:k={background:`${_} no-repeat`,"background-size":"100% 100%","background-color":"transparent",...U},x?.(k,{...O,icon:O.name,mode:g}),k},{layer:u,prefix:r}]]}}}function Ke(e,o){let t=new Map;function n(r){if(qe.includes(r))return t.has(r)||t.set(r,e(`${o}@iconify-json/${r}/icons.json`)),t.get(r)}return async(r,i,c)=>{let a=await M(r,i,c);if(a)return a;let m=await n(r);if(m){let h=[i,i.replace(/([a-z])([A-Z])/g,"$1-$2").toLowerCase(),i.replace(/([a-z])(\d+)/g,"$1-$2")];a=await Q(m,r,h,c)}return a}}function Mt(){let e=typeof process<"u"&&process.stdout&&!process.versions.deno,o=e&&!!process.env.VSCODE_CWD,t=e&&!!process.env.ESLINT;return{isNode:e,isVSCode:o,isESLint:t}}async function Be(e,o,t={}){let n="",r="",i;if(e.includes(":"))[n,r]=e.split(":"),i=await o(n,r,t);else{let c=e.split(/-/g);for(let a=Dt;a>=1&&(n=c.slice(0,a).join("-"),r=c.slice(a).join("-"),i=await o(n,r,t),!i);a--);}if(i)return{collection:n,name:r,svg:i}}window.__unocss_runtime=window.__unocss_runtime??{};window.__unocss_runtime.presets=Object.assign(window.__unocss_runtime?.presets??{},(()=>{let e=We(async o=>{let t=o?.customFetch??(r=>fetch(r).then(i=>i.json())),n=o?.cdn;return n?Ke(t,n):M});return{presetIcons:o=>e(o)}})());})();
