"use strict";

(() => {
    var t = /^\[(.+?)~?="(.*)"\]$/;
    var r = /[\w\u00A0-\uFFFF%-?]/;
    function f(e) {
        return e.match(t);
    }
    function a(e = "") {
        return r.test(e);
    }
    var $ = /^(?!.*\[[^:]+:.+\]$)((?:.+:)?!?)(.*)$/;
    function i(e = {}) {
        let a = e.prefix ?? "un-", u = e.prefixedOnly ?? !1, c = e.trueToNonValued ?? !1, o;
        return {
            name: "attributify",
            match(e, {
                generator: t
            }) {
                let r = f(e);
                if (!r) return;
                let n = r[1];
                if (n.startsWith(a)) n = n.slice(a.length); else if (u) return;
                let i = r[2], [ , l = "", s = i ] = i.match($) || [];
                if (s === "~" || c && s === "true" || !s) return `${l}${n}`;
                if (o == null) {
                    let e = t?.config?.separators?.join("|");
                    e ? o = new RegExp(`^(.*\\](?:${e}))(\\[[^\\]]+?\\])$`) : o = !1;
                }
                if (o) {
                    let [ , e, t ] = i.match(o) || [];
                    if (t) return `${e}${l}${n}-${t}`;
                }
                return `${l}${n}-${s}`;
            }
        };
    }
    var b = /(<\w[\w:.$-]*\s)((?:'[^>']*'|"[^>"]*"|`[^>`]*`|\{[^>}]*\}|[^>]*?)*)/g, A = /(\?|(?!\d|-{2}|-\d)[\w\u00A0-\uFFFF-:%]+)(?:=("[^"]*|'[^']*))?/g, w = /[\s'"`;>]+/;
    function l(m) {
        return {
            name: "attributify",
            extract: ({
                content: e,
                cursor: l
            }) => {
                let t = e.matchAll(b), i, s = 0;
                for (let n of t) {
                    let [ , e, t ] = n, r = n.index + e.length;
                    if (l > r && l <= r + t.length) {
                        s = r, i = t;
                        break;
                    }
                }
                if (!i) return null;
                let r = i.matchAll(A), a = 0, u, c;
                for (let i of r) {
                    let [ e, t, r ] = i, n = s + i.index;
                    if (l > n && l <= n + e.length) {
                        a = n, u = t, c = r?.slice(1);
                        break;
                    }
                }
                if (!u || u === "class" || u === "className" || u === ":class") return null;
                let n = !!m?.prefix && u.startsWith(m.prefix);
                if (m?.prefixedOnly && !n) return null;
                let o = n ? u.slice(m.prefix.length) : u;
                if (c === void 0) return {
                    extracted: o,
                    resolveReplacement(e) {
                        let t = n ? m.prefix.length : 0;
                        return {
                            start: a + t,
                            end: a + u.length,
                            replacement: e
                        };
                    }
                };
                let f = a + u.length + 2, p = w.exec(c), d = 0, h;
                for (;p; ) {
                    let [ e ] = p;
                    if (l > f + d && l <= f + d + p.index) {
                        h = c.slice(d, d + p.index);
                        break;
                    }
                    d += p.index + e.length, p = w.exec(c.slice(d));
                }
                h === void 0 && (h = c.slice(d));
                let [ , x = "", g ] = h.match($) || [];
                return {
                    extracted: `${x}${o}-${g}`,
                    transformSuggestions(e) {
                        return e.filter(e => e.startsWith(`${x}${o}-`)).map(e => x + e.slice(x.length + o.length + 1));
                    },
                    resolveReplacement(e) {
                        return {
                            start: d + f,
                            end: d + f + h.length,
                            replacement: x + e.slice(x.length + o.length + 1)
                        };
                    }
                };
            }
        };
    }
    var u = [ "v-bind:", ":" ], c = /[\s'"`;]+/g, o = /<[^>\s]*\s((?:'[^']*'|"[^"]*"|`[^`]*`|\{[^}]*\}|=>|[^>]*?)*)/g, p = /(\?|(?!\d|-{2}|-\d)[\w\u00A0-\uFFFF:!%.~<-]+)=?(?:"([^"]*)"|'([^']*)'|\{([^}]*)\})?/g, d = [ "placeholder", "fill", "opacity", "stroke-opacity" ];
    function s(n) {
        let i = n?.ignoreAttributes ?? d, l = n?.nonValuedAttribute ?? !0, s = n?.trueToNonValued ?? !1;
        return {
            name: "@unocss/preset-attributify/extractor",
            extract({
                code: e
            }) {
                return Array.from(e.matchAll(o)).flatMap(e => Array.from((e[1] || "").matchAll(p))).flatMap(([ , t, ...e ]) => {
                    let r = e.filter(Boolean).join("");
                    if (i.includes(t)) return [];
                    for (let e of u) if (t.startsWith(e)) {
                        t = t.slice(e.length);
                        break;
                    }
                    if (!r) {
                        if (a(t) && l !== !1) {
                            let e = [ `[${t}=""]` ];
                            return s && e.push(`[${t}="true"]`), e;
                        }
                        return [];
                    }
                    return [ "class", "className" ].includes(t) ? r.split(c).filter(a) : o.test(r) ? (o.lastIndex = 0, 
                    this.extract({
                        code: r
                    })) : n?.prefixedOnly && n.prefix && !t.startsWith(n.prefix) ? [] : r.split(c).filter(e => !!e && e !== ":").map(e => `[${t}~="${e}"]`);
                });
            }
        };
    }
    var e = (e = {}) => {
        e.strict = e.strict ?? !1, e.prefix = e.prefix ?? "un-", e.prefixedOnly = e.prefixedOnly ?? !1, 
        e.nonValuedAttribute = e.nonValuedAttribute ?? !0, e.ignoreAttributes = e.ignoreAttributes ?? d;
        let t = [ i(e) ], r = [ s(e) ], n = [ l(e) ];
        return {
            name: "@unocss/preset-attributify",
            enforce: "post",
            variants: t,
            extractors: r,
            options: e,
            autocomplete: {
                extractors: n
            },
            extractorDefault: e.strict ? !1 : void 0
        };
    }, n = e;
    window.__unocss_runtime = window.__unocss_runtime ?? {};
    window.__unocss_runtime.presets = Object.assign(window.__unocss_runtime?.presets ?? {}, {
        presetAttributify: e => n(e)
    });
})();