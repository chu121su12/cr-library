"use strict";

(() => {
    var O = Object.defineProperty;
    var T = (e, t) => {
        for (var r in t) O(e, r, {
            get: t[r],
            enumerable: !0
        });
    };
    var R = /[\\:]?[\s'"`;{}]+/g;
    function c(e = []) {
        return Array.isArray(e) ? e : [ e ];
    }
    function D(e) {
        return typeof e == "string";
    }
    function a(e) {
        return e.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
    }
    function x(e) {
        let t = e.length, r = -1, n, i = "", o = e.charCodeAt(0);
        for (;++r < t; ) {
            if (n = e.charCodeAt(r), n === 0) {
                i += "\ufffd";
                continue;
            }
            if (n === 37) {
                i += "\\%";
                continue;
            }
            if (n === 44) {
                i += "\\,";
                continue;
            }
            if (n >= 1 && n <= 31 || n === 127 || r === 0 && n >= 48 && n <= 57 || r === 1 && n >= 48 && n <= 57 && o === 45) {
                i += `\\${n.toString(16)} `;
                continue;
            }
            if (r === 0 && t === 1 && n === 45) {
                i += `\\${e.charAt(r)}`;
                continue;
            }
            if (n >= 128 || n === 45 || n === 95 || n >= 48 && n <= 57 || n >= 65 && n <= 90 || n >= 97 && n <= 122) {
                i += e.charAt(r);
                continue;
            }
            i += `\\${e.charAt(r)}`;
        }
        return i;
    }
    var B = /[\w\u00A0-\uFFFF%-?]/;
    function X(e = "") {
        return B.test(e);
    }
    function Y(i) {
        return i.filter(([ t, r ], n) => {
            if (t.startsWith("$$")) return !1;
            for (let e = n - 1; e >= 0; e--) if (i[e][0] === t && i[e][1] === r) return !1;
            return !0;
        });
    }
    function P(e) {
        return e == null ? "" : Y(e).map(([ e, t ]) => t != null && typeof t != "function" ? `${e}:${t};` : void 0).filter(Boolean).join("");
    }
    function I(e) {
        let t, r, n = 2166136261;
        for (t = 0, r = e.length; t < r; t++) n ^= e.charCodeAt(t), n += (n << 1) + (n << 4) + (n << 7) + (n << 8) + (n << 24);
        return `00000${(n >>> 0).toString(36)}`.slice(-6);
    }
    function q(n, i, e, o) {
        for (let r of Array.from(n.matchAll(e))) if (r != null) {
            let e = r[0], t = `${o}${I(e)}`;
            i.set(t, e), n = n.replace(e, t);
        }
        return n;
    }
    function M(r, n) {
        for (let [ e, t ] of n.entries()) r = r.replaceAll(e, t);
        return r;
    }
    var Z = /\/\/#\s*sourceMappingURL=.*\n?/g;
    function H(e) {
        return e.includes("sourceMappingURL=") ? e.replace(Z, "") : e;
    }
    var G = /(?:[\w&:[\]-]|\[\S{1,64}=\S{1,64}\]){1,64}\[\\?['"]?\S{1,64}?['"]\]\]?[\w:-]{0,64}/g, J = /\[(\\\W|[\w-]){1,64}:[^\s:]{0,64}?("\S{1,64}?"|'\S{1,64}?'|`\S{1,64}?`|[^\s:]{1,64}?)[^\s:]{0,64}?\)?\]/g, K = /^\[(?:\\\W|[\w-]){1,64}:['"]?\S{1,64}?['"]?\]$/;
    function V(t) {
        let r = [];
        for (let e of t.matchAll(J)) e.index !== 0 && !/^[\s'"`]/.test(t[e.index - 1] ?? "") || r.push(e[0]);
        for (let e of t.matchAll(G)) r.push(e[0]);
        let n = new Map(), i = "@unocss-skip-arbitrary-brackets";
        return t = q(t, n, /-\[(?!&.+?;)[^\]]*\]/g, i), t && t.split(R).forEach(e => {
            e.includes(i) && (e = M(e, n)), X(e) && !K.test(e) && r.push(e);
        }), r;
    }
    function Q() {
        return {
            name: "@unocss/extractor-arbitrary-variants",
            order: 0,
            extract({
                code: e
            }) {
                return V(H(e));
            }
        };
    }
    var ee = [ {
        layer: "preflights",
        getCSS(e) {
            if (e.theme.preflightBase) {
                let t = P(Object.entries(e.theme.preflightBase));
                return c(e.theme.preflightRoot ?? [ "*,::before,::after", "::backdrop" ]).map(e => `${e}{${t}}`).join("");
            }
        }
    } ];
    function te(t, r, n) {
        if (t === "") return;
        let i = t.length, o = 0, a = !1, s = 0;
        for (let e = 0; e < i; e++) switch (t[e]) {
          case r:
            a || (a = !0, s = e), o++;
            break;

          case n:
            if (--o, o < 0) return;
            if (o === 0) return [ t.slice(s, e + 1), t.slice(e + 1), t.slice(0, s) ];
            break;
        }
    }
    function l(n, e, t, i) {
        if (n === "" || (D(i) && (i = [ i ]), i.length === 0)) return;
        let o = n.length, a = 0;
        for (let r = 0; r < o; r++) switch (n[r]) {
          case e:
            a++;
            break;

          case t:
            if (--a < 0) return;
            break;

          default:
            for (let t of i) {
                let e = t.length;
                if (e && t === n.slice(r, r + e) && a === 0) return r === 0 || r === o - e ? void 0 : [ n.slice(0, r), n.slice(r + e) ];
            }
        }
        return [ n, "" ];
    }
    function re(n, i, o) {
        o = o ?? 10;
        let a = [], s = 0;
        for (;n !== ""; ) {
            if (++s > o) return;
            let e = l(n, "(", ")", i);
            if (!e) return;
            let [ t, r ] = e;
            a.push(t), n = r;
        }
        if (a.length > 0) return a;
    }
    var ne = [ "hsl", "hsla", "hwb", "lab", "lch", "oklab", "oklch", "rgb", "rgba" ], ie = [ "%alpha", "<alpha-value>" ], oe = new RegExp(ie.map(e => a(e)).join("|"));
    function u(e = "") {
        let t = ae(e);
        if (t == null || t === !1) return;
        let {
            type: r,
            components: n,
            alpha: i
        } = t, o = r.toLowerCase();
        if (n.length !== 0 && !(ne.includes(o) && ![ 1, 3 ].includes(n.length))) return {
            type: o,
            components: n.map(e => typeof e == "string" ? e.trim() : e),
            alpha: typeof i == "string" ? i.trim() : i
        };
    }
    function p(e) {
        let t = e.alpha ?? 1;
        return typeof t == "string" && ie.includes(t) ? 1 : t;
    }
    function m(e, t) {
        if (typeof e == "string") return e.replace(oe, `${t ?? 1}`);
        let {
            components: r
        } = e, {
            alpha: n,
            type: i
        } = e;
        return n = t ?? n, i = i.toLowerCase(), [ "hsla", "rgba" ].includes(i) ? `${i}(${r.join(", ")}${n == null ? "" : `, ${n}`})` : (n = n == null ? "" : ` / ${n}`, 
        ne.includes(i) ? `${i}(${r.join(" ")}${n})` : `color(${i} ${r.join(" ")}${n})`);
    }
    function ae(e) {
        if (!e) return;
        let t = se(e);
        if (t != null || (t = le(e), t != null) || (t = ce(e), t != null) || (t = de(e), 
        t != null) || (t = ue(e), t != null)) return t;
    }
    function se(e) {
        let [ , r ] = e.match(/^#([\da-f]+)$/i) || [];
        if (r) switch (r.length) {
          case 3:
          case 4:
            let e = Array.from(r, e => Number.parseInt(e, 16)).map(e => e << 4 | e);
            return {
                type: "rgb",
                components: e.slice(0, 3),
                alpha: r.length === 3 ? void 0 : Math.round(e[3] / 255 * 100) / 100
            };

          case 6:
          case 8:
            let t = Number.parseInt(r, 16);
            return {
                type: "rgb",
                components: r.length === 6 ? [ t >> 16 & 255, t >> 8 & 255, t & 255 ] : [ t >> 24 & 255, t >> 16 & 255, t >> 8 & 255 ],
                alpha: r.length === 6 ? void 0 : Math.round((t & 255) / 255 * 100) / 100
            };
        }
    }
    function le(e) {
        let t = {
            rebeccapurple: [ 102, 51, 153, 1 ]
        }[e];
        if (t != null) return {
            type: "rgb",
            components: t.slice(0, 3),
            alpha: t[3]
        };
    }
    function ce(e) {
        let t = e.match(/^(rgb|rgba|hsl|hsla)\((.+)\)$/i);
        if (!t) return;
        let [ , r, n ] = t, i = re(n, ",", 5);
        if (i) {
            if ([ 3, 4 ].includes(i.length)) return {
                type: r,
                components: i.slice(0, 3),
                alpha: i[3]
            };
            if (i.length !== 1) return !1;
        }
    }
    var fe = new RegExp(`^(${ne.join("|")})\\((.+)\\)$`, "i");
    function de(e) {
        let t = e.match(fe);
        if (!t) return;
        let [ , r, n ] = t, i = pe(`${r} ${n}`);
        if (i) {
            let {
                alpha: e,
                components: [ t, ...r ]
            } = i;
            return {
                type: t,
                components: r,
                alpha: e
            };
        }
    }
    function ue(e) {
        let t = e.match(/^color\((.+)\)$/);
        if (!t) return;
        let n = pe(t[1]);
        if (n) {
            let {
                alpha: e,
                components: [ t, ...r ]
            } = n;
            return {
                type: t,
                components: r,
                alpha: e
            };
        }
    }
    function pe(e) {
        let t = re(e, " ");
        if (!t) return;
        let r = t.length;
        if (t[r - 2] === "/") return {
            components: t.slice(0, r - 2),
            alpha: t[r - 1]
        };
        if (t[r - 2] != null && (t[r - 2].endsWith("/") || t[r - 1].startsWith("/"))) {
            let e = t.splice(r - 2);
            t.push(e.join(" ")), --r;
        }
        let n = re(t[r - 1], "/", 2);
        if (!n) return;
        if (n.length === 1 || n[n.length - 1] === "") return {
            components: t
        };
        let i = n.pop();
        return t[r - 1] = n.join("/"), {
            components: t,
            alpha: i
        };
    }
    function me(n) {
        let t = function(r) {
            let e = this.__options?.sequence || [];
            this.__options.sequence = [];
            for (let t of e) {
                let e = n[t](r);
                if (e != null) return e;
            }
        };
        function r(e, t) {
            return e.__options || (e.__options = {
                sequence: []
            }), e.__options.sequence.push(t), e;
        }
        for (let e of Object.keys(n)) Object.defineProperty(t, e, {
            enumerable: !0,
            configurable: !0,
            get() {
                return r(this, e);
            }
        });
        return t;
    }
    var ge = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", he = new Uint8Array(64), be = new Uint8Array(128);
    for (let t = 0; t < ge.length; t++) {
        let e = ge.charCodeAt(t);
        he[t] = e, be[e] = t;
    }
    function xe(t, e, r) {
        let n = e - r;
        n = n < 0 ? -n << 1 | 1 : n << 1;
        do {
            let e = n & 31;
            n >>>= 5, n > 0 && (e |= 32), t.write(he[e]);
        } while (n > 0);
        return e;
    }
    var ve = 1024 * 16, ye = typeof TextDecoder < "u" ? new TextDecoder() : typeof Buffer < "u" ? {
        decode(e) {
            return Buffer.from(e.buffer, e.byteOffset, e.byteLength).toString();
        }
    } : {
        decode(t) {
            let r = "";
            for (let e = 0; e < t.length; e++) r += String.fromCharCode(t[e]);
            return r;
        }
    }, $e = class {
        constructor() {
            this.pos = 0, this.out = "", this.buffer = new Uint8Array(ve);
        }
        write(e) {
            let {
                buffer: t
            } = this;
            t[this.pos++] = e, this.pos === ve && (this.out += ye.decode(t), this.pos = 0);
        }
        flush() {
            let {
                buffer: e,
                out: t,
                pos: r
            } = this;
            return r > 0 ? t + ye.decode(e.subarray(0, r)) : t;
        }
    };
    function ke(t) {
        let i = new $e(), o = 0, a = 0, s = 0, l = 0;
        for (let e = 0; e < t.length; e++) {
            let r = t[e];
            if (e > 0 && i.write(59), r.length === 0) continue;
            let n = 0;
            for (let t = 0; t < r.length; t++) {
                let e = r[t];
                t > 0 && i.write(44), n = xe(i, e[0], n), e.length !== 1 && (o = xe(i, e[1], o), 
                a = xe(i, e[2], a), s = xe(i, e[3], s), e.length !== 4 && (l = xe(i, e[4], l)));
            }
        }
        return i.flush();
    }
    var we = class U {
        constructor(e) {
            this.bits = e instanceof U ? e.bits.slice() : [];
        }
        add(e) {
            this.bits[e >> 5] |= 1 << (e & 31);
        }
        has(e) {
            return !!(this.bits[e >> 5] & 1 << (e & 31));
        }
    }, ze = class U {
        constructor(e, t, r) {
            this.start = e, this.end = t, this.original = r, this.intro = "", this.outro = "", 
            this.content = r, this.storeName = !1, this.edited = !1, this.previous = null, 
            this.next = null;
        }
        appendLeft(e) {
            this.outro += e;
        }
        appendRight(e) {
            this.intro = this.intro + e;
        }
        clone() {
            let e = new U(this.start, this.end, this.original);
            return e.intro = this.intro, e.outro = this.outro, e.content = this.content, 
            e.storeName = this.storeName, e.edited = this.edited, e;
        }
        contains(e) {
            return this.start < e && e < this.end;
        }
        eachNext(e) {
            let t = this;
            for (;t; ) e(t), t = t.next;
        }
        eachPrevious(e) {
            let t = this;
            for (;t; ) e(t), t = t.previous;
        }
        edit(e, t, r) {
            return this.content = e, r || (this.intro = "", this.outro = ""), this.storeName = t, 
            this.edited = !0, this;
        }
        prependLeft(e) {
            this.outro = e + this.outro;
        }
        prependRight(e) {
            this.intro = e + this.intro;
        }
        reset() {
            this.intro = "", this.outro = "", this.edited && (this.content = this.original, 
            this.storeName = !1, this.edited = !1);
        }
        split(e) {
            let t = e - this.start, r = this.original.slice(0, t), n = this.original.slice(t);
            this.original = r;
            let i = new U(e, this.end, n);
            return i.outro = this.outro, this.outro = "", this.end = e, this.edited ? (i.edit("", !1), 
            this.content = "") : this.content = r, i.next = this.next, i.next && (i.next.previous = i), 
            i.previous = this, this.next = i, i;
        }
        toString() {
            return this.intro + this.content + this.outro;
        }
        trimEnd(e) {
            if (this.outro = this.outro.replace(e, ""), this.outro.length) return !0;
            let t = this.content.replace(e, "");
            if (t.length) return t !== this.content && (this.split(this.start + t.length).edit("", void 0, !0), 
            this.edited && this.edit(t, this.storeName, !0)), !0;
            if (this.edit("", void 0, !0), this.intro = this.intro.replace(e, ""), 
            this.intro.length) return !0;
        }
        trimStart(e) {
            if (this.intro = this.intro.replace(e, ""), this.intro.length) return !0;
            let t = this.content.replace(e, "");
            if (t.length) {
                if (t !== this.content) {
                    let e = this.split(this.end - t.length);
                    this.edited && e.edit(t, this.storeName, !0), this.edit("", void 0, !0);
                }
                return !0;
            } else if (this.edit("", void 0, !0), this.outro = this.outro.replace(e, ""), 
            this.outro.length) return !0;
        }
    };
    function Ee() {
        return typeof globalThis < "u" && typeof globalThis.btoa == "function" ? e => globalThis.btoa(unescape(encodeURIComponent(e))) : typeof Buffer == "function" ? e => Buffer.from(e, "utf-8").toString("base64") : () => {
            throw new Error("Unsupported environment: `window.btoa` or `Buffer` should be supported.");
        };
    }
    var je = Ee(), Se = class {
        constructor(e) {
            this.version = 3, this.file = e.file, this.sources = e.sources, this.sourcesContent = e.sourcesContent, 
            this.names = e.names, this.mappings = ke(e.mappings), typeof e.x_google_ignoreList < "u" && (this.x_google_ignoreList = e.x_google_ignoreList);
        }
        toString() {
            return JSON.stringify(this);
        }
        toUrl() {
            return "data:application/json;charset=utf-8;base64," + je(this.toString());
        }
    };
    function Ce(e) {
        let t = e.split(`\n`), r = t.filter(e => /^\t+/.test(e)), n = t.filter(e => /^ {2,}/.test(e));
        if (r.length === 0 && n.length === 0) return null;
        if (r.length >= n.length) return "\t";
        let i = n.reduce((e, t) => {
            let r = /^ +/.exec(t)[0].length;
            return Math.min(r, e);
        }, 1 / 0);
        return new Array(i + 1).join(" ");
    }
    function Fe(e, t) {
        let r = e.split(/[/\\]/), n = t.split(/[/\\]/);
        for (r.pop(); r[0] === n[0]; ) r.shift(), n.shift();
        if (r.length) {
            let e = r.length;
            for (;e--; ) r[e] = "..";
        }
        return r.concat(n).join("/");
    }
    var Ae = Object.prototype.toString;
    function Le(e) {
        return Ae.call(e) === "[object Object]";
    }
    function _e(e) {
        let r = e.split(`\n`), o = [];
        for (let e = 0, t = 0; e < r.length; e++) o.push(t), t += r[e].length + 1;
        return function(t) {
            let r = 0, n = o.length;
            for (;r < n; ) {
                let e = r + n >> 1;
                t < o[e] ? n = e : r = e + 1;
            }
            let e = r - 1, i = t - o[e];
            return {
                line: e,
                column: i
            };
        };
    }
    var Ne = /\w/, We = class {
        constructor(e) {
            this.hires = e, this.generatedCodeLine = 0, this.generatedCodeColumn = 0, 
            this.raw = [], this.rawSegments = this.raw[this.generatedCodeLine] = [], 
            this.pending = null;
        }
        addEdit(i, o, a, s) {
            if (o.length) {
                let e = o.length - 1, t = o.indexOf(`\n`, 0), r = -1;
                for (;t >= 0 && e > t; ) {
                    let e = [ this.generatedCodeColumn, i, a.line, a.column ];
                    s >= 0 && e.push(s), this.rawSegments.push(e), this.generatedCodeLine += 1, 
                    this.raw[this.generatedCodeLine] = this.rawSegments = [], this.generatedCodeColumn = 0, 
                    r = t, t = o.indexOf(`\n`, t + 1);
                }
                let n = [ this.generatedCodeColumn, i, a.line, a.column ];
                s >= 0 && n.push(s), this.rawSegments.push(n), this.advance(o.slice(r + 1));
            } else this.pending && (this.rawSegments.push(this.pending), this.advance(o));
            this.pending = null;
        }
        addUneditedChunk(t, e, r, n, i) {
            let o = e.start, a = !0, s = !1;
            for (;o < e.end; ) {
                if (r[o] === `\n`) n.line += 1, n.column = 0, this.generatedCodeLine += 1, 
                this.raw[this.generatedCodeLine] = this.rawSegments = [], this.generatedCodeColumn = 0, 
                a = !0; else {
                    if (this.hires || a || i.has(o)) {
                        let e = [ this.generatedCodeColumn, t, n.line, n.column ];
                        this.hires === "boundary" ? Ne.test(r[o]) ? s || (this.rawSegments.push(e), 
                        s = !0) : (this.rawSegments.push(e), s = !1) : this.rawSegments.push(e);
                    }
                    n.column += 1, this.generatedCodeColumn += 1, a = !1;
                }
                o += 1;
            }
            this.pending = null;
        }
        advance(e) {
            if (!e) return;
            let t = e.split(`\n`);
            if (t.length > 1) {
                for (let e = 0; e < t.length - 1; e++) this.generatedCodeLine++, 
                this.raw[this.generatedCodeLine] = this.rawSegments = [];
                this.generatedCodeColumn = 0;
            }
            this.generatedCodeColumn += t[t.length - 1].length;
        }
    }, Ue = `\n`, f = {
        insertLeft: !1,
        insertRight: !1,
        storeName: !1
    }, Oe = class U {
        constructor(e, t = {}) {
            let r = new ze(0, e.length, e);
            Object.defineProperties(this, {
                original: {
                    writable: !0,
                    value: e
                },
                outro: {
                    writable: !0,
                    value: ""
                },
                intro: {
                    writable: !0,
                    value: ""
                },
                firstChunk: {
                    writable: !0,
                    value: r
                },
                lastChunk: {
                    writable: !0,
                    value: r
                },
                lastSearchedChunk: {
                    writable: !0,
                    value: r
                },
                byStart: {
                    writable: !0,
                    value: {}
                },
                byEnd: {
                    writable: !0,
                    value: {}
                },
                filename: {
                    writable: !0,
                    value: t.filename
                },
                indentExclusionRanges: {
                    writable: !0,
                    value: t.indentExclusionRanges
                },
                sourcemapLocations: {
                    writable: !0,
                    value: new we()
                },
                storedNames: {
                    writable: !0,
                    value: {}
                },
                indentStr: {
                    writable: !0,
                    value: void 0
                },
                ignoreList: {
                    writable: !0,
                    value: t.ignoreList
                }
            }), this.byStart[0] = r, this.byEnd[e.length] = r;
        }
        addSourcemapLocation(e) {
            this.sourcemapLocations.add(e);
        }
        append(e) {
            if (typeof e != "string") throw new TypeError("outro content must be a string");
            return this.outro += e, this;
        }
        appendLeft(e, t) {
            if (typeof t != "string") throw new TypeError("inserted content must be a string");
            this._split(e);
            let r = this.byEnd[e];
            return r ? r.appendLeft(t) : this.intro += t, this;
        }
        appendRight(e, t) {
            if (typeof t != "string") throw new TypeError("inserted content must be a string");
            this._split(e);
            let r = this.byStart[e];
            return r ? r.appendRight(t) : this.outro += t, this;
        }
        clone() {
            let r = new U(this.original, {
                filename: this.filename
            }), n = this.firstChunk, i = r.firstChunk = r.lastSearchedChunk = n.clone();
            for (;n; ) {
                r.byStart[i.start] = i, r.byEnd[i.end] = i;
                let e = n.next, t = e && e.clone();
                t && (i.next = t, t.previous = i, i = t), n = e;
            }
            return r.lastChunk = i, this.indentExclusionRanges && (r.indentExclusionRanges = this.indentExclusionRanges.slice()), 
            r.sourcemapLocations = new we(this.sourcemapLocations), r.intro = this.intro, 
            r.outro = this.outro, r;
        }
        generateDecodedMap(e) {
            e = e || {};
            let r = 0, n = Object.keys(this.storedNames), i = new We(e.hires), o = _e(this.original);
            return this.intro && i.advance(this.intro), this.firstChunk.eachNext(e => {
                let t = o(e.start);
                e.intro.length && i.advance(e.intro), e.edited ? i.addEdit(r, e.content, t, e.storeName ? n.indexOf(e.original) : -1) : i.addUneditedChunk(r, e, this.original, t, this.sourcemapLocations), 
                e.outro.length && i.advance(e.outro);
            }), {
                file: e.file ? e.file.split(/[/\\]/).pop() : void 0,
                sources: [ e.source ? Fe(e.file || "", e.source) : e.file || "" ],
                sourcesContent: e.includeContent ? [ this.original ] : void 0,
                names: n,
                mappings: i.raw,
                x_google_ignoreList: this.ignoreList ? [ r ] : void 0
            };
        }
        generateMap(e) {
            return new Se(this.generateDecodedMap(e));
        }
        _ensureindentStr() {
            this.indentStr === void 0 && (this.indentStr = Ce(this.original));
        }
        _getRawIndentString() {
            return this._ensureindentStr(), this.indentStr;
        }
        getIndentString() {
            return this._ensureindentStr(), this.indentStr === null ? "\t" : this.indentStr;
        }
        indent(t, e) {
            let r = /^[^\r\n]/gm;
            if (Le(t) && (e = t, t = void 0), t === void 0 && (this._ensureindentStr(), 
            t = this.indentStr || "\t"), t === "") return this;
            e = e || {};
            let n = {};
            e.exclude && (typeof e.exclude[0] == "number" ? [ e.exclude ] : e.exclude).forEach(t => {
                for (let e = t[0]; e < t[1]; e += 1) n[e] = !0;
            });
            let i = e.indentStart !== !1, o = e => i ? `${t}${e}` : (i = !0, e);
            this.intro = this.intro.replace(r, o);
            let a = 0, s = this.firstChunk;
            for (;s; ) {
                let e = s.end;
                if (s.edited) n[a] || (s.content = s.content.replace(r, o), s.content.length && (i = s.content[s.content.length - 1] === `\n`)); else for (a = s.start; a < e; ) {
                    if (!n[a]) {
                        let e = this.original[a];
                        e === `\n` ? i = !0 : e !== "\r" && i && (i = !1, a === s.start || (this._splitChunk(s, a), 
                        s = s.next), s.prependRight(t));
                    }
                    a += 1;
                }
                a = s.end, s = s.next;
            }
            return this.outro = this.outro.replace(r, o), this;
        }
        insert() {
            throw new Error("magicString.insert(...) is deprecated. Use prependRight(...) or appendLeft(...)");
        }
        insertLeft(e, t) {
            return f.insertLeft || (console.warn("magicString.insertLeft(...) is deprecated. Use magicString.appendLeft(...) instead"), 
            f.insertLeft = !0), this.appendLeft(e, t);
        }
        insertRight(e, t) {
            return f.insertRight || (console.warn("magicString.insertRight(...) is deprecated. Use magicString.prependRight(...) instead"), 
            f.insertRight = !0), this.prependRight(e, t);
        }
        move(e, t, r) {
            if (r >= e && r <= t) throw new Error("Cannot move a selection inside itself");
            this._split(e), this._split(t), this._split(r);
            let n = this.byStart[e], i = this.byEnd[t], o = n.previous, a = i.next, s = this.byStart[r];
            if (!s && i === this.lastChunk) return this;
            let l = s ? s.previous : this.lastChunk;
            return o && (o.next = a), a && (a.previous = o), l && (l.next = n), 
            s && (s.previous = i), n.previous || (this.firstChunk = i.next), i.next || (this.lastChunk = n.previous, 
            this.lastChunk.next = null), n.previous = l, i.next = s || null, l || (this.firstChunk = n), 
            s || (this.lastChunk = i), this;
        }
        overwrite(e, t, r, n) {
            return n = n || {}, this.update(e, t, r, {
                ...n,
                overwrite: !n.contentOnly
            });
        }
        update(t, r, n, e) {
            if (typeof n != "string") throw new TypeError("replacement content must be a string");
            if (this.original.length !== 0) {
                for (;t < 0; ) t += this.original.length;
                for (;r < 0; ) r += this.original.length;
            }
            if (r > this.original.length) throw new Error("end is out of bounds");
            if (t === r) throw new Error("Cannot overwrite a zero-length range \u2013 use appendLeft or prependRight instead");
            this._split(t), this._split(r), e === !0 && (f.storeName || (console.warn("The final argument to magicString.overwrite(...) should be an options object. See https://github.com/rich-harris/magic-string"), 
            f.storeName = !0), e = {
                storeName: !0
            });
            let i = e !== void 0 ? e.storeName : !1, o = e !== void 0 ? e.overwrite : !1;
            if (i) {
                let e = this.original.slice(t, r);
                Object.defineProperty(this.storedNames, e, {
                    writable: !0,
                    value: !0,
                    enumerable: !0
                });
            }
            let a = this.byStart[t], s = this.byEnd[r];
            if (a) {
                let e = a;
                for (;e !== s; ) {
                    if (e.next !== this.byStart[e.end]) throw new Error("Cannot overwrite across a split point");
                    e = e.next, e.edit("", !1);
                }
                a.edit(n, i, !o);
            } else {
                let e = new ze(t, r, "").edit(n, i);
                s.next = e, e.previous = s;
            }
            return this;
        }
        prepend(e) {
            if (typeof e != "string") throw new TypeError("outro content must be a string");
            return this.intro = e + this.intro, this;
        }
        prependLeft(e, t) {
            if (typeof t != "string") throw new TypeError("inserted content must be a string");
            this._split(e);
            let r = this.byEnd[e];
            return r ? r.prependLeft(t) : this.intro = t + this.intro, this;
        }
        prependRight(e, t) {
            if (typeof t != "string") throw new TypeError("inserted content must be a string");
            this._split(e);
            let r = this.byStart[e];
            return r ? r.prependRight(t) : this.outro = t + this.outro, this;
        }
        remove(e, t) {
            if (this.original.length !== 0) {
                for (;e < 0; ) e += this.original.length;
                for (;t < 0; ) t += this.original.length;
            }
            if (e === t) return this;
            if (e < 0 || t > this.original.length) throw new Error("Character is out of bounds");
            if (e > t) throw new Error("end must be greater than start");
            this._split(e), this._split(t);
            let r = this.byStart[e];
            for (;r; ) r.intro = "", r.outro = "", r.edit(""), r = t > r.end ? this.byStart[r.end] : null;
            return this;
        }
        reset(e, t) {
            if (this.original.length !== 0) {
                for (;e < 0; ) e += this.original.length;
                for (;t < 0; ) t += this.original.length;
            }
            if (e === t) return this;
            if (e < 0 || t > this.original.length) throw new Error("Character is out of bounds");
            if (e > t) throw new Error("end must be greater than start");
            this._split(e), this._split(t);
            let r = this.byStart[e];
            for (;r; ) r.reset(), r = t > r.end ? this.byStart[r.end] : null;
            return this;
        }
        lastChar() {
            if (this.outro.length) return this.outro[this.outro.length - 1];
            let e = this.lastChunk;
            do {
                if (e.outro.length) return e.outro[e.outro.length - 1];
                if (e.content.length) return e.content[e.content.length - 1];
                if (e.intro.length) return e.intro[e.intro.length - 1];
            } while (e = e.previous);
            return this.intro.length ? this.intro[this.intro.length - 1] : "";
        }
        lastLine() {
            let e = this.outro.lastIndexOf(Ue);
            if (e !== -1) return this.outro.substr(e + 1);
            let t = this.outro, r = this.lastChunk;
            do {
                if (r.outro.length > 0) {
                    if (e = r.outro.lastIndexOf(Ue), e !== -1) return r.outro.substr(e + 1) + t;
                    t = r.outro + t;
                }
                if (r.content.length > 0) {
                    if (e = r.content.lastIndexOf(Ue), e !== -1) return r.content.substr(e + 1) + t;
                    t = r.content + t;
                }
                if (r.intro.length > 0) {
                    if (e = r.intro.lastIndexOf(Ue), e !== -1) return r.intro.substr(e + 1) + t;
                    t = r.intro + t;
                }
            } while (r = r.previous);
            return e = this.intro.lastIndexOf(Ue), e !== -1 ? this.intro.substr(e + 1) + t : this.intro + t;
        }
        slice(n = 0, i = this.original.length) {
            if (this.original.length !== 0) {
                for (;n < 0; ) n += this.original.length;
                for (;i < 0; ) i += this.original.length;
            }
            let o = "", a = this.firstChunk;
            for (;a && (a.start > n || a.end <= n); ) {
                if (a.start < i && a.end >= i) return o;
                a = a.next;
            }
            if (a && a.edited && a.start !== n) throw new Error(`Cannot use replaced character ${n} as slice start anchor.`);
            let s = a;
            for (;a; ) {
                a.intro && (s !== a || a.start === n) && (o += a.intro);
                let e = a.start < i && a.end >= i;
                if (e && a.edited && a.end !== i) throw new Error(`Cannot use replaced character ${i} as slice end anchor.`);
                let t = s === a ? n - a.start : 0, r = e ? a.content.length + i - a.end : a.content.length;
                if (o += a.content.slice(t, r), a.outro && (!e || a.end === i) && (o += a.outro), 
                e) break;
                a = a.next;
            }
            return o;
        }
        snip(e, t) {
            let r = this.clone();
            return r.remove(0, e), r.remove(t, r.original.length), r;
        }
        _split(e) {
            if (this.byStart[e] || this.byEnd[e]) return;
            let t = this.lastSearchedChunk, r = e > t.end;
            for (;t; ) {
                if (t.contains(e)) return this._splitChunk(t, e);
                t = r ? this.byStart[t.end] : this.byEnd[t.start];
            }
        }
        _splitChunk(t, r) {
            if (t.edited && t.content.length) {
                let e = _e(this.original)(r);
                throw new Error(`Cannot split a chunk that has already been edited (${e.line}:${e.column} \u2013 "${t.original}")`);
            }
            let e = t.split(r);
            return this.byEnd[r] = t, this.byStart[r] = e, this.byEnd[e.end] = e, 
            t === this.lastChunk && (this.lastChunk = e), this.lastSearchedChunk = t, 
            !0;
        }
        toString() {
            let e = this.intro, t = this.firstChunk;
            for (;t; ) e += t.toString(), t = t.next;
            return e + this.outro;
        }
        isEmpty() {
            let e = this.firstChunk;
            do {
                if (e.intro.length && e.intro.trim() || e.content.length && e.content.trim() || e.outro.length && e.outro.trim()) return !1;
            } while (e = e.next);
            return !0;
        }
        length() {
            let e = this.firstChunk, t = 0;
            do {
                t += e.intro.length + e.content.length + e.outro.length;
            } while (e = e.next);
            return t;
        }
        trimLines() {
            return this.trim("[\\r\\n]");
        }
        trim(e) {
            return this.trimStart(e).trimEnd(e);
        }
        trimEndAborted(e) {
            let r = new RegExp((e || "\\s") + "+$");
            if (this.outro = this.outro.replace(r, ""), this.outro.length) return !0;
            let n = this.lastChunk;
            do {
                let e = n.end, t = n.trimEnd(r);
                if (n.end !== e && (this.lastChunk === n && (this.lastChunk = n.next), 
                this.byEnd[n.end] = n, this.byStart[n.next.start] = n.next, this.byEnd[n.next.end] = n.next), 
                t) return !0;
                n = n.previous;
            } while (n);
            return !1;
        }
        trimEnd(e) {
            return this.trimEndAborted(e), this;
        }
        trimStartAborted(e) {
            let r = new RegExp("^" + (e || "\\s") + "+");
            if (this.intro = this.intro.replace(r, ""), this.intro.length) return !0;
            let n = this.firstChunk;
            do {
                let e = n.end, t = n.trimStart(r);
                if (n.end !== e && (n === this.lastChunk && (this.lastChunk = n.next), 
                this.byEnd[n.end] = n, this.byStart[n.next.start] = n.next, this.byEnd[n.next.end] = n.next), 
                t) return !0;
                n = n.next;
            } while (n);
            return !1;
        }
        trimStart(e) {
            return this.trimStartAborted(e), this;
        }
        hasChanged() {
            return this.original !== this.toString();
        }
        _replaceRegexp(e, t) {
            function r(r, e) {
                return typeof t == "string" ? t.replace(/\$(\$|&|\d+)/g, (e, t) => t === "$" ? "$" : t === "&" ? r[0] : +t < r.length ? r[+t] : `$${t}`) : t(...r, r.index, e, r.groups);
            }
            function n(e, t) {
                let r, n = [];
                for (;r = e.exec(t); ) n.push(r);
                return n;
            }
            if (e.global) n(e, this.original).forEach(t => {
                if (t.index != null) {
                    let e = r(t, this.original);
                    e !== t[0] && this.overwrite(t.index, t.index + t[0].length, e);
                }
            }); else {
                let t = this.original.match(e);
                if (t && t.index != null) {
                    let e = r(t, this.original);
                    e !== t[0] && this.overwrite(t.index, t.index + t[0].length, e);
                }
            }
            return this;
        }
        _replaceString(e, t) {
            let {
                original: r
            } = this, n = r.indexOf(e);
            return n !== -1 && this.overwrite(n, n + e.length, t), this;
        }
        replace(e, t) {
            return typeof e == "string" ? this._replaceString(e, t) : this._replaceRegexp(e, t);
        }
        _replaceAllString(t, r) {
            let {
                original: n
            } = this, i = t.length;
            for (let e = n.indexOf(t); e !== -1; e = n.indexOf(t, e + i)) n.slice(e, e + i) !== r && this.overwrite(e, e + i, r);
            return this;
        }
        replaceAll(e, t) {
            if (typeof e == "string") return this._replaceAllString(e, t);
            if (!e.global) throw new TypeError("MagicString.prototype.replaceAll called with a non-global RegExp argument");
            return this._replaceRegexp(e, t);
        }
    };
    var Te = /theme\(\s*(['"])?(.*?)\1?\s*\)/g;
    function Re(e) {
        return e.includes("theme(") && e.includes(")");
    }
    function De(e, n, i = !0) {
        let t = Array.from(e.toString().matchAll(Te));
        if (!t.length) return e;
        let o = new Oe(e);
        for (let r of t) {
            let e = r[2];
            if (!e) throw new Error("theme() expect exact one argument, but got 0");
            let t = Be(e, n, i);
            t && o.overwrite(r.index, r.index + r[0].length, t);
        }
        return o.toString();
    }
    function Be(e, t, r = !0) {
        let [ n, i ] = e.split("/"), o = n.trim().split(".").reduce((e, t) => e?.[t], t);
        if (typeof o == "object" && (o = o.DEFAULT), typeof o == "string") {
            if (i) {
                let e = u(o);
                e && (o = m(e, i));
            }
            return o;
        } else if (r) throw new Error(`theme of "${e}" did not found`);
    }
    function n(n, i) {
        let o;
        return {
            name: n,
            match(e, t) {
                o || (o = new RegExp(`^${a(n)}(?:${t.generator.config.separators.join("|")})`));
                let r = e.match(o);
                if (r) return {
                    matcher: e.slice(r[0].length),
                    handle: (e, t) => t({
                        ...e,
                        ...i(e)
                    })
                };
            },
            autocomplete: `${n}:`
        };
    }
    function t(n, i) {
        let o;
        return {
            name: n,
            match(e, t) {
                o || (o = new RegExp(`^${a(n)}(?:${t.generator.config.separators.join("|")})`));
                let r = e.match(o);
                if (r) return {
                    matcher: e.slice(r[0].length),
                    handle: (e, t) => t({
                        ...e,
                        parent: `${e.parent ? `${e.parent} $$ ` : ""}${i}`
                    })
                };
            },
            autocomplete: `${n}:`
        };
    }
    function Xe(e, n, i) {
        if (n.startsWith(`${e}[`)) {
            let [ t, r ] = te(n.slice(e.length), "[", "]") ?? [];
            if (t && r) {
                for (let e of i) if (r.startsWith(e)) return [ t, r.slice(e.length), e ];
                return [ t, r, "" ];
            }
        }
    }
    function s(i, o, a) {
        if (o.startsWith(i)) {
            let r = Xe(i, o, a);
            if (r) {
                let [ e = "", t = r[1] ] = s("/", r[1], a) ?? [];
                return [ r[0], t, e ];
            }
            for (let n of a.filter(e => e !== "/")) {
                let r = o.indexOf(n, i.length);
                if (r !== -1) {
                    let e = o.indexOf("/", i.length), t = e === -1 || r <= e;
                    return [ o.slice(i.length, t ? r : e), o.slice(r + n.length), t ? "" : o.slice(e + 1, r) ];
                }
            }
        }
    }
    var Ye = {};
    T(Ye, {
        auto: () => it,
        bracket: () => dt,
        bracketOfColor: () => ut,
        bracketOfLength: () => pt,
        bracketOfPosition: () => mt,
        cssvar: () => gt,
        degree: () => bt,
        fraction: () => ct,
        global: () => xt,
        number: () => st,
        numberWithUnit: () => nt,
        percent: () => lt,
        position: () => yt,
        properties: () => vt,
        px: () => at,
        rem: () => ot,
        time: () => ht
    });
    var d = {
        l: [ "-left" ],
        r: [ "-right" ],
        t: [ "-top" ],
        b: [ "-bottom" ],
        s: [ "-inline-start" ],
        e: [ "-inline-end" ],
        x: [ "-left", "-right" ],
        y: [ "-top", "-bottom" ],
        "": [ "" ],
        bs: [ "-block-start" ],
        be: [ "-block-end" ],
        is: [ "-inline-start" ],
        ie: [ "-inline-end" ],
        block: [ "-block-start", "-block-end" ],
        inline: [ "-inline-start", "-inline-end" ]
    }, Pe = {
        ...d,
        s: [ "-inset-inline-start" ],
        start: [ "-inset-inline-start" ],
        e: [ "-inset-inline-end" ],
        end: [ "-inset-inline-end" ],
        bs: [ "-inset-block-start" ],
        be: [ "-inset-block-end" ],
        is: [ "-inset-inline-start" ],
        ie: [ "-inset-inline-end" ],
        block: [ "-inset-block-start", "-inset-block-end" ],
        inline: [ "-inset-inline-start", "-inset-inline-end" ]
    }, Ie = {
        l: [ "-top-left", "-bottom-left" ],
        r: [ "-top-right", "-bottom-right" ],
        t: [ "-top-left", "-top-right" ],
        b: [ "-bottom-left", "-bottom-right" ],
        tl: [ "-top-left" ],
        lt: [ "-top-left" ],
        tr: [ "-top-right" ],
        rt: [ "-top-right" ],
        bl: [ "-bottom-left" ],
        lb: [ "-bottom-left" ],
        br: [ "-bottom-right" ],
        rb: [ "-bottom-right" ],
        "": [ "" ],
        bs: [ "-start-start", "-start-end" ],
        be: [ "-end-start", "-end-end" ],
        s: [ "-end-start", "-start-start" ],
        is: [ "-end-start", "-start-start" ],
        e: [ "-start-end", "-end-end" ],
        ie: [ "-start-end", "-end-end" ],
        ss: [ "-start-start" ],
        "bs-is": [ "-start-start" ],
        "is-bs": [ "-start-start" ],
        se: [ "-start-end" ],
        "bs-ie": [ "-start-end" ],
        "ie-bs": [ "-start-end" ],
        es: [ "-end-start" ],
        "be-is": [ "-end-start" ],
        "is-be": [ "-end-start" ],
        ee: [ "-end-end" ],
        "be-ie": [ "-end-end" ],
        "ie-be": [ "-end-end" ]
    }, qe = {
        x: [ "-x" ],
        y: [ "-y" ],
        z: [ "-z" ],
        "": [ "-x", "-y" ]
    }, Me = [ "x", "y", "z" ], Ze = [ "top", "top center", "top left", "top right", "bottom", "bottom center", "bottom left", "bottom right", "left", "left center", "left top", "left bottom", "right", "right center", "right top", "right bottom", "center", "center top", "center bottom", "center left", "center right", "center center" ], r = Object.assign({}, ...Ze.map(e => ({
        [e.replace(/ /, "-")]: e
    })), ...Ze.map(e => ({
        [e.replace(/\b(\w)\w+/g, "$1").replace(/ /, "")]: e
    }))), i = [ "inherit", "initial", "revert", "revert-layer", "unset" ], He = /^(calc|clamp|min|max)\s*\((.+)\)(.*)/, Ge = /^(var)\s*\((.+)\)(.*)/;
    var Je = /^(-?\d*(?:\.\d+)?)(px|pt|pc|%|r?(?:em|ex|lh|cap|ch|ic)|(?:[sld]?v|cq)(?:[whib]|min|max)|in|cm|mm|rpx)?$/i, Ke = /^(-?\d*(?:\.\d+)?)$/, Ve = /^(px|[sld]?v[wh])$/i, Qe = {
        px: 1,
        vw: 100,
        vh: 100,
        svw: 100,
        svh: 100,
        dvw: 100,
        dvh: 100,
        lvh: 100,
        lvw: 100
    }, et = /^\[(color|image|length|size|position|quoted|string):/i, tt = /,(?![^()]*\))/g;
    var rt = [ "color", "border-color", "background-color", "outline-color", "text-decoration-color", "flex-grow", "flex", "flex-shrink", "caret-color", "font", "gap", "opacity", "visibility", "z-index", "font-weight", "zoom", "text-shadow", "transform", "box-shadow", "border", "background-position", "left", "right", "top", "bottom", "object-position", "max-height", "min-height", "max-width", "min-width", "height", "width", "border-width", "margin", "padding", "outline-width", "outline-offset", "font-size", "line-height", "text-indent", "vertical-align", "border-spacing", "letter-spacing", "word-spacing", "stroke", "filter", "backdrop-filter", "fill", "mask", "mask-size", "mask-border", "clip-path", "clip", "border-radius" ];
    function o(e) {
        return +e.toFixed(10);
    }
    function nt(e) {
        let t = e.match(Je);
        if (!t) return;
        let [ , r, n ] = t, i = Number.parseFloat(r);
        if (n && !Number.isNaN(i)) return `${o(i)}${n}`;
    }
    function it(e) {
        if (e === "auto" || e === "a") return "auto";
    }
    function ot(e) {
        if (!e) return;
        if (Ve.test(e)) return `${Qe[e]}${e}`;
        let t = e.match(Je);
        if (!t) return;
        let [ , r, n ] = t, i = Number.parseFloat(r);
        if (!Number.isNaN(i)) return i === 0 ? "0" : n ? `${o(i)}${n}` : `${o(i / 4)}rem`;
    }
    function at(e) {
        if (Ve.test(e)) return `${Qe[e]}${e}`;
        let t = e.match(Je);
        if (!t) return;
        let [ , r, n ] = t, i = Number.parseFloat(r);
        if (!Number.isNaN(i)) return n ? `${o(i)}${n}` : `${o(i)}px`;
    }
    function st(e) {
        if (!Ke.test(e)) return;
        let t = Number.parseFloat(e);
        if (!Number.isNaN(t)) return o(t);
    }
    function lt(e) {
        if (e.endsWith("%") && (e = e.slice(0, -1)), !Ke.test(e)) return;
        let t = Number.parseFloat(e);
        if (!Number.isNaN(t)) return `${o(t / 100)}`;
    }
    function ct(e) {
        if (!e) return;
        if (e === "full") return "100%";
        let [ t, r ] = e.split("/"), n = Number.parseFloat(t) / Number.parseFloat(r);
        if (!Number.isNaN(n)) return n === 0 ? "0" : `${o(n * 100)}%`;
    }
    function ft(i, o) {
        if (i && i.startsWith("[") && i.endsWith("]")) {
            let t, e, r = i.match(et);
            if (r ? (o || (e = r[1]), t = i.slice(r[0].length, -1)) : t = i.slice(1, -1), 
            !t || t === '=""') return;
            t.startsWith("--") && (t = `var(${t})`);
            let n = 0;
            for (let e of t) if (e === "[") n += 1; else if (e === "]" && (n -= 1, 
            n < 0)) return;
            if (n) return;
            switch (e) {
              case "string":
                return t.replace(/(^|[^\\])_/g, "$1 ").replace(/\\_/g, "_");

              case "quoted":
                return t.replace(/(^|[^\\])_/g, "$1 ").replace(/\\_/g, "_").replace(/(["\\])/g, "\\$1").replace(/^(.+)$/, '"$1"');
            }
            return t.replace(/(url\(.*?\))/g, e => e.replace(/_/g, "\\_")).replace(/(^|[^\\])_/g, "$1 ").replace(/\\_/g, "_").replace(/(?:calc|clamp|max|min)\((.*)/g, e => {
                let r = [];
                return e.replace(/var\((--.+?)[,)]/g, (e, t) => (r.push(t), e.replace(t, "--un-calc"))).replace(/(-?\d*\.?\d(?!-\d.+[,)](?![^+\-/*])\D)(?:%|[a-z]+)?|\))([+\-/*])/g, "$1 $2 ").replace(/--un-calc/g, () => r.shift());
            });
        }
    }
    function dt(e) {
        return ft(e);
    }
    function ut(e) {
        return ft(e, "color");
    }
    function pt(e) {
        return ft(e, "length");
    }
    function mt(e) {
        return ft(e, "position");
    }
    function gt(r) {
        if (/^\$[^\s'"`;{}]/.test(r)) {
            let [ e, t ] = r.slice(1).split(",");
            return `var(--${x(e)}${t ? `, ${t}` : ""})`;
        }
    }
    function ht(e) {
        let t = e.match(/^(-?[0-9.]+)(s|ms)?$/i);
        if (!t) return;
        let [ , r, n ] = t, i = Number.parseFloat(r);
        if (!Number.isNaN(i)) return i === 0 && !n ? "0s" : n ? `${o(i)}${n}` : `${o(i)}ms`;
    }
    function bt(e) {
        let t = e.match(/^(-?[0-9.]+)(deg|rad|grad|turn)?$/i);
        if (!t) return;
        let [ , r, n ] = t, i = Number.parseFloat(r);
        if (!Number.isNaN(i)) return i === 0 ? "0" : n ? `${o(i)}${n}` : `${o(i)}deg`;
    }
    function xt(e) {
        if (i.includes(e)) return e;
    }
    function vt(e) {
        if (e.split(",").every(e => rt.includes(e))) return e;
    }
    function yt(e) {
        if ([ "top", "left", "right", "bottom", "center" ].includes(e)) return e;
    }
    var $t = me(Ye), v = $t;
    var kt = {
        mid: "middle",
        base: "baseline",
        btm: "bottom",
        baseline: "baseline",
        top: "top",
        start: "top",
        middle: "middle",
        bottom: "bottom",
        end: "bottom",
        "text-top": "text-top",
        "text-bottom": "text-bottom",
        sub: "sub",
        "super": "super",
        ...Object.fromEntries(i.map(e => [ e, e ]))
    }, wt = [ [ /^(?:vertical|align|v)-([-\w]+%?)$/, ([ , e ]) => ({
        "vertical-align": kt[e] ?? v.numberWithUnit(e)
    }), {
        autocomplete: [ `(vertical|align|v)-(${Object.keys(kt).join("|")})`, "(vertical|align|v)-<percentage>" ]
    } ] ], zt = [ "center", "left", "right", "justify", "start", "end" ], Et = [ ...zt.map(e => [ `text-${e}`, {
        "text-align": e
    } ]), ...[ ...i, ...zt ].map(e => [ `text-align-${e}`, {
        "text-align": e
    } ]) ];
    var jt = "$$mini-no-negative";
    function e(o) {
        return ([ e, r, n ], {
            theme: i
        }) => {
            let t = i.spacing?.[n || "DEFAULT"] ?? v.bracket.cssvar.global.auto.fraction.rem(n);
            if (t != null) return d[r].map(e => [ `${o}${e}`, t ]);
            if (n?.startsWith("-")) {
                let t = i.spacing?.[n.slice(1)];
                if (t != null) return d[r].map(e => [ `${o}${e}`, `calc(${t} * -1)` ]);
            }
        };
    }
    function St(e, r, t = "colors") {
        let n = e[t], i = -1;
        for (let t of r) {
            if (i += 1, n && typeof n != "string") {
                let e = r.slice(i).join("-").replace(/(-[a-z])/g, e => e.slice(1).toUpperCase());
                if (n[e]) return n[e];
                if (n[t]) {
                    n = n[t];
                    continue;
                }
            }
            return;
        }
        return n;
    }
    function Ct(e, t, r) {
        return St(e, t, r) || St(e, t, "colors");
    }
    function Ft(e, t) {
        let [ r, n ] = l(e, "[", "]", [ "/", ":" ]) ?? [];
        if (r != null) {
            let e = (r.match(et) ?? [])[1];
            if (e == null || e === t) return [ r, n ];
        }
    }
    function At(e, r, n) {
        let t = Ft(e, "color");
        if (!t) return;
        let [ i, o ] = t, a = i.replace(/([a-z])(\d)/g, "$1-$2").split(/-/g), [ s ] = a;
        if (!s) return;
        let l, c = v.bracketOfColor(i), f = c || i;
        if (v.numberWithUnit(f)) return;
        if (/^#[\da-f]+$/i.test(f) ? l = f : /^hex-[\da-fA-F]+$/.test(f) ? l = `#${f.slice(4)}` : i.startsWith("$") && (l = v.cssvar(i)), 
        l = l || c, !l) {
            let e = Ct(r, [ i ], n);
            typeof e == "string" && (l = e);
        }
        let d = "DEFAULT";
        if (!l) {
            let e, [ t ] = a.slice(-1);
            /^\d+$/.test(t) ? (d = t, e = Ct(r, a.slice(0, -1), n), !e || typeof e == "string" ? l = void 0 : l = e[d]) : (e = Ct(r, a, n), 
            !e && a.length <= 2 && ([ , d = d ] = a, e = Ct(r, [ s ], n)), typeof e == "string" ? l = e : d && e && (l = e[d]));
        }
        return {
            opacity: o,
            name: s,
            no: d,
            color: l,
            cssColor: u(l),
            alpha: v.bracket.cssvar.percent(o ?? "")
        };
    }
    function g(c, f, d, u) {
        return ([ , e ], {
            theme: t,
            generator: r
        }) => {
            let n = At(e, t, d);
            if (!n) return;
            let {
                alpha: i,
                color: o,
                cssColor: a
            } = n, s = r.config.envMode === "dev" && o ? ` /* ${o} */` : "", l = {};
            if (a) if (i != null) l[c] = m(a, i) + s; else {
                let e = `--un-${f}-opacity`, t = m(a, `var(${e})`);
                t.includes(e) && (l[e] = p(a)), l[c] = t + s;
            } else if (o) if (i != null) l[c] = m(o, i) + s; else {
                let e = `--un-${f}-opacity`, t = m(o, `var(${e})`);
                t.includes(e) && (l[e] = 1), l[c] = t + s;
            }
            if (u?.(l) !== !1) return l;
        };
    }
    function Lt(a, s) {
        let l = [];
        a = c(a);
        for (let o = 0; o < a.length; o++) {
            let t = re(a[o], " ", 6);
            if (!t || t.length < 3) return a;
            let e = !1, r = t.indexOf("inset");
            r !== -1 && (t.splice(r, 1), e = !0);
            let n = "", i = t.at(-1);
            if (u(t.at(0))) {
                let e = u(t.shift());
                e && (n = `, ${m(e)}`);
            } else if (u(i)) {
                let e = u(t.pop());
                e && (n = `, ${m(e)}`);
            } else i && Ge.test(i) && (n = `, ${t.pop()}`);
            l.push(`${e ? "inset " : ""}${t.join(" ")} var(${s}${n})`);
        }
        return l;
    }
    function _t(e, t, r) {
        return e != null && !!At(e, t, r)?.color;
    }
    var Nt = /[a-z]+/gi, Wt = new WeakMap();
    function Ut({
        theme: e,
        generator: t
    }, r = "breakpoints") {
        let n = t?.userConfig?.theme?.[r] || e[r];
        if (!n) return;
        if (Wt.has(e)) return Wt.get(e);
        let i = Object.entries(n).sort((e, t) => Number.parseInt(e[1].replace(Nt, "")) - Number.parseInt(t[1].replace(Nt, ""))).map(([ e, t ]) => ({
            point: e,
            size: t
        }));
        return Wt.set(e, i), i;
    }
    function h(t, r) {
        return i.map(e => [ `${t}-${e}`, {
            [r ?? t]: e
        } ]);
    }
    function b(e) {
        return e != null && He.test(e);
    }
    function Ot(e) {
        return e[0] === "[" && e.slice(-1) === "]" && (e = e.slice(1, -1)), He.test(e) || Je.test(e);
    }
    function Tt(e, t, r) {
        let n = t.split(tt);
        return e || !e && n.length === 1 ? qe[e].map(e => [ `--un-${r}${e}`, t ]) : n.map((e, t) => [ `--un-${r}-${Me[t]}`, e ]);
    }
    var Rt = [ [ /^outline-(?:width-|size-)?(.+)$/, Dt, {
        autocomplete: "outline-(width|size)-<num>"
    } ], [ /^outline-(?:color-)?(.+)$/, Bt, {
        autocomplete: "outline-$colors"
    } ], [ /^outline-offset-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "outline-offset": t.lineWidth?.[e] ?? v.bracket.cssvar.global.px(e)
    }), {
        autocomplete: "outline-(offset)-<num>"
    } ], [ "outline", {
        "outline-style": "solid"
    } ], ...[ "auto", "dashed", "dotted", "double", "hidden", "solid", "groove", "ridge", "inset", "outset", ...i ].map(e => [ `outline-${e}`, {
        "outline-style": e
    } ]), [ "outline-none", {
        outline: "2px solid transparent",
        "outline-offset": "2px"
    } ] ];
    function Dt([ , e ], {
        theme: t
    }) {
        return {
            "outline-width": t.lineWidth?.[e] ?? v.bracket.cssvar.global.px(e)
        };
    }
    function Bt(e, t) {
        return b(v.bracket(e[1])) ? Dt(e, t) : g("outline-color", "outline-color", "borderColor")(e, t);
    }
    var Xt = [ [ "appearance-auto", {
        "-webkit-appearance": "auto",
        appearance: "auto"
    } ], [ "appearance-none", {
        "-webkit-appearance": "none",
        appearance: "none"
    } ] ];
    function Yt(e) {
        return v.properties.auto.global(e) ?? {
            contents: "contents",
            scroll: "scroll-position"
        }[e];
    }
    var Pt = [ [ /^will-change-(.+)/, ([ , e ]) => ({
        "will-change": Yt(e)
    }) ] ];
    var y = [ "solid", "dashed", "dotted", "double", "hidden", "none", "groove", "ridge", "inset", "outset", ...i ], It = [ [ /^(?:border|b)()(?:-(.+))?$/, $, {
        autocomplete: "(border|b)-<directions>"
    } ], [ /^(?:border|b)-([xy])(?:-(.+))?$/, $ ], [ /^(?:border|b)-([rltbse])(?:-(.+))?$/, $ ], [ /^(?:border|b)-(block|inline)(?:-(.+))?$/, $ ], [ /^(?:border|b)-([bi][se])(?:-(.+))?$/, $ ], [ /^(?:border|b)-()(?:width|size)-(.+)$/, $, {
        autocomplete: [ "(border|b)-<num>", "(border|b)-<directions>-<num>" ]
    } ], [ /^(?:border|b)-([xy])-(?:width|size)-(.+)$/, $ ], [ /^(?:border|b)-([rltbse])-(?:width|size)-(.+)$/, $ ], [ /^(?:border|b)-(block|inline)-(?:width|size)-(.+)$/, $ ], [ /^(?:border|b)-([bi][se])-(?:width|size)-(.+)$/, $ ], [ /^(?:border|b)-()(?:color-)?(.+)$/, Zt, {
        autocomplete: [ "(border|b)-$colors", "(border|b)-<directions>-$colors" ]
    } ], [ /^(?:border|b)-([xy])-(?:color-)?(.+)$/, Zt ], [ /^(?:border|b)-([rltbse])-(?:color-)?(.+)$/, Zt ], [ /^(?:border|b)-(block|inline)-(?:color-)?(.+)$/, Zt ], [ /^(?:border|b)-([bi][se])-(?:color-)?(.+)$/, Zt ], [ /^(?:border|b)-()op(?:acity)?-?(.+)$/, Ht, {
        autocomplete: "(border|b)-(op|opacity)-<percent>"
    } ], [ /^(?:border|b)-([xy])-op(?:acity)?-?(.+)$/, Ht ], [ /^(?:border|b)-([rltbse])-op(?:acity)?-?(.+)$/, Ht ], [ /^(?:border|b)-(block|inline)-op(?:acity)?-?(.+)$/, Ht ], [ /^(?:border|b)-([bi][se])-op(?:acity)?-?(.+)$/, Ht ], [ /^(?:border-|b-)?(?:rounded|rd)()(?:-(.+))?$/, Gt, {
        autocomplete: [ "(border|b)-(rounded|rd)", "(border|b)-(rounded|rd)-$borderRadius", "(rounded|rd)", "(rounded|rd)-$borderRadius" ]
    } ], [ /^(?:border-|b-)?(?:rounded|rd)-([rltbse])(?:-(.+))?$/, Gt ], [ /^(?:border-|b-)?(?:rounded|rd)-([rltb]{2})(?:-(.+))?$/, Gt ], [ /^(?:border-|b-)?(?:rounded|rd)-([bise][se])(?:-(.+))?$/, Gt ], [ /^(?:border-|b-)?(?:rounded|rd)-([bi][se]-[bi][se])(?:-(.+))?$/, Gt ], [ /^(?:border|b)-(?:style-)?()(.+)$/, Jt, {
        autocomplete: [ "(border|b)-style", `(border|b)-(${y.join("|")})`, "(border|b)-<directions>-style", `(border|b)-<directions>-(${y.join("|")})`, `(border|b)-<directions>-style-(${y.join("|")})`, `(border|b)-style-(${y.join("|")})` ]
    } ], [ /^(?:border|b)-([xy])-(?:style-)?(.+)$/, Jt ], [ /^(?:border|b)-([rltbse])-(?:style-)?(.+)$/, Jt ], [ /^(?:border|b)-(block|inline)-(?:style-)?(.+)$/, Jt ], [ /^(?:border|b)-([bi][se])-(?:style-)?(.+)$/, Jt ] ];
    function qt(i, e, o) {
        if (e != null) return {
            [`border${o}-color`]: m(i, e)
        };
        if (o === "") {
            let e = {}, t = "--un-border-opacity", r = m(i, `var(${t})`);
            return r.includes(t) && (e[t] = typeof i == "string" ? 1 : p(i)), e["border-color"] = r, 
            e;
        } else {
            let e = {}, t = "--un-border-opacity", r = `--un-border${o}-opacity`, n = m(i, `var(${r})`);
            return n.includes(r) && (e[t] = typeof i == "string" ? 1 : p(i), e[r] = `var(${t})`), 
            e[`border${o}-color`] = n, e;
        }
    }
    function Mt(a) {
        return ([ , e ], t) => {
            let r = At(e, t, "borderColor");
            if (!r) return;
            let {
                alpha: n,
                color: i,
                cssColor: o
            } = r;
            if (o) return qt(o, n, a);
            if (i) return qt(i, n, a);
        };
    }
    function $([ , e = "", t ], {
        theme: r
    }) {
        let n = r.lineWidth?.[t || "DEFAULT"] ?? v.bracket.cssvar.global.px(t || "1");
        if (e in d && n != null) return d[e].map(e => [ `border${e}-width`, n ]);
    }
    function Zt([ , e = "", t ], r) {
        if (e in d) {
            if (b(v.bracket(t))) return $([ "", e, t ], r);
            if (_t(t, r.theme, "borderColor")) return Object.assign({}, ...d[e].map(e => Mt(e)([ "", t ], r.theme)));
        }
    }
    function Ht([ , e = "", t ]) {
        let r = v.bracket.percent.cssvar(t);
        if (e in d && r != null) return d[e].map(e => [ `--un-border${e}-opacity`, r ]);
    }
    function Gt([ , e = "", t ], {
        theme: r
    }) {
        let n = r.borderRadius?.[t || "DEFAULT"] || v.bracket.cssvar.global.fraction.rem(t || "1");
        if (e in Ie && n != null) return Ie[e].map(e => [ `border${e}-radius`, n ]);
    }
    function Jt([ , e = "", t ]) {
        if (y.includes(t) && e in d) return d[e].map(e => [ `border${e}-style`, t ]);
    }
    var Kt = [ [ /^op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        opacity: v.bracket.percent.cssvar(e)
    }) ] ], Vt = /^\[url\(.+\)\]$/, Qt = /^\[(?:length|size):.+\]$/, er = /^\[position:.+\]$/, tr = /^\[(?:linear|conic|radial)-gradient\(.+\)\]$/, rr = /^\[image:.+\]$/, nr = [ [ /^bg-(.+)$/, (...e) => {
        let t = e[0][1];
        if (Vt.test(t)) return {
            "--un-url": v.bracket(t),
            "background-image": "var(--un-url)"
        };
        if (Qt.test(t) && v.bracketOfLength(t) != null) return {
            "background-size": v.bracketOfLength(t).split(" ").map(e => v.fraction.auto.px.cssvar(e) ?? e).join(" ")
        };
        if ((Ot(t) || er.test(t)) && v.bracketOfPosition(t) != null) return {
            "background-position": v.bracketOfPosition(t).split(" ").map(e => v.position.fraction.auto.px.cssvar(e) ?? e).join(" ")
        };
        if (tr.test(t) || rr.test(t)) {
            let e = v.bracket(t);
            if (e) return {
                "background-image": (e.startsWith("http") ? `url(${e})` : v.cssvar(e)) ?? e
            };
        }
        return g("background-color", "bg", "backgroundColor")(...e);
    }, {
        autocomplete: "bg-$colors"
    } ], [ /^bg-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-bg-opacity": v.bracket.percent.cssvar(e)
    }), {
        autocomplete: "bg-(op|opacity)-<percent>"
    } ] ], ir = [ [ /^color-scheme-(\w+)$/, ([ , e ]) => ({
        "color-scheme": e
    }) ] ];
    var or = [ [ /^@container(?:\/(\w+))?(?:-(normal))?$/, ([ , e, t ]) => ({
        "container-type": t ?? "inline-size",
        "container-name": e
    }) ] ];
    var ar = [ "solid", "double", "dotted", "dashed", "wavy", ...i ], sr = [ [ /^(?:decoration-)?(underline|overline|line-through)$/, ([ , e ]) => ({
        "text-decoration-line": e
    }), {
        autocomplete: "decoration-(underline|overline|line-through)"
    } ], [ /^(?:underline|decoration)-(?:size-)?(.+)$/, lr, {
        autocomplete: "(underline|decoration)-<num>"
    } ], [ /^(?:underline|decoration)-(auto|from-font)$/, ([ , e ]) => ({
        "text-decoration-thickness": e
    }), {
        autocomplete: "(underline|decoration)-(auto|from-font)"
    } ], [ /^(?:underline|decoration)-(.+)$/, cr, {
        autocomplete: "(underline|decoration)-$colors"
    } ], [ /^(?:underline|decoration)-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-line-opacity": v.bracket.percent.cssvar(e)
    }), {
        autocomplete: "(underline|decoration)-(op|opacity)-<percent>"
    } ], [ /^(?:underline|decoration)-offset-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "text-underline-offset": t.lineWidth?.[e] ?? v.auto.bracket.cssvar.global.px(e)
    }), {
        autocomplete: "(underline|decoration)-(offset)-<num>"
    } ], ...ar.map(e => [ `underline-${e}`, {
        "text-decoration-style": e
    } ]), ...ar.map(e => [ `decoration-${e}`, {
        "text-decoration-style": e
    } ]), [ "no-underline", {
        "text-decoration": "none"
    } ], [ "decoration-none", {
        "text-decoration": "none"
    } ] ];
    function lr([ , e ], {
        theme: t
    }) {
        return {
            "text-decoration-thickness": t.lineWidth?.[e] ?? v.bracket.cssvar.global.px(e)
        };
    }
    function cr(e, t) {
        if (b(v.bracket(e[1]))) return lr(e, t);
        let r = g("text-decoration-color", "line", "borderColor")(e, t);
        if (r) return {
            "-webkit-text-decoration-color": r["text-decoration-color"],
            ...r
        };
    }
    var fr = [ [ "flex", {
        display: "flex"
    } ], [ "inline-flex", {
        display: "inline-flex"
    } ], [ "flex-inline", {
        display: "inline-flex"
    } ], [ /^flex-(.*)$/, ([ , e ]) => ({
        flex: v.bracket(e) != null ? v.bracket(e).split(" ").map(e => v.cssvar.fraction(e) ?? e).join(" ") : v.cssvar.fraction(e)
    }) ], [ "flex-1", {
        flex: "1 1 0%"
    } ], [ "flex-auto", {
        flex: "1 1 auto"
    } ], [ "flex-initial", {
        flex: "0 1 auto"
    } ], [ "flex-none", {
        flex: "none"
    } ], [ /^(?:flex-)?shrink(?:-(.*))?$/, ([ , e = "" ]) => ({
        "flex-shrink": v.bracket.cssvar.number(e) ?? 1
    }), {
        autocomplete: [ "flex-shrink-<num>", "shrink-<num>" ]
    } ], [ /^(?:flex-)?grow(?:-(.*))?$/, ([ , e = "" ]) => ({
        "flex-grow": v.bracket.cssvar.number(e) ?? 1
    }), {
        autocomplete: [ "flex-grow-<num>", "grow-<num>" ]
    } ], [ /^(?:flex-)?basis-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "flex-basis": t.spacing?.[e] ?? v.bracket.cssvar.auto.fraction.rem(e)
    }), {
        autocomplete: [ "flex-basis-$spacing", "basis-$spacing" ]
    } ], [ "flex-row", {
        "flex-direction": "row"
    } ], [ "flex-row-reverse", {
        "flex-direction": "row-reverse"
    } ], [ "flex-col", {
        "flex-direction": "column"
    } ], [ "flex-col-reverse", {
        "flex-direction": "column-reverse"
    } ], [ "flex-wrap", {
        "flex-wrap": "wrap"
    } ], [ "flex-wrap-reverse", {
        "flex-wrap": "wrap-reverse"
    } ], [ "flex-nowrap", {
        "flex-wrap": "nowrap"
    } ] ];
    var dr = {
        "": "",
        x: "column-",
        y: "row-",
        col: "column-",
        row: "row-"
    };
    function ur([ , e = "", t ], {
        theme: r
    }) {
        let n = r.spacing?.[t] ?? v.bracket.cssvar.global.rem(t);
        if (n != null) return {
            [`${dr[e]}gap`]: n
        };
    }
    var pr = [ [ /^(?:flex-|grid-)?gap-?()(.+)$/, ur, {
        autocomplete: [ "gap-$spacing", "gap-<num>" ]
    } ], [ /^(?:flex-|grid-)?gap-([xy])-?(.+)$/, ur, {
        autocomplete: [ "gap-(x|y)-$spacing", "gap-(x|y)-<num>" ]
    } ], [ /^(?:flex-|grid-)?gap-(col|row)-?(.+)$/, ur, {
        autocomplete: [ "gap-(col|row)-$spacing", "gap-(col|row)-<num>" ]
    } ] ];
    function k(e) {
        return e.replace("col", "column");
    }
    function mr(e) {
        return e[0] === "r" ? "Row" : "Column";
    }
    function gr(e, t, r) {
        let n = t[`gridAuto${mr(e)}`]?.[r];
        if (n != null) return n;
        switch (r) {
          case "min":
            return "min-content";

          case "max":
            return "max-content";

          case "fr":
            return "minmax(0,1fr)";
        }
        return v.bracket.cssvar.auto.rem(r);
    }
    var hr = [ [ "grid", {
        display: "grid"
    } ], [ "inline-grid", {
        display: "inline-grid"
    } ], [ /^(?:grid-)?(row|col)-(.+)$/, ([ , e, t ], {
        theme: r
    }) => ({
        [`grid-${k(e)}`]: r[`grid${mr(e)}`]?.[t] ?? v.bracket.cssvar.auto(t)
    }) ], [ /^(?:grid-)?(row|col)-span-(.+)$/, ([ , e, t ]) => {
        if (t === "full") return {
            [`grid-${k(e)}`]: "1/-1"
        };
        let r = v.bracket.number(t);
        if (r != null) return {
            [`grid-${k(e)}`]: `span ${r}/span ${r}`
        };
    }, {
        autocomplete: "(grid-row|grid-col|row|col)-span-<num>"
    } ], [ /^(?:grid-)?(row|col)-start-(.+)$/, ([ , e, t ]) => ({
        [`grid-${k(e)}-start`]: v.bracket.cssvar(t) ?? t
    }) ], [ /^(?:grid-)?(row|col)-end-(.+)$/, ([ , e, t ]) => ({
        [`grid-${k(e)}-end`]: v.bracket.cssvar(t) ?? t
    }), {
        autocomplete: "(grid-row|grid-col|row|col)-(start|end)-<num>"
    } ], [ /^(?:grid-)?auto-(rows|cols)-(.+)$/, ([ , e, t ], {
        theme: r
    }) => ({
        [`grid-auto-${k(e)}`]: gr(e, r, t)
    }), {
        autocomplete: "(grid-auto|auto)-(rows|cols)-<num>"
    } ], [ /^(?:grid-auto-flow|auto-flow|grid-flow)-(.+)$/, ([ , e ]) => ({
        "grid-auto-flow": v.bracket.cssvar(e)
    }) ], [ /^(?:grid-auto-flow|auto-flow|grid-flow)-(row|col|dense|row-dense|col-dense)$/, ([ , e ]) => ({
        "grid-auto-flow": k(e).replace("-", " ")
    }), {
        autocomplete: [ "(grid-auto-flow|auto-flow|grid-flow)-(row|col|dense|row-dense|col-dense)" ]
    } ], [ /^(?:grid-)?(rows|cols)-(.+)$/, ([ , e, t ], {
        theme: r
    }) => ({
        [`grid-template-${k(e)}`]: r[`gridTemplate${mr(e)}`]?.[t] ?? v.bracket.cssvar(t)
    }) ], [ /^(?:grid-)?(rows|cols)-minmax-([\w.-]+)$/, ([ , e, t ]) => ({
        [`grid-template-${k(e)}`]: `repeat(auto-fill,minmax(${t},1fr))`
    }) ], [ /^(?:grid-)?(rows|cols)-(\d+)$/, ([ , e, t ]) => ({
        [`grid-template-${k(e)}`]: `repeat(${t},minmax(0,1fr))`
    }), {
        autocomplete: "(grid-rows|grid-cols|rows|cols)-<num>"
    } ], [ /^grid-area(s)?-(.+)$/, ([ , e, t ]) => e != null ? {
        "grid-template-areas": v.cssvar(t) ?? t.split("-").map(e => `"${v.bracket(e)}"`).join(" ")
    } : {
        "grid-area": v.bracket.cssvar(t)
    } ], [ "grid-rows-none", {
        "grid-template-rows": "none"
    } ], [ "grid-cols-none", {
        "grid-template-columns": "none"
    } ], [ "grid-rows-subgrid", {
        "grid-template-rows": "subgrid"
    } ], [ "grid-cols-subgrid", {
        "grid-template-columns": "subgrid"
    } ] ];
    var br = [ "auto", "hidden", "clip", "visible", "scroll", "overlay", ...i ], xr = [ [ /^(?:overflow|of)-(.+)$/, ([ , e ]) => br.includes(e) ? {
        overflow: e
    } : void 0, {
        autocomplete: [ `(overflow|of)-(${br.join("|")})`, `(overflow|of)-(x|y)-(${br.join("|")})` ]
    } ], [ /^(?:overflow|of)-([xy])-(.+)$/, ([ , e, t ]) => br.includes(t) ? {
        [`overflow-${e}`]: t
    } : void 0 ] ];
    var vr = [ [ /^(?:position-|pos-)?(relative|absolute|fixed|sticky)$/, ([ , e ]) => ({
        position: e
    }), {
        autocomplete: [ "(position|pos)-<position>", "(position|pos)-<globalKeyword>", "<position>" ]
    } ], [ /^(?:position-|pos-)([-\w]+)$/, ([ , e ]) => i.includes(e) ? {
        position: e
    } : void 0 ], [ /^(?:position-|pos-)?(static)$/, ([ , e ]) => ({
        position: e
    }) ] ], yr = [ [ "justify-start", {
        "justify-content": "flex-start"
    } ], [ "justify-end", {
        "justify-content": "flex-end"
    } ], [ "justify-center", {
        "justify-content": "center"
    } ], [ "justify-between", {
        "justify-content": "space-between"
    } ], [ "justify-around", {
        "justify-content": "space-around"
    } ], [ "justify-evenly", {
        "justify-content": "space-evenly"
    } ], [ "justify-stretch", {
        "justify-content": "stretch"
    } ], [ "justify-left", {
        "justify-content": "left"
    } ], [ "justify-right", {
        "justify-content": "right"
    } ], ...h("justify", "justify-content"), [ "justify-items-start", {
        "justify-items": "start"
    } ], [ "justify-items-end", {
        "justify-items": "end"
    } ], [ "justify-items-center", {
        "justify-items": "center"
    } ], [ "justify-items-stretch", {
        "justify-items": "stretch"
    } ], ...h("justify-items"), [ "justify-self-auto", {
        "justify-self": "auto"
    } ], [ "justify-self-start", {
        "justify-self": "start"
    } ], [ "justify-self-end", {
        "justify-self": "end"
    } ], [ "justify-self-center", {
        "justify-self": "center"
    } ], [ "justify-self-stretch", {
        "justify-self": "stretch"
    } ], ...h("justify-self") ], $r = [ [ /^order-(.+)$/, ([ , e ]) => ({
        order: v.bracket.cssvar.number(e)
    }) ], [ "order-first", {
        order: "-9999"
    } ], [ "order-last", {
        order: "9999"
    } ], [ "order-none", {
        order: "0"
    } ] ], kr = [ [ "content-center", {
        "align-content": "center"
    } ], [ "content-start", {
        "align-content": "flex-start"
    } ], [ "content-end", {
        "align-content": "flex-end"
    } ], [ "content-between", {
        "align-content": "space-between"
    } ], [ "content-around", {
        "align-content": "space-around"
    } ], [ "content-evenly", {
        "align-content": "space-evenly"
    } ], ...h("content", "align-content"), [ "items-start", {
        "align-items": "flex-start"
    } ], [ "items-end", {
        "align-items": "flex-end"
    } ], [ "items-center", {
        "align-items": "center"
    } ], [ "items-baseline", {
        "align-items": "baseline"
    } ], [ "items-stretch", {
        "align-items": "stretch"
    } ], ...h("items", "align-items"), [ "self-auto", {
        "align-self": "auto"
    } ], [ "self-start", {
        "align-self": "flex-start"
    } ], [ "self-end", {
        "align-self": "flex-end"
    } ], [ "self-center", {
        "align-self": "center"
    } ], [ "self-stretch", {
        "align-self": "stretch"
    } ], [ "self-baseline", {
        "align-self": "baseline"
    } ], ...h("self", "align-self") ], wr = [ [ "place-content-center", {
        "place-content": "center"
    } ], [ "place-content-start", {
        "place-content": "start"
    } ], [ "place-content-end", {
        "place-content": "end"
    } ], [ "place-content-between", {
        "place-content": "space-between"
    } ], [ "place-content-around", {
        "place-content": "space-around"
    } ], [ "place-content-evenly", {
        "place-content": "space-evenly"
    } ], [ "place-content-stretch", {
        "place-content": "stretch"
    } ], ...h("place-content"), [ "place-items-start", {
        "place-items": "start"
    } ], [ "place-items-end", {
        "place-items": "end"
    } ], [ "place-items-center", {
        "place-items": "center"
    } ], [ "place-items-stretch", {
        "place-items": "stretch"
    } ], ...h("place-items"), [ "place-self-auto", {
        "place-self": "auto"
    } ], [ "place-self-start", {
        "place-self": "start"
    } ], [ "place-self-end", {
        "place-self": "end"
    } ], [ "place-self-center", {
        "place-self": "center"
    } ], [ "place-self-stretch", {
        "place-self": "stretch"
    } ], ...h("place-self") ], zr = [ ...yr, ...kr, ...wr ].flatMap(([ e, t ]) => [ [ `flex-${e}`, t ], [ `grid-${e}`, t ] ]);
    function Er(e, {
        theme: t
    }) {
        return t.spacing?.[e] ?? v.bracket.cssvar.global.auto.fraction.rem(e);
    }
    function jr([ , e, t ], r) {
        let n = Er(t, r);
        if (n != null && e in Pe) return Pe[e].map(e => [ e.slice(1), n ]);
    }
    var Sr = [ [ /^(?:position-|pos-)?inset-(.+)$/, ([ , e ], t) => ({
        inset: Er(e, t)
    }), {
        autocomplete: [ "(position|pos)-inset-<directions>-$spacing", "(position|pos)-inset-(block|inline)-$spacing", "(position|pos)-inset-(bs|be|is|ie)-$spacing", "(position|pos)-(top|left|right|bottom)-$spacing" ]
    } ], [ /^(?:position-|pos-)?(start|end)-(.+)$/, jr ], [ /^(?:position-|pos-)?inset-([xy])-(.+)$/, jr ], [ /^(?:position-|pos-)?inset-([rltbse])-(.+)$/, jr ], [ /^(?:position-|pos-)?inset-(block|inline)-(.+)$/, jr ], [ /^(?:position-|pos-)?inset-([bi][se])-(.+)$/, jr ], [ /^(?:position-|pos-)?(top|left|right|bottom)-(.+)$/, ([ , e, t ], r) => ({
        [e]: Er(t, r)
    }) ] ], Cr = [ [ "float-left", {
        "float": "left"
    } ], [ "float-right", {
        "float": "right"
    } ], [ "float-start", {
        "float": "inline-start"
    } ], [ "float-end", {
        "float": "inline-end"
    } ], [ "float-none", {
        "float": "none"
    } ], ...h("float"), [ "clear-left", {
        clear: "left"
    } ], [ "clear-right", {
        clear: "right"
    } ], [ "clear-both", {
        clear: "both"
    } ], [ "clear-start", {
        clear: "inline-start"
    } ], [ "clear-end", {
        clear: "inline-end"
    } ], [ "clear-none", {
        clear: "none"
    } ], ...h("clear") ], Fr = [ [ /^(?:position-|pos-)?z([\d.]+)$/, ([ , e ]) => ({
        "z-index": v.number(e)
    }) ], [ /^(?:position-|pos-)?z-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "z-index": t.zIndex?.[e] ?? v.bracket.cssvar.global.auto.number(e)
    }), {
        autocomplete: "z-<num>"
    } ] ], Ar = [ [ "box-border", {
        "box-sizing": "border-box"
    } ], [ "box-content", {
        "box-sizing": "content-box"
    } ], ...h("box", "box-sizing") ];
    var Lr = [ [ /^(where|\?)$/, (e, {
        constructCSS: t,
        generator: r
    }) => {
        if (r.userConfig.envMode === "dev") return `@keyframes __un_qm{0%{box-shadow:inset 4px 4px #ff1e90, inset -4px -4px #ff1e90}100%{box-shadow:inset 8px 8px #3399ff, inset -8px -8px #3399ff}} ${t({
            animation: "__un_qm 0.5s ease-in-out alternate infinite"
        })}`;
    } ] ];
    var _r = [ "auto", "default", "none", "context-menu", "help", "pointer", "progress", "wait", "cell", "crosshair", "text", "vertical-text", "alias", "copy", "move", "no-drop", "not-allowed", "grab", "grabbing", "all-scroll", "col-resize", "row-resize", "n-resize", "e-resize", "s-resize", "w-resize", "ne-resize", "nw-resize", "se-resize", "sw-resize", "ew-resize", "ns-resize", "nesw-resize", "nwse-resize", "zoom-in", "zoom-out" ], Nr = [ "none", "strict", "content", "size", "inline-size", "layout", "style", "paint" ], w = " ", Wr = [ [ "inline", {
        display: "inline"
    } ], [ "block", {
        display: "block"
    } ], [ "inline-block", {
        display: "inline-block"
    } ], [ "contents", {
        display: "contents"
    } ], [ "flow-root", {
        display: "flow-root"
    } ], [ "list-item", {
        display: "list-item"
    } ], [ "hidden", {
        display: "none"
    } ], [ /^display-(.+)$/, ([ , e ]) => ({
        display: v.bracket.cssvar.global(e)
    }) ] ], Ur = [ [ "visible", {
        visibility: "visible"
    } ], [ "invisible", {
        visibility: "hidden"
    } ], [ "backface-visible", {
        "backface-visibility": "visible"
    } ], [ "backface-hidden", {
        "backface-visibility": "hidden"
    } ], ...h("backface", "backface-visibility") ], Or = [ [ /^cursor-(.+)$/, ([ , e ]) => ({
        cursor: v.bracket.cssvar.global(e)
    }) ], ..._r.map(e => [ `cursor-${e}`, {
        cursor: e
    } ]) ], Tr = [ [ /^contain-(.*)$/, ([ , e ]) => v.bracket(e) != null ? {
        contain: v.bracket(e).split(" ").map(e => v.cssvar.fraction(e) ?? e).join(" ")
    } : Nr.includes(e) ? {
        contain: e
    } : void 0 ] ], Rr = [ [ "pointer-events-auto", {
        "pointer-events": "auto"
    } ], [ "pointer-events-none", {
        "pointer-events": "none"
    } ], ...h("pointer-events") ], Dr = [ [ "resize-x", {
        resize: "horizontal"
    } ], [ "resize-y", {
        resize: "vertical"
    } ], [ "resize", {
        resize: "both"
    } ], [ "resize-none", {
        resize: "none"
    } ], ...h("resize") ], Br = [ [ "select-auto", {
        "-webkit-user-select": "auto",
        "user-select": "auto"
    } ], [ "select-all", {
        "-webkit-user-select": "all",
        "user-select": "all"
    } ], [ "select-text", {
        "-webkit-user-select": "text",
        "user-select": "text"
    } ], [ "select-none", {
        "-webkit-user-select": "none",
        "user-select": "none"
    } ], ...h("select", "user-select") ], Xr = [ [ /^(?:whitespace-|ws-)([-\w]+)$/, ([ , e ]) => [ "normal", "nowrap", "pre", "pre-line", "pre-wrap", "break-spaces", ...i ].includes(e) ? {
        "white-space": e
    } : void 0, {
        autocomplete: "(whitespace|ws)-(normal|nowrap|pre|pre-line|pre-wrap|break-spaces)"
    } ] ], Yr = [ [ /^intrinsic-size-(.+)$/, ([ , e ]) => ({
        "contain-intrinsic-size": v.bracket.cssvar.global.fraction.rem(e)
    }), {
        autocomplete: "intrinsic-size-<num>"
    } ], [ "content-visibility-visible", {
        "content-visibility": "visible"
    } ], [ "content-visibility-hidden", {
        "content-visibility": "hidden"
    } ], [ "content-visibility-auto", {
        "content-visibility": "auto"
    } ], ...h("content-visibility") ], Pr = [ [ /^content-(.+)$/, ([ , e ]) => ({
        content: v.bracket.cssvar(e)
    }) ], [ "content-empty", {
        content: '""'
    } ], [ "content-none", {
        content: "none"
    } ] ], Ir = [ [ "break-normal", {
        "overflow-wrap": "normal",
        "word-break": "normal"
    } ], [ "break-words", {
        "overflow-wrap": "break-word"
    } ], [ "break-all", {
        "word-break": "break-all"
    } ], [ "break-keep", {
        "word-break": "keep-all"
    } ], [ "break-anywhere", {
        "overflow-wrap": "anywhere"
    } ] ], qr = [ [ "text-wrap", {
        "text-wrap": "wrap"
    } ], [ "text-nowrap", {
        "text-wrap": "nowrap"
    } ], [ "text-balance", {
        "text-wrap": "balance"
    } ], [ "text-pretty", {
        "text-wrap": "pretty"
    } ] ], Mr = [ [ "truncate", {
        overflow: "hidden",
        "text-overflow": "ellipsis",
        "white-space": "nowrap"
    } ], [ "text-truncate", {
        overflow: "hidden",
        "text-overflow": "ellipsis",
        "white-space": "nowrap"
    } ], [ "text-ellipsis", {
        "text-overflow": "ellipsis"
    } ], [ "text-clip", {
        "text-overflow": "clip"
    } ] ], Zr = [ [ "case-upper", {
        "text-transform": "uppercase"
    } ], [ "case-lower", {
        "text-transform": "lowercase"
    } ], [ "case-capital", {
        "text-transform": "capitalize"
    } ], [ "case-normal", {
        "text-transform": "none"
    } ], ...h("case", "text-transform") ], Hr = [ [ "italic", {
        "font-style": "italic"
    } ], [ "not-italic", {
        "font-style": "normal"
    } ], [ "font-italic", {
        "font-style": "italic"
    } ], [ "font-not-italic", {
        "font-style": "normal"
    } ], [ "oblique", {
        "font-style": "oblique"
    } ], [ "not-oblique", {
        "font-style": "normal"
    } ], [ "font-oblique", {
        "font-style": "oblique"
    } ], [ "font-not-oblique", {
        "font-style": "normal"
    } ] ], Gr = [ [ "antialiased", {
        "-webkit-font-smoothing": "antialiased",
        "-moz-osx-font-smoothing": "grayscale"
    } ], [ "subpixel-antialiased", {
        "-webkit-font-smoothing": "auto",
        "-moz-osx-font-smoothing": "auto"
    } ] ];
    var Jr = {
        "--un-ring-inset": w,
        "--un-ring-offset-width": "0px",
        "--un-ring-offset-color": "#fff",
        "--un-ring-width": "0px",
        "--un-ring-color": "rgb(147 197 253 / 0.5)",
        "--un-shadow": "0 0 rgb(0 0 0 / 0)"
    }, Kr = [ [ /^ring(?:-(.+))?$/, ([ , e ], {
        theme: t
    }) => {
        let r = t.ringWidth?.[e || "DEFAULT"] ?? v.px(e || "1");
        if (r) return {
            "--un-ring-width": r,
            "--un-ring-offset-shadow": "var(--un-ring-inset) 0 0 0 var(--un-ring-offset-width) var(--un-ring-offset-color)",
            "--un-ring-shadow": "var(--un-ring-inset) 0 0 0 calc(var(--un-ring-width) + var(--un-ring-offset-width)) var(--un-ring-color)",
            "box-shadow": "var(--un-ring-offset-shadow), var(--un-ring-shadow), var(--un-shadow)"
        };
    }, {
        autocomplete: "ring-$ringWidth"
    } ], [ /^ring-(?:width-|size-)(.+)$/, Vr, {
        autocomplete: "ring-(width|size)-$lineWidth"
    } ], [ "ring-offset", {
        "--un-ring-offset-width": "1px"
    } ], [ /^ring-offset-(?:width-|size-)?(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "--un-ring-offset-width": t.lineWidth?.[e] ?? v.bracket.cssvar.px(e)
    }), {
        autocomplete: "ring-offset-(width|size)-$lineWidth"
    } ], [ /^ring-(.+)$/, Qr, {
        autocomplete: "ring-$colors"
    } ], [ /^ring-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-ring-opacity": v.bracket.percent.cssvar(e)
    }), {
        autocomplete: "ring-(op|opacity)-<percent>"
    } ], [ /^ring-offset-(.+)$/, g("--un-ring-offset-color", "ring-offset", "borderColor"), {
        autocomplete: "ring-offset-$colors"
    } ], [ /^ring-offset-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-ring-offset-opacity": v.bracket.percent.cssvar(e)
    }), {
        autocomplete: "ring-offset-(op|opacity)-<percent>"
    } ], [ "ring-inset", {
        "--un-ring-inset": "inset"
    } ] ];
    function Vr([ , e ], {
        theme: t
    }) {
        return {
            "--un-ring-width": t.ringWidth?.[e] ?? v.bracket.cssvar.px(e)
        };
    }
    function Qr(e, t) {
        return b(v.bracket(e[1])) ? Vr(e, t) : g("--un-ring-color", "ring", "borderColor")(e, t);
    }
    var en = {
        "--un-ring-offset-shadow": "0 0 rgb(0 0 0 / 0)",
        "--un-ring-shadow": "0 0 rgb(0 0 0 / 0)",
        "--un-shadow-inset": w,
        "--un-shadow": "0 0 rgb(0 0 0 / 0)"
    }, tn = [ [ /^shadow(?:-(.+))?$/, (e, t) => {
        let [ , r ] = e, {
            theme: n
        } = t, i = n.boxShadow?.[r || "DEFAULT"], o = r ? v.bracket.cssvar(r) : void 0;
        return (i != null || o != null) && !_t(o, n, "shadowColor") ? {
            "--un-shadow": Lt(i || o, "--un-shadow-color").join(","),
            "box-shadow": "var(--un-ring-offset-shadow), var(--un-ring-shadow), var(--un-shadow)"
        } : g("--un-shadow-color", "shadow", "shadowColor")(e, t);
    }, {
        autocomplete: [ "shadow-$colors", "shadow-$boxShadow" ]
    } ], [ /^shadow-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-shadow-opacity": v.bracket.percent.cssvar(e)
    }), {
        autocomplete: "shadow-(op|opacity)-<percent>"
    } ], [ "shadow-inset", {
        "--un-shadow-inset": "inset"
    } ] ];
    var rn = {
        h: "height",
        w: "width",
        inline: "inline-size",
        block: "block-size"
    };
    function z(e, t) {
        return `${e || ""}${rn[t]}`;
    }
    function nn(e, t, r, n) {
        let i = z(e, t).replace(/-(\w)/g, (e, t) => t.toUpperCase()), o = r[i]?.[n];
        if (o != null) return o;
        switch (n) {
          case "fit":
          case "max":
          case "min":
            return `${n}-content`;
        }
        return v.bracket.cssvar.global.auto.fraction.rem(n);
    }
    var on = [ [ /^size-(min-|max-)?(.+)$/, ([ , e, t ], {
        theme: r
    }) => ({
        [z(e, "w")]: nn(e, "w", r, t),
        [z(e, "h")]: nn(e, "h", r, t)
    }) ], [ /^(?:size-)?(min-|max-)?([wh])-?(.+)$/, ([ , e, t, r ], {
        theme: n
    }) => ({
        [z(e, t)]: nn(e, t, n, r)
    }) ], [ /^(?:size-)?(min-|max-)?(block|inline)-(.+)$/, ([ , e, t, r ], {
        theme: n
    }) => ({
        [z(e, t)]: nn(e, t, n, r)
    }), {
        autocomplete: [ "(w|h)-$width|height|maxWidth|maxHeight|minWidth|minHeight|inlineSize|blockSize|maxInlineSize|maxBlockSize|minInlineSize|minBlockSize", "(block|inline)-$width|height|maxWidth|maxHeight|minWidth|minHeight|inlineSize|blockSize|maxInlineSize|maxBlockSize|minInlineSize|minBlockSize", "(max|min)-(w|h|block|inline)", "(max|min)-(w|h|block|inline)-$width|height|maxWidth|maxHeight|minWidth|minHeight|inlineSize|blockSize|maxInlineSize|maxBlockSize|minInlineSize|minBlockSize", "(w|h)-full", "(max|min)-(w|h)-full" ]
    } ], [ /^(?:size-)?(min-|max-)?(h)-screen-(.+)$/, ([ , e, t, r ], n) => ({
        [z(e, t)]: an(n, r, "verticalBreakpoints")
    }) ], [ /^(?:size-)?(min-|max-)?(w)-screen-(.+)$/, ([ , e, t, r ], n) => ({
        [z(e, t)]: an(n, r)
    }), {
        autocomplete: [ "(w|h)-screen", "(min|max)-(w|h)-screen", "h-screen-$verticalBreakpoints", "(min|max)-h-screen-$verticalBreakpoints", "w-screen-$breakpoints", "(min|max)-w-screen-$breakpoints" ]
    } ] ];
    function an(e, t, r = "breakpoints") {
        let n = Ut(e, r);
        if (n) return n.find(e => e.point === t)?.size;
    }
    function sn(e) {
        if (/^\d+\/\d+$/.test(e)) return e;
        switch (e) {
          case "square":
            return "1/1";

          case "video":
            return "16/9";
        }
        return v.bracket.cssvar.global.auto.number(e);
    }
    var ln = [ [ /^(?:size-)?aspect-(?:ratio-)?(.+)$/, ([ , e ]) => ({
        "aspect-ratio": sn(e)
    }), {
        autocomplete: [ "aspect-(square|video|ratio)", "aspect-ratio-(square|video)" ]
    } ] ];
    var cn = [ [ /^pa?()-?(.+)$/, e("padding"), {
        autocomplete: [ "(m|p)<num>", "(m|p)-<num>" ]
    } ], [ /^p-?xy()()$/, e("padding"), {
        autocomplete: "(m|p)-(xy)"
    } ], [ /^p-?([xy])(?:-?(.+))?$/, e("padding") ], [ /^p-?([rltbse])(?:-?(.+))?$/, e("padding"), {
        autocomplete: "(m|p)<directions>-<num>"
    } ], [ /^p-(block|inline)(?:-(.+))?$/, e("padding"), {
        autocomplete: "(m|p)-(block|inline)-<num>"
    } ], [ /^p-?([bi][se])(?:-?(.+))?$/, e("padding"), {
        autocomplete: "(m|p)-(bs|be|is|ie)-<num>"
    } ] ], fn = [ [ /^ma?()-?(.+)$/, e("margin") ], [ /^m-?xy()()$/, e("margin") ], [ /^m-?([xy])(?:-?(.+))?$/, e("margin") ], [ /^m-?([rltbse])(?:-?(.+))?$/, e("margin") ], [ /^m-(block|inline)(?:-(.+))?$/, e("margin") ], [ /^m-?([bi][se])(?:-?(.+))?$/, e("margin") ] ];
    var dn = [ [ /^fill-(.+)$/, g("fill", "fill", "backgroundColor"), {
        autocomplete: "fill-$colors"
    } ], [ /^fill-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-fill-opacity": v.bracket.percent.cssvar(e)
    }), {
        autocomplete: "fill-(op|opacity)-<percent>"
    } ], [ "fill-none", {
        fill: "none"
    } ], [ /^stroke-(?:width-|size-)?(.+)$/, un, {
        autocomplete: [ "stroke-width-$lineWidth", "stroke-size-$lineWidth" ]
    } ], [ /^stroke-dash-(.+)$/, ([ , e ]) => ({
        "stroke-dasharray": v.bracket.cssvar.number(e)
    }), {
        autocomplete: "stroke-dash-<num>"
    } ], [ /^stroke-offset-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "stroke-dashoffset": t.lineWidth?.[e] ?? v.bracket.cssvar.px.numberWithUnit(e)
    }), {
        autocomplete: "stroke-offset-$lineWidth"
    } ], [ /^stroke-(.+)$/, pn, {
        autocomplete: "stroke-$colors"
    } ], [ /^stroke-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-stroke-opacity": v.bracket.percent.cssvar(e)
    }), {
        autocomplete: "stroke-(op|opacity)-<percent>"
    } ], [ "stroke-cap-square", {
        "stroke-linecap": "square"
    } ], [ "stroke-cap-round", {
        "stroke-linecap": "round"
    } ], [ "stroke-cap-auto", {
        "stroke-linecap": "butt"
    } ], [ "stroke-join-arcs", {
        "stroke-linejoin": "arcs"
    } ], [ "stroke-join-bevel", {
        "stroke-linejoin": "bevel"
    } ], [ "stroke-join-clip", {
        "stroke-linejoin": "miter-clip"
    } ], [ "stroke-join-round", {
        "stroke-linejoin": "round"
    } ], [ "stroke-join-auto", {
        "stroke-linejoin": "miter"
    } ], [ "stroke-none", {
        stroke: "none"
    } ] ];
    function un([ , e ], {
        theme: t
    }) {
        return {
            "stroke-width": t.lineWidth?.[e] ?? v.bracket.cssvar.fraction.px.number(e)
        };
    }
    function pn(e, t) {
        return b(v.bracket(e[1])) ? un(e, t) : g("stroke", "stroke", "borderColor")(e, t);
    }
    var mn = [ "translate", "rotate", "scale" ], gn = [ "translateX(var(--un-translate-x))", "translateY(var(--un-translate-y))", "rotate(var(--un-rotate))", "rotateZ(var(--un-rotate-z))", "skewX(var(--un-skew-x))", "skewY(var(--un-skew-y))", "scaleX(var(--un-scale-x))", "scaleY(var(--un-scale-y))" ].join(" "), E = [ "translateX(var(--un-translate-x))", "translateY(var(--un-translate-y))", "translateZ(var(--un-translate-z))", "rotate(var(--un-rotate))", "rotateX(var(--un-rotate-x))", "rotateY(var(--un-rotate-y))", "rotateZ(var(--un-rotate-z))", "skewX(var(--un-skew-x))", "skewY(var(--un-skew-y))", "scaleX(var(--un-scale-x))", "scaleY(var(--un-scale-y))", "scaleZ(var(--un-scale-z))" ].join(" "), hn = [ "translate3d(var(--un-translate-x), var(--un-translate-y), var(--un-translate-z))", "rotate(var(--un-rotate))", "rotateX(var(--un-rotate-x))", "rotateY(var(--un-rotate-y))", "rotateZ(var(--un-rotate-z))", "skewX(var(--un-skew-x))", "skewY(var(--un-skew-y))", "scaleX(var(--un-scale-x))", "scaleY(var(--un-scale-y))", "scaleZ(var(--un-scale-z))" ].join(" "), bn = {
        "--un-rotate": 0,
        "--un-rotate-x": 0,
        "--un-rotate-y": 0,
        "--un-rotate-z": 0,
        "--un-scale-x": 1,
        "--un-scale-y": 1,
        "--un-scale-z": 1,
        "--un-skew-x": 0,
        "--un-skew-y": 0,
        "--un-translate-x": 0,
        "--un-translate-y": 0,
        "--un-translate-z": 0
    }, xn = [ [ /^(?:transform-)?origin-(.+)$/, ([ , e ]) => ({
        "transform-origin": r[e] ?? v.bracket.cssvar(e)
    }), {
        autocomplete: [ `transform-origin-(${Object.keys(r).join("|")})`, `origin-(${Object.keys(r).join("|")})` ]
    } ], [ /^(?:transform-)?perspect(?:ive)?-(.+)$/, ([ , e ]) => {
        let t = v.bracket.cssvar.px.numberWithUnit(e);
        if (t != null) return {
            "-webkit-perspective": t,
            perspective: t
        };
    } ], [ /^(?:transform-)?perspect(?:ive)?-origin-(.+)$/, ([ , e ]) => {
        let t = v.bracket.cssvar(e) ?? (e.length >= 3 ? r[e] : void 0);
        if (t != null) return {
            "-webkit-perspective-origin": t,
            "perspective-origin": t
        };
    } ], [ /^(?:transform-)?translate-()(.+)$/, vn ], [ /^(?:transform-)?translate-([xyz])-(.+)$/, vn ], [ /^(?:transform-)?rotate-()(.+)$/, $n ], [ /^(?:transform-)?rotate-([xyz])-(.+)$/, $n ], [ /^(?:transform-)?skew-()(.+)$/, kn ], [ /^(?:transform-)?skew-([xy])-(.+)$/, kn, {
        autocomplete: [ "transform-skew-(x|y)-<percent>", "skew-(x|y)-<percent>" ]
    } ], [ /^(?:transform-)?scale-()(.+)$/, yn ], [ /^(?:transform-)?scale-([xyz])-(.+)$/, yn, {
        autocomplete: [ `transform-(${mn.join("|")})-<percent>`, `transform-(${mn.join("|")})-(x|y|z)-<percent>`, `(${mn.join("|")})-<percent>`, `(${mn.join("|")})-(x|y|z)-<percent>` ]
    } ], [ /^(?:transform-)?preserve-3d$/, () => ({
        "transform-style": "preserve-3d"
    }) ], [ /^(?:transform-)?preserve-flat$/, () => ({
        "transform-style": "flat"
    }) ], [ "transform", {
        transform: E
    } ], [ "transform-cpu", {
        transform: gn
    } ], [ "transform-gpu", {
        transform: hn
    } ], [ "transform-none", {
        transform: "none"
    } ], ...h("transform") ];
    function vn([ , e, t ], {
        theme: r
    }) {
        let n = r.spacing?.[t] ?? v.bracket.cssvar.fraction.rem(t);
        if (n != null) return [ ...Tt(e, n, "translate"), [ "transform", E ] ];
    }
    function yn([ , e, t ]) {
        let r = v.bracket.cssvar.fraction.percent(t);
        if (r != null) return [ ...Tt(e, r, "scale"), [ "transform", E ] ];
    }
    function $n([ , e = "", t ]) {
        let r = v.bracket.cssvar.degree(t);
        if (r != null) return e ? {
            "--un-rotate": 0,
            [`--un-rotate-${e}`]: r,
            transform: E
        } : {
            "--un-rotate-x": 0,
            "--un-rotate-y": 0,
            "--un-rotate-z": 0,
            "--un-rotate": r,
            transform: E
        };
    }
    function kn([ , e, t ]) {
        let r = v.bracket.cssvar.degree(t);
        if (r != null) return [ ...Tt(e, r, "skew"), [ "transform", E ] ];
    }
    function wn(t, r) {
        let n;
        if (v.cssvar(t) != null) n = v.cssvar(t); else {
            t.startsWith("[") && t.endsWith("]") && (t = t.slice(1, -1));
            let e = t.split(",").map(e => r.transitionProperty?.[e] ?? v.properties(e));
            e.every(Boolean) && (n = e.join(","));
        }
        return n;
    }
    var zn = [ [ /^transition(?:-(\D+?))?(?:-(\d+))?$/, ([ , r, n ], {
        theme: i
    }) => {
        if (!r && !n) return {
            "transition-property": i.transitionProperty?.DEFAULT,
            "transition-timing-function": i.easing?.DEFAULT,
            "transition-duration": i.duration?.DEFAULT ?? v.time("150")
        };
        if (r != null) {
            let e = wn(r, i), t = i.duration?.[n || "DEFAULT"] ?? v.time(n || "150");
            if (e) return {
                "transition-property": e,
                "transition-timing-function": i.easing?.DEFAULT,
                "transition-duration": t
            };
        } else if (n != null) return {
            "transition-property": i.transitionProperty?.DEFAULT,
            "transition-timing-function": i.easing?.DEFAULT,
            "transition-duration": i.duration?.[n] ?? v.time(n)
        };
    }, {
        autocomplete: "transition-$transitionProperty-$duration"
    } ], [ /^(?:transition-)?duration-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "transition-duration": t.duration?.[e || "DEFAULT"] ?? v.bracket.cssvar.time(e)
    }), {
        autocomplete: [ "transition-duration-$duration", "duration-$duration" ]
    } ], [ /^(?:transition-)?delay-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "transition-delay": t.duration?.[e || "DEFAULT"] ?? v.bracket.cssvar.time(e)
    }), {
        autocomplete: [ "transition-delay-$duration", "delay-$duration" ]
    } ], [ /^(?:transition-)?ease(?:-(.+))?$/, ([ , e ], {
        theme: t
    }) => ({
        "transition-timing-function": t.easing?.[e || "DEFAULT"] ?? v.bracket.cssvar(e)
    }), {
        autocomplete: [ "transition-ease-(linear|in|out|in-out|DEFAULT)", "ease-(linear|in|out|in-out|DEFAULT)" ]
    } ], [ /^(?:transition-)?property-(.+)$/, ([ , e ], {
        theme: t
    }) => {
        let r = v.global(e) || wn(e, t);
        if (r) return {
            "transition-property": r
        };
    }, {
        autocomplete: [ `transition-property-(${[ ...i ].join("|")})`, "transition-property-$transitionProperty", "property-$transitionProperty" ]
    } ], [ "transition-none", {
        transition: "none"
    } ], ...h("transition"), [ "transition-discrete", {
        "transition-behavior": "allow-discrete"
    } ], [ "transition-normal", {
        "transition-behavior": "normal"
    } ] ];
    var En = [ [ /^text-(.+)$/, Nn, {
        autocomplete: "text-$fontSize"
    } ], [ /^(?:text|font)-size-(.+)$/, Ln, {
        autocomplete: "text-size-$fontSize"
    } ], [ /^text-(?:color-)?(.+)$/, _n, {
        autocomplete: "text-$colors"
    } ], [ /^(?:color|c)-(.+)$/, g("color", "text", "textColor"), {
        autocomplete: "(color|c)-$colors"
    } ], [ /^(?:text|color|c)-(.+)$/, ([ , e ]) => i.includes(e) ? {
        color: e
    } : void 0, {
        autocomplete: `(text|color|c)-(${i.join("|")})`
    } ], [ /^(?:text|color|c)-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-text-opacity": v.bracket.percent.cssvar(e)
    }), {
        autocomplete: "(text|color|c)-(op|opacity)-<percent>"
    } ], [ /^(?:font|fw)-?([^-]+)$/, ([ , e ], {
        theme: t
    }) => ({
        "font-weight": t.fontWeight?.[e] || v.bracket.global.number(e)
    }), {
        autocomplete: [ "(font|fw)-(100|200|300|400|500|600|700|800|900)", "(font|fw)-$fontWeight" ]
    } ], [ /^(?:font-)?(?:leading|lh|line-height)-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "line-height": An(e, t, "lineHeight")
    }), {
        autocomplete: "(leading|lh|line-height)-$lineHeight"
    } ], [ "font-synthesis-weight", {
        "font-synthesis": "weight"
    } ], [ "font-synthesis-style", {
        "font-synthesis": "style"
    } ], [ "font-synthesis-small-caps", {
        "font-synthesis": "small-caps"
    } ], [ "font-synthesis-none", {
        "font-synthesis": "none"
    } ], [ /^font-synthesis-(.+)$/, ([ , e ]) => ({
        "font-synthesis": v.bracket.cssvar.global(e)
    }) ], [ /^(?:font-)?tracking-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "letter-spacing": t.letterSpacing?.[e] || v.bracket.cssvar.global.rem(e)
    }), {
        autocomplete: "tracking-$letterSpacing"
    } ], [ /^(?:font-)?word-spacing-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "word-spacing": t.wordSpacing?.[e] || v.bracket.cssvar.global.rem(e)
    }), {
        autocomplete: "word-spacing-$wordSpacing"
    } ], [ "font-stretch-normal", {
        "font-stretch": "normal"
    } ], [ "font-stretch-ultra-condensed", {
        "font-stretch": "ultra-condensed"
    } ], [ "font-stretch-extra-condensed", {
        "font-stretch": "extra-condensed"
    } ], [ "font-stretch-condensed", {
        "font-stretch": "condensed"
    } ], [ "font-stretch-semi-condensed", {
        "font-stretch": "semi-condensed"
    } ], [ "font-stretch-semi-expanded", {
        "font-stretch": "semi-expanded"
    } ], [ "font-stretch-expanded", {
        "font-stretch": "expanded"
    } ], [ "font-stretch-extra-expanded", {
        "font-stretch": "extra-expanded"
    } ], [ "font-stretch-ultra-expanded", {
        "font-stretch": "ultra-expanded"
    } ], [ /^font-stretch-(.+)$/, ([ , e ]) => ({
        "font-stretch": v.bracket.cssvar.fraction.global(e)
    }), {
        autocomplete: "font-stretch-<percentage>"
    } ], [ /^font-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "font-family": t.fontFamily?.[e] || v.bracket.cssvar.global(e)
    }), {
        autocomplete: "font-$fontFamily"
    } ] ], jn = [ [ /^tab(?:-(.+))?$/, ([ , e ]) => {
        let t = v.bracket.cssvar.global.number(e || "4");
        if (t != null) return {
            "-moz-tab-size": t,
            "-o-tab-size": t,
            "tab-size": t
        };
    } ] ], Sn = [ [ /^indent(?:-(.+))?$/, ([ , e ], {
        theme: t
    }) => ({
        "text-indent": t.textIndent?.[e || "DEFAULT"] || v.bracket.cssvar.global.fraction.rem(e)
    }), {
        autocomplete: "indent-$textIndent"
    } ] ], Cn = [ [ /^text-stroke(?:-(.+))?$/, ([ , e ], {
        theme: t
    }) => ({
        "-webkit-text-stroke-width": t.textStrokeWidth?.[e || "DEFAULT"] || v.bracket.cssvar.px(e)
    }), {
        autocomplete: "text-stroke-$textStrokeWidth"
    } ], [ /^text-stroke-(.+)$/, g("-webkit-text-stroke-color", "text-stroke", "borderColor"), {
        autocomplete: "text-stroke-$colors"
    } ], [ /^text-stroke-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-text-stroke-opacity": v.bracket.percent.cssvar(e)
    }), {
        autocomplete: "text-stroke-(op|opacity)-<percent>"
    } ] ], Fn = [ [ /^text-shadow(?:-(.+))?$/, ([ , e ], {
        theme: t
    }) => {
        let r = t.textShadow?.[e || "DEFAULT"];
        return r != null ? {
            "--un-text-shadow": Lt(r, "--un-text-shadow-color").join(","),
            "text-shadow": "var(--un-text-shadow)"
        } : {
            "text-shadow": v.bracket.cssvar.global(e)
        };
    }, {
        autocomplete: "text-shadow-$textShadow"
    } ], [ /^text-shadow-color-(.+)$/, g("--un-text-shadow-color", "text-shadow", "shadowColor"), {
        autocomplete: "text-shadow-color-$colors"
    } ], [ /^text-shadow-color-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-text-shadow-opacity": v.bracket.percent.cssvar(e)
    }), {
        autocomplete: "text-shadow-color-(op|opacity)-<percent>"
    } ] ];
    function An(e, t, r) {
        return t[r]?.[e] || v.bracket.cssvar.global.rem(e);
    }
    function Ln([ , e ], {
        theme: t
    }) {
        let r = c(t.fontSize?.[e])?.[0] ?? v.bracket.cssvar.global.rem(e);
        if (r != null) return {
            "font-size": r
        };
    }
    function _n(e, t) {
        return b(v.bracket(e[1])) ? Ln(e, t) : g("color", "text", "textColor")(e, t);
    }
    function Nn([ , e = "base" ], {
        theme: n
    }) {
        let t = Ft(e, "length");
        if (!t) return;
        let [ r, i ] = t, o = c(n.fontSize?.[r]), a = i ? An(i, n, "lineHeight") : void 0;
        if (o?.[0]) {
            let [ e, t, r ] = o;
            return typeof t == "object" ? {
                "font-size": e,
                ...t
            } : {
                "font-size": e,
                "line-height": a ?? t ?? "1",
                "letter-spacing": r ? An(r, n, "letterSpacing") : void 0
            };
        }
        let s = v.bracketOfLength.rem(r);
        return a && s ? {
            "font-size": s,
            "line-height": a
        } : {
            "font-size": v.bracketOfLength.rem(e)
        };
    }
    var Wn = {
        backface: "backface-visibility",
        "break": "word-break",
        "case": "text-transform",
        content: "align-content",
        fw: "font-weight",
        items: "align-items",
        justify: "justify-content",
        select: "user-select",
        self: "align-self",
        vertical: "vertical-align",
        visible: "visibility",
        whitespace: "white-space",
        ws: "white-space"
    }, Un = [ [ /^(.+?)-(\$.+)$/, ([ , e, t ]) => {
        let r = Wn[e];
        if (r) return {
            [r]: v.cssvar(t)
        };
    } ] ], On = [ [ /^\[(.*)\]$/, ([ e, t ]) => {
        if (!t.includes(":")) return;
        let [ r, ...n ] = t.split(":"), i = n.join(":");
        if (!Rn(t) && /^[a-z-]+$/.test(r) && Tn(i)) {
            let e = v.bracket(`[${i}]`);
            if (e) return {
                [r]: e
            };
        }
    } ] ];
    function Tn(t) {
        let r = 0;
        function n(e) {
            for (;r < t.length; ) if (r += 1, t[r] === e) return !0;
            return !1;
        }
        for (r = 0; r < t.length; r++) {
            let e = t[r];
            if ("\"`'".includes(e)) {
                if (!n(e)) return !1;
            } else if (e === "(") {
                if (!n(")")) return !1;
            } else if ("[]{}:".includes(e)) return !1;
        }
        return !0;
    }
    function Rn(e) {
        if (!e.includes("://")) return !1;
        try {
            return new URL(e).host !== "";
        } catch {
            return !1;
        }
    }
    var Dn = [ Un, On, Tr, Rr, Ur, vr, Sr, Fr, $r, hr, Cr, fn, Ar, Wr, ln, on, fr, xn, Or, Br, Dr, Xt, wr, kr, yr, pr, zr, xr, Mr, Xr, Ir, It, nr, ir, dn, cn, Et, Sn, qr, wt, En, Zr, Hr, sr, Gr, jn, Cn, Fn, Kt, tn, Rt, Kr, zn, Pt, Yr, Pr, or, Lr ].flat(1);
    var Bn = {
        position: [ "relative", "absolute", "fixed", "sticky", "static" ],
        globalKeyword: i
    };
    var Xn = {
        inherit: "inherit",
        current: "currentColor",
        transparent: "transparent",
        black: "#000",
        white: "#fff",
        rose: {
            50: "#fff1f2",
            100: "#ffe4e6",
            200: "#fecdd3",
            300: "#fda4af",
            400: "#fb7185",
            500: "#f43f5e",
            600: "#e11d48",
            700: "#be123c",
            800: "#9f1239",
            900: "#881337",
            950: "#4c0519"
        },
        pink: {
            50: "#fdf2f8",
            100: "#fce7f3",
            200: "#fbcfe8",
            300: "#f9a8d4",
            400: "#f472b6",
            500: "#ec4899",
            600: "#db2777",
            700: "#be185d",
            800: "#9d174d",
            900: "#831843",
            950: "#500724"
        },
        fuchsia: {
            50: "#fdf4ff",
            100: "#fae8ff",
            200: "#f5d0fe",
            300: "#f0abfc",
            400: "#e879f9",
            500: "#d946ef",
            600: "#c026d3",
            700: "#a21caf",
            800: "#86198f",
            900: "#701a75",
            950: "#4a044e"
        },
        purple: {
            50: "#faf5ff",
            100: "#f3e8ff",
            200: "#e9d5ff",
            300: "#d8b4fe",
            400: "#c084fc",
            500: "#a855f7",
            600: "#9333ea",
            700: "#7e22ce",
            800: "#6b21a8",
            900: "#581c87",
            950: "#3b0764"
        },
        violet: {
            50: "#f5f3ff",
            100: "#ede9fe",
            200: "#ddd6fe",
            300: "#c4b5fd",
            400: "#a78bfa",
            500: "#8b5cf6",
            600: "#7c3aed",
            700: "#6d28d9",
            800: "#5b21b6",
            900: "#4c1d95",
            950: "#2e1065"
        },
        indigo: {
            50: "#eef2ff",
            100: "#e0e7ff",
            200: "#c7d2fe",
            300: "#a5b4fc",
            400: "#818cf8",
            500: "#6366f1",
            600: "#4f46e5",
            700: "#4338ca",
            800: "#3730a3",
            900: "#312e81",
            950: "#1e1b4b"
        },
        blue: {
            50: "#eff6ff",
            100: "#dbeafe",
            200: "#bfdbfe",
            300: "#93c5fd",
            400: "#60a5fa",
            500: "#3b82f6",
            600: "#2563eb",
            700: "#1d4ed8",
            800: "#1e40af",
            900: "#1e3a8a",
            950: "#172554"
        },
        sky: {
            50: "#f0f9ff",
            100: "#e0f2fe",
            200: "#bae6fd",
            300: "#7dd3fc",
            400: "#38bdf8",
            500: "#0ea5e9",
            600: "#0284c7",
            700: "#0369a1",
            800: "#075985",
            900: "#0c4a6e",
            950: "#082f49"
        },
        cyan: {
            50: "#ecfeff",
            100: "#cffafe",
            200: "#a5f3fc",
            300: "#67e8f9",
            400: "#22d3ee",
            500: "#06b6d4",
            600: "#0891b2",
            700: "#0e7490",
            800: "#155e75",
            900: "#164e63",
            950: "#083344"
        },
        teal: {
            50: "#f0fdfa",
            100: "#ccfbf1",
            200: "#99f6e4",
            300: "#5eead4",
            400: "#2dd4bf",
            500: "#14b8a6",
            600: "#0d9488",
            700: "#0f766e",
            800: "#115e59",
            900: "#134e4a",
            950: "#042f2e"
        },
        emerald: {
            50: "#ecfdf5",
            100: "#d1fae5",
            200: "#a7f3d0",
            300: "#6ee7b7",
            400: "#34d399",
            500: "#10b981",
            600: "#059669",
            700: "#047857",
            800: "#065f46",
            900: "#064e3b",
            950: "#022c22"
        },
        green: {
            50: "#f0fdf4",
            100: "#dcfce7",
            200: "#bbf7d0",
            300: "#86efac",
            400: "#4ade80",
            500: "#22c55e",
            600: "#16a34a",
            700: "#15803d",
            800: "#166534",
            900: "#14532d",
            950: "#052e16"
        },
        lime: {
            50: "#f7fee7",
            100: "#ecfccb",
            200: "#d9f99d",
            300: "#bef264",
            400: "#a3e635",
            500: "#84cc16",
            600: "#65a30d",
            700: "#4d7c0f",
            800: "#3f6212",
            900: "#365314",
            950: "#1a2e05"
        },
        yellow: {
            50: "#fefce8",
            100: "#fef9c3",
            200: "#fef08a",
            300: "#fde047",
            400: "#facc15",
            500: "#eab308",
            600: "#ca8a04",
            700: "#a16207",
            800: "#854d0e",
            900: "#713f12",
            950: "#422006"
        },
        amber: {
            50: "#fffbeb",
            100: "#fef3c7",
            200: "#fde68a",
            300: "#fcd34d",
            400: "#fbbf24",
            500: "#f59e0b",
            600: "#d97706",
            700: "#b45309",
            800: "#92400e",
            900: "#78350f",
            950: "#451a03"
        },
        orange: {
            50: "#fff7ed",
            100: "#ffedd5",
            200: "#fed7aa",
            300: "#fdba74",
            400: "#fb923c",
            500: "#f97316",
            600: "#ea580c",
            700: "#c2410c",
            800: "#9a3412",
            900: "#7c2d12",
            950: "#431407"
        },
        red: {
            50: "#fef2f2",
            100: "#fee2e2",
            200: "#fecaca",
            300: "#fca5a5",
            400: "#f87171",
            500: "#ef4444",
            600: "#dc2626",
            700: "#b91c1c",
            800: "#991b1b",
            900: "#7f1d1d",
            950: "#450a0a"
        },
        gray: {
            50: "#f9fafb",
            100: "#f3f4f6",
            200: "#e5e7eb",
            300: "#d1d5db",
            400: "#9ca3af",
            500: "#6b7280",
            600: "#4b5563",
            700: "#374151",
            800: "#1f2937",
            900: "#111827",
            950: "#030712"
        },
        slate: {
            50: "#f8fafc",
            100: "#f1f5f9",
            200: "#e2e8f0",
            300: "#cbd5e1",
            400: "#94a3b8",
            500: "#64748b",
            600: "#475569",
            700: "#334155",
            800: "#1e293b",
            900: "#0f172a",
            950: "#020617"
        },
        zinc: {
            50: "#fafafa",
            100: "#f4f4f5",
            200: "#e4e4e7",
            300: "#d4d4d8",
            400: "#a1a1aa",
            500: "#71717a",
            600: "#52525b",
            700: "#3f3f46",
            800: "#27272a",
            900: "#18181b",
            950: "#09090b"
        },
        neutral: {
            50: "#fafafa",
            100: "#f5f5f5",
            200: "#e5e5e5",
            300: "#d4d4d4",
            400: "#a3a3a3",
            500: "#737373",
            600: "#525252",
            700: "#404040",
            800: "#262626",
            900: "#171717",
            950: "#0a0a0a"
        },
        stone: {
            50: "#fafaf9",
            100: "#f5f5f4",
            200: "#e7e5e4",
            300: "#d6d3d1",
            400: "#a8a29e",
            500: "#78716c",
            600: "#57534e",
            700: "#44403c",
            800: "#292524",
            900: "#1c1917",
            950: "#0c0a09"
        },
        light: {
            50: "#fdfdfd",
            100: "#fcfcfc",
            200: "#fafafa",
            300: "#f8f9fa",
            400: "#f6f6f6",
            500: "#f2f2f2",
            600: "#f1f3f5",
            700: "#e9ecef",
            800: "#dee2e6",
            900: "#dde1e3",
            950: "#d8dcdf"
        },
        dark: {
            50: "#4a4a4a",
            100: "#3c3c3c",
            200: "#323232",
            300: "#2d2d2d",
            400: "#222222",
            500: "#1f1f1f",
            600: "#1c1c1e",
            700: "#1b1b1b",
            800: "#181818",
            900: "#0f0f0f",
            950: "#080808"
        },
        get lightblue() {
            return this.sky;
        },
        get lightBlue() {
            return this.sky;
        },
        get warmgray() {
            return this.stone;
        },
        get warmGray() {
            return this.stone;
        },
        get truegray() {
            return this.neutral;
        },
        get trueGray() {
            return this.neutral;
        },
        get coolgray() {
            return this.gray;
        },
        get coolGray() {
            return this.gray;
        },
        get bluegray() {
            return this.slate;
        },
        get blueGray() {
            return this.slate;
        }
    };
    Object.values(Xn).forEach(r => {
        typeof r != "string" && r !== void 0 && (r.DEFAULT = r.DEFAULT || r[400], 
        Object.keys(r).forEach(e => {
            let t = +e / 100;
            t === Math.round(t) && (r[t] = r[e]);
        }));
    });
    var Yn = {
        DEFAULT: "8px",
        0: "0",
        sm: "4px",
        md: "12px",
        lg: "16px",
        xl: "24px",
        "2xl": "40px",
        "3xl": "64px"
    }, Pn = {
        DEFAULT: [ "0 1px 2px rgb(0 0 0 / 0.1)", "0 1px 1px rgb(0 0 0 / 0.06)" ],
        sm: "0 1px 1px rgb(0 0 0 / 0.05)",
        md: [ "0 4px 3px rgb(0 0 0 / 0.07)", "0 2px 2px rgb(0 0 0 / 0.06)" ],
        lg: [ "0 10px 8px rgb(0 0 0 / 0.04)", "0 4px 3px rgb(0 0 0 / 0.1)" ],
        xl: [ "0 20px 13px rgb(0 0 0 / 0.03)", "0 8px 5px rgb(0 0 0 / 0.08)" ],
        "2xl": "0 25px 25px rgb(0 0 0 / 0.15)",
        none: "0 0 rgb(0 0 0 / 0)"
    };
    var In = {
        sans: [ "ui-sans-serif", "system-ui", "-apple-system", "BlinkMacSystemFont", '"Segoe UI"', "Roboto", '"Helvetica Neue"', "Arial", '"Noto Sans"', "sans-serif", '"Apple Color Emoji"', '"Segoe UI Emoji"', '"Segoe UI Symbol"', '"Noto Color Emoji"' ].join(","),
        serif: [ "ui-serif", "Georgia", "Cambria", '"Times New Roman"', "Times", "serif" ].join(","),
        mono: [ "ui-monospace", "SFMono-Regular", "Menlo", "Monaco", "Consolas", '"Liberation Mono"', '"Courier New"', "monospace" ].join(",")
    }, qn = {
        xs: [ "0.75rem", "1rem" ],
        sm: [ "0.875rem", "1.25rem" ],
        base: [ "1rem", "1.5rem" ],
        lg: [ "1.125rem", "1.75rem" ],
        xl: [ "1.25rem", "1.75rem" ],
        "2xl": [ "1.5rem", "2rem" ],
        "3xl": [ "1.875rem", "2.25rem" ],
        "4xl": [ "2.25rem", "2.5rem" ],
        "5xl": [ "3rem", "1" ],
        "6xl": [ "3.75rem", "1" ],
        "7xl": [ "4.5rem", "1" ],
        "8xl": [ "6rem", "1" ],
        "9xl": [ "8rem", "1" ]
    }, Mn = {
        DEFAULT: "1.5rem",
        xs: "0.5rem",
        sm: "1rem",
        md: "1.5rem",
        lg: "2rem",
        xl: "2.5rem",
        "2xl": "3rem",
        "3xl": "4rem"
    }, Zn = {
        DEFAULT: "1.5rem",
        none: "0",
        sm: "thin",
        md: "medium",
        lg: "thick"
    }, Hn = {
        DEFAULT: [ "0 0 1px rgb(0 0 0 / 0.2)", "0 0 1px rgb(1 0 5 / 0.1)" ],
        none: "0 0 rgb(0 0 0 / 0)",
        sm: "1px 1px 3px rgb(36 37 47 / 0.25)",
        md: [ "0 1px 2px rgb(30 29 39 / 0.19)", "1px 2px 4px rgb(54 64 147 / 0.18)" ],
        lg: [ "3px 3px 6px rgb(0 0 0 / 0.26)", "0 0 5px rgb(15 3 86 / 0.22)" ],
        xl: [ "1px 1px 3px rgb(0 0 0 / 0.29)", "2px 4px 7px rgb(73 64 125 / 0.35)" ]
    }, Gn = {
        none: "1",
        tight: "1.25",
        snug: "1.375",
        normal: "1.5",
        relaxed: "1.625",
        loose: "2"
    }, Jn = {
        tighter: "-0.05em",
        tight: "-0.025em",
        normal: "0em",
        wide: "0.025em",
        wider: "0.05em",
        widest: "0.1em"
    }, Kn = {
        thin: "100",
        extralight: "200",
        light: "300",
        normal: "400",
        medium: "500",
        semibold: "600",
        bold: "700",
        extrabold: "800",
        black: "900"
    }, Vn = Jn;
    var Qn = {
        sm: "640px",
        md: "768px",
        lg: "1024px",
        xl: "1280px",
        "2xl": "1536px"
    }, ei = {
        ...Qn
    }, ti = {
        DEFAULT: "1px",
        none: "0"
    }, ri = {
        DEFAULT: "1rem",
        none: "0",
        xs: "0.75rem",
        sm: "0.875rem",
        lg: "1.125rem",
        xl: "1.25rem",
        "2xl": "1.5rem",
        "3xl": "1.875rem",
        "4xl": "2.25rem",
        "5xl": "3rem",
        "6xl": "3.75rem",
        "7xl": "4.5rem",
        "8xl": "6rem",
        "9xl": "8rem"
    }, ni = {
        DEFAULT: "150ms",
        none: "0s",
        75: "75ms",
        100: "100ms",
        150: "150ms",
        200: "200ms",
        300: "300ms",
        500: "500ms",
        700: "700ms",
        1e3: "1000ms"
    }, ii = {
        DEFAULT: "0.25rem",
        none: "0",
        sm: "0.125rem",
        md: "0.375rem",
        lg: "0.5rem",
        xl: "0.75rem",
        "2xl": "1rem",
        "3xl": "1.5rem",
        full: "9999px"
    }, oi = {
        DEFAULT: [ "var(--un-shadow-inset) 0 1px 3px 0 rgb(0 0 0 / 0.1)", "var(--un-shadow-inset) 0 1px 2px -1px rgb(0 0 0 / 0.1)" ],
        none: "0 0 rgb(0 0 0 / 0)",
        sm: "var(--un-shadow-inset) 0 1px 2px 0 rgb(0 0 0 / 0.05)",
        md: [ "var(--un-shadow-inset) 0 4px 6px -1px rgb(0 0 0 / 0.1)", "var(--un-shadow-inset) 0 2px 4px -2px rgb(0 0 0 / 0.1)" ],
        lg: [ "var(--un-shadow-inset) 0 10px 15px -3px rgb(0 0 0 / 0.1)", "var(--un-shadow-inset) 0 4px 6px -4px rgb(0 0 0 / 0.1)" ],
        xl: [ "var(--un-shadow-inset) 0 20px 25px -5px rgb(0 0 0 / 0.1)", "var(--un-shadow-inset) 0 8px 10px -6px rgb(0 0 0 / 0.1)" ],
        "2xl": "var(--un-shadow-inset) 0 25px 50px -12px rgb(0 0 0 / 0.25)",
        inner: "inset 0 2px 4px 0 rgb(0 0 0 / 0.05)"
    }, ai = {
        DEFAULT: "3px",
        none: "0"
    }, si = {
        auto: "auto"
    }, li = {
        mouse: "(hover) and (pointer: fine)"
    };
    var ci = {
        ...bn,
        ...en,
        ...Jr
    };
    var fi = {
        xs: "20rem",
        sm: "24rem",
        md: "28rem",
        lg: "32rem",
        xl: "36rem",
        "2xl": "42rem",
        "3xl": "48rem",
        "4xl": "56rem",
        "5xl": "64rem",
        "6xl": "72rem",
        "7xl": "80rem",
        prose: "65ch"
    }, di = {
        auto: "auto",
        ...fi,
        screen: "100vw"
    }, ui = {
        none: "none",
        ...fi,
        screen: "100vw"
    }, pi = {
        auto: "auto",
        ...fi,
        screen: "100vh"
    }, mi = {
        none: "none",
        ...fi,
        screen: "100vh"
    }, gi = {
        ...fi
    };
    var hi = {
        DEFAULT: "cubic-bezier(0.4, 0, 0.2, 1)",
        linear: "linear",
        "in": "cubic-bezier(0.4, 0, 1, 1)",
        out: "cubic-bezier(0, 0, 0.2, 1)",
        "in-out": "cubic-bezier(0.4, 0, 0.2, 1)"
    }, bi = {
        none: "none",
        all: "all",
        colors: [ "color", "background-color", "border-color", "text-decoration-color", "fill", "stroke" ].join(","),
        opacity: "opacity",
        shadow: "box-shadow",
        transform: "transform",
        get DEFAULT() {
            return [ this.colors, "opacity", "box-shadow", "transform", "filter", "backdrop-filter" ].join(",");
        }
    };
    var xi = {
        width: di,
        height: pi,
        maxWidth: ui,
        maxHeight: mi,
        minWidth: ui,
        minHeight: mi,
        inlineSize: di,
        blockSize: pi,
        maxInlineSize: ui,
        maxBlockSize: mi,
        minInlineSize: ui,
        minBlockSize: mi,
        colors: Xn,
        fontFamily: In,
        fontSize: qn,
        fontWeight: Kn,
        breakpoints: Qn,
        verticalBreakpoints: ei,
        borderRadius: ii,
        lineHeight: Gn,
        letterSpacing: Jn,
        wordSpacing: Vn,
        boxShadow: oi,
        textIndent: Mn,
        textShadow: Hn,
        textStrokeWidth: Zn,
        blur: Yn,
        dropShadow: Pn,
        easing: hi,
        transitionProperty: bi,
        lineWidth: ti,
        spacing: ri,
        duration: ni,
        ringWidth: ai,
        preflightBase: ci,
        containers: gi,
        zIndex: si,
        media: li
    };
    var vi = {
        name: "aria",
        match(e, n) {
            let i = s("aria-", e, n.generator.config.separators);
            if (i) {
                let [ e, t ] = i, r = v.bracket(e) ?? n.theme.aria?.[e] ?? "";
                if (r) return {
                    matcher: t,
                    selector: e => `${e}[aria-${r}]`
                };
            }
        }
    };
    function yi(o) {
        return {
            name: `${o}-aria`,
            match(e, n) {
                let i = s(`${o}-aria-`, e, n.generator.config.separators);
                if (i) {
                    let [ e, t ] = i, r = v.bracket(e) ?? n.theme.aria?.[e] ?? "";
                    if (r) return {
                        matcher: `${o}-[[aria-${r}]]:${t}`
                    };
                }
            }
        };
    }
    var $i = [ yi("group"), yi("peer"), yi("parent"), yi("previous") ];
    function ki(t) {
        let r = t.match(/^-?\d+\.?\d*/)?.[0] || "", n = t.slice(r.length);
        if (n === "px") {
            let e = Number.parseFloat(r) - .1;
            return Number.isNaN(e) ? t : `${e}${n}`;
        }
        return `calc(${t} - 0.1px)`;
    }
    var wi = /(max|min)-\[([^\]]*)\]:/;
    function zi() {
        let u = {};
        return {
            name: "breakpoints",
            match(c, f) {
                if (wi.test(c)) {
                    let r = c.match(wi);
                    return {
                        matcher: c.replace(r[0], ""),
                        handle: (e, t) => t({
                            ...e,
                            parent: `${e.parent ? `${e.parent} $$ ` : ""}@media (${r[1]}-width: ${r[2]})`
                        })
                    };
                }
                let d = (Ut(f) ?? []).map(({
                    point: e,
                    size: t
                }, r) => [ e, t, r ]);
                for (let [ a, s, l ] of d) {
                    u[a] || (u[a] = new RegExp(`^((?:([al]t-|[<~]|max-))?${a}(?:${f.generator.config.separators.join("|")}))`));
                    let e = c.match(u[a]);
                    if (!e) continue;
                    let [ , t ] = e, r = c.slice(t.length);
                    if (r === "container") continue;
                    let n = t.startsWith("lt-") || t.startsWith("<") || t.startsWith("max-"), i = t.startsWith("at-") || t.startsWith("~"), o = 3e3;
                    return n ? (o -= l + 1, {
                        matcher: r,
                        handle: (e, t) => t({
                            ...e,
                            parent: `${e.parent ? `${e.parent} $$ ` : ""}@media (max-width: ${ki(s)})`,
                            parentOrder: o
                        })
                    }) : (o += l + 1, i && l < d.length - 1 ? {
                        matcher: r,
                        handle: (e, t) => t({
                            ...e,
                            parent: `${e.parent ? `${e.parent} $$ ` : ""}@media (min-width: ${s}) and (max-width: ${ki(d[l + 1][1])})`,
                            parentOrder: o
                        })
                    } : {
                        matcher: r,
                        handle: (e, t) => t({
                            ...e,
                            parent: `${e.parent ? `${e.parent} $$ ` : ""}@media (min-width: ${s})`,
                            parentOrder: o
                        })
                    });
                }
            },
            multiPass: !0,
            autocomplete: "(at-|lt-|max-|)$breakpoints:"
        };
    }
    var Ei = [ n("*", e => ({
        selector: `${e.selector} > *`
    })) ];
    function ji(o, a) {
        return {
            name: `combinator:${o}`,
            match(t, e) {
                if (!t.startsWith(o)) return;
                let r = e.generator.config.separators, n = Xe(`${o}-`, t, r);
                if (!n) {
                    for (let e of r) if (t.startsWith(`${o}${e}`)) {
                        n = [ "", t.slice(o.length + e.length) ];
                        break;
                    }
                    if (!n) return;
                }
                let i = v.bracket(n[0]) ?? "";
                return i === "" && (i = "*"), {
                    matcher: n[1],
                    selector: e => `${e}${a}${i}`
                };
            },
            multiPass: !0
        };
    }
    var Si = [ ji("all", " "), ji("children", ">"), ji("next", "+"), ji("sibling", "+"), ji("siblings", "~") ];
    var Ci = {
        name: "@",
        match(e, o) {
            if (e.startsWith("@container")) return;
            let a = s("@", e, o.generator.config.separators);
            if (a) {
                let [ e, t, n ] = a, r = v.bracket(e), i;
                if (r ? i = v.numberWithUnit(r) : i = o.theme.containers?.[e] ?? "", 
                i) {
                    let r = 1e3 + Object.keys(o.theme.containers ?? {}).indexOf(e);
                    return n && (r += 1e3), {
                        matcher: t,
                        handle: (e, t) => t({
                            ...e,
                            parent: `${e.parent ? `${e.parent} $$ ` : ""}@container${n ? ` ${n} ` : " "}(min-width: ${i})`,
                            parentOrder: r
                        })
                    };
                }
            }
        },
        multiPass: !0
    };
    function Fi(e = {}) {
        if (e?.dark === "class" || typeof e.dark == "object") {
            let {
                dark: t = ".dark",
                light: r = ".light"
            } = typeof e.dark == "string" ? {} : e.dark;
            return [ n("dark", e => ({
                prefix: `${t} $$ ${e.prefix}`
            })), n("light", e => ({
                prefix: `${r} $$ ${e.prefix}`
            })) ];
        }
        return [ t("dark", "@media (prefers-color-scheme: dark)"), t("light", "@media (prefers-color-scheme: light)") ];
    }
    var Ai = {
        name: "data",
        match(e, n) {
            let i = s("data-", e, n.generator.config.separators);
            if (i) {
                let [ e, t ] = i, r = v.bracket(e) ?? n.theme.data?.[e] ?? "";
                if (r) return {
                    matcher: t,
                    selector: e => `${e}[data-${r}]`
                };
            }
        }
    };
    function Li(a) {
        return {
            name: `${a}-data`,
            match(e, i) {
                let o = s(`${a}-data-`, e, i.generator.config.separators);
                if (o) {
                    let [ e, t, r ] = o, n = v.bracket(e) ?? i.theme.data?.[e] ?? "";
                    if (n) return {
                        matcher: `${a}-[[data-${n}]]${r ? `/${r}` : ""}:${t}`
                    };
                }
            }
        };
    }
    var _i = [ Li("group"), Li("peer"), Li("parent"), Li("previous") ];
    var Ni = [ n("rtl", e => ({
        prefix: `[dir="rtl"] $$ ${e.prefix}`
    })), n("ltr", e => ({
        prefix: `[dir="ltr"] $$ ${e.prefix}`
    })) ];
    function Wi() {
        let i;
        return {
            name: "important",
            match(e, t) {
                i || (i = new RegExp(`^(important(?:${t.generator.config.separators.join("|")})|!)`));
                let r, n = e.match(i);
                if (n ? r = e.slice(n[0].length) : e.endsWith("!") && (r = e.slice(0, -1)), 
                r) return {
                    matcher: r,
                    body: e => (e.forEach(e => {
                        e[1] != null && (e[1] += " !important");
                    }), e)
                };
            }
        };
    }
    var Ui = t("print", "@media print"), Oi = {
        name: "media",
        match(e, n) {
            let i = s("media-", e, n.generator.config.separators);
            if (i) {
                let [ e, t ] = i, r = v.bracket(e) ?? "";
                if (r === "" && (r = n.theme.media?.[e] ?? ""), r) return {
                    matcher: t,
                    handle: (e, t) => t({
                        ...e,
                        parent: `${e.parent ? `${e.parent} $$ ` : ""}@media ${r}`
                    })
                };
            }
        },
        multiPass: !0
    };
    var Ti = {
        name: "selector",
        match(e, t) {
            let n = Xe("selector-", e, t.generator.config.separators);
            if (n) {
                let [ e, t ] = n, r = v.bracket(e);
                if (r) return {
                    matcher: t,
                    selector: () => r
                };
            }
        }
    }, Ri = {
        name: "layer",
        match(e, t) {
            let n = s("layer-", e, t.generator.config.separators);
            if (n) {
                let [ e, t ] = n, r = v.bracket(e) ?? e;
                if (r) return {
                    matcher: t,
                    handle: (e, t) => t({
                        ...e,
                        parent: `${e.parent ? `${e.parent} $$ ` : ""}@layer ${r}`
                    })
                };
            }
        }
    }, Di = {
        name: "uno-layer",
        match(e, t) {
            let n = s("uno-layer-", e, t.generator.config.separators);
            if (n) {
                let [ e, t ] = n, r = v.bracket(e) ?? e;
                if (r) return {
                    matcher: t,
                    layer: r
                };
            }
        }
    }, Bi = {
        name: "scope",
        match(e, t) {
            let n = Xe("scope-", e, t.generator.config.separators);
            if (n) {
                let [ e, t ] = n, r = v.bracket(e);
                if (r) return {
                    matcher: t,
                    selector: e => `${r} $$ ${e}`
                };
            }
        }
    }, Xi = {
        name: "variables",
        match(e, t) {
            if (!e.startsWith("[")) return;
            let [ r, n ] = te(e, "[", "]") ?? [];
            if (!(r && n)) return;
            let i;
            for (let e of t.generator.config.separators) if (n.startsWith(e)) {
                i = n.slice(e.length);
                break;
            }
            if (i == null) return;
            let o = v.bracket(r) ?? "", a = o.startsWith("@");
            if (a || o.includes("&")) return {
                matcher: i,
                handle(e, t) {
                    let r = a ? {
                        parent: `${e.parent ? `${e.parent} $$ ` : ""}${o}`
                    } : {
                        selector: o.replace(/&/g, e.selector)
                    };
                    return t({
                        ...e,
                        ...r
                    });
                }
            };
        },
        multiPass: !0
    }, Yi = {
        name: "theme-variables",
        match(e, r) {
            if (Re(e)) return {
                matcher: e,
                handle(e, t) {
                    return t({
                        ...e,
                        entries: JSON.parse(De(JSON.stringify(e.entries), r.theme))
                    });
                }
            };
        }
    };
    var Pi = /^-?[0-9.]+(?:[a-z]+|%)?$/, Ii = /-?[0-9.]+(?:[a-z]+|%)?/, qi = [ /\b(opacity|color|flex|backdrop-filter|^filter|transform)\b/ ];
    function Mi(e) {
        let r = e.match(He) || e.match(Ge);
        if (r) {
            let [ e, t ] = l(`(${r[2]})${r[3]}`, "(", ")", " ") ?? [];
            if (e) return `calc(${r[1]}${e} * -1)${t ? ` ${t}` : ""}`;
        }
    }
    var Zi = /\b(hue-rotate)\s*(\(.*)/;
    function Hi(e) {
        let n = e.match(Zi);
        if (n) {
            let [ t, r ] = l(n[2], "(", ")", " ") ?? [];
            if (t) {
                let e = Pi.test(t.slice(1, -1)) ? t.replace(Ii, e => e.startsWith("-") ? e.slice(1) : `-${e}`) : `(calc(${t} * -1))`;
                return `${n[1]}${e}${r ? ` ${r}` : ""}`;
            }
        }
    }
    var Gi = {
        name: "negative",
        match(e) {
            if (e.startsWith("-")) return {
                matcher: e.slice(1),
                body: e => {
                    if (e.find(e => e[0] === jt)) return;
                    let i = !1;
                    return e.forEach(t => {
                        let e = t[1]?.toString();
                        if (!e || e === "0" || qi.some(e => e.test(t[0]))) return;
                        let r = Mi(e);
                        if (r) {
                            t[1] = r, i = !0;
                            return;
                        }
                        let n = Hi(e);
                        if (n) {
                            t[1] = n, i = !0;
                            return;
                        }
                        Pi.test(e) && (t[1] = e.replace(Ii, e => e.startsWith("-") ? e.slice(1) : `-${e}`), 
                        i = !0);
                    }), i ? e : [];
                }
            };
        }
    };
    var j = Object.fromEntries([ [ "first-letter", "::first-letter" ], [ "first-line", "::first-line" ], "any-link", "link", "visited", "target", [ "open", "[open]" ], "default", "checked", "indeterminate", "placeholder-shown", "autofill", "optional", "required", "valid", "invalid", "user-valid", "user-invalid", "in-range", "out-of-range", "read-only", "read-write", "empty", "focus-within", "hover", "focus", "focus-visible", "active", "enabled", "disabled", "popover-open", "root", "empty", [ "even-of-type", ":nth-of-type(even)" ], [ "even", ":nth-child(even)" ], [ "odd-of-type", ":nth-of-type(odd)" ], [ "odd", ":nth-child(odd)" ], "first-of-type", [ "first", ":first-child" ], "last-of-type", [ "last", ":last-child" ], "only-child", "only-of-type", [ "backdrop-element", "::backdrop" ], [ "placeholder", "::placeholder" ], [ "before", "::before" ], [ "after", "::after" ], [ "selection", " ::selection" ], [ "marker", "::marker" ], [ "file", "::file-selector-button" ] ].map(e => Array.isArray(e) ? e : [ e, `:${e}` ])), Ji = Object.keys(j), S = Object.fromEntries([ [ "backdrop", "::backdrop" ] ].map(e => Array.isArray(e) ? e : [ e, `:${e}` ])), Ki = Object.keys(S), Vi = [ "not", "is", "where", "has" ], Qi = Object.entries(j).filter(([ , e ]) => !e.startsWith("::")).map(([ e ]) => e).sort((e, t) => t.length - e.length).join("|"), eo = Object.entries(S).filter(([ , e ]) => !e.startsWith("::")).map(([ e ]) => e).sort((e, t) => t.length - e.length).join("|"), C = Vi.join("|");
    function to(s, l, c) {
        let f = new RegExp(`^(${a(l)}:)(\\S+)${a(c)}\\1`), d, u, p, m, g = e => {
            let t = Xe(`${s}-`, e, []);
            if (!t) return;
            let [ r, n ] = t, i = v.bracket(r);
            if (i == null) return;
            let o = n.split(d, 1)?.[0] ?? "", a = `${l}${x(o)}`;
            return [ o, e.slice(e.length - (n.length - o.length - 1)), i.includes("&") ? i.replace(/&/g, a) : `${a}${i}` ];
        }, h = e => {
            let t = e.match(u) || e.match(p);
            if (!t) return;
            let [ r, n, i ] = t, o = t[3] ?? "", a = j[i] || S[i] || `:${i}`;
            return n && (a = `:${n}(${a})`), [ o, e.slice(r.length), `${l}${x(o)}${a}`, i ];
        }, b = e => {
            let t = e.match(m);
            if (!t) return;
            let [ r, n, i ] = t, o = t[3] ?? "", a = `:${n}(${i})`;
            return [ o, e.slice(r.length), `${l}${x(o)}${a}` ];
        };
        return {
            name: `pseudo:${s}`,
            match(e, t) {
                if (d && u && p || (d = new RegExp(`(?:${t.generator.config.separators.join("|")})`), 
                u = new RegExp(`^${s}-(?:(?:(${C})-)?(${Qi}))(?:(/\\w+))?(?:${t.generator.config.separators.join("|")})`), 
                p = new RegExp(`^${s}-(?:(?:(${C})-)?(${eo}))(?:(/\\w+))?(?:${t.generator.config.separators.filter(e => e !== "-").join("|")})`), 
                m = new RegExp(`^${s}-(?:(${C})-)?\\[(.+)\\](?:(/\\w+))?(?:${t.generator.config.separators.filter(e => e !== "-").join("|")})`)), 
                !e.startsWith(s)) return;
                let r = g(e) || h(e) || b(e);
                if (!r) return;
                let [ n, i, o, a = "" ] = r;
                return {
                    matcher: i,
                    handle: (e, t) => t({
                        ...e,
                        prefix: `${o}${c}${e.prefix}`.replace(f, "$1$2:"),
                        sort: Ji.indexOf(a) ?? Ki.indexOf(a)
                    })
                };
            },
            multiPass: !0
        };
    }
    var ro = [ "::-webkit-resizer", "::-webkit-scrollbar", "::-webkit-scrollbar-button", "::-webkit-scrollbar-corner", "::-webkit-scrollbar-thumb", "::-webkit-scrollbar-track", "::-webkit-scrollbar-track-piece", "::file-selector-button" ], no = Object.entries(j).map(([ e ]) => e).sort((e, t) => t.length - e.length).join("|"), io = Object.entries(S).map(([ e ]) => e).sort((e, t) => t.length - e.length).join("|");
    function oo() {
        let n, i;
        return {
            name: "pseudo",
            match(e, t) {
                n && n || (n = new RegExp(`^(${no})(?:${t.generator.config.separators.join("|")})`), 
                i = new RegExp(`^(${io})(?:${t.generator.config.separators.filter(e => e !== "-").join("|")})`));
                let r = e.match(n) || e.match(i);
                if (r) {
                    let n = j[r[1]] || S[r[1]] || `:${r[1]}`, i = Ji.indexOf(r[1]);
                    return i === -1 && (i = Ki.indexOf(r[1])), i === -1 && (i = void 0), 
                    {
                        matcher: e.slice(r[0].length),
                        handle: (e, t) => {
                            let r = n.startsWith("::") && !ro.includes(n) ? {
                                pseudo: `${e.pseudo}${n}`
                            } : {
                                selector: `${e.selector}${n}`
                            };
                            return t({
                                ...e,
                                ...r,
                                sort: i,
                                noMerge: !0
                            });
                        }
                    };
                }
            },
            multiPass: !0,
            autocomplete: `(${no}|${io}):`
        };
    }
    function ao() {
        let r, i, o;
        return {
            match(e, t) {
                r && i || (r = new RegExp(`^(${C})-(${Qi})(?:${t.generator.config.separators.join("|")})`), 
                i = new RegExp(`^(${C})-(${eo})(?:${t.generator.config.separators.filter(e => e !== "-").join("|")})`), 
                o = new RegExp(`^(${C})-(\\[.+\\])(?:${t.generator.config.separators.filter(e => e !== "-").join("|")})`));
                let n = e.match(r) || e.match(i) || e.match(o);
                if (n) {
                    let t = n[1], r = te(n[2], "[", "]") ? v.bracket(n[2]) : j[n[2]] || S[n[2]] || `:${n[2]}`;
                    return {
                        matcher: e.slice(n[0].length),
                        selector: e => `${e}:${t}(${r})`
                    };
                }
            },
            multiPass: !0,
            autocomplete: `(${C})-(${Qi}|${eo}):`
        };
    }
    function so(e = {}) {
        let r = !!e?.attributifyPseudo, n = e?.prefix ?? "";
        n = (Array.isArray(n) ? n : [ n ]).filter(Boolean)[0] ?? "";
        let t = (e, t) => to(e, r ? `[${n}${e}=""]` : `.${n}${e}`, t);
        return [ t("group", " "), t("peer", "~"), t("parent", ">"), t("previous", "+"), t("group-aria", " "), t("peer-aria", "~"), t("parent-aria", ">"), t("previous-aria", "+") ];
    }
    var lo = /(part-\[(.+)\]:)(.+)/, co = {
        match(e) {
            let r = e.match(lo);
            if (r) {
                let t = `part(${r[2]})`;
                return {
                    matcher: e.slice(r[1].length),
                    selector: e => `${e}::${t}`
                };
            }
        },
        multiPass: !0
    };
    var fo = {
        name: "starting",
        match(e) {
            if (e.startsWith("starting:")) return {
                matcher: e.slice(9),
                handle: (e, t) => t({
                    ...e,
                    parent: "@starting-style"
                })
            };
        }
    };
    var uo = {
        name: "supports",
        match(e, n) {
            let i = s("supports-", e, n.generator.config.separators);
            if (i) {
                let [ e, t ] = i, r = v.bracket(e) ?? "";
                if (r === "" && (r = n.theme.supports?.[e] ?? ""), r) return {
                    matcher: t,
                    handle: (e, t) => t({
                        ...e,
                        parent: `${e.parent ? `${e.parent} $$ ` : ""}@supports ${r}`
                    })
                };
            }
        },
        multiPass: !0
    };
    function po(e) {
        return [ vi, Ai, Ri, Ti, Di, Gi, fo, Wi(), uo, Ui, Oi, zi(), ...Si, oo(), ao(), ...so(e), co, ...Fi(e), ...Ni, Bi, ...Ei, Ci, Xi, ..._i, ...$i, Yi ];
    }
    var mo = (e = {}) => (e.dark = e.dark ?? "class", e.attributifyPseudo = e.attributifyPseudo ?? !1, 
    e.preflight = e.preflight ?? !0, e.variablePrefix = e.variablePrefix ?? "un-", 
    {
        name: "@unocss/preset-mini",
        theme: xi,
        rules: Dn,
        variants: po(e),
        options: e,
        prefix: e.prefix,
        postprocess: ho(e.variablePrefix),
        preflights: e.preflight ? bo(ee, e.variablePrefix) : [],
        extractorDefault: e.arbitraryVariants === !1 ? void 0 : Q(),
        autocomplete: {
            shorthands: Bn
        }
    }), go = mo;
    function ho(t) {
        if (t !== "un-") return e => {
            e.entries.forEach(e => {
                e[0] = e[0].replace(/^--un-/, `--${t}`), typeof e[1] == "string" && (e[1] = e[1].replace(/var\(--un-/g, `var(--${t}`));
            });
        };
    }
    function bo(e, n) {
        return n !== "un-" ? e.map(r => ({
            ...r,
            getCSS: async e => {
                let t = await r.getCSS(e);
                if (t) return t.replace(/--un-/g, `--${n}`);
            }
        })) : e;
    }
    function xo(t) {
        if (t == null || t === !1) return [];
        let r = e => e.startsWith(":is(") && e.endsWith(")") ? e : e.includes("::") ? e.replace(/(.*?)(\s*::.*)/, ":is($1)$2") : `:is(${e})`;
        return [ t === !0 ? e => {
            e.entries.forEach(e => {
                e[1] != null && !String(e[1]).endsWith("!important") && (e[1] += " !important");
            });
        } : e => {
            e.selector.startsWith(t) || (e.selector = `${t} ${r(e.selector)}`);
        } ];
    }
    function vo(e) {
        return [ ...c(go(e).postprocess), ...xo(e.important) ];
    }
    var yo = [ [ /^(?:animate-)?keyframes-(.+)$/, ([ , e ], {
        theme: t
    }) => {
        let r = t.animation?.keyframes?.[e];
        if (r) return [ `@keyframes ${e}${r}`, {
            animation: e
        } ];
    }, {
        autocomplete: [ "animate-keyframes-$animation.keyframes", "keyframes-$animation.keyframes" ]
    } ], [ /^animate-(.+)$/, ([ , i ], {
        theme: o
    }) => {
        let a = o.animation?.keyframes?.[i];
        if (a) {
            let e = o.animation?.durations?.[i] ?? "1s", t = o.animation?.timingFns?.[i] ?? "linear", r = o.animation?.counts?.[i] ?? 1, n = o.animation?.properties?.[i];
            return [ `@keyframes ${i}${a}`, {
                animation: `${i} ${e} ${t} ${r}`,
                ...n
            } ];
        }
        return {
            animation: v.bracket.cssvar(i)
        };
    }, {
        autocomplete: "animate-$animation.keyframes"
    } ], [ /^animate-name-(.+)/, ([ , e ]) => ({
        "animation-name": v.bracket.cssvar(e) ?? e
    }) ], [ /^animate-duration-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "animation-duration": t.duration?.[e || "DEFAULT"] ?? v.bracket.cssvar.time(e)
    }), {
        autocomplete: [ "animate-duration", "animate-duration-$duration" ]
    } ], [ /^animate-delay-(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "animation-delay": t.duration?.[e || "DEFAULT"] ?? v.bracket.cssvar.time(e)
    }), {
        autocomplete: [ "animate-delay", "animate-delay-$duration" ]
    } ], [ /^animate-ease(?:-(.+))?$/, ([ , e ], {
        theme: t
    }) => ({
        "animation-timing-function": t.easing?.[e || "DEFAULT"] ?? v.bracket.cssvar(e)
    }), {
        autocomplete: [ "animate-ease", "animate-ease-$easing" ]
    } ], [ /^animate-(fill-mode-|fill-|mode-)?(.+)$/, ([ , e, t ]) => [ "none", "forwards", "backwards", "both", e ? i : [] ].includes(t) ? {
        "animation-fill-mode": t
    } : void 0, {
        autocomplete: [ "animate-(fill|mode|fill-mode)", "animate-(fill|mode|fill-mode)-(none|forwards|backwards|both|inherit|initial|revert|revert-layer|unset)", "animate-(none|forwards|backwards|both|inherit|initial|revert|revert-layer|unset)" ]
    } ], [ /^animate-(direction-)?(.+)$/, ([ , e, t ]) => [ "normal", "reverse", "alternate", "alternate-reverse", e ? i : [] ].includes(t) ? {
        "animation-direction": t
    } : void 0, {
        autocomplete: [ "animate-direction", "animate-direction-(normal|reverse|alternate|alternate-reverse|inherit|initial|revert|revert-layer|unset)", "animate-(normal|reverse|alternate|alternate-reverse|inherit|initial|revert|revert-layer|unset)" ]
    } ], [ /^animate-(?:iteration-count-|iteration-|count-)(.+)$/, ([ , e ]) => ({
        "animation-iteration-count": v.bracket.cssvar(e) ?? e.replace(/-/g, ",")
    }), {
        autocomplete: [ "animate-(iteration|count|iteration-count)", "animate-(iteration|count|iteration-count)-<num>" ]
    } ], [ /^animate-(play-state-|play-|state-)?(.+)$/, ([ , e, t ]) => [ "paused", "running", e ? i : [] ].includes(t) ? {
        "animation-play-state": t
    } : void 0, {
        autocomplete: [ "animate-(play|state|play-state)", "animate-(play|state|play-state)-(paused|running|inherit|initial|revert|revert-layer|unset)", "animate-(paused|running|inherit|initial|revert|revert-layer|unset)" ]
    } ], [ "animate-none", {
        animation: "none"
    } ], ...h("animate", "animation") ];
    function $o(e) {
        return e ? m(e, 0) : "rgb(255 255 255 / 0)";
    }
    function ko(e, t, r, n) {
        return t ? n != null ? m(t, n) : m(t, `var(--un-${e}-opacity, ${p(t)})`) : m(r, n);
    }
    function wo() {
        return ([ , e, t ], {
            theme: r
        }) => {
            let n = At(t, r, "backgroundColor");
            if (!n) return;
            let {
                alpha: i,
                color: o,
                cssColor: a
            } = n;
            if (!o) return;
            let s = ko(e, a, o, i);
            switch (e) {
              case "from":
                return {
                    "--un-gradient-from-position": "0%",
                    "--un-gradient-from": `${s} var(--un-gradient-from-position)`,
                    "--un-gradient-to-position": "100%",
                    "--un-gradient-to": `${$o(a)} var(--un-gradient-to-position)`,
                    "--un-gradient-stops": "var(--un-gradient-from), var(--un-gradient-to)"
                };

              case "via":
                return {
                    "--un-gradient-via-position": "50%",
                    "--un-gradient-to": $o(a),
                    "--un-gradient-stops": `var(--un-gradient-from), ${s} var(--un-gradient-via-position), var(--un-gradient-to)`
                };

              case "to":
                return {
                    "--un-gradient-to-position": "100%",
                    "--un-gradient-to": `${s} var(--un-gradient-to-position)`
                };
            }
        };
    }
    function zo() {
        return ([ , e, t ]) => ({
            [`--un-gradient-${e}-position`]: `${Number(v.bracket.cssvar.percent(t)) * 100}%`
        });
    }
    var Eo = [ [ /^bg-gradient-(.+)$/, ([ , e ]) => ({
        "--un-gradient": v.bracket(e)
    }), {
        autocomplete: [ "bg-gradient", "bg-gradient-(from|to|via)", "bg-gradient-(from|to|via)-$colors", "bg-gradient-(from|to|via)-(op|opacity)", "bg-gradient-(from|to|via)-(op|opacity)-<percent>" ]
    } ], [ /^(?:bg-gradient-)?stops-(\[.+\])$/, ([ , e ]) => ({
        "--un-gradient-stops": v.bracket(e)
    }) ], [ /^(?:bg-gradient-)?(from)-(.+)$/, wo() ], [ /^(?:bg-gradient-)?(via)-(.+)$/, wo() ], [ /^(?:bg-gradient-)?(to)-(.+)$/, wo() ], [ /^(?:bg-gradient-)?(from|via|to)-op(?:acity)?-?(.+)$/, ([ , e, t ]) => ({
        [`--un-${e}-opacity`]: v.bracket.percent(t)
    }) ], [ /^(from|via|to)-([\d.]+)%$/, zo() ], [ /^bg-gradient-((?:repeating-)?(?:linear|radial|conic))$/, ([ , e ]) => ({
        "background-image": `${e}-gradient(var(--un-gradient, var(--un-gradient-stops, rgb(255 255 255 / 0))))`
    }), {
        autocomplete: [ "bg-gradient-repeating", "bg-gradient-(linear|radial|conic)", "bg-gradient-repeating-(linear|radial|conic)" ]
    } ], [ /^bg-gradient-to-([rltb]{1,2})$/, ([ , e ]) => {
        if (e in r) return {
            "--un-gradient-shape": `to ${r[e]} in oklch`,
            "--un-gradient": "var(--un-gradient-shape), var(--un-gradient-stops)",
            "background-image": "linear-gradient(var(--un-gradient))"
        };
    }, {
        autocomplete: `bg-gradient-to-(${Object.keys(r).filter(e => e.length <= 2 && Array.from(e).every(e => "rltb".includes(e))).join("|")})`
    } ], [ /^(?:bg-gradient-)?shape-(.+)$/, ([ , e ]) => {
        let t = e in r ? `to ${r[e]}` : v.bracket(e);
        if (t != null) return {
            "--un-gradient-shape": `${t} in oklch`,
            "--un-gradient": "var(--un-gradient-shape), var(--un-gradient-stops)"
        };
    }, {
        autocomplete: [ "bg-gradient-shape", `bg-gradient-shape-(${Object.keys(r).join("|")})`, `shape-(${Object.keys(r).join("|")})` ]
    } ], [ "bg-none", {
        "background-image": "none"
    } ], [ "box-decoration-slice", {
        "box-decoration-break": "slice"
    } ], [ "box-decoration-clone", {
        "box-decoration-break": "clone"
    } ], ...h("box-decoration", "box-decoration-break"), [ "bg-auto", {
        "background-size": "auto"
    } ], [ "bg-cover", {
        "background-size": "cover"
    } ], [ "bg-contain", {
        "background-size": "contain"
    } ], [ "bg-fixed", {
        "background-attachment": "fixed"
    } ], [ "bg-local", {
        "background-attachment": "local"
    } ], [ "bg-scroll", {
        "background-attachment": "scroll"
    } ], [ "bg-clip-border", {
        "-webkit-background-clip": "border-box",
        "background-clip": "border-box"
    } ], [ "bg-clip-content", {
        "-webkit-background-clip": "content-box",
        "background-clip": "content-box"
    } ], [ "bg-clip-padding", {
        "-webkit-background-clip": "padding-box",
        "background-clip": "padding-box"
    } ], [ "bg-clip-text", {
        "-webkit-background-clip": "text",
        "background-clip": "text"
    } ], ...i.map(e => [ `bg-clip-${e}`, {
        "-webkit-background-clip": e,
        "background-clip": e
    } ]), [ /^bg-([-\w]{3,})$/, ([ , e ]) => ({
        "background-position": r[e]
    }) ], [ "bg-repeat", {
        "background-repeat": "repeat"
    } ], [ "bg-no-repeat", {
        "background-repeat": "no-repeat"
    } ], [ "bg-repeat-x", {
        "background-repeat": "repeat-x"
    } ], [ "bg-repeat-y", {
        "background-repeat": "repeat-y"
    } ], [ "bg-repeat-round", {
        "background-repeat": "round"
    } ], [ "bg-repeat-space", {
        "background-repeat": "space"
    } ], ...h("bg-repeat", "background-repeat"), [ "bg-origin-border", {
        "background-origin": "border-box"
    } ], [ "bg-origin-padding", {
        "background-origin": "padding-box"
    } ], [ "bg-origin-content", {
        "background-origin": "content-box"
    } ], ...h("bg-origin", "background-origin") ];
    var jo = {
        disc: "disc",
        circle: "circle",
        square: "square",
        decimal: "decimal",
        "zero-decimal": "decimal-leading-zero",
        greek: "lower-greek",
        roman: "lower-roman",
        "upper-roman": "upper-roman",
        alpha: "lower-alpha",
        "upper-alpha": "upper-alpha",
        latin: "lower-latin",
        "upper-latin": "upper-latin"
    }, So = [ [ /^list-(.+?)(?:-(outside|inside))?$/, ([ , e, t ]) => {
        let r = jo[e];
        if (r) return t ? {
            "list-style-position": t,
            "list-style-type": r
        } : {
            "list-style-type": r
        };
    }, {
        autocomplete: [ `list-(${Object.keys(jo).join("|")})`, `list-(${Object.keys(jo).join("|")})-(outside|inside)` ]
    } ], [ "list-outside", {
        "list-style-position": "outside"
    } ], [ "list-inside", {
        "list-style-position": "inside"
    } ], [ "list-none", {
        "list-style-type": "none"
    } ], [ /^list-image-(.+)$/, ([ , e ]) => {
        if (/^\[url\(.+\)\]$/.test(e)) return {
            "list-style-image": v.bracket(e)
        };
    } ], [ "list-image-none", {
        "list-style-image": "none"
    } ], ...h("list", "list-style-type") ], Co = [ [ /^accent-(.+)$/, g("accent-color", "accent", "accentColor"), {
        autocomplete: "accent-$colors"
    } ], [ /^accent-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-accent-opacity": v.bracket.percent(e)
    }), {
        autocomplete: [ "accent-(op|opacity)", "accent-(op|opacity)-<percent>" ]
    } ] ], Fo = [ [ /^caret-(.+)$/, g("caret-color", "caret", "textColor"), {
        autocomplete: "caret-$colors"
    } ], [ /^caret-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-caret-opacity": v.bracket.percent(e)
    }), {
        autocomplete: [ "caret-(op|opacity)", "caret-(op|opacity)-<percent>" ]
    } ] ], Ao = [ [ "image-render-auto", {
        "image-rendering": "auto"
    } ], [ "image-render-edge", {
        "image-rendering": "crisp-edges"
    } ], [ "image-render-pixel", [ [ "-ms-interpolation-mode", "nearest-neighbor" ], [ "image-rendering", "-webkit-optimize-contrast" ], [ "image-rendering", "-moz-crisp-edges" ], [ "image-rendering", "-o-pixelated" ], [ "image-rendering", "pixelated" ] ] ] ], Lo = [ [ "overscroll-auto", {
        "overscroll-behavior": "auto"
    } ], [ "overscroll-contain", {
        "overscroll-behavior": "contain"
    } ], [ "overscroll-none", {
        "overscroll-behavior": "none"
    } ], ...h("overscroll", "overscroll-behavior"), [ "overscroll-x-auto", {
        "overscroll-behavior-x": "auto"
    } ], [ "overscroll-x-contain", {
        "overscroll-behavior-x": "contain"
    } ], [ "overscroll-x-none", {
        "overscroll-behavior-x": "none"
    } ], ...h("overscroll-x", "overscroll-behavior-x"), [ "overscroll-y-auto", {
        "overscroll-behavior-y": "auto"
    } ], [ "overscroll-y-contain", {
        "overscroll-behavior-y": "contain"
    } ], [ "overscroll-y-none", {
        "overscroll-behavior-y": "none"
    } ], ...h("overscroll-y", "overscroll-behavior-y") ], _o = [ [ "scroll-auto", {
        "scroll-behavior": "auto"
    } ], [ "scroll-smooth", {
        "scroll-behavior": "smooth"
    } ], ...h("scroll", "scroll-behavior") ];
    var No = [ [ /^columns-(.+)$/, ([ , e ]) => ({
        columns: v.bracket.global.number.auto.numberWithUnit(e)
    }), {
        autocomplete: "columns-<num>"
    } ], [ "break-before-auto", {
        "break-before": "auto"
    } ], [ "break-before-avoid", {
        "break-before": "avoid"
    } ], [ "break-before-all", {
        "break-before": "all"
    } ], [ "break-before-avoid-page", {
        "break-before": "avoid-page"
    } ], [ "break-before-page", {
        "break-before": "page"
    } ], [ "break-before-left", {
        "break-before": "left"
    } ], [ "break-before-right", {
        "break-before": "right"
    } ], [ "break-before-column", {
        "break-before": "column"
    } ], ...h("break-before"), [ "break-inside-auto", {
        "break-inside": "auto"
    } ], [ "break-inside-avoid", {
        "break-inside": "avoid"
    } ], [ "break-inside-avoid-page", {
        "break-inside": "avoid-page"
    } ], [ "break-inside-avoid-column", {
        "break-inside": "avoid-column"
    } ], ...h("break-inside"), [ "break-after-auto", {
        "break-after": "auto"
    } ], [ "break-after-avoid", {
        "break-after": "avoid"
    } ], [ "break-after-all", {
        "break-after": "all"
    } ], [ "break-after-avoid-page", {
        "break-after": "avoid-page"
    } ], [ "break-after-page", {
        "break-after": "page"
    } ], [ "break-after-left", {
        "break-after": "left"
    } ], [ "break-after-right", {
        "break-after": "right"
    } ], [ "break-after-column", {
        "break-after": "column"
    } ], ...h("break-after") ];
    var Wo = /@media \(min-width: (.+)\)/, Uo = [ [ /^__container$/, (e, r) => {
        let {
            theme: t,
            variantHandlers: n
        } = r, i = t.container?.padding, o;
        D(i) ? o = i : o = i?.DEFAULT;
        let a = t.container?.maxWidth, s;
        for (let t of n) {
            let e = t.handle?.({}, e => e)?.parent;
            if (D(e)) {
                let t = e.match(Wo)?.[1];
                if (t) {
                    let e = (Ut(r) ?? []).find(e => e.size === t)?.point;
                    a ? e && (s = a?.[e]) : s = t, e && !D(i) && (o = i?.[e] ?? o);
                }
            }
        }
        let l = {
            "max-width": s
        };
        return n.length || (l.width = "100%"), t.container?.center && (l["margin-left"] = "auto", 
        l["margin-right"] = "auto"), i && (l["padding-left"] = o, l["padding-right"] = o), 
        l;
    }, {
        internal: !0
    } ] ], Oo = [ [ /^(?:(\w+)[:-])?container$/, ([ , e ], t) => {
        let r = (Ut(t) ?? []).map(e => e.point);
        if (e) {
            if (!r.includes(e)) return;
            r = r.slice(r.indexOf(e));
        }
        let n = r.map(e => `${e}:__container`);
        return e || n.unshift("__container"), n;
    } ] ];
    var To = [ [ /^divide-?([xy])$/, Ro, {
        autocomplete: [ "divide-(x|y|block|inline)", "divide-(x|y|block|inline)-reverse", "divide-(x|y|block|inline)-$lineWidth" ]
    } ], [ /^divide-?([xy])-?(.+)$/, Ro ], [ /^divide-?([xy])-reverse$/, ([ , e ]) => ({
        [`--un-divide-${e}-reverse`]: 1
    }) ], [ /^divide-(block|inline)$/, Ro ], [ /^divide-(block|inline)-(.+)$/, Ro ], [ /^divide-(block|inline)-reverse$/, ([ , e ]) => ({
        [`--un-divide-${e}-reverse`]: 1
    }) ], [ /^divide-(.+)$/, g("border-color", "divide", "borderColor"), {
        autocomplete: "divide-$colors"
    } ], [ /^divide-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-divide-opacity": v.bracket.percent(e)
    }), {
        autocomplete: [ "divide-(op|opacity)", "divide-(op|opacity)-<percent>" ]
    } ], ...y.map(e => [ `divide-${e}`, {
        "border-style": e
    } ]) ];
    function Ro([ , n, e ], {
        theme: t
    }) {
        let i = t.lineWidth?.[e || "DEFAULT"] ?? v.bracket.cssvar.px(e || "1");
        if (i != null) {
            i === "0" && (i = "0px");
            let e = d[n].map(e => {
                let t = `border${e}-width`, r = e.endsWith("right") || e.endsWith("bottom") ? `calc(${i} * var(--un-divide-${n}-reverse))` : `calc(${i} * calc(1 - var(--un-divide-${n}-reverse)))`;
                return [ t, r ];
            });
            if (e) return [ [ `--un-divide-${n}-reverse`, 0 ], ...e ];
        }
    }
    var Do = {
        "--un-blur": w,
        "--un-brightness": w,
        "--un-contrast": w,
        "--un-drop-shadow": w,
        "--un-grayscale": w,
        "--un-hue-rotate": w,
        "--un-invert": w,
        "--un-saturate": w,
        "--un-sepia": w
    }, Bo = "var(--un-blur) var(--un-brightness) var(--un-contrast) var(--un-drop-shadow) var(--un-grayscale) var(--un-hue-rotate) var(--un-invert) var(--un-saturate) var(--un-sepia)", Xo = {
        "--un-backdrop-blur": w,
        "--un-backdrop-brightness": w,
        "--un-backdrop-contrast": w,
        "--un-backdrop-grayscale": w,
        "--un-backdrop-hue-rotate": w,
        "--un-backdrop-invert": w,
        "--un-backdrop-opacity": w,
        "--un-backdrop-saturate": w,
        "--un-backdrop-sepia": w
    }, Yo = "var(--un-backdrop-blur) var(--un-backdrop-brightness) var(--un-backdrop-contrast) var(--un-backdrop-grayscale) var(--un-backdrop-hue-rotate) var(--un-backdrop-invert) var(--un-backdrop-opacity) var(--un-backdrop-saturate) var(--un-backdrop-sepia)";
    function Po(e) {
        let t = v.bracket.cssvar(e || "");
        if (t != null || (t = e ? v.percent(e) : "1", t != null && Number.parseFloat(t) <= 1)) return t;
    }
    function F(i, o) {
        return ([ , e, t ], {
            theme: r
        }) => {
            let n = o(t, r) ?? (t === "none" ? "0" : "");
            if (n !== "") return e ? {
                [`--un-${e}${i}`]: `${i}(${n})`,
                "-webkit-backdrop-filter": Yo,
                "backdrop-filter": Yo
            } : {
                [`--un-${i}`]: `${i}(${n})`,
                filter: Bo
            };
        };
    }
    function Io([ , e ], {
        theme: t
    }) {
        let r = t.dropShadow?.[e || "DEFAULT"];
        if (r != null) return {
            "--un-drop-shadow": `drop-shadow(${Lt(r, "--un-drop-shadow-color").join(") drop-shadow(")})`,
            filter: Bo
        };
        if (r = v.bracket.cssvar(e), r != null) return {
            "--un-drop-shadow": `drop-shadow(${r})`,
            filter: Bo
        };
    }
    var qo = [ [ /^(?:(backdrop-)|filter-)?blur(?:-(.+))?$/, F("blur", (e, t) => t.blur?.[e || "DEFAULT"] || v.bracket.cssvar.px(e)), {
        autocomplete: [ "(backdrop|filter)-blur-$blur", "blur-$blur", "filter-blur" ]
    } ], [ /^(?:(backdrop-)|filter-)?brightness-(.+)$/, F("brightness", e => v.bracket.cssvar.percent(e)), {
        autocomplete: [ "(backdrop|filter)-brightness-<percent>", "brightness-<percent>" ]
    } ], [ /^(?:(backdrop-)|filter-)?contrast-(.+)$/, F("contrast", e => v.bracket.cssvar.percent(e)), {
        autocomplete: [ "(backdrop|filter)-contrast-<percent>", "contrast-<percent>" ]
    } ], [ /^(?:filter-)?drop-shadow(?:-(.+))?$/, Io, {
        autocomplete: [ "filter-drop", "filter-drop-shadow", "filter-drop-shadow-color", "drop-shadow", "drop-shadow-color", "filter-drop-shadow-$dropShadow", "drop-shadow-$dropShadow", "filter-drop-shadow-color-$colors", "drop-shadow-color-$colors", "filter-drop-shadow-color-(op|opacity)", "drop-shadow-color-(op|opacity)", "filter-drop-shadow-color-(op|opacity)-<percent>", "drop-shadow-color-(op|opacity)-<percent>" ]
    } ], [ /^(?:filter-)?drop-shadow-color-(.+)$/, g("--un-drop-shadow-color", "drop-shadow", "shadowColor") ], [ /^(?:filter-)?drop-shadow-color-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-drop-shadow-opacity": v.bracket.percent(e)
    }) ], [ /^(?:(backdrop-)|filter-)?grayscale(?:-(.+))?$/, F("grayscale", Po), {
        autocomplete: [ "(backdrop|filter)-grayscale", "(backdrop|filter)-grayscale-<percent>", "grayscale-<percent>" ]
    } ], [ /^(?:(backdrop-)|filter-)?hue-rotate-(.+)$/, F("hue-rotate", e => v.bracket.cssvar.degree(e)) ], [ /^(?:(backdrop-)|filter-)?invert(?:-(.+))?$/, F("invert", Po), {
        autocomplete: [ "(backdrop|filter)-invert", "(backdrop|filter)-invert-<percent>", "invert-<percent>" ]
    } ], [ /^(backdrop-)op(?:acity)?-(.+)$/, F("opacity", e => v.bracket.cssvar.percent(e)), {
        autocomplete: [ "backdrop-(op|opacity)", "backdrop-(op|opacity)-<percent>" ]
    } ], [ /^(?:(backdrop-)|filter-)?saturate-(.+)$/, F("saturate", e => v.bracket.cssvar.percent(e)), {
        autocomplete: [ "(backdrop|filter)-saturate", "(backdrop|filter)-saturate-<percent>", "saturate-<percent>" ]
    } ], [ /^(?:(backdrop-)|filter-)?sepia(?:-(.+))?$/, F("sepia", Po), {
        autocomplete: [ "(backdrop|filter)-sepia", "(backdrop|filter)-sepia-<percent>", "sepia-<percent>" ]
    } ], [ "filter", {
        filter: Bo
    } ], [ "backdrop-filter", {
        "-webkit-backdrop-filter": Yo,
        "backdrop-filter": Yo
    } ], [ "filter-none", {
        filter: "none"
    } ], [ "backdrop-filter-none", {
        "-webkit-backdrop-filter": "none",
        "backdrop-filter": "none"
    } ], ...i.map(e => [ `filter-${e}`, {
        filter: e
    } ]), ...i.map(e => [ `backdrop-filter-${e}`, {
        "-webkit-backdrop-filter": e,
        "backdrop-filter": e
    } ]) ];
    var Mo = [ [ /^line-clamp-(\d+)$/, ([ , e ]) => ({
        overflow: "hidden",
        display: "-webkit-box",
        "-webkit-box-orient": "vertical",
        "-webkit-line-clamp": e,
        "line-clamp": e
    }), {
        autocomplete: [ "line-clamp", "line-clamp-<num>" ]
    } ], ...[ "none", ...i ].map(e => [ `line-clamp-${e}`, {
        overflow: "visible",
        display: "block",
        "-webkit-box-orient": "horizontal",
        "-webkit-line-clamp": e,
        "line-clamp": e
    } ]) ];
    var Zo = [ [ /^\$ placeholder-(.+)$/, g("color", "placeholder", "accentColor"), {
        autocomplete: "placeholder-$colors"
    } ], [ /^\$ placeholder-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-placeholder-opacity": v.bracket.percent(e)
    }), {
        autocomplete: [ "placeholder-(op|opacity)", "placeholder-(op|opacity)-<percent>" ]
    } ] ];
    var Ho = {
        "--un-scroll-snap-strictness": "proximity"
    }, Go = [ [ /^snap-(x|y)$/, ([ , e ]) => ({
        "scroll-snap-type": `${e} var(--un-scroll-snap-strictness)`
    }), {
        autocomplete: "snap-(x|y|both)"
    } ], [ /^snap-both$/, () => ({
        "scroll-snap-type": "both var(--un-scroll-snap-strictness)"
    }) ], [ "snap-mandatory", {
        "--un-scroll-snap-strictness": "mandatory"
    } ], [ "snap-proximity", {
        "--un-scroll-snap-strictness": "proximity"
    } ], [ "snap-none", {
        "scroll-snap-type": "none"
    } ], [ "snap-start", {
        "scroll-snap-align": "start"
    } ], [ "snap-end", {
        "scroll-snap-align": "end"
    } ], [ "snap-center", {
        "scroll-snap-align": "center"
    } ], [ "snap-align-none", {
        "scroll-snap-align": "none"
    } ], [ "snap-normal", {
        "scroll-snap-stop": "normal"
    } ], [ "snap-always", {
        "scroll-snap-stop": "always"
    } ], [ /^scroll-ma?()-?(.+)$/, e("scroll-margin"), {
        autocomplete: [ "scroll-(m|p|ma|pa|block|inline)", "scroll-(m|p|ma|pa|block|inline)-$spacing", "scroll-(m|p|ma|pa|block|inline)-(x|y|r|l|t|b|bs|be|is|ie)", "scroll-(m|p|ma|pa|block|inline)-(x|y|r|l|t|b|bs|be|is|ie)-$spacing" ]
    } ], [ /^scroll-m-?([xy])-?(.+)$/, e("scroll-margin") ], [ /^scroll-m-?([rltb])-?(.+)$/, e("scroll-margin") ], [ /^scroll-m-(block|inline)-(.+)$/, e("scroll-margin") ], [ /^scroll-m-?([bi][se])-?(.+)$/, e("scroll-margin") ], [ /^scroll-pa?()-?(.+)$/, e("scroll-padding") ], [ /^scroll-p-?([xy])-?(.+)$/, e("scroll-padding") ], [ /^scroll-p-?([rltb])-?(.+)$/, e("scroll-padding") ], [ /^scroll-p-(block|inline)-(.+)$/, e("scroll-padding") ], [ /^scroll-p-?([bi][se])-?(.+)$/, e("scroll-padding") ] ];
    var Jo = [ [ /^space-([xy])-(.+)$/, Ko, {
        autocomplete: [ "space-(x|y|block|inline)", "space-(x|y|block|inline)-reverse", "space-(x|y|block|inline)-$spacing" ]
    } ], [ /^space-([xy])-reverse$/, ([ , e ]) => ({
        [`--un-space-${e}-reverse`]: 1
    }) ], [ /^space-(block|inline)-(.+)$/, Ko ], [ /^space-(block|inline)-reverse$/, ([ , e ]) => ({
        [`--un-space-${e}-reverse`]: 1
    }) ] ];
    function Ko([ , n, e ], {
        theme: t
    }) {
        let i = t.spacing?.[e || "DEFAULT"] ?? v.bracket.cssvar.auto.fraction.rem(e || "1");
        if (i != null) {
            i === "0" && (i = "0px");
            let e = d[n].map(e => {
                let t = `margin${e}`, r = e.endsWith("right") || e.endsWith("bottom") ? `calc(${i} * var(--un-space-${n}-reverse))` : `calc(${i} * calc(1 - var(--un-space-${n}-reverse)))`;
                return [ t, r ];
            });
            if (e) return [ [ `--un-space-${n}-reverse`, 0 ], ...e ];
        }
    }
    var Vo = [ [ "uppercase", {
        "text-transform": "uppercase"
    } ], [ "lowercase", {
        "text-transform": "lowercase"
    } ], [ "capitalize", {
        "text-transform": "capitalize"
    } ], [ "normal-case", {
        "text-transform": "none"
    } ] ], Qo = [ ...[ "manual", "auto", "none", ...i ].map(e => [ `hyphens-${e}`, {
        "-webkit-hyphens": e,
        "-ms-hyphens": e,
        hyphens: e
    } ]) ], ea = [ [ "write-vertical-right", {
        "writing-mode": "vertical-rl"
    } ], [ "write-vertical-left", {
        "writing-mode": "vertical-lr"
    } ], [ "write-normal", {
        "writing-mode": "horizontal-tb"
    } ], ...h("write", "writing-mode") ], ta = [ [ "write-orient-mixed", {
        "text-orientation": "mixed"
    } ], [ "write-orient-sideways", {
        "text-orientation": "sideways"
    } ], [ "write-orient-upright", {
        "text-orientation": "upright"
    } ], ...h("write-orient", "text-orientation") ], ra = [ [ "sr-only", {
        position: "absolute",
        width: "1px",
        height: "1px",
        padding: "0",
        margin: "-1px",
        overflow: "hidden",
        clip: "rect(0,0,0,0)",
        "white-space": "nowrap",
        "border-width": 0
    } ], [ "not-sr-only", {
        position: "static",
        width: "auto",
        height: "auto",
        padding: "0",
        margin: "0",
        overflow: "visible",
        clip: "auto",
        "white-space": "normal"
    } ] ], na = [ [ "isolate", {
        isolation: "isolate"
    } ], [ "isolate-auto", {
        isolation: "auto"
    } ], [ "isolation-auto", {
        isolation: "auto"
    } ] ], ia = [ [ "object-cover", {
        "object-fit": "cover"
    } ], [ "object-contain", {
        "object-fit": "contain"
    } ], [ "object-fill", {
        "object-fit": "fill"
    } ], [ "object-scale-down", {
        "object-fit": "scale-down"
    } ], [ "object-none", {
        "object-fit": "none"
    } ], [ /^object-(.+)$/, ([ , e ]) => {
        if (r[e]) return {
            "object-position": r[e]
        };
        if (v.bracketOfPosition(e) != null) return {
            "object-position": v.bracketOfPosition(e).split(" ").map(e => v.position.fraction.auto.px.cssvar(e) ?? e).join(" ")
        };
    }, {
        autocomplete: `object-(${Object.keys(r).join("|")})`
    } ] ], oa = [ [ "bg-blend-multiply", {
        "background-blend-mode": "multiply"
    } ], [ "bg-blend-screen", {
        "background-blend-mode": "screen"
    } ], [ "bg-blend-overlay", {
        "background-blend-mode": "overlay"
    } ], [ "bg-blend-darken", {
        "background-blend-mode": "darken"
    } ], [ "bg-blend-lighten", {
        "background-blend-mode": "lighten"
    } ], [ "bg-blend-color-dodge", {
        "background-blend-mode": "color-dodge"
    } ], [ "bg-blend-color-burn", {
        "background-blend-mode": "color-burn"
    } ], [ "bg-blend-hard-light", {
        "background-blend-mode": "hard-light"
    } ], [ "bg-blend-soft-light", {
        "background-blend-mode": "soft-light"
    } ], [ "bg-blend-difference", {
        "background-blend-mode": "difference"
    } ], [ "bg-blend-exclusion", {
        "background-blend-mode": "exclusion"
    } ], [ "bg-blend-hue", {
        "background-blend-mode": "hue"
    } ], [ "bg-blend-saturation", {
        "background-blend-mode": "saturation"
    } ], [ "bg-blend-color", {
        "background-blend-mode": "color"
    } ], [ "bg-blend-luminosity", {
        "background-blend-mode": "luminosity"
    } ], [ "bg-blend-normal", {
        "background-blend-mode": "normal"
    } ], ...h("bg-blend", "background-blend") ], aa = [ [ "mix-blend-multiply", {
        "mix-blend-mode": "multiply"
    } ], [ "mix-blend-screen", {
        "mix-blend-mode": "screen"
    } ], [ "mix-blend-overlay", {
        "mix-blend-mode": "overlay"
    } ], [ "mix-blend-darken", {
        "mix-blend-mode": "darken"
    } ], [ "mix-blend-lighten", {
        "mix-blend-mode": "lighten"
    } ], [ "mix-blend-color-dodge", {
        "mix-blend-mode": "color-dodge"
    } ], [ "mix-blend-color-burn", {
        "mix-blend-mode": "color-burn"
    } ], [ "mix-blend-hard-light", {
        "mix-blend-mode": "hard-light"
    } ], [ "mix-blend-soft-light", {
        "mix-blend-mode": "soft-light"
    } ], [ "mix-blend-difference", {
        "mix-blend-mode": "difference"
    } ], [ "mix-blend-exclusion", {
        "mix-blend-mode": "exclusion"
    } ], [ "mix-blend-hue", {
        "mix-blend-mode": "hue"
    } ], [ "mix-blend-saturation", {
        "mix-blend-mode": "saturation"
    } ], [ "mix-blend-color", {
        "mix-blend-mode": "color"
    } ], [ "mix-blend-luminosity", {
        "mix-blend-mode": "luminosity"
    } ], [ "mix-blend-plus-lighter", {
        "mix-blend-mode": "plus-lighter"
    } ], [ "mix-blend-normal", {
        "mix-blend-mode": "normal"
    } ], ...h("mix-blend") ], sa = [ [ "min-h-dvh", {
        "min-height": "100dvh"
    } ], [ "min-h-svh", {
        "min-height": "100svh"
    } ], [ "min-h-lvh", {
        "min-height": "100lvh"
    } ], [ "h-dvh", {
        height: "100dvh"
    } ], [ "h-svh", {
        height: "100svh"
    } ], [ "h-lvh", {
        height: "100lvh"
    } ], [ "max-h-dvh", {
        "max-height": "100dvh"
    } ], [ "max-h-svh", {
        "max-height": "100svh"
    } ], [ "max-h-lvh", {
        "max-height": "100lvh"
    } ] ];
    var la = {
        "--un-border-spacing-x": 0,
        "--un-border-spacing-y": 0
    }, ca = "var(--un-border-spacing-x) var(--un-border-spacing-y)", fa = [ [ "inline-table", {
        display: "inline-table"
    } ], [ "table", {
        display: "table"
    } ], [ "table-caption", {
        display: "table-caption"
    } ], [ "table-cell", {
        display: "table-cell"
    } ], [ "table-column", {
        display: "table-column"
    } ], [ "table-column-group", {
        display: "table-column-group"
    } ], [ "table-footer-group", {
        display: "table-footer-group"
    } ], [ "table-header-group", {
        display: "table-header-group"
    } ], [ "table-row", {
        display: "table-row"
    } ], [ "table-row-group", {
        display: "table-row-group"
    } ], [ "border-collapse", {
        "border-collapse": "collapse"
    } ], [ "border-separate", {
        "border-collapse": "separate"
    } ], [ /^border-spacing-(.+)$/, ([ , e ], {
        theme: t
    }) => {
        let r = t.spacing?.[e] ?? v.bracket.cssvar.global.auto.fraction.rem(e);
        if (r != null) return {
            "--un-border-spacing-x": r,
            "--un-border-spacing-y": r,
            "border-spacing": ca
        };
    }, {
        autocomplete: [ "border-spacing", "border-spacing-$spacing" ]
    } ], [ /^border-spacing-([xy])-(.+)$/, ([ , e, t ], {
        theme: r
    }) => {
        let n = r.spacing?.[t] ?? v.bracket.cssvar.global.auto.fraction.rem(t);
        if (n != null) return {
            [`--un-border-spacing-${e}`]: n,
            "border-spacing": ca
        };
    }, {
        autocomplete: [ "border-spacing-(x|y)", "border-spacing-(x|y)-$spacing" ]
    } ], [ "caption-top", {
        "caption-side": "top"
    } ], [ "caption-bottom", {
        "caption-side": "bottom"
    } ], [ "table-auto", {
        "table-layout": "auto"
    } ], [ "table-fixed", {
        "table-layout": "fixed"
    } ], [ "table-empty-cells-visible", {
        "empty-cells": "show"
    } ], [ "table-empty-cells-hidden", {
        "empty-cells": "hide"
    } ] ];
    var da = {
        "--un-pan-x": w,
        "--un-pan-y": w,
        "--un-pinch-zoom": w
    }, ua = "var(--un-pan-x) var(--un-pan-y) var(--un-pinch-zoom)", pa = [ [ /^touch-pan-(x|left|right)$/, ([ , e ]) => ({
        "--un-pan-x": `pan-${e}`,
        "touch-action": ua
    }), {
        autocomplete: [ "touch-pan", "touch-pan-(x|left|right|y|up|down)" ]
    } ], [ /^touch-pan-(y|up|down)$/, ([ , e ]) => ({
        "--un-pan-y": `pan-${e}`,
        "touch-action": ua
    }) ], [ "touch-pinch-zoom", {
        "--un-pinch-zoom": "pinch-zoom",
        "touch-action": ua
    } ], [ "touch-auto", {
        "touch-action": "auto"
    } ], [ "touch-manipulation", {
        "touch-action": "manipulation"
    } ], [ "touch-none", {
        "touch-action": "none"
    } ], ...h("touch", "touch-action") ];
    var ma = {
        "--un-ordinal": w,
        "--un-slashed-zero": w,
        "--un-numeric-figure": w,
        "--un-numeric-spacing": w,
        "--un-numeric-fraction": w
    };
    function A(e) {
        return {
            ...e,
            "font-variant-numeric": "var(--un-ordinal) var(--un-slashed-zero) var(--un-numeric-figure) var(--un-numeric-spacing) var(--un-numeric-fraction)"
        };
    }
    var ga = [ [ /^ordinal$/, () => A({
        "--un-ordinal": "ordinal"
    }), {
        autocomplete: "ordinal"
    } ], [ /^slashed-zero$/, () => A({
        "--un-slashed-zero": "slashed-zero"
    }), {
        autocomplete: "slashed-zero"
    } ], [ /^lining-nums$/, () => A({
        "--un-numeric-figure": "lining-nums"
    }), {
        autocomplete: "lining-nums"
    } ], [ /^oldstyle-nums$/, () => A({
        "--un-numeric-figure": "oldstyle-nums"
    }), {
        autocomplete: "oldstyle-nums"
    } ], [ /^proportional-nums$/, () => A({
        "--un-numeric-spacing": "proportional-nums"
    }), {
        autocomplete: "proportional-nums"
    } ], [ /^tabular-nums$/, () => A({
        "--un-numeric-spacing": "tabular-nums"
    }), {
        autocomplete: "tabular-nums"
    } ], [ /^diagonal-fractions$/, () => A({
        "--un-numeric-fraction": "diagonal-fractions"
    }), {
        autocomplete: "diagonal-fractions"
    } ], [ /^stacked-fractions$/, () => A({
        "--un-numeric-fraction": "stacked-fractions"
    }), {
        autocomplete: "stacked-fractions"
    } ], [ "normal-nums", {
        "font-variant-numeric": "normal"
    } ] ];
    var ha = {
        "bg-blend": "background-blend-mode",
        "bg-clip": "-webkit-background-clip",
        "bg-gradient": "linear-gradient",
        "bg-image": "background-image",
        "bg-origin": "background-origin",
        "bg-position": "background-position",
        "bg-repeat": "background-repeat",
        "bg-size": "background-size",
        "mix-blend": "mix-blend-mode",
        object: "object-fit",
        "object-position": "object-position",
        write: "writing-mode",
        "write-orient": "text-orientation"
    }, ba = [ [ /^(.+?)-(\$.+)$/, ([ , e, t ]) => {
        let r = ha[e];
        if (r) return {
            [r]: v.cssvar(t)
        };
    } ] ];
    var xa = [ [ /^view-transition-([\w-]+)$/, ([ , e ]) => ({
        "view-transition-name": e
    }) ] ];
    var va = [ Un, ba, On, Uo, Tr, ra, Rr, Ur, vr, Sr, Mo, na, Fr, $r, hr, Cr, fn, Ar, Wr, ln, on, fr, fa, xn, yo, Or, pa, Br, Dr, Go, So, Xt, No, wr, kr, yr, pr, zr, Jo, To, xr, Lo, _o, Mr, Xr, Ir, It, nr, Eo, ir, dn, ia, cn, Et, Sn, qr, wt, En, Zr, Vo, Hr, ga, sr, Gr, jn, Cn, Fn, Qo, ea, ta, Fo, Co, Kt, oa, aa, tn, Rt, Kr, Ao, qo, zn, Pt, Yr, Pr, Zo, or, xa, sa, Lr ].flat(1);
    var ya = [ ...Oo ];
    var $a = {
        inherit: "inherit",
        current: "currentColor",
        transparent: "transparent",
        black: "#000",
        white: "#fff",
        rose: {
            50: "#fff1f2",
            100: "#ffe4e6",
            200: "#fecdd3",
            300: "#fda4af",
            400: "#fb7185",
            500: "#f43f5e",
            600: "#e11d48",
            700: "#be123c",
            800: "#9f1239",
            900: "#881337",
            950: "#4c0519"
        },
        pink: {
            50: "#fdf2f8",
            100: "#fce7f3",
            200: "#fbcfe8",
            300: "#f9a8d4",
            400: "#f472b6",
            500: "#ec4899",
            600: "#db2777",
            700: "#be185d",
            800: "#9d174d",
            900: "#831843",
            950: "#500724"
        },
        fuchsia: {
            50: "#fdf4ff",
            100: "#fae8ff",
            200: "#f5d0fe",
            300: "#f0abfc",
            400: "#e879f9",
            500: "#d946ef",
            600: "#c026d3",
            700: "#a21caf",
            800: "#86198f",
            900: "#701a75",
            950: "#4a044e"
        },
        purple: {
            50: "#faf5ff",
            100: "#f3e8ff",
            200: "#e9d5ff",
            300: "#d8b4fe",
            400: "#c084fc",
            500: "#a855f7",
            600: "#9333ea",
            700: "#7e22ce",
            800: "#6b21a8",
            900: "#581c87",
            950: "#3b0764"
        },
        violet: {
            50: "#f5f3ff",
            100: "#ede9fe",
            200: "#ddd6fe",
            300: "#c4b5fd",
            400: "#a78bfa",
            500: "#8b5cf6",
            600: "#7c3aed",
            700: "#6d28d9",
            800: "#5b21b6",
            900: "#4c1d95",
            950: "#2e1065"
        },
        indigo: {
            50: "#eef2ff",
            100: "#e0e7ff",
            200: "#c7d2fe",
            300: "#a5b4fc",
            400: "#818cf8",
            500: "#6366f1",
            600: "#4f46e5",
            700: "#4338ca",
            800: "#3730a3",
            900: "#312e81",
            950: "#1e1b4b"
        },
        blue: {
            50: "#eff6ff",
            100: "#dbeafe",
            200: "#bfdbfe",
            300: "#93c5fd",
            400: "#60a5fa",
            500: "#3b82f6",
            600: "#2563eb",
            700: "#1d4ed8",
            800: "#1e40af",
            900: "#1e3a8a",
            950: "#172554"
        },
        sky: {
            50: "#f0f9ff",
            100: "#e0f2fe",
            200: "#bae6fd",
            300: "#7dd3fc",
            400: "#38bdf8",
            500: "#0ea5e9",
            600: "#0284c7",
            700: "#0369a1",
            800: "#075985",
            900: "#0c4a6e",
            950: "#082f49"
        },
        cyan: {
            50: "#ecfeff",
            100: "#cffafe",
            200: "#a5f3fc",
            300: "#67e8f9",
            400: "#22d3ee",
            500: "#06b6d4",
            600: "#0891b2",
            700: "#0e7490",
            800: "#155e75",
            900: "#164e63",
            950: "#083344"
        },
        teal: {
            50: "#f0fdfa",
            100: "#ccfbf1",
            200: "#99f6e4",
            300: "#5eead4",
            400: "#2dd4bf",
            500: "#14b8a6",
            600: "#0d9488",
            700: "#0f766e",
            800: "#115e59",
            900: "#134e4a",
            950: "#042f2e"
        },
        emerald: {
            50: "#ecfdf5",
            100: "#d1fae5",
            200: "#a7f3d0",
            300: "#6ee7b7",
            400: "#34d399",
            500: "#10b981",
            600: "#059669",
            700: "#047857",
            800: "#065f46",
            900: "#064e3b",
            950: "#022c22"
        },
        green: {
            50: "#f0fdf4",
            100: "#dcfce7",
            200: "#bbf7d0",
            300: "#86efac",
            400: "#4ade80",
            500: "#22c55e",
            600: "#16a34a",
            700: "#15803d",
            800: "#166534",
            900: "#14532d",
            950: "#052e16"
        },
        lime: {
            50: "#f7fee7",
            100: "#ecfccb",
            200: "#d9f99d",
            300: "#bef264",
            400: "#a3e635",
            500: "#84cc16",
            600: "#65a30d",
            700: "#4d7c0f",
            800: "#3f6212",
            900: "#365314",
            950: "#1a2e05"
        },
        yellow: {
            50: "#fefce8",
            100: "#fef9c3",
            200: "#fef08a",
            300: "#fde047",
            400: "#facc15",
            500: "#eab308",
            600: "#ca8a04",
            700: "#a16207",
            800: "#854d0e",
            900: "#713f12",
            950: "#422006"
        },
        amber: {
            50: "#fffbeb",
            100: "#fef3c7",
            200: "#fde68a",
            300: "#fcd34d",
            400: "#fbbf24",
            500: "#f59e0b",
            600: "#d97706",
            700: "#b45309",
            800: "#92400e",
            900: "#78350f",
            950: "#451a03"
        },
        orange: {
            50: "#fff7ed",
            100: "#ffedd5",
            200: "#fed7aa",
            300: "#fdba74",
            400: "#fb923c",
            500: "#f97316",
            600: "#ea580c",
            700: "#c2410c",
            800: "#9a3412",
            900: "#7c2d12",
            950: "#431407"
        },
        red: {
            50: "#fef2f2",
            100: "#fee2e2",
            200: "#fecaca",
            300: "#fca5a5",
            400: "#f87171",
            500: "#ef4444",
            600: "#dc2626",
            700: "#b91c1c",
            800: "#991b1b",
            900: "#7f1d1d",
            950: "#450a0a"
        },
        gray: {
            50: "#f9fafb",
            100: "#f3f4f6",
            200: "#e5e7eb",
            300: "#d1d5db",
            400: "#9ca3af",
            500: "#6b7280",
            600: "#4b5563",
            700: "#374151",
            800: "#1f2937",
            900: "#111827",
            950: "#030712"
        },
        slate: {
            50: "#f8fafc",
            100: "#f1f5f9",
            200: "#e2e8f0",
            300: "#cbd5e1",
            400: "#94a3b8",
            500: "#64748b",
            600: "#475569",
            700: "#334155",
            800: "#1e293b",
            900: "#0f172a",
            950: "#020617"
        },
        zinc: {
            50: "#fafafa",
            100: "#f4f4f5",
            200: "#e4e4e7",
            300: "#d4d4d8",
            400: "#a1a1aa",
            500: "#71717a",
            600: "#52525b",
            700: "#3f3f46",
            800: "#27272a",
            900: "#18181b",
            950: "#09090b"
        },
        neutral: {
            50: "#fafafa",
            100: "#f5f5f5",
            200: "#e5e5e5",
            300: "#d4d4d4",
            400: "#a3a3a3",
            500: "#737373",
            600: "#525252",
            700: "#404040",
            800: "#262626",
            900: "#171717",
            950: "#0a0a0a"
        },
        stone: {
            50: "#fafaf9",
            100: "#f5f5f4",
            200: "#e7e5e4",
            300: "#d6d3d1",
            400: "#a8a29e",
            500: "#78716c",
            600: "#57534e",
            700: "#44403c",
            800: "#292524",
            900: "#1c1917",
            950: "#0c0a09"
        },
        light: {
            50: "#fdfdfd",
            100: "#fcfcfc",
            200: "#fafafa",
            300: "#f8f9fa",
            400: "#f6f6f6",
            500: "#f2f2f2",
            600: "#f1f3f5",
            700: "#e9ecef",
            800: "#dee2e6",
            900: "#dde1e3",
            950: "#d8dcdf"
        },
        dark: {
            50: "#4a4a4a",
            100: "#3c3c3c",
            200: "#323232",
            300: "#2d2d2d",
            400: "#222222",
            500: "#1f1f1f",
            600: "#1c1c1e",
            700: "#1b1b1b",
            800: "#181818",
            900: "#0f0f0f",
            950: "#080808"
        },
        get lightblue() {
            return this.sky;
        },
        get lightBlue() {
            return this.sky;
        },
        get warmgray() {
            return this.stone;
        },
        get warmGray() {
            return this.stone;
        },
        get truegray() {
            return this.neutral;
        },
        get trueGray() {
            return this.neutral;
        },
        get coolgray() {
            return this.gray;
        },
        get coolGray() {
            return this.gray;
        },
        get bluegray() {
            return this.slate;
        },
        get blueGray() {
            return this.slate;
        }
    };
    Object.values($a).forEach(r => {
        typeof r != "string" && r !== void 0 && (r.DEFAULT = r.DEFAULT || r[400], 
        Object.keys(r).forEach(e => {
            let t = +e / 100;
            t === Math.round(t) && (r[t] = r[e]);
        }));
    });
    var ka = {
        l: [ "-left" ],
        r: [ "-right" ],
        t: [ "-top" ],
        b: [ "-bottom" ],
        s: [ "-inline-start" ],
        e: [ "-inline-end" ],
        x: [ "-left", "-right" ],
        y: [ "-top", "-bottom" ],
        "": [ "" ],
        bs: [ "-block-start" ],
        be: [ "-block-end" ],
        is: [ "-inline-start" ],
        ie: [ "-inline-end" ],
        block: [ "-block-start", "-block-end" ],
        inline: [ "-inline-start", "-inline-end" ]
    }, wa = {
        ...ka,
        s: [ "-inset-inline-start" ],
        start: [ "-inset-inline-start" ],
        e: [ "-inset-inline-end" ],
        end: [ "-inset-inline-end" ],
        bs: [ "-inset-block-start" ],
        be: [ "-inset-block-end" ],
        is: [ "-inset-inline-start" ],
        ie: [ "-inset-inline-end" ],
        block: [ "-inset-block-start", "-inset-block-end" ],
        inline: [ "-inset-inline-start", "-inset-inline-end" ]
    };
    var za = {
        x: [ "-x" ],
        y: [ "-y" ],
        z: [ "-z" ],
        "": [ "-x", "-y" ]
    }, Ea = [ "x", "y", "z" ], ja = [ "top", "top center", "top left", "top right", "bottom", "bottom center", "bottom left", "bottom right", "left", "left center", "left top", "left bottom", "right", "right center", "right top", "right bottom", "center", "center top", "center bottom", "center left", "center right", "center center" ], Sa = Object.assign({}, ...ja.map(e => ({
        [e.replace(/ /, "-")]: e
    })), ...ja.map(e => ({
        [e.replace(/\b(\w)\w+/g, "$1").replace(/ /, "")]: e
    }))), Ca = [ "inherit", "initial", "revert", "revert-layer", "unset" ], Fa = /^(calc|clamp|min|max)\s*\((.+)\)(.*)/;
    var Aa = /^(-?\d*(?:\.\d+)?)(px|pt|pc|%|r?(?:em|ex|lh|cap|ch|ic)|(?:[sld]?v|cq)(?:[whib]|min|max)|in|cm|mm|rpx)?$/i, La = /^(-?\d*(?:\.\d+)?)$/, _a = /^(px|[sld]?v[wh])$/i, Na = {
        px: 1,
        vw: 100,
        vh: 100,
        svw: 100,
        svh: 100,
        dvw: 100,
        dvh: 100,
        lvh: 100,
        lvw: 100
    }, Wa = /^\[(color|image|length|size|position|quoted|string):/i, Ua = /,(?![^()]*\))/g, Oa = [ "color", "border-color", "background-color", "outline-color", "text-decoration-color", "flex-grow", "flex", "flex-shrink", "caret-color", "font", "gap", "opacity", "visibility", "z-index", "font-weight", "zoom", "text-shadow", "transform", "box-shadow", "border", "background-position", "left", "right", "top", "bottom", "object-position", "max-height", "min-height", "max-width", "min-width", "height", "width", "border-width", "margin", "padding", "outline-width", "outline-offset", "font-size", "line-height", "text-indent", "vertical-align", "border-spacing", "letter-spacing", "word-spacing", "stroke", "filter", "backdrop-filter", "fill", "mask", "mask-size", "mask-border", "clip-path", "clip", "border-radius" ];
    function L(e) {
        return +e.toFixed(10);
    }
    function Ta(e) {
        let t = e.match(Aa);
        if (!t) return;
        let [ , r, n ] = t, i = Number.parseFloat(r);
        if (n && !Number.isNaN(i)) return `${L(i)}${n}`;
    }
    function Ra(e) {
        if (e === "auto" || e === "a") return "auto";
    }
    function Da(e) {
        if (!e) return;
        if (_a.test(e)) return `${Na[e]}${e}`;
        let t = e.match(Aa);
        if (!t) return;
        let [ , r, n ] = t, i = Number.parseFloat(r);
        if (!Number.isNaN(i)) return i === 0 ? "0" : n ? `${L(i)}${n}` : `${L(i / 4)}rem`;
    }
    function Ba(e) {
        if (_a.test(e)) return `${Na[e]}${e}`;
        let t = e.match(Aa);
        if (!t) return;
        let [ , r, n ] = t, i = Number.parseFloat(r);
        if (!Number.isNaN(i)) return n ? `${L(i)}${n}` : `${L(i)}px`;
    }
    function Xa(e) {
        if (!La.test(e)) return;
        let t = Number.parseFloat(e);
        if (!Number.isNaN(t)) return L(t);
    }
    function Ya(e) {
        if (e.endsWith("%") && (e = e.slice(0, -1)), !La.test(e)) return;
        let t = Number.parseFloat(e);
        if (!Number.isNaN(t)) return `${L(t / 100)}`;
    }
    function Pa(e) {
        if (!e) return;
        if (e === "full") return "100%";
        let [ t, r ] = e.split("/"), n = Number.parseFloat(t) / Number.parseFloat(r);
        if (!Number.isNaN(n)) return n === 0 ? "0" : `${L(n * 100)}%`;
    }
    function Ia(i, o) {
        if (i && i.startsWith("[") && i.endsWith("]")) {
            let t, e, r = i.match(Wa);
            if (r ? (o || (e = r[1]), t = i.slice(r[0].length, -1)) : t = i.slice(1, -1), 
            !t || t === '=""') return;
            t.startsWith("--") && (t = `var(${t})`);
            let n = 0;
            for (let e of t) if (e === "[") n += 1; else if (e === "]" && (n -= 1, 
            n < 0)) return;
            if (n) return;
            switch (e) {
              case "string":
                return t.replace(/(^|[^\\])_/g, "$1 ").replace(/\\_/g, "_");

              case "quoted":
                return t.replace(/(^|[^\\])_/g, "$1 ").replace(/\\_/g, "_").replace(/(["\\])/g, "\\$1").replace(/^(.+)$/, '"$1"');
            }
            return t.replace(/(url\(.*?\))/g, e => e.replace(/_/g, "\\_")).replace(/(^|[^\\])_/g, "$1 ").replace(/\\_/g, "_").replace(/(?:calc|clamp|max|min)\((.*)/g, e => {
                let r = [];
                return e.replace(/var\((--.+?)[,)]/g, (e, t) => (r.push(t), e.replace(t, "--un-calc"))).replace(/(-?\d*\.?\d(?!-\d.+[,)](?![^+\-/*])\D)(?:%|[a-z]+)?|\))([+\-/*])/g, "$1 $2 ").replace(/--un-calc/g, () => r.shift());
            });
        }
    }
    function qa(e) {
        return Ia(e);
    }
    function Ma(e) {
        return Ia(e, "color");
    }
    function Za(e) {
        return Ia(e, "length");
    }
    function Ha(e) {
        return Ia(e, "position");
    }
    function Ga(r) {
        if (/^\$[^\s'"`;{}]/.test(r)) {
            let [ e, t ] = r.slice(1).split(",");
            return `var(--${x(e)}${t ? `, ${t}` : ""})`;
        }
    }
    function Ja(e) {
        let t = e.match(/^(-?[0-9.]+)(s|ms)?$/i);
        if (!t) return;
        let [ , r, n ] = t, i = Number.parseFloat(r);
        if (!Number.isNaN(i)) return i === 0 && !n ? "0s" : n ? `${L(i)}${n}` : `${L(i)}ms`;
    }
    function Ka(e) {
        let t = e.match(/^(-?[0-9.]+)(deg|rad|grad|turn)?$/i);
        if (!t) return;
        let [ , r, n ] = t, i = Number.parseFloat(r);
        if (!Number.isNaN(i)) return i === 0 ? "0" : n ? `${L(i)}${n}` : `${L(i)}deg`;
    }
    function Va(e) {
        if (Ca.includes(e)) return e;
    }
    function Qa(e) {
        if (e.split(",").every(e => Oa.includes(e))) return e;
    }
    function es(e) {
        if ([ "top", "left", "right", "bottom", "center" ].includes(e)) return e;
    }
    var ts = {
        __proto__: null,
        auto: Ra,
        bracket: qa,
        bracketOfColor: Ma,
        bracketOfLength: Za,
        bracketOfPosition: Ha,
        cssvar: Ga,
        degree: Ka,
        fraction: Pa,
        global: Va,
        number: Xa,
        numberWithUnit: Ta,
        percent: Ya,
        position: es,
        properties: Qa,
        px: Ba,
        rem: Da,
        time: Ja
    }, rs = me(ts), _ = rs;
    function ns(e, r, t = "colors") {
        let n = e[t], i = -1;
        for (let t of r) {
            if (i += 1, n && typeof n != "string") {
                let e = r.slice(i).join("-").replace(/(-[a-z])/g, e => e.slice(1).toUpperCase());
                if (n[e]) return n[e];
                if (n[t]) {
                    n = n[t];
                    continue;
                }
            }
            return;
        }
        return n;
    }
    function is(e, t, r) {
        return ns(e, t, r) || ns(e, t, "colors");
    }
    function os(e, t) {
        let [ r, n ] = l(e, "[", "]", [ "/", ":" ]) ?? [];
        if (r != null) {
            let e = (r.match(Wa) ?? [])[1];
            if (e == null || e === t) return [ r, n ];
        }
    }
    function as(e, r, n) {
        let t = os(e, "color");
        if (!t) return;
        let [ i, o ] = t, a = i.replace(/([a-z])(\d)/g, "$1-$2").split(/-/g), [ s ] = a;
        if (!s) return;
        let l, c = _.bracketOfColor(i), f = c || i;
        if (_.numberWithUnit(f)) return;
        if (/^#[\da-f]+$/i.test(f) ? l = f : /^hex-[\da-fA-F]+$/.test(f) ? l = `#${f.slice(4)}` : i.startsWith("$") && (l = _.cssvar(i)), 
        l = l || c, !l) {
            let e = is(r, [ i ], n);
            typeof e == "string" && (l = e);
        }
        let d = "DEFAULT";
        if (!l) {
            let e, [ t ] = a.slice(-1);
            /^\d+$/.test(t) ? (d = t, e = is(r, a.slice(0, -1), n), !e || typeof e == "string" ? l = void 0 : l = e[d]) : (e = is(r, a, n), 
            !e && a.length <= 2 && ([ , d = d ] = a, e = is(r, [ s ], n)), typeof e == "string" ? l = e : d && e && (l = e[d]));
        }
        return {
            opacity: o,
            name: s,
            no: d,
            color: l,
            cssColor: u(l),
            alpha: _.bracket.cssvar.percent(o ?? "")
        };
    }
    function ss(c, f, d, u) {
        return ([ , e ], {
            theme: t,
            generator: r
        }) => {
            let n = as(e, t, d);
            if (!n) return;
            let {
                alpha: i,
                color: o,
                cssColor: a
            } = n, s = r.config.envMode === "dev" && o ? ` /* ${o} */` : "", l = {};
            if (a) if (i != null) l[c] = m(a, i) + s; else {
                let e = `--un-${f}-opacity`, t = m(a, `var(${e})`);
                t.includes(e) && (l[e] = p(a)), l[c] = t + s;
            } else if (o) if (i != null) l[c] = m(o, i) + s; else {
                let e = `--un-${f}-opacity`, t = m(o, `var(${e})`);
                t.includes(e) && (l[e] = 1), l[c] = t + s;
            }
            if (u?.(l) !== !1) return l;
        };
    }
    function N(t, r) {
        return Ca.map(e => [ `${t}-${e}`, {
            [r ?? t]: e
        } ]);
    }
    function ls(e) {
        return e != null && Fa.test(e);
    }
    function cs(e, t, r) {
        let n = t.split(Ua);
        return e || !e && n.length === 1 ? za[e].map(e => [ `--un-${r}${e}`, t ]) : n.map((e, t) => [ `--un-${r}-${Ea[t]}`, e ]);
    }
    var fs = [ "auto", "default", "none", "context-menu", "help", "pointer", "progress", "wait", "cell", "crosshair", "text", "vertical-text", "alias", "copy", "move", "no-drop", "not-allowed", "grab", "grabbing", "all-scroll", "col-resize", "row-resize", "n-resize", "e-resize", "s-resize", "w-resize", "ne-resize", "nw-resize", "se-resize", "sw-resize", "ew-resize", "ns-resize", "nesw-resize", "nwse-resize", "zoom-in", "zoom-out" ];
    var ds = " ";
    var us = [ [ "visible", {
        visibility: "visible"
    } ], [ "invisible", {
        visibility: "hidden"
    } ], [ "backface-visible", {
        "backface-visibility": "visible"
    } ], [ "backface-hidden", {
        "backface-visibility": "hidden"
    } ], ...N("backface", "backface-visibility") ], ps = [ [ /^cursor-(.+)$/, ([ , e ]) => ({
        cursor: _.bracket.cssvar.global(e)
    }) ], ...fs.map(e => [ `cursor-${e}`, {
        cursor: e
    } ]) ];
    var ms = [ [ "pointer-events-auto", {
        "pointer-events": "auto"
    } ], [ "pointer-events-none", {
        "pointer-events": "none"
    } ], ...N("pointer-events") ], gs = [ [ "resize-x", {
        resize: "horizontal"
    } ], [ "resize-y", {
        resize: "vertical"
    } ], [ "resize", {
        resize: "both"
    } ], [ "resize-none", {
        resize: "none"
    } ], ...N("resize") ], hs = [ [ "select-auto", {
        "-webkit-user-select": "auto",
        "user-select": "auto"
    } ], [ "select-all", {
        "-webkit-user-select": "all",
        "user-select": "all"
    } ], [ "select-text", {
        "-webkit-user-select": "text",
        "user-select": "text"
    } ], [ "select-none", {
        "-webkit-user-select": "none",
        "user-select": "none"
    } ], ...N("select", "user-select") ];
    var bs = [ [ /^intrinsic-size-(.+)$/, ([ , e ]) => ({
        "contain-intrinsic-size": _.bracket.cssvar.global.fraction.rem(e)
    }), {
        autocomplete: "intrinsic-size-<num>"
    } ], [ "content-visibility-visible", {
        "content-visibility": "visible"
    } ], [ "content-visibility-hidden", {
        "content-visibility": "hidden"
    } ], [ "content-visibility-auto", {
        "content-visibility": "auto"
    } ], ...N("content-visibility") ];
    var xs = [ [ "case-upper", {
        "text-transform": "uppercase"
    } ], [ "case-lower", {
        "text-transform": "lowercase"
    } ], [ "case-capital", {
        "text-transform": "capitalize"
    } ], [ "case-normal", {
        "text-transform": "none"
    } ], ...N("case", "text-transform") ];
    var vs = {
        "--un-ring-inset": ds,
        "--un-ring-offset-width": "0px",
        "--un-ring-offset-color": "#fff",
        "--un-ring-width": "0px",
        "--un-ring-color": "rgb(147 197 253 / 0.5)",
        "--un-shadow": "0 0 rgb(0 0 0 / 0)"
    }, ys = [ [ /^ring(?:-(.+))?$/, ([ , e ], {
        theme: t
    }) => {
        let r = t.ringWidth?.[e || "DEFAULT"] ?? _.px(e || "1");
        if (r) return {
            "--un-ring-width": r,
            "--un-ring-offset-shadow": "var(--un-ring-inset) 0 0 0 var(--un-ring-offset-width) var(--un-ring-offset-color)",
            "--un-ring-shadow": "var(--un-ring-inset) 0 0 0 calc(var(--un-ring-width) + var(--un-ring-offset-width)) var(--un-ring-color)",
            "box-shadow": "var(--un-ring-offset-shadow), var(--un-ring-shadow), var(--un-shadow)"
        };
    }, {
        autocomplete: "ring-$ringWidth"
    } ], [ /^ring-(?:width-|size-)(.+)$/, $s, {
        autocomplete: "ring-(width|size)-$lineWidth"
    } ], [ "ring-offset", {
        "--un-ring-offset-width": "1px"
    } ], [ /^ring-offset-(?:width-|size-)?(.+)$/, ([ , e ], {
        theme: t
    }) => ({
        "--un-ring-offset-width": t.lineWidth?.[e] ?? _.bracket.cssvar.px(e)
    }), {
        autocomplete: "ring-offset-(width|size)-$lineWidth"
    } ], [ /^ring-(.+)$/, ks, {
        autocomplete: "ring-$colors"
    } ], [ /^ring-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-ring-opacity": _.bracket.percent.cssvar(e)
    }), {
        autocomplete: "ring-(op|opacity)-<percent>"
    } ], [ /^ring-offset-(.+)$/, ss("--un-ring-offset-color", "ring-offset", "borderColor"), {
        autocomplete: "ring-offset-$colors"
    } ], [ /^ring-offset-op(?:acity)?-?(.+)$/, ([ , e ]) => ({
        "--un-ring-offset-opacity": _.bracket.percent.cssvar(e)
    }), {
        autocomplete: "ring-offset-(op|opacity)-<percent>"
    } ], [ "ring-inset", {
        "--un-ring-inset": "inset"
    } ] ];
    function $s([ , e ], {
        theme: t
    }) {
        return {
            "--un-ring-width": t.ringWidth?.[e] ?? _.bracket.cssvar.px(e)
        };
    }
    function ks(e, t) {
        return ls(_.bracket(e[1])) ? $s(e, t) : ss("--un-ring-color", "ring", "borderColor")(e, t);
    }
    var ws = {
        "--un-ring-offset-shadow": "0 0 rgb(0 0 0 / 0)",
        "--un-ring-shadow": "0 0 rgb(0 0 0 / 0)",
        "--un-shadow-inset": ds,
        "--un-shadow": "0 0 rgb(0 0 0 / 0)"
    };
    var zs = [ "translate", "rotate", "scale" ], Es = [ "translateX(var(--un-translate-x))", "translateY(var(--un-translate-y))", "rotate(var(--un-rotate))", "rotateZ(var(--un-rotate-z))", "skewX(var(--un-skew-x))", "skewY(var(--un-skew-y))", "scaleX(var(--un-scale-x))", "scaleY(var(--un-scale-y))" ].join(" "), W = [ "translateX(var(--un-translate-x))", "translateY(var(--un-translate-y))", "translateZ(var(--un-translate-z))", "rotate(var(--un-rotate))", "rotateX(var(--un-rotate-x))", "rotateY(var(--un-rotate-y))", "rotateZ(var(--un-rotate-z))", "skewX(var(--un-skew-x))", "skewY(var(--un-skew-y))", "scaleX(var(--un-scale-x))", "scaleY(var(--un-scale-y))", "scaleZ(var(--un-scale-z))" ].join(" "), js = [ "translate3d(var(--un-translate-x), var(--un-translate-y), var(--un-translate-z))", "rotate(var(--un-rotate))", "rotateX(var(--un-rotate-x))", "rotateY(var(--un-rotate-y))", "rotateZ(var(--un-rotate-z))", "skewX(var(--un-skew-x))", "skewY(var(--un-skew-y))", "scaleX(var(--un-scale-x))", "scaleY(var(--un-scale-y))", "scaleZ(var(--un-scale-z))" ].join(" "), Ss = {
        "--un-rotate": 0,
        "--un-rotate-x": 0,
        "--un-rotate-y": 0,
        "--un-rotate-z": 0,
        "--un-scale-x": 1,
        "--un-scale-y": 1,
        "--un-scale-z": 1,
        "--un-skew-x": 0,
        "--un-skew-y": 0,
        "--un-translate-x": 0,
        "--un-translate-y": 0,
        "--un-translate-z": 0
    }, Cs = [ [ /^(?:transform-)?origin-(.+)$/, ([ , e ]) => ({
        "transform-origin": Sa[e] ?? _.bracket.cssvar(e)
    }), {
        autocomplete: [ `transform-origin-(${Object.keys(Sa).join("|")})`, `origin-(${Object.keys(Sa).join("|")})` ]
    } ], [ /^(?:transform-)?perspect(?:ive)?-(.+)$/, ([ , e ]) => {
        let t = _.bracket.cssvar.px.numberWithUnit(e);
        if (t != null) return {
            "-webkit-perspective": t,
            perspective: t
        };
    } ], [ /^(?:transform-)?perspect(?:ive)?-origin-(.+)$/, ([ , e ]) => {
        let t = _.bracket.cssvar(e) ?? (e.length >= 3 ? Sa[e] : void 0);
        if (t != null) return {
            "-webkit-perspective-origin": t,
            "perspective-origin": t
        };
    } ], [ /^(?:transform-)?translate-()(.+)$/, Fs ], [ /^(?:transform-)?translate-([xyz])-(.+)$/, Fs ], [ /^(?:transform-)?rotate-()(.+)$/, Ls ], [ /^(?:transform-)?rotate-([xyz])-(.+)$/, Ls ], [ /^(?:transform-)?skew-()(.+)$/, _s ], [ /^(?:transform-)?skew-([xy])-(.+)$/, _s, {
        autocomplete: [ "transform-skew-(x|y)-<percent>", "skew-(x|y)-<percent>" ]
    } ], [ /^(?:transform-)?scale-()(.+)$/, As ], [ /^(?:transform-)?scale-([xyz])-(.+)$/, As, {
        autocomplete: [ `transform-(${zs.join("|")})-<percent>`, `transform-(${zs.join("|")})-(x|y|z)-<percent>`, `(${zs.join("|")})-<percent>`, `(${zs.join("|")})-(x|y|z)-<percent>` ]
    } ], [ /^(?:transform-)?preserve-3d$/, () => ({
        "transform-style": "preserve-3d"
    }) ], [ /^(?:transform-)?preserve-flat$/, () => ({
        "transform-style": "flat"
    }) ], [ "transform", {
        transform: W
    } ], [ "transform-cpu", {
        transform: Es
    } ], [ "transform-gpu", {
        transform: js
    } ], [ "transform-none", {
        transform: "none"
    } ], ...N("transform") ];
    function Fs([ , e, t ], {
        theme: r
    }) {
        let n = r.spacing?.[t] ?? _.bracket.cssvar.fraction.rem(t);
        if (n != null) return [ ...cs(e, n, "translate"), [ "transform", W ] ];
    }
    function As([ , e, t ]) {
        let r = _.bracket.cssvar.fraction.percent(t);
        if (r != null) return [ ...cs(e, r, "scale"), [ "transform", W ] ];
    }
    function Ls([ , e = "", t ]) {
        let r = _.bracket.cssvar.degree(t);
        if (r != null) return e ? {
            "--un-rotate": 0,
            [`--un-rotate-${e}`]: r,
            transform: W
        } : {
            "--un-rotate-x": 0,
            "--un-rotate-y": 0,
            "--un-rotate-z": 0,
            "--un-rotate": r,
            transform: W
        };
    }
    function _s([ , e, t ]) {
        let r = _.bracket.cssvar.degree(t);
        if (r != null) return [ ...cs(e, r, "skew"), [ "transform", W ] ];
    }
    var Ns = {
        DEFAULT: "8px",
        0: "0",
        sm: "4px",
        md: "12px",
        lg: "16px",
        xl: "24px",
        "2xl": "40px",
        "3xl": "64px"
    }, Ws = {
        DEFAULT: [ "0 1px 2px rgb(0 0 0 / 0.1)", "0 1px 1px rgb(0 0 0 / 0.06)" ],
        sm: "0 1px 1px rgb(0 0 0 / 0.05)",
        md: [ "0 4px 3px rgb(0 0 0 / 0.07)", "0 2px 2px rgb(0 0 0 / 0.06)" ],
        lg: [ "0 10px 8px rgb(0 0 0 / 0.04)", "0 4px 3px rgb(0 0 0 / 0.1)" ],
        xl: [ "0 20px 13px rgb(0 0 0 / 0.03)", "0 8px 5px rgb(0 0 0 / 0.08)" ],
        "2xl": "0 25px 25px rgb(0 0 0 / 0.15)",
        none: "0 0 rgb(0 0 0 / 0)"
    }, Us = {
        sans: [ "ui-sans-serif", "system-ui", "-apple-system", "BlinkMacSystemFont", '"Segoe UI"', "Roboto", '"Helvetica Neue"', "Arial", '"Noto Sans"', "sans-serif", '"Apple Color Emoji"', '"Segoe UI Emoji"', '"Segoe UI Symbol"', '"Noto Color Emoji"' ].join(","),
        serif: [ "ui-serif", "Georgia", "Cambria", '"Times New Roman"', "Times", "serif" ].join(","),
        mono: [ "ui-monospace", "SFMono-Regular", "Menlo", "Monaco", "Consolas", '"Liberation Mono"', '"Courier New"', "monospace" ].join(",")
    }, Os = {
        xs: [ "0.75rem", "1rem" ],
        sm: [ "0.875rem", "1.25rem" ],
        base: [ "1rem", "1.5rem" ],
        lg: [ "1.125rem", "1.75rem" ],
        xl: [ "1.25rem", "1.75rem" ],
        "2xl": [ "1.5rem", "2rem" ],
        "3xl": [ "1.875rem", "2.25rem" ],
        "4xl": [ "2.25rem", "2.5rem" ],
        "5xl": [ "3rem", "1" ],
        "6xl": [ "3.75rem", "1" ],
        "7xl": [ "4.5rem", "1" ],
        "8xl": [ "6rem", "1" ],
        "9xl": [ "8rem", "1" ]
    }, Ts = {
        DEFAULT: "1.5rem",
        xs: "0.5rem",
        sm: "1rem",
        md: "1.5rem",
        lg: "2rem",
        xl: "2.5rem",
        "2xl": "3rem",
        "3xl": "4rem"
    }, Rs = {
        DEFAULT: "1.5rem",
        none: "0",
        sm: "thin",
        md: "medium",
        lg: "thick"
    }, Ds = {
        DEFAULT: [ "0 0 1px rgb(0 0 0 / 0.2)", "0 0 1px rgb(1 0 5 / 0.1)" ],
        none: "0 0 rgb(0 0 0 / 0)",
        sm: "1px 1px 3px rgb(36 37 47 / 0.25)",
        md: [ "0 1px 2px rgb(30 29 39 / 0.19)", "1px 2px 4px rgb(54 64 147 / 0.18)" ],
        lg: [ "3px 3px 6px rgb(0 0 0 / 0.26)", "0 0 5px rgb(15 3 86 / 0.22)" ],
        xl: [ "1px 1px 3px rgb(0 0 0 / 0.29)", "2px 4px 7px rgb(73 64 125 / 0.35)" ]
    }, Bs = {
        none: "1",
        tight: "1.25",
        snug: "1.375",
        normal: "1.5",
        relaxed: "1.625",
        loose: "2"
    }, Xs = {
        tighter: "-0.05em",
        tight: "-0.025em",
        normal: "0em",
        wide: "0.025em",
        wider: "0.05em",
        widest: "0.1em"
    }, Ys = {
        thin: "100",
        extralight: "200",
        light: "300",
        normal: "400",
        medium: "500",
        semibold: "600",
        bold: "700",
        extrabold: "800",
        black: "900"
    }, Ps = Xs, Is = {
        sm: "640px",
        md: "768px",
        lg: "1024px",
        xl: "1280px",
        "2xl": "1536px"
    }, qs = {
        ...Is
    }, Ms = {
        DEFAULT: "1px",
        none: "0"
    }, Zs = {
        DEFAULT: "1rem",
        none: "0",
        xs: "0.75rem",
        sm: "0.875rem",
        lg: "1.125rem",
        xl: "1.25rem",
        "2xl": "1.5rem",
        "3xl": "1.875rem",
        "4xl": "2.25rem",
        "5xl": "3rem",
        "6xl": "3.75rem",
        "7xl": "4.5rem",
        "8xl": "6rem",
        "9xl": "8rem"
    }, Hs = {
        DEFAULT: "150ms",
        none: "0s",
        75: "75ms",
        100: "100ms",
        150: "150ms",
        200: "200ms",
        300: "300ms",
        500: "500ms",
        700: "700ms",
        1e3: "1000ms"
    }, Gs = {
        DEFAULT: "0.25rem",
        none: "0",
        sm: "0.125rem",
        md: "0.375rem",
        lg: "0.5rem",
        xl: "0.75rem",
        "2xl": "1rem",
        "3xl": "1.5rem",
        full: "9999px"
    }, Js = {
        DEFAULT: [ "var(--un-shadow-inset) 0 1px 3px 0 rgb(0 0 0 / 0.1)", "var(--un-shadow-inset) 0 1px 2px -1px rgb(0 0 0 / 0.1)" ],
        none: "0 0 rgb(0 0 0 / 0)",
        sm: "var(--un-shadow-inset) 0 1px 2px 0 rgb(0 0 0 / 0.05)",
        md: [ "var(--un-shadow-inset) 0 4px 6px -1px rgb(0 0 0 / 0.1)", "var(--un-shadow-inset) 0 2px 4px -2px rgb(0 0 0 / 0.1)" ],
        lg: [ "var(--un-shadow-inset) 0 10px 15px -3px rgb(0 0 0 / 0.1)", "var(--un-shadow-inset) 0 4px 6px -4px rgb(0 0 0 / 0.1)" ],
        xl: [ "var(--un-shadow-inset) 0 20px 25px -5px rgb(0 0 0 / 0.1)", "var(--un-shadow-inset) 0 8px 10px -6px rgb(0 0 0 / 0.1)" ],
        "2xl": "var(--un-shadow-inset) 0 25px 50px -12px rgb(0 0 0 / 0.25)",
        inner: "inset 0 2px 4px 0 rgb(0 0 0 / 0.05)"
    }, Ks = {
        DEFAULT: "3px",
        none: "0"
    }, Vs = {
        auto: "auto"
    }, Qs = {
        mouse: "(hover) and (pointer: fine)"
    }, el = {
        ...Ss,
        ...ws,
        ...vs
    }, tl = {
        xs: "20rem",
        sm: "24rem",
        md: "28rem",
        lg: "32rem",
        xl: "36rem",
        "2xl": "42rem",
        "3xl": "48rem",
        "4xl": "56rem",
        "5xl": "64rem",
        "6xl": "72rem",
        "7xl": "80rem",
        prose: "65ch"
    }, rl = {
        auto: "auto",
        ...tl,
        screen: "100vw"
    }, nl = {
        none: "none",
        ...tl,
        screen: "100vw"
    }, il = {
        auto: "auto",
        ...tl,
        screen: "100vh"
    }, ol = {
        none: "none",
        ...tl,
        screen: "100vh"
    }, al = {
        ...tl
    }, sl = {
        DEFAULT: "cubic-bezier(0.4, 0, 0.2, 1)",
        linear: "linear",
        "in": "cubic-bezier(0.4, 0, 1, 1)",
        out: "cubic-bezier(0, 0, 0.2, 1)",
        "in-out": "cubic-bezier(0.4, 0, 0.2, 1)"
    }, ll = {
        none: "none",
        all: "all",
        colors: [ "color", "background-color", "border-color", "text-decoration-color", "fill", "stroke" ].join(","),
        opacity: "opacity",
        shadow: "box-shadow",
        transform: "transform",
        get DEFAULT() {
            return [ this.colors, "opacity", "box-shadow", "transform", "filter", "backdrop-filter" ].join(",");
        }
    }, cl = {
        width: rl,
        height: il,
        maxWidth: nl,
        maxHeight: ol,
        minWidth: nl,
        minHeight: ol,
        inlineSize: rl,
        blockSize: il,
        maxInlineSize: nl,
        maxBlockSize: ol,
        minInlineSize: nl,
        minBlockSize: ol,
        colors: $a,
        fontFamily: Us,
        fontSize: Os,
        fontWeight: Ys,
        breakpoints: Is,
        verticalBreakpoints: qs,
        borderRadius: Gs,
        lineHeight: Bs,
        letterSpacing: Xs,
        wordSpacing: Ps,
        boxShadow: Js,
        textIndent: Ts,
        textShadow: Ds,
        textStrokeWidth: Rs,
        blur: Ns,
        dropShadow: Ws,
        easing: sl,
        transitionProperty: ll,
        lineWidth: Ms,
        spacing: Zs,
        duration: Hs,
        ringWidth: Ks,
        preflightBase: el,
        containers: al,
        zIndex: Vs,
        media: Qs
    };
    var fl = {
        ...cl,
        aria: {
            busy: 'busy="true"',
            checked: 'checked="true"',
            disabled: 'disabled="true"',
            expanded: 'expanded="true"',
            hidden: 'hidden="true"',
            pressed: 'pressed="true"',
            readonly: 'readonly="true"',
            required: 'required="true"',
            selected: 'selected="true"'
        },
        animation: {
            keyframes: {
                pulse: "{0%, 100% {opacity:1} 50% {opacity:.5}}",
                bounce: "{0%, 100% {transform:translateY(-25%);animation-timing-function:cubic-bezier(0.8,0,1,1)} 50% {transform:translateY(0);animation-timing-function:cubic-bezier(0,0,0.2,1)}}",
                spin: "{from{transform:rotate(0deg)}to{transform:rotate(360deg)}}",
                ping: "{0%{transform:scale(1);opacity:1}75%,100%{transform:scale(2);opacity:0}}",
                "bounce-alt": "{from,20%,53%,80%,to{animation-timing-function:cubic-bezier(0.215,0.61,0.355,1);transform:translate3d(0,0,0)}40%,43%{animation-timing-function:cubic-bezier(0.755,0.05,0.855,0.06);transform:translate3d(0,-30px,0)}70%{animation-timing-function:cubic-bezier(0.755,0.05,0.855,0.06);transform:translate3d(0,-15px,0)}90%{transform:translate3d(0,-4px,0)}}",
                flash: "{from,50%,to{opacity:1}25%,75%{opacity:0}}",
                "pulse-alt": "{from{transform:scale3d(1,1,1)}50%{transform:scale3d(1.05,1.05,1.05)}to{transform:scale3d(1,1,1)}}",
                "rubber-band": "{from{transform:scale3d(1,1,1)}30%{transform:scale3d(1.25,0.75,1)}40%{transform:scale3d(0.75,1.25,1)}50%{transform:scale3d(1.15,0.85,1)}65%{transform:scale3d(0.95,1.05,1)}75%{transform:scale3d(1.05,0.95,1)}to{transform:scale3d(1,1,1)}}",
                "shake-x": "{from,to{transform:translate3d(0,0,0)}10%,30%,50%,70%,90%{transform:translate3d(-10px,0,0)}20%,40%,60%,80%{transform:translate3d(10px,0,0)}}",
                "shake-y": "{from,to{transform:translate3d(0,0,0)}10%,30%,50%,70%,90%{transform:translate3d(0,-10px,0)}20%,40%,60%,80%{transform:translate3d(0,10px,0)}}",
                "head-shake": "{0%{transform:translateX(0)}6.5%{transform:translateX(-6px) rotateY(-9deg)}18.5%{transform:translateX(5px) rotateY(7deg)}31.5%{transform:translateX(-3px) rotateY(-5deg)}43.5%{transform:translateX(2px) rotateY(3deg)}50%{transform:translateX(0)}}",
                swing: "{20%{transform:rotate3d(0,0,1,15deg)}40%{transform:rotate3d(0,0,1,-10deg)}60%{transform:rotate3d(0,0,1,5deg)}80%{transform:rotate3d(0,0,1,-5deg)}to{transform:rotate3d(0,0,1,0deg)}}",
                tada: "{from{transform:scale3d(1,1,1)}10%,20%{transform:scale3d(0.9,0.9,0.9) rotate3d(0,0,1,-3deg)}30%,50%,70%,90%{transform:scale3d(1.1,1.1,1.1) rotate3d(0,0,1,3deg)}40%,60%,80%{transform:scale3d(1.1,1.1,1.1) rotate3d(0,0,1,-3deg)}to{transform:scale3d(1,1,1)}}",
                wobble: "{from{transform:translate3d(0,0,0)}15%{transform:translate3d(-25%,0,0) rotate3d(0,0,1,-5deg)}30%{transform:translate3d(20%,0,0) rotate3d(0,0,1,3deg)}45%{transform:translate3d(-15%,0,0) rotate3d(0,0,1,-3deg)}60%{transform:translate3d(10%,0,0) rotate3d(0,0,1,2deg)}75%{transform:translate3d(-5%,0,0) rotate3d(0,0,1,-1deg)}to{transform:translate3d(0,0,0)}}",
                jello: "{from,11.1%,to{transform:translate3d(0,0,0)}22.2%{transform:skewX(-12.5deg) skewY(-12.5deg)}33.3%{transform:skewX(6.25deg) skewY(6.25deg)}44.4%{transform:skewX(-3.125deg)skewY(-3.125deg)}55.5%{transform:skewX(1.5625deg) skewY(1.5625deg)}66.6%{transform:skewX(-0.78125deg) skewY(-0.78125deg)}77.7%{transform:skewX(0.390625deg) skewY(0.390625deg)}88.8%{transform:skewX(-0.1953125deg) skewY(-0.1953125deg)}}",
                "heart-beat": "{0%{transform:scale(1)}14%{transform:scale(1.3)}28%{transform:scale(1)}42%{transform:scale(1.3)}70%{transform:scale(1)}}",
                hinge: "{0%{transform-origin:top left;animation-timing-function:ease-in-out}20%,60%{transform:rotate3d(0,0,1,80deg);transform-origin:top left;animation-timing-function:ease-in-out}40%,80%{transform:rotate3d(0,0,1,60deg);transform-origin:top left;animation-timing-function:ease-in-out}to{transform:translate3d(0,700px,0);opacity:0}}",
                "jack-in-the-box": "{from{opacity:0;transform-origin:center bottom;transform:scale(0.1) rotate(30deg)}50%{transform:rotate(-10deg)}70%{transform:rotate(3deg)}to{transform:scale(1)}}",
                "light-speed-in-left": "{from{opacity:0;transform:translate3d(-100%,0,0) skewX(-30deg)}60%{opacity:1;transform:skewX(20deg)}80%{transform:skewX(-5deg)}to{transform:translate3d(0,0,0)}}",
                "light-speed-in-right": "{from{opacity:0;transform:translate3d(100%,0,0) skewX(-30deg)}60%{opacity:1;transform:skewX(20deg)}80%{transform:skewX(-5deg)}to{transform:translate3d(0,0,0)}}",
                "light-speed-out-left": "{from{opacity:1}to{opacity:0;transform:translate3d(-100%,0,0) skewX(30deg)}}",
                "light-speed-out-right": "{from{opacity:1}to{opacity:0;transform:translate3d(100%,0,0) skewX(30deg)}}",
                flip: "{from{transform:perspective(400px) scale3d(1,1,1) translate3d(0,0,0) rotate3d(0,1,0,-360deg);animation-timing-function:ease-out}40%{transform:perspective(400px) scale3d(1,1,1) translate3d(0,0,150px) rotate3d(0,1,0,-190deg);animation-timing-function:ease-out}50%{transform:perspective(400px) scale3d(1,1,1) translate3d(0,0,150px) rotate3d(0,1,0,-170deg);animation-timing-function:ease-in}80%{transform:perspective(400px) scale3d(0.95,0.95,0.95) translate3d(0,0,0) rotate3d(0,1,0,0deg);animation-timing-function:ease-in}to{transform:perspective(400px) scale3d(1,1,1) translate3d(0,0,0) rotate3d(0,1,0,0deg);animation-timing-function:ease-in}}",
                "flip-in-x": "{from{transform:perspective(400px) rotate3d(1,0,0,90deg);animation-timing-function:ease-in;opacity:0}40%{transform:perspective(400px) rotate3d(1,0,0,-20deg);animation-timing-function:ease-in}60%{transform:perspective(400px) rotate3d(1,0,0,10deg);opacity:1}80%{transform:perspective(400px) rotate3d(1,0,0,-5deg)}to{transform:perspective(400px)}}",
                "flip-in-y": "{from{transform:perspective(400px) rotate3d(0,1,0,90deg);animation-timing-function:ease-in;opacity:0}40%{transform:perspective(400px) rotate3d(0,1,0,-20deg);animation-timing-function:ease-in}60%{transform:perspective(400px) rotate3d(0,1,0,10deg);opacity:1}80%{transform:perspective(400px) rotate3d(0,1,0,-5deg)}to{transform:perspective(400px)}}",
                "flip-out-x": "{from{transform:perspective(400px)}30%{transform:perspective(400px) rotate3d(1,0,0,-20deg);opacity:1}to{transform:perspective(400px) rotate3d(1,0,0,90deg);opacity:0}}",
                "flip-out-y": "{from{transform:perspective(400px)}30%{transform:perspective(400px) rotate3d(0,1,0,-15deg);opacity:1}to{transform:perspective(400px) rotate3d(0,1,0,90deg);opacity:0}}",
                "rotate-in": "{from{transform-origin:center;transform:rotate3d(0,0,1,-200deg);opacity:0}to{transform-origin:center;transform:translate3d(0,0,0);opacity:1}}",
                "rotate-in-down-left": "{from{transform-origin:left bottom;transform:rotate3d(0,0,1,-45deg);opacity:0}to{transform-origin:left bottom;transform:translate3d(0,0,0);opacity:1}}",
                "rotate-in-down-right": "{from{transform-origin:right bottom;transform:rotate3d(0,0,1,45deg);opacity:0}to{transform-origin:right bottom;transform:translate3d(0,0,0);opacity:1}}",
                "rotate-in-up-left": "{from{transform-origin:left top;transform:rotate3d(0,0,1,45deg);opacity:0}to{transform-origin:left top;transform:translate3d(0,0,0);opacity:1}}",
                "rotate-in-up-right": "{from{transform-origin:right bottom;transform:rotate3d(0,0,1,-90deg);opacity:0}to{transform-origin:right bottom;transform:translate3d(0,0,0);opacity:1}}",
                "rotate-out": "{from{transform-origin:center;opacity:1}to{transform-origin:center;transform:rotate3d(0,0,1,200deg);opacity:0}}",
                "rotate-out-down-left": "{from{transform-origin:left bottom;opacity:1}to{transform-origin:left bottom;transform:rotate3d(0,0,1,45deg);opacity:0}}",
                "rotate-out-down-right": "{from{transform-origin:right bottom;opacity:1}to{transform-origin:right bottom;transform:rotate3d(0,0,1,-45deg);opacity:0}}",
                "rotate-out-up-left": "{from{transform-origin:left bottom;opacity:1}to{transform-origin:left bottom;transform:rotate3d(0,0,1,-45deg);opacity:0}}",
                "rotate-out-up-right": "{from{transform-origin:right bottom;opacity:1}to{transform-origin:left bottom;transform:rotate3d(0,0,1,90deg);opacity:0}}",
                "roll-in": "{from{opacity:0;transform:translate3d(-100%,0,0) rotate3d(0,0,1,-120deg)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "roll-out": "{from{opacity:1}to{opacity:0;transform:translate3d(100%,0,0) rotate3d(0,0,1,120deg)}}",
                "zoom-in": "{from{opacity:0;transform:scale3d(0.3,0.3,0.3)}50%{opacity:1}}",
                "zoom-in-down": "{from{opacity:0;transform:scale3d(0.1,0.1,0.1) translate3d(0,-1000px,0);animation-timing-function:cubic-bezier(0.55,0.055,0.675,0.19)}60%{opacity:1;transform:scale3d(0.475,0.475,0.475) translate3d(0,60px,0);animation-timing-function:cubic-bezier(0.175,0.885,0.32,1)}}",
                "zoom-in-left": "{from{opacity:0;transform:scale3d(0.1,0.1,0.1) translate3d(-1000px,0,0);animation-timing-function:cubic-bezier(0.55,0.055,0.675,0.19)}60%{opacity:1;transform:scale3d(0.475,0.475,0.475) translate3d(10px,0,0);animation-timing-function:cubic-bezier(0.175,0.885,0.32,1)}}",
                "zoom-in-right": "{from{opacity:0;transform:scale3d(0.1,0.1,0.1) translate3d(1000px,0,0);animation-timing-function:cubic-bezier(0.55,0.055,0.675,0.19)}60%{opacity:1;transform:scale3d(0.475,0.475,0.475) translate3d(-10px,0,0);animation-timing-function:cubic-bezier(0.175,0.885,0.32,1)}}",
                "zoom-in-up": "{from{opacity:0;transform:scale3d(0.1,0.1,0.1) translate3d(0,1000px,0);animation-timing-function:cubic-bezier(0.55,0.055,0.675,0.19)}60%{opacity:1;transform:scale3d(0.475,0.475,0.475) translate3d(0,-60px,0);animation-timing-function:cubic-bezier(0.175,0.885,0.32,1)}}",
                "zoom-out": "{from{opacity:1}50%{opacity:0;transform:scale3d(0.3,0.3,0.3)}to{opacity:0}}",
                "zoom-out-down": "{40%{opacity:1;transform:scale3d(0.475,0.475,0.475) translate3d(0,-60px,0);animation-timing-function:cubic-bezier(0.55,0.055,0.675,0.19)}to{opacity:0;transform:scale3d(0.1,0.1,0.1) translate3d(0,2000px,0);transform-origin:center bottom;animation-timing-function:cubic-bezier(0.175,0.885,0.32,1)}}",
                "zoom-out-left": "{40%{opacity:1;transform:scale3d(0.475,0.475,0.475) translate3d(42px,0,0)}to{opacity:0;transform:scale(0.1) translate3d(-2000px,0,0);transform-origin:left center}}",
                "zoom-out-right": "{40%{opacity:1;transform:scale3d(0.475,0.475,0.475) translate3d(-42px,0,0)}to{opacity:0;transform:scale(0.1) translate3d(2000px,0,0);transform-origin:right center}}",
                "zoom-out-up": "{40%{opacity:1;transform:scale3d(0.475,0.475,0.475) translate3d(0,60px,0);animation-timing-function:cubic-bezier(0.55,0.055,0.675,0.19)}to{opacity:0;transform:scale3d(0.1,0.1,0.1) translate3d(0,-2000px,0);transform-origin:center bottom;animation-timing-function:cubic-bezier(0.175,0.885,0.32,1)}}",
                "bounce-in": "{from,20%,40%,60%,80%,to{animation-timing-function:ease-in-out}0%{opacity:0;transform:scale3d(0.3,0.3,0.3)}20%{transform:scale3d(1.1,1.1,1.1)}40%{transform:scale3d(0.9,0.9,0.9)}60%{transform:scale3d(1.03,1.03,1.03);opacity:1}80%{transform:scale3d(0.97,0.97,0.97)}to{opacity:1;transform:scale3d(1,1,1)}}",
                "bounce-in-down": "{from,60%,75%,90%,to{animation-timing-function:cubic-bezier(0.215,0.61,0.355,1)}0%{opacity:0;transform:translate3d(0,-3000px,0)}60%{opacity:1;transform:translate3d(0,25px,0)}75%{transform:translate3d(0,-10px,0)}90%{transform:translate3d(0,5px,0)}to{transform:translate3d(0,0,0)}}",
                "bounce-in-left": "{from,60%,75%,90%,to{animation-timing-function:cubic-bezier(0.215,0.61,0.355,1)}0%{opacity:0;transform:translate3d(-3000px,0,0)}60%{opacity:1;transform:translate3d(25px,0,0)}75%{transform:translate3d(-10px,0,0)}90%{transform:translate3d(5px,0,0)}to{transform:translate3d(0,0,0)}}",
                "bounce-in-right": "{from,60%,75%,90%,to{animation-timing-function:cubic-bezier(0.215,0.61,0.355,1)}0%{opacity:0;transform:translate3d(3000px,0,0)}60%{opacity:1;transform:translate3d(-25px,0,0)}75%{transform:translate3d(10px,0,0)}90%{transform:translate3d(-5px,0,0)}to{transform:translate3d(0,0,0)}}",
                "bounce-in-up": "{from,60%,75%,90%,to{animation-timing-function:cubic-bezier(0.215,0.61,0.355,1)}0%{opacity:0;transform:translate3d(0,3000px,0)}60%{opacity:1;transform:translate3d(0,-20px,0)}75%{transform:translate3d(0,10px,0)}90%{transform:translate3d(0,-5px,0)}to{transform:translate3d(0,0,0)}}",
                "bounce-out": "{20%{transform:scale3d(0.9,0.9,0.9)}50%,55%{opacity:1;transform:scale3d(1.1,1.1,1.1)}to{opacity:0;transform:scale3d(0.3,0.3,0.3)}}",
                "bounce-out-down": "{20%{transform:translate3d(0,10px,0)}40%,45%{opacity:1;transform:translate3d(0,-20px,0)}to{opacity:0;transform:translate3d(0,2000px,0)}}",
                "bounce-out-left": "{20%{opacity:1;transform:translate3d(20px,0,0)}to{opacity:0;transform:translate3d(-2000px,0,0)}}",
                "bounce-out-right": "{20%{opacity:1;transform:translate3d(-20px,0,0)}to{opacity:0;transform:translate3d(2000px,0,0)}}",
                "bounce-out-up": "{20%{transform:translate3d(0,-10px,0)}40%,45%{opacity:1;transform:translate3d(0,20px,0)}to{opacity:0;transform:translate3d(0,-2000px,0)}}",
                "slide-in-down": "{from{transform:translate3d(0,-100%,0);visibility:visible}to{transform:translate3d(0,0,0)}}",
                "slide-in-left": "{from{transform:translate3d(-100%,0,0);visibility:visible}to{transform:translate3d(0,0,0)}}",
                "slide-in-right": "{from{transform:translate3d(100%,0,0);visibility:visible}to{transform:translate3d(0,0,0)}}",
                "slide-in-up": "{from{transform:translate3d(0,100%,0);visibility:visible}to{transform:translate3d(0,0,0)}}",
                "slide-out-down": "{from{transform:translate3d(0,0,0)}to{visibility:hidden;transform:translate3d(0,100%,0)}}",
                "slide-out-left": "{from{transform:translate3d(0,0,0)}to{visibility:hidden;transform:translate3d(-100%,0,0)}}",
                "slide-out-right": "{from{transform:translate3d(0,0,0)}to{visibility:hidden;transform:translate3d(100%,0,0)}}",
                "slide-out-up": "{from{transform:translate3d(0,0,0)}to{visibility:hidden;transform:translate3d(0,-100%,0)}}",
                "fade-in": "{from{opacity:0}to{opacity:1}}",
                "fade-in-down": "{from{opacity:0;transform:translate3d(0,-100%,0)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "fade-in-down-big": "{from{opacity:0;transform:translate3d(0,-2000px,0)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "fade-in-left": "{from{opacity:0;transform:translate3d(-100%,0,0)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "fade-in-left-big": "{from{opacity:0;transform:translate3d(-2000px,0,0)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "fade-in-right": "{from{opacity:0;transform:translate3d(100%,0,0)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "fade-in-right-big": "{from{opacity:0;transform:translate3d(2000px,0,0)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "fade-in-up": "{from{opacity:0;transform:translate3d(0,100%,0)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "fade-in-up-big": "{from{opacity:0;transform:translate3d(0,2000px,0)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "fade-in-top-left": "{from{opacity:0;transform:translate3d(-100%,-100%,0)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "fade-in-top-right": "{from{opacity:0;transform:translate3d(100%,-100%,0)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "fade-in-bottom-left": "{from{opacity:0;transform:translate3d(-100%,100%,0)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "fade-in-bottom-right": "{from{opacity:0;transform:translate3d(100%,100%,0)}to{opacity:1;transform:translate3d(0,0,0)}}",
                "fade-out": "{from{opacity:1}to{opacity:0}}",
                "fade-out-down": "{from{opacity:1}to{opacity:0;transform:translate3d(0,100%,0)}}",
                "fade-out-down-big": "{from{opacity:1}to{opacity:0;transform:translate3d(0,2000px,0)}}",
                "fade-out-left": "{from{opacity:1}to{opacity:0;transform:translate3d(-100%,0,0)}}",
                "fade-out-left-big": "{from{opacity:1}to{opacity:0;transform:translate3d(-2000px,0,0)}}",
                "fade-out-right": "{from{opacity:1}to{opacity:0;transform:translate3d(100%,0,0)}}",
                "fade-out-right-big": "{from{opacity:1}to{opacity:0;transform:translate3d(2000px,0,0)}}",
                "fade-out-up": "{from{opacity:1}to{opacity:0;transform:translate3d(0,-100%,0)}}",
                "fade-out-up-big": "{from{opacity:1}to{opacity:0;transform:translate3d(0,-2000px,0)}}",
                "fade-out-top-left": "{from{opacity:1;transform:translate3d(0,0,0)}to{opacity:0;transform:translate3d(-100%,-100%,0)}}",
                "fade-out-top-right": "{from{opacity:1;transform:translate3d(0,0,0)}to{opacity:0;transform:translate3d(100%,-100%,0)}}",
                "fade-out-bottom-left": "{from{opacity:1;transform:translate3d(0,0,0)}to{opacity:0;transform:translate3d(-100%,100%,0)}}",
                "fade-out-bottom-right": "{from{opacity:1;transform:translate3d(0,0,0)}to{opacity:0;transform:translate3d(100%,100%,0)}}",
                "back-in-up": "{0%{opacity:0.7;transform:translateY(1200px) scale(0.7)}80%{opacity:0.7;transform:translateY(0px) scale(0.7)}100%{opacity:1;transform:scale(1)}}",
                "back-in-down": "{0%{opacity:0.7;transform:translateY(-1200px) scale(0.7)}80%{opacity:0.7;transform:translateY(0px) scale(0.7)}100%{opacity:1;transform:scale(1)}}",
                "back-in-right": "{0%{opacity:0.7;transform:translateX(2000px) scale(0.7)}80%{opacity:0.7;transform:translateY(0px) scale(0.7)}100%{opacity:1;transform:scale(1)}}",
                "back-in-left": "{0%{opacity:0.7;transform:translateX(-2000px) scale(0.7)}80%{opacity:0.7;transform:translateX(0px) scale(0.7)}100%{opacity:1;transform:scale(1)}}",
                "back-out-up": "{0%{opacity:1;transform:scale(1)}80%{opacity:0.7;transform:translateY(0px) scale(0.7)}100%{opacity:0.7;transform:translateY(-700px) scale(0.7)}}",
                "back-out-down": "{0%{opacity:1;transform:scale(1)}80%{opacity:0.7;transform:translateY(0px) scale(0.7)}100%{opacity:0.7;transform:translateY(700px) scale(0.7)}}",
                "back-out-right": "{0%{opacity:1;transform:scale(1)}80%{opacity:0.7;transform:translateY(0px) scale(0.7)}100%{opacity:0.7;transform:translateX(2000px) scale(0.7)}}",
                "back-out-left": "{0%{opacity:1;transform:scale(1)}80%{opacity:0.7;transform:translateX(-2000px) scale(0.7)}100%{opacity:0.7;transform:translateY(-700px) scale(0.7)}}"
            },
            durations: {
                pulse: "2s",
                "heart-beat": "1.3s",
                "bounce-in": "0.75s",
                "bounce-out": "0.75s",
                "flip-out-x": "0.75s",
                "flip-out-y": "0.75s",
                hinge: "2s"
            },
            timingFns: {
                pulse: "cubic-bezier(0.4,0,.6,1)",
                ping: "cubic-bezier(0,0,.2,1)",
                "head-shake": "ease-in-out",
                "heart-beat": "ease-in-out",
                "pulse-alt": "ease-in-out",
                "light-speed-in-left": "ease-out",
                "light-speed-in-right": "ease-out",
                "light-speed-out-left": "ease-in",
                "light-speed-out-right": "ease-in"
            },
            properties: {
                "bounce-alt": {
                    "transform-origin": "center bottom"
                },
                jello: {
                    "transform-origin": "center"
                },
                swing: {
                    "transform-origin": "top center"
                },
                flip: {
                    "backface-visibility": "visible"
                },
                "flip-in-x": {
                    "backface-visibility": "visible !important"
                },
                "flip-in-y": {
                    "backface-visibility": "visible !important"
                },
                "flip-out-x": {
                    "backface-visibility": "visible !important"
                },
                "flip-out-y": {
                    "backface-visibility": "visible !important"
                },
                "rotate-in": {
                    "transform-origin": "center"
                },
                "rotate-in-down-left": {
                    "transform-origin": "left bottom"
                },
                "rotate-in-down-right": {
                    "transform-origin": "right bottom"
                },
                "rotate-in-up-left": {
                    "transform-origin": "left bottom"
                },
                "rotate-in-up-right": {
                    "transform-origin": "right bottom"
                },
                "rotate-out": {
                    "transform-origin": "center"
                },
                "rotate-out-down-left": {
                    "transform-origin": "left bottom"
                },
                "rotate-out-down-right": {
                    "transform-origin": "right bottom"
                },
                "rotate-out-up-left": {
                    "transform-origin": "left bottom"
                },
                "rotate-out-up-right": {
                    "transform-origin": "right bottom"
                },
                hinge: {
                    "transform-origin": "top left"
                },
                "zoom-out-down": {
                    "transform-origin": "center bottom"
                },
                "zoom-out-left": {
                    "transform-origin": "left center"
                },
                "zoom-out-right": {
                    "transform-origin": "right center"
                },
                "zoom-out-up": {
                    "transform-origin": "center bottom"
                }
            },
            counts: {
                spin: "infinite",
                ping: "infinite",
                pulse: "infinite",
                "pulse-alt": "infinite",
                bounce: "infinite",
                "bounce-alt": "infinite"
            },
            category: {
                pulse: "Attention Seekers",
                bounce: "Attention Seekers",
                spin: "Attention Seekers",
                ping: "Attention Seekers",
                "bounce-alt": "Attention Seekers",
                flash: "Attention Seekers",
                "pulse-alt": "Attention Seekers",
                "rubber-band": "Attention Seekers",
                "shake-x": "Attention Seekers",
                "shake-y": "Attention Seekers",
                "head-shake": "Attention Seekers",
                swing: "Attention Seekers",
                tada: "Attention Seekers",
                wobble: "Attention Seekers",
                jello: "Attention Seekers",
                "heart-beat": "Attention Seekers",
                hinge: "Specials",
                "jack-in-the-box": "Specials",
                "light-speed-in-left": "Lightspeed",
                "light-speed-in-right": "Lightspeed",
                "light-speed-out-left": "Lightspeed",
                "light-speed-out-right": "Lightspeed",
                flip: "Flippers",
                "flip-in-x": "Flippers",
                "flip-in-y": "Flippers",
                "flip-out-x": "Flippers",
                "flip-out-y": "Flippers",
                "rotate-in": "Rotating Entrances",
                "rotate-in-down-left": "Rotating Entrances",
                "rotate-in-down-right": "Rotating Entrances",
                "rotate-in-up-left": "Rotating Entrances",
                "rotate-in-up-right": "Rotating Entrances",
                "rotate-out": "Rotating Exits",
                "rotate-out-down-left": "Rotating Exits",
                "rotate-out-down-right": "Rotating Exits",
                "rotate-out-up-left": "Rotating Exits",
                "rotate-out-up-right": "Rotating Exits",
                "roll-in": "Specials",
                "roll-out": "Specials",
                "zoom-in": "Zooming Entrances",
                "zoom-in-down": "Zooming Entrances",
                "zoom-in-left": "Zooming Entrances",
                "zoom-in-right": "Zooming Entrances",
                "zoom-in-up": "Zooming Entrances",
                "zoom-out": "Zooming Exits",
                "zoom-out-down": "Zooming Exits",
                "zoom-out-left": "Zooming Exits",
                "zoom-out-right": "Zooming Exits",
                "zoom-out-up": "Zooming Exits",
                "bounce-in": "Bouncing Entrances",
                "bounce-in-down": "Bouncing Entrances",
                "bounce-in-left": "Bouncing Entrances",
                "bounce-in-right": "Bouncing Entrances",
                "bounce-in-up": "Bouncing Entrances",
                "bounce-out": "Bouncing Exits",
                "bounce-out-down": "Bouncing Exits",
                "bounce-out-left": "Bouncing Exits",
                "bounce-out-right": "Bouncing Exits",
                "bounce-out-up": "Bouncing Exits",
                "slide-in-down": "Sliding Entrances",
                "slide-in-left": "Sliding Entrances",
                "slide-in-right": "Sliding Entrances",
                "slide-in-up": "Sliding Entrances",
                "slide-out-down": "Sliding Exits",
                "slide-out-left": "Sliding Exits",
                "slide-out-right": "Sliding Exits",
                "slide-out-up": "Sliding Exits",
                "fade-in": "Fading Entrances",
                "fade-in-down": "Fading Entrances",
                "fade-in-down-big": "Fading Entrances",
                "fade-in-left": "Fading Entrances",
                "fade-in-left-big": "Fading Entrances",
                "fade-in-right": "Fading Entrances",
                "fade-in-right-big": "Fading Entrances",
                "fade-in-up": "Fading Entrances",
                "fade-in-up-big": "Fading Entrances",
                "fade-in-top-left": "Fading Entrances",
                "fade-in-top-right": "Fading Entrances",
                "fade-in-bottom-left": "Fading Entrances",
                "fade-in-bottom-right": "Fading Entrances",
                "fade-out": "Fading Exits",
                "fade-out-down": "Fading Exits",
                "fade-out-down-big": "Fading Exits",
                "fade-out-left": "Fading Exits",
                "fade-out-left-big": "Fading Exits",
                "fade-out-right": "Fading Exits",
                "fade-out-right-big": "Fading Exits",
                "fade-out-up": "Fading Exits",
                "fade-out-up-big": "Fading Exits",
                "fade-out-top-left": "Fading Exits",
                "fade-out-top-right": "Fading Exits",
                "fade-out-bottom-left": "Fading Exits",
                "fade-out-bottom-right": "Fading Exits",
                "back-in-up": "Back Entrances",
                "back-in-down": "Back Entrances",
                "back-in-right": "Back Entrances",
                "back-in-left": "Back Entrances",
                "back-out-up": "Back Exits",
                "back-out-down": "Back Exits",
                "back-out-right": "Back Exits",
                "back-out-left": "Back Exits"
            }
        },
        media: {
            portrait: "(orientation: portrait)",
            landscape: "(orientation: landscape)",
            os_dark: "(prefers-color-scheme: dark)",
            os_light: "(prefers-color-scheme: light)",
            motion_ok: "(prefers-reduced-motion: no-preference)",
            motion_not_ok: "(prefers-reduced-motion: reduce)",
            high_contrast: "(prefers-contrast: high)",
            low_contrast: "(prefers-contrast: low)",
            opacity_ok: "(prefers-reduced-transparency: no-preference)",
            opacity_not_ok: "(prefers-reduced-transparency: reduce)",
            use_data_ok: "(prefers-reduced-data: no-preference)",
            use_data_not_ok: "(prefers-reduced-data: reduce)",
            touch: "(hover: none) and (pointer: coarse)",
            stylus: "(hover: none) and (pointer: fine)",
            pointer: "(hover) and (pointer: coarse)",
            mouse: "(hover) and (pointer: fine)",
            hd_color: "(dynamic-range: high)"
        },
        supports: {
            grid: "(display: grid)"
        },
        preflightBase: {
            ...bn,
            ...da,
            ...Ho,
            ...ma,
            ...la,
            ...en,
            ...Jr,
            ...Do,
            ...Xo
        }
    };
    var dl = [ n("svg", e => ({
        selector: `${e.selector} svg`
    })) ];
    var ul = [ n(".dark", e => ({
        prefix: `.dark $$ ${e.prefix}`
    })), n(".light", e => ({
        prefix: `.light $$ ${e.prefix}`
    })), t("@dark", "@media (prefers-color-scheme: dark)"), t("@light", "@media (prefers-color-scheme: light)") ];
    var pl = [ t("contrast-more", "@media (prefers-contrast: more)"), t("contrast-less", "@media (prefers-contrast: less)") ], ml = [ t("motion-reduce", "@media (prefers-reduced-motion: reduce)"), t("motion-safe", "@media (prefers-reduced-motion: no-preference)") ], gl = [ t("landscape", "@media (orientation: landscape)"), t("portrait", "@media (orientation: portrait)") ];
    var hl = e => {
        if (!e.startsWith("_") && (/space-[xy]-.+$/.test(e) || /divide-/.test(e))) return {
            matcher: e,
            selector: e => {
                let t = ">:not([hidden])~:not([hidden])";
                return e.includes(t) ? e : `${e}${t}`;
            }
        };
    }, bl = [ n("@hover", e => ({
        parent: `${e.parent ? `${e.parent} $$ ` : ""}@media (hover: hover) and (pointer: fine)`,
        selector: `${e.selector || ""}:hover`
    })) ];
    var xl = (e, {
        theme: n
    }) => {
        let i = e.match(/^(.*)\b(placeholder-)(.+)$/);
        if (i) {
            let [ , e = "", t, r ] = i;
            if (_t(r, n, "accentColor") || vl(r)) return {
                matcher: `${e}placeholder-$ ${t}${r}`
            };
        }
    };
    function vl(e) {
        let t = e.match(/^op(?:acity)?-?(.+)$/);
        return t && t[1] != null ? v.bracket.percent(t[1]) != null : !1;
    }
    function yl(e) {
        return [ xl, hl, ...po(e), ...pl, ...gl, ...ml, ...dl, ...ul, ...bl ];
    }
    var $l = (e = {}) => (e.important = e.important ?? !1, {
        ...mo(e),
        name: "@unocss/preset-wind",
        theme: fl,
        rules: va,
        shortcuts: ya,
        variants: yl(e),
        postprocess: vo(e)
    });
    function kl(e, t, r) {
        return `calc(${t} + (${e} - ${t}) * ${r} / 100)`;
    }
    function wl(e, t, r) {
        let n = [ e, t ], i = [];
        for (let t = 0; t < 2; t++) {
            let e = typeof n[t] == "string" ? u(n[t]) : n[t];
            if (!e || ![ "rgb", "rgba" ].includes(e.type)) return;
            i.push(e);
        }
        let o = [];
        for (let e = 0; e < 3; e++) o.push(kl(i[0].components[e], i[1].components[e], r));
        return {
            type: "rgb",
            components: o,
            alpha: kl(i[0].alpha ?? 1, i[1].alpha ?? 1, r)
        };
    }
    function zl(e, t) {
        return wl("#fff", e, t);
    }
    function El(e, t) {
        return wl("#000", e, t);
    }
    function jl(e, t) {
        let r = Number.parseFloat(`${t}`);
        if (!Number.isNaN(r)) return r > 0 ? El(e, t) : zl(e, -r);
    }
    var Sl = {
        tint: zl,
        shade: El,
        shift: jl
    };
    function Cl() {
        let r;
        return {
            name: "mix",
            match(e, t) {
                r || (r = new RegExp(`^mix-(tint|shade|shift)-(-?\\d{1,3})(?:${t.generator.config.separators.join("|")})`));
                let n = e.match(r);
                if (n) return {
                    matcher: e.slice(n[0].length),
                    body: e => (e.forEach(r => {
                        if (r[1]) {
                            let t = u(`${r[1]}`);
                            if (t) {
                                let e = Sl[n[1]](t, n[2]);
                                e && (r[1] = m(e));
                            }
                        }
                    }), e)
                };
            }
        };
    }
    var Fl = (e = {}) => {
        let t = $l(e);
        return {
            ...t,
            name: "@unocss/preset-uno",
            variants: [ ...t.variants, Cl() ]
        };
    }, Al = Fl;
    window.__unocss_runtime = window.__unocss_runtime ?? {};
    window.__unocss_runtime.presets = Object.assign(window.__unocss_runtime?.presets ?? {}, {
        presetUno: e => Al(e)
    });
})();