<?php

return [
    'av' => [
        'asset' => [
            'cache' => (bool) env('APP_AV_ASSET_CACHE', false),
            'cache-duration' => (int) env('APP_AV_ASSET_CACHE_DURATION', 60),
            'cache-prefix' => (string) env('APP_AV_ASSET_CACHE_PREFIX', ''),
            'revision' => (string) env('APP_AV_ASSET_REVISION', '20240718'),
        ],

        'url' => [
            'secure' => (bool) env('APP_AV_URL_SECURE', false),
        ],

        'insecure-fallback-md5-auth' => (bool) env('APP_AV_INSECURE_FALLBACK_MD5_AUTH', false),
    ],
];
