<!doctype html><html lang="{{ config('app.locale') }}" class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $__avon['appName'] }}</title>

    <script>window.__avon=@json($__avon)</script>
    <script>window.__avon._component=@json($__avon_component)</script>
    <script>window.__avon._start=new Date().getTime()</script>

    <meta name="application-name" content="{{ $__avon['appName'] }}">
    <meta name="theme-color" content="{{ $__avon_app->themeColor }}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="{{ $__avon_app->getBaseAssetUrl('/favicon.ico') }}">
    <link rel="manifest" href="{{ $__avon_app->getBaseAssetUrl('/site.webmanifest') }}">
    <link rel="apple-touch-icon" href="{{ $__avon_app->getBaseAssetUrl('/icon-white.png') }}">

    <link rel="preload" as="font" type="font/woff2" crossorigin href="{{ $__avon_app->getBaseAssetUrl('/inter-ui/variable/InterVariable.woff2?v=4.0') }}">
    <link rel="preload" as="font" type="font/woff2" crossorigin href="{{ $__avon_app->getBaseAssetUrl('/inter-ui/variable/InterVariable-Italic.woff2?v=4.0') }}">
    <link rel="prefetch" as="font" type="font/woff2" crossorigin href="{{ $__avon_app->getBaseAssetUrl('/inter-ui/web/Inter-Regular.woff2?v=4.0') }}">
    <link rel="prefetch" as="font" type="font/woff2" crossorigin href="{{ $__avon_app->getBaseAssetUrl('/inter-ui/web/Inter-SemiBold.woff2?v=4.0') }}">
    <link rel="prefetch" as="font" type="font/woff2" crossorigin href="{{ $__avon_app->getBaseAssetUrl('/inter-ui/web/Inter-Medium.woff2?v=4.0') }}">
    <link rel="prefetch" as="font" type="font/woff2" crossorigin href="{{ $__avon_app->getBaseAssetUrl('/inter-ui/web/Inter-Bold.woff2?v=4.0') }}">
    <link rel="stylesheet" href="{{ $__avon_app->getBaseAssetUrl('/inter-ui/inter.min.css') }}">
    <link rel="stylesheet" href="{{ $__avon_app->getBaseAssetUrl('/inter-ui/inter-variable.min.css') }}">
    <style>{!! $__avon_app->getMainStyle() !!}</style>
    <script>{!! $__avon_app->getMainScript() !!}</script>

    {{-- app parts, login --}}
    <link rel="stylesheet" href="{{ $__avon_app->getBaseAssetUrl('/style.css') }}">
    <script src="{{ $__avon_app->getBaseAssetUrl('/script.js') }}"></script>

    {{-- app parts, rest --}}
    <link rel="stylesheet" href="{{ $__avon_app->getBaseAssetUrl('/app-styles.css') }}">
    <link rel="stylesheet" href="{{ $__avon_app->getBaseAssetUrl('/vendor-styles.css') }}">
    <script src="{{ $__avon_app->getBaseAssetUrl('/app-scripts.js') }}"></script>
    <script src="{{ $__avon_app->getBaseAssetUrl('/vendor-scripts.js') }}"></script>
</head>
<body
    uno-layer-base="font-sans leading-none antialiased text-gray-800 bg-white min-w-screen-md"
    uno-layer-size_xs-scope-size_xs="min-w-[160px]"
>
    <aside class="need-modern-support">
        <section class="prose">
            <h3>Mohon perbarui peramban (<em>browser</em>) anda.</h3>
            <ul>
                <li>Safari:
                    <a href="https://support.apple.com/id-id/HT201541" target="_blank" rel="noopener noreferrer">
                        macOS</a> /
                    <a href="https://support.apple.com/id-id/guide/ipad/ipad9a74c576/ipados" target="_blank" rel="noopener noreferrer">
                        iPadOS</a> /
                    <a href="https://support.apple.com/id-id/ios/update" target="_blank" rel="noopener noreferrer">
                        iPhone</a></li>
                <li><a href="https://www.mozilla.org/id-ID/firefox/browsers/" target="_blank" rel="noopener noreferrer">
                    Firefox</a></li>
                <li>
                    <a href="https://www.microsoft.com/id-id/edge" target="_blank" rel="noopener noreferrer">
                        Edge</a>,
                    <a href="https://www.google.com/chrome/" target="_blank" rel="noopener noreferrer">
                        Chrome</a>, atau
                    <a href="https://brave.com" target="_blank" rel="noopener noreferrer">
                        Brave</a></li>
                <li>
                    Cari <a href="https://duckduckgo.com/?q=web+browser+download" target="_blank" rel="noopener noreferrer">
                        peramban lainnya</a>
            </ul>
        </section>
    </aside>

    <div
        id="wrapper-parent"
        uno-layer-base="transition property-all"
    >
        <div
            id="wrapper"
        >
            <div id="app" uno-layer-base="bg-gray-50"></div>
            <div id="panel-overlay"></div>
        </div>
    </div>
    <div id="modal-overlay"></div>
</body>
</html>
