<?php

namespace CR\Library\Avon\Contracts;

interface Userable
{
    public function getAccountName();

    public function getAccountId();

    public function getAccountTeam();

    public function getAccountType();

    public function shouldResetPassword();

    public function shouldVerifyAccount();

    public function shouldConfirmPassword();

    public static function authenticationCallback();

    public static function resetPasswordCallback();
}
