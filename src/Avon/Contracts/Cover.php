<?php

namespace CR\Library\Avon\Contracts;

interface Cover
{
    public function resolveThumbnailUrl();
}
