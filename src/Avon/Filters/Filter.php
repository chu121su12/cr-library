<?php

namespace CR\Library\Avon\Filters;

use CR\Library\Avon\Helpers;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class Filter
{
    use \CR\Library\Avon\Concerns\AuthorizedToSee;
    use \CR\Library\Avon\Concerns\Component;

    protected $applyResolver;

    protected $blockingWithoutFilterValue;

    protected $component = 'select-filter';

    protected $isolateApplyQuery = true;

    protected $optionsCache;

    protected $preFilter;

    protected $showInline;

    protected $selectedKeyCache;

    public function apply($request, $query, $value)
    {
        return value($this->applyResolver, $request, $query, $value, $this);
    }

    public function applyWith($callback)
    {
        $this->applyResolver = $callback;

        return $this;
    }

    public function asPreFilter($set = true)
    {
        $this->preFilter = $set;

        return $this;
    }

    public function blockWithoutFilterValue($set = true)
    {
        $this->blockingWithoutFilterValue = (bool) $set;

        return $this;
    }

    public function defaultValue()
    {
        return null;
    }

    public function filter($requestFilters, $query, $request)
    {
        $value = $this->value($request, $requestFilters);

        if (! isset($value) && $this->blockingWithoutFilterValue) {
            return $query->whereRaw('0 = 1');
        }

        if (! $this->isolateApplyQuery) {
            return $this->apply($request, $query, $value);
        }

        return $query->where(function ($query) use ($request, $value) {
            return $this->apply($request, $query, $value);
        });
    }

    public function getOptions($request)
    {
        if (! $this->optionsCache) {
            $options = Collection::wrap($this->options($request));
            $defaultValue = null;
            $hadSelected = false;

            if (null !== ($selectedKey = $request->get($this->key()))) {
                if (isset($options->all()[$selectedKey])) {
                    $defaultValue = $options->all()[$selectedKey];
                }
            }
            else {
                $defaultValue = $this->defaultValue();
            }

            $this->optionsCache = $options
                ->map(function ($value, $key) use ($defaultValue, &$hadSelected) {
                    $option = [
                        'value' => $value,
                        'key' => $key,
                        'selected' => false,
                        'keyHash' => \mb_substr(\sha1($key), 0, 8),
                    ];

                    if (! $this->selectedKeyCache && $value === $defaultValue) {
                        $this->selectedKeyCache = $value;
                        $option['selected'] = $hadSelected = true;
                    }

                    return $option;
                })
                ->values();
        }

        return $this->optionsCache;
    }

    public function getPreFilter()
    {
        return $this->preFilter;
    }

    public function inline($set = true)
    {
        $this->showInline = $set;

        return $this;
    }

    public function key()
    {
        return Helpers::classToHtmlId(static::class) . '_filter';
    }

    public function options($request)
    {
        return [];
    }

    public function toJson()
    {
        return \array_merge([
            'component' => $this->resolveComponent(),
            'name' => $this->name(),
            'key' => $this->key(),
            'inline' => (bool) $this->showInline,
        ], $this->optionsJson(request()));
    }

    public function value($request, $requestFilters = null)
    {
        switch ((string) $this->resolveComponent()) {
            case 'boolean-filter': return $this->booleanFilterValue($request, $requestFilters);
            case 'select-filter': return $this->selectFilterValue($request, $requestFilters);
        }
    }

    protected function booleanFilterValue($request, $requestFilters = null)
    {
        if (! $requestFilters) {
            $queryFilters = $request->query('filters');
            $requestFilters = \is_array($queryFilters)
                ? $queryFilters
                : (\json_decode($queryFilters, true) ?: []);
        }

        if (! \array_key_exists($key = $this->key(), $requestFilters)) {
            return;
        }

        $optionKeys = Arr::wrap(\json_decode($requestFilters[$key]));

        return $this->getOptions($request)
            ->mapWithKeys(static function ($option) use ($optionKeys) {
                return [
                    $option['value'] => \array_search(
                        $option['key'],
                        $optionKeys,
                        true
                    ) !== false,
                ];
            })
            ->all();
    }

    protected function selectFilterValue($request, $requestFilters = null)
    {
        if (! $requestFilters) {
            $queryFilters = $request->query('filters');
            $requestFilters = \is_array($queryFilters)
                ? $queryFilters
                : (\json_decode($queryFilters, true) ?: []);
        }

        if (! \array_key_exists($key = $this->key(), $requestFilters)) {
            return;
        }

        $optionKey = $requestFilters[$key];

        $value = $this->getOptions($request)->firstWhere('key', $optionKey);

        if ($value === null) {
            return;
        }

        return Arr::get($value, 'value');
    }

    protected function optionsJson($request)
    {
        $options = $this->getOptions($request);

        return [
            'options' => $options
                ->map(
                    static function ($option) {
                        $return = [
                            'value' => $option['key'],
                        ];

                        if ($option['selected']) {
                            $return['selected'] = true;
                        }

                        return $return;
                    }
                ),
            'selected' => [
                'selected' => $this->selectedKeyCache !== null,
                'value' => $this->selectedKeyCache,
            ],
        ];
    }
}
