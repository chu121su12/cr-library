<?php

namespace CR\Library\Avon\Filters;

class TextFilter extends Filter
{
    protected $component = 'text-filter';

    protected $debounce = 0.5;

    public function debounce($debounce)
    {
        $this->debounce = $debounce;

        return $this;
    }

    public function toJson()
    {
        return \array_merge([
            'debounce' => (float) $this->debounce,
        ], parent::toJson());
    }

    public function value($request, $requestFilters = null)
    {
        if (! $requestFilters) {
            $queryFilters = $request->query('filters');
            $requestFilters = \is_array($queryFilters)
                ? $queryFilters
                : (\json_decode($queryFilters, true) ?: []);
        }

        if (! \array_key_exists($key = $this->key(), $requestFilters)) {
            return;
        }

        return (string) $requestFilters[$key];
    }

    protected function optionsJson($request)
    {
        return [];
    }
}
