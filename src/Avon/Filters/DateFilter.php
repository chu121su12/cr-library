<?php

namespace CR\Library\Avon\Filters;

class DateFilter extends Filter
{
    protected $component = 'date-filter';

    public function value($request, $requestFilters = null)
    {
        if (! $requestFilters) {
            $queryFilters = $request->query('filters');
            $requestFilters = \is_array($queryFilters)
                ? $queryFilters
                : (\json_decode($queryFilters, true) ?: []);
        }

        if (! \array_key_exists($key = $this->key(), $requestFilters)) {
            return;
        }

        $value = (string) $requestFilters[$key];

        return $value === '' ? null : $value;
    }

    protected function optionsJson($request)
    {
        return [];
    }
}
