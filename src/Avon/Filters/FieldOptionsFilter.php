<?php

namespace CR\Library\Avon\Filters;

use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class FieldOptionsFilter extends Filter
{
    protected $filterColumns;

    protected $name;

    public function __construct($name, $column = null)
    {
        $this->name = $name;
        $this->filterColumns = Arr::wrap($column ?: Str::slug($name, '_'));
        $this->applyResolver = function ($r, $query, $value) {
            if ((\is_array($value) && \count($value) === 0)
                || (! \is_array($value) && (string) $value === '')) {
                if ($this->blockingWithoutFilterValue) {
                    return $query->whereRaw('0 = 1');
                }

                return $query;
            }

            $booleanValues = \is_array($value) ? \array_keys(\array_filter($value)) : [];

            return $query->where(function ($q) use ($value, $booleanValues) {
                foreach ($this->filterColumns as $column) {
                    switch ((string) $this->component) {
                        case 'boolean-filter':
                            $q = $q->orWhereIn($column, $booleanValues);

                            break;

                        case 'select-filter':
                            $q = $q->orWhere($column, $value);

                            break;

                        default:
                            throw new Exception(\sprintf('Unknown filter component. (%s, %s)', $this->name(), static::class));
                    }
                }

                return $q;
            });
        };
    }

    public function key()
    {
        return Str::slug($this->name(), '_') . '_option_filter';
    }

    public function boolean()
    {
        $this->component = 'boolean-filter';

        return $this;
    }

    public function select()
    {
        $this->component = 'select-filter';

        return $this;
    }

    protected $options;

    public function options($request)
    {
        return value($this->options, $request);
    }

    public function withOptions($options)
    {
        $this->options = $options;

        return $this;
    }
}
