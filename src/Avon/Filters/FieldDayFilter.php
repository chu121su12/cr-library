<?php

namespace CR\Library\Avon\Filters;

use Carbon\CarbonImmutable;
use CR\Library\Helpers\Helpers as LibHelpers;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class FieldDayFilter extends DateFilter
{
    protected $defaultTodayOnly;

    protected $filterColumns;

    protected $fromToState = 0;

    protected $name;

    public function __construct($name, $column = null)
    {
        $this->name = $name;
        $this->filterColumns = Arr::wrap($column ?: Str::slug($name, '_'));
        $this->applyResolver = function ($r, $query, $value) {
            if (! isset($value)) {
                if ($this->blockingWithoutFilterValue) {
                    return $query->whereRaw('0 = 1');
                }

                if (! $this->defaultTodayOnly) {
                    return $query;
                }

                $value = micro_time();
            }

            try {
                $value = (new CarbonImmutable($value))->startOfDay();
            }
            catch (Exception $e) {
                LibHelpers::reportIgnoredException('Error parsing date.', $e);

                return $query->whereRaw('0 = 1');
            }

            return $query->where(function ($q) use ($value) {
                foreach ($this->filterColumns as $type => $column) {
                    if (\is_int($type)) {
                        $type = $this->fromToState;
                    }
                    else {
                        $tmp = $type;
                        $type = $column;
                        $column = $tmp;
                    }

                    $q = $q->orWhere(static function ($q) use ($value, $type, $column) {
                        switch ((string) $type) {
                            case '-1':
                            case 'from':
                                return $q->where($column, '>=', $value->toDateString());

                            case '1':
                            case 'to':
                                return $q->where($column, '<', $value->addDay()->toDateString());

                            default:
                                return $q
                                    ->where($column, '>=', $value->toDateString())
                                    ->where($column, '<', $value->addDay()->toDateString());
                        }
                    });
                }

                return $q;
            });
        };
    }

    public function defaultTodayOnly($set = true)
    {
        $this->defaultTodayOnly = $set;

        return $this;
    }

    public function from()
    {
        $this->fromToState = -1;

        return $this;
    }

    public function to()
    {
        $this->fromToState = 1;

        return $this;
    }

    public function key()
    {
        return Str::slug($this->name(), '_') . '_date_filter';
    }
}
