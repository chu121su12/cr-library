<?php

namespace CR\Library\Avon\Filters;

use CR\Library\Avon\Helpers;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class FieldTextFilter extends TextFilter
{
    protected $filterColumns;

    protected $name;

    protected $searchType = '_%';

    public function __construct($name, $column = null)
    {
        $this->name = $name;
        $this->filterColumns = Arr::wrap($column ?: Str::slug($name, '_'));
        $this->applyResolver = function ($r, $query, $value) {
            if ((string) $value === '') {
                if ($this->blockingWithoutFilterValue) {
                    return $query->whereRaw('0 = 1');
                }

                return $query;
            }

            return $query->where(function ($q) use ($value) {
                foreach ($this->filterColumns as $type => $column) {
                    if (\is_int($type)) {
                        $type = $this->searchType;
                    }
                    else {
                        $tmp = $type;
                        $type = $column;
                        $column = $tmp;
                    }

                    $parameters = Helpers::generateLikeSearchParameter(
                        \is_int($type) ? $this->searchType : $type,
                        $column,
                        $value
                    );

                    $q = $q->orWhere(...$parameters);
                }

                return $q;
            });
        };
    }

    public function key()
    {
        return Str::slug($this->name(), '_') . '_text_filter';
    }

    public function matchAnywhere()
    {
        $this->searchType = '%';

        return $this;
    }

    public function matchEnd()
    {
        $this->searchType = '%_';

        return $this;
    }

    public function matchEqual()
    {
        $this->searchType = '=';

        return $this;
    }

    public function matchStart()
    {
        $this->searchType = '_%';

        return $this;
    }
}
