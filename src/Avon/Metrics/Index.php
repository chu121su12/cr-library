<?php

namespace CR\Library\Avon\Metrics;

use CR\Library\Avon\Fields\FieldsCollection;
use CR\Library\Avon\Helpers;
use CR\Library\Avon\Resource;
use ErrorException;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Collection;
use Illuminate\Support\Enumerable;

class Index extends Metrics
{
    protected $component = 'index-metric';

    protected $fields;

    protected $query;

    protected $height = '2';

    protected $width = 'full';

    public function fields()
    {
        if (isset($this->fields)) {
            return Helpers::transformCastsToFields(value($this->fields));
        }

        throw new ErrorException(Helpers::abstractErrorMessage(self::class, __METHOD__));
    }

    public function query($request)
    {
        if (isset($this->query)) {
            return value($this->query, $request);
        }

        throw new ErrorException(Helpers::abstractErrorMessage(self::class, __METHOD__));
    }

    public function calculate($request)
    {
        $result = $this->resolveQuery($request)->map(
            function ($model) use ($request) {
                $fieldsCollection = new FieldsCollection($this->fields());

                $idField = $fieldsCollection->idField($request);
                $indexFields = $fieldsCollection->indexFields($request);

                if ($idField && $indexFields->search($idField, true) === false) {
                    $idField->toDisplayJson($indexFields, $model);
                }

                $fields = $indexFields
                    ->map->prepareDependants($request, $indexFields)
                    ->map->toDisplayJson($request, $model);

                return [
                    // 'id' => $fieldsCollection
                    //     ->idField($request)
                    //     ->resourceValue($model),

                    'fields' => $fields->all(),
                ];
            }
        );

        return new IndexResult($result->all());
    }

    public function withFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    public function withQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    public function toJson()
    {
        return \array_merge(
            parent::toJson(),
            [
                'builtInComponent' => true,
            ],
            $this->resolveMeta(request())
        );
    }

    protected function resolveQuery($request)
    {
        $query = $this->query($request);

        if (\is_string($query) && \class_exists($query)) {
            $query = new $query;
        }

        if ($query instanceof Resource) {
            return $query::resourceQuery()->get();
        }

        if ($query instanceof Model) {
            return $query::all();
        }

        if ($query instanceof EloquentBuilder) {
            return $query->get();
        }

        if ($query instanceof QueryBuilder) {
            return $query->get();
        }

        if ($query instanceof Enumerable) {
            return $query;
        }

        return Collection::wrap($query);
    }
}
