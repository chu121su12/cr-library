<?php

namespace CR\Library\Avon\Metrics;

class ValueResult
{
    public $allowZeroResult;

    public $format;

    public $prefix;

    public $previous;

    public $suffix;

    public $value;

    public function __construct($value = null)
    {
        $this->value = $value;
    }

    public function allowZeroResult($set = true)
    {
        $this->allowZeroResult = $set;

        return $this;
    }

    public function format($format)
    {
        $this->format = $format;

        return $this;
    }

    public function prefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    public function previous($previous)
    {
        $this->previous = $previous;

        return $this;
    }

    public function result($value = null)
    {
        $this->value = $value;

        return $this;
    }

    public function suffix($suffix)
    {
        $this->suffix = $suffix;

        return $this;
    }

    public function toArray()
    {
        return [
            'allowZeroResult' => (bool) $this->allowZeroResult,
            'format' => $this->format,
            'value' => $this->value,
            'previous' => $this->previous,
            'prefix' => $this->prefix,
            'suffix' => $this->suffix,
        ];
    }
}
