<?php

namespace CR\Library\Avon\Metrics;

use CR\Library\Avon\Concerns;
use CR\Library\Avon\Resource;
use CR\Library\Helpers\Helpers;
use Exception;
use Illuminate\Contracts\Database\Query\Builder as BuilderContract;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

abstract class Metrics
{
    use Concerns\AuthorizedToSee;
    use Concerns\Component;
    use Concerns\MetricVisibility;
    use Concerns\WithMeta;

    const __CACHE_BUSTER = '|';

    protected $cacheDuration;

    protected $cacheKeyResolver;

    protected $height = '1';

    protected $helpText = '';

    protected $shouldResolveCacheWithUser;

    protected $width = '1/3';

    abstract public function calculate($request);

    public function cacheFor()
    {
        return value($this->cacheDuration);
    }

    public function cacheWith($callback = null)
    {
        $this->cacheKeyResolver = $callback;

        return $this;
    }

    public function doubleHeight($double = true)
    {
        $this->height = $double ? '2' : '1';

        return $this;
    }

    public function generateResult($request)
    {
        $resultResolver = function () use ($request) {
            return $this->calculate($request)->toArray();
        };

        if ($cacheDuration = $this->resolveCacheDuration()) {
            try {
                return Cache::remember(
                    $this->resolveCacheKey($request),
                    $cacheDuration,
                    $resultResolver
                );
            }
            catch (Exception $e) {
                Helpers::reportIgnoredException('Error resolving cache/cache key.', $e);
            }
        }

        return $resultResolver();
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function height($set = '1')
    {
        $this->height = $set;

        return $this;
    }

    public function help($helpText)
    {
        $this->helpText = $helpText;

        return $this;
    }

    public function resolveCacheWithUser($set = true)
    {
        $this->shouldResolveCacheWithUser = (bool) $set;

        return $this;
    }

    public function setCacheFor($duration)
    {
        $this->cacheDuration = $duration;

        return $this;
    }

    public function toJson()
    {
        return [
            'component' => $this->resolveComponent(),
            'name' => $this->name(),
            'uriKey' => static::uriKey(),
            'builtInComponent' => $this->isBuiltInComponent(),
            'width' => $this->width,
            'height' => (string) ((int) $this->height ?: 1),
            'helpText' => $this->helpText,
        ];
    }

    public function width($width)
    {
        if (\in_array($width, ['1/2', '1/3', '2/3', 'full'], true)) {
            $this->width = $width;
        }

        return $this;
    }

    protected function resolveAggregateColumn($aggregate, $column, $query)
    {
        if ($aggregate === 'count' && ! $column) {
            if (\method_exists($query, 'getQualifiedKeyName')) {
                return $query->getQualifiedKeyName();
            }

            if ($query instanceof EloquentBuilder) {
                return $query->getModel()->getQualifiedKeyName();
            }

            throw new Exception(\sprintf('Cannot infer aggregate column name. (%s)', static::class));
        }

        return $column;
    }

    protected function resolveCacheDuration()
    {
        if ($duration = $this->cacheFor()) {
            $duration = \is_scalar($duration)
                ? $duration
                : micro_time()->diffInRealSeconds($duration, false);
        }

        return (int) $duration;
    }

    protected function resolveCacheKey($request)
    {
        $base = self::__CACHE_BUSTER;

        $range = $request->get('range') ?: (\method_exists($this, 'getDefaultRange') ? $this->getDefaultRange() : '0');

        $user = $this->shouldResolveCacheWithUser ? $request->user()->getKey() : '';

        $key = $this->cacheKeyResolver === null
            ? \md5(static::class . '|' . \json_encode($request))
            : \call_user_func($this->cacheKeyResolver, $request, $this);

        return "_avon.{$base}.metrics..{$request->dashboard}..{$this->resolveComponent()}.{$range}.{$user}.{$key}";
    }

    protected function resolveModelQuery($model)
    {
        if (\is_string($model) && \class_exists($model)) {
            $model = new $model;
        }

        if ($model instanceof Resource) {
            if ($resource = $model->resource()) {
                return $resource::query();
            }

            return $model::resourceQuery();
        }

        if ($model instanceof Model) {
            return $model::query();
        }

        if ($model instanceof BuilderContract) {
            return $model;
        }

        throw new Exception('Unknown query');
    }
}
