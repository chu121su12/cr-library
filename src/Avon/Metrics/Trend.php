<?php

namespace CR\Library\Avon\Metrics;

use CR\Library\Avon\Helpers;
use ErrorException;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

class Trend extends Metrics
{
    use RangedMetrics;

    protected $component = 'trend-metric';

    protected $ranges = [
        30 => '30',
        60 => '60',
        90 => '90',
    ];

    public function calculate($request)
    {
        throw new ErrorException(Helpers::abstractErrorMessage(self::class, __METHOD__));
    }

    public function toJson()
    {
        return \array_merge(
            parent::toJson(),
            [
                'builtInComponent' => true,

                'ranges' => collect($this->resolveRanges())->map(static function ($value, $key) {
                    return [
                        'key' => $key,
                        'value' => $value,
                    ];
                })->values()->all(),

                'defaultRange' => $this->getDefaultRange(),
            ],
            $this->resolveMeta(request())
        );
    }

    final public function averageByDays($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Day', 'average', $column, $dateColumn);
    }

    final public function averageByHours($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Hour', 'average', $column, $dateColumn);
    }

    final public function averageByMinutes($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Minute', 'average', $column, $dateColumn);
    }

    final public function averageByMonths($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Month', 'average', $column, $dateColumn);
    }

    final public function averageByWeeks($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Week', 'average', $column, $dateColumn);
    }

    final public function averageByYears($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Year', 'average', $column, $dateColumn);
    }

    final public function countByDays($request, $model, $column = null, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Day', 'count', $column, $dateColumn);
    }

    final public function countByHours($request, $model, $column = null, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Hour', 'count', $column, $dateColumn);
    }

    final public function countByMinutes($request, $model, $column = null, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Minute', 'count', $column, $dateColumn);
    }

    final public function countByMonths($request, $model, $column = null, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Month', 'count', $column, $dateColumn);
    }

    final public function countByWeeks($request, $model, $column = null, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Week', 'count', $column, $dateColumn);
    }

    final public function countByYears($request, $model, $column = null, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Year', 'count', $column, $dateColumn);
    }

    final public function maxByDays($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Day', 'max', $column, $dateColumn);
    }

    final public function maxByHours($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Hour', 'max', $column, $dateColumn);
    }

    final public function maxByMinutes($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Minute', 'max', $column, $dateColumn);
    }

    final public function maxByMonths($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Month', 'max', $column, $dateColumn);
    }

    final public function maxByWeeks($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Week', 'max', $column, $dateColumn);
    }

    final public function maxByYears($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Year', 'max', $column, $dateColumn);
    }

    final public function minByDays($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Day', 'min', $column, $dateColumn);
    }

    final public function minByHours($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Hour', 'min', $column, $dateColumn);
    }

    final public function minByMinutes($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Minute', 'min', $column, $dateColumn);
    }

    final public function minByMonths($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Month', 'min', $column, $dateColumn);
    }

    final public function minByWeeks($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Week', 'min', $column, $dateColumn);
    }

    final public function minByYears($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Year', 'min', $column, $dateColumn);
    }

    final public function sumByDays($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Day', 'sum', $column, $dateColumn);
    }

    final public function sumByHours($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Hour', 'sum', $column, $dateColumn);
    }

    final public function sumByMinutes($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Minute', 'sum', $column, $dateColumn);
    }

    final public function sumByMonths($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Month', 'sum', $column, $dateColumn);
    }

    final public function sumByWeeks($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Week', 'sum', $column, $dateColumn);
    }

    final public function sumByYears($request, $model, $column, $dateColumn = null)
    {
        return $this->trendByPeriod($request, $model, 'Year', 'sum', $column, $dateColumn);
    }

    public function result($value)
    {
        return new TrendResult($value);
    }

    protected function trendByPeriod($request, $model, $period, $aggregate, $column, $dateColumn)
    {
        $query = $this->resolveModelQuery($model);

        $column = $this->resolveAggregateColumn($aggregate, $column, $query);

        $dateColumn = $dateColumn ?: $this->dateColumn;

        $rangeList = $this->trendRangeDates(
            $range = (int) $request->get('range', \key($this->resolveRanges())),
            $period
        );

        $results = [];

        while ($range--) {
            $q = $query->clone_()->from(
                $query->clone_()
                    ->select("{$column} as sub_aggregate")
                    ->where($dateColumn, '>=', $rangeList[$range]->toDateTimeString())
                    ->where($dateColumn, '<', $rangeList[$range + 1]->toDateTimeString())
                    ->groupBy($column),
                'sub'
            );

            tap($q instanceof EloquentBuilder ? $q->getQuery() : $q, static function ($q) {
                $q->wheres = [];
                $q->bindings['where'] = [];
            });

            $results[] = [
                'value' => $q->{$aggregate}('sub.sub_aggregate'),
                'date' => $rangeList[$range + 1],
            ];
        }

        return $this->result(head($results)['value'])->trend(\array_reverse($results));
    }

    private function trendRangeDates($range, $period)
    {
        if (! \is_int($range) || $range < 1) {
            \trigger_error("Range '{$range}' must be a positive integer.");
        }

        $ranges = [];
        $now = micro_time();
        do {
            $ranges[] = $now->copy()
                ->{"sub{$period}"}($range - 1)
                ->{"startOf{$period}"}();
        }
        while ($range--);

        return $ranges;
    }
}
