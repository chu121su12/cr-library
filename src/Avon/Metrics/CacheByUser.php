<?php

namespace CR\Library\Avon\Metrics;

trait CacheByUser
{
    protected function resolveCacheKey($request)
    {
        $range = \method_exists($this, 'getDefaultRange') ? $request->get('range', $this->getDefaultRange()) : '0';

        $user = $request->user()->getKey();

        $key = $this->cacheKeyResolver === null
            ? \md5(static::class . '|' . \json_encode($request))
            : \call_user_func($this->cacheKeyResolver, $request, $this);

        return "avon.metrics.{$this->resolveComponent()}.{$range}.{$user}.{$key}";
    }
}
