<?php

namespace CR\Library\Avon\Metrics;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

trait RangedMetrics
{
    protected $dateColumn = Model::CREATED_AT;

    protected $defaultRange;

    public function dateColumn($name)
    {
        $this->dateColumn = $name;

        return $this;
    }

    public function defaultRange($range)
    {
        $this->defaultRange = $range;

        return $this;
    }

    public function getDefaultRange()
    {
        return $this->defaultRange ?: head($this->resolveRanges());
    }

    public function withRanges($ranges)
    {
        $this->ranges = $ranges;

        return $this;
    }

    public function withoutRanges()
    {
        $this->ranges = null;

        return $this;
    }

    protected function resolveRanges()
    {
        if (isset($this->ranges)) {
            $ranges = $this->ranges;
        }
        elseif (\method_exists($this, 'ranges')) {
            $ranges = $this->ranges();
        }
        else {
            $ranges = [];
        }

        return Arr::wrap($ranges);
    }
}
