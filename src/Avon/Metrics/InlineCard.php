<?php

namespace CR\Library\Avon\Metrics;

use CR\Library\Helpers\Helpers;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class InlineCard extends Metrics
{
    protected $calculateResolver;

    protected $key;

    protected $metricInstance;

    protected $name;

    protected function __construct($metric, $name)
    {
        $this->metricInstance = $metric;

        $this->name = $name;

        $this->withUriKey($name);
    }

    public static function partition($name, $calculation = null)
    {
        return (new self(new Partition, $name))->calculateWith($calculation);
    }

    public static function index($name, $query, $fields)
    {
        $index = (new Index)->withQuery($query)->withFields($fields);

        $instance = new self($index, $name);
        $instance->height = $index->getHeight();
        $instance->width = $index->getWidth();

        return $instance->calculateWith(static function ($request, $index) {
            return $index->calculate($request);
        });
    }

    public static function trend($name, $calculation = null)
    {
        return (new self(new Trend, $name))->calculateWith($calculation);
    }

    public static function value($name, $calculation = null)
    {
        return (new self(new Value, $name))->calculateWith($calculation);
    }

    public function calculate($request)
    {
        return \call_user_func($this->calculateResolver, $request, $this->metricInstance);
    }

    public function calculateWith($calculation)
    {
        $this->calculateResolver = $calculation;

        return $this;
    }

    public function generateResult($request)
    {
        $resultResolver = function () use ($request) {
            return $this->calculate($request)->toArray();
        };

        if ($cacheDuration = $this->metricInstance->resolveCacheDuration()) {
            try {
                return Cache::remember(
                    $this->resolveCacheKey($request),
                    $cacheDuration,
                    $resultResolver
                );
            }
            catch (Exception $e) {
                Helpers::reportIgnoredException('Error resolving cache/cache key.', $e);
            }
        }

        return $resultResolver();
    }

    public function matchUriKey($key)
    {
        return $this->key === $key;
    }

    public function setCacheFor($duration)
    {
        $this->metricInstance->setCacheFor($duration);

        return $this;
    }

    public function toJson()
    {
        return \array_merge(
            $this->metricInstance->toJson(),
            parent::toJson(),
            [
                'uriKey' => $this->key,
            ]
        );
    }

    public function withName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function withUriKey($key)
    {
        $this->key = Str::slug($key, '-');

        return $this;
    }

    protected function resolveCacheKey($request)
    {
        $base = self::__CACHE_BUSTER;

        $range = $request->get('range') ?: (\method_exists($this->metricInstance, 'getDefaultRange') ? $this->metricInstance->getDefaultRange() : '0');

        $user = $this->shouldResolveCacheWithUser ? $request->user()->getKey() : '';

        $key = $this->cacheKeyResolver === null
            ? \md5($this->key . '|' . \json_encode($request))
            : \call_user_func($this->cacheKeyResolver, $request, $this);

        return "_avon.{$base}.metrics..{$request->dashboard}..{$this->resolveComponent()}.{$range}.{$user}.{$key}";
    }

    protected function resolveComponent()
    {
        return $this->metricInstance->resolveComponent();
    }

    public function __call($method, $params)
    {
        if (\method_exists($metric = $this->metricInstance, $method)) {
            $metric->{$method}(...$params);

            return $this;
        }

        throw new Exception('Invalid method');
    }
}
