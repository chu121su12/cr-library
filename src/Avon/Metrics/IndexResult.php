<?php

namespace CR\Library\Avon\Metrics;

class IndexResult
{
    public $data;

    public $noDataText = 'No Data';

    public $numbered = false;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function noDataText($text)
    {
        $this->noDataText = $text;

        return $this;
    }

    public function numbered($numbered = true)
    {
        $this->numbered = $numbered;

        return $this;
    }

    public function toArray()
    {
        return [
            'data' => $this->data,
            'numbered' => (bool) $this->numbered,
            'noDataText' => $this->noDataText,
        ];
    }
}
