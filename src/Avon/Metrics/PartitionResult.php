<?php

namespace CR\Library\Avon\Metrics;

use Illuminate\Support\Arr;

class PartitionResult
{
    public $noDataText = 'No Data';

    public $value;

    protected $colors;

    protected $labelResolver;

    public function __construct(array $value)
    {
        $this->value($value);
    }

    public function colors(array $colors)
    {
        $this->colors = $colors;

        return $this;
    }

    public function label($callback)
    {
        $this->labelResolver = $callback;

        return $this;
    }

    public function noDataText($text)
    {
        $this->noDataText = $text;

        return $this;
    }

    public function toArray()
    {
        $labelResolver = \is_array($this->labelResolver)
            ? function ($label) { return data_get($this->labelResolver, $label); }
        : $this->labelResolver;

        return [
            'noDataText' => $this->noDataText,
            'value' => collect(Arr::wrap($this->value))
                ->filter(static function ($value) {
                    return $value !== null;
                })
                ->mapWithKeys(static function ($value, $label) use ($labelResolver) {
                    return [$labelResolver($label, $value) ?: $label => $value];
                })
                ->map(function ($value, $label) {
                    return [
                        'color' => Arr::get($this->colors, $label),
                        'label' => $label,
                        'value' => $value,
                    ];
                })->values()->all(),
        ];
    }

    public function value(array $value)
    {
        $this->value = $value;

        return $this;
    }
}
