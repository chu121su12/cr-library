<?php

namespace CR\Library\Avon\Metrics;

class TrendResult
{
    public $asBarChart;

    public $format;

    public $locale;

    public $prefix;

    public $showLatestValue;

    public $showSumValue;

    public $suffix;

    public $timeFormat;

    public $timezone;

    public $trend;

    public $value;

    public function __construct($value = null)
    {
        $this->value = $value;
    }

    public function asBarChart($set = true)
    {
        $this->asBarChart = $set;

        return $this;
    }

    public function format($format)
    {
        $this->format = $format;

        return $this;
    }

    public function prefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    public function result($value = null)
    {
        $this->value = $value;

        return $this;
    }

    public function showLatestValue($set = true)
    {
        $this->showLatestValue = $set;

        return $this;
    }

    public function showSumValue($set = true)
    {
        $this->showSumValue = $set;

        return $this;
    }

    public function suffix($suffix)
    {
        $this->suffix = $suffix;

        return $this;
    }

    public function toArray()
    {
        return [
            'asBarChart' => (bool) $this->asBarChart,
            'format' => $this->format,
            'locale' => $this->locale ?: config('app.locale') ?: 'id',
            'prefix' => $this->prefix,
            'showLatestValue' => (bool) $this->showLatestValue,
            'showSumValue' => (bool) $this->showSumValue,
            'suffix' => $this->suffix,
            'timeFormat' => $this->timeFormat ?: 'dddd, D MMMM YYYY, HH:mm:ss',
            'timezone' => $this->timezone ?: config('app.timezone'),
            'trend' => $this->trend,
            'value' => $this->value,
        ];
    }

    public function trend(array $trend)
    {
        $this->trend = $trend;

        return $this;
    }
}
