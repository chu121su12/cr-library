<?php

namespace CR\Library\Avon\Metrics;

use CR\Library\Avon\Helpers;
use ErrorException;

class Partition extends Metrics
{
    protected $component = 'partition-metric';

    public function calculate($request)
    {
        throw new ErrorException(Helpers::abstractErrorMessage(self::class, __METHOD__));
    }

    public function toJson()
    {
        return \array_merge(
            parent::toJson(),
            [
                'builtInComponent' => true,
            ],
            $this->resolveMeta(request())
        );
    }

    final public function average($request, $model, $aggregateColumn, $labelColumn)
    {
        return $this->aggregateParition($request, $model, $labelColumn, 'avg', $aggregateColumn);
    }

    final public function count($request, $model, $labelColumn, $aggregateColumn = null)
    {
        return $this->aggregateParition($request, $model, $labelColumn, 'count', isset($aggregateColumn) ? $aggregateColumn : '1');
    }

    final public function max($request, $model, $aggregateColumn, $labelColumn)
    {
        return $this->aggregateParition($request, $model, $labelColumn, 'max', $aggregateColumn);
    }

    final public function min($request, $model, $aggregateColumn, $labelColumn)
    {
        return $this->aggregateParition($request, $model, $labelColumn, 'min', $aggregateColumn);
    }

    final public function sum($request, $model, $aggregateColumn, $labelColumn)
    {
        return $this->aggregateParition($request, $model, $labelColumn, 'sum', $aggregateColumn);
    }

    public function result($value)
    {
        return new PartitionResult($value);
    }

    protected function aggregateParition($request, $model, $labelColumn, $aggregate, $aggregateColumn)
    {
        $query = $this->resolveModelQuery($model);

        $labelColumn = $this->resolveAggregateColumn($aggregate, $labelColumn, $query);

        $result = $query->selectRaw(
            "{$labelColumn} as label, {$aggregate}({$aggregateColumn}) as aggregate"
        )
            ->groupBy($labelColumn)
            ->pluck('aggregate', 'label');

        $partition = \array_map(static function ($item) {
            if (\is_int($item)) {
                return (int) $item;
            }

            return (float) $item;
        }, $result->all());

        return $this->result($partition);
    }
}
