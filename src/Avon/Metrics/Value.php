<?php

namespace CR\Library\Avon\Metrics;

use CR\Library\Avon\Helpers;
use ErrorException;

class Value extends Metrics
{
    use RangedMetrics;

    protected $component = 'value-metric';

    protected $ranges = [
        'TODAY' => 'Today',
        'MTD' => 'Month To Date',
        'QTD' => 'Quarter To Date',
        'YTD' => 'Year To Date',
        'ALL' => 'All Time',
    ];

    public function calculate($request)
    {
        throw new ErrorException(Helpers::abstractErrorMessage(self::class, __METHOD__));
    }

    public function toJson()
    {
        return \array_merge(
            parent::toJson(),
            [
                'builtInComponent' => true,

                'ranges' => collect($this->resolveRanges())->map(static function ($value, $key) {
                    return [
                        'key' => $key,
                        'value' => $value,
                    ];
                })->values()->all(),

                'defaultRange' => $this->getDefaultRange(),
            ],
            $this->resolveMeta(request())
        );
    }

    final public function average($request, $model, $column, $dateColumn = null)
    {
        return $this->aggregateValue($request, $model, 'avg', $column, $dateColumn);
    }

    final public function count($request, $model, $column = null, $dateColumn = null)
    {
        return $this->aggregateValue($request, $model, 'count', $column, $dateColumn);
    }

    final public function max($request, $model, $column, $dateColumn = null)
    {
        return $this->aggregateValue($request, $model, 'max', $column, $dateColumn);
    }

    final public function min($request, $model, $column, $dateColumn = null)
    {
        return $this->aggregateValue($request, $model, 'min', $column, $dateColumn);
    }

    final public function sum($request, $model, $column, $dateColumn = null)
    {
        return $this->aggregateValue($request, $model, 'sum', $column, $dateColumn);
    }

    public function result($value)
    {
        return new ValueResult($value);
    }

    protected function aggregateRangeValues($request)
    {
        $now = micro_time();

        switch ($key = (string) $request->get('range', \key($this->resolveRanges()))) {
            case 'TODAY':
                return [
                    $now->copy()->toDateString(),
                    $now->copy()->subDay()->toDateString(),
                    $now->copy()->subDays(2)->toDateString(),
                    $now->copy()->subDays(3)->toDateString(),
                ];

            case 'MTD':
                return [
                    $now->copy()->toDateString(),
                    $now->copy()->day(1)->toDateString(),
                    $now->copy()->subMonth()->toDateString(),
                    $now->copy()->day(1)->subMonth()->toDateString(),
                ];

            case 'QTD':
                return [
                    $now->copy()->toDateString(),
                    $now->copy()->day(1)->startOfQuarter()->toDateString(),
                    $now->copy()->subQuarter()->toDateString(),
                    $now->copy()->day(1)->subQuarter()->startOfQuarter()->toDateString(),
                ];

            case 'YTD':
                return [
                    $now->copy()->toDateString(),
                    $now->copy()->day(1)->month(1)->toDateString(),
                    $now->copy()->subYear()->toDateString(),
                    $now->copy()->day(1)->month(1)->subYear()->toDateString(),
                ];

            case 'ALL':
                return [
                    $now->copy()->toDateString(),
                    $now->copy()->day(1)->month(1)->year(1)->toDateString(),
                    $now->copy()->subDay()->toDateString(),
                    $now->copy()->toDateString(),
                ];

            default:
                $key = (int) $key;

                if (! \is_int($key) || $key < 1) {
                    \trigger_error("Range '{$key}' must be a positive integer.");
                }

                --$key;

                return [
                    $now->copy()->toDateString(),
                    $now->copy()->subDays($key)->toDateString(),
                    $now->copy()->subDays(1 + $key)->toDateString(),
                    $now->copy()->subDays(1 + 2 * $key)->toDateString(),
                ];
        }
    }

    protected function aggregateValue($request, $model, $aggregate, $column = null, $dateColumn = null)
    {
        $query = $this->resolveModelQuery($model);
        $dateColumn = $dateColumn ?: $this->dateColumn;
        $ranges = $this->aggregateRangeValues($request);

        $value = $query
            ->whereDate($dateColumn, '>=', $ranges[1])
            ->whereDate($dateColumn, '<=', $ranges[0])
            ->{$aggregate}();

        $previous = $query
            ->whereDate($dateColumn, '>=', $ranges[3])
            ->whereDate($dateColumn, '<=', $ranges[2])
            ->{$aggregate}();

        return $this->result($value)->previous($previous);
    }
}
