<?php

namespace CR\Library\Avon;

use CR\Library\Helpers\Reply;
use Illuminate\Support\Str;
use ReflectionClass;

class Tool
{
    use Concerns\AuthorizedToSee;
    use Concerns\Component;
    use Concerns\Pageable;
    use Concerns\RendersAssets;

    public function getData($request, $key)
    {
        $method = Str::camel("data-{$key}");

        if (\method_exists($this, $method)) {
            return $this->{$method}($request);
        }

        return (new Reply)->sessionMessage('Tidak ada balasan untuk data ini.');
    }

    public function getViewData($request)
    {
        $view = (string) static::renderView();

        // $source = \file_get_contents((new ReflectionClass($this))->getFileName());
        $source = '';

        $signature = \md5($view . $source);

        if ($request->query('signature') === $signature) {
            return response()->json([
                'payloadSignature' => $signature,
            ], 200, [], \JSON_UNESCAPED_UNICODE);
        }

        return response()->json([
            'component' => $this->resolveComponent(),
            'label' => static::label(),
            'payload' => $view,
            'payloadSignature' => $signature,
        ], 200, [], \JSON_UNESCAPED_UNICODE);
    }

    public function renderNavigation()
    {
        //
    }

    public function runAction($request, $key)
    {
        $method = Str::camel("action-{$key}");

        if (\method_exists($this, $method)) {
            return $this->{$method}($request);
        }

        return (new Reply)->sessionMessage('Tidak ada balasan untuk aksi ini.')->withReplaceSelf();
    }

    public static function group()
    {
        return isset(static::$group) ? static::$group : 'Tool';
    }
}
