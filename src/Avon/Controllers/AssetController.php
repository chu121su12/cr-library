<?php

namespace CR\Library\Avon\Controllers;

use CR\Library\Avon\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AssetController
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function publicAssetPath(Request $request)
    {
        $assetRoute = Str::of($request->getPathInfo())->after($this->app->getBaseUrl('/'))->start('/')->toString();

        if (str_contains('../', $assetRoute)) {
            abort(400);
        }

        return Storage::build(public_path())->response($assetRoute);
        // return Storage::build(public_path())->download($assetRoute);
    }
}
