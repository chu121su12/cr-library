<?php

namespace CR\Library\Avon\Controllers;

use CR\Library\Avon\Application;
use Illuminate\Support\Arr;

class ViewStaticFileController
{
    const FAVICON_TRANSPARENT_16X16 = 'AAABAAEAEBACAAEAAQCwAAAAFgAAACgAAAAQAAAAIAAAAAEAAQAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//wAA//8AAP//AAD//wAA//8AAP//AAD//wAA//8AAP//AAD//wAA//8AAP//AAD//wAA//8AAP//AAD//wAA';

    const ICON_TRANSPARENT_1X1 = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABJJREFUeF4FwIEIAAAAAKD9qY8AAgABdDtSRwAAAABJRU5ErkJggg==';

    const ICON_TRANSPARENT_512X512 = 'iVBORw0KGgoAAAANSUhEUgAAAgAAAAIAAQMAAADOtka5AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAADZJREFUeF7twAEBAAAAgqD+r26IsDgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAOCAAABp+wwTAAAAABJRU5ErkJggg==';

    const ICON_WHITE_1X1 = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX///+nxBvIAAAAEklEQVR4XgXAgQgAAAAAoP2pjwACAAF0O1JHAAAAAElFTkSuQmCC';

    const ICON_WHITE_512X512 = 'iVBORw0KGgoAAAANSUhEUgAAAgAAAAIAAQMAAADOtka5AAAAA1BMVEX///+nxBvIAAAANklEQVR4Xu3AAQEAAACCoP6vboiwOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAA4IAAAGn7DBMAAAAAElFTkSuQmCC';

    public function humans()
    {
        return response($this->humansTxtContent(), 200, $this->assetHeader('text/plain; charset=utf-8'));
    }

    public function favicon($id)
    {
        return response(\base64_decode(self::FAVICON_TRANSPARENT_16X16, true), 200, $this->assetHeader('image/x-icon'));
    }

    public function iconTransparent($id)
    {
        return response(\base64_decode(self::ICON_TRANSPARENT_512X512, true), 200, $this->assetHeader('image/png'));
    }

    public function iconWhite($id)
    {
        return response(\base64_decode(self::ICON_WHITE_512X512, true), 200, $this->assetHeader('image/png'));
    }

    public function serviceWorker()
    {
        return response($this->serviceWorkerContent(), 200, $this->assetHeader('application/javascript; charset=utf-8', null));
    }

    public function webManifest(Application $app, $id)
    {
        $viewData = $app->getViewData();

        $base = $viewData['baseUrl'];

        $startUrl = \rtrim((string) $app->url($app->getPrefix()), '/') . '/';

        $icons = [
            [
                'src' => "{$base}/application/asset/{$app->getAssetRevision()}/icon.png",
                'sizes' => '512x512',
                'type' => 'image/png',
            ],
        ];

        return response()->json(
            $viewData['useServiceWorker'] ? [
                'id' => $startUrl,
                'name' => $viewData['appName'],
                'short_name' => $viewData['appName'],
                'display' => 'standalone',
                'background_color' => '#fff',
                'theme_color' => $app->themeColor,
                'scope' => $startUrl,
                'start_url' => $startUrl,
                'icons' => $icons,
                'serviceworker' => [
                    'src' => 'sw.js',
                    'scope' => '.',
                    'update_via_cache' => 'imports',
                ],
            ] : [
                'id' => $startUrl,
                'name' => $viewData['appName'],
                'icons' => $icons,
            ],
            200,
            $this->assetHeader('application/manifest+json; charset=utf-8', null),
            \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE | \JSON_PRETTY_PRINT
        );
    }

    private function assetHeader($contentType, $lastModified = null)
    {
        return Arr::whereNotNull([
            'Last-Modified' => \func_num_args() === 2
                ? $lastModified
                : micro_time()->setTimezone('GMT')->startOfDay()->format('D, d M Y H:i:s T'),
            'Cache-Control' => \sprintf(
                'no-transform, private, max-age=%s, must-revalidate',
                24 * 60 * 60
            ),
            'Keep-Alive' => '',
            'Content-Type' => $contentType,
        ]);
    }

    private function humansTxtContent()
    {
        return <<<'TEXT'
# humanstxt.org/

  CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/)

# APP

  Avon
  Application version ∞
  Christian, programmer, @crawis

TEXT;
    }

    private function serviceWorkerContent()
    {
        return <<<'JS'
(function(self, factory) {
    try {
        factory(self);
    } catch (e) {
        console.error(e);
    }
})(this, function (self) {
    self.addEventListener('install', function() { });
    self.addEventListener('activate', function() { });
    self.addEventListener('fetch', function(event) {
        event.respondWith(fetch(event.request));
    });
});

JS;
    }
}
