<?php

namespace CR\Library\Avon\Controllers;

use CR\Library\Avon\Application;
use CR\Library\Avon\Fields\FieldsCollection;
use CR\Library\Avon\Fields\RelationshipField;
use CR\Library\Avon\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class ApiResourceRequest
{
    public $app;

    public $request;

    public $resourceClass;

    public $viaResourceClass;

    private $actions;

    private $cards;

    private $fields;

    private $filters;

    private $lenses;

    private $resourceInstance;

    private $viaFields;

    private $viaResourceInstance;

    private $viaModelInstance;

    private $viaPivotFields;

    public function __construct(Application $app, Request $request)
    {
        $resource = $request->route('resource');

        $this->request = $request;
        $this->app = $app;
        $this->resourceClass = $app->getResource($resource);

        $via = $request->query('viaResource');
        if ($via) {
            $this->viaResourceClass = $app->getResource($via);
        }
    }

    public function idField()
    {
        return $this->fields()->idField($this->request);
    }

    public function fields()
    {
        if (! $this->fields) {
            $this->fields = new FieldsCollection($this->getEmptyResourceInstance()->fields($this->request));
        }

        return $this->fields;
    }

    public function viaFields()
    {
        if (! $this->viaFields) {
            $this->viaFields = new FieldsCollection($this->getEmptyViaResourceInstance()->fields($this->request));
        }

        return $this->viaFields;
    }

    public function filters()
    {
        if (! $this->filters) {
            $this->filters = collect($this->getEmptyResourceInstance()->filtersForIndex(
                $this->request,
                $this->fields()->indexFields($this->request)
            ));
        }

        return $this->filters;
    }

    public function actions()
    {
        if (! $this->actions) {
            $this->actions = collect($this->getEmptyResourceInstance()->actions($this->request));
        }

        return $this->actions;
    }

    public function cards()
    {
        if (! $this->cards) {
            $this->cards = collect($this->getEmptyResourceInstance()->cards($this->request));
        }

        return $this->cards;
    }

    public function lenses()
    {
        if (! $this->lenses) {
            $this->lenses = collect($this->getEmptyResourceInstance()->lenses($this->request));
        }

        return $this->lenses;
    }

    public function getResourceModelInstance($create = false)
    {
        $resourceClass = $this->resourceClass;

        if (\class_exists($modelClass = $resourceClass::resourceModel())) {
            return new $modelClass;
        }

        if ($create && \method_exists($resourceClass, 'newResourceFromModel')) {
            return $resourceClass::newResourceFromModel([]);
        }
    }

    public function handlingRelatedResource()
    {
        return \count($this->request->only(['viaResource', 'viaResourceId', 'viaRelationship'])) === 3;
    }

    public function resourceExpired(Resource $resource, $retrievedAt)
    {
        if (! $resource::trafficCop($this->request)) {
            return false;
        }

        $resourceModel = $resource->resource();
        if (! $resourceModel->timestamps) {
            return false;
        }

        if (! ($updatedAtColumn = $resourceModel->getUpdatedAtColumn())) {
            return false;
        }

        $retrievedAt = Date::createFromFormat(
            'Y-m-d\TH:i:s.u\Z',
            $this->request->get('_retrieved_at'),
            'GMT'
        );

        if (! $retrievedAt) {
            return false;
        }

        return $retrievedAt->lt($resourceModel->{$updatedAtColumn});
    }

    public function getEmptyResourceInstance()
    {
        if (! $this->resourceInstance) {
            if (! $this->resourceClass) {
                abort(403);
            }

            $this->resourceInstance = new $this->resourceClass;
        }

        return $this->resourceInstance;
    }

    public function getEmptyViaResourceInstance()
    {
        if ($this->viaResourceClass) {
            return new $this->viaResourceClass;
        }
    }

    public function getViaPivotFieldsCollection()
    {
        if (! $this->viaPivotFields) {
            if ($relatedResource = $this->getViaResourceInstance()) {
                $relationAttribute = $this->request->query('viaRelationship');
                $relationField = collect($relatedResource->fieldsForDetail($this->request))
                    ->first(static function ($field) use ($relationAttribute) {
                        return $field->getAttribute() === $relationAttribute;
                    });
                if ($relationField) {
                    $this->viaPivotFields = new FieldsCollection($relationField->resolvePivotFields());
                }
            }
        }

        return $this->viaPivotFields;
    }

    public function getViaResourceInstance()
    {
        if (! $this->viaResourceInstance) {
            if ($viaModel = $this->getViaResourceModel()) {
                $viaResourceClass = $this->viaResourceClass;

                $this->viaResourceInstance = $viaResourceClass::fromModel($viaModel);
            }
        }

        return $this->viaResourceInstance;
    }

    private function getViaResourceModel()
    {
        if (! $this->viaModelInstance) {
            $viaFields = $this->viaFields();

            $viaRelatedField = $viaFields->allFields()->first(function ($field) {
                return $field->getAttribute() === $this->request->query('viaRelationship');
            });

            $viaIdField = $viaFields->idField($this->request);

            if ($viaRelatedField && $viaIdField) {
                $viaResourceClass = $this->viaResourceClass;

                $this->viaModelInstance = $viaResourceClass::findResource(
                    $this->request,
                    $this->request->query('viaResourceId'),
                    $viaResourceClass::resourceQuery(),
                    $viaIdField
                );
            }
        }

        return $this->viaModelInstance;
    }

    public function getViaResourceRelationQuery($options)
    {
        if ($viaResource = $this->getViaResourceInstance()) {
            $viaFields = $this->viaFields();

            $viaRelatedField = $viaFields->allFields()->first(function ($field) {
                return $field instanceof RelationshipField
                    && $field->getAttribute() === $this->request->query('viaRelationship')
                    && $field->getResourceClass() === $this->resourceClass;
            });

            if ($viaRelatedField) {
                return $viaRelatedField->resolveResourceQuery(
                    $this->request,
                    $viaResource,
                    $viaFields->idField($this->request),
                    $options
                );
            }
        }
    }
}
