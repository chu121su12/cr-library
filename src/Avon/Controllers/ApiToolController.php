<?php

namespace CR\Library\Avon\Controllers;

use CR\Library\Avon\Application;
use CR\Library\Helpers\Reply;
use Illuminate\Http\Request;

class ApiToolController
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function view(Request $request, $tool)
    {
        $toolClass = $this->app->getTool($tool);

        if (! $toolClass) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $toolInstance = new $toolClass;

        if (! $toolInstance->authorizedToSee($request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return $toolInstance->getViewData($request);
    }

    public function data(Request $request, $tool, $key)
    {
        $toolClass = $this->app->getTool($tool);

        if (! $toolClass) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $toolInstance = new $toolClass;

        if (! $toolInstance->authorizedToSee($request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return $toolInstance->getData($request, $key);
    }

    public function action(Request $request, $tool, $key)
    {
        $toolClass = $this->app->getTool($tool);

        if (! $toolClass) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $toolInstance = new $toolClass;

        if (! $toolInstance->authorizedToSee($request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return $toolInstance->runAction($request, $key);
    }

    public function badge(Request $request, $tool)
    {
        $toolClass = $this->app->getTool($tool);

        if (! $toolClass) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $toolInstance = new $toolClass;

        if (! $toolInstance->authorizedToSee($request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return [
            'badge' => $toolClass::badge($request),
        ];
    }
}
