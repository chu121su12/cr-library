<?php

namespace CR\Library\Avon\Controllers;

use Closure;
use CR\Library\Avon\Actions\DeleteResource;
use CR\Library\Avon\Fields;
use CR\Library\Avon\Fields\FieldsCollection;
use CR\Library\Avon\Fields\File;
use CR\Library\Avon\Fields\Image;
use CR\Library\Avon\Helpers;
use CR\Library\Helpers\Helpers as LibHelpers;
use CR\Library\Helpers\Reply;
use Exception;
use Illuminate\Contracts\Database\Query\Builder as QueryBuilderContract;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\Relations as EloquentRelations;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ApiResourceController
{
    protected $api;

    protected $isManyToManyRelation;

    public function __construct(ApiResourceRequest $api)
    {
        $this->api = $api;
    }

    public function search($resource)
    {
        return [
            'search' => $this->api->app->globallySearchableResources()
                ->map(static function ($page) use ($resource) {
                    if ($page['route'] === $resource) {
                        if ($page['query'] = $page['class']::resourceQuery()) {
                            return $page;
                        }
                    }
                })
                ->filter()
                ->map(function ($page) {
                    $resourceClass = $page['class'];
                    $hasModel = \class_exists($modelClass = $resourceClass::resourceModel());

                    if ($hasModel) {
                        $modelInstance = new $modelClass;

                        $baseModelQuery = $modelInstance->when(
                            \count($resourceClass::$with),
                            static function ($query) use ($resourceClass) {
                                return $query->with($resourceClass::$with);
                            }
                        );
                    }
                    else {
                        $baseModelQuery = $page['query'];
                    }

                    $baseModelQuery = $this->querySearch(
                        $resourceClass::indexQuery($this->api->request, $baseModelQuery) ?: $baseModelQuery,
                        $hasModel ? $modelInstance : null,
                        collect($resourceClass::$search),
                        $this->api->request->query('q')
                    );

                    return [
                        $resourceClass,
                        $baseModelQuery
                            ->when($this->api->idField(), static function ($query, $idField) {
                                return $query->orderBy($idField->getAttribute(), 'desc');
                            })
                            ->limit($resourceClass::$globalSearchResults ?: 3),
                    ];
                })
                ->map(function ($data) {
                    list($resourceClass, $query) = $data;

                    return $query->get()->map(function ($model) use ($resourceClass) {
                        return Helpers::generateResourcePreview(
                            $this->api->request,
                            $resourceClass::fromModel($model),
                            $fromSearch = false
                        );
                    });
                })
                ->flatten(1)
                ->values()->all(),
        ];
    }

    public function index($resource)
    {
        $resourceInstance = $this->api->getEmptyResourceInstance();

        if (! $resourceInstance || ! $resourceInstance->authorizedToViewAny($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        $resourceClass = $this->api->resourceClass;

        $resourcePerPages = $resourceClass::perPageOptions($this->api->handlingRelatedResource());
        $perPageOption = $this->api->request->query('perPage');
        $fields = $this->getFieldsForIndex();
        $data = $this->loadIndexModels(
            data_get($fields, '2'),
            (int) (\in_array($perPageOption, $resourcePerPages, false) ? $perPageOption : head($resourcePerPages)) ?: 5
        );

        $now = micro_time()->toJSON();
        $count = null;

        if ($resourceClass::$pagination === 'simple') {
            $count = false;
        }
        elseif ($data instanceof LengthAwarePaginator) {
            $count = $data->total();
        }
        elseif (! $data->hasMorePages()) {
            $count = ($data->currentPage() - 1) * $data->perPage() + $data->count();
        }

        return [
            'resourceName' => $resource,

            'resources' => $data
                ->map(function ($model) use ($fields, $resourceClass, $now) {
                    return $this->generateIndexData(
                        $fields,
                        $resourceClass::fromModel($model),
                        $now
                    );
                })
                ->values()
                ->all(),

            'count' => $count,

            'pager' => [
                'hasMore' => $data->hasMorePages(),
                'currentPage' => $data->currentPage(),
                'perPage' => $data->perPage(),
                'perPageOptions' => $data instanceof LengthAwarePaginator
                    ? [$data->perPage()]
                    : $resourceClass::perPageOptions($this->api->handlingRelatedResource()),
            ],
        ];
    }

    public function count($resource)
    {
        $resourceInstance = $this->api->getEmptyResourceInstance();

        if (! $resourceInstance || ! $resourceInstance->authorizedToViewAny($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        $resourceClass = $this->api->resourceClass;

        $unsortedQuery = $this->loadIndexModels(null, null, true);

        $countQuery = $resourceClass::countQuery($this->api->request, $unsortedQuery);

        $resourceCount = false;

        if ($countQuery !== false) {
            try {
                $resourceCount = $countQuery instanceof LengthAwarePaginator
                    ? $countQuery->total()
                    : $countQuery->count();
            }
            catch (Exception $e) {
                LibHelpers::reportIgnoredException('Unable to count resource.', $e);
            }
        }

        return [
            'count' => $resourceCount,
        ];
    }

    public function support($resource)
    {
        $resourceInstance = $this->api->getEmptyResourceInstance();

        if (! $resourceInstance || ! $resourceInstance->authorizedToViewAny($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        $now = micro_time()->toJSON();

        $resourceClass = $this->api->resourceClass;
        $preventIndexSelection = $resourceClass::$preventIndexSelection || ! $this->api->idField();

        return [
            'fields' => $this->generateIndexData(
                $this->getFieldsForIndex(),
                $resourceClass::fromModel(null),
                $now
            )['fields'],

            'filters' => $this->api->filters()
                ->filter->authorizedToSee($this->api->request)
                ->map->toJson()
                ->values()
                ->all(),

            'actions' => $this->api->actions()
                ->filter->authorizedToSee($this->api->request)
                ->filter(function ($action) use ($preventIndexSelection) {
                    return $action->resolveBatchIndexVisibility($this->api->request, $preventIndexSelection)
                        || $action->resolveInlineVisibility($this->api->request);
                })
                // resourceActions() // relation
                ->map->toJson()
                ->values()
                ->all(),

            'metrics' => $this->api->cards()
                ->reject->authorizedToSee($this->api->request)
                ->filter->resolveIndexVisibility($this->api->request)
                ->map->toJson()
                ->values()
                ->all(),

            'lenses' => $this->api->lenses()
                ->filter->authorizedToSee($this->api->request)
                ->map->toIndexListing()
                ->values()
                ->all(),

            'label' => $resourceClass::label(),
            'searchable' => \count($resourceClass::$search) > 0,
            'tableStyle' => $resourceClass::$tableStyle,
            'showColumnBorders' => (bool) $resourceClass::$showColumnBorders,
            'debounce' => (float) $resourceClass::$debounce,
            'pollingInterval' => (float) ($resourceClass::$polling ? $resourceClass::$pollingInterval : 0),
            'preventIndexSelection' => (bool) $preventIndexSelection,
            'showNumberings' => (bool) $resourceClass::$showNumberings,
            'showPollingToggle' => (bool) $resourceClass::$showPollingToggle,
            'showRefreshToggle' => (bool) $resourceClass::$showRefreshToggle,

            'authorizedToViewAny' => (bool) $resourceInstance->authorizedToViewAny($this->api->request),
            'authorizedToCreate' => (bool) $resourceInstance->authorizedToCreate($this->api->request),
        ];
    }

    public function badge($request)
    {
        $resourceInstance = $this->api->getEmptyResourceInstance();

        if (! $resourceInstance || ! $resourceInstance->authorizedToViewAny($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        $resourceClass = $this->api->resourceClass;

        return [
            'badge' => $resourceClass::badge($request),
        ];
    }

    public function action($resource, $action)
    {
        $activeAction = $this->api->actions()->first(static function ($a) use ($action) {
            return $a->actionKey() === $action;
        });

        if (! $activeAction) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        return $this->runAction($activeAction);
    }

    private function runAction($action, $query = null, array $options = [])
    {
        if (! isset($query)) {
            $query = $this->actionQuery(
                $action,
                \json_decode($this->api->request->post('_selectAll'), true),
                collect(\json_decode($this->api->request->post('_resources'), true))
            );
        }

        if ($reply = $action->execute($this->api->request, $query, $options)) {
            return $reply;
        }

        return (new Reply)->sessionMessage('Aksi selesai dijalankan.')->withReplaceSelf();
    }

    public function detail($resource, $resourceId)
    {
        if (! ($model = $this->loadSingleModel($resourceId))) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $now = micro_time()->toJSON();

        $resourceClass = $this->api->resourceClass;
        $resourceInstance = $resourceClass::fromModel($model);
        if (! $resourceInstance->authorizedToView($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return [
            'label' => $resourceClass::singularLabel(),
            'resource' => $this->generateDetailData($resourceInstance, $now),
            'resourceName' => $resource,

            'actions' => $this->api->actions()
                ->filter->authorizedToSee($this->api->request)
                ->filter->resolveDetailVisibility($this->api->request)
                ->map->toJson()
                ->values()
                ->all(),

            'metrics' => $this->api->cards()
                ->reject->authorizedToSee($this->api->request)
                ->filter->resolveDetailVisibility($this->api->request)
                ->map->toJson()
                ->values()
                ->all(),

            'pollingInterval' => (float) ($resourceClass::$polling ? $resourceClass::$pollingInterval : 0),
            'showPollingToggle' => (bool) $resourceClass::$showPollingToggle,
            'showRefreshToggle' => (bool) $resourceClass::$showRefreshToggle,

            'authorizedToViewAny' => (bool) $resourceInstance->authorizedToViewAny($this->api->request),
        ];
    }

    public function edit($resource, $resourceId)
    {
        if (! ($model = $this->loadSingleModel($resourceId))) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $now = micro_time()->toJSON();

        $resourceClass = $this->api->resourceClass;
        $resourceInstance = $resourceClass::fromModel($model);
        if (! $resourceInstance->authorizedToUpdate($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return [
            'label' => $resourceClass::singularLabel(),
            'resource' => $this->generateFormData(false, $resourceClass::fromModel($model), $now),
            'resourceName' => $resource,

            'preventFormAbandonment' => (bool) $resourceClass::$preventFormAbandonment,
            'authorizedToViewAny' => (bool) $resourceInstance->authorizedToViewAny($this->api->request),
        ];
    }

    public function replicate($resource, $resourceId)
    {
        if (! ($model = $this->loadSingleModel($resourceId))) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $now = micro_time()->toJSON();

        $resourceClass = $this->api->resourceClass;
        $resourceInstance = $resourceClass::fromModel($model);
        if (! $resourceInstance->authorizedToReplicate($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return [
            'label' => $resourceClass::singularLabel(),
            'resource' => $this->generateFormData(true, $resourceClass::fromModel($model), $now),
            'resourceName' => $resource,

            'preventFormAbandonment' => (bool) $resourceClass::$preventFormAbandonment,
            'authorizedToViewAny' => (bool) $resourceInstance->authorizedToViewAny($this->api->request),
        ];
    }

    public function update($resource, $resourceId)
    {
        $resourceInstance = $this->api->getEmptyResourceInstance();

        if (! $resourceInstance || ! $resourceInstance->authorizedToViewAny($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        if (! ($model = $this->loadSingleModel($resourceId))) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $now = micro_time()->toJSON();

        $resourceClass = $this->api->resourceClass;
        $resourceInstance = $resourceClass::fromModel($model);
        if (! $resourceInstance->authorizedToUpdate($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        $updateFields = (new FieldsCollection(
            $resourceInstance->fieldsForUpdate($this->api->request)
        ))->updateFields($this->api->request);

        $updateFields = $updateFields->map->prepareDependants($this->api->request, $updateFields);

        $validated = Validator::make(
            $this->api->request->all(),
            $updateFields->mapWithKeys(function ($field) {
                return $field->getUpdateValidationRules($this->api->request);
            })->all()
        )->after(function ($validator) use ($resourceInstance) {
            $resourceInstance::afterUpdateValidationCallback($this->api->request, $validator);
        })->validate();

        $callbacks = $updateFields
            // ->each->resolve($model)
            ->map->fillModel($this->api->request, $model)
            ->filter();

        if ($callbacks->isEmpty() && ! $model->isDirty()) {
            return (new Reply)->message('Tidak ada data yang diubah.');
        }

        if ($return = $resourceClass::beforeUpdate($this->api->request, $model)) {
            return $return;
        }

        // ActionModel::forUpdate($this->api->user(), collect([$model]))->each->save();

        $model->save();

        $callbacks->each(static function ($callback) {
            if ($callback instanceof Closure) {
                $callback();
            }
        });

        LibHelpers::safeDispatchAfterResponse(function () use ($resourceClass, $model) {
            $resourceClass::afterUpdate($this->api->request, $model);
        });

        return (new Reply)
            ->sessionMessage($resourceClass::label() . ' berhasil diubah.')
            // ->redirect(
            //     $resourceClass::redirectAfterUpdate($this->api, $resourceData)
            //         ?: $this->redirectUrl()
            // )
        ;
    }

    public function delete($resource)
    {
        $resourceClass = $this->api->resourceClass;

        return $this->runAction(new DeleteResource, null, [
            'beforeCallback' => function ($model) use ($resourceClass) {
                return $resourceClass::beforeDelete($this->api->request, $model);
            },
            'afterCallback' => function ($model) use ($resourceClass) {
                LibHelpers::safeDispatchAfterResponse(function () use ($resourceClass, $model) {
                    $resourceClass::afterDelete($this->api->request, $model);
                });
            },
        ]);
    }

    public function deleteById($resource, $resourceId)
    {
        if (! ($model = $this->loadSingleModel($resourceId))) {
            return response('', 404);
        }

        $resourceClass = $this->api->resourceClass;

        if ($return = $resourceClass::beforeDelete($this->api->request, $model)) {
            return $return;
        }

        $result = $this->runAction(new DeleteResource, collect([$model]));

        LibHelpers::safeDispatchAfterResponse(function () use ($resourceClass, $model) {
            $resourceClass::afterDelete($this->api->request, $model);
        });

        return $result;
    }

    public function create($resource)
    {
        $resourceInstance = $this->api->getEmptyResourceInstance();

        if (! $resourceInstance || ! $resourceInstance->authorizedToViewAny($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        $now = micro_time()->toJSON();
        $resourceClass = $this->api->resourceClass;

        return [
            'label' => $resourceClass::singularLabel(),
            'resource' => $this->generateFormData(true, $resourceClass::fromModel(null), $now),
            'resourceName' => $resource,

            'preventFormAbandonment' => (bool) $resourceClass::$preventFormAbandonment,
            'authorizedToViewAny' => (bool) $resourceInstance->authorizedToViewAny($this->api->request),
        ];
    }

    public function store($resource)
    {
        $resourceInstance = $this->api->getEmptyResourceInstance();

        if (! $resourceInstance || ! $resourceInstance->authorizedToViewAny($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        $model = $this->api->getResourceModelInstance(true);
        $now = micro_time()->toJSON();

        $hasModel = false;
        if ($model) {
            $hasModel = true;
        }
        else {
            $model = (object) [];
        }

        $resourceClass = $this->api->resourceClass;
        if (! $resourceInstance->authorizedToCreate($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        $storeFields = (new FieldsCollection(
            $resourceInstance->fieldsForCreate($this->api->request)
        ))->storeFields($this->api->request);

        $storeFields = $storeFields->map->prepareDependants($this->api->request, $storeFields);

        $validated = Validator::make(
            $this->api->request->all(),
            $storeFields->mapWithKeys(function ($field) {
                return $field->getCreationValidationRules($this->api->request);
            })->all()
        )->after(function ($validator) use ($resourceInstance) {
            $resourceInstance::afterCreationValidationCallback($this->api->request, $validator);
        })->validate();

        $callbacks = $storeFields
            // ->each->resolve($model)
            ->map->{$hasModel ? 'fillModel' : 'fillForAction'}($this->api->request, $model)
            ->filter();

        if ($callbacks->isEmpty()) {
            if (($hasModel && ! $model->isDirty()) || (! $hasModel && \count((array) $model) === 0)) {
                return (new Reply)->message('Tidak ada data untuk disimpan.');
            }
        }

        if ($return = $resourceClass::beforeCreate($this->api->request, $model)) {
            return $return;
        }

        $resourceData = $hasModel
            ? $resourceClass::fromModel(tap($model)->save())
            : $resourceClass::fromModel($model);

        // ActionModel::forCreate($this->api->user(), collect([$model]))->each->save();

        $callbacks->each(static function ($callback) {
            if ($callback instanceof Closure) {
                $callback();
            }
        });

        LibHelpers::safeDispatchAfterResponse(function () use ($resourceClass, $model) {
            $resourceClass::afterCreate($this->api->request, $model);
        });

        if (! $hasModel) {
            return (new Reply)
                ->sessionMessage($resourceClass::label() . ' baru telah dikirim.');
        }

        return (new Reply)
            ->sessionMessage($resourceClass::label() . ' baru telah dibuat.')
            // ->redirect(
            //     $resourceClass::redirectAfterCreate($this->api, $resourceData)
            //         ?: $this->redirectUrl()
            // )
        ;
    }

    public function fileDownload($resource, $resourceId, $fieldName)
    {
        if (! ($model = $this->loadSingleModel($resourceId))) {
            return response('', 404);
        }

        $resourceClass = $this->api->resourceClass;
        $resourceInstance = $resourceClass::fromModel($model);
        if (! $resourceInstance->authorizedToView($this->api->request)) {
            return response('', 403);
        }

        $fileField = $this->api->fields()
            ->detailFields($this->api->request)
            ->first(static function ($field) use ($fieldName) {
                return ($field instanceof File) && $field->getAttribute() === $fieldName;
            });

        if (! $fileField) {
            return response('', 404);
        }

        $fileField->resolveToValue($resourceInstance);

        return $fileField->downloadFile($this->api->request, $model);
    }

    public function fileDeleteApi($resource, $resourceId, $fieldName)
    {
        if (! ($model = $this->loadSingleModel($resourceId))) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $resourceClass = $this->api->resourceClass;
        $resourceInstance = $resourceClass::fromModel($model);
        if (! $resourceInstance->authorizedToUpdate($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        $fileField = $this->api->fields()
            ->editFields($this->api->request)
            ->first(static function ($field) use ($fieldName) {
                return ($field instanceof File) && $field->getAttribute() === $fieldName;
            });

        if (! $fileField) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $fileField->resolveToValue($resourceInstance);
        $fileField->deleteFile($this->api->request, $model);
        $model->save();

        return (new Reply)
            ->message('File dihapus.');
    }

    public function filePreview($resource, $resourceId, $fieldName)
    {
        if (! ($model = $this->loadSingleModel($resourceId))) {
            return response('', 404);
        }

        $resourceClass = $this->api->resourceClass;
        $resourceInstance = $resourceClass::fromModel($model);
        if (! $resourceInstance->authorizedToView($this->api->request)) {
            return response('', 403);
        }

        $imageField = $this->api->fields()
            ->detailFields($this->api->request)
            ->first(static function ($field) use ($fieldName) {
                return ($field instanceof Image) && $field->getAttribute() === $fieldName;
            });

        if (! $imageField) {
            return response('', 404);
        }

        $imageField->resolveToValue($resourceInstance);

        return $imageField->downloadPreview($this->api->request, $model);
    }

    public function fileThumbnail($resource, $resourceId, $fieldName)
    {
        if (! ($model = $this->loadSingleModel($resourceId))) {
            return response('', 404);
        }

        $resourceClass = $this->api->resourceClass;
        $resourceInstance = $resourceClass::fromModel($model);
        if (! $resourceInstance->authorizedToView($this->api->request)) {
            return response('', 403);
        }

        $imageField = $this->api->fields()
            ->detailFields($this->api->request)
            ->first(static function ($field) use ($fieldName) {
                return ($field instanceof Image) && $field->getAttribute() === $fieldName;
            });

        if (! $imageField) {
            return response('', 404);
        }

        $imageField->resolveToValue($resourceInstance);

        return $imageField->downloadThumbnail($this->api->request, $model);
    }

    public function relatable($resource)
    {
        $resourceInstance = $this->api->getEmptyResourceInstance();

        if (! $resourceInstance || ! $resourceInstance->authorizedToViewAny($this->api->request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        $resourceClass = $this->api->resourceClass;
        $resourceNickName = Str::afterLast($resourceClass, '\\');
        $methodName = \method_exists($resourceClass, "relatable{$resourceNickName}")
            ? "relatable{$resourceNickName}"
            : 'relatableQuery';

        $query = $this->querySearch(
            $resourceClass::resourceQuery(),
            $model = $this->api->getResourceModelInstance(),
            $searchColumns = collect($resourceClass::$search),
            $this->api->request->query('search')
        );

        return [
            'searchable' => \count($resourceClass::$search) > 0,
            'relatedOptions' => $resourceClass::{$methodName}($this->api->request, $query)
                ->when(isset($resourceClass::$relatableSearchResults), static function ($query) use ($resourceClass) {
                    return $query->limit($resourceClass::$relatableSearchResults);
                })
                ->get()
                ->map(function ($model) use ($resourceClass) {
                    return Helpers::generateResourcePreview(
                        $this->api->request,
                        $resourceClass::fromModel($model)
                    );
                })
                ->sortBy('title')
                ->values()
                ->all(),
        ];
    }

    private function actionQuery($action, $selectMode, $requestSelections)
    {
        $selectMode = (int) $selectMode;
        if ($selectMode === 1) {
            if ($requestSelections->count() !== 1) {
                throw new Exception(\sprintf('Action with select mode one got not one selection. (%s)', \get_class($action)));
            }

            $selectMode = 'one';
        }
        else {
            switch ($actionSelectMode = (string) $action->getStandalone()) {
                case 'all':
                case 'matching':
                case 'none':
                    $selectMode = $actionSelectMode;

                    break;

                default:
                    switch ((string) $selectMode) {
                        case '0': $selectMode = 'selections';

                            break;

                        case '2': $selectMode = 'matching';

                            break;

                        case '3': $selectMode = 'all';

                            break;

                        default: throw new Exception(\sprintf('Unknown select mode. (%s)', \get_class($action)));
                    }
            }
        }

        $attribute = optional($this->api->idField())->getAttribute();
        $selections = $requestSelections->pluck('0')->all();
        $query = $this->loadIndexModels();
        $selectMode = (string) $selectMode;

        if ($query instanceof LengthAwarePaginator) {
            $data = $query->getCollection();

            switch (true) {
                case $selectMode === 'all': return $data;
                case $selectMode === 'matching' && ! \count($selections): return $data;
                case $selectMode === 'none': return collect();
                default: return $data->filter(static function ($item) use ($attribute, $selections) {
                    return $attribute !== null && \in_array((string) data_get($item, $attribute), $selections, true);
                });
            }
        }

        switch (true) {
            case $selectMode === 'all': return $this->newIndexQuery();
            case $selectMode === 'matching' && ! \count($selections): return $query;
            case $selectMode === 'none': return $this->newIndexQuery()->whereRaw('0 = 1');
            default: return $attribute === null
                ? $query->whereRaw('0 = 1')
                : $query->whereIn($attribute, $selections);
        }
    }

    // private function resourceActions()
    // {
    //     // TODO: ?
    //     if ($this->relationField) {
    //         return $this->resourceActions = $this->resourceActions()->merge($this->relationField->resolveActions())->values();
    //     }

    //     return $this->resourceActions = $this->resourceActions()->values();
    // }

    private function getFieldsForIndex()
    {
        $idField = $this->api->idField();
        $indexFields = $this->api->fields()->indexFields($this->api->request);

        if ($this->api->handlingRelatedResource()) {
            $viaClass = $this->api->viaResourceClass;
            $indexFields = $indexFields->reject(static function ($field) use ($viaClass) {
                return $field instanceof Fields\BelongsTo && $field->getResourceClass() === $viaClass;
            });
        }

        return [$idField, $indexFields, collect([$idField])->merge($indexFields)->values()];
    }

    private function generateIndexData($fields, $resource, $now)
    {
        list($idField, $indexFields) = $fields;

        if ($idField && $indexFields->search($idField, true) === false) {
            $idField->toDisplayJson($this->api->request, $resource);
        }

        $displayFields = $indexFields
            ->map->prepareDependants($this->api->request, $indexFields)
            ->map->toDisplayJson($this->api->request, $resource);

        $json = [];

        if ($this->api->handlingRelatedResource() && $this->isManyToManyRelation()) {
            if ($relatedResource = $this->api->getViaResourceInstance()) {
                $hasEditField = false;

                if ($pivotFieldsCollection = $this->api->getViaPivotFieldsCollection()) {
                    $hasEditField = $pivotFieldsCollection->editFields($this->api->request)->count() > 0;

                    $pivotFields = $pivotFieldsCollection
                        ->indexFields($this->api->request);

                    $pivotFields = $pivotFields
                        ->map->prepareDependants($this->api->request, $pivotFields)
                        ->map->toDisplayJson($this->api->request, $resource->pivot);

                    $displayFields->push(...$pivotFields);
                }

                $json['authorizedToView'] = (bool) $relatedResource->authorizedToView($this->api->request);
                $json['authorizedToUpdateAttached'] = $hasEditField && $relatedResource->authorizedToAttach($this->api, $relatedResource->resource());
                $json['authorizedToDetach'] = (bool) $relatedResource->authorizedToDetach($this->api, $relatedResource->resource());
            }
        }
        else {
            if ($idField) {
                $json['authorizedToView'] = (bool) $resource->authorizedToView($this->api->request);
                $json['authorizedToUpdate'] = (bool) $resource->authorizedToUpdate($this->api->request);
                $json['authorizedToDelete'] = (bool) $resource->authorizedToDelete($this->api->request);
                $json['authorizedToRestore'] = (bool) $resource->authorizedToRestore($this->api->request);
                $json['authorizedToForceDelete'] = (bool) $resource->authorizedToForceDelete($this->api->request);

                $json['authorizedToReplicate'] = (bool) $resource->authorizedToReplicate($this->api->request) && $resource->authorizedToCreate($this->api->request);

                // if (($model = $resource->resource()) instanceof Impersonateable) {
                //     $json['authorizedToImpersonate'] = $model;
                // }
            }
        }

        // do not inline! this needs to be separate to resove potential id field
        return \array_merge([
            'id' => optional($idField)->value,
            'title' => $resource->title(),
            'subtitle' => $resource->subtitle(),
            'retrieved_at' => $now,
            'fields' => $displayFields->values()->all(),
        ], $json);
    }

    private function generateDetailData($resource, $now)
    {
        $idField = $this->api->idField();
        $detailFields = $this->api->fields()->detailFields($this->api->request);

        if ($idField && $detailFields->search($idField, true) === false) {
            $idField->toDisplayJson($this->api->request, $resource);
        }

        $fields = $detailFields
            ->map->prepareDependants($this->api->request, $detailFields)
            ->map->toDisplayJson($this->api->request, $resource)
            ->merge($pivotFields = []);

        $title = $resource->title() ?: 'Detail';

        return [
            'id' => optional($idField)->value,
            'title' => $title,
            'subtitle' => $resource->subtitle(),
            'retrieved_at' => $now,
            'fields' => $fields->all(),

            'panels' => $this->api->fields()
                ->defaultPanelLabel($title)
                ->allPanels()
                ->map->toJson()
                ->all(),

            'authorizedToUpdate' => (bool) ($idField && $resource->authorizedToUpdate($this->api->request)),
            'authorizedToDelete' => (bool) ($idField && $resource->authorizedToDelete($this->api->request)),
        ];
    }

    private function generateFormData($creating, $resource, $now)
    {
        $idField = $this->api->idField();

        $formFields = (new FieldsCollection(
            $this->api->getEmptyResourceInstance()->{$creating ? 'fieldsForCreate' : 'fieldsForUpdate'}($this->api->request)
        ))->{$creating ? 'creationFields' : 'editFields'}($this->api->request);

        if ($idField && $formFields->search($idField, true) === false) {
            $idField->toDisplayJson($this->api->request, $resource);
        }

        $fields = $formFields
            ->map->prepareDependants($this->api->request, $formFields)
            ->map->toDisplayJson($this->api->request, $resource)
            ->merge($pivotFields = []);

        $title = $resource->title() ?: ($creating ? 'Create' : 'Edit');

        $panels = $this->api->fields()
            ->defaultPanelLabel($title)
            ->allPanels()
            ->map->toJson();

        return $creating ? [
            'title' => $title,
            'subtitle' => $resource->subtitle(),
            'retrieved_at' => $now,
            'fields' => $fields->all(),
            'panels' => $panels->all(),
        ] : [
            'id' => optional($idField)->value,
            'title' => $title,
            'subtitle' => $resource->subtitle(),
            'retrieved_at' => $now,
            'fields' => $fields->all(),
            'panels' => $panels->all(),

            'authorizedToView' => (bool) ($idField && $resource->authorizedToView($this->api->request)),
            'authorizedToDelete' => (bool) ($idField && $resource->authorizedToDelete($this->api->request)),
        ];
    }

    protected function loadSingleModel($resourceId)
    {
        if ($idField = $this->api->idField()) {
            $resourceClass = $this->api->resourceClass;

            return $resourceClass::findResource(
                $this->api->request,
                $resourceId,
                $this->newBaseQuery((object) [
                    'index' => false,
                    'id' => $resourceId,
                ]),
                $idField
            );
        }

        throw new Exception('Resource is missing ID field');
    }

    protected function loadIndexModels($fields = null, $perPage = null, $getCountQuery = false)
    {
        $query = $this->newIndexQuery();

        if (! ($query instanceof QueryBuilderContract)) {
            if ($query instanceof LengthAwarePaginator) {
                return $query;
            }

            return with(Collection::wrap($query), static function ($collected) {
                return new LengthAwarePaginator(
                    $collected->all(),
                    $collected->count(),
                    $collected->count() ?: 1
                );
            });
        }

        $resourceClass = $this->api->resourceClass;

        $queryFilters = $this->api->request->query('filters');
        $filterValues = \is_array($queryFilters)
            ? $queryFilters
            : (\json_decode($queryFilters, true) ?: []);
        $filteredQuery = $this->queryFilter(
            $this->querySearch(
                $query,
                $model = $this->api->getResourceModelInstance(),
                $searchColumns = collect($resourceClass::$search),
                $this->api->request->query('search')
            ),
            $this->api->filters()->reject->getPreFilter(),
            $filterValues
        );

        $baseQuery = $getCountQuery ? $filteredQuery : $this->querySort(
            $filteredQuery,
            $this->api->request->query('order'),
            $this->api->request->query('direction')
        );

        if (! isset($perPage)) {
            return $baseQuery;
        }

        if (! $getCountQuery && $fields && ($resourceClass::$indexSelects === null || $resourceClass::$indexSelects)) {
            list($computed, $nonComputed) = $fields->whereNotNull()->partition(function ($field) {
                return $field->computedResolver;
            });

            if ($resourceClass::$indexSelects === null) {
                if ($computed->isEmpty() && ! $nonComputed->isEmpty()) {
                    $baseQuery = $baseQuery->select(
                        $nonComputed->map->getAttribute()
                            ->unique()
                            ->values()
                            ->all()
                    );
                }
            }
            else {
                $baseQuery = $baseQuery->select(
                    Collection::wrap($resourceClass::$indexSelects)
                        ->filter(function ($attribute) {
                            return \is_string($attribute);
                        })
                        ->merge($nonComputed->map->getAttribute())
                        ->unique()
                        ->values()
                        ->all()
                );
            }
        }

        return $baseQuery->simplePaginate($perPage);
    }

    private function querySearch($query, $model, $searchColumns, $searchString)
    {
        return Helpers::resourceSearch($query, $model, $searchColumns, $searchString);
    }

    private function queryFilter($query, $filters, $requestFilters)
    {
        return $filters->reduce(function ($query, $filter) use ($requestFilters) {
            return $filter->filter($requestFilters, $query, $this->api->request);
        }, $query);
    }

    private function querySort($query, $orderBy, $direction)
    {
        $resourceClass = $this->api->resourceClass;

        if ($orderBy) {
            $sortableField = $this->api->fields()
                ->indexFields($this->api->request)
                ->first(static function ($field) use ($orderBy) {
                    return $field->isSortable()
                        && $field->getAttribute() === $orderBy;
                });

            if (! $sortableField) {
                return $query;
            }

            switch ($direction = \mb_strtolower($direction)) {
                case 'asc':
                case 'desc':
                    $model = $this->api->getResourceModelInstance();
                    $orderByColumn = $model && $model instanceof EloquentModel
                        ? $model->qualifyColumn($orderBy)
                        : $orderBy;

                    return $query->orderBy($orderByColumn, $direction);

                default:
                    return $query;
            }
        }

        if (! $resourceClass::$defaultSort) {
            return $query;
        }

        if ($this->api->handlingRelatedResource()) {
            if (! isset($query)) {
                // noop
            }
            elseif ($query instanceof EloquentRelations\HasOneOrMany) {
                $key = $query->getRelated()->getQualifiedKeyName();
            }
            elseif ($query instanceof EloquentRelations\BelongsTo) {
                $key = $query->getQualifiedForeignKeyName();
            }
            elseif ($query instanceof EloquentRelations\BelongsToMany) {
                $key = $query->getRelated()->qualifyColumn(
                    $query->getRelatedKeyName()
                );
            }
            elseif ($query instanceof QueryBuilderContract) {
                // none
            }
            else {
                throw new Exception(\sprintf('Cannot order by relation class. (%s)', \get_class($query)));
            }
        }
        elseif (($model = $this->api->getResourceModelInstance()) && $model instanceof EloquentModel) {
            $key = $model->getQualifiedKeyName();
        }

        if (! isset($key)) {
            $idField = $this->api->idField();
            if ($idField && $idField->isSortable()) {
                $key = $idField->getAttribute();
            }
        }

        return isset($key) ? $query->orderBy($key, 'desc') : $query;
    }

    private function newIndexQuery()
    {
        $resourceClass = $this->api->resourceClass;

        $queryFilters = $this->api->request->query('filters');
        $filterValues = \is_array($queryFilters)
            ? $queryFilters
            : (\json_decode($queryFilters, true) ?: []);

        return $resourceClass::indexQuery(
            $this->api->request,
            $this->newBaseQuery((object) [
                'index' => true,
                'search' => $this->api->request->query('search'),
                'filters' => $this->api->filters()
                    ->mapWithKeys(function ($filter) use ($filterValues) {
                        if ($pre = $filter->getPreFilter()) {
                            return [$pre === true ? $filter->key() : $pre => [
                                'filter' => $filter,
                                'value' => $filter->value($this->api->request, $filterValues),
                            ]];
                        }

                        return [];
                    })
                    ->filter(),
            ])
        );
    }

    private function newBaseQuery($options = null)
    {
        $resourceClass = $this->api->resourceClass;

        if (! $this->api->handlingRelatedResource()) {
            return $resourceClass::resourceQuery($options);
        }

        $parentModelRelationQuery = $this->api->getViaResourceRelationQuery($options);

        $this->isManyToManyRelation = $parentModelRelationQuery instanceof EloquentRelations\BelongsToMany; // || MorphToMany

        if ($this->isManyToManyRelation) {
            if ($pivotFieldsCollection = $this->api->getViaPivotFieldsCollection()) {
                $pivotAttributes = $pivotFieldsCollection->indexFields($this->api->request)
                    ->map->getAttribute()
                    ->whereNotNull();

                return $parentModelRelationQuery->withPivot($pivotAttributes->all());
            }
        }

        return $parentModelRelationQuery;
    }

    private function isManyToManyRelation()
    {
        return (bool) $this->isManyToManyRelation;
    }
}
