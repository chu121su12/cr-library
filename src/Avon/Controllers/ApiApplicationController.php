<?php

namespace CR\Library\Avon\Controllers;

use CR\Library\Avon\Application;
use CR\Library\Helpers\Encryption;
use CR\Library\Helpers\Helpers;
use CR\Library\Helpers\Uncategorized;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class ApiApplicationController
{
    public function appData(Application $app)
    {
        return [
            'appData' => $app->getAppData(),
        ];
    }

    public function error(Request $request)
    {
        Log::debug('client-error', $request->json()->all());

        return response('', 201);
    }

    public function menu(Application $app, Request $request)
    {
        return [
            'mainMenu' => $app->getAvailableMenu($request),
        ];
    }

    public function publicKey()
    {
        return [
            'key' => Encryption::rsaLoadCached()['pk'],
        ];
    }

    public function publicKeySignature()
    {
        return [
            'signature' => Encryption::rsaLoadCached()['spk'],
        ];
    }

    public function requestCaptcha(Request $request)
    {
        $key = $request->get('key');
        $questionKey = "avon:captcha:question:{$key}";
        $questionPair = Arr::wrap(Cache::get($questionKey));

        if (\count($questionPair) !== 2) {
            $questionPair = Uncategorized::generateBasicImageCaptchaChallange();
            Cache::set($questionKey, $questionPair);
        }

        $match = (string) Cache::get("avon:captcha:key:{$key}") === '1';
        if ($match) {
            Cache::forget($questionKey);
        }

        return [
            'question' => $match ? '' : data_get($questionPair, '0'),
            'solved' => $match,
        ];
    }

    public function solveCaptcha(Request $request)
    {
        $key = $request->get('key');
        $questionKey = "avon:captcha:question:{$key}";
        $questionPair = Arr::wrap(Cache::get($questionKey));
        $match = (string) data_get($questionPair, '1') === (string) $request->get('solution');

        Cache::set("avon:captcha:key:{$key}", $match ? '1' : '0');
        if ($match) {
            Cache::forget($questionKey);
        }

        return [
            'question' => $match ? '' : data_get($questionPair, '0'),
            'solved' => $match,
        ];
    }

    public function time()
    {
        return response('', 204, [
            'Cache-Control' => 'no-store',
            'X-Date' => micro_time()->toJSON(),
        ]);
    }

    public function user()
    {
        try {
            if ($user = auth()->user()) {
                return [
                    'userData' => [
                        'name' => \method_exists($user, 'getAccountName') ? $user->getAccountName() : optional($user)->name,
                        'id' => \method_exists($user, 'getAccountId') ? $user->getAccountId() : optional($user)->getKey(),
                        'type' => \method_exists($user, 'getAccountType') ? $user->getAccountType() : null,
                        'team' => \method_exists($user, 'getAccountTeam') ? $user->getAccountTeam() : null,
                    ],
                ];
            }
        }
        catch (Exception $e) {
            Helpers::reportIgnoredException('Error fetching user or user info.', $e);
        }

        return [
            'userData' => null,
        ];
    }
}
