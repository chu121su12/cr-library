<?php

namespace CR\Library\Avon\Controllers;

use CR\Library\Avon\Application;
use CR\Library\Avon\Helpers;
use CR\Library\Helpers\Helpers as LibHelpers;
use CR\Library\Helpers\Reply;
use CR\Library\Helpers\Uncategorized;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class LoginController
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function csrf()
    {
        return response('', 204);
    }

    public function loginView()
    {
        return response()->view(
            'cr-avon2::login',
            [
                '__avon' => $this->app->getViewData(),
                '__avon_app' => $this->app,
                '__avon_component' => 'login',
            ],
            200,
            [
                'permissions-policy' => 'interest-cohort=()',
                'x-content-type-options' => 'nosniff',
                'x-frame-options' => 'DENY',
                'x-xss-protection' => '1; mode=block',
                'x-powered-by' => '',
            ]
        );
    }

    public function loginApi(Request $request)
    {
        $signature = (array) $request->json('mark');
        Log::info(\sprintf(
            'account: signature 0:%s 1:%s 2:%s',
            data_get($signature, 'thumbmark.0', '-'),
            data_get($signature, 'fingerprint2.0', '-'),
            data_get($signature, 'fp2b.0', '-')
        ));
        Log::debug('account: signature details', [
            '#0' => (array) data_get($signature, 'thumbmark.1', []),
            '#1' => (array) data_get($signature, 'fingerprint2.1', []),
        ]);

        $username = (string) $request->json('username');
        if (\trim($username) === '') {
            return (new Reply)->message('ID kosong.');
        }

        $remaining = $this->app->getRateLimitHalting('avon:rl:login:ips::' . Uncategorized::generateRateLimitIpKey($request));
        if ($remaining) {
            return (new Reply)->message(\sprintf(
                'Terlalu banyak akses. Silakan coba lagi dalam %s menit',
                (int) \ceil($remaining / 60)
            ));
        }

        $remaining = $this->app->getRateLimitCaptcha('avon:rl:login:user-like::' . Uncategorized::generateRateLimitUserLikeKey($username));
        if ($remaining) {
            $captchaBaseKey = (string) $request->json('captcha');

            if (! $request->has('captcha') || \trim($captchaBaseKey) === '') {
                return (new Reply)->payload([
                    'captcha' => Str::random(),
                ]);
            }

            $captchaKey = 'avon:captcha:key:' . $captchaBaseKey;

            if ((string) Cache::get($captchaKey) !== '1') {
                return (new Reply)->payload([
                    'captcha' => $captchaBaseKey,
                ]);
            }

            Cache::forget($captchaKey);
        }

        // ///

        Log::info('account: log in attempt');

        try {
            $user = null;
            $user = $this->app->login($request);

            if ($user instanceof Authenticatable) {
                auth()->login($user);

                Cache::put('avon:sig:login:user-id::' . $username, $signature, 60 * 5);

                Cache::put(Helpers::authUserLsKey($user)['base'], (int) now()->format('U'), 60 * (1 + config('session.lifetime')));

                Log::info('account: logged in');

                return (new Reply)->payload([
                    'loggedIn' => 1,
                ])->redirect($this->app->getBaseUrl('/'));
            }
        }
        catch (Exception $e) {
            LibHelpers::reportIgnoredException('Login callback exception', $e);
        }

        Log::info('account: log in - other');

        if ($user instanceof Responsable) {
            return $user;
        }

        if ($user !== null) {
            return $user;
        }

        if (! app()->isProduction()) {
            return (new Reply)->message('account: log in - failed');
        }

        return (new Reply)->message('ID/password salah.');
    }

    public function logoutApi(Request $request)
    {
        Helpers::logoutUser($request);

        Log::info('account: logged out - session');

        return (new Reply)->redirect($this->app->getBaseUrl('/login'));
    }
}
