<?php

namespace CR\Library\Avon\Controllers;

use CR\Library\Avon\Application;
use CR\Library\Avon\Metrics\InlineCard;
use CR\Library\Helpers\Reply;
use Illuminate\Http\Request;

class ApiDashboardController
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function metrics(Request $request, $dashboard)
    {
        $dashboardClass = $this->app->getDashboard($dashboard);

        if (! $dashboardClass) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $dashboardInstance = new $dashboardClass;

        if (! $dashboardInstance->authorizedToSee($request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return $dashboardInstance->getViewData($request);
    }

    public function metric(Request $request, $dashboard, $metric)
    {
        $dashboardClass = $this->app->getDashboard($dashboard);

        if (! $dashboardClass) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $dashboardInstance = new $dashboardClass;

        if (! $dashboardInstance->authorizedToSee($request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        $metricInstance = collect($dashboardInstance->cards($request))
            ->filter()
            ->first(static function ($card) use ($metric) {
                return $card instanceof InlineCard
                    ? $card->matchUriKey($metric)
                    : $card::uriKey() === $metric;
            });

        if (! $metricInstance) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        if (! $metricInstance->authorizedToSee($request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return $metricInstance->generateResult($request);
    }

    public function badge(Request $request, $dashboard)
    {
        $dashboardClass = $this->app->getDashboard($dashboard);

        if (! $dashboardClass) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $dashboardInstance = new $dashboardClass;

        if (! $dashboardInstance->authorizedToSee($request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return [
            'badge' => $dashboardClass::badge($request),
        ];
    }
}
