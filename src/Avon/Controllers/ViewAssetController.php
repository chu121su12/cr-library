<?php

namespace CR\Library\Avon\Controllers;

use Carbon\CarbonImmutable;
use CR\Library\Avon\Application;
use CR\Library\Avon\Concerns\ViewAssetProvider;
use CR\Library\Helpers\Helpers;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ViewAssetController
{
    public $app;

    public $request;

    private $debug;

    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;

        $this->request = $request;

        $this->debug = app()->hasDebugModeEnabled();
    }

    public function version()
    {
        return $this->app->getViewData()['appId'];
    }

    public function appScripts($id)
    {
        return $this->mergeInternalAssets(
            'js',
            [
                'asset/dayjs/dayjs.min.js',
                'asset/dayjs/locale/id.js',
                'asset/dayjs/plugin/customParseFormat.js',
                'asset/dayjs/plugin/duration.js',
                'asset/dayjs/plugin/relativeTime.js',
                'asset/dayjs/plugin/timezone.js',
                'asset/dayjs/plugin/utc.js',

                'asset/tabbable/index.umd.min.js',
                'asset/focus-trap/focus-trap.umd.min.js',

                'asset/flatpickr/flatpickr.min.js',
                'asset/flatpickr/l10n/id.min.js',

                'asset/vue-virtual-scroller/vue3-observe-visibility2.min.js',
                'asset/vue-virtual-scroller/vue3-resize.min.js',
                'asset/vue-virtual-scroller/vue3-virtual-scroller.min.js',

                'asset/autonumeric/autoNumeric.min.js',
                'asset/autosize/autosize.min.js',
                'asset/chartist/index.umd.min.js',
                'asset/chartist/plugins/chartist-plugin-tooltip-mod.min.js',
                'asset/codemirror/codemirror.min.js',
                'asset/fuse.js/fuse.min.js',
                'asset/inert/inert.min.js',
                'asset/localforage/localforage.min.js',
                'asset/marked/marked.min.js',
                'asset/mitt/mitt.umd.min.js',
                'asset/popperjs/popper.min.js',

                'asset/big.js/big.min.js',

                'asset/avon/init.min.js',
            ]
        );
    }

    public function appStyles($id)
    {
        return $this->mergeInternalAssets(
            'css',
            [
                'asset/vue-virtual-scroller/vue3-resize.min.css',
                'asset/vue-virtual-scroller/vue3-virtual-scroller.min.css',

                'asset/chartist/index.min.css',
                'asset/chartist/plugins/chartist-plugin-tooltip.min.css',
                'asset/codemirror/codemirror.min.css',
                'asset/flatpickr/flatpickr.min.css',
            ]
        );
    }

    public function script($id)
    {
        return $this->mergeInternalAssets(
            'js',
            [
                $this->debug
                    ? 'asset/avon/base.js'
                    : 'asset/avon/base.min.js',

                'asset/unocss/runtime/preset-attributify.global.js',
                'asset/unocss/runtime/preset-uno.global.js',
                'asset/unocss/runtime/preset-icons.global.js',

                $this->debug
                    ? 'asset/unocss/runtime/core.global.expanded.js'
                    : 'asset/unocss/runtime/core.global.min.js',

                $this->debug
                    ? 'asset/vue3/vue.global.js'
                    : 'asset/vue3/vue.global.prod.js',

                'asset/vueuse-shared/index.iife.min.js',
                'asset/vueuse-core/index.iife.min.js',

                'asset/axios/axios.min.js',
                'asset/crypto-js/crypto-js.min.js',
                'asset/jsencrypt/jsencrypt.min.js',

                'asset/fingerprint/fingerprint2.min.js',
                'asset/thumbmarkjs/thumbmark.umd.min.js',

                $this->debug
                    ? 'asset/primevue/core/core.js'
                    : 'asset/primevue/core/core.min.js',

                // 'asset/primevue/api/api.min.js',
                // 'asset/primevue/badge/badge.min.js',
                // 'asset/primevue/base/base.min.js',
                // 'asset/primevue/basecomponent/basecomponent.min.js',
                // 'asset/primevue/basedirective/basedirective.min.js',
                // 'asset/primevue/baseicon/baseicon.min.js',
                // 'asset/primevue/button/button.min.js',
                // 'asset/primevue/config/config.min.js',
                // 'asset/primevue/confirmationeventbus/confirmationeventbus.min.js',
                // 'asset/primevue/dialog/dialog.min.js',
                // 'asset/primevue/dropdown/dropdown.min.js',
                // 'asset/primevue/dynamicdialogeventbus/dynamicdialogeventbus.min.js',
                // 'asset/primevue/focustrap/focustrap.min.js',
                // 'asset/primevue/icons/icons.min.js',
                // 'asset/primevue/inputnumber/inputnumber.min.js',
                // 'asset/primevue/inputtext/inputtext.min.js',
                // 'asset/primevue/menu/menu.min.js',
                // 'asset/primevue/message/message.min.js',
                // 'asset/primevue/overlayeventbus/overlayeventbus.min.js',
                // 'asset/primevue/paginator/paginator.min.js',
                // 'asset/primevue/passthrough/passthrough.min.js',
                // 'asset/primevue/portal/portal.min.js',
                // 'asset/primevue/progressbar/progressbar.min.js',
                // 'asset/primevue/ripple/ripple.min.js',
                // 'asset/primevue/terminalservice/terminalservice.min.js',
                // 'asset/primevue/tieredmenu/tieredmenu.min.js',
                // 'asset/primevue/toasteventbus/toasteventbus.min.js',
                // 'asset/primevue/tooltip/tooltip.min.js',
                // 'asset/primevue/tree/tree.min.js',
                // 'asset/primevue/useconfirm/useconfirm.min.js',
                // 'asset/primevue/usedialog/usedialog.min.js',
                // 'asset/primevue/usestyle/usestyle.min.js',
                // 'asset/primevue/usetoast/usetoast.min.js',
                // 'asset/primevue/utils/utils.min.js',
                // 'asset/primevue/virtualscroller/virtualscroller.min.js',

                'asset/primevue/confirmationservice/confirmationservice.min.js',
                'asset/primevue/dialogservice/dialogservice.min.js',
                'asset/primevue/toastservice/toastservice.min.js',

                'asset/primevue/autocomplete/autocomplete.min.js',
                'asset/primevue/avatar/avatar.min.js',
                'asset/primevue/badgedirective/badgedirective.min.js',
                'asset/primevue/blockui/blockui.min.js',
                'asset/primevue/calendar/calendar.min.js',
                'asset/primevue/card/card.min.js',
                'asset/primevue/cascadeselect/cascadeselect.min.js',
                'asset/primevue/chart/chart.min.js',
                'asset/primevue/checkbox/checkbox.min.js',
                'asset/primevue/chip/chip.min.js',
                'asset/primevue/chips/chips.min.js',
                'asset/primevue/colorpicker/colorpicker.min.js',
                'asset/primevue/column/column.min.js',
                'asset/primevue/columngroup/columngroup.min.js',
                'asset/primevue/confirmdialog/confirmdialog.min.js',
                'asset/primevue/confirmpopup/confirmpopup.min.js',
                'asset/primevue/contextmenu/contextmenu.min.js',
                'asset/primevue/datatable/datatable.min.js',
                'asset/primevue/dataview/dataview.min.js',
                'asset/primevue/dataviewlayoutoptions/dataviewlayoutoptions.min.js',
                'asset/primevue/deferredcontent/deferredcontent.min.js',
                'asset/primevue/divider/divider.min.js',
                'asset/primevue/dynamicdialog/dynamicdialog.min.js',
                'asset/primevue/fileupload/fileupload.min.js',
                'asset/primevue/image/image.min.js',
                'asset/primevue/inlinemessage/inlinemessage.min.js',
                'asset/primevue/inputmask/inputmask.min.js',
                'asset/primevue/inputswitch/inputswitch.min.js',
                'asset/primevue/listbox/listbox.min.js',
                'asset/primevue/multiselect/multiselect.min.js',
                'asset/primevue/overlaypanel/overlaypanel.min.js',
                'asset/primevue/password/password.min.js',
                'asset/primevue/progressspinner/progressspinner.min.js',
                'asset/primevue/radiobutton/radiobutton.min.js',
                'asset/primevue/row/row.min.js',
                'asset/primevue/scrollpanel/scrollpanel.min.js',
                'asset/primevue/selectbutton/selectbutton.min.js',
                'asset/primevue/slider/slider.min.js',
                'asset/primevue/splitbutton/splitbutton.min.js',
                'asset/primevue/splitter/splitter.min.js',
                'asset/primevue/splitterpanel/splitterpanel.min.js',
                'asset/primevue/steps/steps.min.js',
                'asset/primevue/styleclass/styleclass.min.js',
                'asset/primevue/tag/tag.min.js',
                'asset/primevue/textarea/textarea.min.js',
                'asset/primevue/timeline/timeline.min.js',
                'asset/primevue/toast/toast.min.js',
                'asset/primevue/togglebutton/togglebutton.min.js',
                'asset/primevue/toolbar/toolbar.min.js',
                'asset/primevue/tristatecheckbox/tristatecheckbox.min.js',
            ]
        );
    }

    public function style($id)
    {
        return $this->mergeInternalAssets(
            'css',
            [
                $this->debug
                    ? 'asset/avon/base.css'
                    : 'asset/avon/base.min.css',

                function () {
                    return \preg_replace(
                        '~/fonts/~',
                        '/primeicons/fonts/',
                        self::fileGetContentsAsset('asset/primeicons/primeicons.min.css')
                    );
                },

                $this->debug
                    ? 'asset/primevue/resources/themes/theme.css'
                    : 'asset/primevue/resources/themes/theme.min.css',
            ]
        );
    }

    public function otherPath($id, $path)
    {
        $fullPath = __DIR__ . '/../res/asset/' . Helpers::ensureNoDirectoryClimb($path);

        if (! \file_exists($fullPath)) {
            abort(404);
        }

        return $this->sendContent(
            \pathinfo($path)['extension'],
            \file_get_contents($fullPath),
            \filemtime($fullPath),
            false
        );
    }

    public function vendorScripts($id)
    {
        return $this->mergeAssets(
            'js',
            \array_merge(
                [
                    static function () {
                        return \sprintf(
                            'var XLSX=(function(){%s;return XLSX})();',
                            self::fileGetContentsAsset('asset/xlsx/xlsx.full.min.js')
                        );
                    },
                    static function () {
                        return self::fileGetContentsAsset('asset/billboard.js/billboard.pkgd.min.js');
                    },
                    static function () {
                        return self::fileGetContentsAsset('asset/tagify/tagify.min.js');
                    },
                    static function () {
                        return self::fileGetContentsAsset('asset/tagify/tagify.polyfills.min.js');
                    },
                    static function () {
                        return self::fileGetContentsAsset('asset/jszip/jszip.min.js');
                    },
                ],
                // \array_map(function ($path) {
                //     return static function () use ($path) {
                //         return self::fileGetContentsAsset($path);
                //     };
                // }, [
                //     'asset/primevue/accordion/accordion.min.js',
                //     'asset/primevue/accordiontab/accordiontab.min.js',
                //     'asset/primevue/avatargroup/avatargroup.min.js',
                //     'asset/primevue/breadcrumb/breadcrumb.min.js',
                //     'asset/primevue/carousel/carousel.min.js',
                //     'asset/primevue/dock/dock.min.js',
                //     'asset/primevue/editor/editor.min.js',
                //     'asset/primevue/fieldset/fieldset.min.js',
                //     'asset/primevue/galleria/galleria.min.js',
                //     'asset/primevue/inplace/inplace.min.js',
                //     'asset/primevue/knob/knob.min.js',
                //     'asset/primevue/megamenu/megamenu.min.js',
                //     'asset/primevue/menubar/menubar.min.js',
                //     'asset/primevue/orderlist/orderlist.min.js',
                //     'asset/primevue/organizationchart/organizationchart.min.js',
                //     'asset/primevue/panel/panel.min.js',
                //     'asset/primevue/panelmenu/panelmenu.min.js',
                //     'asset/primevue/picklist/picklist.min.js',
                //     'asset/primevue/rating/rating.min.js',
                //     'asset/primevue/scrolltop/scrolltop.min.js',
                //     'asset/primevue/sidebar/sidebar.min.js',
                //     'asset/primevue/skeleton/skeleton.min.js',
                //     'asset/primevue/speeddial/speeddial.min.js',
                //     'asset/primevue/tabmenu/tabmenu.min.js',
                //     'asset/primevue/tabpanel/tabpanel.min.js',
                //     'asset/primevue/tabview/tabview.min.js',
                //     'asset/primevue/terminal/terminal.min.js',
                //     'asset/primevue/treeselect/treeselect.min.js',
                //     'asset/primevue/treetable/treetable.min.js',
                // ]),
                $this->app->getVendorScripts()
            )
        );
    }

    public function vendorStyles($id)
    {
        return $this->mergeAssets(
            'css',
            \array_merge(
                [
                    static function () {
                        return \sprintf(
                            '%s%s',
                            self::fileGetContentsAsset('asset/billboard.js/theme/graph.min.css'),
                            '.bb svg,.bb-tooltip-container{font-family:var(--_p_font-family,inherit)}!important'
                        );
                    },
                    static function () {
                        return self::fileGetContentsAsset('asset/tagify/tagify.min.css');
                    },
                ],
                $this->app->getVendorStyles()
            )
        );
    }

    public function icons($id, $collection)
    {
        return $this->mergeAndSendAssets(null, 'json', [
            ViewAssetProvider::icons($collection),
        ]);
    }

    protected function mergeAndSendAssets($pathResolver, $type, $list, $realtime = false)
    {
        $lastModifieds = [];

        $merged = Collection::wrap($list)
            ->map(static function ($path) use ($pathResolver, &$lastModifieds) {
                if (\is_string($path)) {
                    $fullPath = \realpath($resolvedPath = $pathResolver($path));
                    $lastModifieds[] = \filemtime($fullPath);

                    try {
                        return \file_get_contents($fullPath);
                    }
                    catch (Exception $e) {
                        throw new Exception(\sprintf("Error getting file: `%s' (`%s')", $path, $resolvedPath), $e->getCode(), $e);
                    }
                }

                return $path();
            })
            ->map(static function ($content) {
                return \trim($content);
            })
            ->join("\n\n");

        $lastModified = \count($lastModifieds) ? \max($lastModifieds) : false;

        return $this->sendContent($type, $merged, $lastModified, $realtime);
    }

    protected function mergeAssets($type, $list, $realtime = false)
    {
        return $this->mergeAndSendAssets(static function ($path) {
            return public_path($path);
        }, $type, $list, $realtime);
    }

    protected function mergeInternalAssets($type, $list, $realtime = false)
    {
        return $this->mergeAndSendAssets(static function ($path) {
            return __DIR__ . '/../res/' . Helpers::ensureNoDirectoryClimb($path);
        }, $type, $list, $realtime);
    }

    protected function sendContent($type, $content, $filemtime, $realtime)
    {
        $lastModified = $filemtime
            ? CarbonImmutable::createFromTimestampUTC((string) $filemtime)
                ->setTimezone('GMT')
                ->format('D, d M Y H:i:s T')
            : false;

        $headers = \array_filter([
            'Last-Modified' => $lastModified,
            'Cache-Control' => \sprintf(
                'no-transform, private, max-age=%s, %s',
                $realtime ? (! $this->app->getAssetCache() ? 30 : 15 * 60) : 8 * 60 * 60,
                $realtime ? (! $this->app->getAssetCache() ? 'must-revalidate' : 'stale-while-revalidate=' . (5 * 60)) : 'stale-while-revalidate=' . (1 * 60 * 60)
            ),
            'Keep-Alive' => null, // default keep alive, also sets Connection
            'Content-Type' => self::contentType($type),
            'E-Tag' => \md5($content),
        ]);

        return response($content, 200, $headers);
    }

    private static function contentType($type)
    {
        switch ((string) $type) {
            case 'css': return 'text/css; charset=UTF-8';
            case 'html': return 'text/html; charset=UTF-8';
            case 'js': return 'application/javascript; charset=UTF-8';
            case 'json': return 'application/json; charset=UTF-8';
            case 'png': return 'image/png';
            case 'woff2': return 'font/woff2';

            default: return 'application/octet-stream';
        }
    }

    private static function fileGetContentsAsset($path)
    {
        return \file_get_contents(__DIR__ . '/../res/' . Helpers::ensureNoDirectoryClimb($path));
    }
}
