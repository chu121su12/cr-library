<?php

namespace CR\Library\Avon\Controllers;

use CR\Library\Avon\Application;
use CR\Library\Helpers\Reply;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserController
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    private function defaultWithComponent($component)
    {
        return view('cr-avon2::resources', [
            '__avon' => $this->app->getViewData(),
            '__avon_app' => $this->app,
            '__avon_component' => $component,
        ]);
    }

    public function profileView()
    {
        return $this->defaultWithComponent('user-page');
    }

    public function verifyAccountView()
    {
        return $this->defaultWithComponent('verify-account');
    }

    public function confirmPasswordView()
    {
        return $this->defaultWithComponent('confirm-password');
    }

    public function logoutView()
    {
        return response()->view(
            'cr-avon2::logout',
            [
                '__avon' => $this->app->getViewData(),
                '__avon_app' => $this->app,
                '__avon_component' => 'logout',
            ],
            200,
            [
                'permissions-policy' => 'interest-cohort=()',
                'x-content-type-options' => 'nosniff',
                'x-frame-options' => 'DENY',
                'x-xss-protection' => '1; mode=block',
                'x-powered-by' => '',
            ]
        );
    }

    public function updatePassword(Request $request)
    {
        Log::info('account: change password attempt');

        $currentPassword = (string) $request->json('currentPassword');
        $newPassword = (string) $request->json('newPassword');
        $newPasswordConfirmation = (string) $request->json('newPasswordConfirmation');
        $newPasswordLength = \mb_strlen($newPassword);

        if ($currentPassword === '') {
            return (new Reply)->message('Password lama tidak boleh kosong.');
        }

        if ($newPassword === '') {
            return (new Reply)->message('Password baru tidak boleh kosong.');
        }

        if ($currentPassword === $newPassword) {
            return (new Reply)->message('Password baru tidak boleh sama dengan password lama.');
        }

        if ($newPasswordConfirmation === '') {
            return (new Reply)->message('Konfirmasi password baru tidak boleh kosong.');
        }

        if ($newPassword !== $newPasswordConfirmation) {
            return (new Reply)->message('Konfirmasi password baru tidak cocok.');
        }

        if ($newPasswordLength <= 6) {
            return (new Reply)->message('Panjang password harus lebih dari 6 karakter.');
        }

        if ($newPasswordLength >= 1000) {
            return (new Reply)->message('Panjang password harus kurang dari 1000 karakter.');
        }

        if ($newPasswordLength <= 14 && ! \preg_match('/\pL.*?\pN|\pN.*?\pL/u', $newPassword)) {
            return (new Reply)->message('Password pendek harus disertai angka dan huruf.');
        }

        if ($newPasswordLength <= 14 && ! \preg_match('/[\pZ\pS\pP]/u', $newPassword)) {
            return (new Reply)->message('Password pendek harus disertai simbol non-angka/non-huruf.');
        }

        $user = auth()->user();

        if (self::checkContains($newPassword, optional($user)->getAuthIdentifier())) {
            return (new Reply)->message('Password tidak boleh diisi komponen user.');
        }

        if (self::checkContains($newPassword, optional($user)->getAccountName())) {
            return (new Reply)->message('Password tidak boleh diisi komponen nama.');
        }

        Log::info('account: change password validation');

        if (! value($user::authenticationCallback(), $user->getAuthIdentifier(), $currentPassword, $request, $this->app)) {
            return (new Reply)->message('Password lama salah.');
        }

        Log::info('account: changing password');

        $result = value($user::resetPasswordCallback(), $user, $newPassword, $currentPassword, $request, $this->app);

        if ($result === true) {
            Log::info('account: change password success');

            return (new Reply)->message('Password berhasil diganti.');
        }

        Log::info('account: change password - other');

        if (\is_string($result)) {
            return (new Reply)->message($result);
        }

        if ($result instanceof Responsable) {
            return $result;
        }

        return (new Reply)->message('Password gagal diganti.');
    }

    private static function checkContains($check, $base)
    {
        $base = \strtoupper(\preg_replace('/[_\D\W]/i', '', $base ?: ''));
        $check = \strtoupper(\preg_replace('/[_\D\W]/i', '', $check ?: ''));

        if ($base === '' || $check === '') {
            return false;
        }

        return \str_contains($check, $base) || \str_contains($base, $check);
    }

    public function verifyAccount(Request $request)
    {
        $user = auth()->user();

        dd($user);
    }

    public function confirmPassword(Request $request)
    {
        $user = auth()->user();

        dd($user);
    }
}
