<?php

namespace CR\Library\Avon\Controllers;

use CR\Library\Avon\Application;

class ViewController
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function home()
    {
        return $this->defaultWithComponent('home-page');
    }

    public function index($resource)
    {
        return $this->useResource($resource, 'resource-index');
    }

    public function create($resource)
    {
        return $this->useResource($resource, 'resource-create');
    }

    public function detail($resource)
    {
        return $this->useResource($resource, 'resource-detail');
    }

    public function edit($resource)
    {
        return $this->useResource($resource, 'resource-edit');
    }

    public function replicate($resource)
    {
        return $this->useResource($resource, 'resource-replicate');
    }

    public function noResource()
    {
        return $this->redirectHome();
    }

    private function defaultWithComponent($component)
    {
        return response()->view(
            'cr-avon2::resources',
            [
                '__avon' => $this->app->getViewData(),
                '__avon_app' => $this->app,
                '__avon_component' => $component,
            ],
            200,
            [
                'permissions-policy' => 'interest-cohort=()',
                'x-content-type-options' => 'nosniff',
                'x-frame-options' => 'DENY',
                'x-xss-protection' => '1; mode=block',
                'x-powered-by' => '',
            ]
        );
    }

    private function redirectHome()
    {
        return redirect($this->app->url($this->app->getPrefix()));
    }

    private function useResource($resource, $component)
    {
        if ($this->app->getResource($resource)) {
            return $this->defaultWithComponent($component);
        }

        return $this->redirectHome();
    }

    public function dashboard($dashboard)
    {
        if ($this->app->getDashboard($dashboard)) {
            return $this->defaultWithComponent('dashboard');
        }

        return $this->redirectHome();
    }

    public function tool($tool)
    {
        if ($this->app->getTool($tool)) {
            return $this->defaultWithComponent('tool');
        }

        return $this->redirectHome();
    }
}
