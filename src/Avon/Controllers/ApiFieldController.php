<?php

namespace CR\Library\Avon\Controllers;

use CR\Library\Avon\Application;
use CR\Library\Helpers\Reply;
use Illuminate\Http\Request;

class ApiFieldController
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function view(Request $request, $field)
    {
        $fieldClass = $this->app->getField($field);

        if (! $fieldClass) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $fieldInstance = new $fieldClass;

        if (! $fieldInstance->authorizedToSee($request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return $fieldInstance->resolveViewApi($request);
    }

    public function data(Request $request, $field, $key)
    {
        $fieldClass = $this->app->getField($field);

        if (! $fieldClass) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $fieldInstance = new $fieldClass;

        if (! $fieldInstance->authorizedToSee($request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return $fieldInstance->resolveDataApi($request, $key);
    }

    public function action(Request $request, $field, $key)
    {
        $fieldClass = $this->app->getField($field);

        if (! $fieldClass) {
            return (new Reply)->code(404)->danger('Data tidak ada.');
        }

        $fieldInstance = new $fieldClass;

        if (! $fieldInstance->authorizedToSee($request)) {
            return (new Reply)->code(403)->danger('Data tidak bisa diakses.');
        }

        return $fieldInstance->resolveActionApi($request, $key);
    }
}
