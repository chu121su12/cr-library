<?php

namespace CR\Library\Laravel;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Query\Builder as QueryBuilder;

class MacroServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $builderMixin = new DeferredJoinPagination\BuilderMixin;
        QueryBuilder::mixin($builderMixin);
        EloquentBuilder::mixin($builderMixin);

        $relationMixin = new DeferredJoinPagination\RelationMixin;
        BelongsToMany::mixin($relationMixin);
        HasManyThrough::mixin($relationMixin);
    }
}
