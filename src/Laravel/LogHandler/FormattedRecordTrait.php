<?php

namespace CR\Library\Laravel\LogHandler;

use Monolog\LogRecord;

trait FormattedRecordTrait
{
    private $updates;

    public static function make($_parentRecord, $updates)
    {
        if (! \class_exists(LogRecord::class)) {
            return $updates;
        }

        $instance = new static(
            $updates['datetime'],
            $updates['channel'],
            $updates['level'],
            $updates['message'],
            $updates['context'],
            $updates['extra'],
            $updates['formatted']
        );

        $instance->updates = $updates;

        return $instance;
    }
}
