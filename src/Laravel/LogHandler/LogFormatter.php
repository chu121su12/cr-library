<?php

namespace CR\Library\Laravel\LogHandler;

use CR\Library\Helpers\Helpers;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;

# grep --line-buffered -vE '(\]: -(cache|dispatch|httpc-.*?Request Sending|httpr-(.shut|fin|user|response)|sql)|\(encrypted\):)|","timestamp":"[^"]{50,}' 
# | grep --line-buffered -P '^\x{200B}\[[^]]+][^]]+]:( -httpr-access.*| -[^ ]+|)'

class LogFormatter
{
    const ZWSP = '​';

    private static $appStart;

    private static $lineFormatter;

    private static $staticKey;

    private $instanceKey;

    public function __construct()
    {
        if (! isset(self::$appStart)) {
            self::$appStart = (string) (\defined('LARAVEL_START') ? LARAVEL_START : \microtime(true));
        }

        $this->instanceKey = Str::random(6);

        if (! isset(self::$staticKey)) {
            self::$staticKey = Str::random(8);
        }

        if (! isset(self::$lineFormatter)) {
            self::$lineFormatter = tap(new LineFormatter(
                \implode(' ', [
                    '' . self::ZWSP . '[%datetime%] %__now_timestamp%',
                    '%__start_timestamp%-%__static_key%-%__instance_key%',
                    '%__client_nonce%',
                    '%__request_id%-%__task_id%-%_group_id%',
                    '%channel%.%level_name%',
                    '%__internal_status%-%__limited_status%',
                    '%__app% %__user_id%: %message% %context% %extra%',
                    "\n",
                ]),
                'Y-m-d H:i:s',
                true,
                true
            ))->includeStacktraces();

            Log::withContext([
                '__app' => '',
            ]);
        }
    }

    public function __invoke($logger)
    {
        if (\method_exists($logger, 'getLogger')) {
            $logger = $logger->getLogger();
        }

        $logger->pushProcessor(function ($record) {
            return $this->processor($record);
        });

        foreach ($logger->getHandlers() as $handler) {
            if ($handler instanceof StreamHandler) {
                $handler->setFormatter(self::$lineFormatter);
            }
        }
    }

    private function processor($logRecord)
    {
        $record = self::processLogRecord(
            \is_array($logRecord) ? $logRecord : collect($logRecord)->all(),
            self::$appStart,
            self::$staticKey,
            $this->instanceKey
        );

        return FormattedRecord::make($logRecord, $record);
    }

    public static function processLogRecord(array $record, $appStart, $staticKey, $instanceKey)
    {
        list($mt1, $mt2) = Helpers::microTimeData();
        list($ls1, $ls2) = \explode('.', $appStart . '.0', 3);

        $record['__now_timestamp'] = $mt1 . $mt2;
        $record['__start_timestamp'] = "{$ls1}." . \str_pad($ls2, 6, '0', \STR_PAD_RIGHT);
        $record['__static_key'] = $staticKey;
        $record['__instance_key'] = $instanceKey;
        $record['__limited_status'] = '0';
        $record['__internal_status'] = '0';

        $context = $record['context'];

        foreach (['limited', 'internal'] as $key) {
            $contextKey = '__cr_' . $key;
            $record['__' . $key . '_status'] = \array_key_exists($contextKey, $context) && $context[$contextKey] ? '1' : '0';
            unset($context[$contextKey]);
        }

        foreach (['client_nonce', 'request_id', 'task_id', 'user_id'] as $key) {
            $contextKey = '__cr_' . $key;
            if (\array_key_exists($contextKey, $context)) {
                if ($context[$contextKey] !== false) {
                    $record['__' . $key] = $context[$contextKey];
                }
                unset($context[$contextKey]);
            }
        }

        foreach (['user_id', 'app'] as $key) {
            $contextKey = '__' . $key;
            if (\array_key_exists($contextKey, $context)) {
                if ($context[$contextKey] !== false) {
                    $record['__' . $key] = $context[$contextKey];
                }
                unset($context[$contextKey]);
            }
        }

        if ($record['__limited_status'] !== '0' && app()->isProduction()) {
            $record['context'] = [];
        }
        elseif ($record['__internal_status'] !== '0' && ! app()->hasDebugModeEnabled()) { // is non-debug
            $record['context'] = [];
        }
        else {
            $record['context'] = $context;
        }

        return $record;
    }
}
