<?php

namespace CR\Library\Laravel\LogHandler;

use Monolog\LogRecord;

if (\class_exists(LogRecord::class)) {
    class FormattedRecord extends LogRecord
    {
        use FormattedRecordTrait;

        public function toArray()
        {
            return \array_merge(parent::toArray(), $this->updates);
        }
    }
}
else {
    class FormattedRecord
    {
        use FormattedRecordTrait;

        public function toArray()
        {
            return $this->updates;
        }
    }
}
