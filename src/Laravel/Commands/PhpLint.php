<?php

namespace CR\Library\Laravel\Commands;

use CR\Library\Helpers\ConcurrentWorker;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Process\Process;

class PhpLint extends Command
{
    use GetPhpPath;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run php -l to a directory.';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cr:lint
                            {--d|directory=* : File/folder to lint}
                            ';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \set_time_limit(0);

        foreach (($directories = Arr::wrap($this->option('directory'))) as $directory) {
            $this->info('Linting ' . \realpath($directory));
            $this->lintDirectory($directory);
        }

        $this->info('Done ' . \count($directories) . ' directories.');
    }

    protected function lintDirectory($directory)
    {
        $files = (new Finder)
            ->in($directory)
            ->name('*.php')
            ->files();

        $bar = $this->output->createProgressBar($files->count());

        $paths = [];
        foreach ($files as $file) {
            $paths[] = $file->getPathname();
        }

        (new ConcurrentWorker)
            ->concurrency(16)
            ->handler(function ($file) {
                $this->lintFile($file);
            })
            ->notify(static function () use ($bar) {
                $bar->advance();
            })
            ->push($paths);

        $bar->finish();
        $this->line('');
    }

    protected function lintFile($file)
    {
        $linter = new Process([
            $this->phpPath(),
            '-l',
            \realpath($file),
        ]);

        if ($linter->run() !== 0) {
            $this->line('');
            $this->error($linter->getOutput());
        }
    }
}
