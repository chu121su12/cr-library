<?php

namespace CR\Library\Laravel\Commands;

use Symfony\Component\Process\PhpExecutableFinder;

trait GetPhpPath
{
    protected $phpPath;

    protected function phpPath()
    {
        if (null === $this->phpPath) {
            $this->phpPath = (new PhpExecutableFinder)->find();
        }

        return $this->phpPath;
    }
}
