<?php

namespace CR\Library\Laravel\HttpMiddleware;

class BlockRootPhpServe
{
    public function handle($request, $next)
    {
        if (\str_starts_with($request->server('REQUEST_URI'), '/index.php')) {
            abort(404);
        }

        return $next($request);
    }
}
