<?php

namespace CR\Library\Laravel\HttpMiddleware;

class LogShutdownFunction extends LogHttpAccess
{
    protected $accessKey;

    protected $class;

    public function __construct($class, $key)
    {
        $this->accessKey = $key;

        $this->class = $class;
    }

    public function __invoke()
    {
        $class = $this->class;

        $class::shutdownFunction($this->accessKey);
    }
}
