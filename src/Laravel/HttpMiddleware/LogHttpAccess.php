<?php

namespace CR\Library\Laravel\HttpMiddleware;

use CR\Library\Helpers\Helpers;
use CR\Library\Helpers\Locations;
use CR\Library\Laravel\LogHandler\LogHttpShutdown;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use JsonSerializable;
use UAParser\Parser as UAParser;

class LogHttpAccess
{
    protected static $appStart;

    protected static $terminateCache;

    protected $finish;

    protected $key;

    protected $start;

    public function handle($request, $next)
    {
        if (! isset(static::$appStart)) {
            static::$appStart = \defined('LARAVEL_START') ? LARAVEL_START : \microtime(true);
        }

        $this->start = (float) Helpers::microTime()->format('U.u');

        $this->key = Str::random();

        Log::withContext([
            '__cr_request_id' => $this->key,
            '__cr_client_nonce' => self::parseClientNonce($request->header('x-random')),
        ]);

        $this->requestLog1($request);
        $this->requestLog2($request);

        $this->initTermination();

        $response = $next($request);

        $this->finish = (float) Helpers::microTime()->format('U.u');

        $user = $this->resolveUser();

        $this->responseLog1($user, $request, $response);
        $this->responseLog2($user, $response);

        $this->updateTermination();

        return $response;
    }

    public function terminate($request, $response)
    {
        $this->resolveUser();

        // new instance(!) by default
        self::shutdownFunction($this->key);
    }

    protected function getUserKey($user)
    {
        if ($user === null) {
            return null;
        }

        if ($user instanceof Authenticatable || \method_exists($user, 'getAuthIdentifier')) {
            return $user->getAuthIdentifier();
        }

        if (\method_exists($user, 'getKey')) {
            return $user->getKey();
        }

        if (isset($user->id)) {
            return $user->id;
        }

        return null;
    }

    protected function initTermination()
    {
        if (! \is_array(static::$terminateCache)) {
            static::$terminateCache = [];
        }

        $key = $this->key;

        static::$terminateCache[$key] = [
            'type' => 'end',
            'app-start' => static::$appStart,
            'now' => null,

            'boot' => \round(($this->start - static::$appStart) * 1000, 2),
            'timing' => null,
            'shutdown' => null,
            'total' => null,

            'start' => $this->start,
            'finish' => null,
            'end' => null,

            'outsider' => false,
            'terminate-count' => null,
        ];

        Helpers::safeDispatchAfterResponse(new LogHttpShutdown(static::class, $key));
    }

    protected function requestLog1($request)
    {
        $requestString = \sprintf('%s %s %s', $request->getMethod(), $request->getRequestUri(), $request->server('SERVER_PROTOCOL'));
        $userAgent = $request->header('user-agent');
        $iOSWebView = \mb_strpos($userAgent, 'Mobile/') !== false && \mb_strpos($userAgent, 'Safari/') === false;

        Log::info(\sprintf('-httpr-access %s "%s"', $request->ip(), $requestString), [
            '__cr_internal' => true,
        ]);

        self::httpDebugLog('-httpr-request', [
            '__cr_internal' => true,

            'type' => 'req',
            'app-start' => static::$appStart,
            'now' => Helpers::microTime()->format('Y-m-d H:i:s.u'),

            'start' => $this->start,
            'boot' => \round(($this->start - static::$appStart) * 1000, 2),

            'ip' => $request->ip(),
            'method' => $request->getRealMethod(),
            'host' => $request->getHttpHost(),
            'path' => $request->getPathInfo(),
            'request' => $requestString,
            'referer' => $request->server('HTTP_REFERER'),
            'app-id-ios' => $iOSWebView === false ? null : $iOSWebView,
            'app-id-android' => $request->server('HTTP_X_REQUESTED_WITH'),
            'raw-ua' => $userAgent,
            'crawler' => self::parseUaForCrawler($userAgent),
            'parsed-ua' => self::parseUserAgent($userAgent),
            'possible-ips' => self::locateIps($request),
        ]);
    }

    protected function requestLog2($request)
    {
        self::httpDebugLog('-httpr-data', [
            '__cr_internal' => true,

            'type' => 'data',
            'post' => $request->post(),
            'query' => $request->query(),
            'cookie' => $request->cookie(),
            'file-key' => $request->files->keys(),
            'headers' => collect($request->headers->all())->all(),

            '__cr_limited' => true,
        ]);
    }

    protected function resolveUser()
    {
        try {
            if (auth()->hasUser() && ($user = auth()->user())) {
                $key = $this->getUserKey($user);

                if ($key && \is_scalar($key)) {
                    $key = (string) $key;
                }

                if ($key !== '') {
                    Log::withContext([
                        '__cr_user_id' => "[id:{$key}]",
                    ]);
                }

                return $user;
            }
        }
        catch (InvalidArgumentException $e) {
            if (! \preg_match('/^Database connection \[.*\] not configured\.$/', $e->getMessage())) {
                Helpers::reportIgnoredException('Error fetching user or user info.', $e);
            }
        }
        catch (Exception $e) {
            Helpers::reportIgnoredException('Error fetching user or user info.', $e);
        }
    }

    protected function responseLog1($user, $request, $response)
    {
        self::httpDebugLog('-httpr-response', [
            '__cr_internal' => true,

            'type' => 'res',
            'app-start' => static::$appStart,
            'now' => Helpers::microTime()->format('Y-m-d H:i:s.u'),

            'finish' => $this->finish,
            'timing' => \round(($this->finish - $this->start) * 1000, 2),

            'status' => optional($response)->getStatusCode(),
            'response-type' => \get_class($response),
            'user' => $this->getUserKey($user),
            'access' => $request->getMethod() . ' ' . $request->getRequestUri(),
        ]);
    }

    protected function responseLog2($user, $response)
    {
        self::httpDebugLog('-httpr-user', [
            '__cr_internal' => true,

            'type' => 'usr',
            'user-data' => self::userData($user),
            'sessions' => collect(session()->all())->except([
                '_token',
                '_previous',
                '_flash',
            ])->all(),

            '__cr_limited' => true,
        ]);
    }

    protected function updateTermination()
    {
        if (! \is_array(static::$terminateCache)) {
            static::$terminateCache = [];
        }

        static::$terminateCache[$this->key]['finish'] = $this->finish;
        static::$terminateCache[$this->key]['timing'] = \round(($this->finish - $this->start) * 1000, 2);
    }

    public static function shutdownFunction($key)
    {
        if (isset($key) && \is_array(static::$terminateCache) && isset(static::$terminateCache[$key])) {
            self::shutdownInternal($key);
        }
        else {
            self::shutdownExternal($key);
        }
    }

    protected static function locateIps($request)
    {
        return collect($request->server)
            ->only([
                'HTTP_CLIENT_IP',
                'HTTP_X_FORWARDED_FOR',
                'HTTP_X_FORWARDED',
                'HTTP_X_CLUSTER_CLIENT_IP',
                'HTTP_FORWARDED_FOR',
                'HTTP_FORWARDED',
                'REMOTE_ADDR',
            ])
            ->filter()
            ->map(function ($ip) {
                $private = Helpers::checkPrivateIp($ip);

                if ($private) {
                    return "{$ip} (prv)";
                }

                $location = Locations::viaIp($ip);

                return $location ? "{$ip} ({$location})" : $ip;
            })
            ->all();
    }

    protected static function parseClientNonce($clientNonce)
    {
        if (! \is_string($clientNonce)) {
            return '-';
        }

        $len = \strlen($clientNonce);

        if ($len >= 4 && $len <= 32) {
            return "[{$clientNonce}]";
        }

        return '-';
    }

    protected static function parseUaForCrawler($userAgent)
    {
        if (\class_exists(CrawlerDetect::class)) {
            return tap(new CrawlerDetect)->isCrawler($userAgent)->getMatches();
        }

        return false;
    }

    protected static function parseUserAgent($userAgent)
    {
        if (\class_exists(UAParser::class)) {
            return with(UAParser::create()->parse($userAgent), static function ($parsed) {
                return [
                    'ua' => (array) $parsed->ua,
                    'os' => (array) $parsed->os,
                    'device' => (array) $parsed->device,
                    'osVersion' => $parsed->os->toVersion(),
                    'uaVersion' => $parsed->ua->toVersion(),
                ];
            });
        }

        return false;
    }

    protected static function shutdownExternal($key)
    {
        $end = (float) Helpers::microTime()->format('U.u');

        self::httpDebugLog('-httpr-eshutdown', [
            '__cr_internal' => true,

            'type' => 'end',
            'key' => $key,
            'app-start' => static::$appStart,
            'now' => Helpers::microTime()->format('Y-m-d H:i:s.u'),

            'boot' => $end,
            'timing' => 0,
            'shutdown' => 0,
            'total' => $end,

            'start' => $end,
            'finish' => $end,
            'end' => $end,

            'outsider' => true,
            'terminate-count' => null,
        ]);
    }

    protected static function shutdownInternal($key)
    {
        $cache = (array) static::$terminateCache[$key];
        $cache['__cr_internal'] = true;

        unset(static::$terminateCache[$key]);

        $end = (float) Helpers::microTime()->format('U.u');

        if (! isset($cache['start'])) {
            $cache['start'] = $end;
        }

        if (! isset($cache['boot'])) {
            $cache['boot'] = $end;
        }

        if (! isset($cache['finish'])) {
            $cache['outsider'] = true;
            $cache['finish'] = $end;
            $cache['timing'] = \round(($end - $cache['start']) * 1000, 2);
        }

        $cache['now'] = Helpers::microTime()->format('Y-m-d H:i:s.u');
        $cache['shutdown'] = \round(($end - $cache['finish']) * 1000, 2);
        $cache['total'] = $cache['boot'] + $cache['timing'] + $cache['shutdown'];
        $cache['end'] = $end;
        $cache['terminate-count'] = \count(static::$terminateCache);

        self::httpDebugLog('-httpr-ishutdown', $cache);
    }

    protected static function userData($user)
    {
        if ($user === null) {
            return null;
        }

        if ($user instanceof JsonSerializable) {
            return [\get_class($user) => $user->jsonSerialize()];
        }

        return [\get_class($user) => $user];
    }

    protected static function httpDebugLog($prefix, array $context)
    {
        $alterIni = \version_compare(\PHP_VERSION, '7.1', '>=');

        if ($alterIni) {
            $iniPrecision = \ini_get('precision');
            if ($iniPrecision === false || ! backport_is_numeric($iniPrecision) || $iniPrecision >= 17) {
                $iniPrecision = false;
            }
            else {
                \ini_set('precision', '17');
            }

            $iniSerialize = \ini_get('serialize_precision');
            if ($iniSerialize === false || ! backport_is_numeric($iniSerialize) || (int) $iniSerialize === -1) {
                $iniSerialize = false;
            }
            else {
                \ini_set('serialize_precision', '-1');
            }
        }

        Log::debug($prefix, $context);

        if ($alterIni) {
            if ($iniPrecision !== false) {
                \ini_set('precision', $iniPrecision);
            }

            if ($iniSerialize !== false) {
                \ini_set('serialize_precision', $iniSerialize);
            }
        }
    }
}
