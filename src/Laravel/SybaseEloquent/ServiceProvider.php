<?php

namespace CR\Library\Laravel\SybaseEloquent;

use CR\Library\NotPdo\Sybase;
use Exception;
use Illuminate\Database\Connection as IlluminateConnection;
use Illuminate\Database\Connectors\Connector;
use Illuminate\Support\Arr;
use Illuminate\Support\ConfigurationUrlParser;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s.u';

    const SELECT_TOP_MAX_LIMIT = 32767;

    public function boot()
    {
        static::registerDriver();

        //
    }

    public function register()
    {
        //
    }

    public static function correctNonEnglishLocale($date)
    {
        if (! \is_string($date)) {
            return $date;
        }

        return Sybase::fixSybaseDate($date);
    }

    public static function hasSybaseFunctions()
    {
        return \function_exists('sybase_connect');
    }

    public static function registerDriver($as = null)
    {
        $connector = new Connector;

        $as = $as === null
            ? ['sybase', 'fsybase', 'dsybase']
            : Arr::wrap($as);

        $parser = new ConfigurationUrlParser;

        foreach ($as as $alias) {
            static::registerResolver($parser, $alias, $connector);
        }
    }

    public static function newDriverInstance($config, $options)
    {
        if (! isset($config['name'])) {
            throw new Exception(\sprintf('Connection name not defined. (%s)', \json_encode($config)));
        }

        $connection = $config['name'];

        foreach (['database', 'username', 'password', 'host', 'port'] as $key) {
            if (! isset($config[$key])) {
                throw new Exception("Parameter '{$key}' on '{$connection}' not defined.");
            }
        }

        Sybase::env($config);

        $instance = new Sybase;
        $instance->init(Statement::class, $config, $options);

        if (! static::canUseSybaseFunctions() || (isset($config['usepdo']) && $config['usepdo'])) {
            if (! (isset($config['notpdo']) && $config['notpdo'])) {
                $callback = Sybase::pdoResolver();

                return $callback($instance);
            }

            $instance->setPdoResolver(Sybase::pdoResolver());
        }

        return $instance;
    }

    protected static function canUseSybaseFunctions()
    {
        return \version_compare(\PHP_VERSION, '7.0.0', '<') && static::hasSybaseFunctions();
    }

    protected static function registerResolver(ConfigurationUrlParser $parser, $alias, Connector $connector)
    {
        IlluminateConnection::resolverFor($alias, static function ($_connection, $_database, $_prefix, $config) use ($parser, $connector) {
            $config = $parser->parseConfiguration($config);
            $options = $connector->getOptions($config);

            if (! isset($config['name']) || ! $config['name']) {
                throw new Exception('Database name not defined');
            }

            $config['prefix'] = isset($config['prefix']) ? $config['prefix'] : '';

            return new Connection(
                static::newDriverInstance($config, $options),
                $config['database'],
                $config['prefix'],
                $config
            );
        });
    }
}
