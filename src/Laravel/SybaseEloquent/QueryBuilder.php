<?php

namespace CR\Library\Laravel\SybaseEloquent;

use Illuminate\Support\Arr;

class QueryBuilder extends \Illuminate\Database\Query\Builder
{
    public function insert(array $values)
    {
        if (empty($values)) {
            return true;
        }

        if (! \is_array(\reset($values))) {
            $values = [$values];
        }
        else {
            foreach ($values as $key => $value) {
                \ksort($value);

                $values[$key] = $value;
            }
        }

        foreach ($values as $bindings) {
            $this->connection->insert(
                $this->grammar->compileInsert($this, $bindings),
                $this->cleanBindings(Arr::flatten($bindings, 1))
            );
        }

        return true;
    }
}
