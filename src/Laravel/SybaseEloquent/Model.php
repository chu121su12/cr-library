<?php

namespace CR\Library\Laravel\SybaseEloquent;

use Illuminate\Database\Query\Expression;

class Model extends \Illuminate\Database\Eloquent\Model
{
    protected static $cachedStringCastable;

    protected static $cachedTimestampCastable;

    public function newEloquentBuilder($query)
    {
        return new EloquentBuilder($query);
    }

    public function selectColumnModifier($grammar, $columns = ['*'])
    {
        if ($columns === ['*']) {
            $dateCast = $this->getCachedTimestampCastable();
            $stringCast = $this->getCachedStringCastable();

            if ($dateCast->count() || $stringCast->count()) {
                $columns = [];

                foreach ($dateCast as $column => $_type) {
                    $columns[] = new Expression(\sprintf(
                        'cast(%s as char) as %s',
                        $grammar->wrap($column),
                        $grammar->wrap($this->wrapTimestampColumn($column))
                    ));
                }

                foreach ($stringCast as $column => $_type) {
                    $columns[] = new Expression(\sprintf(
                        'length(%s) as %s',
                        $grammar->wrap($column),
                        $grammar->wrap($this->wrapStringColumn($column))
                    ));
                }

                $columns[] = '*';
            }
        }

        return $columns;
    }

    protected function getArrayableAttributes()
    {
        $attributes = parent::getArrayableAttributes();

        foreach ($this->getCachedTimestampCastable() as $key => $_type) {
            if (isset($attributes[$key])) {
                $attributes[$key] = $this->castTimestampColumnValue($key, $attributes[$key]) ?: $this->asDateTime($attributes[$key]);
            }
        }

        foreach ($this->getCachedStringCastable() as $key => $_type) {
            if (isset($attributes[$key])) {
                $stringValue = $this->castStringColumnValue($key, $attributes[$key]);
                if (isset($stringValue)) {
                    $attributes[$key] = $stringValue;
                }
            }
        }

        return $attributes;
    }

    protected function asDateTime($value)
    {
        return parent::asDateTime(ServiceProvider::correctNonEnglishLocale($value));
    }

    protected function castAttribute($key, $value)
    {
        if ($value === null) {
            return $value;
        }

        if ($casted = $this->castTimestampColumnValue($key, $value)) {
            return $casted;
        }

        if ($casted = $this->castStringColumnValue($key, $value)) {
            return $casted;
        }

        return parent::castAttribute($key, $value);
    }

    protected function castStringColumnValue($key, $value)
    {
        if (isset($this->original[$wrappedKey = $this->wrapStringColumn($key)])) {
            $length = $this->original[$wrappedKey];

            if ($value !== null && \strlen($value) > $length) {
                return \substr($value, 0, $length);
            }

            return $value;
        }

        return null;
    }

    protected function castTimestampColumnValue($key, $value)
    {
        if (isset($this->original[$wrappedKey = $this->wrapTimestampColumn($key)])) {
            $dateFormat = $this->dateFormat;
            $this->dateFormat = ServiceProvider::DEFAULT_DATE_FORMAT;
            $value = $this->asDateTime($this->original[$wrappedKey]);
            $this->dateFormat = $dateFormat;

            return $value;
        }

        return null;
    }

    protected function getCachedStringCastable()
    {
        if (static::$cachedStringCastable !== null) {
            return static::$cachedStringCastable;
        }

        $casts = collect($this->getCasts())->filter(static function ($value) {
            return \in_array($value, ['string'], true);
        });

        return static::$cachedStringCastable = $casts;
    }

    protected function getCachedTimestampCastable()
    {
        if (static::$cachedTimestampCastable !== null) {
            return static::$cachedTimestampCastable;
        }

        $dates = collect($this->getDates())->flip()->map(static function () {
            return 'timestamp';
        });

        $casts = collect($this->getCasts())->filter(static function ($value) {
            return \in_array($value, ['date', 'datetime', 'timestamp'], true);
        });

        return static::$cachedTimestampCastable = $casts->merge($dates);
    }

    protected function newBaseQueryBuilder()
    {
        $connection = $this->getConnection();

        return new QueryBuilder(
            $connection,
            $connection->getQueryGrammar(),
            $connection->getPostProcessor()
        );
    }

    protected function wrapTimestampColumn($column)
    {
        return "_{$column}_ts";
    }

    protected function wrapStringColumn($column)
    {
        return "_{$column}_len";
    }
}
