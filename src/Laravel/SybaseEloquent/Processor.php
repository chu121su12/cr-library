<?php

namespace CR\Library\Laravel\SybaseEloquent;

use Illuminate\Database\Query\Builder;

class Processor extends \Illuminate\Database\Query\Processors\Processor
{
    public function processSelect(Builder $query, $results)
    {
        foreach ($results as $key => $row) {
            $results[$key] = $this->processRowTimestamp($row);
        }

        return $results;
    }

    protected function processRowTimestamp($row)
    {
        return $row;
    }
}
