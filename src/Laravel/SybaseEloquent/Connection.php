<?php

namespace CR\Library\Laravel\SybaseEloquent;

use DateTimeInterface;
use Exception;
use Throwable;

class Connection extends \Illuminate\Database\Connection
{
    public function cursor($query, $bindings = [], $useReadPdo = true)
    {
        $statement = $this->run($query, $bindings, function ($query, $bindings) use ($useReadPdo) {
            if ($this->pretending()) {
                return [];
            }

            return $this->executeSelectQuery(true, $query, $bindings, $useReadPdo);
        });

        while ($record = $statement->fetch()) {
            yield $record;
        }
    }

    public function cursorUnprocessed($query, $bindings = [], $useReadPdo = true)
    {
        $statement = $this->run($query, $bindings, function ($query, $bindings) use ($useReadPdo) {
            if ($this->pretending()) {
                return [];
            }

            return $this->executeSelectQuery(false, $query, $bindings, $useReadPdo);
        });

        while ($record = $statement->fetch()) {
            yield $record;
        }
    }

    public function prepareBindings(array $bindings)
    {
        $grammar = $this->getQueryGrammar();

        foreach ($bindings as $key => $value) {
            if ($value instanceof DateTimeInterface) {
                $bindings[$key] = $value->format($grammar->getDateFormat());

                if ($grammar->getDateFormat() === ServiceProvider::DEFAULT_DATE_FORMAT) {
                    $bindings[$key] = \substr($bindings[$key], 0, -3);
                }
            }
            elseif ($value === false) {
                $bindings[$key] = 0;
            }
        }

        return $bindings;
    }

    public function select($query, $bindings = [], $useReadPdo = true)
    {
        return $this->run($query, $bindings, function ($query, $bindings) use ($useReadPdo) {
            if ($this->pretending()) {
                return [];
            }

            $statement = $this->executeSelectQuery(true, $query, $bindings, $useReadPdo);

            return $statement->fetchAll();
        });
    }

    public function selectUnprocessed($query, $bindings = [], $useReadPdo = true)
    {
        return $this->run($query, $bindings, function ($query, $bindings) use ($useReadPdo) {
            if ($this->pretending()) {
                return [];
            }

            $statement = $this->executeSelectQuery(false, $query, $bindings, $useReadPdo);

            return $statement->fetchAll();
        });
    }

    protected function getDefaultPostProcessor()
    {
        return new Processor;
    }

    protected function getDefaultQueryGrammar()
    {
        return new Grammar;
    }

    private function executeCombinedSelectQuery($query, $bindings, $useReadPdo)
    {
        $statement = $this->prepared($this->getPdoForSelect($useReadPdo)->prepare($query));

        $this->bindValues($statement, $this->prepareBindings($bindings));

        $statement->execute();

        return $statement;
    }

    private function executeSelectQuery($tryProcessed, $query, $bindings, $useReadPdo)
    {
        try {
            $e = null;

            if ($tryProcessed && ! ServiceProvider::hasSybaseFunctions()) {
                /** @var Grammar $grammar */
                $grammar = $this->getQueryGrammar();

                if ($ansiSelect = $grammar->parseAnsiPdoSelect($query)) {
                    return $this->executeSplitSelectQuery($ansiSelect, $bindings, $useReadPdo);
                }
            }

            return $this->executeCombinedSelectQuery($query, $bindings, $useReadPdo);
        }
        catch (Exception $e) {
        }
        catch (Throwable $e) {
        }

        if ($e !== null) {
            throw $e;
        }
    }

    private function executeSplitSelectQuery($queries, $bindings, $useReadPdo)
    {
        list($copyQuery, $updateQuery, $selectQuery) = $queries;

        $pdo = $this->getPdoForSelect($useReadPdo);

        // Copy to temp
        tap($this->prepared($pdo->prepare($copyQuery)), function ($statement) use ($bindings) {
            $this->bindValues($statement, $this->prepareBindings($bindings));

            $statement->execute();
        });

        // Update primary key
        $pdo->prepare($updateQuery)->execute();

        // Select result
        return tap($pdo->prepare($selectQuery), function ($statement) {
            $statement->setFetchMode($this->fetchMode);

            $statement->execute();
        });
    }
}
