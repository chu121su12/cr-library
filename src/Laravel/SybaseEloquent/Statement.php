<?php

namespace CR\Library\Laravel\SybaseEloquent;

use PDOStatement;

class Statement extends PDOStatement
{
    use Patch\Statement;

    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    #[\ReturnTypeWillChange]
    public function bindValue($paramno, $param, $type = null)
    {
        return $this->pdo->bindValue($paramno, $param, $type);
    }

    #[\ReturnTypeWillChange]
    public function closeCursor()
    {
        return $this->pdo->closeCursor();
    }

    #[\ReturnTypeWillChange]
    public function execute($bindings = null)
    {
        return $this->pdo->execute($bindings);
    }

    #[\ReturnTypeWillChange]
    public function fetch($how = null, $orientation = null, $offset = null)
    {
        return $this->pdo->fetch($how, $orientation);
    }

    #[\ReturnTypeWillChange]
    public function rowCount()
    {
        return $this->pdo->rowCount();
    }

    protected function _fetchAll()
    {
        return $this->pdo->fetchAll(...\func_get_args());
    }

    protected function _setFetchMode()
    {
        $args = \func_get_args();
        if (! isset($args[1])) {
            $args[1] = null;
        }

        return $this->pdo->setFetchMode(...$args);
    }
}
