<?php

namespace CR\Library\Laravel\SybaseEloquent;

use Illuminate\Database\Query\Builder;

class Grammar extends \Illuminate\Database\Query\Grammars\Grammar
{
    public function compileSelect(Builder $query)
    {
        if (! $query->offset) {
            return parent::compileSelect($query);
        }

        if ($query->columns === null) {
            $query->columns = ['*'];
        }

        return $this->compileAnsiOffset(
            $query,
            $this->compileComponents($query)
        );
    }

    public function compileTruncate(Builder $query)
    {
        return ['truncate table ' . $this->wrapTable($query->from) => []];
    }

    public function getDateFormat()
    {
        return ServiceProvider::DEFAULT_DATE_FORMAT;
    }

    public function parseAnsiPdoSelect($query)
    {
        if (\str_contains($query, ';') && \str_contains($query, $this->buildAnsiAppendQuery())) {
            $copyRegex = \sprintf(
                '(.*?, %s as )(%s)( into )(#%s)([^;]*); ', // capture 1, 2, 3, 4, 5
                \preg_quote($this->selectIdentityKeyword(), '~'), // as \1
                \preg_quote($this->rowNumColumnName(), '~'), // as \2
                \preg_quote($this->tempTableName(), '~') // as \4
            );

            $updateRegex = '(update \4 set \2 = identity\(0\)); '; // capture 6
            $selectRegex = '(select \* from \4 where \2 (?:between \d+ and \d+|>= \d+))'; // capture 7

            $matchRegex = \sprintf('~^%s%s%s$~', $copyRegex, $updateRegex, $selectRegex);
            $matches = [];
            \preg_match($matchRegex, $query, $matches);

            return [
                $matches[1] . $matches[2] . $matches[3] . $matches[4] . $matches[5],
                $matches[6],
                $matches[7],
            ];
        }
    }

    protected function buildAnsiAppendQuery()
    {
        return \sprintf(', %s as %s into #%s', $this->selectIdentityKeyword(), $this->rowNumColumnName(), $this->tempTableName());
    }

    protected function compileAnsiOffset(Builder $query, $components)
    {
        $components['columns'] .= $this->buildAnsiAppendQuery();

        $sql = $this->concatenate($components);

        $start = $query->offset + 1;

        if ($query->limit > 0) {
            $finish = $query->offset + $query->limit;

            $constraint = "between {$start} and {$finish}";

            if ($finish < ServiceProvider::SELECT_TOP_MAX_LIMIT) {
                $sql = \preg_replace('/^select\b/', "select top ({$finish})", $sql);
            }
        }
        else {
            $constraint = ">= {$start}";
        }

        return \implode('; ', [
            $sql,
            \sprintf('update #%s set %s = identity(0)', $this->tempTableName(), $this->rowNumColumnName()),
            \sprintf('select * from #%s where %s %s', $this->tempTableName(), $this->rowNumColumnName(), $constraint),
        ]);
    }

    protected function compileColumns(Builder $query, $columns)
    {
        if ($query->aggregate !== null) {
            return null;
        }

        $select = $query->distinct ? 'select distinct ' : 'select ';

        // If there is a limit on the query, but not an offset, we will add the top
        // clause to the query, which serves as a "limit" type clause within the
        // SQL Server system similar to the limit keywords available in MySQL.
        if ($query->limit > 0 && $query->offset <= 0 && $query->limit < ServiceProvider::SELECT_TOP_MAX_LIMIT) {
            $select .= "top ({$query->limit}) ";
        }

        return $select . $this->columnize($columns);
    }

    protected function compileLimit(Builder $query, $limit)
    {
        return '';
    }

    protected function compileOffset(Builder $query, $offset)
    {
        return '';
    }

    protected function compileOrders(Builder $query, $orders)
    {
        if (\is_array($orders) && \count($orders) > 0 && \is_array($query->groups) && \count($query->groups) > 0) {
            return '';
        }

        return parent::compileOrders($query, $orders);
    }

    protected function rowNumColumnName()
    {
        return 'row_num';
    }

    protected function selectIdentityKeyword()
    {
        return 'identity(0)';
    }

    protected function tempTableName()
    {
        return 'temp_table';
    }

    protected function wrapValue($value)
    {
        return $value === '*' ? $value : '[' . \str_replace(']', ']]', $value) . ']';
    }
}
