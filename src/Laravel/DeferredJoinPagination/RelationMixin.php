<?php

namespace CR\Library\Laravel\DeferredJoinPagination;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Pagination\LengthAwarePaginator;

class RelationMixin
{
    /**
     * Get a paginator for the "select" statement.
     *
     * @param  int|null  $perPage
     * @param  array  $columns
     * @param  string  $pageName
     * @param  int|null  $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function deferredPaginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
    {
        return $this->relationDeferredPaginator('simpleDeferredPaginate', [
            $perPage, $columns, $pageName, $page,
        ]);
    }

    /**
     * Paginate the given query into a simple paginator.
     *
     * @param  int|null  $perPage
     * @param  array  $columns
     * @param  string  $pageName
     * @param  int|null  $page
     * @return \Illuminate\Contracts\Pagination\Paginator
     */
    public function simpleDeferredPaginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
    {
        return $this->relationDeferredPaginator('simpleDeferredPaginate', [
            $perPage, $columns, $pageName, $page,
        ]);
    }

    /**
     * @param  string  $method
     * @param  array  $arguments
     */
    private function relationDeferredPaginator($method, array $arguments)
    {
        if (! \method_exists($this, 'getQuery')) {
            return new LengthAwarePaginator([], 0, $arguments[0] ?: 1);
        }

        /** @var \Illuminate\Database\Eloquent\Relations\BelongsToMany|\Illuminate\Database\Eloquent\Relations\HasManyThrough $this */
        $this->getQuery()->addSelect($this->shouldSelect($arguments[1]));

        return tap($this->getQuery()->{$method}(...$arguments), function ($paginator) {
            if ($this instanceof BelongsToMany) {
                $this->hydratePivotRelation($paginator->items());
            }
        });
    }
}
