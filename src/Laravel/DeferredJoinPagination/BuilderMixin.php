<?php

namespace CR\Library\Laravel\DeferredJoinPagination;

use Closure;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Pagination\LengthAwarePaginator;
use InvalidArgumentException;

class BuilderMixin
{
    /**
     * Paginate the given query into a keyed paginator.
     *
     * @param  string|null  $key
     * @param  int|\Closure  $perPage
     * @param  array|string  $columns
     * @param  string  $pageName
     * @param  int|null  $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function deferredPaginateWith($key = null, $perPage = 15, $columns = ['*'], $pageName = 'page', $page = null)
    {
        return $this->deferredPaginator('paginate', [
            $perPage, $columns, $pageName, $page,
        ], $key);
    }

    /**
     * Get a keyed paginator only supporting simple next and previous links.
     *
     * This is more efficient on larger data-sets, etc.
     *
     * @param  string|null  $key
     * @param  int  $perPage
     * @param  array|string  $columns
     * @param  string  $pageName
     * @param  int|null  $page
     * @return \Illuminate\Contracts\Pagination\Paginator
     */
    public function simpleDeferredPaginateWith($key = null, $perPage = 15, $columns = ['*'], $pageName = 'page', $page = null)
    {
        return $this->deferredPaginator('simplePaginate', [
            $perPage, $columns, $pageName, $page,
        ], $key);
    }

    /**
     * Paginate the given query.
     *
     * @param  int|null|\Closure  $perPage
     * @param  array|string  $columns
     * @param  string  $pageName
     * @param  int|null  $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     *
     * @throws \InvalidArgumentException
     */
    public function deferredPaginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
    {
        return $this->deferredPaginator('paginate', [
            $perPage, $columns, $pageName, $page,
        ], null);
    }

    /**
     * Paginate the given query into a simple paginator.
     *
     * @param  int|null  $perPage
     * @param  array|string  $columns
     * @param  string  $pageName
     * @param  int|null  $page
     * @return \Illuminate\Contracts\Pagination\Paginator
     */
    public function simpleDeferredPaginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
    {
        return $this->deferredPaginator('simplePaginate', [
            $perPage, $columns, $pageName, $page,
        ], null);
    }

    /**
     * @param  string  $method
     * @param  array  $arguments
     * @param  string|int|null  $key
     */
    private function deferredPaginator($method, array $arguments, $key)
    {
        $whereMethod = 'whereIn';

        /** @var \Illuminate\Contracts\Database\Query\Builder $this */
        if ($this instanceof EloquentBuilder) {
            $model = $this->getModel();

            if ($model->getKeyType() === 'int') {
                $whereMethod = 'whereIntegerInRaw';
            }

            $key = $key ?: $model->getQualifiedKeyName();

            $builder = $this->toBase();
        }
        elseif ($this instanceof QueryBuilder) {
            $key = $key ?: $this->defaultKeyName();

            $builder = $this;
        }
        else {
            return new LengthAwarePaginator([], 0, $arguments[0] ?: 1);
        }

        $columns = $arguments[1];

        $arguments[1] = $key;

        $paginator = $builder->{$method}(...$arguments);

        $values = $paginator->map(static function ($item) {
            return head((array) $item);
        });

        return $paginator->setCollection(
            $this->{$whereMethod}($key, $values)
                ->simplePaginate($paginator->perPage() - 1, $columns, $paginator->getPageName(), 1)
                ->getCollection()
        );
    }
}
