<?php

namespace CR\Library\Laravel\EventLogger;

use CR\Library\Helpers\Helpers;
use Illuminate\Support\Facades\Log;

class LogHttpClientRequest
{
    public function handle(\Illuminate\Http\Client\Events\RequestSending $event)
    {
        Log::debug(\sprintf(
            '-httpc-client: %s Request Sending @%s %s',
            Helpers::hashObject($event->request),
            $event->request->method(),
            $event->request->url()
        ), [
            '__cr_internal' => true,
        ]);
    }
}
