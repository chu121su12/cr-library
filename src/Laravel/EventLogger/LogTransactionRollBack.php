<?php

namespace CR\Library\Laravel\EventLogger;

use CR\Library\Helpers\Helpers;
use Illuminate\Support\Facades\Log;

class LogTransactionRollBack
{
    public function handle(\Illuminate\Database\Events\TransactionRolledBack $event)
    {
        Log::debug(\sprintf(
            "-sql-transaction: %s Transaction on '%s' roll back at %s",
            Helpers::hashObject($event->connection->getPdo()),
            $event->connectionName,
            Helpers::microTime()
        ), [
            '__cr_internal' => true,
        ]);
    }
}
