<?php

namespace CR\Library\Laravel\EventLogger;

use Illuminate\Support\Facades\Log;

class LogScheduledTaskFinished
{
    public function handle(\Illuminate\Console\Events\ScheduledTaskFinished $event)
    {
        Log::debug(\sprintf(
            '-scheduled: finished [%s] [%s]',
            $event->task->description,
            $event->task->command
        ), [
            '__cr_internal' => true,
        ]);
    }
}
