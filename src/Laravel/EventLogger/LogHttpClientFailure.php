<?php

namespace CR\Library\Laravel\EventLogger;

use CR\Library\Helpers\Helpers;
use Illuminate\Support\Facades\Log;

class LogHttpClientFailure
{
    public function handle(\Illuminate\Http\Client\Events\ConnectionFailed $event)
    {
        Log::debug(\sprintf(
            '-http-client: %s Connection Failed @%s %s',
            Helpers::hashObject($event->request),
            $event->request->method(),
            $event->request->url()
        ), [
            '__cr_internal' => true,
        ]);
    }
}
