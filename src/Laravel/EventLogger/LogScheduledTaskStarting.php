<?php

namespace CR\Library\Laravel\EventLogger;

use Illuminate\Support\Facades\Log;

class LogScheduledTaskStarting
{
    public function handle(\Illuminate\Console\Events\ScheduledTaskStarting $event)
    {
        Log::debug(\sprintf(
            '-scheduled: executing [%s] [%s]',
            $event->task->description,
            $event->task->command
        ), [
            '__cr_internal' => true,
        ]);
    }
}
