<?php

namespace CR\Library\Laravel\EventLogger;

use CR\Library\Helpers\Helpers;
use Illuminate\Support\Facades\Log;

class LogTransactionStart
{
    public function handle(\Illuminate\Database\Events\TransactionBeginning $event)
    {
        Log::debug(\sprintf(
            "-sql-transaction: %s Transaction on '%s' start at %s",
            Helpers::hashObject($event->connection->getPdo()),
            $event->connectionName,
            Helpers::microTime()
        ), [
            '__cr_internal' => true,
        ]);
    }
}
