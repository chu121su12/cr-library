<?php

namespace CR\Library\Laravel\EventLogger;

use CR\Library\Providers\LibraryServiceProvider;
use Illuminate\Support\Facades\Log;

class LogCacheHit
{
    public function handle(\Illuminate\Cache\Events\CacheHit $event)
    {
        Log::debug(\sprintf(
            '-cache: [%s] hit [%s]',
            $event->storeName,
            $event->key
        ), [
            '__cr_internal' => \in_array($event->storeName, LibraryServiceProvider::CACHE_NAMES, true),
        ]);
    }
}
