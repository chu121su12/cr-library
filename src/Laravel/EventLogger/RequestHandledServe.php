<?php

namespace CR\Library\Laravel\EventLogger;

use Illuminate\Support\Facades\Cache;

class RequestHandledServe
{
    public function handle(\Illuminate\Foundation\Http\Events\RequestHandled $event)
    {
        $time = microtime(true);

        $line = [
            null, // trace
            $event->request->server('REQUEST_TIME_FLOAT') ?: $time,
            $time,
            $event->request->method(),
            $event->request->path(),
            $event->request->getHttpHost(),
            $event->response->getStatusCode(),
            $event->response->headers->get('content-length'),
        ];

        Cache::store('internal')->lock('rhs-lock', 10)->get(function () use ($line) {
            $data = (array) (Cache::store('internal')->get('rhs-data') ?: []);

            $data[] = $line;

            Cache::store('internal')->put('rhs-data', $data, 10);
        });
    }
}
