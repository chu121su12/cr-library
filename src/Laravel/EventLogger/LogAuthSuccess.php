<?php

namespace CR\Library\Laravel\EventLogger;

use Illuminate\Support\Facades\Log;

class LogAuthSuccess
{
    public function handle(\Illuminate\Auth\Events\Authenticated $event)
    {
        Log::debug(\sprintf(
            '-auth: success %s %s %s',
            $event->guard,
            \get_class($event->user),
            \json_encode($event->user)
        ), [
            '__cr_internal' => true,
        ]);

        Log::withContext([
            '__cr_user_id' => "[auth:{$event->user->getAuthIdentifier()}]",
        ]);
    }
}
