<?php

namespace CR\Library\Laravel\EventLogger;

use Illuminate\Support\Facades\Log;

class LogCommandFinished
{
    public function handle(\Illuminate\Console\Events\CommandFinished $event)
    {
        Log::debug(\sprintf(
            '-cli: finished [%s] %s',
            $event->command,
            $event->exitCode
        ), [
            '__cr_internal' => true,
        ]);

        Log::withContext([
            '__cr_task_id' => false,
        ]);
    }
}
