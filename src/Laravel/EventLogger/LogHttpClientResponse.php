<?php

namespace CR\Library\Laravel\EventLogger;

use CR\Library\Helpers\Helpers;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class LogHttpClientResponse
{
    public function handle(\Illuminate\Http\Client\Events\ResponseReceived $event)
    {
        $stat = $event->response->handlerStats();
        $stat['__cr_internal'] = true;

        Log::debug(\sprintf(
            '-httpc-client: %s Response Recieved @%s %s -> %s %s %s %s',
            Helpers::hashObject($event->request),
            $event->request->method(),
            $event->request->url(),
            \round(Arr::get($stat, 'pretransfer_time') * 1000, 2),
            \round(Arr::get($stat, 'total_time') * 1000, 2),
            $event->response->status(),
            $event->response->effectiveUri()
        ), $stat);
    }
}
