<?php

namespace CR\Library\Laravel\EventLogger;

use CR\Library\Providers\LibraryServiceProvider;
use Illuminate\Support\Facades\Log;

class LogCacheMiss
{
    public function handle(\Illuminate\Cache\Events\CacheMissed $event)
    {
        Log::debug(\sprintf(
            '-cache: [%s] miss [%s]',
            $event->storeName,
            $event->key
        ), [
            '__cr_internal' => \in_array($event->storeName, LibraryServiceProvider::CACHE_NAMES, true),
        ]);
    }
}
