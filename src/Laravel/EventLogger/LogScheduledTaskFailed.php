<?php

namespace CR\Library\Laravel\EventLogger;

use CR\Library\Helpers\Helpers;
use Illuminate\Support\Facades\Log;

class LogScheduledTaskFailed
{
    public function handle(\Illuminate\Console\Events\ScheduledTaskFailed $event)
    {
        Helpers::reportExceptionStack($event->exception);

        Log::debug(\sprintf(
            '-scheduled: failed [%s] [%s] %s',
            $event->task->description,
            $event->task->command,
            $event->exception->getMessage()
        ), [
            '__cr_internal' => true,
        ]);
    }
}
