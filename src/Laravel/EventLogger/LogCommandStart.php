<?php

namespace CR\Library\Laravel\EventLogger;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class LogCommandStart
{
    public function handle(\Illuminate\Console\Events\CommandStarting $event)
    {
        Log::debug(\sprintf(
            '-cli: executing [%s] [%s] %s',
            $event->command,
            \gethostname(),
            static::serializeEventInput($event->input)
        ), [
            '__cr_internal' => true,
        ]);

        Log::withContext([
            '__cr_task_id' => Str::random(),
        ]);
    }

    protected static function serializeEventInput($input)
    {
        if (! $input) {
            return '-';
        }

        if (\method_exists($input, 'inputgetArguments')) {
            return \json_encode($input->inputgetArguments());
        }

        return $input;
    }
}
