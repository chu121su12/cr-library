<?php

namespace CR\Library\Laravel\EventLogger;

use CR\Library\Helpers\Helpers;
use Illuminate\Support\Facades\Log;

class LogTransactionCommitted
{
    public function handle(\Illuminate\Database\Events\TransactionCommitted $event)
    {
        Log::debug(\sprintf(
            "-sql-transaction: %s Transaction on '%s' done at %s",
            Helpers::hashObject($event->connection->getPdo()),
            $event->connectionName,
            Helpers::microTime()
        ), [
            '__cr_internal' => true,
        ]);
    }
}
