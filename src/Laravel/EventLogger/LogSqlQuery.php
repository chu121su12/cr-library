<?php

namespace CR\Library\Laravel\EventLogger;

use CR\Library\Helpers\Database;
use CR\Library\Helpers\Helpers;
use Illuminate\Support\Facades\Log;

class LogSqlQuery
{
    protected static $excludeConnectionName = [
        'app-telescope',
    ];

    public function handle(\Illuminate\Database\Events\QueryExecuted $event)
    {
        if (! \in_array(
            $event->connectionName,
            static::$excludeConnectionName,
            true
        )) {
            self::logQuery($event, $event->connection);
        }
    }

    protected static function isInsertQuery($event)
    {
        return \mb_strtolower(\mb_substr(\ltrim($event->sql), 0, 6)) === 'insert';
    }

    protected static function logQuery($event, $connection)
    {
        $pdo = $connection->getPdo();

        $query = app()->isProduction() ? $event->sql : Database::naiveQueryBinder(
            $event->sql,
            $event->bindings,
            $pdo
        );

        if (self::isInsertQuery($event)) {
            $id = $pdo->lastInsertId();

            if (\is_int($id) || ! \preg_match('/\D/', $id)) {
                $lastInsertId = $id;
            }
            else {
                $lastInsertId = $id ? \sprintf('"%s"', \addslashes($id)) : '?';
            }
        }
        else {
            $lastInsertId = '-';
        }

        $logData = $connection->getDriverName() === 'sqlite'
            ? [
                Helpers::hashObject($pdo),
                $event->connectionName,
                $connection->getDriverName(),
                \basename($connection->getConfig('database')),
                '-',
                '-',
                $lastInsertId,
                $event->time,
                $query,
            ] : [
                Helpers::hashObject($pdo),
                $event->connectionName,
                $connection->getDriverName(),
                $connection->getConfig('database'),
                $connection->getConfig('host'),
                $connection->getConfig('port'),
                $lastInsertId,
                $event->time,
                $query,
            ];

        Log::debug(\sprintf(
            '-sql-query: %s %s %s/%s/%s:%s %s %s %s',
            ...$logData
        ), [
            '__cr_internal' => true,
            '__cr_limited' => true,
        ]);
    }
}
