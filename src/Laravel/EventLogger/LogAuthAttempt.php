<?php

namespace CR\Library\Laravel\EventLogger;

use Illuminate\Support\Facades\Log;

class LogAuthAttempt
{
    public function handle(\Illuminate\Auth\Events\Attempting $event)
    {
        Log::debug(\sprintf(
            '-auth: attempt %s',
            $event->guard
        ), [
            'credentials' => $event->credentials,
            '__cr_internal' => true,
            '__cr_limited' => true,
        ]);
    }
}
