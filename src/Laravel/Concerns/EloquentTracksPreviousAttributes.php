<?php

namespace CR\Library\Laravel\Concerns;

use Illuminate\Support\Arr;

// https://github.com/laravel/framework/pull/42504/files
trait EloquentTracksPreviousAttributes
{
    /**
     * The previous model attributes.
     *
     * @var array
     */
    protected $previous = [];

    /**
     * Begin tracking the models previous attributes.
     *
     * @return void
     */
    public static function bootTracksPreviousAttributes()
    {
        static::updated(static function ($model) {
            $model->syncPrevious();
        });
    }

    /**
     * Sync the previous attributes.
     *
     * @return $this
     */
    public function syncPrevious()
    {
        $this->previous = $this->getRawOriginal();

        return $this;
    }

    /**
     * Get the model's attribute values prior to an update.
     *
     * @param  string|null  $key
     * @param  mixed  $default
     * @return mixed|array
     */
    public function getPrevious($key = null, $default = null)
    {
        return (new static)->setRawAttributes(
            $this->previous,
            $sync = true
        )->getOriginalWithoutRewindingModel($key, $default);
    }

    /**
     * Get the model's raw previous attribute values.
     *
     * @param  string|null  $key
     * @param  mixed  $default
     * @return mixed|array
     */
    public function getRawPrevious($key = null, $default = null)
    {
        return Arr::get($this->previous, $key, $default);
    }
}
