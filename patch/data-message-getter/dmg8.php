<?php

namespace CR\Library\Helpers\Patch;

trait DataMessageGetter
{
    /**
     * @param  ?string $key
     * @param  mixed $fallback
     * @return ?array
     */
    public function array($key, $fallback = null)
    {
        return $this->getArray($key, $fallback);
    }

    /**
     * @param  string $key
     * @param  mixed $fallback
     * @return ?string
     */
    public function string($key, $fallback = null)
    {
        return $this->getString($key, $fallback);
    }

    /**
     * @deprecated
     * @param  ?string $key
     * @param  mixed $fallback
     * @return ?array
     */
    public function array_($key, $fallback = null)
    {
        return $this->getArray($key, $fallback);
    }

    /**
     * @deprecated
     * @param  string $key
     * @param  mixed $fallback
     * @return ?string
     */
    public function string_($key, $fallback = null)
    {
        return $this->getString($key, $fallback);
    }
}
