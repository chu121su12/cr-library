<?php

if (\version_compare(\PHP_VERSION, '8.0.0', '<')) {
    require __DIR__ . '/dmg5.php';
} else {
    require __DIR__ . '/dmg8.php';
}
