<?php

use CR\Library\NotPdo\SybaseFunctionAdapter as SFA;

if (! \function_exists('sybase_affected_rows'))
{
    function sybase_affected_rows($connection = null) {
        return SFA::sybase_affected_rows($connection);
    }
}

if (! \function_exists('sybase_close'))
{
    function sybase_close($connection = null) {
        return SFA::sybase_close($connection);
    }
}

if (! \function_exists('sybase_connect'))
{
    function sybase_connect(
        $servername = null,
        $username = null,
        $password = null,
        $charset = null,
        $appname = null,
        $new = false
    ) {
        return SFA::sybase_connect($servername, $username, $password, $charset, $appname, $new);
    }
}

if (! \function_exists('sybase_fetch_array'))
{
    function sybase_fetch_array($queryResult) {
        return SFA::sybase_fetch_assoc($queryResult);
    }
}

if (! \function_exists('sybase_fetch_assoc'))
{
    function sybase_fetch_assoc($queryResult) {
        return SFA::sybase_fetch_assoc($queryResult);
    }
}

if (! \function_exists('sybase_fetch_object'))
{
    function sybase_fetch_object($queryResult, $object = null) {
        return SFA::sybase_fetch_object($queryResult, $object);
    }
}

if (! \function_exists('sybase_fetch_row'))
{
    function sybase_fetch_row($queryResult) {
        return SFA::sybase_fetch_assoc($queryResult);
    }
}

if (! \function_exists('sybase_free_result'))
{
    function sybase_free_result($queryResult) {
        return SFA::sybase_free_result($queryResult);
    }
}

if (! \function_exists('sybase_num_fields'))
{
    function sybase_num_fields($queryResult) {
        SFA::fetchAll($queryResult);

        if (isset($queryResult->fetch[0])) {
            return \count((array) $queryResult->fetch[0]);
        }

        return 0;
    }
}

if (! \function_exists('sybase_num_rows'))
{
    function sybase_num_rows($queryResult) {
        SFA::fetchAll($queryResult);

        return $queryResult->fetchCount;
    }
}

if (! \function_exists('sybase_query'))
{
    function sybase_query($query, $connection = null) {
        SFA::sybase_query($query, $connection);
    }
}
