<?php

namespace CR\Library\Laravel\SybaseEloquent\Patch;

trait Statement
{
    public function fetchAll($how = null, $class_name = null, $ctor_args = null)
    {
        return $this->_fetchAll(...\func_get_args());
    }

    public function setFetchMode($mode, $params = null)
    {
        return $this->_setFetchMode(...\func_get_args());
    }
}
