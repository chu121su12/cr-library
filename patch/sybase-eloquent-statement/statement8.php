<?php

namespace CR\Library\Laravel\SybaseEloquent\Patch;

use PDO;

trait Statement
{
    public function fetchAll(int $fetch_style = PDO::FETCH_BOTH, mixed ...$fetch_args)
    {
        return $this->_fetchAll(...\func_get_args());
    }

    public function setFetchMode(int $mode, mixed ...$params)
    {
        return $this->_setFetchMode(...\func_get_args());
    }
}
