<?php

if (\version_compare(\PHP_VERSION, '7.0.0', '<')) {
    require __DIR__ . '/statement5.php';
} elseif (\version_compare(\PHP_VERSION, '8.0.0', '<')) {
    require __DIR__ . '/statement7.php';
} else {
    require __DIR__ . '/statement8.php';
}
